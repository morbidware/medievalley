{
    "id": "99e4de38-f25f-4021-b639-c77edf05e42f",
    "modelName": "GMExtension",
    "mvc": "1.2",
    "name": "Extension1",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 32,
    "date": "2018-11-07 10:05:56",
    "description": "",
    "exportToGame": true,
    "extensionName": "",
    "files": [
        {
            "id": "117a3285-791f-44ae-ad75-4e67d99587d9",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 96,
            "filename": "externalfunctions.js",
            "final": "",
            "functions": [
                {
                    "id": "ac9d5300-e529-4ef2-be4a-bea6d9c36e4d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getClientIdAtomic",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getClientIdAtomic",
                    "returnType": 1
                },
                {
                    "id": "17dec8e1-c1bb-4160-b6c4-a9ffa79edaa6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        1,
                        1,
                        1,
                        1,
                        2,
                        2,
                        2,
                        1,
                        1,
                        2,
                        2
                    ],
                    "externalName": "sendCharData",
                    "help": "x,y,sprite,sprite,sprite,sprite,image,scalex,scaley,socketid,name,room",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendCharData",
                    "returnType": 2
                },
                {
                    "id": "7fdd248a-c0cc-4bf2-a0f9-a58b40a310d5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2
                    ],
                    "externalName": "removePickable",
                    "help": "xx,yy,objectid",
                    "hidden": false,
                    "kind": 5,
                    "name": "removePickable",
                    "returnType": 2
                },
                {
                    "id": "3cd19cdf-3ff6-414c-9037-b8717f33c3f4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2,
                        1,
                        1,
                        1
                    ],
                    "externalName": "eventPerformInteractable",
                    "help": "xx,yy,iid,oid,evt,evn",
                    "hidden": false,
                    "kind": 5,
                    "name": "eventPerformInteractable",
                    "returnType": 2
                },
                {
                    "id": "43b1d9e9-dd3c-4da2-b850-0db22d89c256",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        1,
                        2
                    ],
                    "externalName": "dropPickable",
                    "help": "xx,yy,pid,quant",
                    "hidden": false,
                    "kind": 5,
                    "name": "dropPickable",
                    "returnType": 2
                },
                {
                    "id": "c42d9e8b-8779-4f2c-a678-dc4bed5f249d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "logga",
                    "help": "str",
                    "hidden": false,
                    "kind": 5,
                    "name": "logga",
                    "returnType": 2
                },
                {
                    "id": "230f6275-f727-4c0f-8580-ada73380792f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2
                    ],
                    "externalName": "changeGroundTile",
                    "help": "col,row,type",
                    "hidden": false,
                    "kind": 5,
                    "name": "changeGroundTile",
                    "returnType": 2
                },
                {
                    "id": "61e178fc-5fb5-4145-a3ad-3e0627ad5120",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2,
                        1
                    ],
                    "externalName": "sowPlant",
                    "help": "col,row,obj,str",
                    "hidden": false,
                    "kind": 5,
                    "name": "sowPlant",
                    "returnType": 2
                },
                {
                    "id": "f6c3bcab-4ad6-46a7-832b-792cdab127dd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2
                    ],
                    "externalName": "waterTile",
                    "help": "col,row",
                    "hidden": false,
                    "kind": 5,
                    "name": "waterTile",
                    "returnType": 2
                },
                {
                    "id": "0df73dd3-ccaa-4cc5-8618-50619a989806",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2,
                        1
                    ],
                    "externalName": "placeStructure",
                    "help": "x,y,obj,id",
                    "hidden": false,
                    "kind": 5,
                    "name": "placeStructure",
                    "returnType": 2
                },
                {
                    "id": "234bf960-f4d4-4e15-a62f-a4010490f16e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2,
                        1
                    ],
                    "externalName": "startStructureJob",
                    "help": "xx,yy,structure,outcome",
                    "hidden": false,
                    "kind": 5,
                    "name": "startStructureJob",
                    "returnType": 2
                },
                {
                    "id": "99e4b2fe-f2de-48b0-b33b-7eff29bc510a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 7,
                    "args": [
                        1,
                        2,
                        2,
                        2,
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "drawEquipped",
                    "help": "sid,xx,yy,ia,si,ii,xs,ys",
                    "hidden": false,
                    "kind": 5,
                    "name": "drawEquipped",
                    "returnType": 2
                },
                {
                    "id": "408f2472-5468-4d6a-a379-d3bb774f411e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendFarmString",
                    "help": "userid,farmstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendFarmString",
                    "returnType": 2
                },
                {
                    "id": "cc325a72-1f65-4b41-bc84-add3d55e73d6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendPickablesString",
                    "help": "userid,pickablesstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendPickablesString",
                    "returnType": 2
                },
                {
                    "id": "a1a92727-f401-4bcd-bf96-72150f263653",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendInteractablesString",
                    "help": "userid,interactablesstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendInteractablesString",
                    "returnType": 2
                },
                {
                    "id": "a6133ec7-b636-4ba6-85fa-df8a1d910154",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "getUserFarm",
                    "help": "userid,socketid",
                    "hidden": false,
                    "kind": 5,
                    "name": "getUserFarm",
                    "returnType": 2
                },
                {
                    "id": "3cb63137-d272-4444-a161-a4edac6939d8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getSocketList",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getSocketList",
                    "returnType": 2
                },
                {
                    "id": "b151f6d8-9514-4b25-8dfb-4715c4750d26",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "joinRoom",
                    "help": "socket",
                    "hidden": false,
                    "kind": 5,
                    "name": "joinRoom",
                    "returnType": 2
                },
                {
                    "id": "679bcb17-0c04-4a10-ba74-e02cb45bf16c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "findGetParameter",
                    "help": "str",
                    "hidden": false,
                    "kind": 5,
                    "name": "findGetParameter",
                    "returnType": 1
                },
                {
                    "id": "12b38dc7-7118-4188-854d-19cf18b835e5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "getPlayerData",
                    "help": "usrname",
                    "hidden": false,
                    "kind": 5,
                    "name": "getPlayerData",
                    "returnType": 2
                },
                {
                    "id": "32cc6ecc-5827-45f8-9c45-573539d79a80",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "getUserData",
                    "help": "socketid",
                    "hidden": false,
                    "kind": 5,
                    "name": "getUserData",
                    "returnType": 2
                },
                {
                    "id": "42fb49c5-d4d4-463d-af23-59c5bb8e0aac",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "storeAllInSocket",
                    "help": "farm,pick,int,usrname",
                    "hidden": false,
                    "kind": 5,
                    "name": "storeAllInSocket",
                    "returnType": 2
                },
                {
                    "id": "57568d52-058b-4c32-b077-ad4880137bb7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "updateFarm",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "updateFarm",
                    "returnType": 2
                },
                {
                    "id": "6365b539-7430-4e91-8947-e866b12a18c7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "storeItemsInSocket",
                    "help": "str",
                    "hidden": false,
                    "kind": 5,
                    "name": "storeItemsInSocket",
                    "returnType": 2
                },
                {
                    "id": "b319d6fd-6e36-4ca0-8988-8d534de79732",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendItemsString",
                    "help": "userid,itemsString,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendItemsString",
                    "returnType": 2
                },
                {
                    "id": "6d82432a-6c45-47ee-8f3f-55694ff8c731",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        2,
                        2
                    ],
                    "externalName": "sendItemToUserStash",
                    "help": "userid,itemid,quantity,durability",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendItemToUserStash",
                    "returnType": 2
                },
                {
                    "id": "25eefa2d-dc18-44f8-a232-e56b3ff2bc40",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendMissionsString",
                    "help": "userid,missionsString,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendMissionsString",
                    "returnType": 2
                },
                {
                    "id": "9b09cd0f-794d-4324-9296-7cb0fec7522c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendUnlockablesString",
                    "help": "userid,unlockablesString,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendUnlockablesString",
                    "returnType": 2
                },
                {
                    "id": "74769b1f-cb79-4c83-aa3c-502e5227429d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        1,
                        1
                    ],
                    "externalName": "magnetPickable",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "magnetPickable",
                    "returnType": 2
                },
                {
                    "id": "022d8764-308e-4b03-8096-5d6d550ea88d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendHouseInteractablesString",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendHouseInteractablesString",
                    "returnType": 2
                },
                {
                    "id": "c5d737cc-c2bd-4a2c-9b9d-58f6007c0f44",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendHousePickablesString",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendHousePickablesString",
                    "returnType": 2
                },
                {
                    "id": "5fc4f351-e560-445a-bb45-b3f8354d657f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "clearPlayerData",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "clearPlayerData",
                    "returnType": 2
                },
                {
                    "id": "cd39d250-3a55-47cc-be8b-c69cb1b2a1ed",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendUserdataString",
                    "help": "userid,userdataString,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendUserdataString",
                    "returnType": 2
                },
                {
                    "id": "5d080e72-ddfd-4876-b926-17be05e12f56",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "finishedLoading",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "finishedLoading",
                    "returnType": 2
                },
                {
                    "id": "a538b2e5-6390-4e05-a82b-be88b2dbdc13",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getTimestamp",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getTimestamp",
                    "returnType": 2
                },
                {
                    "id": "5c162fb9-63b8-4a24-b7ce-52e40a357348",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "exitFull",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "exitFull",
                    "returnType": 2
                },
                {
                    "id": "3945d961-4429-4da0-890e-dc522d3f3242",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "getPlayerRecord",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getPlayerRecord",
                    "returnType": 2
                },
                {
                    "id": "013e25bd-196c-4268-9757-f09d9bba0ab3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        1
                    ],
                    "externalName": "gasend",
                    "help": "userid,cat,quant",
                    "hidden": false,
                    "kind": 5,
                    "name": "gasend",
                    "returnType": 2
                },
                {
                    "id": "525185ff-6fae-4cb9-9ac6-182de052761e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendNumPlantsString",
                    "help": "userid,numPlants,",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendNumPlantsString",
                    "returnType": 2
                },
                {
                    "id": "57a9089e-cad8-4cb6-bb83-e1494965ef6b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendNumRocksString",
                    "help": "userid,numRocks,",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendNumRocksString",
                    "returnType": 2
                },
                {
                    "id": "c43576b4-11fb-49d1-9371-5131b70fe057",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendNumTreesString",
                    "help": "userid,numTrees,",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendNumTreesString",
                    "returnType": 2
                },
                {
                    "id": "8299044a-347d-4203-be73-cea92d3d4f11",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        2
                    ],
                    "externalName": "removeInteractable",
                    "help": "xx,yy,objectid",
                    "hidden": false,
                    "kind": 5,
                    "name": "removeInteractable",
                    "returnType": 2
                },
                {
                    "id": "d52c22c1-f2a3-4de7-b192-ed8bf136c018",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendNumVisitorsString",
                    "help": "userId,numVisitors,",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendNumVisitorsString",
                    "returnType": 2
                },
                {
                    "id": "be78f715-349c-4844-8214-286dfb8b9b32",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "kickAllPlayers",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "kickAllPlayers",
                    "returnType": 2
                },
                {
                    "id": "f4b0e0f6-33f9-408e-87d0-e56073666336",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "leaveRoom",
                    "help": "socketid",
                    "hidden": false,
                    "kind": 5,
                    "name": "leaveRoom",
                    "returnType": 2
                },
                {
                    "id": "56f62473-57ec-4abc-b9c3-67fd6b7dce0f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "sendCharResting",
                    "help": "socketid,value",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendCharResting",
                    "returnType": 2
                },
                {
                    "id": "b71e3d5b-d9eb-44c0-a6be-e75a0efed933",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "reloadPage",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "reloadPage",
                    "returnType": 2
                },
                {
                    "id": "e21ff08b-fa09-4cb4-b2fd-65afdc3a3a12",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "getClientsInRoom",
                    "help": "roomName",
                    "hidden": false,
                    "kind": 5,
                    "name": "getClientsInRoom",
                    "returnType": 2
                },
                {
                    "id": "4feb1e7a-17ff-4977-9573-2abbbfd3a1ba",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "sendEmote",
                    "help": "username,emoteType",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendEmote",
                    "returnType": 2
                },
                {
                    "id": "428c97e0-f49d-4b83-b7c9-1f7571f041a4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getYOffset",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getYOffset",
                    "returnType": 2
                },
                {
                    "id": "6877506e-9dde-4de5-a696-461d99607ccc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "isMobile",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "isMobile",
                    "returnType": 2
                },
                {
                    "id": "dc6cb885-69a6-4919-8bc8-71941e253891",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        1,
                        1,
                        1,
                        1,
                        1,
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "sendBadge",
                    "help": "usrid,challengeType,win,finishTime,gold,a,b,c,d,e,l",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendBadge",
                    "returnType": 2
                },
                {
                    "id": "9e4e5c38-c847-4851-a5b2-bba65fd3d671",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "checkIfPlayerCanPlayChallenge",
                    "help": "usrid,action",
                    "hidden": false,
                    "kind": 5,
                    "name": "checkIfPlayerCanPlayChallenge",
                    "returnType": 2
                },
                {
                    "id": "6b998800-e9b7-4e29-85ef-8a599e169aeb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "visitURL",
                    "help": "url",
                    "hidden": false,
                    "kind": 5,
                    "name": "visitURL",
                    "returnType": 2
                },
                {
                    "id": "ca74dbd8-4071-4fa6-b826-16b7368d9b4c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendDeathDataString",
                    "help": "usrid,deathData,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendDeathDataString",
                    "returnType": 2
                },
                {
                    "id": "a08647b2-8794-46f1-bc0d-e593dfc6556a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "postChallengeResult",
                    "help": "igid,challengetype,won,time,gold",
                    "hidden": false,
                    "kind": 5,
                    "name": "postChallengeResult",
                    "returnType": 2
                },
                {
                    "id": "e1a46b35-88fc-4b68-98c1-d155734ac100",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "findUserDataParameter",
                    "help": "str",
                    "hidden": false,
                    "kind": 5,
                    "name": "findUserDataParameter",
                    "returnType": 1
                },
                {
                    "id": "7184988d-8647-4f64-a169-2067c84b9fc8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "getUserCreationDate",
                    "help": "userigid",
                    "hidden": false,
                    "kind": 5,
                    "name": "getUserCreationDate",
                    "returnType": 2
                },
                {
                    "id": "37e63224-60e7-414e-a593-71da6da03ebf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendUserstatsString",
                    "help": "userid,userstatsString,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendUserstatsString",
                    "returnType": 2
                },
                {
                    "id": "6c55e76c-8163-4ab7-a57a-d209cb5e7f26",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getCSD",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getCSD",
                    "returnType": 2
                },
                {
                    "id": "d2bfa74f-8601-4c20-b628-b73e1643e230",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "getUserHistory",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getUserHistory",
                    "returnType": 2
                },
                {
                    "id": "1cf25364-54c7-4936-bcd4-bb414eeb4fc1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "removeMalus",
                    "help": "igid,seconds",
                    "hidden": false,
                    "kind": 5,
                    "name": "removeMalus",
                    "returnType": 2
                },
                {
                    "id": "572d85c5-6b76-4955-aae9-8da7b821f088",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "blogga",
                    "help": "str",
                    "hidden": false,
                    "kind": 5,
                    "name": "blogga",
                    "returnType": 2
                },
                {
                    "id": "7f044b47-8044-4ae6-adf3-118e3264b4f6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "insertRecord",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "insertRecord",
                    "returnType": 2
                },
                {
                    "id": "1665e4cf-e03a-4d54-9686-993550a266d3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "getChallengeData",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getChallengeData",
                    "returnType": 2
                },
                {
                    "id": "099a1102-72dc-4332-bf74-304f6c65dfde",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "goFull",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "goFull",
                    "returnType": 2
                },
                {
                    "id": "e817cd77-721c-4824-a7ac-1da8572d3dea",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "getIsChallenge",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "getIsChallenge",
                    "returnType": 2
                },
                {
                    "id": "fcedbc4b-1d66-4682-b95e-fe40ffd7aa0b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "refreshFullScreen",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "refreshFullScreen",
                    "returnType": 2
                },
                {
                    "id": "f3da5b0f-3b39-4ca6-810c-9c7fde650fee",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "alerta",
                    "help": "msg",
                    "hidden": false,
                    "kind": 5,
                    "name": "alerta",
                    "returnType": 2
                },
                {
                    "id": "a5b2d9d5-dbbf-4301-9f14-fffc6199de84",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "sendUserdataSurvivalString",
                    "help": "userid,userdataString,username",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendUserdataSurvivalString",
                    "returnType": 2
                },
                {
                    "id": "edf47097-d740-4dd6-a939-4ae596b4c9ff",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendStoreyString",
                    "help": "userid,farmstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendStoreyString",
                    "returnType": 2
                },
                {
                    "id": "4a8245ff-4e63-41ba-ac09-d7222befba7d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendFarmStringSurvival",
                    "help": "userid,farmstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendFarmStringSurvival",
                    "returnType": 2
                },
                {
                    "id": "f1f1ff99-232e-46c2-80e5-61347f8391ab",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendWallsStringSurvival",
                    "help": "userid,farmstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendWallsStringSurvival",
                    "returnType": 2
                },
                {
                    "id": "d3a115f3-5831-48e7-a7e7-fac86103f255",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendMobsStringSurvival",
                    "help": "userid,farmstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendMobsStringSurvival",
                    "returnType": 2
                },
                {
                    "id": "e2c3968a-ae21-4449-b3c3-232012ab302c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        1,
                        2,
                        2,
                        2,
                        1,
                        2,
                        2,
                        2,
                        2
                    ],
                    "externalName": "sendMobData",
                    "help": "x,y,sprite_index,image_index,scalex,mid,sid,life,state,targetx,targety",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendMobData",
                    "returnType": 2
                },
                {
                    "id": "931ed834-23ae-425e-94f5-24637b91ae1d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "getClientsInIsland",
                    "help": "islandName",
                    "hidden": false,
                    "kind": 5,
                    "name": "getClientsInIsland",
                    "returnType": 2
                },
                {
                    "id": "6c5e071e-c758-40ae-987b-0e6a86f0f3b7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "generateIsland",
                    "help": "usrname",
                    "hidden": false,
                    "kind": 5,
                    "name": "generateIsland",
                    "returnType": 2
                },
                {
                    "id": "aab3e71e-25cb-4d1c-a1b4-c23cf2a6be21",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1,
                        1
                    ],
                    "externalName": "sendInteractablesStringSurvival",
                    "help": "landid,interactablesstring",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendInteractablesStringSurvival",
                    "returnType": 2
                },
                {
                    "id": "0d7f709d-6315-4393-a12e-5728e1e12c46",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "getNewMobOwner",
                    "help": "landid",
                    "hidden": false,
                    "kind": 5,
                    "name": "getNewMobOwner",
                    "returnType": 2
                },
                {
                    "id": "4882a3d5-42e5-4c66-8b11-49e93eee575c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "sendDummyDataToOwner",
                    "help": "damage,socket",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendDummyDataToOwner",
                    "returnType": 2
                },
                {
                    "id": "a3394ac0-bf5b-4123-abdd-6501358651b4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        1
                    ],
                    "externalName": "sendDummyDataToMobOwner",
                    "help": "mobid,damage,socketid",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendDummyDataToMobOwner",
                    "returnType": 2
                },
                {
                    "id": "4cf67adc-8688-48a4-bf81-6004bd3f8581",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        1,
                        2
                    ],
                    "externalName": "storeAllInSocketSurvival",
                    "help": "",
                    "hidden": false,
                    "kind": 5,
                    "name": "storeAllInSocketSurvival",
                    "returnType": 2
                },
                {
                    "id": "f50934f1-5c96-4cea-9ee1-bd5ea22c6e1a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        1
                    ],
                    "externalName": "sendOneInteractableSurvival",
                    "help": "global.landid, compressed_string",
                    "hidden": false,
                    "kind": 5,
                    "name": "sendOneInteractableSurvival",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 5,
            "order": [
                "c42d9e8b-8779-4f2c-a678-dc4bed5f249d",
                "a538b2e5-6390-4e05-a82b-be88b2dbdc13",
                "679bcb17-0c04-4a10-ba74-e02cb45bf16c",
                "e1a46b35-88fc-4b68-98c1-d155734ac100",
                "ac9d5300-e529-4ef2-be4a-bea6d9c36e4d",
                "3cb63137-d272-4444-a161-a4edac6939d8",
                "12b38dc7-7118-4188-854d-19cf18b835e5",
                "5fc4f351-e560-445a-bb45-b3f8354d657f",
                "32cc6ecc-5827-45f8-9c45-573539d79a80",
                "a6133ec7-b636-4ba6-85fa-df8a1d910154",
                "57568d52-058b-4c32-b077-ad4880137bb7",
                "17dec8e1-c1bb-4160-b6c4-a9ffa79edaa6",
                "56f62473-57ec-4abc-b9c3-67fd6b7dce0f",
                "b151f6d8-9514-4b25-8dfb-4715c4750d26",
                "cd39d250-3a55-47cc-be8b-c69cb1b2a1ed",
                "408f2472-5468-4d6a-a379-d3bb774f411e",
                "cc325a72-1f65-4b41-bc84-add3d55e73d6",
                "c5d737cc-c2bd-4a2c-9b9d-58f6007c0f44",
                "a1a92727-f401-4bcd-bf96-72150f263653",
                "022d8764-308e-4b03-8096-5d6d550ea88d",
                "b319d6fd-6e36-4ca0-8988-8d534de79732",
                "25eefa2d-dc18-44f8-a232-e56b3ff2bc40",
                "9b09cd0f-794d-4324-9296-7cb0fec7522c",
                "42fb49c5-d4d4-463d-af23-59c5bb8e0aac",
                "6365b539-7430-4e91-8947-e866b12a18c7",
                "7fdd248a-c0cc-4bf2-a0f9-a58b40a310d5",
                "3cd19cdf-3ff6-414c-9037-b8717f33c3f4",
                "43b1d9e9-dd3c-4da2-b850-0db22d89c256",
                "74769b1f-cb79-4c83-aa3c-502e5227429d",
                "230f6275-f727-4c0f-8580-ada73380792f",
                "61e178fc-5fb5-4145-a3ad-3e0627ad5120",
                "f6c3bcab-4ad6-46a7-832b-792cdab127dd",
                "0df73dd3-ccaa-4cc5-8618-50619a989806",
                "234bf960-f4d4-4e15-a62f-a4010490f16e",
                "99e4b2fe-f2de-48b0-b33b-7eff29bc510a",
                "6d82432a-6c45-47ee-8f3f-55694ff8c731",
                "5d080e72-ddfd-4876-b926-17be05e12f56",
                "5c162fb9-63b8-4a24-b7ce-52e40a357348",
                "3945d961-4429-4da0-890e-dc522d3f3242",
                "013e25bd-196c-4268-9757-f09d9bba0ab3",
                "57a9089e-cad8-4cb6-bb83-e1494965ef6b",
                "c43576b4-11fb-49d1-9371-5131b70fe057",
                "525185ff-6fae-4cb9-9ac6-182de052761e",
                "d52c22c1-f2a3-4de7-b192-ed8bf136c018",
                "be78f715-349c-4844-8214-286dfb8b9b32",
                "8299044a-347d-4203-be73-cea92d3d4f11",
                "f4b0e0f6-33f9-408e-87d0-e56073666336",
                "b71e3d5b-d9eb-44c0-a6be-e75a0efed933",
                "e21ff08b-fa09-4cb4-b2fd-65afdc3a3a12",
                "4feb1e7a-17ff-4977-9573-2abbbfd3a1ba",
                "428c97e0-f49d-4b83-b7c9-1f7571f041a4",
                "6877506e-9dde-4de5-a696-461d99607ccc",
                "dc6cb885-69a6-4919-8bc8-71941e253891",
                "9e4e5c38-c847-4851-a5b2-bba65fd3d671",
                "6b998800-e9b7-4e29-85ef-8a599e169aeb",
                "ca74dbd8-4071-4fa6-b826-16b7368d9b4c",
                "a08647b2-8794-46f1-bc0d-e593dfc6556a",
                "7184988d-8647-4f64-a169-2067c84b9fc8",
                "37e63224-60e7-414e-a593-71da6da03ebf",
                "6c55e76c-8163-4ab7-a57a-d209cb5e7f26",
                "d2bfa74f-8601-4c20-b628-b73e1643e230",
                "1cf25364-54c7-4936-bcd4-bb414eeb4fc1",
                "572d85c5-6b76-4955-aae9-8da7b821f088",
                "7f044b47-8044-4ae6-adf3-118e3264b4f6",
                "1665e4cf-e03a-4d54-9686-993550a266d3",
                "099a1102-72dc-4332-bf74-304f6c65dfde",
                "e817cd77-721c-4824-a7ac-1da8572d3dea",
                "fcedbc4b-1d66-4682-b95e-fe40ffd7aa0b",
                "f3da5b0f-3b39-4ca6-810c-9c7fde650fee",
                "a5b2d9d5-dbbf-4301-9f14-fffc6199de84",
                "edf47097-d740-4dd6-a939-4ae596b4c9ff",
                "4a8245ff-4e63-41ba-ac09-d7222befba7d",
                "f1f1ff99-232e-46c2-80e5-61347f8391ab",
                "d3a115f3-5831-48e7-a7e7-fac86103f255",
                "e2c3968a-ae21-4449-b3c3-232012ab302c",
                "931ed834-23ae-425e-94f5-24637b91ae1d",
                "6c5e071e-c758-40ae-987b-0e6a86f0f3b7",
                "aab3e71e-25cb-4d1c-a1b4-c23cf2a6be21",
                "0d7f709d-6315-4393-a12e-5728e1e12c46",
                "4882a3d5-42e5-4c66-8b11-49e93eee575c",
                "a3394ac0-bf5b-4123-abdd-6501358651b4",
                "4cf67adc-8688-48a4-bf81-6004bd3f8581",
                "f50934f1-5c96-4cea-9ee1-bd5ea22c6e1a"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "options": null,
    "optionsFile": "options.json",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "supportedTargets": 32,
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosdelegatename": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "0.0.1"
}