//external functions, these must be declared inside gamemaker too in the Extensions.
//these functions must always return a number or a string, that's why many return 0
//these functions are called from inside the game and they can talk to the server
//by emitting events with socket.emit("event",args);

//this is used for logging to the client console

function gasend(userid,cat,quant)
{
	logga("gasend " + userid + " " + cat + " " + quant);
	window.ga('send', 'event', userid, cat, quant);
	return 0;
}

function goFull()
{
	logga("externalfunctions: goFull()");
	goFullscreenReset();
	return 0;
}

function exitFull()
{
	exitFullscreen();
	return 0;
}

function finishedLoading()
{
	gameLoaded();
	return 0;
}

function reloadPage()
{
	location.reload(true);
	return 0;
}

function alerta(msg)
{
	//window.alert(msg);
	return 0;
}

function getClientsInRoom(roomname)
{
	socket.emit("get clients in room",roomname);
	return 0;
}

function getClientsInIsland(roomname)
{
	socket.emit("get clients in island",roomname);
	return 0;
}

function registerUser( challenge )
{
	console.log("registerUser() challenge: " + challenge);
	socket.emit("register user",window.userigid,window.useremail, challenge);
	return 0;
}

function refreshFullScreen()
{
	logga("refreshFullScreen");
	//alert("ext funcs refreshFullScreen");
	//goFullScreen2();
	goFullscreen();
	return 0;
}

function logga(str)
{
	console.log(str);
	//socket.emit("logga",str);
	return 0;
}

function blogga(str)
{
	socket.emit("bunyan log",str);
	return 0;
}

function getTimestamp()
{
	if (!Date.now) {
    	Date.now = function() { return new Date().getTime(); }
	}
	return Date.now();
}

function getYOffset()
{
	return window.pageYOffset;
}

function isMobile() {

	if(/Mobi|Android/i.test(navigator.userAgent)
	|| /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
   		return 1;
	}
	return 0;
}

function getIsChallenge()
{
	var str = "/challenges";
	var n = window.location.href.search(str);
	if(n >= 0)
	{
		return 1;
	}

	str = "/chl";
	n = window.location.href.search(str);
	if(n >= 0)
	{
		return 1;
	}

	return 0;
}

//this is used on startup to get the username from the url
function findGetParameter(parameterName) {
	//hack.. parlare con Diego...
	if( parameterName == "challenge" )
		return 1;
		
    var result = 0,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

//this is used on startup to get the username from the url
function findUserDataParameter(parameterName) 
{
	if(parameterName == "_indiegala_username")
	{
		return window.username;
	}
	return window.ud[parameterName];
}
//CALLED ON START UP
function getPlayerRecord(usrigid)
{
	socket.emit("get player record", usrigid, function(err, success) 
	{
		if(err)
		{
			logga("error while getPlayerRecord: " + err);
			gm_call("gml_Script_gmcallback_getPlayerData_failed");
		}
	});
	return 0;
}

function fixName()
{
	socket.emit("fix name",encodeURIComponent(window.username), function(err, success) 
	{
		if(err)
		{
			logga("error while fixing name " + window.username + " :" + err);
		}
	});
}
function generateIsland(usrname,usrigid)
{
	console.log("gm:externalfunctions:generateIstand");

	socket.emit("generate island",usrname,usrigid, function(err, success) 
	{
		if(err)
		{
			logga("error while generatingIsland: " + err);
			gm_call("gml_Script_gmcallback_getPlayerData_failed");
		}
	});
	return 0;
}
//CALLED WHEN CLICKING NEW GAME OR CONTINUE OR PLAY CHALLENGE
function getPlayerData(usrname,usrigid)
{
	console.log("gm:externalfunctions:getPlayerData("+usrname+")");
	//logga("gm:externalfunctions:getPlayerData("+usrname+","+usrigid+")");
	window.username = usrname;
	window.userigid = usrigid;
	//UTF here we must send usrname ENCODED
	//UTF Point is, data between sockets must travel ENCODED
	socket.emit("get player data",usrname, usrigid, function(err, success) 
	{
		if(err)
		{
			logga("error while getPlayerData: " + err);
			gm_call("gml_Script_gmcallback_getPlayerData_failed");
		}
	});
	return 0;
}
//this is used to clear all progress for the logged user
function clearPlayerData(usrigid)
{
	//console.log("gm:externalfunctions:getPlayerData("+usrname+")");
	//logga("gm:externalfunctions:clearPlayerData("+usrigid+")");
	window.userigid = usrigid;
	socket.emit("clear player data", usrigid, function(err, success) 
	{
		if(err)
		{
			logga("error while clearPlayerData: " + err);
			gm_call("gml_Script_gmcallback_deletePlayerData_failed");
		}
	});
	return 0;
}

//GOING TO ANOTHER FARM 1: this will call the server with the socketid we want to get info from to go to another user farm
function getUserData(sock)
{
	socket.emit("reach user for data",sock, function(err, success) 
	{
		if(err)
		{
			gm_call("gml_Script_gmcallback_getUserData_failed");
		}
	});
	return 0;
}
function getSocketList()
{
	//console.log("gm:externalfunctions:getSocketList");
	socket.emit("get socket list");
	return 0;
}

function kickAllPlayers()
{
	socket.emit("kick all players");
	return 0;
}

function getClientIdAtomic()
{
	//console.log("gm:externalfunctions:getClientIdAtomic");
	return socket.id;
}

function sendCharResting(id,val)
{
	//logga("gm:externalfunctions:sendCharResting " + id + " " + val)
	socket.emit('send char resting',id,val);
	return 0;
}
function sendDummyDataToOwner(dmg,sid)//damage, socket
{
	//logga("gm:externalfunctions:sendDummyDataToOwner");
	
	socket.emit('send dummy data to owner',dmg,sid);
	return 0;
}
function sendDummyDataToMobOwner(mid,dmg,sid)//damage, socket
{
	//logga("gm:externalfunctions:sendDummyDataToMobOwner");
	socket.emit('send dummy data to mob owner',mid,dmg,sid);
	return 0;
}
function sendCharData(xx,yy,ss1,ss2,ss3,ss4,ii,sx,sy,id,nam,sta)//xpos, ypos, sprite_index x 4, image_index, scalex, scaley, socketid, name, stamina
{
	//logga("gm:externalfunctions:sendCharData "+xx+" "+yy+" "+ss1+" "+ss2+" "+ss3+" "+ss4+" "+ii+" "+sx+" "+sy+" "+id+" "+nam+" "+sta);
	socket.emit('send char data',xx,yy,ss1,ss2,ss3,ss4,ii,sx,sy,id,nam,sta);
	return 0;
}

function sendMobData(xx,yy,si,ii,sx,mid,sid,lf,stt,trgtx,trgty)//xpos, ypos, sprite_index, image_index, scalex, id, life, state, targetx, targety
{
	//logga("gm:externalfunctions:sendMobData "+xx+" "+yy+" "+si+" "+ii+" "+sx+" "+id);
	socket.emit('send mob data',xx,yy,si,ii,sx,mid,sid,lf,stt,trgtx,trgty);
	return 0;
}
function removePickable(xx,yy,oid)//pickable_id
{
	//logga("gm:externalfunctions:removePickable "+xx+" "+yy+" "+oid);
	socket.emit('remove pickable',xx,yy,oid);
	return 0;
}

function removeInteractable(xx,yy,oid)//pickable_id
{
	//logga("gm:externalfunctions:removePickable "+xx+" "+yy+" "+oid);
	socket.emit('remove interactable',xx,yy,oid);
	return 0;
}

function eventPerformInteractable(xx,yy,iid,oid,evt,evn)
{
	//console.log("gm:externalfunctions:eventPerformInteractable "+xx+" "+yy+" "+iid+" "+oid+" "+evt+" "+evn);
	socket.emit('event perform interactable',xx,yy,iid,oid,evt,evn);
	return 0;
}

function dropPickable(xx,yy,pid,quant)//x pos, y pos, pickableid, quantity
{
	//console.log("gm:externalfunctions:dropPickable "+xx+" "+yy+" "+pid+" "+quant);
	socket.emit('drop pickable',xx,yy,pid,quant);
	return 0;
}

function magnetPickable(xx,yy,pid,sid)//x pos, y pos, pickableid, quantity
{
	//console.log("gm:externalfunctions:magnetPickable "+xx+" "+yy+" "+pid+" "+sid);
	socket.emit('magnet pickable',xx,yy,pid,sid);
	return 0;
}

function changeGroundTile(col,row,type)//
{
	//console.log("gm:externalfunctions:changeGroundTile "+col+" "+row+" "+type);
	socket.emit('change ground tile',col,row,type);
	return 0;
}

function sowPlant(col,row,obj,str)//
{
	//console.log("gm:externalfunctions:sowPlant "+col+" "+row+" "+obj+" "+str);
	socket.emit('sow plant',col,row,obj,str);
	return 0;
}

function waterTile(col,row)//
{
	console.log("gm:externalfunctions:waterTile "+col+" "+row);
	socket.emit('water tile',col,row);
	return 0;
}

function sendEmote(usr,type)//
{
	//console.log("gm:externalfunctions:sendEmote "+usr+" "+type);
	socket.emit('send emote',usr,type);
	return 0;
}

function placeStructure(xx,yy,obj,id)//
{
	//console.log("gm:externalfunctions:placeStructure "+xx+" "+yy+" "+obj+" "+id);
	socket.emit('place structure',xx,yy,obj,id);
	return 0;
}

function startStructureJob(xx,yy,obj,id)//
{
	//console.log("gm:externalfunctions:startStructureJob "+xx+" "+yy+" "+obj+" "+id);
	socket.emit('start structure job',xx,yy,obj,id);
	return 0;
}

function drawEquipped(sid,xx,yy,ia,si,ii,xs,ys)//
{
	//console.log("gm:externalfunctions:drawEquipped "+sid+" "+xx+" "+yy+" "+ia+" "+si+" "+ii+" "+xs+" "+ys);
	socket.emit('draw equipped',sid,xx,yy,ia,si,ii,xs,ys);
	return 0;
}

function sendUserdataString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendUserdataString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send userdata string',usrid,fstr,username);
	return 0;
}

function sendUserstatsString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendUserstatsString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send userstats string',usrid,fstr,username);
	return 0;
}

function sendFarmString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendFarmString "+username);
	//logga("gm:externalfunctions:sendFarmString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send farm string',usrid,fstr,username);
	return 0;
}

function sendFarmStringSurvival(landid,fstr,username)
{
	//logga("gm:externalfunctions:sendFarmString "+username + " " + landid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send farm string',landid,compressed,username);
	return 0;
}

function sendStoreyString(landid,fstr,username)
{
	//logga("gm:externalfunctions:sendStoreyString "+username + " " + landid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send storey string',landid,compressed,username);
	return 0;
}

function sendWallsStringSurvival(landid,fstr,username)
{
	///logga("gm:externalfunctions:sendWallsString "+username + " " + landid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send walls string',landid,compressed,username);
	return 0;
}

function sendMobsStringSurvival(landid,fstr,username)
{
	//logga("gm:externalfunctions:sendMobsString "+username + " " + landid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send mobs string',landid,compressed,username);
	return 0;
}

function sendPickablesString(landid,fstr,username)
{
	//console.log("gm:externalfunctions:sendPickablesString "+username + " " + usrid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send pickables string',landid,compressed,username);
	return 0;
}

function sendHousePickablesString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendHousePickablesString " + username + " " + usrid);// + " " + fstr);
	socket.emit('send house pickables string',usrid,fstr,username);
	return 0;
}

function sendInteractablesString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendInteractablesString "+username + " " + usrid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send interactables string',usrid,compressed,username);
	return 0;
}
function sendOneInteractableSurvival(landid,fstr)
{
	socket.emit('send one interactable string',landid,fstr);
	return 0;
}
function sendInteractablesStringSurvival(landid,fstr,username)
{
	//console.log("gm:externalfunctions:sendInteractablesString "+username + " " + usrid);// + " " + fstr);
	var compressed = LZString.compressToBase64(fstr);
	socket.emit('send interactables string survival',landid,compressed,username);
	return 0;
}

function sendNumTreesString(usrid,npstr,username)
{
	//logga("..*..*..*..* SEND NUM TREES STRING");
	socket.emit('send num trees string',usrid,npstr,username);
	return 0;
}

function sendNumRocksString(usrid,npstr,username)
{
	//logga("..*..*..*..* SEND NUM ROCKS STRING");
	socket.emit('send num rocks string',usrid,npstr,username);
	return 0;
}

function sendNumPlantsString(usrid,npstr,username)
{
	//logga("..*..*..*..* SEND NUM PLANTS STRING");
	socket.emit('send num plants string',usrid,npstr,username);
	return 0;
}

function sendNumVisitorsString(usrid,npstr,username)
{
	//logga("..*..*..*..* SEND NUM VISITORS STRING");
	socket.emit('send num visitors string',usrid,npstr,username);
	return 0;
}

function sendHouseInteractablesString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendHouseInteractablesString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send house interactables string',usrid,fstr,username);
	return 0;
}

function sendItemsString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendItemsString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send items string',usrid,fstr,username);
	return 0;
}

function sendDeathDataString(usrid,fstr,username)
{
	//logga("gm:externalfunctions:sendDeathDataString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send deathdata string',usrid,fstr,username);
	return 0;
}

function sendMissionsString(usrid,fstr,username)
{
	//logga("gm:externalfunctions:sendMissionsString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send missions string',usrid,fstr,username);
	return 0;
}

function sendUnlockablesString(usrid,fstr,username)
{
	//console.log("gm:externalfunctions:sendUnlockablesString "+username + " " + usrid);// + " " + fstr);
	socket.emit('send unlockables string',usrid,fstr,username);
	return 0;
}

function getUserFarm(usrid,sock)
{
	//console.log("gm:externalfunctions:getUserFarm "+usrid);
	socket.emit('get user farm',usrid,sock);
	return 0;
}

function leaveRoom(sock)
{
	socket.emit('leave room',sock);
	return 0;
}

function joinRoom(sock)
{
	socket.emit('join room',sock);
	return 0;
}

function storeAllInSocket(fstr,pstr,istr,usrname)
{
	//console.log("gm:externalfunctions:storeAllInSocket "+ usrname);
	socket.emit('store all in socket',fstr,pstr,istr,usrname);
	return 0;
}
function storeAllInSocketSurvival(fstr,pstr,istr,landid)
{
	var compressed_fstr = LZString.compressToBase64(fstr);
	var compressed_pstr = LZString.compressToBase64(pstr);
	var compressed_istr = LZString.compressToBase64(istr);
	//console.log("gm:externalfunctions:storeAllInSocket "+ landid);
	socket.emit('store all in socket',compressed_fstr,compressed_pstr,compressed_istr ,landid);
	return 0;
}
function storeItemsInSocket(fstr)
{
	//console.log("gm:externalfunctions:storeItemsInSocket");
	socket.emit('store items in socket',fstr);
	return 0;
}

function getUserCreationDate(usrigid)
{
    socket.emit('get user creation date', usrigid);
    return 0;
}

//GOING BACK TO MY FARM 1
function updateFarm()
{
	socket.emit('update farm');
	return 0;
}

function sendItemToUserStash(usrid,iid,q,d)
{
	//console.log("gm:externalfunctions:sendItemToUserStash");
	socket.emit('send item to user stash',usrid,iid,q,d);
	return 0;
}

function sendBadge(usrid,challengetype,won,time,gold,a,b,c,d,e,l)
{
	console.log("WILL EMIT SEND BADGE NOW");
	socket.emit("send badge", usrid, challengetype, won, time, gold,a,b,c,d,e,l);
	return 0;
}
/*
function sendBadge2(usrid,challengetype,won,time,gold,a,b,c,d,e,l)
{
	console.log("WILL EMIT SEND BADGE 2 NOW");
	socket.emit("send badge 2", usrid, challengetype, won, time, gold,a,b,c,d,e,l);
	return 0;
}
*/

function insertRecord(usrid,challengetype,d,e,g,p)
{
	socket.emit("insert record", usrid, challengetype, d, e, g, p);
	return 0;
}

function checkIfPlayerCanPlayChallenge(usrid,action)
{
	socket.emit("check if player can play challenge", usrid, action);
	return 0;
}

function visitURL(url)
{
	socket.emit("visit url", url);
	return 0;
}

function getCSD()
{
	socket.emit("getCSD");
	return 0;
}

function getUserHistory(usrid)
{
	console.log("getUserHistory");
	socket.emit("get user history",usrid);
	return 0;
}

function removeMalus(usrid,seconds)
{
	socket.emit("remove malus",usrid,seconds);
	return 0;
}

function getChallengeData(ch)
{
	console.log("getChallengeData");
	socket.emit("get challenge data",ch);
	return 0;
}

///////////////////////////////////////SURV
function sendUserdataSurvivalString(usrid,str)
{
	socket.emit('send userdatasurvival string',usrid,str);
	return 0;
}
///////////////////////////////////////