{
    "id": "23cbb724-20a9-42d6-8d35-c94e6f89748f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_strawberry_plant",
    "eventList": [
        {
            "id": "4ad343c7-1e29-42f4-99bd-6c46bcdf2856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "23cbb724-20a9-42d6-8d35-c94e6f89748f"
        },
        {
            "id": "bd167446-8bf0-4600-8997-a9ffcd2e4db6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "23cbb724-20a9-42d6-8d35-c94e6f89748f"
        },
        {
            "id": "45be7a78-e354-401f-af1b-d974fc24c318",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "23cbb724-20a9-42d6-8d35-c94e6f89748f"
        },
        {
            "id": "ed1564ba-75ab-4761-88d7-e64b0e293b70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "23cbb724-20a9-42d6-8d35-c94e6f89748f"
        },
        {
            "id": "e20fe097-a2bf-4f91-a498-2624183d34b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "23cbb724-20a9-42d6-8d35-c94e6f89748f"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
    "visible": true
}