{
    "id": "81f3d3d7-9ff0-438a-825b-6d9d257af11f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_safe",
    "eventList": [
        {
            "id": "b6b7eb54-e0bf-4e8d-82d5-a3c272059bc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "81f3d3d7-9ff0-438a-825b-6d9d257af11f"
        },
        {
            "id": "684ef28e-aab3-47af-894d-1538d467e212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "81f3d3d7-9ff0-438a-825b-6d9d257af11f"
        },
        {
            "id": "974a931c-203f-4685-94fe-0853288de06b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "81f3d3d7-9ff0-438a-825b-6d9d257af11f"
        },
        {
            "id": "e3d12981-12af-4dea-85c8-16e91415d803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "81f3d3d7-9ff0-438a-825b-6d9d257af11f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fce279aa-51cb-4b8d-8ee3-61fd625a601d",
    "visible": true
}