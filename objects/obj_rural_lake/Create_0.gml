event_inherited();

ds_map_add(grass,"wildgrass",70);
ds_map_add(grass,"hypericum",2);
ds_map_add(grass,"lavender",2);
ds_map_add(grass,"echinacea",2);
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}
// No plants in this submap

ds_map_add(boulders,"rock",1);
ds_map_add(boulders,"rockiron",1);
ds_map_add(boulders,"sapling",4);

ds_map_add(mobs,"bear",30);