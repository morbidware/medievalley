/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"stone",6);
ds_map_add(ingredients,"woodlog",6);
ds_map_add(ingredients,"ironnugget",2);

resitemid = "alchemytable";
unlocked = true;
desc = "A workplace for preparing less conventional drinks...";