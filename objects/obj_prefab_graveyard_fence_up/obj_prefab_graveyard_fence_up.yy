{
    "id": "f4f67e4e-a0e4-4757-b2d3-a8b3f6703869",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_fence_up",
    "eventList": [
        {
            "id": "769ce3f4-675f-4981-bbf4-4fcdfeb39f31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4f67e4e-a0e4-4757-b2d3-a8b3f6703869"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d28a98ab-5a55-41ce-bd9b-c42bda474999",
    "visible": true
}