{
    "id": "57926914-f046-40d8-8fc5-876894cb0468",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipe_pickaxe",
    "eventList": [
        {
            "id": "425a198c-b567-406a-a0e7-f4a2fcc3d24e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "57926914-f046-40d8-8fc5-876894cb0468"
        },
        {
            "id": "782d36bb-09fb-4102-a3bb-1a9f758e5973",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "57926914-f046-40d8-8fc5-876894cb0468"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "93f355f3-d76a-4841-a79d-f9b850cd184e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}