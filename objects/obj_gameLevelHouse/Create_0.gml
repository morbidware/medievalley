/// @description Insert description here
// You can write your code in this editor
event_inherited();

ds_list_clear(global.lockHUDparticipants);

random_set_seed(global.userid);

houseGround = instance_create_depth(0,0,0,obj_houseGround);
player = instance_create_depth(houseGround.exitpointx, houseGround.exitpointy - 24, 0, obj_char);
player.prevx = houseGround.exitpointx;
player.prevy = houseGround.exitpointy;
player.lastdirection = Direction.Up;

instance_create_depth(0,0,0,obj_fadein);

audio_play_sound(choose(bgm_farm_a, bgm_farm_b),0,true);

if(string_length(global.housepickables) > 0)
{	
	
	show_debug_message("Reading saved house pickables");
	logga("Reading saved house pickables");
	logga(global.housepickables);
	var pickables = ds_list_create();
	ds_list_read(pickables, global.housepickables); 
	
	logga("pickables list length " + string(ds_list_size(pickables)));
	
	for(var i = 0; i < ds_list_size(pickables); i++)
	{
		var str = ds_list_find_value(pickables,i);
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		var qu = str;

		scr_gameItemCreate(real(px),real(py),obj_pickable,iid,real(qu));
	}
}

logga("create house interactables");
if(string_length(global.houseinteractables) > 0)
{
	var interactables = ds_list_create();
	ds_list_read(interactables, global.houseinteractables); 
	show_debug_message("Reading saved house interactables");
	logga("Reading saved house interactables");
	logga("interactables list length " + string(ds_list_size(interactables)));
	for(var i = 0; i < ds_list_size(interactables); i++)
	{
		var str = ds_list_find_value(interactables,i);
		
		//logga("str is " + str);
		//show_debug_message(string(str));
		
		var obj = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,obj+":","");
		
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		//growcrono
		var gc = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gc+":","");
		//growstage
		var gs = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gs+":","");
		//quantity
		var gq = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gq+":","");
		//durability
		var gd = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gd+":","");
		//gridded
		var gg = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gg+":","");
		//life
		var gl = str;

		scr_gameItemCreate(real(px), real(py), scr_asset_get_index(obj), iid,real(gq), real(gc), real(gs), real(gd), real(gg), real(gl));
	}
}

if(!instance_exists(obj_interactable_bed))
{
	scr_gameItemCreate(0+(16*(global.houseborder+1)), 0+(16*(global.houseborder+4)), obj_interactable_bed, "bed");
}
if(!instance_exists(obj_interactable_wardrobe))
{
	scr_gameItemCreate(0+(16*(global.houseborder+1)), 0+(16*(global.houseborder+9)), obj_interactable_wardrobe, "wardrobe");
}
if(!instance_exists(obj_interactable_stool))
{
	scr_gameItemCreate(8+(16*(global.houseborder+4)), 8+(16*(global.houseborder+6)), obj_interactable_stool, "stool");
	scr_gameItemCreate(8+(16*(global.houseborder+9)), 8+(16*(global.houseborder+6)), obj_interactable_stool, "stool");
}
if(!instance_exists(obj_interactable_table))
{
	scr_gameItemCreate(0+(16*(global.houseborder+7)), 8+(16*(global.houseborder+6)), obj_interactable_table, "table");
}
if(!instance_exists(obj_interactable_bench))
{
	scr_gameItemCreate(0+(16*(global.houseborder+7)), 8+(16*(global.houseborder+7)), obj_interactable_bench, "bench");
}
if(!instance_exists(obj_interactable_safe))
{
	scr_gameItemCreate(0+(16*(global.houseborder+2)), 8+(16*(global.houseborder+0)), obj_interactable_safe, "safe");
}
scr_storeHouseInteractables();

/////////////////////////////
instance_create_depth(0,0,-1000,obj_ui_black_stripes);
instance_create_depth(player.x,player.y,0,obj_camera);
/////////////////////////////

logga("fine gamelevel");