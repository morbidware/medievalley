{
    "id": "b6cf8727-8727-4c01-ac75-0a94fb6cd96a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevelHouse",
    "eventList": [
        {
            "id": "f57f89a4-d2b6-423c-b7e8-79c1865ea512",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6cf8727-8727-4c01-ac75-0a94fb6cd96a"
        },
        {
            "id": "cc629d12-c040-4c0c-bb49-4354b9bb9c50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6cf8727-8727-4c01-ac75-0a94fb6cd96a"
        },
        {
            "id": "c7c8896e-c370-43de-b54a-a88db95c614e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "b6cf8727-8727-4c01-ac75-0a94fb6cd96a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}