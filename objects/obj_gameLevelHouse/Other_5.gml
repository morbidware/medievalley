/// @description Stop sounds, save last visit
global.lastVisitedPlace = LastVisitedPlace.House;
audio_stop_sound(bgm_farm_a);
audio_stop_sound(bgm_farm_b);
audio_stop_sound(snd_white_noise);