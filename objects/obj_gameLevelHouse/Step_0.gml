/// @description Insert description here
// You can write your code in this editor
event_inherited();
timeline_speed = global.dt;

if(instance_exists(obj_console)){exit;}

//detect when the player is changing room
if (!changingRoom)
{
	with (obj_char)
	{
		// EXIT DOOR
		if (point_distance(x, y, other.houseGround.exitpointx, other.houseGround.exitpointy) < 12)
		{
			other.changingRoom = true;
			event_perform_object(obj_gameLevel, ev_other, ev_user0);
		}
	}
}