{
    "id": "982e0581-456c-4e06-894a-9d2693ed215f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadows_lv1",
    "eventList": [
        {
            "id": "bc2bc6c0-4b2c-4b83-a172-fb8a67c110b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "982e0581-456c-4e06-894a-9d2693ed215f"
        },
        {
            "id": "b47fa055-00ec-49b1-892e-6cd8209f2102",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "982e0581-456c-4e06-894a-9d2693ed215f"
        },
        {
            "id": "f1711061-2cf3-4150-89c6-4b6ddf36e343",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "982e0581-456c-4e06-894a-9d2693ed215f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}