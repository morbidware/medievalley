{
    "id": "eadf1187-78b2-4a27-9ac5-cb46ba09e765",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wildlife_firefly",
    "eventList": [
        {
            "id": "e6330b95-6b3e-42db-86de-147ef02e7fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eadf1187-78b2-4a27-9ac5-cb46ba09e765"
        },
        {
            "id": "270434bf-2689-4123-9b96-8801d3540939",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eadf1187-78b2-4a27-9ac5-cb46ba09e765"
        },
        {
            "id": "2f9b0bea-b8fc-4bab-a9c9-d10a09f38efb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "eadf1187-78b2-4a27-9ac5-cb46ba09e765"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "389843b1-8fa4-42bf-98e7-2828f05bb9b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b93a5d3-97a7-4226-b224-d8c35c9c5d66",
    "visible": true
}