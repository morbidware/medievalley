/// @description Follow Player
// You can write your code in this editor
if(player)
{
	var dist = distance_to_object(player);
	var dir = point_direction(x,y,player.x,player.y);

	if(dist>distToPlayer)
	{
		//x += player.x + distToPlayer;
		//y += player.y + distToPlayer;
		x = lerp(x,player.x, global.dt * 0.7);
		y = lerp(y,player.y, global.dt * 0.7);
	}
}