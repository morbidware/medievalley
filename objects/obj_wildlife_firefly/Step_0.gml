event_inherited();
scr_setDepth(DepthType.Elements);
ang += anglespeed*angledir*global.dt;
rad = cos(ang*3.0)*maxrad;
if(distToPlayer != 0)
{
	player = instance_find(obj_char,0);
	//event_perform(ev_other,ev_user0);
	x = player.x + distToPlayer;
	y = player.y - distToPlayer - 32;
	x = x + cos(ang)*rad;
	y = y - sin(ang)*rad;
}
else
{	
	x = cx + cos(ang)*rad;
	y = cy - sin(ang)*rad;
}
if(state == 0)
{
	if(a < 1.0)
	{
		a += 0.02*global.dt;
	}
	if(distToPlayer == 0)
	{
		image_alpha = 1.0+sin(ang*3.0);
		image_alpha *= a;
	}
	
}
image_angle += sin(ang*3.0);


//show_debug_message("cx "+string(cx)+" cos(ang) "+string(cos(ang))+" rad " + string(rad) + " x " + string(x));