event_inherited();

ang = random(2*pi);
anglespeed = 0.002+random(0.01);
maxrad = 16+random(16);
rad = sin(ang)*maxrad;
angledir = choose(-1.0,1.0);
cx = x+random_range(-16.0,16.0);
cy = y+random_range(-16.0,16.0);

alivefrom = 0.75;
aliveto = 0.95;

a = 0.0;

distToPlayer = 0;
player = noone;