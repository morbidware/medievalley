{
    "id": "fa6778b2-d041-4c88-8401-3eef2ac640b6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipe_grasssuit",
    "eventList": [
        {
            "id": "adadf847-b992-4019-9aad-a12bb7b6e9cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa6778b2-d041-4c88-8401-3eef2ac640b6"
        },
        {
            "id": "21215653-fa3d-4659-8f16-d8359e3fc7e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa6778b2-d041-4c88-8401-3eef2ac640b6"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "93f355f3-d76a-4841-a79d-f9b850cd184e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}