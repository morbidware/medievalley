/// @description Insert description here
// You can write your code in this editor
map = spr_marketmap;
cols = sprite_get_width(map);
rows = sprite_get_height(map);

surf = surface_create(cols,rows);
surface_set_target(surf);
draw_set_color(c_white);
draw_rectangle(0,0,cols,rows,false);
draw_sprite(map,0,0,0);
surface_reset_target();

border = 0;

hspx = 0;
hspy = 0;

grid = ds_grid_create(cols,rows);//solo per gli enum
gridaccess = ds_grid_create(cols,rows);//solo per determinare quali tiles sono accessibili e quali no
itemsgrid = ds_grid_create(cols,rows);//per gli item incastrati su gli oggetti default -1
wallsgrid = ds_grid_create(cols,rows);//solo per i walls
limitgrid = ds_grid_create(ceil(cols/2),ceil(rows/2));//solo per il limite mappa, diviso 2 perché è a 32px
storeygrid = ds_grid_create(cols,rows);	// storey value of the single tile
pathgrid = mp_grid_create(0,0,cols,rows,16,16);	// griglia per pathfinding

// init base grids
ds_grid_clear(grid,GroundType.None);
ds_grid_clear(itemsgrid,-1);
ds_grid_clear(storeygrid,0);
ds_grid_clear(gridaccess,-1);
ds_grid_clear(storeygrid,0);
//read map
var r = 0;
var c = 0;
repeat(cols)
{
	r = 0;
	repeat(rows)
	{
		var col = surface_getpixel(surf, c, r);
		
		var blue = color_get_blue(col);
		var green = color_get_green(col);
		var red = color_get_red(col);
		
		// create walls on black
		if(blue == 0 && green == 0 && red == 0)
		{
			ds_grid_set(grid, c, r, GroundType.Grass);
			var w = instance_create_depth(8+c*16, 8+r*16, 0, obj_wall_invisible);
			w.storey_0 = true;
			w.storey_1 = true;
			w.storey_2 = true;
			w.storey_3 = true;
			ds_grid_add(wallsgrid, c, r, w);
		}
		
		// create player on blue
		if(blue == 255 && green == 0 && red == 0)
		{
			hspx = 8+c*16;
			hspy = 8+r*16;
		}
		
		r++;
	}
	c++;
}
scr_setDepth(DepthType.StaticBg);