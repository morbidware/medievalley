{
    "id": "08809f84-e370-4df6-bba3-0b55c913a823",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_marketGround",
    "eventList": [
        {
            "id": "fd78aa35-7e74-4122-89ee-1c626e2bd6b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08809f84-e370-4df6-bba3-0b55c913a823"
        },
        {
            "id": "8a38ce89-6ae7-4aa3-8889-5c98bee7cdaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08809f84-e370-4df6-bba3-0b55c913a823"
        },
        {
            "id": "9f2fe755-3cdb-4ae8-ae06-3872bc547c4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08809f84-e370-4df6-bba3-0b55c913a823"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}