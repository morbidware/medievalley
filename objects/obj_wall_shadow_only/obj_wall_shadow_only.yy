{
    "id": "3088fd65-9e69-4c7f-924e-473939efb7b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_shadow_only",
    "eventList": [
        {
            "id": "0620a4b3-f7f0-4a6f-9016-b0f8ee41ca9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3088fd65-9e69-4c7f-924e-473939efb7b4"
        },
        {
            "id": "a28ecdde-b996-4d57-bd21-88b3a6e837c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3088fd65-9e69-4c7f-924e-473939efb7b4"
        }
    ],
    "maskSpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
    "overriddenProperties": null,
    "parentObjectId": "71187ee3-21dc-4a51-b7ca-a4ebfb966176",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}