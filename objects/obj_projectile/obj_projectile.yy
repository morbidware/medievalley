{
    "id": "a0a911a5-9aae-44d0-ae57-69d15e975e55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_projectile",
    "eventList": [
        {
            "id": "6014ada2-8b1c-4a3c-bf18-05f6d41f9fe5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0a911a5-9aae-44d0-ae57-69d15e975e55"
        },
        {
            "id": "d95b2a12-9ba9-4792-ab0e-b46d4e6ffb24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a0a911a5-9aae-44d0-ae57-69d15e975e55"
        },
        {
            "id": "c1df9882-c7e6-4814-84b4-d6d920610ba3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a0a911a5-9aae-44d0-ae57-69d15e975e55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}