scr_setDepth(DepthType.Elements);
depth -= 8;

//---------------------------------- MOVEMENT
_sin = sin(degtorad(dir));
_cos = cos(degtorad(dir));
x += _cos * velocity * global.dt;
y -= _sin * velocity * global.dt;
velocity = clamp(velocity, 1, velocity + (velocitychange * global.dt));
dist += velocity * global.dt;

if (dist >= maxdistance)
{
	instance_destroy();
}

//---------------------------------- ROTATION
if (rotatesprite)
{
	image_angle = dir;
}
else
{
	image_angle = 0;
}

//---------------------------------- COLLISION

// read storey from grid (clamp needed otherwise it will break when walking outside play area)
storey = ds_grid_get(ground.storeygrid, clamp(floor(x/16),0,ground.cols-1), clamp(floor(y/16),0,ground.rows-1));

// hit player
if (harmplayer)
{
	with (obj_char)
	{
		if (point_distance(x,y,other.x,other.y) < other.radius)
		{
			scr_heroGetDamage(id, other.damage);
			instance_destroy(other.id);
		}
	}
}

// hit solid objects
if (!ghost)
{
	with (obj_wall)
	{
		var ok = false;
		switch (other.storey)
		{
			case 0: if (storey_0) {ok = true;} break;
			case 1: if (storey_1) {ok = true;} break;
			case 2: if (storey_2) {ok = true;} break;
			case 3: if (storey_3) {ok = true;} break;
		}
		
		if (ok && (point_in_rectangle(other.x,other.y,x-8,y-8,x+8,y+8) || point_distance(x,y,other.x,other.y) < 8 + other.radius))
		{
			instance_destroy(other.id);
		}
	}
	with (obj_interactable)
	{
		if (collisionradius > 0 && point_distance(x,y,other.x,other.y) < other.radius)
		{
			instance_destroy(other.id);
		}
	}
}