// projectiles are drawn above the ground, plus a shadow
draw_sprite_ext(sprite_index, image_index, x, y, 1, 1, image_angle, c_black, 0.2);	// shadow
draw_sprite_ext(sprite_index, image_index, x, y-16, 1, 1, image_angle, c_white, 1);		// bullet