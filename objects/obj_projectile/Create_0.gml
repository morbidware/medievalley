/// @description Init

// setup
velocity = 5;				// starting speed
velocitychange = -0.04;		// speed variation at each frame
maxdistance = 200;			// distance before self destroy
homing = false;				// self explanatory
homingaccuracy = 1;			// quickness in turning towards the target
damage = 10;				// self explanatory
radius = 4;					// radius for hitting things
homingtarget = noone;		// current homing target
harmplayer = true;			// can this projectile do damage to the player?
ghost = false;				// can this projectile pass through solid objects?
rotatesprite = true;		// rotate the sprite in the direction of movement?

// utility vars
dir = 0;
dist = 0;
_sin = 0;
_cos = 0;
storey = 0;
ground = instance_find(obj_baseGround,0);

if(global.isChallenge)
{
	damage = ds_map_find_value(global.challengeData,"enemy_damage");
}