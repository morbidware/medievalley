/// @description Custom setup
event_inherited();
/*
inv = ds_map_create();
ds_map_add(inv,"potion_base",99);

ds_map_add(inv,"potion_life",99);
ds_map_add(inv,"potion_stamina",99);
ds_map_add(inv,"potion_defence",99);
ds_map_add(inv,"potion_speed",99);
ds_map_add(inv,"potion_strength",99);

ds_map_add(inv,"scroll_potionlife",99);
ds_map_add(inv,"scroll_potionstamina",99);
ds_map_add(inv,"scroll_potiondefence",99);
ds_map_add(inv,"scroll_potionspeed",99);
ds_map_add(inv,"scroll_potionstrength",99);
*/

inv = ds_grid_create(2,11);
ds_grid_set(inv,0,0,"potion_base");
ds_grid_set(inv,1,0,99);

ds_grid_set(inv,0,1,"potion_life");
ds_grid_set(inv,1,1,99);

ds_grid_set(inv,0,2,"potion_stamina");
ds_grid_set(inv,1,2,99);

ds_grid_set(inv,0,3,"potion_defence");
ds_grid_set(inv,1,3,99);

ds_grid_set(inv,0,4,"potion_speed");
ds_grid_set(inv,1,4,99);

ds_grid_set(inv,0,5,"potion_strength");
ds_grid_set(inv,1,5,99);

ds_grid_set(inv,0,6,"scroll_potionlife");
ds_grid_set(inv,1,6,1);

ds_grid_set(inv,0,7,"scroll_potionstamina");
ds_grid_set(inv,1,7,1);

ds_grid_set(inv,0,8,"scroll_potiondefence");
ds_grid_set(inv,1,8,1);

ds_grid_set(inv,0,9,"scroll_potionspeed");
ds_grid_set(inv,1,9,1);

ds_grid_set(inv,0,10,"scroll_potionstrength");
ds_grid_set(inv,1,10,1);

ignoregroundlimits = true;

