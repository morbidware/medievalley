{
    "id": "fa04ead6-872a-40ef-b635-f50dfc959ab2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_selling_npc_potions",
    "eventList": [
        {
            "id": "74151585-ccea-4f6f-a82b-5bd165d2bb68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "fa04ead6-872a-40ef-b635-f50dfc959ab2"
        },
        {
            "id": "e875e5c3-e3ee-46c6-95ce-5815d13ee17b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa04ead6-872a-40ef-b635-f50dfc959ab2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "96fed520-bd73-4ff8-be08-104b556f20b4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1cea9112-bec0-4cf1-967a-0a0c96f2aaf4",
    "visible": true
}