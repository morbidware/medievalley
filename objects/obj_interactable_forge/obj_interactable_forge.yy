{
    "id": "249b91cc-a3bb-4d35-a40a-c4328b6f143d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_forge",
    "eventList": [
        {
            "id": "9889cc7c-2a29-4dd5-974d-ac0aaecf6997",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "249b91cc-a3bb-4d35-a40a-c4328b6f143d"
        },
        {
            "id": "f106f765-2a9f-400e-8ddb-3dfc208cb39a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "249b91cc-a3bb-4d35-a40a-c4328b6f143d"
        },
        {
            "id": "1a2cb7b7-77db-46d5-b7d0-5a3a42a8f3db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "249b91cc-a3bb-4d35-a40a-c4328b6f143d"
        },
        {
            "id": "c3aeb8a1-ce73-4e36-9cd9-5c7bbbb6d590",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "249b91cc-a3bb-4d35-a40a-c4328b6f143d"
        },
        {
            "id": "37fec78c-fbdb-4d5a-8886-c9a97ce41ac5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "249b91cc-a3bb-4d35-a40a-c4328b6f143d"
        },
        {
            "id": "15f83bda-2def-401b-9ec9-541888d670fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "249b91cc-a3bb-4d35-a40a-c4328b6f143d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1e52d73-d000-46a7-874c-305a38e1ee7b",
    "visible": true
}