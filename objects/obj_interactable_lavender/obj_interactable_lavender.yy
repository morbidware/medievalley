{
    "id": "91c7c980-8bdf-4a62-a407-72145a7dafc2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_lavender",
    "eventList": [
        {
            "id": "00faf9ea-440a-40b6-9278-2ed2e8218d79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "91c7c980-8bdf-4a62-a407-72145a7dafc2"
        },
        {
            "id": "978dc772-740d-435c-a773-c4eae101142a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "91c7c980-8bdf-4a62-a407-72145a7dafc2"
        },
        {
            "id": "22de5b17-9788-436d-aa12-bb6cc9286f48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "91c7c980-8bdf-4a62-a407-72145a7dafc2"
        },
        {
            "id": "d0b36bce-6bd5-4a68-bd60-baca62978053",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "91c7c980-8bdf-4a62-a407-72145a7dafc2"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2596d29-4683-40ba-8ac4-98c5f6741c76",
    "visible": true
}