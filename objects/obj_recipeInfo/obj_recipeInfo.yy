{
    "id": "8584b8a5-262c-4e8a-8549-6ada0afa1470",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipeInfo",
    "eventList": [
        {
            "id": "98a5d237-b55e-4f93-a27c-c61d90ac8674",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8584b8a5-262c-4e8a-8549-6ada0afa1470"
        },
        {
            "id": "353e2333-3e6e-4922-8770-c06331319381",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8584b8a5-262c-4e8a-8549-6ada0afa1470"
        },
        {
            "id": "6c7194f2-99fa-42dd-8234-b79c1dc2fcc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8584b8a5-262c-4e8a-8549-6ada0afa1470"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}