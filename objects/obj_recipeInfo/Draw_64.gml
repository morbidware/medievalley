// draw only if recipe exists
if(recipe != noone && list.listx >= sprite_get_width(list.sprite_index)-4)
{
	// setup
	var oy = 72;
	var startx = -16;
	
	// background
	draw_set_alpha(image_alpha);
	draw_sprite(spr_recipeinfo_bg,0,x,y);
	draw_sprite(spr_recipeinfo_pointer,0,x+7,y+h/2);
	
	// draw separators
	if(string_pos("\n",name)>0)
	{
		draw_sprite(spr_recipeinfo_bg_separator,0, x+w/2, y+62);
		oy = 88;
	}
	else
	{
		draw_sprite(spr_recipeinfo_bg_separator,0, x+w/2, y+46);
	}
	draw_sprite(spr_recipeinfo_bg_separator,0, x+w/2, y+oy+32);

	// setup fonts
	draw_set_font(global.FontTitles);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_color($031F32);
	
	// title with shadow
	draw_text(x+w/2,y+22,name);	
	draw_set_color($074797);
	draw_text(x+w/2,y+20,name);	
	draw_set_color(c_white);
	
	// ingredients alignment
	if(ds_map_size(recipe.ingredients) == 1)
	{
		startx = 0;
	}
	else if(ds_map_size(recipe.ingredients) == 2)
	{
		startx = -24;
	}
	if(ds_map_size(recipe.ingredients) == 3)
	{
		startx = -48;
	}
	
	// draw ingredients with quantity text
	var key = ds_map_find_first(recipe.ingredients);
	for(var i = 0; i < ds_map_size(recipe.ingredients); i++)
	{
		// ingredient
		var ingredient = key;
		var quantity = ds_map_find_value(recipe.ingredients,key);
		var ox = x + (w/2) + startx + (48*i);
	
		var tot = 0;
		for(var j = 0; j < global.inv_backpacklimit; j++)
		{
			var itm = ds_grid_get(inv.inventory,j,0);
			if (itm > 0)
			{
				if(itm.itemid == ingredient)
				{
					tot += itm.quantity;
				}
			}
		}
		if(ingredient == "gold")
		{
			tot = global.gold;
			scr_draw_sprite_enclosed(spr_pickable_gold,0,ox,y+oy-2,32,1);
		}
		else
		{
			scr_draw_sprite_enclosed(scr_asset_get_index("spr_pickable_"+scr_getGameItemValue(ingredient, "spritename")),0,ox,y+oy-2,32,1);
		}
		
		// quantity
		draw_set_font(global.FontStandardOutline);
		if(tot < quantity)
		{
			draw_set_color(c_red);
		}
		else
		{
			draw_set_color(c_white);
		}
		draw_text(ox,y+oy+18,string(tot)+"/"+string(quantity));
	
		key = ds_map_find_next(recipe.ingredients, key);
	}
	
	// draw desctiption
	draw_set_color(c_white);
	draw_set_valign(fa_top);
	draw_set_font(global.FontStandard);
	draw_set_color($031F32);
	scr_draw_text_ext(x+4+(w/2), y+oy+40, desc, 12, w-42);
	draw_set_alpha(1.0);
}