{
    "id": "adca2dae-05ca-41c0-9d5f-c272cd6254e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_craft_btn_up",
    "eventList": [
        {
            "id": "50ca1ffc-aa01-4c95-b65c-d03c05dd3805",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "adca2dae-05ca-41c0-9d5f-c272cd6254e1"
        },
        {
            "id": "66ca2880-0ef1-4444-be5e-f93bdc1b0640",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "adca2dae-05ca-41c0-9d5f-c272cd6254e1"
        },
        {
            "id": "32fef336-caf6-45d4-94ff-04740cc60cb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "adca2dae-05ca-41c0-9d5f-c272cd6254e1"
        },
        {
            "id": "dcd99185-44f9-4e42-a045-e3ddbeeac43b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "adca2dae-05ca-41c0-9d5f-c272cd6254e1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87b95281-c1cf-488c-a7fa-94432e3ec08f",
    "visible": false
}