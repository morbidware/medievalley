{
    "id": "2c3ebb9f-7fda-4c19-9528-1802d286d644",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_rockiron",
    "eventList": [
        {
            "id": "3e5e7ba2-1daf-4bee-907e-5cf56478a3b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        },
        {
            "id": "45de0f94-8141-466d-a7c7-a5fa19bb0be6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        },
        {
            "id": "620b756c-c767-4dff-9526-953a90bc0d35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        },
        {
            "id": "8c8cc988-bd7a-4950-bf61-db86f520a72d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        },
        {
            "id": "9f82b5cf-ef8e-4a64-accb-1dd843e31dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        },
        {
            "id": "b19cfea5-1674-40a8-a25c-1feea23c9f7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        },
        {
            "id": "7ed3f3eb-be34-4ee5-a09f-eb58ea1ac964",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "2c3ebb9f-7fda-4c19-9528-1802d286d644"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26288e2c-607e-461d-ba58-206b72881e2f",
    "visible": true
}