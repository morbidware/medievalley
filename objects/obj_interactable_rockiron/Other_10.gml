/// @description finish interaction
// You can write your code in this editor
event_perform(ev_other,ev_user4);

//GENERATE GOLD
if(!isNet)
{
	scr_getGoldOnline(ActorState.Mine);
}

isNet = false;

with (obj_baseGround)
{
	ds_grid_set(itemsgrid,floor(other.x/16),floor(other.y/16),-1);
}
instance_destroy();

scr_progressMission("boulder");
scr_progressMission("rockmetal");