/// @description get mined
if(!global.ismobile)
{
	instance_create_depth(x,y,depth,obj_fx_particle_rock);
}
life--;
if (m_godMode)
{
	if (life > floor(lifeperstage / 2))
	{
		life = floor(lifeperstage / 2)
	}
	else
	{
		life = 0;
	}
}

var s = audio_play_sound(snd_mine,0,false);
audio_sound_pitch(s, 0.85 + random (0.35));

//when breaking the first stage, drop some material
if (life == floor(lifeperstage / 2))
{
	event_perform(ev_other,ev_user4);
}
if (life <= 0)
{
	event_perform(ev_other,ev_user0); 
}