if (!enabled || global.lockHUD)
{
	exit;
}

//fade in
if (idletimer > 0)
{
	if (fadetimer < 1)
	{
		fadetimer += 0.02 * global.dt;
		currenttext = ds_list_find_value(texts,0);
		currentimage = ds_list_find_value(images,0);
	}
	else
	{
		currenttext = ds_list_find_value(texts,pageindex);
		currentimage = ds_list_find_value(images,pageindex);
		
		idletimer -= global.dt;
		if (idletimer < 1 && pageindex < ds_list_size(texts)-1)
		{
			idletimer = hintduration;
			pageindex++;
		}
	}
}

//fade out
else
{
	if (fadetimer > 0)
	{
		fadetimer -= 0.02 * global.dt;
	}
	else
	{
		ds_queue_dequeue(global.hintqueue);
		instance_destroy();
	}
}

posy = display_get_gui_height() - 60 - (fadetimer*40);
alpha = fadetimer;

// if idletimer is high, shake window
if (idletimer > (hintduration-10) && alpha >= 1)
{
	x = choose(-1,1);
	posy += choose(-1,1);
}
else
{
	x = 0;
}
	