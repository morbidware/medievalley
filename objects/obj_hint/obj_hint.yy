{
    "id": "bb2d0e2c-9315-4a6a-b7d1-4cddb0ea4a2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hint",
    "eventList": [
        {
            "id": "f4517464-a7e2-4036-9b11-f2e2a4c063a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bb2d0e2c-9315-4a6a-b7d1-4cddb0ea4a2f"
        },
        {
            "id": "479365ed-4ac0-4f5f-95c0-1ff0762df4b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb2d0e2c-9315-4a6a-b7d1-4cddb0ea4a2f"
        },
        {
            "id": "9ad8e2c7-0fc8-4273-80b9-65f02b45a333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb2d0e2c-9315-4a6a-b7d1-4cddb0ea4a2f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}