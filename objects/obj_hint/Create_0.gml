posy = 0;
alpha = 0;

hintduration = 300;
if(m_godMode)
{
	hintduration = 120;
}
idletimer = hintduration;
fadetimer = 0;

pageindex = 0;

title = "";

texts = ds_list_create();
currenttext = "";

images = ds_list_create();
currentimage = spr_null;

//depth = 2;
depth = 0;

enabled = false;

show_debug_message(">>> Hint created");