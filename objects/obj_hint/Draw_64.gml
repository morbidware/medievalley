draw_set_alpha(alpha);
draw_set_color(c_white);
scr_nine_slice_at(spr_9slice_letter, display_get_gui_width()/2 + x, posy, 450, 48, 2);

// draw filler to indicate overlay duration
draw_set_color(c_black);
draw_set_alpha(alpha * 0.1);
var progress = 1 - idletimer / hintduration;
draw_rectangle(display_get_gui_width()/2 - 217, posy - 16, display_get_gui_width()/2 - 217 + (433 * progress), posy + 15, false);

draw_set_alpha(alpha);
draw_set_font(global.FontStandard);
draw_set_valign(fa_center);
draw_set_halign(fa_middle);


if (currentimage != spr_null && currentimage != noone)
{
	scr_draw_sprite_enclosed(currentimage, 0, display_get_gui_width()/2 + x - 200, posy, 24, alpha);
	draw_text_ext(display_get_gui_width()/2 + x + 8, posy-3, currenttext, 0, 430);
}
else
{
	draw_text_ext(display_get_gui_width()/2 + x, posy-3, currenttext, 0, 430);
}
draw_set_font(global.FontStandardBoldOutline);
draw_set_color(c_white);
draw_set_valign(fa_bottom);
draw_set_halign(fa_left);
draw_text_ext(display_get_gui_width()/2 + x - 216, posy-34, title, 0, 430);

draw_set_alpha(1);