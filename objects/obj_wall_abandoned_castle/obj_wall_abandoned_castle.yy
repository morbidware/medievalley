{
    "id": "6c9bd973-2977-424f-ab20-2b8850e3e11a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_abandoned_castle",
    "eventList": [
        {
            "id": "99fa381f-cd44-4456-a050-02fcc1e902d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c9bd973-2977-424f-ab20-2b8850e3e11a"
        },
        {
            "id": "b163de22-55db-4e8b-b57c-d6123d39eb37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c9bd973-2977-424f-ab20-2b8850e3e11a"
        }
    ],
    "maskSpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
    "overriddenProperties": null,
    "parentObjectId": "71187ee3-21dc-4a51-b7ca-a4ebfb966176",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}