ground = instance_find(obj_baseGround,0);

//Renderers
g0 = instance_create_depth(0,0,0,obj_groundrendererSurvival);
with (g0) 
{ 
	scr_setDepth(DepthType.Lv0);
	
}

g1 = instance_create_depth(0,0,0,obj_groundrendererSurvival);
with (g1) 
{ 
	scr_setDepth(DepthType.Lv1); 
}

g2 = instance_create_depth(0,0,0,obj_groundrendererSurvival);
with (g2) 
{ 
	scr_setDepth(DepthType.Lv2); 	
}

g3 = instance_create_depth(0,0,0,obj_groundrendererSurvival);
with (g3) 
{
	scr_setDepth(DepthType.Lv3); 
}


col = 0;
row = 0;
repeat(global.survivalcellcolumns * 32)
{
	repeat(global.survivalcellrows * 32)
	{
		// check value overshoot
		if (col < 0 || col > ground.cols-1 || row < 0 || row > ground.rows-1)
		{
			row++;
			continue;
		}
		sprite = ds_grid_get(ground.spritegrid,col,row);
		storey = ds_grid_get(ground.storeygrid,col,row); 
		index = ds_grid_get(ground.indexgrid,col,row);
		
		switch (storey)
		{
			case 0:
			
				if((sprite == spr_ground_tallgrass_tiles
					|| sprite == spr_ground_tallgrass_variation
					|| sprite == spr_ground_tallgrass_variation_47))
				{
					if(sprite == spr_ground_tallgrass_tiles 
						&& (index == 34 || index == 28 
						||  index == 38 || index == 36 
						||  index == 20 || index == 2))
					{
						g0.tilespriteTallGrassVar[g0.tallGrassSizeVar] = sprite;
						g0.tileindexTallGrassVar[g0.tallGrassSizeVar] = index;
						g0.tilexTallGrassVar[g0.tallGrassSizeVar] = col;
						g0.tileyTallGrassVar[g0.tallGrassSizeVar] = row;
						g0.tallGrassSizeVar++;
					}
					else
					{
						g0.tilespriteTallGrass[g0.tallGrassSize] = sprite;
						g0.tileindexTallGrass[g0.tallGrassSize] = index;
						g0.tilexTallGrass[g0.tallGrassSize] = col;
						g0.tileyTallGrass[g0.tallGrassSize] = row;
						g0.tallGrassSize++;	
					}
							
				}
				else if(sprite == spr_ground_coast_tiles && index == 47)
				{
					
					g0.tilespriteWater[g0.waterSize] = sprite;
					g0.tileindexWater[g0.waterSize] = index;
					g0.tilexWater[g0.waterSize] = col;
					g0.tileyWater[g0.waterSize] = row;
					g0.waterSize++;
				}
				else if(sprite == spr_ground_plowed_tiles)
				{
					g0.tilespritePlowed[g0.plowedSize] = sprite;
					g0.tileindexPlowed[g0.plowedSize] = index;
					g0.tilexPlowed[g0.plowedSize] = col;
					g0.tileyPlowed[g0.plowedSize] = row;
					g0.plowedSize++;
				}
				else
				{
					g0.tilespriteGrass[g0.grassSize] = sprite;
					g0.tileindexGrass[g0.grassSize] = index;
					g0.tilexGrass[g0.grassSize] = col;
					g0.tileyGrass[g0.grassSize] = row;
					g0.grassSize++;
				}
				
				break;
				
			case 1:
				if(sprite == spr_ground_tallgrass_tiles 
					|| sprite == spr_ground_tallgrass_variation 
					|| sprite == spr_ground_tallgrass_variation_47)
				{
					if(sprite == spr_ground_tallgrass_tiles 
						&& (index == 34 || index == 28 
						||  index == 38 || index == 36 
						||  index == 20 || index == 2))
					{
						g1.tilespriteTallGrassVar[g1.tallGrassSizeVar] = sprite;
						g1.tileindexTallGrassVar[g1.tallGrassSizeVar] = index;
						g1.tilexTallGrassVar[g1.tallGrassSizeVar] = col;
						g1.tileyTallGrassVar[g1.tallGrassSizeVar] = row;
						g1.tallGrassSizeVar++;
					}
					else
					{
						g1.tilespriteTallGrass[g1.tallGrassSize] = sprite;
						g1.tileindexTallGrass[g1.tallGrassSize] = index;
						g1.tilexTallGrass[g1.tallGrassSize] = col;
						g1.tileyTallGrass[g1.tallGrassSize] = row;
						g1.tallGrassSize++;	
					}
					
				}
				else if(sprite == spr_ground_coast_tiles && index == 47)
				{
					g1.tilespriteWater[g1.waterSize] = sprite;
					g1.tileindexWater[g1.waterSize] = index;
					g1.tilexWater[g1.waterSize] = col;
					g1.tileyWater[g1.waterSize] = row;
					g1.waterSize++;
				}
				else if(sprite == spr_ground_cliff_tiles  ||sprite == spr_ground_cliff_variation)
				{

						g1.tilespriteCliff[g1.cliffSize] = sprite;
						g1.tileindexCliff[g1.cliffSize] = index;
						g1.tilexCliff[g1.cliffSize] = col;
						g1.tileyCliff[g1.cliffSize] = row;
						g1.cliffSize++;					
				}
				else
				{
					g1.tilespriteGrass[g1.grassSize] = sprite;
					g1.tileindexGrass[g1.grassSize] = index;
					g1.tilexGrass[g1.grassSize] = col;
					g1.tileyGrass[g1.grassSize] = row;
					g1.grassSize++;
				}
				break;
				
			case 2:
				if((sprite == spr_ground_tallgrass_tiles
					|| sprite == spr_ground_tallgrass_variation
					|| sprite == spr_ground_tallgrass_variation_47)
					/*&& (index != 34 && index != 28 && index != 38 && index != 36 && index != 20 && index != 2)*/)
				{
					if(sprite == spr_ground_tallgrass_tiles 
						&& (index == 34 || index == 28 
						||  index == 38 || index == 36 
						||  index == 20 || index == 2))
					{
						g2.tilespriteTallGrassVar[g2.tallGrassSizeVar] = sprite;
						g2.tileindexTallGrassVar[g2.tallGrassSizeVar] = index;
						g2.tilexTallGrassVar[g2.tallGrassSizeVar] = col;
						g2.tileyTallGrassVar[g2.tallGrassSizeVar] = row;
						g2.tallGrassSizeVar++;
					}
					else
					{
						g2.tilespriteTallGrass[g2.tallGrassSize] = sprite;
						g2.tileindexTallGrass[g2.tallGrassSize] = index;
						g2.tilexTallGrass[g2.tallGrassSize] = col;
						g2.tileyTallGrass[g2.tallGrassSize] = row;
						g2.tallGrassSize++;	
					}	
				}
				else if(sprite == spr_ground_coast_tiles && index == 47)
				{
					
					g2.tilespriteWater[g2.waterSize] = sprite;
					g2.tileindexWater[g2.waterSize] = index;
					g2.tilexWater[g2.waterSize] = col;
					g2.tileyWater[g2.waterSize] = row;
					g2.waterSize++;
				}
				else if(sprite == spr_ground_cliff_tiles ||sprite == spr_ground_cliff_variation)
				{
						g2.tilespriteCliff[g2.cliffSize] = sprite;
						g2.tileindexCliff[g2.cliffSize] = index;
						g2.tilexCliff[g2.cliffSize] = col;
						g2.tileyCliff[g2.cliffSize] = row;
						g2.cliffSize++;
					
				}
				else
				{
					g2.tilespriteGrass[g2.grassSize] = sprite;
					g2.tileindexGrass[g2.grassSize] = index;
					g2.tilexGrass[g2.grassSize] = col;
					g2.tileyGrass[g2.grassSize] = row;
					g2.grassSize++;
				}
				
				break;
				
			case 3:
				if((sprite == spr_ground_tallgrass_tiles
					|| sprite == spr_ground_tallgrass_variation
					|| sprite == spr_ground_tallgrass_variation_47)
					/*&& (index != 34 && index != 28 && index != 38 && index != 36 && index != 20 && index != 2)*/)
				{
					if(sprite == spr_ground_tallgrass_tiles 
						&& (index == 34 || index == 28 
						||  index == 38 || index == 36 
						||  index == 20 || index == 2))
					{
						g3.tilespriteTallGrassVar[g3.tallGrassSizeVar] = sprite;
						g3.tileindexTallGrassVar[g3.tallGrassSizeVar] = index;
						g3.tilexTallGrassVar[g3.tallGrassSizeVar] = col;
						g3.tileyTallGrassVar[g3.tallGrassSizeVar] = row;
						g3.tallGrassSizeVar++;
					}
					else
					{
						g3.tilespriteTallGrass[g3.tallGrassSize] = sprite;
						g3.tileindexTallGrass[g3.tallGrassSize] = index;
						g3.tilexTallGrass[g3.tallGrassSize] = col;
						g3.tileyTallGrass[g3.tallGrassSize] = row;
						g3.tallGrassSize++;	
					}
				}
				else if(sprite == spr_ground_coast_tiles && index == 47)
				{
					g3.tilespriteWater[g3.waterSize] = sprite;
					g3.tileindexWater[g3.waterSize] = index;
					g3.tilexWater[g3.waterSize] = col;
					g3.tileyWater[g3.waterSize] = row;
					g3.waterSize++;
				}
				else if(sprite == spr_ground_cliff_tiles)
				{
					g3.tilespriteCliff[g3.cliffSize] = sprite;
					g3.tileindexCliff[g3.cliffSize] = index;
					g3.tilexCliff[g3.cliffSize] = col;
					g3.tileyCliff[g3.cliffSize] = row;
					g3.cliffSize++;
				}
				else
				{
					g3.tilespriteGrass[g3.grassSize] = sprite;
					g3.tileindexGrass[g3.grassSize] = index;
					g3.tilexGrass[g3.grassSize] = col;
					g3.tileyGrass[g3.grassSize] = row;
					g3.grassSize++;
				}
				
				break;
		}
		// if the current sprite is a cliff, add a grass patch to the previous storey
		if (storey > 0 && (sprite == spr_ground_cliff_tiles || sprite == spr_ground_cliff_variation))
		{
			switch (storey)
			{
				case 1:
					g0.tilespriteGrass[g0.grassSize] = spr_ground_grass_tiles;
					g0.tileindexGrass[g0.grassSize] = 47;
					g0.tilexGrass[g0.grassSize] = col;
					g0.tileyGrass[g0.grassSize] = row;
					g0.grassSize++;
					break;
					
				case 2:
					g1.tilespriteTallGrass[g1.tallGrassSize] = spr_ground_grass_tiles;
					g1.tileindexTallGrass[g1.tallGrassSize] = 47;
					g1.tilexTallGrass[g1.tallGrassSize] = col;
					g1.tileyTallGrass[g1.tallGrassSize] = row;
					g1.tallGrassSize++;
					break;
					
				case 3:
					g2.tilespriteTallGrass[g2.tallGrassSize] = spr_ground_grass_tiles;
					g2.tileindexTallGrass[g2.tallGrassSize] = 47;
					g2.tilexTallGrass[g2.tallGrassSize] = col;
					g2.tileyTallGrass[g2.tallGrassSize] = row;
					g2.tallGrassSize++;
					break;
			}
		}
		
		row++;
	}
	col++;
	row = 0;
}
with (g0) { event_perform(ev_alarm,0); }
with (g1) { event_perform(ev_alarm,60); }
with (g2) { event_perform(ev_alarm,120); }
//with (g3) { event_perform(ev_alarm,180); }


