/// @description Update g0
// You can write your code in this editor
sprite = ds_grid_get(ground.spritegrid,col,row);
storey = ds_grid_get(ground.storeygrid,col,row); 
index = ds_grid_get(ground.indexgrid,col,row);

g0.tilespritePlowed[g0.plowedSize] = sprite;
g0.tileindexPlowed[g0.plowedSize] = index;
g0.tilexPlowed[g0.plowedSize] = col;
g0.tileyPlowed[g0.plowedSize] = row;
g0.plowedSize++;

i = 0;
repeat(g0.plowedSize)
{
	sprite = ds_grid_get(ground.spritegrid,g0.tilexPlowed[i],g0.tileyPlowed[i]);
	index = ds_grid_get(ground.indexgrid,g0.tilexPlowed[i],g0.tileyPlowed[i]);
	g0.tilespritePlowed[i] = sprite;
	g0.tileindexPlowed[i] = index;
	i++
}
with(g0){event_perform(ev_other,ev_user0);}

