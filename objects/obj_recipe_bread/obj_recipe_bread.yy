{
    "id": "c2b49572-ce91-4db3-95a1-09f74f169788",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipe_bread",
    "eventList": [
        {
            "id": "92960bfa-f10d-45c5-b8eb-fd25411329a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c2b49572-ce91-4db3-95a1-09f74f169788"
        },
        {
            "id": "5734fd61-7f32-43ba-bc5f-d804741d8a8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c2b49572-ce91-4db3-95a1-09f74f169788"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "93f355f3-d76a-4841-a79d-f9b850cd184e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}