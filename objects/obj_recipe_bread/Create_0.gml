/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"wheat",4);

resitemid = "food_bread";
unlocked = true;
desc = "The simplest, most important food. Always have some with you.";