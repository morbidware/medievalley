{
    "id": "f6a5a5f9-5d24-4c72-ac9b-2cc2a5736de4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_craft_btn_down",
    "eventList": [
        {
            "id": "1fb32aca-f72f-499b-a9a2-c70f4a69ac61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6a5a5f9-5d24-4c72-ac9b-2cc2a5736de4"
        },
        {
            "id": "2f1eedae-7600-4bc3-a4a9-458b1ee58bd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6a5a5f9-5d24-4c72-ac9b-2cc2a5736de4"
        },
        {
            "id": "4e0a0c5f-1131-4184-bf71-37ada93147c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f6a5a5f9-5d24-4c72-ac9b-2cc2a5736de4"
        },
        {
            "id": "4bcedb21-e0cd-49dd-bda2-7afd441b39d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f6a5a5f9-5d24-4c72-ac9b-2cc2a5736de4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c57ac7be-51cb-453a-ac56-320aa50c15d4",
    "visible": false
}