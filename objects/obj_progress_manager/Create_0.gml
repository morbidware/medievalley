/// @description Init progress system



// default unlockables value
var def = 0;
if (m_unlockAll == true || global.isSurvival)
{
	def = 1;
}

// ---------------- UNLOCKABLES ----------------
logga("OBJ_PROGRESS_MANAGER: creating new unlockables");
global.unlockables = ds_map_create();

// categories
ds_map_add(global.unlockables, "cat_structures", def);
ds_map_add(global.unlockables, "cat_kitchen", def);
ds_map_add(global.unlockables, "cat_forge", def);
ds_map_add(global.unlockables, "cat_grindstone", def);
ds_map_set(global.unlockables, "cat_alchemytable",def);

// tools
ds_map_add(global.unlockables, "rec_craftingtools", def);
ds_map_add(global.unlockables, "rec_farmingtools", def);
ds_map_add(global.unlockables, "rec_buildingtools", def);
ds_map_set(global.unlockables, "rec_spear", def);

// structures
ds_map_add(global.unlockables, "rec_tent", def);
ds_map_add(global.unlockables, "rec_bonfire", def);
ds_map_add(global.unlockables, "rec_house", def);
ds_map_add(global.unlockables, "rec_grindstone", def);
ds_map_add(global.unlockables, "rec_forge", def);
ds_map_add(global.unlockables, "rec_kitchen", def);
ds_map_set(global.unlockables, "rec_cookedmeat",def);
ds_map_set(global.unlockables, "rec_alchemytable",def);

// forge
ds_map_add(global.unlockables, "rec_sword", def);

// potions
ds_map_add(global.unlockables, "rec_potionlife", def);
ds_map_add(global.unlockables, "rec_potionstamina", def);
ds_map_add(global.unlockables, "rec_potionspeed", def);
ds_map_add(global.unlockables, "rec_potiondefence", def);
ds_map_add(global.unlockables, "rec_potion", def);

// map access
ds_map_add(global.unlockables, "access_town", def);
ds_map_add(global.unlockables, "access_wild", def);

// ui
ds_map_add(global.unlockables, "ui_diary", def);
ds_map_add(global.unlockables, "ui_dock", def);
ds_map_add(global.unlockables, "ui_inventory", def);
ds_map_add(global.unlockables, "ui_craft", def);

if(m_unlockUI == true || global.isSurvival == 1)
{
	ds_map_set(global.unlockables, "ui_diary", 1);
	ds_map_set(global.unlockables, "ui_dock", 1);
	ds_map_set(global.unlockables, "ui_inventory", 1);
	ds_map_set(global.unlockables, "ui_craft", 1);
}

// other & special
ds_map_add(global.unlockables, "rec_everything", def);
ds_map_add(global.unlockables, "obj_porcinimushroom", def);

// ---------------------------------------------

if (m_unlockAll == true || global.isSurvival)
{
	instance_destroy(id);
	exit;
}