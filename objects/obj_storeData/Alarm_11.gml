///@description Finish
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 11");
global.uisavestatus.saving = false;

if(global.newUserForChallenge)
{
	global.newUserForChallenge = false;
	with(instance_create_depth(0,0,0,obj_fadeout))
	{
		gotoroom = roomChallenge;
		canSave = true;
	}
}
logga("obj_storeData finished saving");
instance_destroy(id);