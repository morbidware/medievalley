/// @description Save 4/4
//show_debug_message("---------- SAVING ALL GAME DATA (4/4)...");
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 3");

if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}

scr_storeItems();
scr_storeUnlockables();

global.uisavestatus.saving = true;