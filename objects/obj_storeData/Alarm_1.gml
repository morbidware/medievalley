/// @description Save 2/4
//show_debug_message("---------- SAVING ALL GAME DATA (2/4)...");
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 1");

if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}

if(room == roomFarm)
{
	scr_storeInteractables();
}
else if (room == roomHouse)
{
	scr_storeHouseInteractables();
}

global.uisavestatus.saving = true;