///@description Save 1/4
//show_debug_message("---------- SAVING ALL GAME DATA (1/4)...");
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 0");

if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}

if(room == roomFarm)
{
	scr_storeGround();
}
else if (room == roomHouse)
{
	scr_storeHousePickables();
}

global.uisavestatus.saving = true;