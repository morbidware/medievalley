{
    "id": "3316bdc5-482c-43cb-8653-aa548bd68f0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_storeData",
    "eventList": [
        {
            "id": "f91adc29-5af4-4b63-b0be-98d896407c9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3316bdc5-482c-43cb-8653-aa548bd68f0f"
        },
        {
            "id": "5b02946b-593a-4384-bd85-e81b3610a380",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3316bdc5-482c-43cb-8653-aa548bd68f0f"
        },
        {
            "id": "15de88d0-8fc1-47c1-8632-f383e1028098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "3316bdc5-482c-43cb-8653-aa548bd68f0f"
        },
        {
            "id": "a4481a16-9701-4016-b150-41fb271191ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3316bdc5-482c-43cb-8653-aa548bd68f0f"
        },
        {
            "id": "fae0762a-e3f0-4561-b9da-ef4672b5cfc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "3316bdc5-482c-43cb-8653-aa548bd68f0f"
        },
        {
            "id": "3ecbe6a3-17b3-4f55-aaca-cd2c5fcb7062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "3316bdc5-482c-43cb-8653-aa548bd68f0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}