/// @description Save 3/4
//show_debug_message("---------- / GAME DATA (3/4)...");
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 2");

if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}

if(room == roomFarm)
{
	scr_storePickables();
}
scr_storeUserdata();
scr_storeDeathData();
scr_storeMissions();

global.uisavestatus.saving = true;