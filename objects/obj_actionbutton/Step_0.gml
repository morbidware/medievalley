var xview = camera_get_view_x(view_camera[0]);
var yview = camera_get_view_y(view_camera[0]);
var multx = window_get_width() / camera_get_view_width(view_camera[0]);
var multy = window_get_height() / camera_get_view_height(view_camera[0]);	

var clickX = noone;
var	clickY = noone;
if(actionButtonX != scr_adaptive_x(850) || actionButtonY != scr_adaptive_y(380))
{
	actionButtonX = scr_adaptive_x(850);
	actionButtonY = scr_adaptive_y(380);
}
if((device_mouse_x(clickCounter)-xview)*multx>window_get_width()/2)
{
	clickX = (device_mouse_x(clickCounter)-xview)*multx;
	clickY = (device_mouse_y(clickCounter)-yview)*multy;
}

var distToCenter = point_distance(clickX,clickY,actionButtonX,actionButtonY);

with(obj_char)
{
	if(device_mouse_check_button_pressed(other.clickCounter,mb_left) && distToCenter < other.actionSprWidth)
	{
		if(!checkActionButton)
		{
			checkActionButton = true;
			
			
			event_perform(ev_mouse,ev_global_right_press);
			event_perform(ev_mouse,ev_global_left_press);
		}
	}
	if(device_mouse_check_button_released(other.clickCounter,mb_left))
	{
		other.imageState = 0;
		checkActionButton = false;
	}
	if(device_mouse_check_button(other.clickCounter,mb_left) && checkActionButton = true)
	{
		other.imageState = 1;
	}
}
event_perform(ev_draw,ev_gui);