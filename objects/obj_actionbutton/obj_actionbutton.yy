{
    "id": "a289eb77-7ddb-4954-bab3-89a5a7072b1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_actionbutton",
    "eventList": [
        {
            "id": "d03fb8eb-4a60-46c5-a255-0e5cd7c13c0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a289eb77-7ddb-4954-bab3-89a5a7072b1d"
        },
        {
            "id": "a4076a0e-4624-46f2-bff0-f962f979026e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a289eb77-7ddb-4954-bab3-89a5a7072b1d"
        },
        {
            "id": "8eb57aef-448c-423f-a723-dcf6d9d5856e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a289eb77-7ddb-4954-bab3-89a5a7072b1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}