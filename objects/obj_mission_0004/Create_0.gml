///@description Producing food - Harvest basic food.
event_inherited();

missionid = "0004";
title = "Producing food";
shortdesc = "Harvest basic food.";
longdesc = "Sow seeds in the soil using the hoe. Water them at the beginning, then each time they grow. It will take some time!";
silent = true;

ds_map_add(rewards,obj_reward_0004,1);
ds_map_add(required,"redbeans",9);

// This mission is silent and starts together with 0005, so hints are in obj_mission_0005
ds_list_clear(hintTexts);
ds_list_clear(hintImages);

scr_storeMissions();