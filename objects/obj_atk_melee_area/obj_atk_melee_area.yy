{
    "id": "af7f54ec-0e1a-4178-8785-0618a75085d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_atk_melee_area",
    "eventList": [
        {
            "id": "76b361da-cd18-42af-9299-f37bec852d69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "af7f54ec-0e1a-4178-8785-0618a75085d4"
        },
        {
            "id": "d24cff4b-5435-4281-8617-9e335a108f47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "af7f54ec-0e1a-4178-8785-0618a75085d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f507ea47-1c07-4b08-ad08-a6811d9f52e3",
    "visible": false
}