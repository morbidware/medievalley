/// @description Insert description here
// You can write your code in this editor
if(ground != noone)
{
	var storey_col = clamp(floor(x/16),0,ground.cols-1);
	var storey_row = clamp(floor(y/16),0,ground.rows-1);
	logga("storey_col:"+string(storey_col)+" storey_row:"+string(storey_row));
	storey = ds_grid_get(ground.storeygrid, storey_col, storey_row);
	logga("storey:"+string(storey));
}

//depth = scr_setDepth(DepthType.Elements);
with(obj_enemy)
{
	if(state != MobState.Suffer && state != MobState.Die && flashCrono <= 0 && storey == other.storey)
	{
		
		if(place_meeting(x,y,obj_atk_melee_area))
		{
			with(other)
			{
				hasCutSomething = true;
				enemiesCut++;
			}	
			if(global.isChallenge)
			{
				if(other.enemiesCut == 1)
				{
					global.interactions ++;
					logga("[[ global.interactions ++ ]]");
					var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
					p.val = global.interactionsValue;
				}
				else
				{
					global.bonusInteractions ++;
					logga("[[ global.bonusInteractions ++ ]]");
					var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
					p.val = global.bonusInteractionsValue;
				}
				
			}
			var hero = instance_find(obj_char,0);
			scr_mobGetDamage(id, instance_nearest(hero.x,hero.y, obj_equipped));
			pacific = false;
			var s = audio_play_sound(snd_sword_hit,0,0);
			audio_sound_pitch(s, 0.85 + random (0.35));
		}
	}
}
enemiesCut = 0;
with(obj_mob_dummy)
{
	if(state != 5 && state != 6 && attacktimer<=0)
	{
		if(place_meeting(x,y,obj_atk_melee_area))
		{
			with(other)
			{
				hasCutSomething = true;
				enemiesCut++;
			}
			var hero = instance_find(obj_char,0);
			var weapon = instance_nearest(hero.x,hero.y, obj_equipped);
			var dmg = floor(random_range(weapon.mindamage,weapon.maxdamage));
			sendDummyDataToMobOwner(mid,dmg,ownersocket);
			state = 5;
			attacktimer = 20
		}
	}
	
}
enemiesCut = 0;
