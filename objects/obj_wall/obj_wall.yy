{
    "id": "71187ee3-21dc-4a51-b7ca-a4ebfb966176",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall",
    "eventList": [
        {
            "id": "ea4a62ce-3ff2-4d46-90ab-124f8d31936c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71187ee3-21dc-4a51-b7ca-a4ebfb966176"
        },
        {
            "id": "7a9a876d-ec9d-4e6f-8909-4422ba2e3cc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "71187ee3-21dc-4a51-b7ca-a4ebfb966176"
        },
        {
            "id": "14819f06-3024-4832-8a4a-fe465681328e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "71187ee3-21dc-4a51-b7ca-a4ebfb966176"
        },
        {
            "id": "0c3fc115-7345-4f1e-9837-906745f18a07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "71187ee3-21dc-4a51-b7ca-a4ebfb966176"
        }
    ],
    "maskSpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
    "visible": true
}