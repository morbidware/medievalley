/// @description Clean grids
if (ground <= 0)
{
	return;
}
ds_grid_set(ground.wallsgrid, floor(x/16), floor(y/16), -1);
ds_grid_set(ground.gridaccess, floor(x/16), floor(y/16), 1);
mp_grid_clear_cell(ground.pathgrid, floor(x/16), floor(y/16));