hasTop = true;
hasBottom = true;
hasLeft = true;
hasRight = true;

diagonalTopLeft = false;
diagonalTopRight = false;
diagonalBottomLeft = false;
diagonalBottomRight = false;

storey_0 = false;
storey_1 = false;
storey_2 = false;
storey_3 = false;
storey_stairs_in = false;
storey_stairs_out = false;
blocking = true;

// shadow setup
shadowType = ShadowType.Square;
shadowBlobScale = 1;
shadowLength = 18;
offsetx = 0;
offsety = 0;

image_speed = 0;
image_index = choose(1,2);
var r = choose(0,1);
tind = 2+r;
bind = 0+r;

sprtop = spr_wall_woods_top;
sprbot = spr_wall_woods_bottom;

scr_setDepth(DepthType.Elements);

ground = instance_find(obj_baseGround,0);

// set grid data after all initialization is complete
alarm_set(0,2);