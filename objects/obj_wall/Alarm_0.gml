// Remove items on this same tile
with (obj_interactable)
{
	if (!isastructure && !ignoregroundlimits)
	{
		if ((floor(x/16) == floor(other.x/16)) && (floor(y/16) == floor(other.y/16)))
		{
			instance_destroy(id);
		}
	}
}

if (blocking)
{
	ds_grid_set(ground.gridaccess, x/16, y/16, -1);
	mp_grid_add_cell(ground.pathgrid, x/16, y/16);
}