///@description Farm essentials - Craft the farming tools.
event_inherited();

missionid = "0003";
title = "Farm essentials";
shortdesc = "Craft the farming tools.";
longdesc = "Using raw materials, craft a Hoe and a Watering Can.";
silent = false;

ds_map_add(rewards,obj_reward_0003,1);
ds_map_add(required,"hoe",1);
ds_map_add(required,"wateringcan",1);

ds_list_clear(hintTexts);
ds_list_add(hintTexts, "New tools are available in the crafting bar.", "These tools will help you with farming activities.", "Extra tools are also available, you may need them later.");
ds_list_clear(hintImages);
ds_list_add(hintImages, spr_crafting_cat_tools, spr_pickable_hoe, spr_pickable_shovel);

scr_storeMissions();