// Most of this is an extremely simplified version of obj_groundrendering + obj_groundrenderer

// get view position in tile space
c = 0;
r = 0;
col = floor(global.viewx/16) - margin;
row = floor(global.viewy/16) - margin;

var x1 = col;
var y1 = row;
var x2 = col+38;
var y2 = row+25;
ds_grid_set_grid_region(grid,ground.grid,x1,y1,x2,y2,0,0);
ds_grid_set_grid_region(indexgrid,ground.indexgrid,x1,y1,x2,y2,0,0);

var cc = 0;
var rr = 0;

c = col;
repeat (30 + (margin*2))
{
	r = row;
	rr = 0;
	repeat (17 + (margin*2))
	{
		// check value overshoot
		if (c < 0 || c > ground.cols-1 || r < 0 || r > ground.rows-1)
		{
			r++;
			rr++;
			continue;
		}
		
		posx = c*16+8;
		posy = r*16+24;
		type = ds_grid_get(grid,cc,rr);
		if (type == GroundType.Water)
		{
			draw_sprite(spr_ground_uwcoast_tiles,ds_grid_get(indexgrid,cc,rr),posx,posy);
		}
		r++;
		rr++;
	}
	c++;
	cc++;
}