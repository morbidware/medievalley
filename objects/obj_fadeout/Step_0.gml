if (t < 1)
{
	t += 0.025 * global.dt;
	audio_master_gain(1-t);
}
else
{
	audio_master_gain(0);
	if (gotoroom != noone && !instance_exists(obj_storeData))
	{
		instance_destroy();
		room_goto(gotoroom);
	}
}