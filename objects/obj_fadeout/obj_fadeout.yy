{
    "id": "60249bde-3cf0-4928-9df8-aa88e99b9a57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fadeout",
    "eventList": [
        {
            "id": "c7905970-f8e2-4a35-adf5-bc7f80a49388",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60249bde-3cf0-4928-9df8-aa88e99b9a57"
        },
        {
            "id": "e2532abb-709e-4cc6-b852-cc2103086892",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "60249bde-3cf0-4928-9df8-aa88e99b9a57"
        },
        {
            "id": "91cf4d0e-177f-4ade-8e28-b9f296867436",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60249bde-3cf0-4928-9df8-aa88e99b9a57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}