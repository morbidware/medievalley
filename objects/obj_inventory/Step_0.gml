///@description Sprite assign, mouse follow
if(sprite_index == spr_null)
{
	sprite_index = scr_asset_get_index("spr_pickable_"+spritename);
}

// manage object when under the cursor
if (state == 1)
{
	depth = - (y/16) -700;
	x = mouse_get_relative_x();
	y = mouse_get_relative_y();
	
	if(isastructure && !global.mouseOverUI)
	{
		x = floor(mouse_x/16.0);
		y = floor(mouse_y/16.0);
		
		var c = x;
		var r = y;
		
		var cols = wallsw;
		var rows = wallsh;
		
		//show_debug_message("C: "+string(cols)+", R: "+string(rows));
	
		var startx = floor((x*16) - ((cols/2)*16) + 8);
		var starty = floor((y*16) - ((rows/2)*16) + 8);	
		
		canBePlaced = true;
		
		var ground = instance_find(obj_baseGround,0);
		/*
		with(obj_groundtile)
		{
			image_alpha = 1.0;	
		}
		*/
		if(!global.isSurvival){
			for(var i = 0; i < cols; i++)
			{
				for(var j = 0; j < rows; j++)
				{
					//position for wall objects
					var posx = startx+(16*i);
					var posy = starty+(16*j);
			
					//coordinates for grass and wall grid
					var col = floor(posx/16);
					var row = floor(posy/16);
				
					if(itemid != "house" && itemid != "tent" && itemid != "pallet" && itemid != "bonfire" && itemid != "chest")
					{
						if (ds_grid_get(ground.grid,col,row) != GroundType.HouseBasement)
						{
							canBePlaced = false;
						}
					}
				
					if (ds_grid_get(ground.wallsgrid,col,row) > 0)
					{
						canBePlaced = false;
					}
					if (ds_grid_get(ground.itemsgrid,col,row) > 0)
					{
						canBePlaced = false;
					}
					if (ds_grid_get(ground.gridaccess,col,row) < 0)
					{
						canBePlaced = false;
					}
					
					var a = ds_grid_get(ground.wallsgrid,col,row);//0
					var b = ds_grid_get(ground.itemsgrid,col,row);//-1
					var c = ds_grid_get(ground.gridaccess,col,row);//1
				
					show_debug_message("WALLS:"+string(a)+" ITEMS:"+string(b)+" ACCESS:"+string(c));
				
					for (var l = 0; l < instance_number(obj_interactable); l++) 
					{
						var inst = instance_find(obj_interactable, l);
						if (inst.collisionradius == 0)
						{
							continue;
						}
						else if (point_distance(posx,posy,inst.x,inst.y) < inst.collisionradius)
						{
							canBePlaced = false;
							break;
						}
					}
				}			
			}
		}
		x = 16*c;
		y = 16*r;
		if(wallsh mod(2) != 0)
		{
			y += 8;
		}
		if(wallsw mod(2) != 0)
		{
			x += 8;
		}
		
		/*
		if(instance_place(x,y,obj_interactable))
		{
			canBePlaced = false;
		}
		*/
		
		//show_debug_message("CAN BE PLACED? "+string(canBePlaced));	
	}
}
else
{
	depth = -(y/16) -600;
}

var xview = mouse_get_relative_x();
var yview = mouse_get_relative_y();
/*
show_debug_message("xview " + string(xview) + " yview " + string(yview));
show_debug_message("mouse_x " + string(mouse_x) + " mouse_y " + string(mouse_y));
show_debug_message("x " + string(x) + " y " + string(y));
*/
if(point_in_rectangle(xview,yview,x-16,y-16,x+16,y+16))
{
	highlight = true;
}
else
{
	highlight = false;
}