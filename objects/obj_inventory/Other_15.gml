/// @description Blueprinties
// You can write your code in this editor
if(itemid == "blueprint_house")
{
	ds_map_set(global.unlockables,"rec_house",1);
	global.saveuserdatatimer = 0;
}
if(itemid == "blueprint_kitchen")
{
	ds_map_set(global.unlockables,"cat_kitchen",1);
	ds_map_set(global.unlockables,"rec_kitchen",1);
	global.saveuserdatatimer = 0;
}
if(itemid == "blueprint_alchemytable")
{
	ds_map_set(global.unlockables,"cat_alchemytable",1);
	ds_map_set(global.unlockables,"rec_alchemytable",1);
	global.saveuserdatatimer = 0;
}
if(itemid == "blueprint_forge")
{
	ds_map_set(global.unlockables,"cat_forge",1);
	ds_map_set(global.unlockables,"rec_forge",1);
	global.saveuserdatatimer = 0;
}
if(itemid == "blueprint_grindstone")
{
	ds_map_set(global.unlockables,"cat_grindstone",1);
	ds_map_set(global.unlockables,"rec_grindstone",1);
	global.saveuserdatatimer = 0;
}