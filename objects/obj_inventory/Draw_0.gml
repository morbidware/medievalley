/// @description Draw structure
if(state != 1)
{
	return;
}

if(isastructure && !global.mouseOverUI)
{
	shader_set(shaderStructurePlace);
	if(canBePlaced)
	{
		shader_set_uniform_f(shader_get_uniform(shaderStructurePlace,"u_canBePlaced"),1.0);
	}
	else
	{
		shader_set_uniform_f(shader_get_uniform(shaderStructurePlace,"u_canBePlaced"),0.0);
	}
	var spr = scr_asset_get_index("spr_interactable_"+spritename);
	draw_sprite(spr,sprite_get_number(spr)-1,x,y);
	shader_reset();
		
	var c = floor(mouse_x/16.0);
	var r = floor(mouse_y/16.0);
		
	var cols = wallsw;
	var rows = wallsh;
	
	var startx = floor(x - ((cols/2)*16) + 8);
	var starty = floor(y - ((rows/2)*16) + 8);	
		
	for(var i = 0; i < cols; i++)
	{
		for(var j = 0; j < rows; j++)
		{
			//position for wall objects
			var posx = startx+(16*i);
			var posy = starty+(16*j);
		
			//coordinates for grass and wall grid
			var col = floor(posx/16);
			var row = floor(posy/16);
			//draw_set_color(c_red);
			//draw_rectangle(posx-8, posy-8, posx+8, posy+8, true);
			//draw_set_color(c_white);
		}			
	}
}