/// @description Foodies
// You can write your code in this editor
var v = 0;
var stomach = 0;
if(itemid == "food_apple")
{
	v = 2;
	stomach = foodGain;
}
else if(itemid == "food_olive")
{
	v = 1;
	stomach = foodGain;
}
else if(itemid == "food_blueberry")
{
	v = 1;
	stomach = foodGain;
}
else if(itemid == "food_slimecake")
{
	v = 1;
	stomach = foodGain;
}
else if(itemid == "food_bread")
{
	v = 2;
	stomach = foodGain;
}
else if(itemid == "food_meat")
{
	v = 4;
	stomach = foodGain;
}
else if(itemid == "food_cookedmeat")
{
	v = 10;
	stomach = foodGain;
}
else if(itemid == "food_egg")
{
	v = 3;
	stomach = foodGain;
}
else if(itemid == "food_cookedgoop")
{
	v = -5;
	stomach = foodGain;
}
var player = instance_find(obj_char,0);
if (v < 0)
{
	scr_heroGetDamage(player, -v);
}
else
{
	player.life = clamp(player.life + v, 0, player.maxlife);
}
player.stomach = clamp(player.stomach + stomach, 0, player.maxstomach);