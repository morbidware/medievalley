if(destroy || !instance_exists(inv))
{
	exit;
}

draw_set_alpha(1);
draw_set_color(c_white);

if(state == 0)
{
	if(highlight)
	{
		shader_set(shaderHighlight);
	}
	scr_draw_sprite_enclosed(sprite_index, image_index, x, y, 32, 1);
	if(highlight)
	{
		shader_reset();
	}

	//DRAW NAME ON MOUSE OVER AND WHILE SELECTED
	draw_set_color(c_white);
	draw_set_font(global.FontStandardBoldOutline);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	var yoffset = 22;
	if (slot < global.inv_inventorylimit)
	{
		yoffset = 30;
	}
	
	// First check if there are other highlighted inv items; in that case don't write name of selected tool
	othershighlighted = false;
	with (obj_inventory) {
		if (slot < 10 && inv.currentSelectedItemIndex != slot && highlight) {
			other.othershighlighted = true;
		}
	}
	if(!othershighlighted && inv.currentSelectedItemIndex == slot)
	{
		var str = name;
		if(consumable)
		{
			if (string_pos("scroll_",itemid) != 0 || string_pos("blueprint_",itemid) != 0)
			{
				str = "Learn " + name;
				draw_sprite_ext(spr_mouse_right, 0, x - string_width(str)/2 - 12, y-yoffset, 2, 2, 0, c_white, 1);
				with(obj_char)
				{
					consumableobject = scr_getItemFromInventory(other.slot);
					consumabletype = ConsumableType.Learn;
				}
			}
			else if (string_pos("potion_",itemid) != 0 || string_pos("drink_",itemid) != 0)
			{
				str = "Drink " + name;
				draw_sprite_ext(spr_mouse_right, 0, x - string_width(str)/2 - 12, y-yoffset, 2, 2, 0, c_white, 1);
				with(obj_char)
				{
					consumableobject = scr_getItemFromInventory(other.slot);
					consumabletype = ConsumableType.Drink;
				}
			}
			else if (string_pos("food_",itemid) > 0)
			{
				var nearBonfire = false;
				for(var i = 0; i < instance_number(obj_interactable_bonfire); i++)
				{
					var bf = instance_find(obj_interactable_bonfire,i);
					if(bf.highlight && bf.growstage == 2)
					{
						nearBonfire = true;
						break;
					}
				}
				if(nearBonfire && string_pos("cooked",itemid) == 0)
				{
					str = "Cook " + name;
					with(obj_char)
					{
						consumableobject = scr_getItemFromInventory(other.slot);
						consumabletype = ConsumableType.Cook;
					}
				}
				else
				{
					str = "Eat " + name;
					with(obj_char)
					{
						consumableobject = scr_getItemFromInventory(other.slot);
						consumabletype = ConsumableType.Eat;
					}
				}
			
				draw_sprite_ext(spr_mouse_right, 0, x - string_width(str)/2 - 12, y-yoffset, 2, 2, 0, c_white, 1);
			}
		}	
		else
		{
			consumableobject = noone;
			consumabletype = -1;
		}
		draw_text(x,y-yoffset,str);
	}
	if (highlight && inv.currentSelectedItemIndex != slot)
	{
		draw_text(x,y-yoffset,name);
	}
	if(maxquantity > 1)
	{
		//DRAW QUANTITY
		draw_set_font(global.FontNumbersOutline);
		draw_set_halign(fa_right);
		draw_set_valign(fa_middle);
		draw_text(x+14,y+10,string(scr_fix_text(quantity)));
	}
	else if(durability > 0)
	{
		//DRAW DURABILITY
		//outline
		draw_set_color(make_color_rgb(60,40,10));
		draw_rectangle(x-12,y+8,x+12,y+13,false);
		//fill and reset
		var w = (durability/100)*20;
		draw_set_color(make_color_rgb(0,240,0));
		if(w < 15)
		{
			draw_set_color(make_color_rgb(120,240,0));
		}
		if(w < 10)
		{
			draw_set_color(make_color_rgb(240,120,0));
		}
		if(w < 5)
		{
			draw_set_color(make_color_rgb(240,0,0));
		}
		draw_rectangle(x-10,y+10,x-10+w,y+11,false);
		draw_set_color(c_white);
	}
}
else if(state == 1)
{
	if(!(isastructure && !global.mouseOverUI))
	{
		scr_draw_sprite_enclosed(sprite_index, image_index, x, y, 32, 1);
	}
	if(!global.mouseOverInventory)
	{
		draw_set_font(global.FontStandardBoldOutline);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		if(isastructure && canBePlaced)
		{
			draw_text(mouse_get_relative_x(),mouse_get_relative_y(),"Place " + name);
		}
		else
		{
			if(global.otheruserid == 0)
			{
				if(quantity > 1)
				{
					draw_text(mouse_get_relative_x(),mouse_get_relative_y(),"Drop " + name + " x" + string(quantity));
				}
				else
				{
					draw_text(mouse_get_relative_x(),mouse_get_relative_y(),"Drop " + name);
				}
			}
		}
	}
}