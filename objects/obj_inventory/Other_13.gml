/// @description Potionies
// You can write your code in this editor
if(itemid == "potion_life")
{
	instance_create_depth(0,0,0,obj_status_life);
}
else if(itemid == "potion_speed")
{
	instance_create_depth(0,0,0,obj_status_speed);
}
else if(itemid == "potion_defence")
{
	instance_create_depth(0,0,0,obj_status_defence);
}
else if(itemid == "potion_stamina")
{
	instance_create_depth(0,0,0,obj_status_stamina);
}
else if(itemid == "potion_strength")
{
	instance_create_depth(0,0,0,obj_status_strength);
}