/// @description Scrollies
// You can write your code in this editor
if(itemid == "scroll_potionlife")
{
	ds_map_set(global.unlockables,"rec_potionlife",1);
	global.saveuserdatatimer = 0;
}
else if(itemid == "scroll_potionspeed")
{
	ds_map_set(global.unlockables,"rec_potionspeed",1);
	global.saveuserdatatimer = 0;
}
else if(itemid == "scroll_potiondefence")
{
	ds_map_set(global.unlockables,"rec_potiondefence",1);
	global.saveuserdatatimer = 0;
}
else if(itemid == "scroll_potionstamina")
{
	ds_map_set(global.unlockables,"rec_potionstamina",1);
	global.saveuserdatatimer = 0;
}
else if(itemid == "scroll_potionstrength")
{
	ds_map_set(global.unlockables,"rec_potionstrength",1);
	global.saveuserdatatimer = 0;
}