{
    "id": "6e20c13f-fc76-4119-80ee-4d166d43758e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory",
    "eventList": [
        {
            "id": "df12c9b8-9b2a-4643-8008-bb5487c20521",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "fc5510e9-d223-4f69-aba8-a8a87ee45055",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "ed8a6b6d-769d-4e4e-8584-683b84a82207",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "66bdf799-5ca3-4d1e-8ac6-a39e0a6d149b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "525357b4-b89f-436b-bed4-c9189be06f84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "39a8aacc-6775-4220-a6de-ca2758791649",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "a87d1568-9d63-48ec-86ef-d6a47c659681",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "13922bd8-3791-46c7-971e-f944506adaf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "6982626f-6051-473a-9489-5c1416f9a67f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "7c71f8bc-7420-47ab-8e86-caecc109ed64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        },
        {
            "id": "4639c91a-f3e8-4578-8e89-6ee3ae53a9b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6e20c13f-fc76-4119-80ee-4d166d43758e"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "cf8deb36-79b4-4043-940c-ca9d662edda0",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}