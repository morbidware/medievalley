/// @description Insert description here
// You can write your code in this editor

event_inherited();
unlocked = true;
name = "SURVIVAL";
with(instance_create_depth(0,0,-1,obj_recipe_bonfire))
{
	index = 0;
	cat = other.id;
	ds_list_add(other.entries, id);
}
with(instance_create_depth(0,0,-1,obj_recipe_tent))
{
	index = 1;
	cat = other.id;
	ds_list_add(other.entries, id);
}
with(instance_create_depth(0,0,-1,obj_recipe_groundtrap))
{
	index = 2;
	cat = other.id;
	ds_list_add(other.entries, id);
}