{
    "id": "9e0bae3b-782a-4e76-aab0-e3e73a0bb19a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_survival",
    "eventList": [
        {
            "id": "a667e6bd-3a2f-40b1-b58e-d06728c59f46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e0bae3b-782a-4e76-aab0-e3e73a0bb19a"
        },
        {
            "id": "1d4f97e3-f4ea-4b59-b0b6-1cbfe829a6d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e0bae3b-782a-4e76-aab0-e3e73a0bb19a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e34e1a06-d12c-44c0-8f59-5a14a66b5e52",
    "visible": true
}