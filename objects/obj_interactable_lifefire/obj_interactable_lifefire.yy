{
    "id": "8fec1699-dcdd-463d-a43b-2b1d6d305e85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_lifefire",
    "eventList": [
        {
            "id": "6cd3a46a-53ff-485e-92f6-78a5255fcf04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8fec1699-dcdd-463d-a43b-2b1d6d305e85"
        },
        {
            "id": "b5131360-1e0c-479c-a321-db6be0628110",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8fec1699-dcdd-463d-a43b-2b1d6d305e85"
        },
        {
            "id": "7b1c177e-76d6-461a-b5e0-05b54ee851c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8fec1699-dcdd-463d-a43b-2b1d6d305e85"
        },
        {
            "id": "cdd3eb90-2cee-4b4e-8e4e-9c03f910ae4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "8fec1699-dcdd-463d-a43b-2b1d6d305e85"
        },
        {
            "id": "e743946a-8d26-41e6-920e-131960c3ec97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8fec1699-dcdd-463d-a43b-2b1d6d305e85"
        },
        {
            "id": "75404af5-1082-4199-b5d5-2bca71d0c8f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8fec1699-dcdd-463d-a43b-2b1d6d305e85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
    "visible": true
}