/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",5);

resitemid = "wateringcan";
unlocked = true;
desc = "Usable to water crops to make them grow.";