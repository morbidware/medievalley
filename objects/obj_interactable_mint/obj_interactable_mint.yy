{
    "id": "e08b06eb-b04a-4f63-a735-96b23f06ed5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_mint",
    "eventList": [
        {
            "id": "287da3b4-bda9-48d3-a8b6-93acb441a390",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e08b06eb-b04a-4f63-a735-96b23f06ed5c"
        },
        {
            "id": "7ffea286-ed5b-4347-8e80-e84825e9603b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e08b06eb-b04a-4f63-a735-96b23f06ed5c"
        },
        {
            "id": "0fda97b4-8c85-421f-9cdb-5acb9d3db0df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e08b06eb-b04a-4f63-a735-96b23f06ed5c"
        },
        {
            "id": "2ddf423d-6f0a-4af0-b98c-f8b7348cce8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "e08b06eb-b04a-4f63-a735-96b23f06ed5c"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "06a4cddd-4ac3-4e05-be4f-667326bc6e71",
    "visible": true
}