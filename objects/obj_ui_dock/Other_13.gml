/// @description Open diary
if (global.lockHUD || global.game.player.state != ActorState.Idle)
{
	exit;
}

if(!instance_exists(obj_diary_panel))
{
	instance_create_depth(0,0,0,obj_diary_panel);
}