if(instance_number(obj_ui_dock) > 1)
{
	logga("DUPLICATED UI DOCK!");
	instance_destroy();
	exit;
}

x = scr_adaptive_x(960)-88-12;
y = 12;
depth = 0;

hero = instance_find(obj_char,0);

// Backpack
btnBck = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnBck.sprite_index = spr_dock_btn_backpack;
btnBck.caller = id;
btnBck.evt = ev_user1;

// Diary
btnDiary = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnDiary.sprite_index = spr_dock_btn_diary;
btnDiary.caller = id;
btnDiary.evt = ev_user3;

// Home
btnHome = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnHome.sprite_index = spr_dock_btn_home;
btnHome.caller = id;
btnHome.evt = ev_user4;

// Settings
btnSet = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnSet.sprite_index = spr_dock_btn_settings;
btnSet.caller = id;
btnSet.evt = ev_user0;

// Save
btnSave = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnSave.sprite_index = spr_dock_btn_save;
btnSave.caller = id;
btnSave.evt = ev_user2;

// Save
btnEmote = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnEmote.sprite_index = spr_dock_btn_emotes;
btnEmote.caller = id;
btnEmote.evt = ev_user5;
