// keep dock hidden as long as the intro is active
with(obj_gameLevelFarm)
{
	if (onIntro)
	{
		other.x = scr_adaptive_x(1060)-88-12;
	}
}
if (ds_map_find_value(global.unlockables, "ui_dock") < 1 || room == roomChallenge)
{
	other.x = scr_adaptive_x(1060)-88-12;
}
x -= global.dt;
x = clamp(x,scr_adaptive_x(960)-88-12, 99999);

// home button is active only when visiting
if (global.othersocketid == "")
{
	btnHome.disabled = true;
	btnSave.disabled = false;
}
else
{
	btnHome.disabled = false;
	btnSave.disabled = true;
}

if(btnBck.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnBck.x,btnBck.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "BACKPACK (I)";
		
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == btnBck.y)
		{
			instance_destroy(mi);
		}
	}
}

if(btnDiary.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnDiary.x,btnDiary.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "JOURNAL (J)";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == btnDiary.y)
		{
			instance_destroy(mi);
		}
	}
}

if(btnSet.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnSet.x,btnSet.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "SETTINGS (O)";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == btnSet.y)
		{
			instance_destroy(mi);
		}
	}
}

if(btnHome.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnHome.x,btnHome.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "GO HOME";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == btnHome.y)
		{
			instance_destroy(mi);
		}
	}
}

if(btnSave.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnSave.x,btnSave.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "SAVE DATA";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == btnSave.y)
		{
			instance_destroy(mi);
		}
	}
}

if(btnEmote.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnEmote.x,btnEmote.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "EMOTES (E)";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == btnEmote.y)
		{
			instance_destroy(mi);
		}
	}
}