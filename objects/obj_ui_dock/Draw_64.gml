var clockx = x+58;
var clocky = y+66;

// Gold counter
draw_sprite_ext(spr_money_label,0,x+50,y+6,2,2,0,c_white,1);
draw_set_font(global.FontStandardOutline);
draw_set_color(c_white);
draw_set_valign(fa_middle);
draw_set_halign(fa_right);
draw_text(x+90,y+7,string(hero.gold));
draw_set_halign(fa_left);
draw_sprite_ext(spr_dock,0,x,y+24,2,2,0,c_white,1);
draw_sprite_ext(spr_dock_life_stamina_bg,0,x+60,y+106,2,2,0,c_white,1);
draw_sprite_ext(spr_dock_sundial,0,clockx,clocky,2,2,0,c_white,1);

// Sunlight dial cursor
var a = (global.dayprogress) * (pi*2.0) - (pi/2.0);
draw_set_color(c_black);
draw_set_alpha(0.5);
draw_line_width(clockx,clocky,clockx+cos(a)*18,clocky+sin(a)*18,2);
draw_line_width(clockx,clocky,clockx+cos(a)*12,clocky+sin(a)*12,4);
draw_line_width(clockx,clocky,clockx+cos(a)*6,clocky+sin(a)*6,6);
draw_set_alpha(1.0);
draw_set_color(c_white);
draw_sprite_ext(spr_dock_sundial_dial,0,clockx,clocky-2,2,2,0,c_white,1);

// Stamina and life
var h = sprite_get_height(spr_dock_life_bar);
var l = round((h / 100) * hero.life);
draw_sprite_part_ext(spr_dock_life_bar, 0, 0, h-l, 10, l, x+62, y+120+((h-l)*2), 2, 2, c_white, 1);
var s = round((h / 100) * hero.stamina);
draw_sprite_part_ext(spr_dock_stamina_bar, 0, 0, h-s, 10, s, x+78, y+120+((h-s)*2), 2, 2, c_white, 1);

// Align buttons
btnBck.ox = 44;
btnBck.oy = 120;
//
btnDiary.ox = 44;
btnDiary.oy = 144;
//
btnHome.ox = 44;
btnHome.oy = 170;
//
btnSet.ox = 44;
btnSet.oy = 196;
//
btnSave.ox = 44;
btnSave.oy = 222;
//
btnEmote.ox = 44;
btnEmote.oy = 248;