/// @description Open settings
if (global.lockHUD)
{
	exit;
}

if(!instance_exists(obj_settings_panel))
{
	instance_create_depth(0,0,0,obj_settings_panel);
}