/// @description Open backpack
if (global.lockHUD || global.game.player.state != ActorState.Idle)
{
	exit;
}

if(!instance_exists(obj_backpack_panel))
{
	instance_create_depth(0,0,0,obj_backpack_panel);
}