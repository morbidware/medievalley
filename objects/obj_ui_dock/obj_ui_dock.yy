{
    "id": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_dock",
    "eventList": [
        {
            "id": "cb43f7bb-93f1-49d9-8881-d9ba2ddd4f05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "fa85de8e-786c-4bc9-b9a3-609b21ce4f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "dd5fb430-253f-493c-8acb-bba3b5119ad0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "6d9735b2-3906-4c3e-a23c-8b3e5155c569",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "5f75a5d2-b425-47f4-8111-ae4a3eca608f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "e50b557f-e58a-4166-8bc1-37b9731ea22e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "74272b00-db02-406a-b9cd-e31b4787e2dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "498c19d7-ac46-45bb-ae2f-02aaed7b1703",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        },
        {
            "id": "35d0e9e9-9f00-4e1d-b13e-7ba3541c6a2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "8ff2d1ba-14c0-4099-b9e0-1c6af4d8b00b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}