/// @description Open emotes
if (global.lockHUD || global.game.player.state != ActorState.Idle)
{
	exit;
}

if(!instance_exists(obj_ui_emotedrawer))
{
	instance_create_depth(0,0,0,obj_ui_emotedrawer);
}