/// @description Insert description here
// You can write your code in this editor
btnCTA.disabled = true;
if(btnCTA2 != noone)
{
	btnCTA2.disabled = true;
}
btnClose.disabled = true;
if(ctaURL == "https://feudalife.indiegala.com/challenges")
{
	willRedirect = false;
	global.lastChallengeGalasilver = global.challengeGalasilver;
	global.lastFullState = global.isFull;
	if(global.ismobile)
	{
		global.lastFullState = 1;
	}
	audio_stop_all();
	with(all)
	{
		instance_destroy();
	}
	game_restart();
}
else
{
	visitURL(ctaURL);
}