draw_set_color(c_white);
scr_nine_slice_at(spr_9slice_letter, display_get_gui_width()/2, y, width, height, 2);

draw_sprite_ext(spr_medievalley_post_stamp,0,x,y,2,2,0,c_white,0.2);

var startx = display_get_gui_width()/2 - width/2;
var starty = y - height/2;

draw_set_color(c_black);
draw_set_font(global.FontTitles);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
//draw_text(startx + margin, starty + margin, title);

var sep = 14;
var w = width-margin-margin;

draw_text_ext(startx + margin, starty + margin, title, sep+8, w - 24);

var titleh = scr_string_height_ext(title, sep+8, w-24) + 30;

draw_set_font(global.FontStandard);
draw_set_color(c_black);


if(hasCup)
{
	scr_draw_text_ext(startx + margin, starty + margin + titleh + margin, body, sep, w-48);
	draw_sprite(spr_challenge_letter_cup,0,startx + margin + w - 24, starty + height/2.0);
}
else
{
	scr_draw_text_ext(startx + margin, starty + margin + titleh + margin, body, sep, w);
}

var bodyh = scr_string_height_ext(body, sep, w);
var diff = clamp( 125 - bodyh, 15, 999);
bodyh += diff;

draw_text_ext(startx + margin, starty + margin + titleh + margin + bodyh + margin, signature, sep, w);

var signatureh = scr_string_height_ext(signature, sep, w);

height = margin + margin + titleh + margin + bodyh + margin + signatureh + margin;
btnClose.ox = width/2 - 16-8;
btnClose.oy = -height/2 + 16+8;
if(btnCTA != noone)
{
	btnCTA.ox = width/2 - 84;
	btnCTA.oy = height/2 - 32;
}
if(btnCTA2 != noone)
{
	btnCTA2.ox = width/2 - 84-140-4;
	btnCTA2.oy = height/2 - 32;
}
draw_set_color(c_white);

var spr_coin = spr_fx_coin;
if(global.willEarnGalaCreds)
{
	spr_coin = spr_fx_galacredit;
}

if(assignedGold > 0)
{
	draw_sprite_ext(spr_coin,0,startx + width/2.0-12.0, starty + height/2.0,2.0,2.0,0,c_white,1.0);
	draw_set_font(global.FontStandardBoldOutlineX2);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_text(startx + width/2.0,starty + height/2.0,string(assignedGold));
}