/// @description Insert description here
// You can write your code in this editor
if(hold)
{
	exit;
}
// Inherit the parent event
event_inherited();

if(btnCTA == noone && ctaText != "")
{
	btnCTA = instance_create_depth(0,0,-10003,obj_btn_generic);
	btnCTA.caller = id;
	btnCTA.text = ctaText;
	btnCTA.evt = ev_user1;
	btnCTA.depth = depth-1;
	btnCTA.sprite_index = spr_ui_home_btn_start;
}

if(btnCTA2 == noone && ctaText2 != "")
{
	btnCTA2 = instance_create_depth(0,0,-10003,obj_btn_generic);
	btnCTA2.caller = id;
	btnCTA2.text = ctaText2;
	btnCTA2.evt = ev_user2;
	btnCTA2.depth = depth-1;
	btnCTA2.sprite_index = spr_ui_home_btn_start;
}