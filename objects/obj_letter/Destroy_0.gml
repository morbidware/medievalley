event_inherited();
instance_destroy(btnClose);

if(willRedirect)
{
	visitURL(redirectURL);
}