{
    "id": "042c6977-5508-4379-a784-4d3971d7a94f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_letter",
    "eventList": [
        {
            "id": "ed8b4589-93c7-4790-a111-9349066f7aeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        },
        {
            "id": "79480815-9c81-4527-8c3f-b216e572506d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        },
        {
            "id": "39d470ae-dce4-4e86-abac-c64fcecbc8a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        },
        {
            "id": "27b97d9a-321b-4e10-8e16-16b1e5ffd20f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        },
        {
            "id": "451ff95b-bf31-409a-bad8-5be76ed388a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        },
        {
            "id": "0fe37e93-a071-4ff3-ad32-45cc2b7b2582",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        },
        {
            "id": "06fa68f1-ff6f-4a8f-910a-a1ab0c7bab99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "042c6977-5508-4379-a784-4d3971d7a94f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}