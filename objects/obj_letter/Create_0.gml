event_inherited();
depth = -1100;

width = 420; // YEET!!!

title = "Hallo!";
body = "Hi there,\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris fermentum ex dolor, sit amet luctus neque semper a. Proin arcu ex, tincidunt vulputate risus non, sollicitudin fringilla ligula. Pellentesque a mollis lorem, eget fermentum augue. Sed eu mi dignissim, fringilla est non, ullamcorper libero. Aenean vitae nunc ut erat molestie iaculis ut sed leo. Nulla rhoncus nunc nisl, sed pharetra massa rhoncus in. Nulla vitae rhoncus massa. Vestibulum eget elit porta lectus euismod vehicula eget vel tortor. Donec vel nulla eu justo ultricies maximus. Donec tempor accumsan purus vel sagittis. Vivamus tincidunt luctus metus, vitae maximus arcu feugiat ut.";
signature = "My Best,\nMister Bison";

margin = 16;
height = 0;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

btnClose = instance_create_depth(0,0,-10003,obj_btn_generic);
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;
btnClose.sprite_index = spr_btn_close_silver;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}

rrot = random_range(-10,10);

w_width = 420;
w_height = 420;

assignedGold = 0;

hasCup = false;
willRedirect = false;
redirectURL = "";
audio_play_sound(snd_mail_present,0,false);

ctaText = "";
ctaURL = "";
btnCTA = noone;

ctaText2 = "";
ctaURL2 = "";
btnCTA2 = noone;
hold = false;
dontchangeoncallback = false;
