{
    "id": "0e045ede-c2b4-437a-9ef5-2724f60f3174",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_grindstone",
    "eventList": [
        {
            "id": "2865f05a-236f-4761-9d74-e7fb104eced6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e045ede-c2b4-437a-9ef5-2724f60f3174"
        },
        {
            "id": "5a58d7a6-61bf-47bf-a5ea-a11a1cce33c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e045ede-c2b4-437a-9ef5-2724f60f3174"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "764e32e5-abf6-4578-957d-07fa840180e1",
    "visible": true
}