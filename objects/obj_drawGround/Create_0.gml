ground = instance_find(obj_baseGround,0);

// init vars
posx = 0;
posy = 0;

margin = 10;

c = 0;
r = 0;

col = 0;
row = 0;

storey = -1;
sprite = -1;

// create renderers
g0 = instance_create_depth(0,0,0,obj_groundrenderer);
with (g0) { scr_setDepth(DepthType.Lv0); }

g1 = instance_create_depth(0,0,0,obj_groundrenderer);
with (g1) { scr_setDepth(DepthType.Lv1); }

g2 = instance_create_depth(0,0,0,obj_groundrenderer);
with (g2) { scr_setDepth(DepthType.Lv2); }

g3 = instance_create_depth(0,0,0,obj_groundrenderer);
with (g3) { scr_setDepth(DepthType.Lv3); }

spritegrid = ds_grid_create(30 + (margin*2), 17 + (margin*2));
storeygrid = ds_grid_create(30 + (margin*2), 17 + (margin*2));
indexgrid = ds_grid_create(30 + (margin*2), 17 + (margin*2));

// start calculations
alarm_set(0,1);