// get view position in tile space
c = 0;
r = 0;
col = floor(global.viewx/16) - margin;
row = floor(global.viewy/16) - margin;
var x1 = col;
var y1 = row;
var x2 = col+50;
var y2 = row+37;
ds_grid_set_grid_region(spritegrid,ground.spritegrid,x1,y1,x2,y2,0,0);
ds_grid_set_grid_region(storeygrid,ground.storeygrid,x1,y1,x2,y2,0,0);
ds_grid_set_grid_region(indexgrid,ground.indexgrid,x1,y1,x2,y2,0,0);


// reset renderers
g0.size = 0;
g1.size = 0;
g2.size = 0;
g3.size = 0;

c = col;
var cc = 0;
var rr = 0;
repeat (30 + (margin*2))
{
	r = row;
	rr = 0;
	repeat (17 + (margin*2))
	{
		// check value overshoot
		if (c < 0 || c > ground.cols-1 || r < 0 || r > ground.rows-1)
		{
			r++;
			rr++;
			continue;
		}
		
		posx = c*16+8;
		posy = r*16+8;
		sprite = ds_grid_get(spritegrid,cc,rr);
		storey = ds_grid_get(storeygrid,cc,rr); 
		index = ds_grid_get(indexgrid,cc,rr);
		switch (storey)
		{
			case 0:
				g0.tilesprite[g0.size] = sprite;
				g0.tileindex[g0.size] = index;
				g0.tilex[g0.size] = c;
				g0.tiley[g0.size] = r;
				g0.size++;
				break;
				
			case 1:
				g1.tilesprite[g1.size] = sprite;
				g1.tileindex[g1.size] = index;
				g1.tilex[g1.size] = c;
				g1.tiley[g1.size] = r;
				g1.size++;
				break;
				
			case 2:
				g2.tilesprite[g2.size] = sprite;
				g2.tileindex[g2.size] = index;
				g2.tilex[g2.size] = c;
				g2.tiley[g2.size] = r;
				g2.size++;
				break;
				
			case 3:
				g3.tilesprite[g3.size] = sprite;
				g3.tileindex[g3.size] = index;
				g3.tilex[g3.size] = c;
				g3.tiley[g3.size] = r;
				g3.size++;
				break;
		}
		
		// if the current sprite is a cliff, add a grass patch to the previous storey
		if (storey > 0 && (sprite == spr_ground_cliff_tiles || sprite == spr_ground_cliff_variation))
		{
			switch (storey)
			{
				case 1:
					g0.tilesprite[g0.size] = spr_ground_grass_tiles;
					g0.tileindex[g0.size] = 47;
					g0.tilex[g0.size] = c;
					g0.tiley[g0.size] = r;
					g0.size++;
					break;
					
				case 2:
					g1.tilesprite[g1.size] = spr_ground_grass_tiles;
					g1.tileindex[g1.size] = 47;
					g1.tilex[g1.size] = c;
					g1.tiley[g1.size] = r;
					g1.size++;
					break;
					
				case 3:
					g2.tilesprite[g2.size] = spr_ground_grass_tiles;
					g2.tileindex[g2.size] = 47;
					g2.tilex[g2.size] = c;
					g2.tiley[g2.size] = r;
					g2.size++;
					break;
			}
		}
		
		r++;
		rr++;
	}
	c++;
	cc++;
}

alarm_set(0,10);