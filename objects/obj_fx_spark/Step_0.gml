var pi2 = pi*2.0;
angle += 0.02 * global.dt;
if(angle > pi2)
{
	angle -= pi2;
}
x += sin(angle)*0.25;
y -= 0.5 * global.dt;
if(state == 0)
{
	if(image_alpha < 1.0)
	{
		image_alpha += 0.1;
	}
	else
	{
		state = 1;
	}
}
else if(state == 1)
{
	if(image_alpha > 0.0)
	{
		image_alpha -= 0.1;
	}
	else
	{
		instance_destroy();
	}
}