/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"woodlog",25);
ds_map_add(ingredients,"stone",40);
ds_map_add(ingredients,"gold",200);

resitemid = "house";
unlocked = true;
desc = "A full fledged home. Contains usable furniture and a basement where to place working structures.";