{
    "id": "ae8e4c77-f24e-4625-8439-e5daa00dae6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_btn_generic_persistent",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "348fc906-1b4f-4117-819d-d2ff3801aff8",
    "visible": true
}