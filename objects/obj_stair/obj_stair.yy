{
    "id": "cd75e832-f504-4c57-b0eb-20927d52b583",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_stair",
    "eventList": [
        {
            "id": "9ec0a00d-ab85-498c-9105-641a1a8d99dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd75e832-f504-4c57-b0eb-20927d52b583"
        },
        {
            "id": "af0b5d9f-5cb7-4333-86e5-7cb10c8c1df4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd75e832-f504-4c57-b0eb-20927d52b583"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0007196-f2fc-4c64-8264-47a51f0752dd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}