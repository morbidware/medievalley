///@descriptions Set chars on stairs
with (obj_char)
{
	onstairs = false;
	if (collision_point(x,y,obj_stair,false,true))
	{
		onstairs = true;
		break;
	}
	else if (collision_point(x,y-16,obj_stair,false,true))
	{
		onstairs = true;
		break;
	}
	else if (collision_point(x,y+16,obj_stair,false,true))
	{
		onstairs = true;
		break;
	}
}
with (obj_enemy)
{
	onstairs = false;
	if (collision_point(x,y,obj_stair,false,true))
	{
		onstairs = true;
		break;
	}
	else if (collision_point(x,y-16,obj_stair,false,true))
	{
		onstairs = true;
		break;
	}
	else if (collision_point(x,y+16,obj_stair,false,true))
	{
		onstairs = true;
		break;
	}
}