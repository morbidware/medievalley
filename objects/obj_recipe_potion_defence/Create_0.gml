/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"potion_base",1);
ds_map_add(ingredients,"lavender",5);
resitemid = "potion_defence";
unlocked = true;
desc = "This potion will increase your ability to sustain damage for a short period of time.";