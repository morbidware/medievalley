{
    "id": "beafa6fc-a9f9-4a83-ad5a-0ec11d1c853b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_hypericum",
    "eventList": [
        {
            "id": "add8e6ce-c7b2-4b1e-8049-3f874015ee33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "beafa6fc-a9f9-4a83-ad5a-0ec11d1c853b"
        },
        {
            "id": "f5d7faae-f56f-4566-9ade-ea63162cbdea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "beafa6fc-a9f9-4a83-ad5a-0ec11d1c853b"
        },
        {
            "id": "58f5be2e-cc99-4405-bd96-fe7d95c3b7cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "beafa6fc-a9f9-4a83-ad5a-0ec11d1c853b"
        },
        {
            "id": "6bef10e5-885b-417f-ba70-6d48a47cc2b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "beafa6fc-a9f9-4a83-ad5a-0ec11d1c853b"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a24afe2-dd1a-4bce-8a94-09322720c6a3",
    "visible": true
}