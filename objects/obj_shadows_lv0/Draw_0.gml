// blurred shadows
if (global.graphicsquality == 0){exit;}
event_perform(ev_other,ev_user0);
if(!global.isSurvival)
{
	with (obj_fx_cast_shadow_cliff)
	{
		if (storey < 1)
		{
			continue;
		}
	
		scr_drawShadow(storey-1);
	}
	with (obj_prefab)
	{
	
		scr_drawShadow(storey);
	}
	with (obj_interactable)
	{
		if(canBeDrawn)
		{
		
			scr_drawShadow(storey);
		}
	}
	with (obj_wall)
	{	
	
		if (visible)
		{
			if (storey_0 == true) scr_drawShadow(0);
			else if (storey_1 == true) scr_drawShadow(1);
			else if (storey_2 == true) scr_drawShadow(2);
			else if (storey_3 == true) scr_drawShadow(3);
		}
	}
	with (obj_char)
	{
		if (!visible)
		{
			continue;
		}
	
		scr_drawShadow(storey);
	}
	with (obj_dummy)
	{
		if (!visible || resting)
		{
			continue;
		}
	
		scr_drawShadow(storey);
	}
	with (obj_wildlife)
	{
		if(castsShadow)
		{
		
			scr_drawShadow(storey);
		}
	}
	with (obj_dummy)
	{
	
		scr_drawShadow(storey);
	}
	with (obj_enemy)
	{
	
		scr_drawShadow(storey);
	}
}
else
{
	with (obj_fx_cast_shadow_cliff)
	{
		if (storey < 1)
		{
			continue;
		}
	
		draw_sprite_ext(spr_shadow_square, 0, x+offsetx-global.viewx, y+offsety-global.viewy, 1.1, global.shadowlength * (shadowLength / 19), global.shadowangle, c_black, 1);
		draw_sprite_ext(spr_shadow_square_top, 0, x+offsetx-global.viewx+(global.shadowsin*shadowLength*global.shadowlength), y+offsety-global.viewy+(global.shadowcos*shadowLength*global.shadowlength), 1, 1, 0, c_black, 1);
	}
	
	with (obj_interactable)
	{
		if(canBeDrawn)
		{
		
			if (sprite_index < 0)
			{
				break;
			}
			draw_sprite_ext(sprite_index, image_index, x+offsetx-global.viewx, y+offsety-global.viewy, 1, global.shadowlength, global.shadowangle, c_black, 1);
		}
	}
	/*with (obj_wall)
	{	
	
		if (visible)
		{
			if (storey_0 == true) scr_drawShadow(0);
			else if (storey_1 == true) scr_drawShadow(1);
			else if (storey_2 == true) scr_drawShadow(2);
			else if (storey_3 == true) scr_drawShadow(3);
		}
	}*/
	with (obj_char)
	{
		if (!visible)
		{
			continue;
		}
		if (sprite_index < 0)
		{
			break;
		}
		draw_sprite_ext(sprite_index, image_index, x+offsetx-global.viewx, y+offsety-global.viewy, 1, global.shadowlength, global.shadowangle, c_black, 1);
	}
	with (obj_dummy)
	{
		if (!visible || resting)
		{
			continue;
		}
		if (sprite_index < 0)
		{
			break;
		}
		draw_sprite_ext(sprite_index, image_index, x+offsetx-global.viewx, y+offsety-global.viewy, 1, global.shadowlength, global.shadowangle, c_black, 1);
	}
	with (obj_wildlife)
	{
		if(castsShadow)
		{
			if (sprite_index < 0)
			{
				break;
			}
			draw_sprite_ext(sprite_index, image_index, x+offsetx-global.viewx, y+offsety-global.viewy, 1, global.shadowlength, global.shadowangle, c_black, 1);
		}
	}
	with (obj_enemy)
	{
	
		draw_sprite_ext(spr_shadow_enemy, 0, x+offsetx-global.viewx, y+offsety-global.viewy, 1, global.shadowlength, global.shadowangle, c_black, 1);
	}
}

if (assigned == true)
{
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
	if (global.enableshadowshader)
	{
		shader_set(shaderFullscreenShadow);
		shader_set_uniform_f(scrx,display_get_width());
		shader_set_uniform_f(scry,display_get_height());
	}
	draw_surface_ext(surf,global.viewx,global.viewy,1,1,0,c_black,global.shadowalpha);
	shader_reset();
	draw_set_alpha(1);
	draw_set_color(c_white);
}
assigned = false;