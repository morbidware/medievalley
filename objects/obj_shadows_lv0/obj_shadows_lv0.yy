{
    "id": "4cfeac2a-ede1-470c-a8b8-d0bcac4bb538",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadows_lv0",
    "eventList": [
        {
            "id": "b0b07b36-e0fd-45e3-bd4c-457a6b05aff7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4cfeac2a-ede1-470c-a8b8-d0bcac4bb538"
        },
        {
            "id": "788e70f7-53d9-4628-ac60-14581413247c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4cfeac2a-ede1-470c-a8b8-d0bcac4bb538"
        },
        {
            "id": "8eeadac3-f83f-47a7-bffa-3681bd52ad9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4cfeac2a-ede1-470c-a8b8-d0bcac4bb538"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}