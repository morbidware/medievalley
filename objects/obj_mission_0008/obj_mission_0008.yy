{
    "id": "f6022c66-ec44-4dd4-8ae6-8f60d7e65a53",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mission_0008",
    "eventList": [
        {
            "id": "2f5196ba-d712-4724-953c-ccadedc7f824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6022c66-ec44-4dd4-8ae6-8f60d7e65a53"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8299d0fd-0188-4419-ba64-b8c0f3b131ce",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}