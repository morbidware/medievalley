///@description Master Chef? - Let's start cooking.
event_inherited();

missionid = "0008";
title = "Master Chef?";
shortdesc = "Let's start cooking.";
longdesc = "Build a Bonfire and start cooking.";
silent = false;

ds_map_add(rewards,obj_reward_0008,1);
ds_map_add(required,"food_cookedmeat",3);

ds_list_clear(hintTexts);
ds_list_add(hintTexts, "Now that you have some raw meat.", "Collect resources to build a Bonfire.", "Use the Bonfire to cook your meat.");
ds_list_clear(hintImages);
ds_list_add(hintImages, spr_pickable_meat, spr_pickable_bonfire, spr_pickable_cookedmeat);

scr_storeMissions();