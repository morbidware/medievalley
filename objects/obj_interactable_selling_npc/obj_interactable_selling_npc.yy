{
    "id": "96fed520-bd73-4ff8-be08-104b556f20b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_selling_npc",
    "eventList": [
        {
            "id": "7511704d-c325-40b8-a31c-0511ba1616df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "96fed520-bd73-4ff8-be08-104b556f20b4"
        },
        {
            "id": "db10858a-b97d-4687-801e-3c691747e1ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "96fed520-bd73-4ff8-be08-104b556f20b4"
        },
        {
            "id": "17d6744f-1c1c-4ad5-be83-532b7ebf399c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "96fed520-bd73-4ff8-be08-104b556f20b4"
        },
        {
            "id": "3c8d23e4-c32e-46de-8019-a6d8f103dbf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "96fed520-bd73-4ff8-be08-104b556f20b4"
        },
        {
            "id": "53a6550a-7d7a-4f46-b368-d9d40e1b66f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "96fed520-bd73-4ff8-be08-104b556f20b4"
        },
        {
            "id": "f70edadd-d61d-4b89-8cd1-9c7e336ffa27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96fed520-bd73-4ff8-be08-104b556f20b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1cea9112-bec0-4cf1-967a-0a0c96f2aaf4",
    "visible": true
}