/// @description Custom setup
event_inherited();
/*
inv = ds_map_create();
ds_map_add(inv,"redbeansseeds",99);
ds_map_add(inv,"tomatoesseeds",99);
ds_map_add(inv,"carrotseeds",99);
ds_map_add(inv,"onionseeds",99);
ds_map_add(inv,"wheatseeds",99);
ds_map_add(inv,"aubergineseeds",99);
ds_map_add(inv,"cauliflowerseeds",99);
ds_map_add(inv,"cornseeds",99);
ds_map_add(inv,"garlicseeds",99);
ds_map_add(inv,"grapeseeds",99);
ds_map_add(inv,"hopseeds",99);
ds_map_add(inv,"potatoseeds",99);
ds_map_add(inv,"pumpkinseeds",99);
ds_map_add(inv,"redcabbageseeds",99);
ds_map_add(inv,"strawberryseeds",99);
ds_map_add(inv,"sugarbeetseeds",99);
ds_map_add(inv,"potion_base",99);
*/

inv = ds_grid_create(2,16);
ds_grid_set(inv,0,0,"redbeansseeds");
ds_grid_set(inv,1,0,99);

ds_grid_set(inv,0,1,"tomatoesseeds");
ds_grid_set(inv,1,1,99);

ds_grid_set(inv,0,2,"carrotseeds");
ds_grid_set(inv,1,2,99);

ds_grid_set(inv,0,3,"onionseeds");
ds_grid_set(inv,1,3,99);

ds_grid_set(inv,0,4,"wheatseeds");
ds_grid_set(inv,1,4,99);

ds_grid_set(inv,0,5,"aubergineseeds");
ds_grid_set(inv,1,5,99);

ds_grid_set(inv,0,6,"cauliflowerseeds");
ds_grid_set(inv,1,6,99);

ds_grid_set(inv,0,7,"cornseeds");
ds_grid_set(inv,1,7,99);

ds_grid_set(inv,0,8,"garlicseeds");
ds_grid_set(inv,1,8,99);

ds_grid_set(inv,0,9,"grapeseeds");
ds_grid_set(inv,1,9,99);

ds_grid_set(inv,0,10,"hopseeds");
ds_grid_set(inv,1,10,99);

ds_grid_set(inv,0,11,"potatoseeds");
ds_grid_set(inv,1,11,99);

ds_grid_set(inv,0,12,"pumpkinseeds");
ds_grid_set(inv,1,12,99);

ds_grid_set(inv,0,13,"redcabbageseeds");
ds_grid_set(inv,1,13,99);

ds_grid_set(inv,0,14,"strawberryseeds");
ds_grid_set(inv,1,14,99);

ds_grid_set(inv,0,15,"sugarbeetseeds");
ds_grid_set(inv,1,15,99);


ignoregroundlimits = true;
canSendNetEvents = false;
