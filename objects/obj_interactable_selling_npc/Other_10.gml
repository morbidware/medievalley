/// @description Finish interaction
var merchantpanel = instance_create_depth(0,0,0,obj_merchant_panel);
merchantpanel.merchantInventory = ds_grid_create(ds_grid_width(inv),ds_grid_height(inv));
ds_grid_copy(merchantpanel.merchantInventory,inv);
