/// @description Insert description here
if (status == 0)
{
	if (alpha < 1)
	{
		alpha += 0.02;
	}
	else
	{
		textalpha += 0.04;
	}
}
else
{
	alpha -= 0.04;
	textalpha -= 0.04;
	if (alpha <= 0)
	{
		with (obj_progress_manager)
		{
			event_perform(ev_other,ev_user0)
		}
		instance_destroy(id);
	}
}
if (timer > 0)
{
	timer--;
}
textalpha = clamp(textalpha,0,1);