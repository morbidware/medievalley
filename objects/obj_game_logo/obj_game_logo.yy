{
    "id": "a06ec4fd-c6b6-47cf-b291-197baf5397e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_logo",
    "eventList": [
        {
            "id": "9d105f45-937b-4d67-a27a-b46a9da79589",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a06ec4fd-c6b6-47cf-b291-197baf5397e6"
        },
        {
            "id": "335584f4-9108-4f20-bdd9-ae5326c9a473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a06ec4fd-c6b6-47cf-b291-197baf5397e6"
        },
        {
            "id": "5ef46c79-b296-4474-926f-be870c22d2d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "a06ec4fd-c6b6-47cf-b291-197baf5397e6"
        },
        {
            "id": "528b76aa-6670-402b-a3be-e8dd2799cad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a06ec4fd-c6b6-47cf-b291-197baf5397e6"
        },
        {
            "id": "ab8167b1-5316-43fd-ac6a-a644057328d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a06ec4fd-c6b6-47cf-b291-197baf5397e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3d04a22-c9fe-4ce3-af48-f17c663bdce8",
    "visible": true
}