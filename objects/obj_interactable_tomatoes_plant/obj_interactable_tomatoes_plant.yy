{
    "id": "539c86ae-ee1f-40db-8385-aded6b9d1957",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_tomatoes_plant",
    "eventList": [
        {
            "id": "19129b4f-609f-4672-8eea-3a04179a13b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "539c86ae-ee1f-40db-8385-aded6b9d1957"
        },
        {
            "id": "204d77b5-784a-4727-8807-ca5cd4f11aec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "539c86ae-ee1f-40db-8385-aded6b9d1957"
        },
        {
            "id": "2362def4-9171-48c6-946f-a1fe7dccaca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "539c86ae-ee1f-40db-8385-aded6b9d1957"
        },
        {
            "id": "104bdf85-4611-4243-97fe-b7ad9ccba689",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "539c86ae-ee1f-40db-8385-aded6b9d1957"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
    "visible": true
}