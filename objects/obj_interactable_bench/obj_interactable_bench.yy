{
    "id": "3abdc4cf-4efa-4001-9d0c-bcd9bc87c23a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_bench",
    "eventList": [
        {
            "id": "46f68874-0d07-4d0e-b3ef-f363680a7caa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "3abdc4cf-4efa-4001-9d0c-bcd9bc87c23a"
        },
        {
            "id": "11bd4e1a-ffc9-4902-b4b2-bf3e8761582d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3abdc4cf-4efa-4001-9d0c-bcd9bc87c23a"
        },
        {
            "id": "ad785ca9-47f5-4400-b2e9-72c26dc41579",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "3abdc4cf-4efa-4001-9d0c-bcd9bc87c23a"
        },
        {
            "id": "29c5fa64-9daa-4d68-8b66-ad55aca3dcc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "3abdc4cf-4efa-4001-9d0c-bcd9bc87c23a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ddf2b2f7-0696-4e11-9ce0-a4e2ec3cd0d3",
    "visible": true
}