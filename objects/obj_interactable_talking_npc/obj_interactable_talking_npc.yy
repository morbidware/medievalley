{
    "id": "eb2a1b56-c126-40fd-8294-b30fbce4a20a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_talking_npc",
    "eventList": [
        {
            "id": "e6a9ab93-4167-44d3-a8ea-2f64f14e0e74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "eb2a1b56-c126-40fd-8294-b30fbce4a20a"
        },
        {
            "id": "c7645196-f94c-4b7a-bf10-90c0b335eefc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "eb2a1b56-c126-40fd-8294-b30fbce4a20a"
        },
        {
            "id": "02fa131f-ce22-4506-8e68-20855396a451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "eb2a1b56-c126-40fd-8294-b30fbce4a20a"
        },
        {
            "id": "65c90e01-2370-4ef8-a8ca-70b9876f64f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "eb2a1b56-c126-40fd-8294-b30fbce4a20a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0cbab8ac-4aec-49b2-9b11-04bf0ba13d8e",
    "visible": true
}