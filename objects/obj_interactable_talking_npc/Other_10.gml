/// @description Finish interaction
var dia = instance_create_depth(0,0,0,obj_dialogue);

var strings = ds_list_create();
ds_list_add(strings,"Wonderful day isn't it?");
ds_list_add(strings,"In the Market you will be able to buy and sell stuff. It's still a bit empty today...");
ds_list_add(strings,"The Wild is quite a dangerous place, but it's full of interesting items too.");
ds_list_add(strings,"Remember to keep an eye on your life and stamina. They won't recover by themselves!");
ds_list_add(strings,"In the Wild you can find all sorts of wild grass and medicines, you can craft potions with those.");
ds_list_add(strings,"All crops can be sold to merchants. Better than letting them rot!");
ds_list_add(strings,"Did you know that you can recover all stamina by sleeping in a tent or in a bed?");
ds_list_add(strings,"If your main weapon breaks, you can still try to hit mobs with other tools.");
ds_list_add(strings,"You can use the shovel to remove grass from the ground. A good way to draw a path.");
ds_list_add(strings,"Pay attention to what you carry in the Wild. If you faint there, you will drop all your stuff!");
ds_list_add(strings,"Plants and trees take some time to grow, but they'll grow while you are out of the Farm.");
ds_list_add(strings,"Crops need water to grow. If the underneath soil is dry, it means it's time to water them.");
ds_list_add(strings,"The Farm is a safe place. Nothing will hurt you while you're there!");
ds_list_add(strings,"The people here deliver letters with pidgeons. If a pidgeon approaches your mailbox, well, you got mail!");
ds_list_add(strings,"King Thistle is an honest man. We owe much to him.");
ds_list_add(strings,"You can use the wardrobe in your house to change your clothes.");
ds_list_add(strings,"If you are far into the wild and are in danger, search for the nearest road sign to go back home.");
ds_list_add(strings,"Drinking the same kind of potion multiple times will extend the duration of the effect!");

dia.str = ds_list_find_value(strings,floor(random(ds_list_size(strings))));
dia.title = "Villager";
ds_list_destroy(strings);