/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"potion_base",1);
ds_map_add(ingredients,"mint",5);
resitemid = "potion_speed";
unlocked = true;
desc = "This potion will make you walk faster for a short period of time.";