{
    "id": "a76d546c-a24e-4ac6-90ce-1d4f18b46c03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_tomb_3",
    "eventList": [
        {
            "id": "e78a700a-4287-474d-a4fe-df4ed4c223a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a76d546c-a24e-4ac6-90ce-1d4f18b46c03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "17e47095-5762-40e9-9808-6884e312e147",
    "visible": true
}