{
    "id": "a52de1de-4ab3-40a4-a337-03792d097632",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_status",
    "eventList": [
        {
            "id": "c39b0156-e377-47a5-8ae3-8856ebeb556d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a52de1de-4ab3-40a4-a337-03792d097632"
        },
        {
            "id": "d5fb7407-59de-435c-a0bc-2dc5cc1f1ba6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a52de1de-4ab3-40a4-a337-03792d097632"
        },
        {
            "id": "88a4cfa4-a07b-4bde-802b-633933328072",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a52de1de-4ab3-40a4-a337-03792d097632"
        },
        {
            "id": "1d663786-21f1-433a-898d-de12e68e7ba4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a52de1de-4ab3-40a4-a337-03792d097632"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}