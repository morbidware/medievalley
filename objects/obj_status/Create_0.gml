timer = 60*60;
crono = timer;
ticks = 12;
index = instance_number(obj_status)-1;

// If an identical status exists, add the effect
if(instance_exists(obj_status))
{
	with(obj_status)
	{
		if(object_index == other.object_index && id != other.id)
		{
			timer += other.timer;
			crono += other.timer;
			ticks += 12;
			//show_debug_message("timer is " + string(timer));
			//show_debug_message("crono is " + string(crono));
			instance_destroy(other);
			exit;
		}
	}
	
}