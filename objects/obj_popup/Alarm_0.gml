/// @description Create buttons
switch(type)
{
	case 1:
		btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
		btnClose.ox = 142;
		btnClose.oy = -72;
		btnClose.caller = id;
		btnClose.evt = ev_user0;
		btnClose.depth = depth - 1;
		btnClose.sprite_index = spr_btn_close_orange;
		if(global.ismobile){
			btnClose.image_xscale = btnClose.image_xscale * 2;
			btnClose.image_yscale = btnClose.image_yscale * 2;
		}
	
		btn = instance_create_depth(0,0,-10001,obj_btn_generic);
		btn.sprite_index = spr_popup_btn;
		btn.ox = 0;
		btn.oy = 60;
		btn.text = btnText;
		btn.caller = id;
		btn.evt = ev_user1;
		btn.depth = depth - 1;
		break;
	
	case 2:
		btnYes = instance_create_depth(0,0,-10001,obj_btn_generic);
		btnYes.sprite_index = spr_popup_btn_yes;
		btnYes.ox = -60;
		btnYes.oy = 60;
		btnYes.text = btnYesText;
		btnYes.caller = id;
		btnYes.evt = ev_user2;
		btnYes.depth = depth - 1;
	
		btnNo = instance_create_depth(0,0,-10001,obj_btn_generic);
		btnNo.sprite_index = spr_popup_btn_no;
		btnNo.ox = 60;
		btnNo.oy = 60;
		btnNo.text = btnNoText;
		btnNo.caller = id;
		btnNo.evt = ev_user3;
		btnNo.depth = depth - 1;
		break;
}