///@description Yes action
if(state == 0)
{
	t = 0;
	state = 1;
	if (caller != noone)
	{
		with (caller && btnYesAction != noone)
		{
			event_perform(ev_other, other.btnYesAction);
		}
	}
}