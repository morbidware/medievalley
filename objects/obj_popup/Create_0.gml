event_inherited();
depth = -15000;
global.onPopup = true;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -800;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

btnClose = noone;

gamelevel = instance_find(obj_gameLevel,0);

title = "LOREM IPSUM";
body = "Lorem Ipsum Dolor Sit Amet, Consequectuer Dolor Sit Amet Est.";

btn = noone;
btnYes = noone;
btnNo = noone;

btnText = "OK";
btnYesText = "YES";
btnNoText = "NO";

caller = noone;
btnYesAction = noone;
btnNoAction = noone;
btnOkAction = noone;

type = 0;

w_width = sprite_get_width(spr_popup_bg);
w_height = sprite_get_height(spr_popup_bg);

alarm_set(0,2);