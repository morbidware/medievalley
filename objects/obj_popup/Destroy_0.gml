event_inherited();

if(btnClose != noone)
{
	instance_destroy(btnClose);
}
if(btn != noone)
{
	instance_destroy(btn);
}
if(btnYes != noone)
{
	instance_destroy(btnYes);
}
if(btnNo != noone)
{
	instance_destroy(btnNo);
}

global.onPopup = false;