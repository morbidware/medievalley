///@description Ok action
if(state == 0)
{
	t = 0;
	state = 1;
	if (caller != noone)
	{
		with (caller && btnOkAction != noone)
		{
			event_perform(ev_other, other.btnOkAction);
		}
	}
}