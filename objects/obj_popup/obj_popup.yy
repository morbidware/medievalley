{
    "id": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_popup",
    "eventList": [
        {
            "id": "aafeed71-3285-444b-bc56-a3d7a067642b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "f492a98f-7408-43d0-b84f-8382a6e7d241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "bb1b2a2a-3ab8-496b-a87c-0726e12f7a12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "51a40a24-b58a-4289-a941-8ed18f2dede1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "68399c10-0879-434f-8313-c4483d04c5ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "be7a6afc-3558-4fcb-9554-1c54dfc242e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "896bab23-5d1e-4d1e-98f6-cb1fbe4be855",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "9699f702-e8bb-4dd8-b266-cc4d52f68502",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        },
        {
            "id": "38a29012-59b3-4555-ad3a-b4f7fdb3f5ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}