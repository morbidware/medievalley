event_inherited();

// Remove highlighted object to avoid interactions while this menu is open
with (obj_char)
{
	highlightedobject = noone;	
}