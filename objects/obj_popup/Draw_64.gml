draw_set_color(c_white);
draw_set_alpha(1.0);

draw_sprite_ext(spr_popup_bg, 0, display_get_gui_width()/2, y, 2, 2, 0, c_white, 1);

draw_set_font(global.FontStandardBoldOutline);
draw_set_halign(fa_center);
draw_set_valign(fa_top);
scr_draw_text_ext(x,y-76,title,10,290);

draw_set_color(c_black);
draw_set_font(global.FontStandard);
draw_set_valign(fa_middle);
scr_draw_text_ext(x,y-12,body,10,290);
draw_set_color(c_white);