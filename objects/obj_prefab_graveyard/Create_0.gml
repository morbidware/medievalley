/// @description Init
event_inherited();

// Generate child prefabs
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_base)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_fence_shadow)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_fence_up)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_fence_down)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_pit)) { parent = other.id; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_1)) { parent = other.id; relative_x = -40 relative_y = -12; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_1)) { parent = other.id; relative_x = 72; relative_y = 98; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_2)) { parent = other.id; relative_x = -75; relative_y = -93; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_2)) { parent = other.id; relative_x = 84; relative_y = 19; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_2)) { parent = other.id; relative_x = -91; relative_y = 84; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_3)) { parent = other.id; relative_x = -33; relative_y = -61; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_3)) { parent = other.id; relative_x = 47; relative_y = -92; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_3)) { parent = other.id; relative_x = 48; relative_y = 20; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_3)) { parent = other.id; relative_x = -32; relative_y = 83; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_4)) { parent = other.id; relative_x = -89; relative_y = -45; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_4)) { parent = other.id; relative_x = 71; relative_y = -76; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_4)) { parent = other.id; relative_x = 39; relative_y = 67; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_5)) { parent = other.id; relative_x = -87; relative_y = 16; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_5)) { parent = other.id; relative_x = 57; relative_y = -32; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_6)) { parent = other.id; relative_x = 5; relative_y = -98; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_6)) { parent = other.id; relative_x = -42; relative_y = 30; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tomb_7)) { parent = other.id; relative_x = 8; relative_y = -42; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_1)) { parent = other.id; relative_x = 38; relative_y = -68; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_1)) { parent = other.id; relative_x = 94; relative_y = -98; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_1)) { parent = other.id; relative_x = -89; relative_y = -15; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_1)) { parent = other.id; relative_x = -24; relative_y = 48; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_1)) { parent = other.id; relative_x = 88; relative_y = 44; }

with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_2)) { parent = other.id; relative_x = -40; relative_y = -88; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_2)) { parent = other.id; relative_x = 86; relative_y = -34; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_2)) { parent = other.id; relative_x = -90; relative_y = 43; }
with (instance_create_depth(x,y,depth,obj_prefab_graveyard_tree_2)) { parent = other.id; relative_x = 105; relative_y = 108; }
