{
    "id": "f29ee3a0-4b27-42ab-aee2-8173ceb905fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_garlic_plant",
    "eventList": [
        {
            "id": "de9601a3-a21f-4c5e-a12e-1865df8f805b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "f29ee3a0-4b27-42ab-aee2-8173ceb905fa"
        },
        {
            "id": "77d1acaa-df6a-47e1-bf46-8ae818e6e939",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "f29ee3a0-4b27-42ab-aee2-8173ceb905fa"
        },
        {
            "id": "ba8505b4-58c4-482c-b759-4eceda3c620c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "f29ee3a0-4b27-42ab-aee2-8173ceb905fa"
        },
        {
            "id": "89d567c3-327c-4af2-a114-dadde64e862c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "f29ee3a0-4b27-42ab-aee2-8173ceb905fa"
        },
        {
            "id": "e3f5d0cc-70e8-4f9c-a114-3bd870257b02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "f29ee3a0-4b27-42ab-aee2-8173ceb905fa"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
    "visible": true
}