if (owner != noone)
{
	draw_sprite_ext(sprite_index, 0, x, y, 1, 1, 0, c_white, alpha);
	if (emotesprite != noone)
	{
		draw_sprite_ext(emotesprite, 0, x, y-12, 1, 1, 0, c_white, alpha);
	}
}