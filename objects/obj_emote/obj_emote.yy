{
    "id": "34ac259a-3acb-4e13-bb9b-1c88addc862f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_emote",
    "eventList": [
        {
            "id": "0823c5e7-3274-40cd-8dd6-027312a19f55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34ac259a-3acb-4e13-bb9b-1c88addc862f"
        },
        {
            "id": "0e54de71-2432-4748-925f-95caf1b46034",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "34ac259a-3acb-4e13-bb9b-1c88addc862f"
        },
        {
            "id": "a8bc7f2d-a52e-483f-b088-7d42bea85f1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34ac259a-3acb-4e13-bb9b-1c88addc862f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f07f0fcc-c48b-418e-8c13-2cc866b2f9c0",
    "visible": true
}