if (owner != noone)
{
	x = floor(owner.x);
	y = floor(owner.y - 34 - movey);
	movey += spd * global.dt;
	spd *= 0.95;
	depth = owner.depth - 34;
}

if (lifetimer > 0)
{
	alpha += 0.2;
	lifetimer -= global.dt;
}
else
{
	alpha -= 0.1;
	if (alpha < 0)
	{
		instance_destroy(id);
	}
}
alpha = clamp(alpha,0,1);