event_inherited();

ds_map_add(grass,"wildgrass",80);
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}

ds_map_add(plants,"beechtree",40);
ds_map_add(plants,"olivetree",5);
ds_map_add(plants,"lavender",3);

// No boulders in this submap

ds_map_add(mobs,"wolf",40);

landmark = obj_prefab_witch_lair;
wall = obj_wall_witch_lair;

keeporientation = true;