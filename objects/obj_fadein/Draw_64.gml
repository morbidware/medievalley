posx = display_get_gui_width() / 2;
posy = (display_get_gui_height() / 2) + 900 - (t*1300);

draw_sprite_ext(spr_screen_fade, 0, posx, posy, 30, 20+(t*10), t * 20, c_white, 1);

draw_set_halign(fa_center);
draw_set_valign(fa_bottom);
draw_set_color(c_white);
draw_set_alpha(t * 0.8);
draw_set_font(global.FontStandardBoldOutlineX2);
draw_text(display_get_gui_width() / 2, display_get_gui_height() - 64, "Loading...");
draw_set_alpha(1);