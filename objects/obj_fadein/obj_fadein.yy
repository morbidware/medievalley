{
    "id": "de7a42f4-ce22-4659-bdee-82d4b5ce6610",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fadein",
    "eventList": [
        {
            "id": "0592afba-2366-49af-8355-ac4eb3528371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de7a42f4-ce22-4659-bdee-82d4b5ce6610"
        },
        {
            "id": "5bdb90f8-ed60-483a-8de8-cde6919e4411",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "de7a42f4-ce22-4659-bdee-82d4b5ce6610"
        },
        {
            "id": "1d8389a7-b0e3-446d-a60c-4de65616d88f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de7a42f4-ce22-4659-bdee-82d4b5ce6610"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}