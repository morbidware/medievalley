if (t > 0)
{
	if(!global.newUserForChallenge)
	{
		t -= 0.025 * global.dt;
		audio_master_gain(1-t);
	}
}
else
{
	audio_master_gain(1);
	instance_destroy();
	
	if (string_length(global.popupTitle) > 0 && string_length(global.popupBody) > 0)
	{
		with(instance_create_depth(0,0,0,obj_popup))
		{
			title = global.popupTitle;
			body = global.popupBody;
			type = 1;
			btnText = "OK";
			caller = id;
		}
		global.popupTitle = "";
		global.popupBody = "";
	}
}