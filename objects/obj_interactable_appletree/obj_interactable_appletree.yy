{
    "id": "b1398446-b5aa-4b03-b1b5-c677be724efe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_appletree",
    "eventList": [
        {
            "id": "a09020d4-e57d-46c9-9cbe-8e90258a9712",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        },
        {
            "id": "1fb36f6b-b7a6-4ab7-9d9b-0230e58c1dff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        },
        {
            "id": "fb0107f1-2380-45b2-bda6-21496a7dcf11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        },
        {
            "id": "ac77cfea-60e8-4443-9485-d8e195b29be0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        },
        {
            "id": "bad8ae02-a0e4-4a16-bb7a-a5314de3b1ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        },
        {
            "id": "6bc836a3-b8ed-41a9-bd9d-f1b9b11e8a10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        },
        {
            "id": "9462ffec-4a60-40c0-a0e4-b4295fabdeee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "b1398446-b5aa-4b03-b1b5-c677be724efe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
    "visible": true
}