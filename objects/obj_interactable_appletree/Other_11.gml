/// @description can be interacted
// You can write your code in this editor
//show_debug_message("can be interacted?");
interactionPrefixLeft = "";
interactionPrefixRight = "";
var handState = global.game.player.handState;
if(growstage == growstages)
{
	global.retval = true;
	interactionPrefixLeft = "Pick";
	if(handState == HandState.Axe)
	{
		global.retval = true;
		interactionPrefixRight = "Chop";
	}
	else
	{
		interactionPrefixRight = "";
	}
}
else if(growstage > 1)
{
	interactionPrefixLeft = "";
	if(handState == HandState.Axe)
	{
		global.retval = true;
		interactionPrefixRight = "Chop";
	}
	else
	{
		global.retval = false;
	}
}
else if (room == roomFarm && global.othersocketid == "")
{
	if(global.game.player.handState == HandState.Shovel &&
		global.game.player.highlightCol = floor(x/16) &&
		global.game.player.highlightRow = floor(y/16))
	{
		global.retval = true;
		interactionPrefixRight = "Remove";
	}
	else
	{
		global.retval = false;
	}
}