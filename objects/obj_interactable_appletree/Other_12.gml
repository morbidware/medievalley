/// @description get chopped
var s = audio_play_sound(snd_chop,0,false);
audio_sound_pitch(s, 0.85 + random (0.35));
if(!global.ismobile)
{
	instance_create_depth(x,y,depth,obj_fx_particle_wood);
}
life--;
if (m_godMode)
{
	life -= 9;
}
growtimer -= 60;
if (life <= 0)
{
	event_perform(ev_other,ev_user0); 
}
else
{
	shakeCrono = 10;
}