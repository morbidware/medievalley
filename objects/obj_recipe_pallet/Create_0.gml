/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"wildgrass",6);
ds_map_add(ingredients,"sapling",2);

resitemid = "pallet";
unlocked = true;
desc = "An emergency bed to slowly recover stamina and health. Needs to be fixed after each use.";