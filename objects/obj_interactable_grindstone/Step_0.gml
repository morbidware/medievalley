/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(craftCrono > 0)
{
	if (!audio_is_playing(snd_grindstone))
	{
		audio_play_sound(snd_grindstone,0,true);
	}
	
	craftCrono --;
	t += ts;
	if(t > 8)
	{
		t = 0.0;
	}
	if(craftCrono <= 0)
	{
		repeat(3)
		{
			instance_create_depth(x,y+32,-(y+33),obj_fx_cloudlet);
		}
		scr_gameItemCreate(x,y+32,obj_pickable,craftid);
		craftid = "";
		t = 0.0;
		ts = 0;
		audio_stop_sound(snd_grindstone);
	}
}
