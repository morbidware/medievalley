{
    "id": "5a962b50-be57-47e3-9636-5ab9f24029e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_grindstone",
    "eventList": [
        {
            "id": "23556b82-a9e8-4ede-bf61-1c83042b8621",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5a962b50-be57-47e3-9636-5ab9f24029e7"
        },
        {
            "id": "4dc16b83-bc3e-491c-b967-32f0a84dc377",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a962b50-be57-47e3-9636-5ab9f24029e7"
        },
        {
            "id": "1ff8cc45-671f-4228-8efc-288f5503567e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5a962b50-be57-47e3-9636-5ab9f24029e7"
        },
        {
            "id": "5c9cf1d4-63f2-4503-808e-dcf8e7ce060a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5a962b50-be57-47e3-9636-5ab9f24029e7"
        },
        {
            "id": "aea327a9-6845-4f60-a7c5-4acd51457835",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "5a962b50-be57-47e3-9636-5ab9f24029e7"
        },
        {
            "id": "1dfe2cab-9a45-467b-abb5-22e09b0de724",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5a962b50-be57-47e3-9636-5ab9f24029e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5f926423-2718-45ac-925b-e656357be9ae",
    "visible": true
}