/// @description Debug, rooms, regrow routine

event_inherited();
global.dayprogress = 0.25;
if(challengeState == 0 && !instance_exists(obj_letter))
{
	precrono -= global.dt/60.0;
	if(precrono <= 0.0)
	{
		checkIfPlayerCanPlayChallenge(global.userigid,"play");
		challengeState = 1;
		var tool = -1;
		switch(global.challengeType)
		{
			case ChallengeType.Chopping:
			tool = scr_gameItemCreate(player.x,player.y+8,obj_pickable,"axe");
			break;
			case ChallengeType.Sickling:
			tool = scr_gameItemCreate(player.x,player.y+8,obj_pickable,"sickle");
			break;
			case ChallengeType.Mining:
			tool = scr_gameItemCreate(player.x,player.y+8,obj_pickable,"pickaxe");
			break;
			case ChallengeType.Watering:
			tool = scr_gameItemCreate(player.x,player.y+8,obj_pickable,"wateringcan");
			break;
			case ChallengeType.Fighting:
			tool = scr_gameItemCreate(player.x,player.y+8,obj_pickable,"sword");
			break;
			case ChallengeType.Picking:
			break;
			case ChallengeType.Hoeing:
			tool = scr_gameItemCreate(player.x,player.y+8,obj_pickable,"hoe");
			break;
		}
		player.prevx = player.x;
		player.prevy = player.y;
		if(tool > -1)
		{
			tool.magnetcrono = 1;
		}
		if(global.challengeType == ChallengeType.Fighting)
		{
			var r = choose(0,1,2);
			var rx = 0;
			var ry = 192;
			var fx = 48;
			var fy = 192;
			switch(r)
			{
				case 1:
				rx = 248;
				ry = 352;
				fx = 248;
				fy = 352-48;
				break;
				case 2:
				rx = 480;
				ry = 192;
				fx = 480-48;
				fy = 192;
				break;
			}
			
			
			var arch = instance_create_depth(rx, ry, 0, obj_archer);
			arch.forcedx = fx;
			arch.forcedy = fy;
		}
	}
}
if(challengeState == 1)
{
	var wfp = floor(global.walkFrames*0.1);
	if(wfp > global.walkFramesPoints)
	{
		global.walkFramesPoints ++;
		if(instance_exists(player))
		{
			var p = instance_create_depth(player.x,player.y-(sprite_get_height(player.sprite_index)/2),player.depth-1,obj_fx_challenge_points);
			p.val = global.walkFramesPointsValue;
		}
	}
	global.challengePoints = 0;
	global.challengePoints += global.walkFramesPoints*global.walkFramesPointsValue;//1
	global.challengePoints += global.interactions*global.interactionsValue;//10
	global.challengePoints += global.finishedInteractions*global.finishedInteractionsValue;//100
	global.challengePoints += global.bonusInteractions*global.bonusInteractionsValue;//200
	global.challengePoints += global.missedInteractions*global.missedInteractionsValue;//-20
	global.challengePoints += global.challengeHits*global.challengeHitsValue;//-50
	/*
	if(global.challengePoints < 0)
	{
		global.challengePoints = 0;
	}
	*/
	if(total >= required && finishTime == 0)
	{
		finishTime = (60.0 - global.challengeMalus) - crono;
	}
	if(global.challengeType == ChallengeType.Fighting)
	{
		archerCrono --;
		if(archerCrono <= 0)
		{
			archerCrono = archerTimer;
			
			var r = choose(0,1,2);
			var rx = 0;
			var ry = 192;
			var fx = 48;
			var fy = 192;
			switch(r)
			{
				case 1:
				rx = 248;
				ry = 352;
				fx = 248;
				fy = 352-48;
				break;
				case 2:
				rx = 480;
				ry = 192;
				fx = 480-48;
				fy = 192;
				break;
			}
			
			
			var arch = instance_create_depth(rx, ry, 0, obj_archer);
			arch.forcedx = fx;
			arch.forcedy = fy;
		}
	}
	if(!instance_exists(obj_letter))
	{
		crono -= (delta_time*0.000001)/global.csd;
	}
	if(crono <= 0.0)
	{
		crono = 0.0;
		challengeState = 2;
		tempGold = round((total / required) * 100);
		var gcearned = 0.00;
		var won = false;
		if(total >= required)
		{
			won = true;
		}
		if(total >= required)
		{
			//PREPARE LETTER TO WIN KEY
			var letter = instance_create_depth(0,0,0,obj_letter);
			letter.title = "The Challenge is over!";
			letter.body = "Astonishing! You won a Steam Key!\nGo to your profile and discover which game it is!\nYour Key will be in your profile under:\nBUNDLES LIBRARY->Indiegala Giveaway->Gameplay Giveaway";
			letter.signature = "King Thistle";
			letter.hasCup = true;
			letter.willRedirect = true;
			letter.redirectURL = "https://www.indiegala.com/profile";
			letter.ctaText = "Go to your Profile";
			letter.ctaURL = letter.redirectURL;
			letter.hold = true;
			letter.dontchangeoncallback = false;
		}
		else
		{
			if(total > 0)
			{
				if(global.willEarnGalaCreds)
				{
					/*-----------------------------------------*/
					/*
					var pot = 6;
					var tempGold2 = round((total/(required-1))*100);
					var expo = power(tempGold2,pot)/power(100,pot);
					if(total > 0)
					{
						if(expo < 0.1)
					    {
					        expo = 0.1;
					    }
						expo *= 0.1;
					    gcearned = round(expo*100)*0.01;
					}
					*/
					/*-----------------------------------------*/
					//PREPARE LETTER TO WIN GALACREDITS
					var letter = instance_create_depth(0,0,0,obj_letter);
					letter.title = "The Challenge is over!";
					letter.body = "Congratulations! You won some GalaCredits!";
					letter.signature = "King Thistle";
					letter.assignedGold = gcearned;
					letter.willRedirect = true;
					letter.redirectURL = "https://feudalife.indiegala.com/";
					letter.ctaText = "Play Feudalife";
					letter.ctaURL = letter.redirectURL;
					letter.ctaText2 = "Play Again";
					letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
					letter.hold = true;
					letter.dontchangeoncallback = false;
				}
				else
				{
					//PREPARE LETTER TO WIN GOLD
					var letter = instance_create_depth(0,0,0,obj_letter);
					letter.title = "The Challenge is over!";
					letter.body = "Congratulations! You won some gold to spend in feudalife!";
					letter.signature = "King Thistle";
					letter.assignedGold = tempGold;
					letter.willRedirect = true;
					letter.redirectURL = "https://feudalife.indiegala.com/";
					letter.ctaText = "Play Feudalife";
					letter.ctaURL = letter.redirectURL;
					letter.ctaText2 = "Play Again";
					letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
					letter.hold = true;
					letter.dontchangeoncallback = false;
					/*
					with(obj_char)
					{
						gold += other.tempGold;
					}
					*/
				}
			}
			else
			{
				//PREPARE LETTER TO WIN NOTHING
				var letter = instance_create_depth(0,0,0,obj_letter);
				letter.title = "The Challenge is over!";
				letter.body = "Unfortunately you won nothing. Better luck next time!";
				letter.signature = "King Thistle";
				letter.assignedGold = 0;
				letter.willRedirect = true;
				letter.redirectURL = "https://www.indiegala.com/gameplay-giveaway";
				letter.ctaText = "Back to Giveaway";
				letter.ctaURL = letter.redirectURL;
				letter.hold = true;
				letter.dontchangeoncallback = false;
			}
			scr_storeUserdata();
		}
		/*
		var a = round(1+won);
		var b = round(1+finishTime);
		var c = round(1+tempGold);
		var d = round(1+global.interactions);
		var e = round(1+global.finishedInteractions);
		var f = round(1+global.walkFrames);
		var g = round(1+(60-global.challengeMalus));

		var h = string(a*b)+string(c-(d*g))+string(((f*f)*(d*d))+b-e)+string((e-g+f)*(b-c+a));
		h = string_replace_all(h,".00","");
		var i = base64_encode(h);
		var l = md5_string_utf8(i);
		blogga("*****************************");
		blogga("string is " + string(h));
		blogga("base64 is " + string(i));
		blogga("md5 is " + string(l));
		blogga("*****************************");
		*/
		//sendBadge(global.userigid,global.challengeType,won,finishTime,tempGold,gcearned,global.interactions,global.finishedInteractions,global.walkFrames,60-global.challengeMalus,global.challengeMalusSecondsRemoved,l);
		sendBadge(global.userigid,global.challengeType,won,finishTime,tempGold,global.interactions,global.finishedInteractions,global.walkFrames,60-global.challengeMalus,global.challengeMalusSecondsRemoved,global.challengePoints);
	}
}
timestamp = real(getTimestamp());