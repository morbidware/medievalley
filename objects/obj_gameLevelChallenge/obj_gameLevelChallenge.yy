{
    "id": "910a2014-cea0-481d-985b-48aaa586b94a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevelChallenge",
    "eventList": [
        {
            "id": "e1428200-a476-4ed9-9aa9-ede851dc054b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "910a2014-cea0-481d-985b-48aaa586b94a"
        },
        {
            "id": "747bdb51-b234-41ce-85d1-24bf3e06d22c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "910a2014-cea0-481d-985b-48aaa586b94a"
        },
        {
            "id": "344882f5-6bee-45b5-9403-2118eee4af59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "910a2014-cea0-481d-985b-48aaa586b94a"
        },
        {
            "id": "fb7d67ce-fe8f-48d6-a8bb-d06c75e6a079",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "910a2014-cea0-481d-985b-48aaa586b94a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}