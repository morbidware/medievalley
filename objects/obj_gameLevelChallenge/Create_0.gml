/// @description Init and populate map
event_inherited();
ds_list_clear(global.lockHUDparticipants);

random_set_seed(global.userid);

logga("----------------------------------");
logga("CREATE NEW CHALLENGE ROOM");

global.dayprogress = 0.25;
randomize();


if (global.online)
{
	logga("username " + global.username);
	logga("other username " + global.otherusername);
	logga("userid " + string(global.userid));
	logga("other userid " + string(global.otheruserid));
}

global.challengeType = global.lastChallengeDone;
if(m_debugChallengeType > -1)
{
	global.challengeType = m_debugChallengeType;
}

challengeground = instance_create_depth(0,0,0,obj_challengeGround);

player = instance_create_depth(((global.challengeCols/2)+1)*16, 2, 0, obj_char);

//player.life = 100;
player.stamina = 100;


instance_create_depth(0,0,0,obj_fadein);
instance_create_depth(0,0,0,obj_sound_birds);

audio_play_sound(bgm_farm_a,0,true);

audio_play_sound(snd_white_noise,0,true);

// ======================================| INTERACTABLES |======================================

logga("Spawning new interactables");

var sx = 8+32*2;
var sy = 8+32*4;

if(global.challengeType == ChallengeType.Chopping)
{
	var i = 0;
	var ox = 32;
	var oy = 32;
	repeat(11)
	{
		scr_gameItemCreate(sx+ox*i,sy+oy*0,obj_interactable_pine,"pine");
		scr_gameItemCreate(sx+ox*i,sy+oy*1,obj_interactable_pine,"pine");
		scr_gameItemCreate(sx+ox*i,sy+oy*2,obj_interactable_pine,"pine");
		scr_gameItemCreate(sx+ox*i,sy+oy*3,obj_interactable_pine,"pine");
		scr_gameItemCreate(sx+ox*i,sy+oy*4,obj_interactable_pine,"pine");	
		i++;
	}
	
	do
	{
		var obj = obj_interactable_pine;
		var rand = irandom_range(0,instance_number(obj)-1);
		
		var inst = instance_find(obj,rand);
		if(inst.growstage == 4)
		{
			inst.growstage = 3;
			inst.life = inst.lifeperstage * inst.growstage;
			inst.image_index = inst.growstage-1;
			global.challengeGoodNumber--;
			logga("changed pine: " + string(global.challengeGoodNumber) + " left.");
		}
	}until(global.challengeGoodNumber == 0);
}
else if(global.challengeType == ChallengeType.Sickling)
{
	var i = 0;
	var ox = 16;
	var oy = 16;
	repeat(22)
	{
		scr_gameItemCreate(sx+ox*i,sy+oy*0,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*1,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*2,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*3,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*4,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*5,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*6,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*7,obj_interactable_wildgrass,"wildgrass");
		scr_gameItemCreate(sx+ox*i,sy+oy*8,obj_interactable_wildgrass,"wildgrass");
		i++;
	}
	
	do
	{
		var obj = obj_interactable_wildgrass;
		var inst = instance_find(obj,irandom_range(0,instance_number(obj)-1));
		if(inst.growstage == 2)
		{
			inst.growstage = 1;
			inst.life = 2;
			inst.image_index = inst.growstage-1;
			global.challengeGoodNumber--;
		}
	}until(global.challengeGoodNumber == 0);
	
}
else if(global.challengeType == ChallengeType.Mining)
{
	var i = 0;
	var ox = 32;
	var oy = 32;
	repeat(11)
	{
		var j = 0;
		repeat(5)
		{
			scr_gameItemCreate(sx+ox*i,sy+oy*j,obj_interactable_rockbig,"rockbig");
			j++;
		}
		i++;
	}
	
	do
	{
		var obj = obj_interactable_rockbig;
		var inst = instance_find(obj,irandom_range(0,instance_number(obj)-1));
		var xx = inst.x;
		var yy = inst.y;
		instance_destroy(inst);
		scr_gameItemCreate(xx,yy,obj_interactable_rock,"rock");
		global.challengeGoodNumber--;
	}until(global.challengeGoodNumber == 0);
	
}
else if(global.challengeType == ChallengeType.Watering)
{
	var i = 0;
	var ox = 32;
	var oy = 32;
	repeat(11)
	{
		var j = 0;
		repeat(5)
		{
			var plant = scr_gameItemCreate(sx+ox*i,sy+oy*j,obj_interactable_redbeans_plant,"redbeansplant");
			plant.growstage = 1;
			plant.gridded = 1;
			plant.growcrono = 0;
			
			var intCol = (plant.x-8)/16;
			var intRow = (plant.y-8)/16;
			
			show_debug_message("intCol " + string(intCol) + " intRow " + string(intRow));
			
			ds_grid_set(challengeground.itemsgrid, intCol, intRow, plant);
			j++;
		}
		i++;
	}
}
else if(global.challengeType == ChallengeType.Picking)
{
	var i = 0;
	var ox = 32;
	var oy = 32;
	repeat(11)
	{
		scr_gameItemCreate(sx+ox*i,sy+oy*0,obj_interactable_sapling,"sapling");
		scr_gameItemCreate(sx+ox*i,sy+oy*1,obj_interactable_sapling,"sapling");
		scr_gameItemCreate(sx+ox*i,sy+oy*2,obj_interactable_sapling,"sapling");
		scr_gameItemCreate(sx+ox*i,sy+oy*3,obj_interactable_sapling,"sapling");
		scr_gameItemCreate(sx+ox*i,sy+oy*4,obj_interactable_sapling,"sapling");
		i++;
	}
	
	do
	{
		var obj = obj_interactable_sapling;
		var inst = instance_find(obj,irandom_range(0,instance_number(obj)-1));
		if(inst.hard)
		{
			inst.hard = false;
			inst.sprite_index = spr_interactable_sapling;
			global.challengeGoodNumber--;
		}
	}until(global.challengeGoodNumber == 0);
	
}
// ======================================| MISSIONS |======================================
var cam = instance_create_depth(player.x,player.y,0,obj_camera);
// ======================================| FXS |======================================
repeat(2)
{
	var rx = random(room_width);
	var ry = random(room_height);
	if(!global.ismobile)
	{
		instance_create_depth(rx, ry, -ry, obj_fx_ground_dust);
	}
}
// ======================================| PLAYER SPAWN POSITION |======================================
player.x = (global.challengeCols/2)*16;
player.y = 64;
player.lastdirection = Direction.Down;

cam.x = player.x;
cam.y = player.y;

precrono = 3;
crono = 60 - global.challengeMalus;
total = 0;
required = 8;
challengeState = 0;
tempGold = 0;
finishTime = 0;

archerTimer = 300*global.csd;
archerCrono = 300*global.csd;

var letter = instance_create_depth(0,0,0,obj_letter);
letter.title = "Beat the chopping challenge!";
letter.body = scr_fix_text("Chop "+string(required)+" Trees within "+string(crono)+" seconds to get your prize!");
letter.signature = "Indiegala";
if(global.challengeType == ChallengeType.Sickling)
{
	required = 60;
	letter.title = "Beat the sickling challenge!";
	letter.body = scr_fix_text("Sickle "+string(required)+" Wildgrass within "+string(crono)+" seconds to get your prize!");
	
}
if(global.challengeType == ChallengeType.Mining)
{
	required = 14;
	letter.title = "Beat the mining challenge!";
	letter.body = scr_fix_text("Mine "+string(required)+" Rocks within "+string(crono)+" seconds to get your prize!");
}
if(global.challengeType == ChallengeType.Watering)
{
	required = 16;
	letter.title = "Beat the watering challenge!";
	letter.body = scr_fix_text("Grow "+string(required)+" Red Beans Plants within "+string(crono)+" seconds to get your prize!");
}
if(global.challengeType == ChallengeType.Fighting)
{
	required = 9;
	letter.title = "Beat the fighting challenge!";
	letter.body = scr_fix_text("Kill "+string(required)+" archers within "+string(crono)+" seconds to get your prize!");
}
if(global.challengeType == ChallengeType.Picking)
{
	required = 40;
	letter.title = "Beat the picking challenge!";
	letter.body = scr_fix_text("Pick "+string(required)+" saplings within "+string(crono)+" seconds to get your prize!");
}
if(global.challengeType == ChallengeType.Hoeing)
{
	required = 60;
	letter.title = "Beat the hoeing challenge!";
	letter.body = scr_fix_text("Hoe "+string(required)+" tiles within "+string(crono)+" seconds to get your prize!");
}
scr_storeUserdata();

timestamp = real(getTimestamp());