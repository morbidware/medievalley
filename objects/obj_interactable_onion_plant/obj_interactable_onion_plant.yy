{
    "id": "dc0acc1a-b7b3-48cb-b296-34069e740a12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_onion_plant",
    "eventList": [
        {
            "id": "036dcfab-319e-4745-98f0-aec2c77a45e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "dc0acc1a-b7b3-48cb-b296-34069e740a12"
        },
        {
            "id": "160179b9-bbd6-4c30-a309-9e48b547e7e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "dc0acc1a-b7b3-48cb-b296-34069e740a12"
        },
        {
            "id": "ffc02961-8f55-4542-bf7e-c0c9eb157780",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "dc0acc1a-b7b3-48cb-b296-34069e740a12"
        },
        {
            "id": "408abc3a-4d9e-4564-a819-2f6a6cde5c08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "dc0acc1a-b7b3-48cb-b296-34069e740a12"
        },
        {
            "id": "7e87543f-b296-4604-a75c-1cbc88947b6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "dc0acc1a-b7b3-48cb-b296-34069e740a12"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
    "visible": true
}