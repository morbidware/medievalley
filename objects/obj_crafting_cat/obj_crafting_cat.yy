{
    "id": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat",
    "eventList": [
        {
            "id": "5e107288-4e3f-4c9e-98bf-e5b6bd5fb365",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac"
        },
        {
            "id": "92d9e5d8-622f-4d74-ac43-caf275c5b010",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac"
        },
        {
            "id": "9dd0a2fc-059a-4cfa-a879-55b45c5d5b7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac"
        },
        {
            "id": "5b1590a7-3a8a-45dd-b9dd-4c3506e63471",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}