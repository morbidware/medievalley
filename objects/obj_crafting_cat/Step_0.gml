// position
x = sidebar.x + 14;
y = sidebar.y + + 14 + 38 * index;

// move away if not unloked yet
if(!unlocked)
{
	x = -200;
	exit;
}

// turn category on/off depending by distance to structure
if(neededStructure != noone)
{
	if(instance_exists(neededStructure))
	{
		var pla = instance_find(obj_char,0);
		var gs = instance_nearest(pla.x,pla.y,neededStructure);
		var dx = abs(pla.x - gs.x);
		var dy = abs(pla.y - gs.y);
		if(dx < 48 && dy < 48)
		{
			x = sidebar.x + 14;
		}
		else
		{
			x = -100;
			if(state == 1)
			{
				state = 0;		
			}
		}
	}
	else
	{
		x = -100;
		if(state == 1)
		{
			state = 0;		
		}
	}
}

// Detect if any recipe inside is craftable
var value = false;
for(var i = 0; i < ds_list_size(entries); i++)
{
	var entry = ds_list_find_value(entries,i);
	if(entry.cat == other.id && (entry.state == 1 || entry.state == 3))
	{
		value = true;
		break;
	}
}

if(value == true)
{
	if(image_index == 0)
	{
		if(vfxinstance != obj_fx_ui_circle)
		{
			if(!global.isSurvival)vfxinstance = instance_create_depth(x+14,y+14,depth-1,obj_fx_ui_circle);
		}
		
	}
	image_index = 1;
}
else
{
	image_index = 0;
}
var mx = mouse_get_relative_x();
var my = mouse_get_relative_y();

if(point_in_rectangle(mx,my,x,y,x+28,y+28))
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(x,y+14,0,obj_ui_dock_buttoninfo);
		mi.desc = name;
		mi.offsetx = 200;
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.y == y+14)
		{
			instance_destroy(mi);
		}
	}
}