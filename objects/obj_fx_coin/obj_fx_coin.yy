{
    "id": "c0d53c98-229f-491f-a96a-914a2c419cd8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_coin",
    "eventList": [
        {
            "id": "cc669977-9ca5-44b7-bd86-60472117463a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0d53c98-229f-491f-a96a-914a2c419cd8"
        },
        {
            "id": "0150097d-e934-4c29-b537-4d6d9f9fc2d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0d53c98-229f-491f-a96a-914a2c419cd8"
        },
        {
            "id": "a593217c-f320-4e9c-9e85-128854c7e5fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c0d53c98-229f-491f-a96a-914a2c419cd8"
        },
        {
            "id": "f5bc1e88-8cb5-48a1-8ad4-d1f61b27e64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c0d53c98-229f-491f-a96a-914a2c419cd8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
    "visible": true
}