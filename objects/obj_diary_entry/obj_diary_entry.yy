{
    "id": "05a3eae8-a12b-468c-9b9f-1ff394d1062e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_diary_entry",
    "eventList": [
        {
            "id": "e282547f-7307-4b9e-8dcb-164740ba2ff6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05a3eae8-a12b-468c-9b9f-1ff394d1062e"
        },
        {
            "id": "bd375649-4103-4586-a66b-cb35eb8e7b0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "05a3eae8-a12b-468c-9b9f-1ff394d1062e"
        },
        {
            "id": "eb930c98-9c10-46b3-8e02-f51d19c0965a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "05a3eae8-a12b-468c-9b9f-1ff394d1062e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}