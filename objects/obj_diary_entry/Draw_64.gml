if (instance_exists(obj_diary_panel))
{
	if (instance_find(obj_diary_panel,0).showStats)
	{
		return;
	}
}

draw_set_halign(fa_left);
draw_set_valign(fa_top);

if (highlight)
{
	draw_set_alpha(0.1);
	draw_set_color(c_black);
	draw_rectangle(x-2,y-2,x+width+4,y+height-4,false);
}

// title
draw_set_alpha(0.7);
draw_set_color(c_white);
draw_set_font(global.FontStandardBoldOutline);
draw_text(x,y,title);

// description
draw_set_color(c_black);
draw_set_font(global.FontStandard);
scr_draw_text_ext(x,y+13,shortdesc,10,width);

// separator line
draw_set_alpha(0.5);
draw_rectangle(x, y+height-8, x+width, y+height-7, false);
draw_set_alpha(1);
