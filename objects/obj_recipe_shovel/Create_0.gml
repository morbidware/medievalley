/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",3);
ds_map_add(ingredients,"flint",2);

resitemid = "shovel";
unlocked = true;
desc = "Usable to remove grass, plants and tree stumps from the ground.";