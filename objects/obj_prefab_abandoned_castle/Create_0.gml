/// @description Init
event_inherited();

// Generate child prefabs
with (instance_create_depth(x,y,depth,obj_prefab_abandoned_castle_a)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_abandoned_castle_b)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_abandoned_castle_planks)) { parent = other.id; }