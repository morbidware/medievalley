{
    "id": "82621e82-31bf-46b0-8619-f865b77edfb4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_maplimit",
    "eventList": [
        {
            "id": "7ab99df2-1ed1-4224-b32a-1a56f38229da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82621e82-31bf-46b0-8619-f865b77edfb4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0007196-f2fc-4c64-8264-47a51f0752dd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
    "visible": true
}