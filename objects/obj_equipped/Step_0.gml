if (!visible)
{
	x = player.x;
	y = player.y;
}

//change depth and position depending by player direction
switch (player.lastdirection)
{
	case Direction.Up:
	depth = player.depth + 2;
	//y = y-2;
	image_xscale = 1;
	break;
	case Direction.Down:
	depth = player.depth - 2;
	//y = y+2;
	image_xscale = 1;
	break;
	case Direction.Left:
	depth = player.depth - 2;
	//x = x-2;
	image_xscale = -1;
	break;
	case Direction.Right:
	depth = player.depth - 2;
	//x = x+2;
	image_xscale = 1;
	break;
}

if(sprite_index == spr_null)
{
	//show_debug_message("spr_equipped_"+spritename+"_side");
	sprite_index = scr_asset_get_index("spr_equipped_"+spritename+"_side");
}