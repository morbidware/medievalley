{
    "id": "cd38c1c5-7bf4-4dbb-9c6f-9e48d8842c10",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_equipped",
    "eventList": [
        {
            "id": "83d25bcb-0200-45bc-9bbc-e97d4e762819",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cd38c1c5-7bf4-4dbb-9c6f-9e48d8842c10"
        },
        {
            "id": "6e4c31ec-f95b-4422-bf95-95281736d5f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cd38c1c5-7bf4-4dbb-9c6f-9e48d8842c10"
        },
        {
            "id": "ba679663-b392-4741-9e03-b1577f32ee62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cd38c1c5-7bf4-4dbb-9c6f-9e48d8842c10"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "cf8deb36-79b4-4043-940c-ca9d662edda0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}