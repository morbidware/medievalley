{
    "id": "438ca5ff-6970-44a4-a557-430b15eb946d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_beechtree",
    "eventList": [
        {
            "id": "7ce43af5-a9ff-4aa7-92ac-b2a87ae26045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        },
        {
            "id": "7ffeef2d-9ca5-4990-842d-34f6b115372c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        },
        {
            "id": "61c2ab3f-5010-47f3-8720-538df5c6c4a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        },
        {
            "id": "823949fc-34ef-471d-9a2a-e29c56626c2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        },
        {
            "id": "abc9fe41-ddfe-4024-970d-bb02e0cef9d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        },
        {
            "id": "4641cee8-183a-4c7b-bb18-c86f35305862",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        },
        {
            "id": "249bb78f-80ba-43d9-9f82-d26a2c4acb7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "438ca5ff-6970-44a4-a557-430b15eb946d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
    "visible": true
}