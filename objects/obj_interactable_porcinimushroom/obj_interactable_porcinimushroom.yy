{
    "id": "870b3336-1827-49b4-b067-d80f8af70d47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_porcinimushroom",
    "eventList": [
        {
            "id": "2d344286-785c-4d41-9c18-345d0e2fa89b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "870b3336-1827-49b4-b067-d80f8af70d47"
        },
        {
            "id": "31562d2d-7a05-4188-adc9-9a5ec320008c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "870b3336-1827-49b4-b067-d80f8af70d47"
        },
        {
            "id": "1d529a9e-fd4e-48f5-a24b-c3b897693c9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "870b3336-1827-49b4-b067-d80f8af70d47"
        },
        {
            "id": "b36dd411-cac0-412e-89f6-09b5d61c2a47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "870b3336-1827-49b4-b067-d80f8af70d47"
        },
        {
            "id": "f44d9624-5c82-4c24-a32a-24fa4bc75cf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "870b3336-1827-49b4-b067-d80f8af70d47"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed8edc33-b18e-4ab1-8eea-2507e5d03f1f",
    "visible": true
}