/// @description Insert description here
// You can write your code in this editor
event_inherited();
unlocked = true;
name = "DRESS";
with(instance_create_depth(0,0,-1,obj_recipe_grasshat))
{
	index = 0;
	cat = other.id;
	ds_list_add(other.entries, id);
}
with(instance_create_depth(0,0,-1,obj_recipe_grasssuit))
{
	index = 1;
	cat = other.id;
	ds_list_add(other.entries, id);
}
