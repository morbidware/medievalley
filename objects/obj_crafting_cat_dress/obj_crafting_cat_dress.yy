{
    "id": "4c46e32b-3dd0-4296-bb77-5094d6cae574",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_dress",
    "eventList": [
        {
            "id": "6f6764b0-d536-4dbf-9473-ac10e485fbe0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c46e32b-3dd0-4296-bb77-5094d6cae574"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
    "visible": true
}