{
    "id": "3e1b0bd1-0ea3-4799-b538-9907d9176ada",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_base",
    "eventList": [
        {
            "id": "62c2435e-3918-49d0-a623-f54695e20407",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e1b0bd1-0ea3-4799-b538-9907d9176ada"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0aeefc49-46cc-4852-ba3a-6ce6735b84f9",
    "visible": true
}