{
    "id": "434d9173-f446-40ed-9c6f-2b3a1b5d4062",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challenge_malus_letter",
    "eventList": [
        {
            "id": "0731ed22-429a-4132-9b27-a892dc56ba42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "475cd9ed-b780-456e-8e93-2b1a3b8e2e00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "e7db355a-d365-4872-a9f1-1a5383ea0ffb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "99c0e9c3-72cf-4b6c-a43b-90bf88fc0f39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "513eeb79-caaa-4f23-9c7c-882183935d41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "90446bdd-6783-4ff5-8308-e4428e7e5734",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "063e13eb-485b-4bb6-86a0-3f88c82a5833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "00cbcfdb-e330-4cb8-9a24-851704191f19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "f9d1bb94-0919-42b4-a744-708572cd9bf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "ee297c36-7ecc-4e77-b2cf-438e7486618b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        },
        {
            "id": "642916a1-d7a8-49bd-9523-54ea90afe390",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 7,
            "m_owner": "434d9173-f446-40ed-9c6f-2b3a1b5d4062"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}