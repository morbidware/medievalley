/// @description Remove Malus
// You can write your code in this editor
isFetching = true;
btnMalus.disabled = true;
btnMinus.disabled = true;
btnPlus.disabled = true;
btnPlay.disabled = true;
if(m_debugChallenge)
{
	gmcallback_removeMalus("1");
}
else
{
	removeMalus(global.userigid,string(seconds_malus_to_remove));
}