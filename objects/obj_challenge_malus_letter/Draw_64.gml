draw_set_color(c_black);
if(state == 0)
{
	draw_set_alpha(t*0.75);
}
else if(state == 1)
{
	draw_set_alpha(0.75-(t*0.75));
}
draw_rectangle(0,0,display_get_gui_width(),display_get_gui_height(),false);
draw_set_alpha(1.0);

draw_set_color(c_white);
scr_nine_slice_at(spr_9slice_letter, display_get_gui_width()/2, y, width, height, 2);

draw_sprite_ext(spr_medievalley_post_stamp,0,x,y,2,2,0,c_white,0.2);

var startx = scr_adaptive_x(display_get_gui_width()/2 - width/2);
var starty = y - height/2;

draw_set_color(c_black);
draw_set_font(global.FontTitles);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
//draw_text(startx + margin, starty + margin, title);

var sep = 13;
var w = width-margin-margin;

draw_text_ext(startx + margin, starty + margin, title, sep+8, w - 24);

var titleh = scr_string_height_ext(title, sep+8, w-24) + 30;

draw_set_font(global.FontStandard);
draw_set_color(c_black);

scr_draw_text_ext(startx + margin, starty + margin + titleh + margin, body, sep, w);

var bodyh = scr_string_height_ext(body, sep, w);
var diff = clamp( 125 - bodyh, 15, 999);
//bodyh += diff;

draw_set_font(global.FontStandardBold);
if(seconds_malus > 0)
{
	scr_draw_text_ext(startx + margin, starty + margin + titleh + margin + bodyh + margin, calculations, sep, w);
}
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
scr_draw_text_ext(startx + ((width/4.0)*3.0), starty + margin, "Galasilver:"+string(global.challengeGalasilver), sep, w);
var calch = scr_string_height_ext(calculations, sep, w);

draw_set_font(global.FontTitles);
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
//btnMalus.ox = 240;
//btnMalus.oy = 70;
if(seconds_malus > 0)
{
	draw_text(startx + width/2.0 + btnMalus.ox, starty + margin + titleh + margin + bodyh + margin + margin + margin,"Malus: " + string(seconds_malus) + "s ");
}
draw_set_font(global.FontStandard);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
draw_text_ext(startx + margin, starty + height - margin - margin, signature, sep, w);

var signatureh = scr_string_height_ext(signature, sep, w);

height = margin + margin + titleh + margin + bodyh + margin + signatureh + margin;
height = 300;
btnClose.ox = width/2 - 16-8;
btnClose.oy = -height/2 + 16+8;

if(global.ismobile)
{
	width = global.cw;
	height = global.ch;
}