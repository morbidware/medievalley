event_inherited();
if(instance_exists(btnClose))
{
	instance_destroy(btnClose);
}

if(instance_exists(btnPlay))
{
	instance_destroy(btnPlay);
}

if(instance_exists(btnMalus))
{
	instance_destroy(btnMalus);
}

if(instance_exists(btnMinus))
{
	instance_destroy(btnMinus);
}

if(instance_exists(btnPlus))
{
	instance_destroy(btnPlus);
}

with(obj_home)
{
	btnChallenge.disabled = false;
	btnWiki.disabled = false;
	btnFeedback.disabled = false;
}

with(obj_loading_panel)
{
	instance_destroy();
}