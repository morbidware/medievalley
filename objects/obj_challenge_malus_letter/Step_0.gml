/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(isFetching)
{
	exit;
}
if(instance_exists(btnMalus))
{
	if(seconds_malus_to_remove == 0)
	{
		btnMalus.disabled = true;
	}
	else
	{
		btnMalus.disabled = false;
	}
}

if(instance_exists(btnMinus))
{
	if(seconds_malus_to_remove == 0)
	{
		btnMinus.disabled = true;
	}
	else
	{
		btnMinus.disabled = false;
	}
}
if(instance_exists(btnPlus))
{
	if(seconds_malus_to_remove == seconds_malus)
	{
		btnPlus.disabled = true;
	}
	else
	{
		btnPlus.disabled = false;
	}
}