event_inherited();
depth = -15600;

width = 740; // YEET!!!

title = "Notice from the King!";
body = "You won " + string(global.challengeMalus_keysWon) + " keys in the last 24 hours. Every key won gives you a malus. For every key won (24 hours): "+string(global.malusSecondsHigh)+" seconds malus, if your account is old (30 days) remove "+string(global.malusSecondsHigh-global.malusSecondsBase)+" seconds, if you have made a store purchase (24 hours) remove "+string(1*global.bonusSecondsMulti)+" seconds, if you have made a bundle purchase (24 hours) remove "+string(1*global.bonusSecondsMulti)+" seconds, if you have at least one positive feedback for a giveaway (24 hours) remove "+string(1*global.bonusSecondsMulti)+" seconds, if you are a member of the Indiegala Steam Group remove 1 second. You can lower your malus by waiting or by paying "+string(global.challengeGalasilverPrice)+" Galasilver per second.";

calculations = "Keys won = " + string(global.challengeMalus_keysWon); 
calculations += "\nStarting Malus Multiplier = "+string(global.malusSecondsHigh)+" seconds";  
var malusMultiplier = global.malusSecondsBase;
if(global.challengeMalus_newUser)
{ 
	malusMultiplier = global.malusSecondsHigh;
}
else
{
	var diff = string(global.malusSecondsHigh-global.malusSecondsBase);
	calculations += "\nOld Account = remove "+diff+" seconds = "+string(global.malusSecondsBase)+" seconds";  
}
if(global.challengeMalus_storePurchase)
{
	malusMultiplier -= 1*global.bonusSecondsMulti;
	calculations += "\nStore purchase = remove "+string(1*global.bonusSecondsMulti)+" second = "+string(malusMultiplier) + " seconds";  
}
if(global.challengeMalus_bundlePurchase)
{
	malusMultiplier -= 1*global.bonusSecondsMulti;
	calculations += "\nBundle purchase = remove "+string(1*global.bonusSecondsMulti)+" second = "+string(malusMultiplier) + " seconds";  
}
if(global.challengeMalus_giveawayCreated)
{
	malusMultiplier -= 1*global.bonusSecondsMulti;
	calculations += "\nPositive feedback on giveaway = remove "+string(1*global.bonusSecondsMulti)+" second = "+string(malusMultiplier) + " seconds";  
}
if(global.challengeMalus_steamGroupMember)
{
	malusMultiplier -= 1;
	calculations += "\nSteam group member = remove 1 second = "+string(malusMultiplier) + " seconds";  
}
seconds_malus = clamp(global.challengeMalus_keysWon*malusMultiplier,0,1000);
seconds_malus = global.challengeMalus_keysWon*malusMultiplier;
calculations += "\nFinal Malus = Keys X Malus = " + string(global.challengeMalus_keysWon) + " X " + string(malusMultiplier) + " = " + string(global.challengeMalus_keysWon*malusMultiplier) + " = " +  string(seconds_malus) + " seconds";
signature = "King Thistle";

margin = 16;
height = 0;

seconds_malus_to_remove = 0;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

btnClose = instance_create_depth(0,0,-10003,obj_btn_generic);
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;
btnClose.sprite_index = spr_btn_close_silver;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}

rrot = random_range(-10,10);

w_width = 740;
w_height = 840;
var btnsoy = 88;
btnPlay = instance_create_depth(0,0,-10001,obj_btn_generic);
btnPlay.sprite_index = spr_btn_challenge_malus_play;
btnPlay.fnt = global.FontTitles;
btnPlay.text = "Play Anyway";
btnPlay.caller = id;
btnPlay.color = $000000;
btnPlay.topShadow = false;
btnPlay.evt = ev_user1;
btnPlay.ox = -w_width/4.0;
btnPlay.oy = btnsoy;
btnPlay.depth = depth-1;

btnMalus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnMalus.sprite_index = spr_btn_challenge_malus_play;
btnMalus.fnt = global.FontStandardBold;
btnMalus.text = "Remove 0 s";
btnMalus.caller = id;
btnMalus.color = $000000;
btnMalus.topShadow = false;
btnMalus.evt = ev_user2;
btnMalus.ox = w_width/4.0-16;
btnMalus.oy = btnsoy;
btnMalus.depth = depth-1;

btnMinus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnMinus.sprite_index = spr_btn_challenge_malus_modifier_minus;
btnMinus.fnt = global.FontTitles;
btnMinus.text = "-";
btnMinus.caller = id;
btnMinus.color = $000000;
btnMinus.topShadow = false;
btnMinus.evt = ev_user3;
btnMinus.ox = btnMalus.ox-130;
btnMinus.oy = btnsoy;
btnMinus.depth = depth-1;

btnPlus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnPlus.sprite_index = spr_btn_challenge_malus_modifier_plus;
btnPlus.fnt = global.FontTitles;
btnPlus.text = "+";
btnPlus.caller = id;
btnPlus.color = $000000;
btnPlus.topShadow = false;
btnPlus.evt = ev_user4;
btnPlus.ox = btnMalus.ox+130;
btnPlus.oy = btnsoy;
btnPlus.depth = depth-1;

with(obj_home)
{
	btnChallenge.disabled = true;
	btnWiki.disabled = true;
	btnFeedback.disabled = true;
}

isFetching = false;