{
    "id": "752fa282-f1f1-49a1-8f81-0de7183d3ef3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_camera",
    "eventList": [
        {
            "id": "967a0add-3179-4127-b915-a1db4c5945ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "752fa282-f1f1-49a1-8f81-0de7183d3ef3"
        },
        {
            "id": "ebeb5c29-c1c2-46d0-914a-1c9c35bb494e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "752fa282-f1f1-49a1-8f81-0de7183d3ef3"
        },
        {
            "id": "0657b9f9-5cae-4449-bd7b-1a1e7ff53165",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "752fa282-f1f1-49a1-8f81-0de7183d3ef3"
        },
        {
            "id": "df10b362-ffa6-4a6a-8dfa-6ce4f6c7b806",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "752fa282-f1f1-49a1-8f81-0de7183d3ef3"
        },
        {
            "id": "90441489-2b0d-4c4a-8adb-d85d870c0eba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "752fa282-f1f1-49a1-8f81-0de7183d3ef3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}