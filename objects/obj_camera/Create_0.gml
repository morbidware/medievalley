// Setup
target = instance_find(obj_char,0);
margin = 64;
accelrange = 96;
maxspeed = 8;
zoom = 0;

// Locals
travelspeed = 0;
distancetraveled = 0;
m = 0;
offx = 0;
offy = 0;
viewx = 0;
viewy = 0;
width = 0;
height = 0;
mult = 0
culltimer = 0;
timestamp = 0;
prevtarget = noone;
camera_set_view_target(view_camera[0],id);