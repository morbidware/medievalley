/// @description Follow target

if(target == noone || target == undefined)
{
	exit;
}
if (!instance_exists(target))
{
	exit;
}

if (prevtarget != camera_get_view_target(view_camera[0]))
{
	prevtarget = camera_get_view_target(view_camera[0]);
	distancetraveled = 0;
}

var dist = point_distance(x,y,target.x,target.y);
var angle = point_direction(x,y,target.x,target.y);
var anglerad = degtorad(angle);

if (distancetraveled < accelrange)
{
	travelspeed = ((distancetraveled / accelrange) + 0.1) * maxspeed * global.dt;
	if (zoom == 0)
	{
		travelspeed *= 2;
	}
}
else if (dist < accelrange)
{
	travelspeed = (dist / accelrange) * maxspeed * global.dt;
	if (zoom == 0)
	{
		travelspeed *= 2;
	}
}
else if (dist < travelspeed)
{
	travelspeed = dist;
}
else
{
	travelspeed = maxspeed * global.dt + 0.1;
	if (zoom == 0)
	{
		travelspeed *= 2;
	}
}

x += cos(anglerad) * travelspeed;
y -= sin(anglerad) * travelspeed;
distancetraveled += travelspeed;

// stop culling routine if scene is doing a fade; the save system will enable all deactivated objects
if (instance_exists(obj_fadeout) || instance_exists(obj_fadein))
{
	culltimer = 20;
}