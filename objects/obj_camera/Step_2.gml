/// @description Object culling

// Unexpectedly, native "culling" mode (it's not culling actually) is way faster than using
// data structures for keeping track of objects to activate and deactivate depending by visibility.

// ------------------------ NATIVE CULLING MODE ------------------------
culltimer -= global.dt;
if (culltimer <= 0)
{
	// When pidgeon is arriving, extend the margin (camera goes fast in that moment)
	var pidgeon = instance_find(obj_wildlife_mail_pidgeon,0);
	m = margin;
	if (pidgeon > 0)
	{
		if (pidgeon.status == Status.Arrive)
		{
			m *= 2;
		}
	}
	
	// Calculate default view area and dynamic offset
	mult = global.shadowlength * 64;
	if (global.shadowalpha <= 0)
	{
		mult = 0;
	}
	offx = sin(degtorad(global.shadowangle)) * mult;
	offy = cos(degtorad(global.shadowangle)) * mult;
	viewx = global.viewx - m;
	viewy = global.viewy - m;
	width = scr_adaptive_x(480) + (m * 2);
	height = scr_adaptive_y(270) + (m * 4); // Margin in the bottom area is longer to help show correctly tall objects
	if (offx < 0)
	{
		viewx += offx;
		width -= offx;
	}
	if (offx > 0)
	{
		width += offx;
	}
	if (offy < 0)
	{
		viewy += offy;
		height -= offy;
	}
	if (offy > 0)
	{
		height += offy;
	}
	
	// Disable specific objects if outside the view. Note: interactables behave a different way
	timestamp = round(get_timer() / 1000000);
	if(room != roomFarm)
	{
		with (obj_interactable)
		{
			if (!wasdisabled)
			{
				lasttimestamp = other.timestamp;
				wasdisabled = true;
			}
		}
		instance_deactivate_object(obj_interactable);
	}
	else
	{
		for(var i = 0; i < instance_number(obj_interactable); i++)
		{
			var interactable = instance_find(obj_interactable,i);
			interactable.canBeDrawn = false;
		}	
	}
	instance_deactivate_object(obj_pickable);
	instance_deactivate_object(obj_fx);
	instance_deactivate_object(obj_statictile);
	instance_deactivate_object(obj_wildlife);

	with(obj_enemy)
	{
		if(canBeDeactivated)
		{
			instance_deactivate_object(id);
		}
	}
	
	instance_deactivate_object(obj_wall);
	//logga(string(instance_number(obj_statictile)));
	// Enable what's in view and objects that always need to stay active. Here too, interactables are different
	instance_activate_object(obj_wildlife_mail_pidgeon);
	instance_activate_object(obj_interactable_mailbox);
	instance_activate_region(viewx, viewy, width, height, true);
	if(room == roomFarm)
	{
		for(var i = 0; i < instance_number(obj_interactable); i++)
		{
			var interactable = instance_find(obj_interactable,i);
			if(interactable.x > viewx && interactable.x < viewx + width && interactable.y > viewy && interactable.y < viewy + height)
			{
				interactable.canBeDrawn = true;
			}
		}	
	}
	with (obj_interactable)
	{
		if (wasdisabled)
		{
			if (other.timestamp - lasttimestamp > 1 && growcrono > 1)
			{
				growcrono = clamp(growcrono - ((other.timestamp - lasttimestamp) / (1.0/60.0)), 1, 99999999);
			}
			wasdisabled = false;
		}
	}
	culltimer = 20;
}