/// @description Zoom
switch (zoom)
{
	case 0: 
		zoom = 1;
		camera_set_view_size(view_camera[0],480/2,270/2);
		culltimer = 0;
		break;
	case 1:
		zoom = 0;
		camera_set_view_size(view_camera[0],480,270);
		with(obj_interactable)
		{
			canBeDrawn = true;
		}
		break;
}