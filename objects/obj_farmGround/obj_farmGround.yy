{
    "id": "c2315833-a851-4ea1-b7be-2e7563536308",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_farmGround",
    "eventList": [
        {
            "id": "99b3cdb1-9890-450a-97e7-b7146faea5f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c2315833-a851-4ea1-b7be-2e7563536308"
        },
        {
            "id": "b6221ca5-a942-4768-92c1-0f305e0845fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c2315833-a851-4ea1-b7be-2e7563536308"
        },
        {
            "id": "28ed12d9-84b3-485b-9e78-4c5f9b0b9c77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c2315833-a851-4ea1-b7be-2e7563536308"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}