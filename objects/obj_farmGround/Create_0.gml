/// @description Init/load objects
cols = global.farmCols;
rows = global.farmRows;
border = 6;
waterstep = 0;

room_set_width(room, cols*16);
room_set_height(room, rows*16);

// Ground grids
grid = ds_grid_create(cols,rows);		// groundType enum					GRID NO 2
spritegrid = ds_grid_create(cols,rows);	// sprite_index of the single tiles GRID NO 3
indexgrid = ds_grid_create(cols,rows);	// image_index of the single tiles	GRID NO 4
storeygrid = ds_grid_create(cols,rows);	// storey value of the single tiles GRID NO 5

// Other grids
gridaccess = ds_grid_create(cols,rows);	// tiles accessibili (1) oppure no (-1)	GRID NO 6
itemsgrid = ds_grid_create(cols,rows);	// items snappati alla griglia, default -1	GRID NO 7
wallsgrid = ds_grid_create(cols,rows);	// walls nella griglia, default -1	GRID NO 8
pathgrid = mp_grid_create(0,0,cols,rows,16,16);	// griglia per pathfinding
limitgrid = ds_grid_create(ceil(cols/2),ceil(rows/2));	// griglia tileset map limits, diviso 2 perché è a 32px	GRID NO 9
/*
show_debug_message("-----------------------------------GRID CHECK FARMGROUND");
ds_grid_set(grid,-1,-1,-1);
ds_grid_set(spritegrid,-1,-1,-1);
ds_grid_set(indexgrid,-1,-1,-1);
ds_grid_get(storeygrid,-1,-1);

ds_grid_set(gridaccess,-1,-1,-1);
ds_grid_set(itemsgrid,-1,-1,-1);
ds_grid_set(wallsgrid,-1,-1,-1);
ds_grid_set(limitgrid,-1,-1,-1);
show_debug_message("-----------------------------------");
*/
templock_x = ds_list_create();
templock_y = ds_list_create();

var r = 0;
var c = 0;

//fill access grid with 1 (all accessible) and storey grid with 0
repeat (cols)
{
	r = 0;
	repeat (rows)
	{
		ds_grid_set(gridaccess, c, r, 1);
		ds_grid_set(storeygrid, c, r, 0);
		r++;
	}
	c++;
}
if(string_length(global.othergridstring) > 0)
{
	logga("reading OTHER saved ground string");
	
	ds_grid_read(grid, global.othergridstring);
	//ds_grid_read(itemsgrid, global.itemsgridstring);
	
	c = 0;
	repeat(cols)
	{
		r = 0;
		repeat(rows)
		{
			//var tile = instance_create_depth(c,r,0,obj_groundtile);
			//ds_grid_set(tilegrid, c, r, tile);
			ds_grid_set(itemsgrid, c, r, -1);
			r++;
		}
		c++;
	}
}
else if(string_length(global.gridstring) > 0)
{
	logga("reading saved ground string");
	ds_grid_read(grid, global.gridstring);
	//ds_grid_read(itemsgrid, global.itemsgridstring);
	
	c = 0;
	repeat(cols)
	{
		r = 0;
		repeat(rows)
		{
			//var tile = instance_create_depth(c,r,0,obj_groundtile);
			//ds_grid_set(tilegrid, c, r, tile);
			ds_grid_set(itemsgrid, c, r, -1);
			r++;
		}
		c++;
	}
}
else
{
	logga("creating a new map");
	
	// create outer border
	c = 0;
	repeat(cols)
	{
		r = 0;	
		repeat(rows)
		{
			if(r < border || c < border || r > rows - border || c > cols - border)
			{
				ds_grid_set(grid, c, r, GroundType.TallGrass);
				ds_grid_set(gridaccess, c, r, -1);
			}
			else if(r < 9 || c < 9 || r >= rows - 9 || c >= cols - 9)
			{
				ds_grid_set(grid, c, r, GroundType.TallGrass);
			}
			else if(r < 10 || c < 10 || r >= rows - 10 || c >= cols - 10)
			{
				ds_grid_set(grid, c, r, choose(GroundType.TallGrass, GroundType.Grass));
			}
			else if(r < 11 || c < 11 || r >= rows - 11 || c >= cols - 11)
			{
				ds_grid_set(grid, c, r, GroundType.Grass);
			}
			else
			{
				ds_grid_set(grid, c, r, GroundType.Grass);
			}			
			
			//var tile = instance_create_depth(c,r,0,obj_groundtile);
			//ds_grid_set(tilegrid, c, r, tile);
			ds_grid_set(itemsgrid, c, r, -1);
			r++;
		}
		c++;
	}

	logga("ground created");
	
	// prepare isles
	var minsize = 4;
	var randomsize = 6;
	var minoffsety = 1;
	var randomoffsety = 2;
	var minoffsetx = 2;
	var randomoffsetx = 3;
	var px = 0;
	var py = 0;
	var posx = round(((cols-(border*2)) / 2.0) / 2.0);
	var posy = round(((rows-(border*2)) / 2.0) / 2.0);
	var offx = 0;
	var offy = 0;
	var count = 0;
	var grounds = ds_list_create();
	ds_list_add(grounds,GroundType.Grass);
	ds_list_add(grounds,GroundType.Soil);
	ds_list_add(grounds,GroundType.Soil);
	ds_list_add(grounds,GroundType.TallGrass);
	
	// Create the 4 isles (grass isle is skipped)
	repeat(4)
	{
		var ind = floor(random(ds_list_size(grounds)));
		var gt = ds_list_find_value(grounds,ind);
		ds_list_delete(grounds,ind);
	
		var islew = minsize + random(randomsize);
		var isleh = minsize + random(randomsize);
		switch (count)
		{
			case 0: px = border+posx; py = border+posy; break;
			case 1: px = round(cols/2)+posx; py = border+posy; break;
			case 2: px = border+posx; py = round(rows/2)+posy; break;
			case 3: px = round(cols/2)+posx; py = round(rows/2)+posy; break;
		}
		if (gt != GroundType.Grass)
		{
			scr_createIsle(id, px, py, islew, isleh, GroundType.Grass, gt);
			repeat(4)
			{
				islew = minsize + random(randomsize);
				isleh = minsize + random(randomsize);
				if (random(1.0) > 0.5) {offx = minoffsetx+random(randomoffsetx);} else {offx = -minoffsetx-random(randomoffsetx);}
				if (random(1.0) > 0.5) {offy = minoffsety+random(randomoffsety);} else {offy = -minoffsety-random(randomoffsety);}
				scr_createIsle(id, px+offx, py+offy, islew, isleh, GroundType.Grass, gt);
			}
		}
		count++;
	}
	
	logga("isles created");
	
	// create roads from market to center (temporarily make these tiles unaccessible)
	var c = 0;
	var r = 0;
	repeat(4)
	{
		r = 0;
		repeat(rows/2)
		{
			switch (c)
			{
				case 0: ds_grid_set(grid, (cols/2)-1+c, r, GroundType.Grass); break;
				case 1: ds_grid_set(grid, (cols/2)-1+c, r, choose(GroundType.Grass, GroundType.Soil, GroundType.Soil, GroundType.Soil, GroundType.Soil)); break;
				case 2: ds_grid_set(grid, (cols/2)-1+c, r, choose(GroundType.Grass, GroundType.Soil, GroundType.Soil, GroundType.Soil, GroundType.Soil)); break;
				case 3: ds_grid_set(grid, (cols/2)-1+c, r, GroundType.Grass); break;
			}
			if (ds_grid_get(gridaccess, (cols/2)-1+c, r) == 1)
			{
				ds_grid_set(gridaccess, (cols/2)-1+c, r, -1);
				ds_list_add(templock_x,(cols/2)-1+c);
				ds_list_add(templock_y,r);
			}
			r++;
		}
		c++;
	}
	
	// create roads from wild (temporarily make these tiles unaccessible)
	var c = cols/2;
	var r = 0;
	repeat(cols/2)
	{
		r = 0;
		repeat(4)
		{
			switch (r)
			{
				case 0: if (c > (cols/2)+3) {ds_grid_set(grid, c, (rows/2)-1+r, GroundType.Grass);} break;
				case 1: ds_grid_set(grid, c, (rows/2)-1+r, choose(GroundType.Grass, GroundType.Soil, GroundType.Soil, GroundType.Soil)); break;
				case 2: ds_grid_set(grid, c, (rows/2)-1+r, choose(GroundType.Grass, GroundType.Soil, GroundType.Soil, GroundType.Soil)); break;
				case 3: ds_grid_set(grid, c, (rows/2)-1+r, GroundType.Grass); break;
			}
			if (ds_grid_get(gridaccess, c, (rows/2)-1+r) == 1)
			{
				ds_grid_set(gridaccess, c, (rows/2)-1+r, -1);
				ds_list_add(templock_x,c);
				ds_list_add(templock_y,(rows/2)-1+r);
			}
			r++;
		}
		c++;
	}	
	
	scr_storeGround();
	/*
	if (global.online)
	{
		sendFarmString(global.userid, ds_grid_write(grid));
	}
	*/
}
/*
if(string_length(global.itemsgridstring) > 0)
{
	show_debug_message("reading saved items grid string");
	logga("reading saved items grid string");
	ds_grid_read(itemsgrid, global.itemsgridstring);
}
*/

scr_createMapLimits(spr_limit_grass,ceil(border/2),false,true,true,false);
scr_findLimitSprites();
scr_updateTileSprites();

with(obj_water)
{
	event_perform(ev_other,ev_user0);
}

global.gridstring = scr_compressFarmString(grid);
global.itemsgridstring = ds_grid_write(itemsgrid);

instance_create_depth(0,0,0,obj_drawGround);
instance_create_depth(0,0,0,obj_drawWater); 
instance_create_depth(0,0,0,obj_drawUnderwater);

logga("farmground finish");