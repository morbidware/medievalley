/// @description Water step
waterstep += 0.05 * global.dt;
if (waterstep > 16)
{
	waterstep = 0;
}
event_inherited();

