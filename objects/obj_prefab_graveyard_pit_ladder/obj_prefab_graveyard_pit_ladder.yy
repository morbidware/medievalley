{
    "id": "0368738a-d5af-4cf7-b61f-09c8d5cc4f00",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_pit_ladder",
    "eventList": [
        {
            "id": "f3f3ea4a-d38f-4221-bce6-5ba3549f8275",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0368738a-d5af-4cf7-b61f-09c8d5cc4f00"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4c495df-4e1b-4304-9242-88495c7e6596",
    "visible": true
}