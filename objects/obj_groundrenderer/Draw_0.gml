i = 0;

repeat (size)
{
	var spr = tilesprite[i];
	if(spr != -1)
	{
		draw_sprite(spr,tileindex[i],tilex[i]*16 + 8,tiley[i]*16 + 8);
	}

	// debug - indagare sul perché le linee disegnate dal draw_rectangle sono ad 1px su schermo e non ad 1px su grafica
	//draw_set_color(c_white);
	//draw_rectangle(tilex[i]*16,tiley[i]*16,tilex[i]*16 + 16,tiley[i]*16 + 16,true);

	i++;
}

