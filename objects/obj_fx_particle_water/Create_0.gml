event_inherited();

system = part_system_create();
part_system_depth(system,-y-6);

particle = part_type_create();
part_type_sprite(particle,spr_particle_water,false,false,true);
part_type_orientation(particle,0,360,random(20)-10,0,0);
part_type_alpha1(particle,0.4);
part_type_direction(particle,0,360,0,0);
part_type_speed(particle,0.05,0.1,0,0);
part_type_life(particle,5,10);

emitter = part_emitter_create(system);
part_emitter_region(system,emitter,x-5,x+5,y-5,y+5,ps_shape_rectangle,ps_distr_linear);

alarm_set(0,40);