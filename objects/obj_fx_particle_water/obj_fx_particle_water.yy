{
    "id": "de86ac68-c1a3-4a12-8141-085299530e7a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_particle_water",
    "eventList": [
        {
            "id": "05bb19d8-1880-4532-8c4b-810863862fd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "de86ac68-c1a3-4a12-8141-085299530e7a"
        },
        {
            "id": "969e53f8-f768-4d78-b3c4-c70770a56bce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "de86ac68-c1a3-4a12-8141-085299530e7a"
        },
        {
            "id": "a6d5404e-5134-442b-8641-fdc50579badd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "de86ac68-c1a3-4a12-8141-085299530e7a"
        },
        {
            "id": "d1feca4b-7e4f-44e3-acc8-064b97c1158f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "de86ac68-c1a3-4a12-8141-085299530e7a"
        },
        {
            "id": "0a4cf64d-b716-4f94-be19-684f731db966",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "de86ac68-c1a3-4a12-8141-085299530e7a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d28c31b4-f583-4c24-93fe-90b48cdaafe6",
    "visible": true
}