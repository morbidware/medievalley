if (room == roomMain || room == roomChallenge)
{
	exit;
}

var sx = 10;
var sy = 24;

// Username
draw_set_font(global.FontStandardBoldOutline);
draw_set_valign(fa_top);
draw_set_halign(fa_left);
draw_set_color(c_white);
draw_text(10, 10, scr_fix_text(global.username));
var width1 = string_width(scr_fix_text(global.username)) + 10;

// Online informations
draw_set_color(c_yellow);
if (string(global.otherusername) != "")
{
	draw_set_font(global.FontStandardOutline);
	draw_text(width1, 10, " - You are visiting ");
	var width2 = string_width(" - You are visiting ");
	
	draw_set_font(global.FontStandardBoldOutline);
	draw_text(width1 + width2, 10, scr_fix_text(global.otherusername));
	var width3 = string_width(scr_fix_text(global.otherusername));

	draw_set_font(global.FontStandardOutline);
	draw_text(width1 + width2 + width3, 10, "'s Farm");
}
else if (instance_number(obj_dummy) == 1 && room == roomFarm)
{
	var name = instance_find(obj_dummy,0).name;
	draw_set_font(global.FontStandardBoldOutline);
	draw_text(width1, 10, " - "+scr_fix_text(name));
	var width2 = string_width(" - "+scr_fix_text(name));
	draw_set_font(global.FontStandardOutline);
	draw_text(width1 + width2 , 10, " is visiting your farm");
}
else if (instance_number(obj_dummy) > 1 && room == roomFarm)
{
	draw_set_font(global.FontStandardOutline);
	draw_text(width1 , 10, "- Some players are visiting your Farm");
}
else if (global.visitorscount > 0 && room != roomFarm)
{
	draw_set_font(global.FontStandardOutline);
	draw_text(width1 , 10, "- Someone is visiting your Farm");
}
draw_set_color(c_white);

// Save status
draw_set_font(global.FontStandardOutline);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_color(make_color_rgb(180,180,180));
draw_set_alpha(1);
if (!global.online)
{
	str = "You are offline!";
	draw_text(sx,sy,str);
	exit;
}

if (saving)
{
	str = "Saving...";
	draw_text(sx,sy,str);
	exit;
}
global.dateB = date_current_datetime();
var secs = date_second_span(global.dateA, global.dateB); 
var mins = floor(secs/60);
str = "Last save: " + string(secs) + " seconds ago";
if(mins == 1)
{
	str = "Last save: " + string(mins) + " minute ago";
}
else if (mins > 1)
{
	str = "Last save: " + string(mins) + " minutes ago";
}
draw_text(sx,sy,str);
draw_set_color(c_white);
draw_set_alpha(1);
