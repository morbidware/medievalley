x = 0;
y = 0;
depth = -1001;

//Exit Fullscreen
btnExitFullScreen = instance_create_depth(0, 0, depth - 1,obj_btn_generic_persistent);
btnExitFullScreen.sprite_index = spr_btn_exit_fullscreen;
btnExitFullScreen.caller = id;
btnExitFullScreen.evt = ev_user0;
btnExitFullScreen.text = "Exit fullscreen";
btnExitFullScreen.textoy = 2;