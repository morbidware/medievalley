{
    "id": "b46c80c8-ddbb-4094-8efb-4f91ec4f5b20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_fullscreen",
    "eventList": [
        {
            "id": "f97ad6ac-b67b-4d31-9d27-116c84b2f9e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b46c80c8-ddbb-4094-8efb-4f91ec4f5b20"
        },
        {
            "id": "27539bdc-292e-40a7-915b-ccd0fdbfe156",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b46c80c8-ddbb-4094-8efb-4f91ec4f5b20"
        },
        {
            "id": "07d3e47e-7947-486d-8d7e-27a42167ad74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b46c80c8-ddbb-4094-8efb-4f91ec4f5b20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}