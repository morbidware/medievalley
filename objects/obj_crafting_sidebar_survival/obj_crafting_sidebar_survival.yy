{
    "id": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_sidebar_survival",
    "eventList": [
        {
            "id": "bb0d0a12-5829-4417-8641-0880727aec02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        },
        {
            "id": "9f04bad3-2de3-49a2-94f3-6b0fb21ee55a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        },
        {
            "id": "4f1b1054-3004-4dda-9239-7af6f56a6c2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        },
        {
            "id": "fc94843b-1eaf-4a17-a5eb-bba875e10c12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        },
        {
            "id": "95ed8176-9828-4f5b-a8c8-aae8fd96e899",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        },
        {
            "id": "f76ae5f1-6bd6-47fd-9b45-a267fe899f07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        },
        {
            "id": "60106846-cfb6-45ab-a46e-6b8e694a17d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "3b564fe6-c2bf-4124-8bc9-3dc3cad537a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5aeffe4c-b3fc-4433-acea-c39b0feec58f",
    "visible": true
}