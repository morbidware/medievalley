/// @description Insert description here
// You can write your code in this editor

// keep dock hidden as long as the intro is active
with(obj_gameLevelFarm)
{
	if (onIntro)
	{
		other.x = -70;
	}
}
if (ds_map_find_value(global.unlockables, "ui_craft") < 1 || room == roomChallenge)
{
	x = -70;
}
x += global.dt;
x = clamp(x,-70,0);

if(global.lockHUD)
{
	exit;
}

var mx = mouse_get_relative_x();
var my = mouse_get_relative_y();
var sw = sprite_get_width(sprite_index);
var sh = sprite_get_height(sprite_index);

//detect if recipe list is open and slide it
isOpen = false;
for(var i = 0; i < ds_list_size(cats); i++)
{
	var cct = ds_list_find_value(cats,i);
	if(!is_undefined(cct))
	{
		if(cct.state == 1)
		{
			isOpen = true;
			//consider total sprite size for mouseover
			sw += sprite_get_width(listSprite);
			break;
		}
	}
}
if (isOpen)
{
	listx += 4;
}
else
{
	listx -= 4;
}
listx = x + clamp(listx,6,sprite_get_width(sprite_index)-4);
listObject.x = listx;
listObject.y = y;

var rein = noone;
rein = instance_find(obj_recipeInfo,0);

//when mouse is over crafting ui, manage recipe ui
if(global.mouseOverCrafting)
{
	if(position_meeting(mx,my,obj_recipe))
	{
		if(!instance_exists(obj_recipeInfo))
		{
			var recipe = instance_nearest(mx,my,obj_recipe);
			var rein = instance_create_depth(0,0,0,obj_recipeInfo);
			rein.recipe = recipe;
		}
		else
		{
			var rein = instance_find(obj_recipeInfo,0);
			var recipe = instance_nearest(mx,my,obj_recipe);
			rein.recipe = recipe;
		}
	}
	else
	{
		if(instance_exists(obj_recipeInfo))
		{
			with(obj_recipeInfo)
			{
				instance_destroy();
			}
		}
	}
}
else
{
	if(instance_exists(obj_recipeInfo))
	{
		with(obj_recipeInfo)
		{
			instance_destroy();
		}
	}
}