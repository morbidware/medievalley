/// @description Insert description here
// You can write your code in this editor
if(instance_number(obj_crafting_cat) > 0)
{
	logga("DUPLICATED CRAFTING SIDEBAR!");
	instance_destroy();
	exit;
}

x = 0;
y = 64;
depth = -200;

isOpen = false;
listx = x;

cats = ds_list_create();
///////////////////////////////////////////////////////////////
var tools = instance_create_depth(0,0,-3,obj_crafting_cat_tools);
tools.index = 0;
tools.sidebar = id;
/////////////////////
ds_list_add(cats, tools);

if(global.isSurvival)
{
	///////////////////////////////////////////////////////////////
	survival = instance_create_depth(0,0,-3,obj_crafting_cat_survival);
	survival.index = 1;
	survival.sidebar = id;
	/////////////////////
	ds_list_add(cats, survival);
	
	///////////////////////////////////////////////////////////////
	refined = instance_create_depth(0,0,-3,obj_crafting_cat_refined);
	refined.index = 2;
	refined.sidebar = id;
	/////////////////////
	ds_list_add(cats, refined);
	
	///////////////////////////////////////////////////////////////
	furniture = instance_create_depth(0,0,-3,obj_crafting_cat_furniture);
	furniture.index = 3;
	furniture.sidebar = id;
	/////////////////////
	ds_list_add(cats, furniture);
	
	///////////////////////////////////////////////////////////////
	dress = instance_create_depth(0,0,-3,obj_crafting_cat_dress);
	dress.index = 4;
	dress.sidebar = id;
	/////////////////////
	ds_list_add(cats, dress);
	
	///////////////////////////////////////////////////////////////
	weapons = instance_create_depth(0,0,-3,obj_crafting_cat_weapons);
	weapons.index = 5;
	weapons.sidebar = id;
	/////////////////////
	ds_list_add(cats, weapons);
}
else
{
	///////////////////////////////////////////////////////////////
	var structures = instance_create_depth(0,0,-3,obj_crafting_cat_structures);
	structures.index = 1;
	structures.sidebar = id;
	/////////////////////
	ds_list_add(cats, structures);
	
	///////////////////////////////////////////////////////////////
	var anvil = instance_create_depth(0,0,-3,obj_crafting_cat_forge);
	anvil.index = 2;
	anvil.sidebar = id;
	anvil.neededStructure = obj_interactable_forge;
	/////////////////////
	ds_list_add(cats, anvil);
	
	///////////////////////////////////////////////////////////////
	var food = instance_create_depth(0,0,-3,obj_crafting_cat_food);
	food.index = 3;
	food.sidebar = id;
	food.neededStructure = obj_interactable_kitchen;
	/////////////////////
	ds_list_add(cats, food);

	///////////////////////////////////////////////////////////////
	var potions = instance_create_depth(0,0,-3,obj_crafting_cat_potions);
	potions.index = 4;
	potions.sidebar = id;
	potions.neededStructure = obj_interactable_alchemytable;
	/////////////////////
	ds_list_add(cats, potions);

	///////////////////////////////////////////////////////////////
	var grindstone = instance_create_depth(0,0,-3,obj_crafting_cat_grindstone);
	grindstone.index = 5;
	grindstone.sidebar = id;
	grindstone.neededStructure = obj_interactable_grindstone;
	/////////////////////
	ds_list_add(cats, grindstone);
}


///////////////////////////////////////////////////////////////
btnUp = instance_create_depth(0,0,-1,obj_craft_btn_up);
btnUp.sidebar = id;

btnDown = instance_create_depth(0,0,-1,obj_craft_btn_down);
btnDown.sidebar = id;

listObject = instance_create_depth(-100,-100,10,obj_crafting_list_survival);
listSprite = spr_crafting_list_survival_bg;

over = false;
overrecipe = false;