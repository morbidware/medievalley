{
    "id": "82346316-01a8-4021-a857-c5dc936f26ea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_smoke",
    "eventList": [
        {
            "id": "a8f0af9c-675e-4f3d-ac64-d996661dd3bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82346316-01a8-4021-a857-c5dc936f26ea"
        },
        {
            "id": "bcf486bf-2ae9-4063-93ff-6ec26833ddef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "82346316-01a8-4021-a857-c5dc936f26ea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}