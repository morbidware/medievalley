var pi2 = pi*2.0;
angle += 0.01 * global.dt;
if(angle > pi2)
{
	angle -= pi2;
}
x += sin(angle)*0.12;
y -= 0.5 * global.dt;
if(state == 0)
{
	if(image_xscale < 1.0)
	{
		image_xscale += 0.05;
		image_yscale = image_xscale;
		image_alpha = image_xscale;
	}
	else
	{
		state = 1;
	}
}
else if(state == 1)
{
	if(image_alpha > 0.0)
	{
		image_xscale += 0.02;
		image_yscale = image_xscale;
		image_alpha -= 0.02;
	}
	else
	{
		instance_destroy();
	}
}