/// @description Trigger animation
if (startx < 0)
{
	startx = x;
}
if (starty < 0)
{
	starty = y;
}

x = startx + random(64)-32;
y = starty + random(64)-32;

image_alpha = 1;
image_index = 0;
image_speed = 0.4;