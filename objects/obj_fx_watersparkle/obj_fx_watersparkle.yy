{
    "id": "b861b68f-15c4-421b-87f9-a26fe6413312",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_watersparkle",
    "eventList": [
        {
            "id": "e092221c-747b-46a2-bc23-3eac47298803",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b861b68f-15c4-421b-87f9-a26fe6413312"
        },
        {
            "id": "ac305f71-d96e-4fb4-b157-3442237f5646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b861b68f-15c4-421b-87f9-a26fe6413312"
        },
        {
            "id": "dc1160f8-71a6-4cf2-9d0d-a3c599981db0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "b861b68f-15c4-421b-87f9-a26fe6413312"
        },
        {
            "id": "1991321a-02d4-46ef-8f82-7215744fe628",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b861b68f-15c4-421b-87f9-a26fe6413312"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "463036db-61c7-4679-a3aa-9acc5eb31150",
    "visible": true
}