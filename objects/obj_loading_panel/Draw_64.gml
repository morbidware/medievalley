draw_set_alpha(1);
draw_set_color(c_white);

x = display_get_gui_width()/2;

scr_nine_slice_at(spr_9slice_summary_bg, x, y, 220, 100, 2);

draw_set_font(global.FontStandardBold);
draw_set_halign(fa_left);
draw_set_valign(fa_middle);

draw_set_color(c_black);
draw_set_alpha(0.8);
draw_text(x-44, y, "Please wait"+pointstring);
pointtimer -= global.dt;
if (pointtimer < 0)
{
    pointtimer = 20;
    pointstring += ".";
    if (string_length(pointstring) > 3)
    {
        pointstring = "";
    }
}
draw_set_alpha(1);
draw_set_color(c_white);