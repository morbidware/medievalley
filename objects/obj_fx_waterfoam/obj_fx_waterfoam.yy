{
    "id": "ca1447bf-2251-4096-95ca-6fdc699ccc43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_waterfoam",
    "eventList": [
        {
            "id": "6a837458-b92f-4a32-8030-0dbe94bbce73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca1447bf-2251-4096-95ca-6fdc699ccc43"
        },
        {
            "id": "9d1b8f92-7471-4d8b-964a-8d47ee25c4d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ca1447bf-2251-4096-95ca-6fdc699ccc43"
        },
        {
            "id": "2e692ec8-6e6f-454e-84e6-f452682fab07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca1447bf-2251-4096-95ca-6fdc699ccc43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dd30d206-2eb4-4d11-812b-ec792eab2e7c",
    "visible": true
}