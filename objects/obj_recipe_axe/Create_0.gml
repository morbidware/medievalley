/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",2);
ds_map_add(ingredients,"flint",1);

resitemid = "axe";
unlocked = true;
desc = "Usable to chop down trees.";