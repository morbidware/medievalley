{
    "id": "6a800586-8609-4968-af0a-ede798bde33f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_sweat",
    "eventList": [
        {
            "id": "62b3c1c0-76a5-443e-a769-269520b6fc34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6a800586-8609-4968-af0a-ede798bde33f"
        },
        {
            "id": "ee1b80e8-c360-4852-b0a0-23d08364a64c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6a800586-8609-4968-af0a-ede798bde33f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "341a4cca-65af-4837-8376-f04d87cab6dc",
    "visible": true
}