if (!instance_exists(pla))
{
	instance_destroy();
}
ox += cos(angle)*0.4*global.dt;
oy -= sin(angle)*0.4*global.dt;
x = pla.x + ox;
y = pla.y + oy;
depth = pla.depth - 2;
image_angle = radtodeg(angle);
crono += global.dt;
if(crono <  30)
{
	if(image_alpha < 1.0)
	{
		image_alpha += 0.1*global.dt;
	}
}
else
{
	if(image_alpha > 0.0)
	{
		image_alpha -= 0.1*global.dt;
		if(image_alpha <= 0.0)
		{
			instance_destroy();
		}
	}
}
