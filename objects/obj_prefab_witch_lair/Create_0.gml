/// @description Init
event_inherited();

// Generate child prefabs
with (instance_create_depth(x,y,depth,obj_prefab_witch_lair_entrance)) { parent = other.id; }
with (instance_create_depth(x,y,depth,obj_prefab_witch_lair_rocks)) { parent = other.id; }
