/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(wear.slot != global.inv_head && wear.wearable == 1){
	scr_selectWardrobePart(0,false,"head");
	ds_list_set(global.currentwardrobe,2,player.custom_head_index);
	player.defence -= wear.defence;
	instance_destroy(id);
}
if(wear.slot != global.inv_torso && wear.wearable == 2){
	scr_selectWardrobePart(0,false,"upper");
	ds_list_set(global.currentwardrobe,2,player.custom_upper_index);
	player.defence -= wear.defence;
	instance_destroy(id);
}

