{
    "id": "cf9e339c-65bf-4863-be20-2cc3fa6743de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wearable",
    "eventList": [
        {
            "id": "3c94de1b-f936-4919-86e4-05670fcfdfdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf9e339c-65bf-4863-be20-2cc3fa6743de"
        },
        {
            "id": "e1886ae1-6a26-4f80-bb47-d0feab98ddd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "cf9e339c-65bf-4863-be20-2cc3fa6743de"
        },
        {
            "id": "99c8c9dd-3dde-4762-835e-97533a83d534",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cf9e339c-65bf-4863-be20-2cc3fa6743de"
        },
        {
            "id": "cfc95231-bd98-44c7-a6a8-63bdf8a04e74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cf9e339c-65bf-4863-be20-2cc3fa6743de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cf8deb36-79b4-4043-940c-ca9d662edda0",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}