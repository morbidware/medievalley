if(ground == noone)
{
	ground = instance_find(obj_baseGround,0);
	return;
}

storey = ds_grid_get(ground.storeygrid, clamp(floor(x/16),0,ground.cols-1), clamp(floor(y/16),0,ground.rows-1));

if(state == 1)
{
	image_alpha -= 0.01 * global.dt;
	if(image_alpha <= 0.0)
	{
		instance_destroy();
	}
}

if(global.dayprogress > aliveto || global.dayprogress < alivefrom)
{
	if(state == 0)
	{
		state = 1;
	}
}