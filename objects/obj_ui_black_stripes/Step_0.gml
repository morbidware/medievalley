/// @description Insert description here
// You can write your code in this editor
var char = instance_find(obj_char,0);
if(char != noone)
{
	if(char.controlType == CharControlType.manual)
	{
		if(state > 0)
		{
			state -= 1.0/room_speed;
		}
		else
		{
			state = 0;
		}
	}
	else
	{
		if(state < 0)
		{
			state += 1.0/room_speed;
		}
		else
		{
			state = 1;
		}
	}
}