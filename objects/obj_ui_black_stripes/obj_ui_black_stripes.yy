{
    "id": "cfe26f96-e402-454a-885d-5b7ef052c51d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_black_stripes",
    "eventList": [
        {
            "id": "06f9ff07-45ed-4f32-ad56-21d291e0ae70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cfe26f96-e402-454a-885d-5b7ef052c51d"
        },
        {
            "id": "187ba559-b8e6-474b-bc85-2bc4bc6f1353",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cfe26f96-e402-454a-885d-5b7ef052c51d"
        },
        {
            "id": "96f849ba-42ad-4ea9-addb-f5b2886928ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cfe26f96-e402-454a-885d-5b7ef052c51d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}