/// @description Init

application_surface_draw_enable(false);
dayColor = c_white;
intensity = 0; // intensity of point lights; decreases to 0 during day

event_perform(ev_other,ev_user0);

canvaswidth = room_width;
canvasheight = room_height;

color_sunrise = make_color_rgb(185,117,213);
color_midday = make_color_rgb(255,255,255);
color_day = make_color_rgb(210,210,220);
color_sunset = make_color_rgb(255,160,75);
//color_night = make_color_rgb(68,68,234);
color_night = make_color_rgb(50,50,244); // more blue

color_1 = color_night;
color_2 = color_sunrise;

resynctimer = 1;

lightblend = 0;
global.dayprogress = 0;
global.shadowangle = 0;
global.shadowsinmod = 0;
global.shadowcosmod = 0;
global.shadowalpha = 0;
global.blobalpha = 0;

enum DayPhase
{
	None,
	Sunrise,
	Morning,
	Afternoon,
	Evening,
	Night,
	MidNight,
}

global.dayPhase = DayPhase.None;

w = scr_adaptive_x(480);
h = scr_adaptive_y(270);
surf = surface_create(w,h);
surf2 = surface_create(w,h);