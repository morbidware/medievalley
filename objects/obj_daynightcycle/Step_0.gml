/// @description Progress time, fx timing

// Resync game time often to avoid time drifting on the long run (it happens!)
if (os_browser != browser_not_a_browser)
{
	resynctimer -= global.dt;
	if (resynctimer <= 0)
	{
		event_perform(ev_other,ev_user0);
		resynctimer = 300; // in frames at 60 fps
	}
}

if (m_forceDay)
{
	global.dayprogress = 0.25; // Midday
}

// Time progress
if(room != roomChallenge)
{
	global.dayprogress += ((1 / m_dayLength) * (global.dt / 60)) / 60;
	if (global.dayprogress >= 1)
	{
		global.dayprogress -= 1;
	}
}

// Change day color
if (global.dayprogress >= 0 &&  global.dayprogress < 0.1) // NIGHT TO SUNRISE
{
	color_1 = color_night;
	color_2 = color_sunrise;
	lightblend = global.dayprogress * 10;
}
else if (global.dayprogress >= 0.1 &&  global.dayprogress < 0.2) // SUNRISE TO MID DAY
{
	color_1 = color_sunrise;
	color_2 = color_midday;
	lightblend = (global.dayprogress - 0.1) * 10;
}
else if (global.dayprogress >= 0.2 &&  global.dayprogress < 0.3) // MID DAY TO DAY
{
	color_1 = color_midday;
	color_2 = color_day;
	lightblend = (global.dayprogress - 0.2) * 10;
}
else if (global.dayprogress >= 0.3 &&  global.dayprogress < 0.5) // FIXED DAY
{
	color_1 = color_day;
	color_2 = color_day;
	lightblend = 0;
}
else if (global.dayprogress >= 0.5 &&  global.dayprogress < 0.6) // DAY TO SUNSET
{
	color_1 = color_day;
	color_2 = color_sunset;
	lightblend = (global.dayprogress - 0.5) * 10;
}
else if (global.dayprogress >= 0.6 &&  global.dayprogress < 0.65) // FIXED SUNSET
{
	color_1 = color_sunset;
	color_2 = color_sunset;
	lightblend = 0;
}
else if (global.dayprogress >= 0.65 &&  global.dayprogress < 0.75) // SUNSET TO NIGHT
{
	color_1 = color_sunset;
	color_2 = color_night;
	lightblend = (global.dayprogress - 0.65) * 10;
}
else if (global.dayprogress >= 0.75 &&  global.dayprogress < 1) // FIXED NIGHT
{
	color_1 = color_night;
	color_2 = color_night;
	lightblend = 0;
}

global.shadowangle = -60 + (global.dayprogress * 250);
global.shadowsin = -sin(degtorad(global.shadowangle));
global.shadowcos = -cos(degtorad(global.shadowangle));

global.shadowlength = global.dayprogress * 1.5;				// Scale to stay in a shorter timeframe
global.shadowlength = abs((global.shadowlength * 2) - 1);	// Make it go from 1 to 0, then back to 1
global.shadowlength *= global.shadowlength;					// Make it proportional
global.shadowlength = 0.4 + (global.shadowlength * 2);		// Apply to final effect

global.shadowalpha = global.dayprogress * 1.35;				// Scale to stay in a shorter timeframe
global.shadowalpha = 1-(abs((global.shadowalpha * 2) - 1));	// Make it go from 0 to 1, then back to 0
global.shadowalpha = clamp(global.shadowalpha * 2,0,0.4)	// Speed up fade, clamp to max

global.blobalpha = (global.dayprogress - 0.05) * 1.35;		// Shift and scale to stay in a shorter timeframe
global.blobalpha = abs((global.blobalpha * 2) - 1);			// Make it go from 1 to 0, then back to 1
global.blobalpha = clamp(global.blobalpha / 2, 0.25, 0.4)	// Slow down fade, clamp to max

global.shadowsinmod = 1; //abs(sin(degtorad(global.shadowangle+180)));
global.shadowcosmod = 1; //(1-abs(cos(degtorad(global.shadowangle+180))));

if (room == roomHouse)
{
	dayColor = c_white;
	intensity = 0;
}
else
{
	dayColor = merge_color(color_1, color_2, clamp(lightblend, 0.0, 1.0));
	// point lights affect lighting from dusk to past sunset, but no during day
	if (global.dayprogress >= 0.5 && global.dayprogress < 0.75)
	{
		intensity = (global.dayprogress - 0.5) * 4;
	}
	else if (global.dayprogress >= 0.75 && global.dayprogress < 1)
	{
		intensity = 1;
	}
	else if (global.dayprogress >= 0 && global.dayprogress <= 0.25)
	{
		intensity = 1 - (global.dayprogress * 4);
	}
	else
	{
		intensity = 0;
	}
}

// enable fxs and wildlife
if(room == roomFarm || room == roomWild || room == roomSurvival)
{	
	if(global.dayprogress >= 0.0 && global.dayprogress < 0.1 && global.dayPhase != DayPhase.Sunrise)
	{
		global.dayPhase = DayPhase.Sunrise;
		with(obj_gameLevelSurvival)
		{
			for(var i = 0; i < instance_number(obj_pickable); i++)
			{
				var inst = instance_find(obj_pickable,i);
				if(inst.name == "flint")
				{
					flintcount++;
				}
			}
			if(flintcount < 16)
			{
				
				repopulate = true;
				flintcount = 16;
			}
			seedcount = 4;
		}
		
		with(obj_nightwolf)
		{
			if(state != MobState.Die)
			{
				var survGround = instance_find(obj_survivalGround,0);
				if(survGround)
				{
					var angle = random(360);
					var condition = false
					while(condition != true)
					{
						var rx = hero.x + lengthdir_x(window_get_width()/2,angle);
						var ry = hero.y + lengthdir_y(window_get_width()/2,angle);
							
						var c = floor(rx/16);
						var r = floor(ry/16);
						c = clamp(c,0,global.survivalcellcolumns * 32);
						r = clamp(r,0,global.survivalcellrows * 32);
						rx = 8 + c * 16;
						ry = 8 + r * 16;
					
						if(c < global.survivalcellcolumns * 32 && r < global.survivalcellrows * 32 )
						{
							var groundtype = ds_grid_get(survGround.grid,round(c),round(r));
							var hasWall = ds_grid_get(survGround.wallsgrid,round(c),round(r));
							if(groundtype != GroundType.Water && groundtype != GroundType.Void && groundtype != GroundType.None)
							{
								if(hasWall == -1)
								{
									condition = true;	
								}
							}
							
						}
						angle += 10;
					}
					targetx = rx;
					targetx = ry;
					scr_setMobState(id, MobState.RunAway);
				}
			}
		}
		
	}
	if(global.dayprogress >= 0.1 && global.dayprogress < 0.25 && global.dayPhase != DayPhase.Morning)
	{	
		global.dayPhase = DayPhase.Morning;
		repeat(4)
		{
			var rx = random(room_width);
			var ry = random(room_height);
			instance_create_depth(rx, ry, -ry, obj_wildlife_butterfly);
		}
	}
	if(global.dayprogress >= 0.25 && global.dayprogress < 0.5 && global.dayPhase != DayPhase.Afternoon)
	{	
		global.dayPhase = DayPhase.Afternoon;
		repeat(4)
		{
			var rx = random(room_width);
			var ry = random(room_height);
			instance_create_depth(rx, ry, -ry, obj_wildlife_butterfly);
		}
	}
	if(global.dayprogress >= 0.5 && global.dayprogress < 0.75 && global.dayPhase != DayPhase.Evening)
	{	
		global.dayPhase = DayPhase.Evening;
	}
	if(global.dayprogress >= 0.75 && global.dayprogress < 0.87 && global.dayPhase != DayPhase.Night)
	{	
		global.dayPhase = DayPhase.Night;
		repeat(8)
		{
			var rx = random(room_width);
			var ry = random(room_height);
			repeat(10)
			{
				instance_create_depth(rx, ry, -ry, obj_wildlife_firefly);
			}
		}
		var firstday = instance_find(obj_gameLevel,0).firstday;
		if(true && global.isSurvival)
		{
			var player = instance_find(obj_char,0);
			var firefly = instance_create_depth(player.x, player.y, -player.y, obj_wildlife_firefly);
			firefly.distToPlayer = 1;
			firefly.player = player;
			player.firefly = firefly;
			firefly.aliveto = 1;
			
		}
			
	}
	if(global.dayprogress >= 0.87 && global.dayprogress < 1.0 && global.dayPhase != DayPhase.MidNight)
	{	
		global.dayPhase = DayPhase.MidNight;
		var player = instance_find(obj_char,0);
		var firstday = instance_find(obj_gameLevel,0).firstday;
		with(obj_wildlife_firefly)
		{
			if(distToPlayer != 0)
			{
				instance_destroy(id);	
			}
		}
		if(player.firefly != obj_wildlife_firefly && global.isSurvival)
		{
			var firefly = instance_create_depth(player.x, player.y-24, -player.y, obj_wildlife_firefly);
			firefly.distToPlayer = 1;
			firefly.player = player;
			player.firefly = firefly;
			firefly.aliveto = 1;	
		}
		
		if(player != noone && m_enableMobs)
		{
			var survGround = instance_find(obj_survivalGround,0);
			if(survGround)
			{
				repeat(3)
				{
					var condition = false;

					var angle = random(360);
					while(condition != true)
					{
						var rx = player.x + lengthdir_x(window_get_width()/2,angle);
						var ry = player.y + lengthdir_y(window_get_width()/2,angle);
						
						var c = floor(rx/16);
						var r = floor(ry/16);
						c = clamp(c,0,global.survivalcellcolumns * 32);
						r = clamp(r,0,global.survivalcellrows * 32);
						rx = 8 + c * 16;
						ry = 8 + r * 16;
						if(c < global.survivalcellcolumns * 32 && r < global.survivalcellrows * 32 )
						{
							var groundtype = ds_grid_get(survGround.grid,c,r);
							var hasWall = ds_grid_get(survGround.wallsgrid,c,r);
							if(groundtype != GroundType.Water && groundtype != GroundType.Void && groundtype != GroundType.None)
							{
								if(hasWall == -1)
								{
									condition = true;	
								}
							}
							
						}
						angle += 10;
					}

					instance_create_depth(rx, ry, -ry, obj_nightwolf);
				}

			}
			
		}
	}
}