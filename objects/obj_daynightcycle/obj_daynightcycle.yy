{
    "id": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_daynightcycle",
    "eventList": [
        {
            "id": "992b938f-0e65-4c2b-8cce-31739dcb674e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        },
        {
            "id": "0ba13895-b3d4-4101-a6ab-f4260e552f67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        },
        {
            "id": "aa883379-5402-4c5c-8996-a2e57674ad73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        },
        {
            "id": "d1932997-8364-4725-8eda-f7e8cb96c0ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        },
        {
            "id": "f0cfee1b-3556-4241-a002-7fbd9c47dcf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        },
        {
            "id": "0d767f2a-768c-4ad1-b3a9-9e508fee93e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        },
        {
            "id": "5729e556-d879-41ce-8e07-243907affbee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "3373d49b-c97e-43b1-b9c4-3cfb17075a0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}