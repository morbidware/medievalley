/// @description Set timestamp
// Extract day time from UNIX time

if (global.skiptimestamp || room == roomChallenge)
{
	exit;
}

timestamp = real(getTimestamp());
//logga(">> UNIX timestamp: "+string(timestamp)+" ms.");

timespeed = (24 * 60) / m_dayLength;
t = floor((timestamp * timespeed) / 1000);
//logga(">> Converted timestamp: "+string(t)+" sec.");

var dt = date_create_datetime(2019,0,0,0,0,t);
//logga(string(date_get_hour(dt))+"h "+string(date_get_minute(dt))+"m "+string(date_get_second(dt))+"s");
global.dayprogress = ((date_get_hour(dt) * 60 * 60) + (date_get_minute(dt) * 60) + date_get_second(dt)) / 86400;
//logga(">> Progress: "+string(global.dayprogress));