/// @description Reset shaders

//gpu_set_colorwriteenable(true,true,true,false);
if(room != roomHouse)
{
	shader_set(shaderDayNightCycle)
	shader_set_uniform_f(shader_get_uniform(shaderDayNightCycle,"u_r"), color_get_red(dayColor) / 255.0);
	shader_set_uniform_f(shader_get_uniform(shaderDayNightCycle,"u_g"), color_get_green(dayColor) / 255.0);
	shader_set_uniform_f(shader_get_uniform(shaderDayNightCycle,"u_b"), color_get_blue(dayColor) / 255.0);
	var t_sampler = shader_get_sampler_index(shaderDayNightCycle, "u_normalColorTex");
	var spr = surface_get_texture(surf);
	texture_set_stage(t_sampler, spr); 
	
	var t_sampler2 = shader_get_sampler_index(shaderDayNightCycle, "u_lightColorTex");
	var spr2 = surface_get_texture(surf2);
	texture_set_stage(t_sampler2, spr2);
}

if (os_browser == browser_not_a_browser)
{
	if (window_get_width() != canvaswidth || window_get_height() != canvasheight)
	{
		canvaswidth = clamp(window_get_width(),16,9999999);
		canvasheight = clamp(window_get_height(),16,9999999);
		surface_resize(application_surface,canvaswidth,canvasheight);
	}
}
if(global.ismobile)
{
	if (os_browser != browser_not_a_browser)
	{
		if (window_get_width() != canvaswidth || window_get_height() != canvasheight)
		{
			canvaswidth = clamp(window_get_width(),16,9999999);
			canvasheight = clamp(window_get_height(),16,9999999);
			surface_resize(application_surface,canvaswidth,canvasheight);
		}
	}
}


/*
draw_set_color(c_black);
draw_rectangle(0,0,window_get_width(),window_get_height(),false);
draw_set_color(c_white);
*/
draw_clear_alpha(c_black,1);
draw_surface(application_surface,0,0);

if(room != roomHouse)
{
	shader_reset();
}