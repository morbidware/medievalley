/// @description Draw light surfaces

if(!surface_exists(surf))
{
	surf = surface_create(w,h);
}

surface_set_target(surf);
draw_set_color(c_black);
draw_set_alpha(1.0);
draw_rectangle(0,0,w,h,false);

gpu_set_fog(true,c_white,0,0);

/*
with(obj_char)
{
	draw_sprite_ext(custom_body_sprite,anim_index, pos_get_relative_x()/2.0,(pos_get_relative_y()/2.0)-16.0,image_xscale,image_yscale,image_angle,c_white,image_alpha);
	draw_sprite_ext(custom_head_sprite,anim_index, pos_get_relative_x()/2.0,(pos_get_relative_y()/2.0)-16.0,image_xscale,image_yscale,image_angle,c_white,image_alpha);
	draw_sprite_ext(custom_lower_sprite,anim_index, pos_get_relative_x()/2.0,(pos_get_relative_y()/2.0)-16.0,image_xscale,image_yscale,image_angle,c_white,image_alpha);
	draw_sprite_ext(custom_upper_sprite,anim_index, pos_get_relative_x()/2.0,(pos_get_relative_y()/2.0)-16.0,image_xscale,image_yscale,image_angle,c_white,image_alpha);
}
*/

// ------------------------------- SETTING NORMAL COLOR TEX
with(obj_wildlife_firefly)
{
	draw_sprite_ext(sprite_index,image_index, floor(pos_get_relative_x()/2.0),floor(pos_get_relative_y()/2.0),image_xscale,image_yscale,image_angle,c_white,image_alpha * other.intensity);
}
with(obj_fox)
{
	draw_sprite_ext(sprite_index,image_index, round(pos_get_relative_x()/2.0),round(pos_get_relative_y()/2.0),image_xscale,image_yscale,image_angle,c_white,image_alpha* other.intensity);
}
with(obj_interactable_bonfire)
{
	if (growstage == 2)
	{
		draw_sprite_ext(sprite_index,image_index, floor(pos_get_relative_x()/2.0),floor(pos_get_relative_y()/2.0),image_xscale,image_yscale,image_angle,c_white,image_alpha* other.intensity);
	}
}
with(obj_emote)
{
	draw_sprite_ext(sprite_index, 0, floor(pos_get_relative_x()/2), floor(pos_get_relative_y()/2), 1, 1, 0, c_white, alpha* other.intensity);
	draw_sprite_ext(emotesprite, 0, floor(pos_get_relative_x()/2), floor(pos_get_relative_y()/2-12), 1, 1, 0, c_white, alpha* other.intensity);
}
/*
with(obj_dummy)
{
	draw_set_color(c_white);
	draw_set_alpha(other.intensity);
	draw_set_font(global.FontStandardBoldOutline);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_text_transformed(floor(pos_get_relative_x()/2), floor(pos_get_relative_y()/2) - 36, name, 0.5, 0.5, 0);
}
*/

gpu_set_fog(false,c_white,0,0);
surface_reset_target();

////

if(!surface_exists(surf2))
{
	surf2 = surface_create(w,h);
}

surface_set_target(surf2);
draw_set_color(c_black);
draw_set_alpha(1.0);
draw_rectangle(0,0,w,h,false);

gpu_set_fog(true,c_white,0,0);

// ------------------------------- SETTING LIGHTEN COLOR TEX
with(obj_wildlife_firefly)
{
	draw_sprite_ext(spr_pp_light_small,0, pos_get_relative_x()/2, pos_get_relative_y()/2, 1, 1, 0, c_white, image_alpha * other.intensity);
}

with(obj_interactable_bonfire)
{
	if(growstage == 2)
	{
		draw_sprite_ext(spr_pp_light_3, 0, pos_get_relative_x()/2, pos_get_relative_y()/2, flickerscale, flickerscale, 0, c_white, image_alpha * other.intensity);
	}
}

with(obj_wolf)
{
	draw_sprite_ext(scr_asset_get_index(sprite_get_name(sprite_index)+"_eyes"),image_index, pos_get_relative_x()/2.0,pos_get_relative_y()/2.0,image_xscale,image_yscale,0,c_white, image_alpha * other.intensity);
}

with(obj_nightwolf)
{
	draw_sprite_ext(scr_asset_get_index(sprite_get_name(sprite_index)+"_eyes"),image_index, pos_get_relative_x()/2.0,pos_get_relative_y()/2.0,image_xscale,image_yscale,0,c_white, image_alpha * other.intensity);
}


gpu_set_fog(false,c_white,0,0);
surface_reset_target();