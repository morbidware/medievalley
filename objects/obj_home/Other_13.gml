/// @description Open wiki

// Stop if a popup is active
if (instance_exists(obj_popup))
{
	exit;
}

url_open_ext("http://feudalifewiki.indiegala.com", "_blank");