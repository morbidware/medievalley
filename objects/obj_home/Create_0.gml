userrecordreceived = false;
newuser = true;
reloadtimer = 3;
failed = false;
//generate island
var sprite = "";
if(global.isSurvival)
{
	sprite = spr_ui_home_btn_survival;
}
else
{
	sprite = spr_ui_home_btn_start;
}

btnGenerate = instance_create_depth(0,0,-10001,obj_btn_generic);
btnGenerate.sprite_index = sprite;
btnGenerate.fnt = global.FontTitles;
btnGenerate.text = "Generate";
btnGenerate.caller = id;
btnGenerate.color = $B1E2F1;
btnGenerate.topShadow = true;
btnGenerate.evt = ev_user10;
btnGenerate.ox = 480;

// Create buttons
btnStart = instance_create_depth(0,0,-10001,obj_btn_generic);
btnStart.sprite_index = sprite;
btnStart.fnt = global.FontTitles;
btnStart.text = "New game";
btnStart.caller = id;
btnStart.color = $B1E2F1;
btnStart.topShadow = true;
btnStart.evt = ev_user0;
btnStart.ox = 480;

btnContinue = instance_create_depth(0,0,-10001,obj_btn_generic);
btnContinue.sprite_index = sprite;
btnContinue.fnt = global.FontTitles;
btnContinue.text = "Continue";
btnContinue.caller = id;
btnContinue.color = $B1E2F1;
btnContinue.topShadow = true;
btnContinue.evt = ev_user2;
btnContinue.ox = 480;

btnChallenge = instance_create_depth(0,0,-10001,obj_btn_generic);
btnChallenge.sprite_index = sprite;
btnChallenge.fnt = global.FontTitles;
btnChallenge.text = "Challenge";
btnChallenge.caller = id;
btnChallenge.color = $B1E2F1;
btnChallenge.topShadow = true;
btnChallenge.evt = ev_user5;
btnChallenge.ox = 480;

btnChallengeB = instance_create_depth(0,0,-10001,obj_btn_generic);
btnChallengeB.sprite_index = spr_ui_home_btn_start_long;
btnChallengeB.fnt = global.FontTitles;
btnChallengeB.text = "Challenge B";
btnChallengeB.caller = id;
btnChallengeB.color = $B1E2F1;
btnChallengeB.topShadow = true;
btnChallengeB.evt = ev_user6;
btnChallengeB.ox = 480;

btnWiki = instance_create_depth(0,0,-10001,obj_btn_generic);
btnWiki.sprite_index = spr_ui_home_btn_start;
btnWiki.fnt = global.FontTitles;
btnWiki.text = "Wiki";
btnWiki.caller = id;
btnWiki.color = $B1E2F1;
btnWiki.topShadow = true;
btnWiki.evt = ev_user3;
btnWiki.ox = 480;

btnFeedback = instance_create_depth(0,0,-10001,obj_btn_generic);
btnFeedback.sprite_index = spr_ui_home_btn_start;
btnFeedback.fnt = global.FontTitles;
btnFeedback.text = "Feedback";
if(global.isChallenge)
{
	btnFeedback.text = "Rules";
}
btnFeedback.caller = id;
btnFeedback.color = $B1E2F1;
btnFeedback.topShadow = true;
btnFeedback.evt = ev_user4;
btnFeedback.ox = 480;

// Request player record
logga("Fetching player record...");
getPlayerRecord(global.userigid);
sprcount = 0;
timer = 0;