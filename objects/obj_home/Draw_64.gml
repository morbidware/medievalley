// BG image
if(global.isChallenge)
{
	draw_sprite_ext(spr_home_bg,0,scr_adaptive_x(480),scr_adaptive_y(270),2.0,2.0,0.0,c_white,1.0);
	draw_sprite_ext(spr_logo,0,scr_adaptive_x(480),scr_adaptive_y(135),2.0,2.0,0.0,c_white,1.0);
	draw_sprite_ext(spr_challenge_plate,0,scr_adaptive_x(480)-scr_adaptive_x(320),scr_adaptive_y(135),2.0,2.0,0.0,c_white,1.0);
	draw_sprite_ext(spr_challenge_plate,0,scr_adaptive_x(480)+scr_adaptive_x(320),scr_adaptive_y(135),2.0,2.0,0.0,c_white,1.0);
}
else if(global.isSurvival)
{
	if(timer >= 0.3)
	{
		if(sprcount <=17)
		{
			draw_sprite_ext(spr_ui_home_survival_bg,sprcount,scr_adaptive_x(480),scr_adaptive_y(270),2.0,2.0,0.0,c_white,1.0);
			sprcount++;
			timer = 0;
		}
		else
		{
			sprcount = 0;
			timer = 0;
			draw_sprite_ext(spr_ui_home_survival_bg,sprcount,scr_adaptive_x(480),scr_adaptive_y(270),2.0,2.0,0.0,c_white,1.0);
		}
	}
	else
	{
		timer +=0.1;
		draw_sprite_ext(spr_ui_home_survival_bg,sprcount,scr_adaptive_x(480),scr_adaptive_y(270),2.0,2.0,0.0,c_white,1.0);
	}
		
	
	//draw_sprite_ext(spr_ui_home_survival_bg,0,scr_adaptive_x(480),scr_adaptive_y(270),2.0,2.0,0.0,c_white,1.0);
	//draw_sprite_ext(spr_logo,0,scr_adaptive_x(480),scr_adaptive_y(135),2.0,2.0,0.0,c_white,1.0);
}
else
{
	draw_sprite_ext(spr_home_bg,0,scr_adaptive_x(480),scr_adaptive_y(270),2.0,2.0,0.0,c_white,1.0);
	draw_sprite_ext(spr_logo,0,scr_adaptive_x(480),scr_adaptive_y(135),2.0,2.0,0.0,c_white,1.0);
	
}

draw_set_font(global.FontStandardBoldOutlineX2);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_color(c_white);
draw_set_alpha(1);

// Wait until record is received, only if online
if (!userrecordreceived && global.online)
{
	draw_text(scr_adaptive_x(480),scr_adaptive_y(270),"Fetching user data...");

	btnStart.oy = 9999;
	btnContinue.oy = 9999;
	btnChallenge.oy = 9999;
	btnChallengeB.oy = 9999;
}
else if (global.online && (global.username == undefined || global.userigid == undefined || global.username == noone || global.userigid == noone))
{
	failed = true;
	draw_text(scr_adaptive_x(480),scr_adaptive_y(270),"Could not read user data.");
	draw_text(scr_adaptive_x(480),scr_adaptive_y(300),"Reloading page in "+string(ceil(reloadtimer))+"...");
	btnStart.oy = 9999;
	btnContinue.oy = 9999;
	btnChallenge.oy = 9999;
	btnChallengeB.oy = 9999;
}
else
{
	if(global.isChallenge == true)
	{
		btnChallenge.oy = 540 - 130;
		btnStart.oy = 9999;
		btnContinue.oy = 9999;
		btnWiki.oy = 9999;

		if(newuser)
		{
			global.newUserForChallenge = true;
		}
		
		if (!newuser && global.online)
		{
			draw_text(scr_adaptive_x(480),scr_adaptive_y(270), "Ready for a challenge "+global.username+"?");
		}
		else
		{
			draw_text(scr_adaptive_x(480),scr_adaptive_y(270), "Ready for a Challenge?");
		}
		if(!global.willEarnGalaCreds)
		{
			draw_set_font(global.FontStandardBoldOutlineX2);
			draw_set_halign(fa_right);
			draw_set_valign(fa_top);
			draw_set_color(c_white);
			draw_sprite_ext(spr_money_label_big,0,display_get_gui_width()-16 ,4,2.0,2.0,0,c_white,1.0);
			draw_text(display_get_gui_width()-16 ,10,string(global.gold));
		}
		else
		{
			
			if(!global.hideChallengePrize)
			{
				btnChallenge.oy = 540 - 180;
				btnChallenge.text = "Play for Steam Key";
				btnChallenge.sprite_index = spr_ui_home_btn_start_long;
				btnChallengeB.oy = 540 - 130;
				btnChallengeB.text = "Play for Galacredits";
			}
			else
			{
				btnChallenge.oy = 540 - 130;
				if(global.ismobile)
				{
					btnChallenge.oy = 540 - 150;
				}
			}
		}
	}
	else
	{
		btnStart.oy = 540 - 130;
		btnChallenge.oy = 9999;
		btnChallengeB.oy = 9999;
		if (!newuser && global.online)
		{
			draw_text(scr_adaptive_x(480), scr_adaptive_y(270), "Welcome back, "+global.username+"!");
			if(global.isSurvival && global.landid == 0)
			{
				btnContinue.oy = 9999;
			}
			else
			{
				btnContinue.oy = 540 - 180;
			}
		}
		else
		{
			if (os_browser == browser_not_a_browser)
			{
				
				draw_text(scr_adaptive_x(480), scr_adaptive_y(270), "Welcome back!");
			}
			btnContinue.oy = 9999;
		}
	}
}

if(global.isSurvival)
{
	if(global.online && (global.userigid == 4633525518204928 || global.userigid == 4954661227986944))
	{
		btnGenerate.oy = 540 - 230;
	}
	else
	{
		btnGenerate.oy = 9999;
	}
}
else
{
	btnGenerate.oy = 9999;
}
if(!global.isSurvival)
{
	btnWiki.oy = 540 - 80;
	btnFeedback.oy = 540 - 30;
}
else
{
	btnWiki.oy =9999;
	btnFeedback.oy = 9999;
}
if(global.isChallenge)
{
	btnWiki.oy = 9999;
	btnFeedback.oy = 540 - 80;
}