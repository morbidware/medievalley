/// @description Open feedback

// Stop if a popup is active
if (instance_exists(obj_popup))
{
	exit;
}
if(global.isChallenge)
{
	url_open_ext("https://forums.indiegala.com/threads/feudalife-challenges-rules.1988/", "_blank");
}
else
{
	url_open_ext("https://forums.indiegala.com/forums/feudalife.7/", "_blank");
}



