{
    "id": "c5d0a3d2-b594-44d0-9179-5e19943f2e45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_home",
    "eventList": [
        {
            "id": "b2dc16b4-12de-430e-b75e-c731389fb375",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "5bf282a9-a4ea-454f-9a09-64d0b84f74af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "5bd054af-d236-457f-9e08-d7b02697f7a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "d5312639-69cb-41cd-9f7f-81d736e679da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "bb1f1f63-c73e-42a2-87b9-1b68dacd91f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "2f61de7f-b534-4185-970c-9b547a07340b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "da7c15df-2b7e-415c-805f-c7589575432a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "2be67fa6-65b9-4e9d-b2e5-13760f758cd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "7c75978b-20cc-465c-92eb-77cddce6d735",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "923c7631-9b00-49fa-a72c-f688ac87dd94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "094423d1-7147-456b-859e-73de072cef7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "50f3f551-7df7-472a-bc68-17a94d963405",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        },
        {
            "id": "cd19cf00-2a75-4aa1-bbcc-f7fea0790d46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "c5d0a3d2-b594-44d0-9179-5e19943f2e45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}