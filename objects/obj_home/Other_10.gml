/// @description Start button

// Stop if a popup is active
if (instance_exists(obj_popup))
{
	exit;
}

// If not online, jump direclty to farm
if (!global.online)
{
	with (obj_main_controller)
	{
		event_perform(ev_other,ev_user0);
	}
	btnStart.disabled = true;
	exit;
}

// Stop if no ID is found
if(global.userigid <= 0 || global.userigid == "")
{
	with(instance_create_depth(0,0,0,obj_popup))
	{
		title = "SESSION ERROR";
		body = "You must login to IndieGala to continue.";
		type = 1;
		btnText = "CLOSE";
		caller = id;
	}
	exit;
}

// If not a new user, ask confirmation for restarting game from scratch 
if (!newuser)
{
	instance_create_depth(0,0,0,obj_popup_newgame);
}
// If new user, load game directly (it will load an empty game anyways)
else
{
	event_perform(ev_other,ev_user2);
	btnStart.disabled = true;
}