/// @description Restart game

// If not online, jump direclty to farm
if (!global.online)
{
	with (obj_main_controller)
	{
		event_perform(ev_other,ev_user0);
	}
	exit;
}

// Stop if no ID is found
if(global.userigid <= 0 || global.userigid == "")
{
	with(instance_create_depth(0,0,0,obj_popup))
	{
		title = "SESSION ERROR";
		body = "You must login to IndieGala to continue.";
		type = 1;
		btnText = "CLOSE";
		caller = id;
	}
	exit;
}

// Clear player data first, then wait a moment before actually starting the game
clearPlayerData(global.userigid);
instance_create_depth(0,0,0,obj_loading_panel);
alarm_set(0,60);