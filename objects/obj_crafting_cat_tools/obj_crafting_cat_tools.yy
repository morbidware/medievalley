{
    "id": "d7cf2bd3-653b-464d-a94a-28cf23c85025",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_tools",
    "eventList": [
        {
            "id": "bd27f2d3-9f34-42cc-b811-3312458a164a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7cf2bd3-653b-464d-a94a-28cf23c85025"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9459517-9af5-418b-a7c0-07dffc802dcd",
    "visible": true
}