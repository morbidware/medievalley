/// @description Insert description here
// You can write your code in this editor
event_inherited();
unlocked = true;
name = "TOOLS";

with(instance_create_depth(0,0,-1,obj_recipe_axe))
{
	index = 0;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_pickaxe))
{
	index = 1;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_sickle))
{
	index = 2;
	cat = other.id;
	ds_list_add(other.entries, id);
}

// -------------------

with(instance_create_depth(0,0,-1,obj_recipe_hoe))
{
	index = 3;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_wateringcan))
{
	index = 4;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_shovel))
{
	index = 5;
	cat = other.id;
	ds_list_add(other.entries, id);
}
with(instance_create_depth(0,0,-1,obj_recipe_hammer))
{
	index = 6;
	cat = other.id;
	ds_list_add(other.entries, id);
}
if(!global.isSurvival)
{
	with(instance_create_depth(0,0,-1,obj_recipe_spear))
	{
		index = 7;
		cat = other.id;
		ds_list_add(other.entries, id);
	}
}
else
{
	
	/*with(instance_create_depth(0,0,-1,obj_recipe_fishingrod))
	{
		index = 7;
		cat = other.id;
		ds_list_add(other.entries, id);
	}*/
}
/*
for(var i = 0; i < 10; i++)
{
	var axe = instance_create_depth(0,0,-1,obj_recipe);
	axe.index = i;
	axe.cat = id;
	ds_list_add(entries, axe);
}
*/