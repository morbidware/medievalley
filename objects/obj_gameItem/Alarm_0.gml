///@description Storey and depth
var ground = instance_find(obj_baseGround,0);
storey = ds_grid_get(ground.storeygrid,clamp(floor(x/16),0,9999),clamp(floor(y/16),0,9999));
scr_setDepth(DepthType.Elements);
if (collisionradius > 8)
{
	mp_grid_add_cell(ground.pathgrid, x/16, y/16);
}