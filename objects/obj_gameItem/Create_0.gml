/// @description Init vars

//nid = 0;
itemid = "";
name = "";
equippable = 0;
wearable = 0;
wearIndex = 0;
spritename = "";
maxquantity = 0;
quantity = 1;
state = 0;
storey = 0;


//growing vars
growstage = 0;
growtimer = 0;
growcrono = 0;
growstages = 0;

//durability
lossperusage = 0;
durability = 0;
lifeperstage = 0;

//watering
needswatering = 0;

//attack
mindamage = 0;
maxdamage = 0;

//armor
defence = 0;

//structures
isastructure = 0;
wallsw = 0;
wallsh = 0;
craftCrono = 0;
craftid = "";
structurewalls = ds_list_create();

kind = -1;
life = 0;
shadowType = ShadowType.Sprite;
shadowBlobScale = 1;
shadowLength = 24;
shadowOnWalls = false;

consumable = 0;
foodGain = 0;
collisionradius = 0;

destroy = false;

ignoregroundlimits = false;
smoothspawn = false;
smoothspawntimer = 0;

alarm_set(0,4);