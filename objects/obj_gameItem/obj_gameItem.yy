{
    "id": "cf8deb36-79b4-4043-940c-ca9d662edda0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameItem",
    "eventList": [
        {
            "id": "1f4a077e-8920-4430-9d45-3f9dc6637acc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf8deb36-79b4-4043-940c-ca9d662edda0"
        },
        {
            "id": "3ca8c071-f68b-47ca-97fc-e6930495ff30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cf8deb36-79b4-4043-940c-ca9d662edda0"
        },
        {
            "id": "e42df8f3-b75d-4720-9a51-ec0cbf3fce7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cf8deb36-79b4-4043-940c-ca9d662edda0"
        },
        {
            "id": "8b981e97-f6e9-4f3c-b417-5e817f45ea5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cf8deb36-79b4-4043-940c-ca9d662edda0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}