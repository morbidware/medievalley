/// @description Smooth spawn
if (!smoothspawn)
{
	exit;
}

smoothspawntimer += global.dt / 90;

image_alpha = smoothspawntimer;
image_yscale = 0.5 + (smoothspawntimer * 0.5);
image_xscale = 0.8 + (smoothspawntimer * 0.2);

if (smoothspawntimer >= 1)
{
	image_alpha = 1;
	image_yscale = 1;
	image_xscale = 1;
	smoothspawn = false;
}