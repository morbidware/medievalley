relativex = pos_get_relative_x();
relativey = pos_get_relative_y();

var cam = view_camera[0];
x1 = camera_get_view_x(cam);
y1 = camera_get_view_y(cam);
x2 = x1 + camera_get_view_width(cam);
y2 = y1 + camera_get_view_height(cam);
with(obj_enemy)
{	
	if(!point_in_rectangle(x, y, other.x1, other.y1, other.x2, other.y2))
	{
		var dir = point_direction(x,y,other.player.x,other.player.y) + 180;
		//var xPos = (player.x - x1)*(display_get_gui_width()/camera_get_view_width(cam)) + lengthdir_x(display_get_gui_height()/7,dir);
		//var yPos = (player.y - y1)*(display_get_gui_height()/camera_get_view_height(cam)) + lengthdir_y(display_get_gui_height()/7,dir);
		var xPos = other.relativex + lengthdir_x(display_get_gui_height()/7,dir);
		var yPos = other.relativey + lengthdir_y(display_get_gui_height()/7,dir);
		
		draw_sprite_ext(spr_recipeinfo_pointer,0,xPos,yPos,1,1,dir -180,c_white,1);
	}
}


