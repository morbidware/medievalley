{
    "id": "8a535594-4397-4167-baf2-b9ccd29c74da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mob_out_of_screen",
    "eventList": [
        {
            "id": "74623d0a-359b-488c-a564-0a02e64f8ef0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8a535594-4397-4167-baf2-b9ccd29c74da"
        },
        {
            "id": "d1ca1354-1534-4a2d-abed-7a61d7a68abf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a535594-4397-4167-baf2-b9ccd29c74da"
        },
        {
            "id": "694e03cb-beb4-49ac-a3a8-2f7e5c67ceda",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a535594-4397-4167-baf2-b9ccd29c74da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}