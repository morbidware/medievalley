/// @description can be interacted
// You can write your code in this editor
//show_debug_message("can be interacted?");
global.retval = true;
var pla = instance_find(obj_char,0);
if(pla.state == ActorState.Rest)
{
	interactionPrefixLeft = "Exit";
}
else
{
	interactionPrefixLeft = "Sleep in";
}
var handState = global.game.player.handState;
if(handState == HandState.Hammer)
{
	interactionPrefixRight = "Dismantle";
}
else
{
	interactionPrefixRight = "";
}