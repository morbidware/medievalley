{
    "id": "2ec8edbe-337e-410e-95bb-fc740101648f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_tent",
    "eventList": [
        {
            "id": "4b4398c8-31c9-4924-b816-2d7e149e70ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2ec8edbe-337e-410e-95bb-fc740101648f"
        },
        {
            "id": "059799bd-7ba4-4714-89a6-0cc093f6b2f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2ec8edbe-337e-410e-95bb-fc740101648f"
        },
        {
            "id": "e7c10daa-8206-40e5-ae61-3f039f5c74c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ec8edbe-337e-410e-95bb-fc740101648f"
        },
        {
            "id": "8498f826-a9d7-4a0f-8da1-99028aa48c08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2ec8edbe-337e-410e-95bb-fc740101648f"
        },
        {
            "id": "0de5cf48-41cb-45f6-b5f8-3913080b5630",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "2ec8edbe-337e-410e-95bb-fc740101648f"
        },
        {
            "id": "25cdb162-3466-4d42-8ce7-4369cdcf7f01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "2ec8edbe-337e-410e-95bb-fc740101648f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5d449c6a-3857-4210-aa67-06f14ee5b261",
    "visible": true
}