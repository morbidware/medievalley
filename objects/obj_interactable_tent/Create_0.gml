/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ingredients = ds_map_create();
ds_map_add(ingredients,"woodlog",5);
ds_map_add(ingredients,"stone",3);

tooltip_alpha = 0.0; 

healthgain = 0.25;
staminagain = 2;