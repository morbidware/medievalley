/// @description Align, depth
if (parent != noone && parent > 0)
{
	x = parent.x + relative_x;
	y = parent.y + relative_y;
}
scr_setDepth(depthType);
storey = ds_grid_get(ground.storeygrid, clamp(floor(x/16),0,ground.cols-1), clamp(floor(y/16),0,ground.rows-1));