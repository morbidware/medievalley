///@description Init
ground = instance_find(obj_baseGround,0);
storey = 0;
parent = noone;
depthType = DepthType.Elements;

relative_x = 0;
relative_y = 0;

// shadow setup
shadowType = ShadowType.Sprite;
shadowBlobScale = 1;
shadowLength = 20;
offsetx = 0;
offsety = 0;