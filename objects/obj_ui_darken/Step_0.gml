if (instance_exists(owner))
{
	t = owner.t;
	state = owner.state;
}
else
{
	instance_destroy(id);
}