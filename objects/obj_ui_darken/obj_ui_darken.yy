{
    "id": "999e4efa-e100-4046-8eeb-6b6914041b91",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_darken",
    "eventList": [
        {
            "id": "dd9e2953-bb6a-4977-971f-ebe20a5da767",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "999e4efa-e100-4046-8eeb-6b6914041b91"
        },
        {
            "id": "8c1c4671-be30-458c-9692-41ce8ee31993",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "999e4efa-e100-4046-8eeb-6b6914041b91"
        },
        {
            "id": "a9671751-3f49-47a8-aec1-0efb6895240c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "999e4efa-e100-4046-8eeb-6b6914041b91"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}