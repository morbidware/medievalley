///@description Crafting basics - Craft the primary tools.
event_inherited();

missionid = "0001";
title = "Crafting basics";
shortdesc = "Craft the primary tools.";
longdesc = "Pick up raw materials from the ground and use them to craft an Axe, a Pickaxe and a Sickle.";
silent = false;
ds_map_set(global.unlockables, "rec_craftingtools", 1);

ds_map_add(rewards,obj_reward_0001,1);
ds_map_add(required,"axe",1);
ds_map_add(required,"pickaxe",1);
ds_map_add(required,"sickle",1);

ds_list_clear(hintTexts);
ds_list_add(hintTexts, "Pick up raw materials from the ground.", "Using the materials, create tools from the crafting bar.", "Check your diary to keep track of the active quests.");
ds_list_clear(hintImages);
ds_list_add(hintImages, spr_pickable_flint, spr_crafting_cat_tools, spr_dock_btn_diary);

scr_storeMissions();