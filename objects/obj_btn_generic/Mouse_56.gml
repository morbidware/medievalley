// if a popup is active, only accept inputs on btn_generic of that popup
if(disabled)
{
	exit;
}
if(global.onPopup && object_get_name(caller.object_index) != "obj_popup" && object_get_name(caller.object_index) != "obj_popup_newgame")
{
	exit;
}

// detect click
if(mouse_get_relative_x() > x - abs(sprite_width) && mouse_get_relative_x() < x + abs(sprite_width) && mouse_get_relative_y() > y - abs(sprite_height) && mouse_get_relative_y() < y + abs(sprite_height))
{
	with(caller)
	{
		event_perform(ev_other, other.evt);
		audio_play_sound(snd_click,0,false);
	}
}