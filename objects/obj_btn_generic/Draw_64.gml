draw_self();
draw_sprite_ext(sprite_index, image_index, x, y, image_xscale*2, image_yscale*2, image_angle, c_white, 1);

if(text != "")
{
	draw_set_font(fnt);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	if(topShadow)
	{
		draw_set_color(c_black);
		draw_set_alpha(0.8);
		draw_text(x+textox, y-2+textoy, text);
		draw_set_color(c_white);
		draw_set_alpha(1.0);
	}
	
	draw_set_color(color);
	draw_text(x+textox, y+textoy, text);
	draw_set_color(c_white);
}