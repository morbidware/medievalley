{
    "id": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_btn_generic",
    "eventList": [
        {
            "id": "ac983073-3c98-47a3-a52f-b471a7140754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c"
        },
        {
            "id": "71b0e5f9-f8c9-445e-9b88-d50a6aa6e174",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c"
        },
        {
            "id": "71a0022a-8b4f-43e6-a9cc-9f0b60adb533",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c"
        },
        {
            "id": "9a02cc87-40a0-4a70-a26f-6111b7b0e0d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c"
        },
        {
            "id": "d711f3bd-2a8c-49cf-b01d-dd6917dc4242",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c"
        },
        {
            "id": "53329eb0-f7ae-46e3-b798-6a13e53ab560",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "574f5f8e-57e6-4d14-bcb9-7bd90156cd2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "348fc906-1b4f-4117-819d-d2ff3801aff8",
    "visible": true
}