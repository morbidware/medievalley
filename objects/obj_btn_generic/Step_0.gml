// NOTE: previously spr width and height were divided by 2, but now the sprite is drawn 2x the size, so no need to divide
if(disabled)
{
	image_index = 3;
	exit;
}
if(mouse_get_relative_x() > x - abs(sprite_width) && mouse_get_relative_x() < x + abs(sprite_width) && mouse_get_relative_y() > y - abs(sprite_height) && mouse_get_relative_y() < y + abs(sprite_height))
{
	if(mouse_check_button(mb_left))
	{
		image_index = 2;
	}
	else
	{
		image_index = 1;
	}
}
else
{
	image_index = 0;
}