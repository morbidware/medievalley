if(mouse_get_relative_x() > x && mouse_get_relative_x() < x + caller.width-8 && mouse_get_relative_y() > y && mouse_get_relative_y() < y + 32)
{
	if(!instance_exists(obj_letter))
	{
		if (read == 0)
		{
			var panel = instance_find(obj_mailbox_panel,0);
			read = 1;
		}
		ds_list_set(global.mailRead,globalindex,1);
		var letter = instance_create_depth(0,0,0,obj_letter);
		letter.title = title;
		letter.body = body;
		letter.signature = signature;
	}
}