{
    "id": "971cdb3c-e695-4c10-a10e-baf2d10ee243",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_btn_mail",
    "eventList": [
        {
            "id": "06d9a544-3bd2-40c3-85d2-bfe63b5a3c2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "971cdb3c-e695-4c10-a10e-baf2d10ee243"
        },
        {
            "id": "ee480c2f-290c-402f-91be-b1c7b5c5e182",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "971cdb3c-e695-4c10-a10e-baf2d10ee243"
        },
        {
            "id": "630a69db-ebc3-4c41-b59b-72787b8945bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "971cdb3c-e695-4c10-a10e-baf2d10ee243"
        },
        {
            "id": "258d2744-4542-4580-b8a5-1ec5ea67bb59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "971cdb3c-e695-4c10-a10e-baf2d10ee243"
        },
        {
            "id": "11458958-2d2a-40ac-a1bb-3d0f24a95222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "971cdb3c-e695-4c10-a10e-baf2d10ee243"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8e78dd7-831b-4ab5-9fda-55f8e629fec9",
    "visible": true
}