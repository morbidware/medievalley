draw_set_color(c_white);
if(read == 0)
{
	scr_nine_slice(spr_mailbox_container_unread,x,y,x+caller.width-28,y+32,2);
}
else
{
	scr_nine_slice(spr_mailbox_container_read,x,y,x+caller.width-28,y+32,2);
}

draw_sprite_ext(spr_mailbox_read_unread,read,x,y,2,2,0,c_white,1);
draw_set_color(c_black);
draw_set_font(global.FontStandard);
draw_set_valign(fa_middle);
draw_set_halign(fa_left);
draw_text(x + 32 + 8, y + 16, title);
draw_set_color(c_white);