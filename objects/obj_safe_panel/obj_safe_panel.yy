{
    "id": "b8781bf2-8d99-48c5-a101-68a4f06a81c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_safe_panel",
    "eventList": [
        {
            "id": "d4699627-9067-4c9f-893b-28e009280530",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8781bf2-8d99-48c5-a101-68a4f06a81c8"
        },
        {
            "id": "ddd2de8d-76b9-4ae0-aae9-6b02130b7655",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b8781bf2-8d99-48c5-a101-68a4f06a81c8"
        },
        {
            "id": "7d759a53-d601-43b6-b012-56930b808ebe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "b8781bf2-8d99-48c5-a101-68a4f06a81c8"
        },
        {
            "id": "862b3c92-59a6-479d-8cec-7fc01f58326f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8781bf2-8d99-48c5-a101-68a4f06a81c8"
        },
        {
            "id": "832ac3bb-cfa5-4808-939d-94aab478a189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b8781bf2-8d99-48c5-a101-68a4f06a81c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}