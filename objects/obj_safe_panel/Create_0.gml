event_inherited();

depth = -500;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -400;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
btnClose.ox = 203;
btnClose.oy = -108;
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.sprite_index = spr_btn_close_orange;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}

gamelevel = instance_find(obj_gameLevel,0);

state = 0;
t = 0;

slots = 50;
cols = 10;
rows = 5;
cellsize = 32;
startx = 0;
starty = 0;
spacing = 6;

inv = instance_find(obj_playerInventory,0);

w_width = sprite_get_width(spr_stash_bg);
w_height = sprite_get_height(spr_stash_bg);