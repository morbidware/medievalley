event_inherited();

startx = -166;
starty = -76;

if (instance_exists(btnClose))
{
	btnClose.ox = 208;
	btnClose.oy = -108;
}

// Align objects to grid
var c = 0;
var r = 0;
var index = -1;
var obj = noone;
repeat (rows)
{
	c = 0;
	repeat (cols)
	{
		index = global.inv_stashlimit + (r * cols) + c;
		obj = ds_grid_get(inv.inventory,index,0);
		if (obj > -1 && instance_exists(obj))
		{
			obj.x = x + startx + (spacing * c) + (cellsize * c);
			obj.y = y + starty + (spacing * r) + (cellsize * r);
		}
		c++;
	}
	r++;
}

/*
// Align objects to grid
for (var i = 84; i < 104; i++)
{
	var obj = ds_grid_get(inv.inventory,i,0);
	if (obj != -1)
	{
		if(obj.state == 0)
		{
			var col = obj.slot-84;
			var row = 0;
			while(col > 9)
			{
				col-= 10;
				row ++;
			}
			obj.x = startx + 38 * col;
			obj.y = starty + 38 * row;
		}
	}
}
*/