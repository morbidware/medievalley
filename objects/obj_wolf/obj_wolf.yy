{
    "id": "5a493797-6ed1-48fa-b1aa-fd3b7843c9df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wolf",
    "eventList": [
        {
            "id": "5dec97e3-6a7f-4a5a-b922-592d3471bc93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5a493797-6ed1-48fa-b1aa-fd3b7843c9df"
        },
        {
            "id": "7de5068a-9e40-43f7-8a3d-37ed3fb9e909",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5a493797-6ed1-48fa-b1aa-fd3b7843c9df"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73e72f19-6ffd-4b28-a5da-c77a2366206e",
    "visible": true
}