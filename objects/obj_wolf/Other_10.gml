/// @description Custom mob behavior
maxlife = 20;
life = 20;				// Health
spd = 1.2;				// Movement speed
sightradius = 200;		// Radius for detecting the player
chaseradius = 150;		// Radius for start chasing the player or fleeing if ranged
attackradius = 16;		// Radius of attack
attackpause = 1;		// Seconds between each consecutive attack
damage = 15;			// Damage for each single attack
alerttime = 2;			// How many seconds the mob will wait before going from alert to an action state
chasetime = 5;			// How many seconds the mob will chase the player
pacific = false;		// If true, the mob will attack only when attacked
cautious = false;		// If true, the mob will stay at least 'chaseradius' far from the player
ranged = false;			// if true, the mob behavior better fits a ranged attack style
lazy = 0.4;				// Probability of the mob to not move from its place when idling
attackTimer = 15
chargeTimer = 15