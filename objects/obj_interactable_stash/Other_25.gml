/// @description Custom setup
shadowType = ShadowType.None;
shadowOnWalls = true;
shadowLength = 8;
event_inherited();
ignoregroundlimits = true;
canSendNetEvents = false;