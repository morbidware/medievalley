{
    "id": "9bd7e0f2-bb55-417d-aaf8-f1befd515a7b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_stash",
    "eventList": [
        {
            "id": "1d7ab07f-d310-486d-bfcf-df83657a5d8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "9bd7e0f2-bb55-417d-aaf8-f1befd515a7b"
        },
        {
            "id": "721d445c-e9fd-4b5d-b84c-711a458ad410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9bd7e0f2-bb55-417d-aaf8-f1befd515a7b"
        },
        {
            "id": "90321d0a-f3bb-41f1-8ddb-343ea74a3dfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "9bd7e0f2-bb55-417d-aaf8-f1befd515a7b"
        },
        {
            "id": "9ddd0287-d4d6-477c-8347-0f519238f82a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9bd7e0f2-bb55-417d-aaf8-f1befd515a7b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a6975173-dd8d-487d-a8df-5ba7d599281d",
    "visible": true
}