/// @description Set Male
with (obj_char)
{
	gender = "m";
	scr_selectWardrobePart(custom_body_index,false,"body");
	scr_selectWardrobePart(custom_head_index,false,"head");
	scr_selectWardrobePart(custom_upper_index,false,"upper");
	scr_selectWardrobePart(custom_lower_index,false,"lower");
}
event_perform(ev_other,ev_user15);