{
    "id": "99952860-4b7a-4d44-a963-984198b1bd8c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_charcr_panel",
    "eventList": [
        {
            "id": "85025178-a3ee-45f4-bd23-31721b76416f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "8175fa6a-c723-4c19-87c0-e078c3093d5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "26538dfc-1c75-4785-833c-b141c5d24177",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "a8347508-8c50-48bc-905c-ddcb017f3b44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "9be3dfe5-06fb-462d-b534-4d16d2420fc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "c0facce3-a08e-4791-9f60-4148596e3652",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "84e76786-bc43-4e38-9cc1-c353761c74eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "9f0953dc-fe3c-4c7d-be81-d9c2828e8506",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "423742f5-5172-4691-bf00-79065a00d966",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "fa476f91-6494-4028-96cb-e508da078cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "0920cf64-8d40-40b2-945d-29f00628a5e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "26afe414-f7be-4e4d-beee-c3394f87ff0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        },
        {
            "id": "34c601d4-43f2-4cd0-9461-c4d4822889d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "99952860-4b7a-4d44-a963-984198b1bd8c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}