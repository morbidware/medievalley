/// @description Update sprites and save locally
sprbody = scr_asset_get_index(string(player.custom_body)+"_body_idle_right_strip4");
sprhead = scr_asset_get_index(string(player.custom_head)+"_head_idle_right_strip4");
sprlower = scr_asset_get_index(string(player.custom_lower)+"_lower_idle_right_strip4");
sprupper = scr_asset_get_index(string(player.custom_upper)+"_upper_idle_right_strip4");

namehead = ds_list_find_value(player.custom_head_object,1);
nameupper = ds_list_find_value(player.custom_upper_object,1);
namelower = ds_list_find_value(player.custom_lower_object,1);

ds_list_set(global.currentwardrobe,0,player.gender);
ds_list_set(global.currentwardrobe,1,player.custom_body_index);
ds_list_set(global.currentwardrobe,2,player.custom_head_index);
ds_list_set(global.currentwardrobe,3,player.custom_upper_index);
ds_list_set(global.currentwardrobe,4,player.custom_lower_index);