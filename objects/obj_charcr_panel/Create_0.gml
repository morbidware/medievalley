// Inherit the parent event
event_inherited();

depth = -10000;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -9900;
player = instance_find(obj_char,0);
sprbody = spr_missing;
sprhead = spr_missing;
sprupper = spr_missing;
sprlower = spr_missing;
namehead = "";
nameupper = "";
namelower = "";

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

w_width = sprite_get_width(spr_charcr_bg);
w_height = sprite_get_height(spr_charcr_bg);

// Set male
btnMale = instance_create_depth(0,0,-10010,obj_btn_generic);
btnMale.ox = 14;
btnMale.oy = -146;
btnMale.sprite_index = spr_charcr_male;
btnMale.caller = id;
btnMale.evt = ev_user1;

// Set female
btnFemale = instance_create_depth(0,0,-10010,obj_btn_generic);
btnFemale.ox = 56;
btnFemale.oy = -146;
btnFemale.sprite_index = spr_charcr_female;
btnFemale.caller = id;
btnFemale.evt = ev_user2;

// Head left
headLeft = instance_create_depth(0,0,-10010,obj_btn_generic);
headLeft.ox = -96;
headLeft.oy = -26;
headLeft.sprite_index = spr_charcr_arrow;
headLeft.caller = id;
headLeft.evt = ev_user3;
headLeft.image_xscale = -1;

// Head right
headRight = instance_create_depth(0,0,-10010,obj_btn_generic);
headRight.ox = 110;
headRight.oy = -26;
headRight.sprite_index = spr_charcr_arrow;
headRight.caller = id;
headRight.evt = ev_user4;

// Upper left
upperLeft = instance_create_depth(0,0,-10010,obj_btn_generic);
upperLeft.ox = -96;
upperLeft.oy = 32;
upperLeft.sprite_index = spr_charcr_arrow;
upperLeft.caller = id;
upperLeft.evt = ev_user5;
upperLeft.image_xscale = -1;

// Upper right
upperRight = instance_create_depth(0,0,-10010,obj_btn_generic);
upperRight.ox = 110;
upperRight.oy = 32;
upperRight.sprite_index = spr_charcr_arrow;
upperRight.caller = id;
upperRight.evt = ev_user6;

// Lower left
lowerLeft = instance_create_depth(0,0,-10010,obj_btn_generic);
lowerLeft.ox = -96
lowerLeft.oy = 86;
lowerLeft.sprite_index = spr_charcr_arrow;
lowerLeft.caller = id;
lowerLeft.evt = ev_user7;
lowerLeft.image_xscale = -1;

// Lower right
lowerRight = instance_create_depth(0,0,-10010,obj_btn_generic);
lowerRight.ox = 114;
lowerRight.oy = 86;
lowerRight.sprite_index = spr_charcr_arrow;
lowerRight.caller = id;
lowerRight.evt = ev_user8;

// Close
btnClose = instance_create_depth(0,0,-10010,obj_btn_generic);
btnClose.ox = 152;
btnClose.oy = -164;
btnClose.sprite_index = spr_btn_close_orange;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}
btnClose.caller = id;
btnClose.evt = ev_user0;

// Refresh parts
event_perform(ev_other,ev_user15);