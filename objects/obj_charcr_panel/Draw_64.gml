draw_set_alpha(1);
draw_set_color(c_white);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_font(global.FontStandardOutline);

// -- Background
draw_sprite_ext(spr_charcr_bg, 0, x, y-32, 2, 2, 0, c_white, 1);

// -- Char window
draw_sprite_ext(spr_charcr_charbase, 0, x-64, y-112, 2, 2, 0, c_white, 1);

// -- Name label
draw_sprite_ext(spr_charcr_bar, 0, x+78, y-80, 2, 2, 0, c_white, 1);
//draw_set_color($2E3D55);
if (string_length(global.username) > 0)
{
	draw_text(x+78, y-80, string(global.username));
}
else
{
	draw_text(x+78, y-80, "Player");
}
draw_set_color(c_white);

// -- Part labels
draw_sprite_ext(spr_charcr_bar, 0, x+8, y-26, 2, 2, 0, c_white, 1);
draw_sprite_ext(spr_charcr_bar, 0, x+8, y+32, 2, 2, 0, c_white, 1);
draw_sprite_ext(spr_charcr_bar, 0, x+8, y+86, 2, 2, 0, c_white, 1);

// -- Label texts
draw_text(x+8, y+32, nameupper);
if (player.renderhead)
{
	draw_set_alpha(1);
	draw_text(x+8, y-26, namehead);
}
else
{
	draw_set_alpha(0.5);
	draw_text(x+8, y-26, namehead);
}
if (player.renderlower)
{
	draw_set_alpha(1);
	draw_text(x+8, y+86, namelower);
}
else
{
	draw_set_alpha(0.5);
	draw_text(x+8, y+86, namelower);
}

// -- Character
draw_set_alpha(1);
draw_sprite_ext(sprbody,0,x-66,y-102,2,2,0,c_white,1);
if (player.renderhead)
{
	draw_sprite_ext(sprhead,0,x-66,y-102,2,2,0,c_white,1);
}
if (player.renderlower)
{
	draw_sprite_ext(sprlower,0,x-66,y-102,2,2,0,c_white,1);
}
draw_sprite_ext(sprupper,0,x-66,y-102,2,2,0,c_white,1);