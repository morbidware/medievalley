/// @description Early setup
gridded = 1;
growstage = 2+ceil(random(growstages-2));
growcrono = growtimer;
growcrono = (growtimer/2)+random(growtimer/2);

if(global.isChallenge)
{
	growstage = 4;
	growcrono = 0;
	life = lifeperstage * growstage;
	image_index = growstage-1;
}