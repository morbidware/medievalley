/// @description can be interacted
interactionPrefixLeft = "";
interactionPrefixRight = "";
global.retval = false;

if(growstage > 1)
{
	var handState = global.game.player.handState;
	if(handState == HandState.Axe)
	{
		global.retval = true;
		interactionPrefixRight = "Chop";
	}
	else
	{
		global.retval = false;
	}
}
else
{
	if(global.game.player.handState == HandState.Shovel &&
		global.game.player.highlightCol = floor(x/16) &&
		global.game.player.highlightRow = floor(y/16))
	{
		global.retval = true;
		interactionPrefixRight = "Remove";
	}
	else
	{
		global.retval = false;
	}
}