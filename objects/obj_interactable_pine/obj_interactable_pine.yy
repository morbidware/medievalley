{
    "id": "97d040d0-5942-495a-8c49-bf0cd6c10d94",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_pine",
    "eventList": [
        {
            "id": "74665aa5-df06-42ad-a52b-8e2253023329",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        },
        {
            "id": "f83a0248-8cf5-4d89-9a9a-eb6e89722591",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        },
        {
            "id": "b6e0ed63-8fbc-4f4b-b26e-96770f6fd3bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        },
        {
            "id": "6f0e080d-b03a-4a22-875c-40842e44511f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        },
        {
            "id": "50a0c73c-f2e0-4dc1-808e-3be95e498372",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        },
        {
            "id": "b2b8748f-9499-4377-a4ea-b66b123bfb28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        },
        {
            "id": "9a930202-a929-426e-b422-67e6e5c831f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "97d040d0-5942-495a-8c49-bf0cd6c10d94"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "22d81275-76a2-4883-a1af-c1ee7801a0da",
    "visible": true
}