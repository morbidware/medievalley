{
    "id": "1b05672f-b612-4b44-ac3d-2f0655a38c2f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fox_old",
    "eventList": [
        {
            "id": "c1eaddac-045a-4789-843e-8f29f7cee11e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1b05672f-b612-4b44-ac3d-2f0655a38c2f"
        },
        {
            "id": "628e1811-f153-478a-b1e0-b2b2a3491c89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "1b05672f-b612-4b44-ac3d-2f0655a38c2f"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7200c15-8327-43af-a033-1e89c9f39857",
    "visible": true
}