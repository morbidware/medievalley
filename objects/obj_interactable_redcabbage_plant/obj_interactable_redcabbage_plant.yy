{
    "id": "5e4550f5-2519-4b8b-ab4c-d2828007fb17",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_redcabbage_plant",
    "eventList": [
        {
            "id": "6aa9a64a-8afb-4cfa-8c5c-161bb2c2e98c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5e4550f5-2519-4b8b-ab4c-d2828007fb17"
        },
        {
            "id": "133e5b40-09b2-4d5a-b6e9-5d76bbb346df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5e4550f5-2519-4b8b-ab4c-d2828007fb17"
        },
        {
            "id": "e36c7920-4813-4e07-b82f-5e7509c5ebc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "5e4550f5-2519-4b8b-ab4c-d2828007fb17"
        },
        {
            "id": "4cbe653c-6d67-44d8-9139-752a01964f15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5e4550f5-2519-4b8b-ab4c-d2828007fb17"
        },
        {
            "id": "6568c891-f300-4612-8fbb-dc08f5ab1417",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "5e4550f5-2519-4b8b-ab4c-d2828007fb17"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
    "visible": true
}