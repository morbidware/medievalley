event_inherited();
if(instance_exists(btnClose))
{
	instance_destroy(btnClose);
}

with(obj_challenge_malus_letter)
{
	isFetching = false;
	if(instance_exists(btnMalus))
	{
		btnMalus.disabled = false;
	}
	if(instance_exists(btnMinus))
	{
		btnMinus.disabled = false;
	}
	if(instance_exists(btnPlus))
	{
		btnPlus.disabled = false;
	}
	if(instance_exists(btnPlay))
	{
		btnPlay.disabled = false;
	}
}