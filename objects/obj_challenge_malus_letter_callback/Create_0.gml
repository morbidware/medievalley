event_inherited();
depth = -15700;

width = 420; // YEET!!!

title = "";
body = "You won " + string(global.challengeMalus_keysWon) + " keys in the last 24 hours. Every key won in the last 24 hours gives you a malus. The calculation goes as follows. For every key won (24 hours): 5 seconds malus, if your account is old (30 days) remove 2 seconds, if you have made a store purchase (30 days) remove 1 more second, if you have made a bundle purchase (30 days) remove 1 more second. You get a maximum malus of 25 seconds. You can lower your malus by waiting or by paying "+string(global.challengeGalasilverPrice)+" Galasilver per second.";
signature = "King Thistle";

margin = 16;
height = 0;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

btnClose = instance_create_depth(0,0,-10003,obj_btn_generic);
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;
btnClose.sprite_index = spr_btn_close_silver;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}

rrot = random_range(-10,10);

w_width = 420;
w_height = 210;