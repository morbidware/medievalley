{
    "id": "5b17d394-333d-4c4e-b3b9-f963a53e4f6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challenge_malus_letter_callback",
    "eventList": [
        {
            "id": "25c9ec85-2b81-4750-b321-8521881e55f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b17d394-333d-4c4e-b3b9-f963a53e4f6a"
        },
        {
            "id": "de28be59-c949-4f31-b74f-388babac0396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5b17d394-333d-4c4e-b3b9-f963a53e4f6a"
        },
        {
            "id": "66bf21ce-2376-468e-a2b4-9780a4096b5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5b17d394-333d-4c4e-b3b9-f963a53e4f6a"
        },
        {
            "id": "22c37bfb-c3c6-4390-a8d0-5ae862f49512",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5b17d394-333d-4c4e-b3b9-f963a53e4f6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}