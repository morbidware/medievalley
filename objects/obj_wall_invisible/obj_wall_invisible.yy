{
    "id": "84548f4a-934c-4d9d-9945-ca1b9addb1e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_invisible",
    "eventList": [
        {
            "id": "52fb4bc8-ad07-43e5-b884-2c2359ae670e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84548f4a-934c-4d9d-9945-ca1b9addb1e7"
        },
        {
            "id": "1b3583c3-f355-4d8d-8213-7c868718f874",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "84548f4a-934c-4d9d-9945-ca1b9addb1e7"
        }
    ],
    "maskSpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
    "overriddenProperties": null,
    "parentObjectId": "71187ee3-21dc-4a51-b7ca-a4ebfb966176",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}