{
    "id": "9290426f-bccd-42af-8fc6-d9f65fb8174f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_pit",
    "eventList": [
        {
            "id": "4eb658b0-74d3-4581-a1c0-a706053e0397",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9290426f-bccd-42af-8fc6-d9f65fb8174f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd393bc4-ccc5-4224-a6e0-8509fea470de",
    "visible": true
}