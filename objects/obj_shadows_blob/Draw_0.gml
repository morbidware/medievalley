// blob shadows
// these are drawn at quality 0 too, otherwise everything really looks like shit
// not a problem since they don't use the blur shader and are fast to draw
event_perform(ev_other,ev_user0);
if(!global.isSurvival){
	
	with (obj_interactable)
	{
		if(canBeDrawn)
		{
			if (!isastructure)
			{
		
				scr_drawShadow(-1);
			}
		}
	}
	with (obj_prefab)
	{
	
		scr_drawShadow(-1);
	}
	with (obj_char)
	{
		if (!visible)
		{
			continue;
		}
	
		scr_drawShadow(-1);
	}
	with (obj_dummy)
	{
		if(!resting)
		{	
			scr_drawShadow(-1);
		}
	}
	with (obj_enemy)
	{
	
		scr_drawShadow(-1);
	}
	with (obj_prefab)
	{
	
		scr_drawShadow(-1);
	}

	
}
else
{
	with (obj_interactable)
	{
		if(canBeDrawn)
		{
			if (!isastructure)
			{
		
				var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
				draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx, y+offsety-global.viewy, diff, diff, 0, c_black, 1);
			}
		}
	}
	/*with (obj_prefab)
	{
	
		var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
		draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx, y+offsety-global.viewy, diff, diff, 0, c_black, 1);
	}*/
	with (obj_char)
	{
		if (!visible)
		{
			continue;
		}
	
		var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
		draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx, y+offsety-global.viewy, diff, diff, 0, c_black, 1);
	}
	with (obj_dummy)
	{
		if(!resting)
		{	
			var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
			draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx, y+offsety-global.viewy, diff, diff, 0, c_black, 1);
		}
	}
	with (obj_enemy)
	{
	
		var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
		draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx, y+offsety-global.viewy, diff, diff, 0, c_black, 1);
	}
	with (obj_prefab)
	{
	
		var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
		draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx, y+offsety-global.viewy, diff, diff, 0, c_black, 1);
	}

}

if (assigned == true)
	{
		gpu_set_blendmode(bm_normal);
		surface_reset_target();
		draw_surface_ext(surf,global.viewx,global.viewy,1,1,0,c_black,global.blobalpha);
		draw_set_alpha(1);
		draw_set_color(c_white);
	}
	assigned = false;