{
    "id": "69bfbf29-36a9-40d7-8769-a701f2f9a887",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadows_blob",
    "eventList": [
        {
            "id": "763541b2-ac66-41a4-852e-868eb0bd9020",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69bfbf29-36a9-40d7-8769-a701f2f9a887"
        },
        {
            "id": "de47e430-6f91-4d3f-b1a2-e93116cb1abb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "69bfbf29-36a9-40d7-8769-a701f2f9a887"
        },
        {
            "id": "7ec1d9ba-31a8-4cd6-87f0-9fa8fc021caf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "69bfbf29-36a9-40d7-8769-a701f2f9a887"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}