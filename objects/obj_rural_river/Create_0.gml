event_inherited();

ds_map_add(grass,"wildgrass",50);
ds_map_add(grass,"hypericum",2);
ds_map_add(grass,"lavender",2);
ds_map_add(grass,"echinacea",2);
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}

ds_map_add(plants,"appletree",15);
ds_map_add(plants,"olivetree",15);
ds_map_add(plants,"beechtree",40);
ds_map_add(plants,"porcinimushroom",8);
ds_map_add(plants,"mint",4);

ds_map_add(boulders,"rock",2);
ds_map_add(boulders,"rockiron",1);
ds_map_add(boulders,"sapling",4);

ds_map_add(mobs,"bear",30);
ds_map_add(mobs,"wolf",20);

