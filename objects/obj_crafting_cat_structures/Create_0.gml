/// @description Insert description here
// You can write your code in this editor

event_inherited();
unlocked = true;

with(instance_create_depth(0,0,-1,obj_recipe_pallet))
{
	index = 0;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_tent))
{
	index = 1;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_bonfire))
{
	index = 2;
	cat = other.id;
	ds_list_add(other.entries, id);
}


with(instance_create_depth(0,0,-1,obj_recipe_house))
{
	index = 3;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_forge))
{
	index = 4;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_kitchen))
{
	index = 5;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_alchemytable))
{
	index = 6;
	cat = other.id;
	ds_list_add(other.entries, id);
}

with(instance_create_depth(0,0,-1,obj_recipe_grindstone))
{
	index = 7;
	cat = other.id;
	ds_list_add(other.entries, id);
}