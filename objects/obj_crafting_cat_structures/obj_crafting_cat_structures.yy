{
    "id": "837151f0-e7a7-4ee7-90b1-6177860d2a42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_structures",
    "eventList": [
        {
            "id": "9ddceb75-273c-4d90-9513-1acad02639bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "837151f0-e7a7-4ee7-90b1-6177860d2a42"
        },
        {
            "id": "6822d177-3de4-4220-b598-1db50ce01ec1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "837151f0-e7a7-4ee7-90b1-6177860d2a42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "888f0a00-b3e8-4594-9be3-fe0a7b7bf591",
    "visible": true
}