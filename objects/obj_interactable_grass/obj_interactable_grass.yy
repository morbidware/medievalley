{
    "id": "5fb3e462-84ff-4af1-bb62-a000f83666a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_grass",
    "eventList": [
        {
            "id": "15793fa3-5f12-495f-b9ab-f4ee350c9bcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5fb3e462-84ff-4af1-bb62-a000f83666a9"
        },
        {
            "id": "409a1578-a829-40e5-a940-66ae4e8ce19f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5fb3e462-84ff-4af1-bb62-a000f83666a9"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c1b46d1-82de-435f-82d7-83dd29b273b3",
    "visible": true
}