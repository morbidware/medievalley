{
    "id": "2e72a4c7-1143-491e-ace0-96325b8c5a9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_challengeGround",
    "eventList": [
        {
            "id": "f8f6c797-0541-4e01-b4c6-c52b7497a8da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e72a4c7-1143-491e-ace0-96325b8c5a9a"
        },
        {
            "id": "5543fc08-2665-454b-9a57-48301e11b184",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2e72a4c7-1143-491e-ace0-96325b8c5a9a"
        },
        {
            "id": "1b4ebe53-f89d-40ae-8d15-2bfa5073b541",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2e72a4c7-1143-491e-ace0-96325b8c5a9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}