/// @description Init/load objects
cols = global.challengeCols;
rows = global.challengeRows;
border = 2;
waterstep = 0;

room_set_width(room, cols*16);
room_set_height(room, rows*16);

// Ground grids
grid = ds_grid_create(cols,rows);		// groundType enum					GRID NO 2
spritegrid = ds_grid_create(cols,rows);	// sprite_index of the single tiles GRID NO 3
indexgrid = ds_grid_create(cols,rows);	// image_index of the single tiles	GRID NO 4
storeygrid = ds_grid_create(cols,rows);	// storey value of the single tiles GRID NO 5

// Other grids
gridaccess = ds_grid_create(cols,rows);	// tiles accessibili (1) oppure no (-1)	GRID NO 6
itemsgrid = ds_grid_create(cols,rows);	// items snappati alla griglia, default -1	GRID NO 7
wallsgrid = ds_grid_create(cols,rows);	// walls nella griglia, default -1	GRID NO 8
pathgrid = mp_grid_create(0,0,cols,rows,16,16);	// griglia per pathfinding
limitgrid = ds_grid_create(ceil(cols/2),ceil(rows/2));	// griglia tileset map limits, diviso 2 perché è a 32px	GRID NO 9

templock_x = ds_list_create();
templock_y = ds_list_create();

var r = 0;
var c = 0;

//fill access grid with 1 (all accessible) and storey grid with 0
repeat (cols)
{
	r = 0;
	repeat (rows)
	{
		ds_grid_set(gridaccess, c, r, 1);
		ds_grid_set(storeygrid, c, r, 0);
		r++;
	}
	c++;
}

logga("creating a new map");

var externalGroundType = GroundType.TallGrass;
var internalGroundType = GroundType.Grass;
if(global.challengeType == ChallengeType.Sickling)
{
	externalGroundType = GroundType.Grass;
	internalGroundType = GroundType.TallGrass;
}
else if(global.challengeType == ChallengeType.Mining || global.challengeType == ChallengeType.Picking)
{
	externalGroundType = GroundType.Grass;
	internalGroundType = GroundType.Soil;
}
else if(global.challengeType == ChallengeType.Watering)
{
	externalGroundType = GroundType.Grass;
	internalGroundType = GroundType.Plowed;
}
else if(global.challengeType == ChallengeType.Hoeing)
{
	externalGroundType = GroundType.TallGrass;
	internalGroundType = GroundType.Grass;
}
c = 0;
repeat(cols)
{
	r = 0;	
	repeat(rows)
	{
		if(r < border || c < border || r > rows - border || c > cols - border)
		{
			ds_grid_set(grid, c, r, externalGroundType);
			ds_grid_set(gridaccess, c, r, -1);
		}
		else if(r < 7 || c < 4 || r >= rows - 4 || c >= cols - 4)
		{
			ds_grid_set(grid, c, r, externalGroundType);
		}
		else if(r < 8 || c < 5 || r >= rows - 5 || c >= cols - 5)
		{
			if(global.challengeType == ChallengeType.Watering)
			{
				ds_grid_set(grid, c, r, internalGroundType);
			}
			else
			{
				ds_grid_set(grid, c, r, choose(externalGroundType, internalGroundType));
			}
		}
		else if(r < 9 || c < 6 || r >= rows - 6 || c >= cols - 6)
		{
			ds_grid_set(grid, c, r, internalGroundType);
		}
		else
		{
			if(global.challengeType == ChallengeType.Hoeing)
			{
				ds_grid_set(grid, c, r, choose(internalGroundType,GroundType.Soil));
			}
			else
			{
				ds_grid_set(grid, c, r, internalGroundType);
			}
			
		}			

		ds_grid_set(itemsgrid, c, r, -1);
		r++;
	}
	c++;
}

if(global.challengeType == ChallengeType.Fighting)
{
	scr_createMapLimits(spr_limit_grass,ceil(border/2),true,true,false,true);
}
else
{
	scr_createMapLimits(spr_limit_grass,ceil(border/2),false,false,false,false);
}
scr_findLimitSprites();
scr_updateTileSprites();

with(obj_water)
{
	event_perform(ev_other,ev_user0);
}

global.gridstring = scr_compressFarmString(grid);
global.itemsgridstring = ds_grid_write(itemsgrid);

instance_create_depth(0,0,0,obj_drawGround);
//instance_create_depth(0,0,0,obj_drawWater);
//instance_create_depth(0,0,0,obj_drawUnderwater);