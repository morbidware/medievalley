{
    "id": "7edf55fe-cf9a-426f-90a2-d9986cd14c6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_potato_plant",
    "eventList": [
        {
            "id": "1869cd93-07fb-48fd-83f6-4b9dba3ee80e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "7edf55fe-cf9a-426f-90a2-d9986cd14c6f"
        },
        {
            "id": "f11d390f-071f-454d-a459-6c257d74c7e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "7edf55fe-cf9a-426f-90a2-d9986cd14c6f"
        },
        {
            "id": "a8644e50-16cd-4960-8e47-ba9e57f6eae8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "7edf55fe-cf9a-426f-90a2-d9986cd14c6f"
        },
        {
            "id": "c4e1272e-f6cd-4041-aef9-cbc1c702d10f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "7edf55fe-cf9a-426f-90a2-d9986cd14c6f"
        },
        {
            "id": "3928ad9f-2a14-4952-8074-a42850008cd6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "7edf55fe-cf9a-426f-90a2-d9986cd14c6f"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
    "visible": true
}