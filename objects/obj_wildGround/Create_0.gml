/// @description Create map from sprites

// map setup
var sizecols = global.wildcellcolumns;//2
var sizerows = global.wildcellrows;//2
waterstep = 0;
tempstorey = 0;
border = global.border;

// set wildmap size
//ogni wildmap room è composta da 4 spr_mappe, dato che ognuna è da 32 pixel, avremo
//32*2 = 64x64 come mappa di base + doppio border (6) e doppio clearance (2) = 72x72
cols = (global.mapsize * sizecols) + (global.border * 2) + (global.clearance * 2);
rows = (global.mapsize * sizerows) + (global.border * 2) + (global.clearance * 2);
mapgrid = ds_grid_create(cols,rows);
var r = 0;
var c = 0;

// create and clean necessary surfaces
surf = surface_create(cols, rows);
surface_set_target(surf);
draw_set_color(c_white);
draw_rectangle(0,0,cols,rows,false);
surface_reset_target();

heightsurf = surface_create(cols, rows);
surface_set_target(heightsurf);
draw_set_color(c_black);
draw_rectangle(0,0,cols,rows,false);
surface_reset_target();

// Ground grids
grid = ds_grid_create(cols,rows);		// groundType enum
spritegrid = ds_grid_create(cols,rows);	// sprite_index of the single tiles
indexgrid = ds_grid_create(cols,rows);	// image_index of the single tiles
storeygrid = ds_grid_create(cols,rows);	// storey value of the single tiles

// Other grids
gridaccess = ds_grid_create(cols,rows);	// tiles accessibili (1) oppure no (-1)
itemsgrid = ds_grid_create(cols,rows);	// items snappati alla griglia, default -1
wallsgrid = ds_grid_create(cols,rows);	// walls nella griglia, default -1
pathgrid = mp_grid_create(0,0,cols,rows,16,16);	// path per AI
limitgrid = ds_grid_create(ceil(cols/2),ceil(rows/2));	// griglia tileset map limits, diviso 2 perché è a 32px

// Other stuff
spawnpointsx = ds_list_create();
spawnpointsy = ds_list_create();
hspx = -1;
hspy = -1;
t = 0;

//fill all grids with default value
c = 0;
repeat (cols)
{
	r = 0;
	repeat (rows)
	{
		ds_grid_set(gridaccess, c, r, 1);
		ds_grid_set(grid, c, r, GroundType.Grass);
		ds_grid_set(wallsgrid, c, r, -1);
		r++;
	}
	c++;
}

//show_debug_message("==== GENERATING WILDLANDS ====");

c = 0;
while(c < sizecols)//2
{
	r = 0;
	while(r < sizerows)//2
	{
		var celldata = ds_grid_get(global.wildgrid, global.wildposx, global.wildposy);
		var cellgrid = ds_grid_create(1,1);
		ds_grid_read(cellgrid, celldata);
		var submap = ds_grid_get(cellgrid, c, r);
		//show_debug_message("Submap value: "+string(submap));
		if (submap > 0)
		{
			//show_debug_message("Drawing a "+string(c)+", "+string(r)+ " ---------- "+string(sprite_get_name(submap)));
			scr_createSubmap(submap, floor(c*32) + global.border + global.clearance, floor(r*32) + global.border + global.clearance);
		}
		r++;
	}
	c++;
}

//show_debug_message("==== GENERATION COMPLETED ====");

// add invisible walls on map border
c = 0;
repeat(cols)//72
{
	r = 0;
	repeat (rows)//72
	{
		if (c == 0 || c == cols-1 || r == 0 || r == rows-1)
		{
			var w = instance_create_depth(8+c*16,8+r*16,0,obj_wall_invisible);
			w.storey_0 = true;
			w.storey_1 = true;
			w.storey_2 = true;
			w.storey_3 = true;
			ds_grid_set(wallsgrid, c, r, w);
		}
		r++;
	}
	c++;
}

// choose one spawnpoint
var spindex = floor(random(ds_list_size(spawnpointsx)-1));
hspx = ds_list_find_value(spawnpointsx,spindex);
hspy = ds_list_find_value(spawnpointsy,spindex);

// enable openings depending by current position on the wild grid
openLeft = false;
openRight = false;
openUp = false;
openDown = false;
if (global.wildposx == 0 && global.wildposy == floor(global.wildmapsizey/2)) openLeft = true;
if (global.wildposx > 0) openLeft = true;
if (global.wildposx < ds_grid_width(global.wildgrid)-1) openRight = true;
if (global.wildposy > 0) openUp = true;
if (global.wildposy < ds_grid_height(global.wildgrid)-1) openDown = true;

// execute operations to finalize the map and navmesh
scr_createMapLimits(spr_limit_grass,ceil(global.border/2),openLeft,openRight,openUp,openDown);
scr_findLimitSprites();
scr_updateTileSprites();
scr_findWallEdges();
scr_setCliffNavigation();
scr_createStairs();

// if the player died in this wildland cell, respawn his stuff
if ((global.wildposy * global.wildmapsizey) + global.wildposx = global.lastdeathcell)
{
	var px = global.lastdeathx;
	var py = global.lastdeathy;
	var items = global.lastdeathinventory;
	for (var i = 0; i < ds_grid_width(items); i++)
	{
		var itemid = ds_grid_get(global.lastdeathinventory,i,0);
		if (itemid != noone)
		{
			var quantity = ds_grid_get(global.lastdeathinventory,i,1);
			var durability = ds_grid_get(global.lastdeathinventory,i,2);
			var itm = scr_gameItemCreate(px,py,obj_pickable,itemid,quantity,0,0,durability);
			itm.isDeathPickable = true;
			itm.x += random(24)-12;
			itm.y += random(24)-12;
		}
	}
}

with(obj_water)
{
	event_perform(ev_other,ev_user0);
}

//now that the entire wild is prepared, fill access grid with 0 (none accessible)
c = 0;
repeat (cols)
{
	r = 0;
	repeat (rows)
	{
		ds_grid_set(gridaccess, c, r, -1);
		r++;
	}
	c++;
}

surface_free(surf);
surface_free(heightsurf);

instance_create_depth(0,0,0,obj_drawGround);
instance_create_depth(0,0,0,obj_drawWater);
instance_create_depth(0,0,0,obj_drawUnderwater);