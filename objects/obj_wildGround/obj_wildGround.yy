{
    "id": "13c1a42a-6d47-4be4-ad99-f5688dc5606d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wildGround",
    "eventList": [
        {
            "id": "7272f10a-0e81-45f8-aca4-057a2ace176a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13c1a42a-6d47-4be4-ad99-f5688dc5606d"
        },
        {
            "id": "c45fcbf5-7e40-46a9-a41b-811910c7f8d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13c1a42a-6d47-4be4-ad99-f5688dc5606d"
        },
        {
            "id": "6d72bdac-655a-458d-b3ef-0b4677901ca9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "13c1a42a-6d47-4be4-ad99-f5688dc5606d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}