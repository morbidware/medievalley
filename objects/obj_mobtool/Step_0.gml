x = owner.x;
y = owner.y;
switch (dir)
{
	case Direction.Up: x = owner.x; y = owner.y-20; image_xscale = 1; image_angle = 90; break;
	case Direction.Down: x = owner.x; y = owner.y-12; image_xscale = 1; image_angle = 180; break;
	case Direction.Left: x = owner.x-4; y = owner.y-16; image_xscale = -1; image_angle = 0; break;
	case Direction.Right: x = owner.x+4; y = owner.y-16; image_xscale = 1; image_angle = 0; break;
}
scr_setDepth(DepthType.Elements);
depth -= 18;