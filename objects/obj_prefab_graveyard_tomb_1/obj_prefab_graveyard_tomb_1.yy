{
    "id": "05854ca5-8d25-4731-8085-4cb428b4c161",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_tomb_1",
    "eventList": [
        {
            "id": "fc15e69e-1143-42b7-9906-20c628d566b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "05854ca5-8d25-4731-8085-4cb428b4c161"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9bc9c17-5125-4a80-93fe-8b2c4b68296c",
    "visible": true
}