event_inherited();

system = part_system_create();
part_system_depth(system,-y-6);

particle = part_type_create();
part_type_sprite(particle,spr_particle_soil,false,false,true);
part_type_orientation(particle,0,360,random(10)-5,0,0);
part_type_alpha3(particle,1,1,0);
part_type_color_mix(particle,make_color_rgb(200,200,200),make_color_rgb(255,255,255));
part_type_gravity(particle,0.02,270);
part_type_direction(particle,10,170,0,0);
part_type_speed(particle,0.3,0.5,0,0);
part_type_life(particle,20,30);

emitter = part_emitter_create(system);
part_emitter_region(system,emitter,x-4,x+4,y-4,y-4,ps_shape_rectangle,ps_distr_linear);

part_emitter_burst(system,emitter,particle,choose(2,3));
alarm_set(0,30);