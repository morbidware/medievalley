{
    "id": "8f6b3dd5-5090-4b18-9870-b85eafd659f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_particle_soil",
    "eventList": [
        {
            "id": "421f9635-f35c-45a8-8578-027b1435c21d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f6b3dd5-5090-4b18-9870-b85eafd659f9"
        },
        {
            "id": "73862b57-f8ec-426f-8f79-63de2741ce05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8f6b3dd5-5090-4b18-9870-b85eafd659f9"
        },
        {
            "id": "7e23ed5d-7f2d-4498-91ce-7538d53c23db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8f6b3dd5-5090-4b18-9870-b85eafd659f9"
        },
        {
            "id": "cc1d0f45-fe9a-4b4c-8d4a-a292115c8ed6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8f6b3dd5-5090-4b18-9870-b85eafd659f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aca52aa6-766a-4f89-ae38-c2917f139d74",
    "visible": true
}