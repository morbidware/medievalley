{
    "id": "31a28af3-298c-4ced-a584-fc7549d9a0ef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_challengetorch",
    "eventList": [
        {
            "id": "e71a1ec1-08c8-49f5-9325-33cc1453c922",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "31a28af3-298c-4ced-a584-fc7549d9a0ef"
        },
        {
            "id": "02844e3a-191c-46b6-b231-55a96b8188d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "31a28af3-298c-4ced-a584-fc7549d9a0ef"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f5483d2d-afe6-473b-9290-4a7192a626a6",
    "visible": true
}