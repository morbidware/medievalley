/// @description Insert description here
// You can write your code in this editor
if(delay > 0)
{
	delay -= global.dt;
	if(delay <= 0)
	{
		visible = true;
		imgspd = 0.3 + random(0.1) * global.dt;
	}
}
if(floor(image_index) >= image_number - 1)
{
	var rx = random(room_width);
	var ry = random(room_height);
	instance_create_depth(rx, ry, -ry, obj_fx_ground_dust);
	instance_destroy();
}
image_speed = imgspd * global.dt;