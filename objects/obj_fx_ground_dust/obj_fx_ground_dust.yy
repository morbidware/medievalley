{
    "id": "b410275d-e00b-4f44-b14d-b1afda2c66b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_ground_dust",
    "eventList": [
        {
            "id": "cb8ac436-e5b0-4535-877c-638d22acdc76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b410275d-e00b-4f44-b14d-b1afda2c66b3"
        },
        {
            "id": "a7ad092d-eb43-475e-b68b-ccc5956d4897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b410275d-e00b-4f44-b14d-b1afda2c66b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
    "visible": true
}