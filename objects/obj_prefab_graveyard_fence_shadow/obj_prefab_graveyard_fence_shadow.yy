{
    "id": "b76a5598-77c4-438f-b289-8833230a5ba3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_fence_shadow",
    "eventList": [
        {
            "id": "9a751c94-39cf-421a-a250-45ad4ecc3f6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b76a5598-77c4-438f-b289-8833230a5ba3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8c7cf0c-bbd7-41a7-bc9e-e834dbcb6ae7",
    "visible": true
}