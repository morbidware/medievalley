/// @description Depth, collision, state
scr_setDepth(DepthType.Elements);

//avoid other enemies
with(obj_enemy)
{
	if(id != other.id)
	{
		var pAx = other.x;
		var pAy = other.y;
		var pBx = x
		var pBy = y;
		var dist = point_distance(pAx, pAy, pBx, pBy);
		if(dist < 8)
		{
			var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
			var off = abs(dist - 8) * 0.6;
			other.x += cos(angle+pi)*off;
			other.y -= sin(angle+pi)*off;
		}
	}
}
if(state != MobState.Flee)
{
	if(stealcrono > 0)
	{
		stealcrono--;
		if(stealcrono <= 0)
		{
			with(instance_nearest(x,y,obj_pickable))
			{
				instance_destroy();
			}
			repeat(10)
			{
				instance_create_depth(x, y,-y,obj_fx_cloudlet);
			}
			instance_destroy();
			exit;
		}
	}

	//avoid obstacles with a collision radius
	with(instance_nearest(x,y,obj_interactable))
	{
		if (collisionradius > 0)
		{
			var pAx = other.x;
			var pAy = other.y;
			var pBx = x;
			var pBy = y;
			var dist = point_distance(pAx, pAy, pBx, pBy);
			if(dist < collisionradius)
			{
				var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
				var off = abs(dist - collisionradius) * 0.6;
				other.x += cos(angle+pi)*off;
				other.y -= sin(angle+pi)*off;
			}
		}
	}
	scr_wallCollisions(id,0,0);
}
else
{
	if(image_alpha > 0)
	{
		image_alpha -= 0.01;
		if(image_alpha <= 0.0)
		{
			instance_destroy();
		}
	}
}
scr_updateMobState(id);