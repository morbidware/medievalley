/// @description Target reached

var hero = instance_find(obj_char,0);
switch(state)
{
	case MobState.Idle:
		crono = idleTimer;	
		break;
		
	case MobState.Chase:
		scr_setMobState(id,MobState.Charge);	
		break;
		
	case MobState.Flee:
		instance_destroy();
		break;
}