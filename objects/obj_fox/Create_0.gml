/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

var hero = instance_find(obj_char,0);
var final = noone;
for(var i = 0; i < instance_number(obj_pickable); i++)
{
	var cand = instance_find(obj_pickable,i);
	var dist = point_distance(hero.x,hero.y,cand.x,cand.y);
	if(dist > 200 && dist < 400)
	{
		final = cand;
		break;
	}
}
/*
if(final == noone)
{
	final = instance_nearest(hero.x,hero.y,obj_pickable);
}
*/
if(final == noone)
{
	instance_destroy();
	exit;
}

x = final.x;
y = final.y;
depth = -y;

fleeAnimSpeed = 2.0;