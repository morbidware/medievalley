{
    "id": "ca7dd6aa-259c-4ee3-9eae-7e405714b649",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fox",
    "eventList": [
        {
            "id": "eb74c898-5e6a-48e2-b943-8d76821a5ca2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ca7dd6aa-259c-4ee3-9eae-7e405714b649"
        },
        {
            "id": "738115a7-b20e-4e31-a752-115963b1bf5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ca7dd6aa-259c-4ee3-9eae-7e405714b649"
        },
        {
            "id": "e28abf43-4e99-403b-b0ee-660b8bc58629",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca7dd6aa-259c-4ee3-9eae-7e405714b649"
        },
        {
            "id": "e5cb501e-6b52-4205-9ea9-c5d77246b428",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "ca7dd6aa-259c-4ee3-9eae-7e405714b649"
        },
        {
            "id": "699517ff-4af7-4360-93ca-13eeaf297bb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "ca7dd6aa-259c-4ee3-9eae-7e405714b649"
        },
        {
            "id": "a957bbf3-bbe0-452c-9c79-cb7f5f7fb260",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca7dd6aa-259c-4ee3-9eae-7e405714b649"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7200c15-8327-43af-a033-1e89c9f39857",
    "visible": true
}