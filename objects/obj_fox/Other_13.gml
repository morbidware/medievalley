/// @description upd->Idle

// While in idle, lower the timer
if(stealcrono > 0)
{
	// When timer is over...
	stealcrono --;
	if(stealcrono <= 0)
	{
		with(instance_nearest(x,y,obj_pickable))
		{
			instance_destroy();
		}
		instance_destroy();
	}
}
var hero = instance_find(obj_char,0);
if(hero != noone)
{
	var distToHero = point_distance(x,y,hero.x,hero.y);
	if(distToHero < sightradius && hero.life > 0)
	{
		lastdirection = scr_directionFromPointAngle(hero.x,hero.y,x,y);
		
		var an = degtorad(point_direction(hero.x,hero.y,x,y));
		targetx = x + cos(an)*1000;
		targety = y - sin(an)*1000;
		animSpeed = fleeAnimSpeed;
		scr_setMobState(id, MobState.Flee);
		exit;
	}
	
}
	