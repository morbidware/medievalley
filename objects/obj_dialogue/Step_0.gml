if(cursor < string_length(str))
{
	cursor += 2;
	if(cursor > string_length(str))
	{
		cursor = string_length(str);
	}
}
blinkCrono --;
if(blinkCrono <= 0)
{
	if(blinkState == 0)
	{
		blinkState = 1;
		blinkCrono = blinkTimer1;
	}
	else
	{
		blinkState = 0;
		blinkCrono = blinkTimer2;
	}
}