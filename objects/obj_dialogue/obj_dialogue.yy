{
    "id": "64b25fb7-b43c-4d1d-8456-3ca4dff29656",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dialogue",
    "eventList": [
        {
            "id": "0d7c7773-bacf-4974-a962-790c604496aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "64b25fb7-b43c-4d1d-8456-3ca4dff29656"
        },
        {
            "id": "36026bab-d548-4451-8491-a9a2107ab80a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64b25fb7-b43c-4d1d-8456-3ca4dff29656"
        },
        {
            "id": "4bc9c221-1afd-4a2f-9860-5b1171a8f8aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "64b25fb7-b43c-4d1d-8456-3ca4dff29656"
        },
        {
            "id": "c1a54b91-61dc-4e9e-8634-d49651cfedd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "64b25fb7-b43c-4d1d-8456-3ca4dff29656"
        },
        {
            "id": "bac967ae-b2ab-4c0c-9c49-610357e8e3c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "64b25fb7-b43c-4d1d-8456-3ca4dff29656"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}