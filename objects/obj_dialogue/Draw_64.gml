var w = 700;
var h = 100;
var m = 20;

var tempstr = string_copy(str,1,cursor);

// BG and title
scr_nine_slice_at(spr_9slice_letter, display_get_gui_width()/2.0, display_get_gui_height() - (h/2)-30, w, h, 2);
draw_set_font(global.FontStandardBoldOutlineX2);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_text((display_get_gui_width()/2) - ( w/2 ) + 14, display_get_gui_height() - (h/2) - 90, title);

// Text
draw_set_color(c_black);
draw_set_font(global.FontStandard);
draw_text_ext((display_get_gui_width()/2.0)-(w/2)+m,display_get_gui_height() - (h/2)-30 - (h/2) + m, tempstr, 12, w-m-m);

// Cursor
draw_set_color(c_white);
if(cursor == string_length(str))
{
	draw_sprite_ext(spr_farmboard_btn_next,0,display_get_gui_width()/2.0+(w/2)-30, display_get_gui_height() - 50, 2.0,2.0,0.0,c_white,blinkState);
}