{
    "id": "e657b204-5167-4959-9d2e-8b5adab9183e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_house",
    "eventList": [
        {
            "id": "20201a5e-36cb-447a-b276-bc8b20592821",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e657b204-5167-4959-9d2e-8b5adab9183e"
        },
        {
            "id": "90f9049c-d476-4f8a-8ff0-558bfa5e9f95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e657b204-5167-4959-9d2e-8b5adab9183e"
        },
        {
            "id": "25fa0369-d1e2-48e9-bff0-68467f517e29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "e657b204-5167-4959-9d2e-8b5adab9183e"
        },
        {
            "id": "afd3ac11-4bbe-4d69-88be-d7118ddb6b71",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "e657b204-5167-4959-9d2e-8b5adab9183e"
        },
        {
            "id": "bc2b67f6-4a0d-44af-947d-62bd84c741a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e657b204-5167-4959-9d2e-8b5adab9183e"
        },
        {
            "id": "4b4b3a97-0597-4030-a950-39ff017cda79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "e657b204-5167-4959-9d2e-8b5adab9183e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1dc1e004-3bcc-4aac-82f1-f683c02212d9",
    "visible": true
}