/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(growstage == 1)
{
	draw_set_alpha(1.0);

	var barsize = 96;
	var uiheight = 96;

	draw_set_color(c_black);
	draw_rectangle(x-(barsize/2)-1, y-uiheight-3, x+(barsize/2)+1, y-uiheight, false);

	draw_set_color(make_color_rgb(64,64,64));
	draw_rectangle(x-(barsize/2), y-uiheight-2, x+(barsize/2), y-uiheight-1, false);
	
	draw_set_color(c_white);
	draw_rectangle(x-(barsize/2), y-uiheight-2, x-(barsize/2) + barsize - floor(barsize * (growcrono/growtimer)), y-uiheight-1, false);
}
