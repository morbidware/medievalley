/// @description Custom setup
shadowType = ShadowType.None;
shadowOnWalls = true;
shadowLength = 42;
event_inherited();
ignoregroundlimits = true;
if (growstage != 2 && growcrono <= 0)
{
	growstage = 2;
}