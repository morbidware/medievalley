// Inherit the parent event
event_inherited();

if (invinciblecrono > 0)
{
	invinciblecrono -= global.dt;
}
//if(!global.isSurvival)
{
	if(char != noone)
	{
		var dist = point_distance(x,y,char.x,char.y);
		if(dist < 12)
		{
			if(canShake)
			{
				shakeAmplitude = 12;
				canShake = false;
			}
		}
		else
		{
			canShake = true;
		}
	}
	else
	{
		char = instance_find(obj_char,0);	
	}

	if(shakeAmplitude > 1)
	{
		shakeAngle += (shakeAmplitude/15.0)*global.dt;
		shakeAmplitude *= 0.95;
		image_angle = sin(shakeAngle)*shakeAmplitude;
	}
	else
	{
		shakeAngle = 0;
		shakeAmplitude = 0;
		image_angle = 0;
	}
}