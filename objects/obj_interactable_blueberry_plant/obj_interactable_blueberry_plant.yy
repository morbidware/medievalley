{
    "id": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_blueberry_plant",
    "eventList": [
        {
            "id": "b81da075-60fa-4cf0-b31e-da5e361ba39f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "ae2cbfb9-adbe-45db-8916-74278e8b73e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "247669c7-e217-4013-b87d-43d487e4a861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "df753c38-b203-417c-883f-75bcb16b86ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "1d151667-b56e-40f7-ba8f-0eb3e4e944f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "365e1bcb-44b8-48a7-851b-b08d0698cc35",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "e7c82a59-846a-4ae8-889b-5c84fb18f1b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        },
        {
            "id": "a2f89cae-bcfa-4d57-a417-b87cb62746a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e7c55463-1b60-4781-82e9-e2e8aff2b1e6"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58f26c55-1885-4013-8b21-e980eda92529",
    "visible": true
}