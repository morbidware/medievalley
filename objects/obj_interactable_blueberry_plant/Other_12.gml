/// @description get chopped by sickle
if (invinciblecrono > 0)
{
	exit;
}

if (!audio_is_playing(snd_sickle))
{
	var s = audio_play_sound(snd_sickle,0,false);
	audio_sound_pitch(s, 0.85 + random (0.35));
}
if (growstage > 1)
{
	growstage--;
	invinciblecrono = 20;
	image_index = growstage-1;
	if(!global.ismobile)
	{
		part_emitter_burst(ps,pe_grass,pt_flower,2);
		part_emitter_burst(ps,pe_grass,pt_grass,2);
	}
}	
else
{
	if(!global.ismobile)
	{
		part_emitter_burst(ps,pe_grass,pt_grass,8);
	}
	scr_dropItems(id,"wildgrass");
	
	if(room == roomChallenge)
	{
		with(obj_gameLevelChallenge)
		{
			if(challengeState == 1)
			{
				total ++;
				//tempGold += 2;
				global.finishedInteractions ++;
				with(obj_char)
				{
					var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
					p.val = global.finishedInteractionsValue;
				}
			}
		}
		
	}
	if(!isNet)
	{
		scr_getGoldOnline(ActorState.Sickle);
	}
	isNet = false;
	
	instance_destroy();
}