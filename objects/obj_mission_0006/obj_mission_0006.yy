{
    "id": "bf7779b2-6981-4c34-8ef1-68b5d0159e6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mission_0006",
    "eventList": [
        {
            "id": "c39dada6-7fe0-4ae8-b11c-cc25571dafd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf7779b2-6981-4c34-8ef1-68b5d0159e6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8299d0fd-0188-4419-ba64-b8c0f3b131ce",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}