///@description Back to the Primitive - Craft the spear.
event_inherited();

missionid = "0006";
title = "Back to the Primitive";
shortdesc = "Craft the spear.";
longdesc = "Gather the required materials ad build a spear. This way you will be able to hunt down wild animals.";
silent = false;

ds_map_add(rewards,obj_reward_0006,1);
ds_map_add(required,"spear",1);

ds_list_clear(hintTexts);
ds_list_add(hintTexts, "Gather required resources to craft a Spear.");
ds_list_clear(hintImages);
ds_list_add(hintImages, spr_pickable_spear);

scr_storeMissions();