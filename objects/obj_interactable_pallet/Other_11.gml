/// @description can be interacted
// You can write your code in this editor
//show_debug_message("can be interacted?");
/*if(growstage >= 2)
{*/
if(growstage == 1)
{
	interactionPrefixLeft = "Rebuild";
	var canbecrafted = true;
	var key = ds_map_find_first(ingredients);

	for(var i = 0; i < ds_map_size(ingredients); i++)
	{
		var ingredient = key;
		var quantity = ds_map_find_value(ingredients,key);
		
		var tot = 0;
		for(var j = 0; j < instance_number(obj_inventory); j++)
		{
			var inv = instance_find(obj_inventory,j);
			if(inv.itemid == ingredient)
			{
				tot += inv.quantity;
			}
		}
	
		if(tot < quantity)
		{
			canbecrafted = false;
			break;
		}
		else 
		{
			key = ds_map_find_next(ingredients, key);
		}
	}
	global.retval = canbecrafted;
}
else if(growstage == 2)
{
	var pla = instance_find(obj_char,0);
	if(pla.state == ActorState.Rest)
	{
		interactionPrefixLeft = "Exit";
	}
	else
	{
		interactionPrefixLeft = "Sleep in";
	}
	global.retval = true;
}

var handState = global.game.player.handState;
if(handState == HandState.Hammer)
{
	global.retval = true;
	interactionPrefixRight = "Dismantle";
}
else
{
	interactionPrefixRight = "";
}

/*}
else
{
	interactionPrefixLeft = "";
	interactionPrefixRight = "";
	global.retval = false;
}*/
