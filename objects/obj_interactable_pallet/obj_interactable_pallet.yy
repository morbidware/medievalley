{
    "id": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_pallet",
    "eventList": [
        {
            "id": "44e8ce3d-d689-4bb8-9e7f-a320801b0e41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        },
        {
            "id": "a1e829a0-81ee-4540-8845-184a6a87ea05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        },
        {
            "id": "49f17a43-86da-4484-9662-53343700f045",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        },
        {
            "id": "8dc5689f-164a-45d7-98ff-4271a584dd09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        },
        {
            "id": "3dbe4df9-d311-4a00-8b6c-ca9e3f86554f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        },
        {
            "id": "c34648d7-f031-4f40-98e0-8fb2cbe86178",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        },
        {
            "id": "e4121410-df0e-4a14-aa48-2381130866f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "f7ee9c40-5a3a-4f2d-8262-09b111f1f65d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf5f8e8e-3a9a-453c-bfa0-efce6b91e31f",
    "visible": true
}