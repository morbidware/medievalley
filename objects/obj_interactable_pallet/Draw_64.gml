// Inherit the parent event
event_inherited();

var w = 174;//160
var h = 103;//192
var px = pos_get_relative_x()-w/2;
var py = pos_get_relative_y() - 138+(10*tooltip_alpha);

var char = instance_find(obj_char,0);

if(highlight && global.othersocketid == "")
{
	if(growstage == 1 && char.state != ActorState.Rebuild)
	{
		if(tooltip_alpha < 1.0)
		{
			tooltip_alpha += 0.05;
		}
	}
}
else
{
	if(tooltip_alpha > 0.0)
	{
		tooltip_alpha -= 0.05;
	}
}

if(char.state == ActorState.Rebuild)
{
	if(tooltip_alpha > 0.0)
	{
		tooltip_alpha -= 0.05;
	}
}

//if(highlight && growstage == 1 && char.state != ActorState.Rebuild)
if(tooltip_alpha > 0.0)
{
	draw_set_alpha(tooltip_alpha);
	draw_sprite(spr_upgradeinfo_bg,0,px,py);
	
	//draw_sprite(spr_upgradeinfo_pointer,0,px+7,py+h/2);
	draw_sprite(spr_upgradeinfo_pointer,0,px+w/2,py+h-8);

	draw_set_font(global.FontTitles);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_color($031F32);
	draw_text(px+w/2,py+22,"Rebuild");	
	draw_set_color($074797);
	draw_text(px+w/2,py+20,"Rebuild");	
	draw_set_color(c_white);
	var oy = 62;
	
	draw_sprite(spr_recipeinfo_bg_separator,0, px+w/2, py+46);
	
	var startx = -16;
	if(ds_map_size(ingredients) == 1)
	{
		startx = 0;
	}
	else if(ds_map_size(ingredients) == 2)
	{
		startx = -24;
	}
	if(ds_map_size(ingredients) == 3)
	{
		startx = -48;
	}
	var key = ds_map_find_first(ingredients);
	for(var i = 0; i < ds_map_size(ingredients); i++)
	{
		var ingredient = key;
		var quantity = ds_map_find_value(ingredients,key);
		var ox = px + (w/2) + startx + (48*i);
	
		var tot = 0;
		for(var j = 0; j < instance_number(obj_inventory); j++)
		{
			var inv = instance_find(obj_inventory,j);
			if(inv.itemid == ingredient)
			{
				tot += inv.quantity;
			}
		}
	
		scr_draw_sprite_enclosed(scr_asset_get_index("spr_pickable_"+ingredient),0,ox,py+oy,32,1);
		//draw_sprite(scr_asset_get_index("spr_inventory_"+ingredient),0,ox,py+oy-2);
		draw_set_font(global.FontStandardOutline);
		if(tot < quantity)
		{
			draw_set_color(c_red);
		}
		else
		{
			draw_set_color(c_white);
		}
		draw_text(ox,py+oy+18,string(tot)+"/"+string(quantity));
	
		key = ds_map_find_next(ingredients, key);
	}
	//draw_sprite(spr_recipeinfo_bg_separator,0, x+w/2, y+oy+32);
	draw_set_color(c_white);
	draw_set_alpha(1.0);
}