{
    "id": "107371ee-b131-4ba8-8ffc-1f13f83e2f9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mob_dummy",
    "eventList": [
        {
            "id": "6f987696-95d8-44b9-8cfd-43fc32f1611b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "107371ee-b131-4ba8-8ffc-1f13f83e2f9a"
        },
        {
            "id": "895d69cf-09df-4ff0-9f95-9a2e7f50e83e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "107371ee-b131-4ba8-8ffc-1f13f83e2f9a"
        },
        {
            "id": "997a2971-d123-421e-a990-5010aee43677",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "107371ee-b131-4ba8-8ffc-1f13f83e2f9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ea533252-0ed6-4cc2-9d4a-57d3dc92a987",
    "visible": true
}