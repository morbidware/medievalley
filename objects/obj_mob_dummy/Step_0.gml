/// @description

// ------------ SET DEPTH & GET STOREY
scr_setDepth(DepthType.Elements);
storey = ds_grid_get(ground.storeygrid, clamp(floor(x/16),0,ground.cols-1), clamp(floor(y/16),0,ground.rows-1));

// ------------ EQUIPPED TOOL TIMER
/*if (eq_timer > 0)
{
	eq_timer -= global.dt;
}*/

// ------------ MOVE MOB
if (enablelerp)
{
	//lerp character
	var dir = point_direction(x,y,tgt_x,tgt_y);
	var dist = floor(point_distance(x,y,tgt_x,tgt_y));
	var c = cos(degtorad(dir));
	var s = sin(degtorad(dir));
	if (dist > warpthreshold || dist < spd)
	{
		x = tgt_x;
		y = tgt_y;
	}
	else
	{
		x += c * spd * global.dt;
		y -= s * spd * global.dt;
	}
}
else
{
	x = tgt_x;
	y = tgt_y;
}
if(attacktimer>=0){
	attacktimer--;
}
/*if (stamina <= 0)
{
	sweatCrono--;
	if(sweatCrono <= 0)
	{
		sweatCrono = 30;
		var sw = instance_create_depth(x+random_range(-4,4),y-28,depth-2,obj_fx_sweat);
		sw.pla = id;
		sw.ox = sw.x - x;
		sw.oy = sw.y - y;
	}
}*/