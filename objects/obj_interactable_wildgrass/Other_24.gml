/// @description Early setup
growstage = choose(1,2);

if(global.isChallenge)
{
	growstage = 2;
	growcrono = 0;
	life = 2;
	image_index = growstage-1;
}