{
    "id": "fb39981e-e502-4542-a42e-4ed7dcf5a948",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_wildgrass",
    "eventList": [
        {
            "id": "dc0bd392-4bf6-410c-91fd-8b9d4a617888",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        },
        {
            "id": "4737a193-d916-4fdf-8272-801e3cb1eb85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        },
        {
            "id": "7da3187a-f86c-495e-8fea-f1fb09375a2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        },
        {
            "id": "a3dee1c5-e10c-4590-88d5-6add962ca7ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        },
        {
            "id": "3919ad0a-fb92-40ed-b30b-14e029d0e309",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        },
        {
            "id": "cbc582dc-751d-4f8e-bd82-72f83462fd7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        },
        {
            "id": "fef33446-5abc-41b5-93cc-7b827e19f448",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "fb39981e-e502-4542-a42e-4ed7dcf5a948"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cb762e9d-9ff2-458e-8f0a-784dcdabc941",
    "visible": true
}