event_inherited();

sprite_index = choose(spr_interactable_wildgrassdark,spr_interactable_wildgrass);
image_xscale = choose(-1.0,1.0);
image_speed = 0;
image_index = growstage-1;
invinciblecrono = 0;
hasWaveShader = false;
char = noone;
//grass particles, look at all the code for a fucking grass blade
if(!global.ismobile)
{
	ps = part_system_create();
	part_system_depth(ps,-y-1);

	pt_grass = part_type_create();
	part_type_sprite(pt_grass,spr_particle_grass,false,false,false);
	part_type_orientation(pt_grass,9,360,0,0,0);
	part_type_alpha3(pt_grass,1,1,0);
	part_type_gravity(pt_grass,0.06,270);
	part_type_direction(pt_grass,60,120,0,0);
	part_type_speed(pt_grass,1,1.5,0,0);
	part_type_life(pt_grass,30,50);

	//also more for a flower
	pt_flower = part_type_create();
	part_type_sprite(pt_flower,spr_particle_flower,false,false,false);
	part_type_orientation(pt_flower,9,360,0,0,0);
	part_type_alpha3(pt_flower,1,1,0);
	part_type_gravity(pt_flower,0.06,270);
	part_type_direction(pt_flower,60,120,0,0);
	part_type_speed(pt_flower,1,1.5,0,0);
	part_type_life(pt_flower,30,50);

	pe_grass = part_emitter_create(ps);
	part_emitter_region(ps,pe_grass,x-4,x+4,y-8,y,ps_shape_rectangle,ps_distr_linear);
}
shakeAmplitude = 0;
shakeAngle = 0;
canShake = false;
