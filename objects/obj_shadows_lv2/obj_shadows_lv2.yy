{
    "id": "3fd40ee1-de06-4ca6-8252-b95687ff3510",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadows_lv2",
    "eventList": [
        {
            "id": "1ff9e43f-f918-41d4-aa93-211b4f28190e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3fd40ee1-de06-4ca6-8252-b95687ff3510"
        },
        {
            "id": "cc43e2df-0078-4fc9-8342-a6ac035a2fa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3fd40ee1-de06-4ca6-8252-b95687ff3510"
        },
        {
            "id": "1bced511-189c-4787-8986-edb5d8f39ef6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3fd40ee1-de06-4ca6-8252-b95687ff3510"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}