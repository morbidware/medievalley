// blurred shadows
if (global.graphicsquality == 0){exit;}
event_perform(ev_other,ev_user0);
with (obj_fx_cast_shadow_cliff)
{
	if (storey < 3)
	{
		continue;
	}
	
	scr_drawShadow(storey-3);
}
with (obj_prefab)
{
	if (storey < 2)
	{
		continue;
	}
	
	scr_drawShadow(storey-2);
}
with (obj_interactable)
{
	if(canBeDrawn)
	{
		if (storey < 2)	
		{
			continue;
		}
		
		scr_drawShadow(storey-2);
	}
}
with (obj_wall)
{
	if (storey_0 == true || storey_1 == true)
	{
		continue;
	}
	
	if (visible)
	{
		if (storey_2 == true) scr_drawShadow(0);
		else if (storey_3 == true) scr_drawShadow(1);
	}
}
with (obj_char)
{
	if (storey < 2 || !visible)
	{
		continue;
	}
	
	scr_drawShadow(storey-2);
}
with (obj_dummy)
{
	if (storey < 2 || !visible || resting)
	{
		continue;
	}
	
	scr_drawShadow(storey-2);
}
with (obj_wildlife)
{
	if (storey < 2)
	{
		continue;
	}
	
	scr_drawShadow(storey-2);
}
with (obj_dummy)
{
	if (storey < 2)
	{
		continue;
	}
	
	scr_drawShadow(storey-2);
}
with (obj_enemy)
{
	if (storey < 2)
	{
		continue;
	}
	
	scr_drawShadow(storey-2);
}

if (assigned == true)
{
	gpu_set_blendmode(bm_normal);
	surface_reset_target();
	if (global.enableshadowshader)
	{
		shader_set(shaderFullscreenShadow);
		shader_set_uniform_f(scrx,display_get_width());
		shader_set_uniform_f(scry,display_get_height());
	}
	draw_surface_ext(surf,global.viewx,global.viewy,1,1,0,c_black,global.shadowalpha);
	shader_reset();
	draw_set_alpha(1);
	draw_set_color(c_white);
}
assigned = false;