{
    "id": "35a33bd6-8800-4b8e-a08f-7fdb161623be",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_abandoned_castle_a",
    "eventList": [
        {
            "id": "9a7d0c25-f3e2-4cb9-b44a-5f51897217b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "35a33bd6-8800-4b8e-a08f-7fdb161623be"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4c21a8d6-92ef-4e49-9ebe-08d7e4a92fc6",
    "visible": true
}