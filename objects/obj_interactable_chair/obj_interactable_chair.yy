{
    "id": "2e40684e-6996-4b9a-8780-0b0ae88f1b19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_chair",
    "eventList": [
        {
            "id": "1cfeb957-b0e7-4238-8e01-2f47970d88a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "2e40684e-6996-4b9a-8780-0b0ae88f1b19"
        },
        {
            "id": "02bf5a1a-cd50-4fb5-ba35-6db380d619a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2e40684e-6996-4b9a-8780-0b0ae88f1b19"
        },
        {
            "id": "5928d1b0-364a-4aef-aa5e-76afa270db81",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "2e40684e-6996-4b9a-8780-0b0ae88f1b19"
        },
        {
            "id": "378bac49-8b26-43ef-ae38-236d280bda07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "2e40684e-6996-4b9a-8780-0b0ae88f1b19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f4fdcc4-4e2b-4a46-9bd2-da8c190393f3",
    "visible": true
}