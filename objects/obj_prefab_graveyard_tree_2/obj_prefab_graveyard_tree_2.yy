{
    "id": "1c4be655-5e3f-4ea2-9ad9-8f5d7ea98327",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_tree_2",
    "eventList": [
        {
            "id": "c7b31e0d-b9b4-4222-bb77-296336972f62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c4be655-5e3f-4ea2-9ad9-8f5d7ea98327"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5f535c8-f9e1-4b21-8409-d09cc186884d",
    "visible": true
}