/// @description Debug drawing
exit;

var r = 0;
var c = 0;

draw_set_alpha(0.5);
repeat(cols)
{
	r = 0;
	repeat(rows)
	{
		//var value = ds_grid_get(grid,c,r);
		draw_set_font(global.FontNumbers);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		//draw_text(8+c*16, 8+r*16, string(c)+" "+string(r));
		var ga = ds_grid_get(gridaccess,c,r);
		if (ga < 0)
		{
			draw_text(8+c*16, 8+r*16, "0");
		}
		else
		{
			draw_text(8+c*16, 8+r*16, "1");
		}
		draw_sprite(spr_outline_16,0,8+c*16, 8+r*16);
		//show_debug_message(string(c)+" "+string(r)+" "+string(value));
		/*
		if(value == GroundType.Grass)
		{
			draw_sprite(spr_ground_grass, 0, 16+c*32, 16+r*32);
		}
		else if(value == GroundType.Soil)
		{
			draw_sprite(spr_ground_soil, 0, 16+c*32, 16+r*32);
		}
		else
		{
			draw_sprite(spr_ground_soil, 0, 16+c*32, 16+r*32);
		}
		*/
		r++;
	}
	c++;
}
draw_set_alpha(1);


