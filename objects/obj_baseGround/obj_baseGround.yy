{
    "id": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_baseGround",
    "eventList": [
        {
            "id": "8c8a1d21-1b87-4998-8e6b-8bb2e1ac7f66",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77040b30-4feb-436d-8801-61dcc0d4f657"
        },
        {
            "id": "55affe55-649f-4909-8f28-49aaec11e69a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77040b30-4feb-436d-8801-61dcc0d4f657"
        },
        {
            "id": "f7bd6948-d188-4673-91ed-7442c221bad7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77040b30-4feb-436d-8801-61dcc0d4f657"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}