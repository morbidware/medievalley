{
    "id": "fbfc821e-3309-439a-bb7a-d49f8b9bbf4a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rural_edge_r",
    "eventList": [
        {
            "id": "6d69fc37-51b8-4868-8dec-e00020b51b90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fbfc821e-3309-439a-bb7a-d49f8b9bbf4a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "92e15c9b-b197-4392-83fb-90e6eba834a6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}