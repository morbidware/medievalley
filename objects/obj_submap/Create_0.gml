/// @description Init

// *** MO TE SPIEGO ***
// DS Maps indexes contain all the possible spawns of this submap.
// DS Maps values are the chances (from 0 to 100) for that spawn to appear in the submap.

// IMPORTANT: all spawn chance values are summed up together! For example, 3 objects with a 10% chance each
// will produce a total of 30% (10+10+10) meaning that there's a 70% chance of not spawning any object.
// Having 3 items with 60% each will overflow to 180%: in this case, first item will have 60%, second item
// only 40%, and third item 0% since it's way pas the 100% limit.

// Usually every tile of the submap will try to spawn something (except grass, landmarks, etc).
// Examples:
// - Tallgrass tries to spawn a tree on every tile, as long as there is some space between the trees.
//	 A total of 100% spawn chance will generate a nicely dense forest.
// - Soil will do the same, but with boulders instead of trees.
//	 A total of 100% spawn chance will create a very unrealistic zone filled with rocks.

// Natural elements
grass = ds_map_create();
plants = ds_map_create();
boulders = ds_map_create();

// Treasures
chests = ds_map_create();
items = ds_map_create();

// Enemies
mobs = ds_map_create();
boss = ds_map_create();

// Structures
landmark = noone;
wall = obj_wall;

// Settings
keeporientation = true;
canmirrorx = false;