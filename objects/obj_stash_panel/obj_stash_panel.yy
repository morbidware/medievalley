{
    "id": "f25bca8f-a13e-468c-9386-ebe35a4121d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_stash_panel",
    "eventList": [
        {
            "id": "ff643a73-0d41-43ca-88b9-6b07167504ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f25bca8f-a13e-468c-9386-ebe35a4121d1"
        },
        {
            "id": "ca24503a-219a-46fc-a3a5-fac38f91a01d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f25bca8f-a13e-468c-9386-ebe35a4121d1"
        },
        {
            "id": "0d5ebb79-9ddb-44fc-8d40-1fd085197a99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "f25bca8f-a13e-468c-9386-ebe35a4121d1"
        },
        {
            "id": "76cd7c62-d248-4c5c-9456-e3863a200bf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f25bca8f-a13e-468c-9386-ebe35a4121d1"
        },
        {
            "id": "c82b815c-9965-499c-9308-9e9844bd88be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "f25bca8f-a13e-468c-9386-ebe35a4121d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}