
// Remove highlighted object to avoid interactions while this menu is open
with (obj_char)
{
	highlightedobject = noone;	
}

// Fade in
if(state == 0)
{
	if(t < 1.0)
	{
		t += (1.0/room_speed)*global.dt;
		y = scr_easeOutBack(t, -134, 268, 1.0);
		//y = scr_easeOutBack(t, display_get_gui_height()/2-display_get_gui_height(), display_get_gui_height(), 1.0);
	}
}
// Fade out
else if(state == 1)
{
	y = scr_easeInBack(t, 134, -268, 1.0);
	//y = scr_easeInBack(t, display_get_gui_height()/2, display_get_gui_height(), 1.0);
	if(t < 1.0)
	{
		t += (1.0/room_speed)*global.dt;
		if(t >= 1.0)
		{
			instance_destroy();
		}
	}
}
x = display_get_gui_width()/2;


//event_inherited();

var startx = -166;
var starty = -76;

if (instance_exists(btnClose))
{
	btnClose.ox = 208;
	btnClose.oy = -108;
}

// Align objects to grid
var c = 0;
var r = 0;
var index = -1;
var obj = noone;
repeat (rows)
{
	c = 0;
	repeat (cols)
	{
		index = global.inv_robelimit + (r * cols) + c;
		obj = ds_grid_get(inv.inventory,index,0);
		if (obj > -1 && instance_exists(obj))
		{
			obj.x = x + startx + (spacing * c) + (cellsize * c);
			obj.y = y + starty + (spacing * r) + (cellsize * r);
		}
		c++;
	}
	r++;
}

/*
// Align objects to grid
for (var i = global.inv_robelimit; i < global.inv_stashlimit; i++)
{
	var obj = ds_grid_get(inv.inventory,i,0);
	if (obj != -1)
	{
		if(obj.state == 0)
		{
			var col = obj.slot-global.inv_robelimit;
			var row = 0;
			while(col > 9)
			{
				col-= 10;
				row ++;
			}
			obj.x = startx + 38 * col;
			obj.y = starty + 38 * row;
		}
	}
}
*/