{
    "id": "d0544db2-c9e6-43e5-9aa9-235395440f39",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_tomb_4",
    "eventList": [
        {
            "id": "a718a003-7ff6-4616-b3a3-efdd9d05846f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0544db2-c9e6-43e5-9aa9-235395440f39"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f81bf287-cbad-4f43-b476-fbd51e368fcc",
    "visible": true
}