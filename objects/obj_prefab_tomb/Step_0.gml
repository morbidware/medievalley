/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(room == roomChallenge || room == roomSurvival)
{
	exit;
}
if(crono > 0)
{
	crono -= global.dt;
}
else if (crono <= 0)
{
	crono = 1000000;
	global.stamina = 100;
	global.life = 100;
	scr_storeUserdata();
	with(obj_gameLevel)
	{
		event_perform(ev_other,ev_user0);
	}
}