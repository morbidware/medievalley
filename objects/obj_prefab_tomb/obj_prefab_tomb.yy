{
    "id": "7d1bed96-8e57-4fb1-bb88-d10ba9dca17a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_tomb",
    "eventList": [
        {
            "id": "dcea1963-adeb-47ea-aa72-accc8d0fa204",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d1bed96-8e57-4fb1-bb88-d10ba9dca17a"
        },
        {
            "id": "0d0778e6-6428-47c1-86d8-95e8d05bbbed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d1bed96-8e57-4fb1-bb88-d10ba9dca17a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2f0970be-dd88-48a5-94de-1b249c940b6a",
    "visible": true
}