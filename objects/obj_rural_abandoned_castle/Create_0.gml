event_inherited();

ds_map_add(grass,"wildgrass",50);
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}

ds_map_add(plants,"appletree",20);
ds_map_add(plants,"beechtree",30);
ds_map_add(plants,"porcinimushroom",3);

// No boulders in this submap

ds_map_add(mobs,"archer",50);

landmark = obj_prefab_abandoned_castle;
wall = obj_wall_abandoned_castle;

keeporientation = true;