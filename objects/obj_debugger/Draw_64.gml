/// @description Insert description here
// You can write your code in this editor
if (!global.online)
{
	exit;
}

draw_set_font(global.Font);

draw_set_halign(fa_left);
draw_set_valign(fa_top);


var sx = 200;
var sy = 20;
var oy = 10;

draw_text(sx,sy,"USERNAME: " + string(global.username));
draw_text(sx,sy+oy*1,"USERID: " + string(global.userid));
draw_text(sx,sy+oy*2,"SOCKETID: " + string(global.socketid));
draw_text(sx,sy+oy*3,"PICKABLES: " + string(global.pickables));

draw_text(sx,sy+oy*5,"OTHER NAME: " + string(global.otherusername));
draw_text(sx,sy+oy*6,"OTHER ID: " + string(global.otheruserid));
draw_text(sx,sy+oy*7,"OTHER SOCK: " + string(global.othersocketid));
draw_text(sx,sy+oy*8,"OTHER PICK: " + string(global.otherpickables));

draw_text(sx,sy+oy*10,str);
