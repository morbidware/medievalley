{
    "id": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_rock",
    "eventList": [
        {
            "id": "741c23be-c97f-4889-98b3-95e2ba1738ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        },
        {
            "id": "72806184-085f-47e1-b068-9c66a475633d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        },
        {
            "id": "f63849ae-ee6e-4c9f-961d-30f288f55989",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        },
        {
            "id": "99bb9b7f-f12c-4088-92ae-b7fa100560f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        },
        {
            "id": "fca1b098-c3fa-47e7-999d-98a4888e07ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        },
        {
            "id": "69e7eae0-af49-49ae-9b96-b9a11302abe6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        },
        {
            "id": "11241d00-13cd-4c1e-a67c-665f6372294b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "ee50c6dc-42eb-4801-b2e1-d533160b2ff1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ea658d2c-1ac4-4bea-a3ff-3c48e3a7141c",
    "visible": true
}