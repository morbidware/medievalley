/// @description finish interaction
// You can write your code in this editor
event_perform(ev_other,ev_user4);

if(room == roomChallenge)
{
	with(obj_gameLevelChallenge)
	{
		if(challengeState == 1)
		{
			total ++;
			//tempGold += 10;
			global.finishedInteractions ++;
			with(obj_char)
			{
				var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
				p.val = global.finishedInteractionsValue;
			}
		}
	}
}

//GENERATE GOLD
if(!isNet)
{
	scr_getGoldOnline(ActorState.Mine);
}

isNet = false;

with (obj_baseGround)
{
	ds_grid_set(itemsgrid,floor(other.x/16),floor(other.y/16),-1);
}
instance_destroy();

scr_progressMission("boulder");
scr_progressMission("rock");