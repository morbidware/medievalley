/// @description craft
// You can write your code in this editor

if(cat.neededStructure != noone)
{
	show_debug_message("other cat neededStructure exists");
	var near = instance_nearest(x,y,cat.neededStructure);
	if(string_length(near.craftid) > 0)
	{
		show_debug_message("structure is busy");
		exit;
	}
}
with(obj_char)
{
	if (state != ActorState.Idle || crono > 0)
	{
		show_debug_message("character is busy");
		exit;
	}
	if (cursorobject != noone)
	{
		show_debug_message("cursor object not empty");
		exit;
	}
}

if(state == 1 || state == 3)
{
	var k = ds_map_find_first(ingredients);
	for(var i = 0; i < ds_map_size(ingredients); i++)
	{
		var itemid = k;
		var itemq = ds_map_find_value(ingredients,k);
		
		show_debug_message("I NEED " + string(itemq) + " OF " + itemid);
		if(itemid == "gold")
		{
			global.gold -= itemq;
			with(obj_char)
			{
				gold -= itemq;
			}
			k = ds_map_find_next(ingredients,k);
		}
		else
		{
			for(var j = 0; j < global.inv_backpacklimit; j++)
			{
				var invobj = ds_grid_get(inv.inventory,j,0);
				if (invobj > 0)
				{
					if (invobj.itemid == itemid)
					{
						if(invobj.quantity >= itemq)
						{
							scr_loseQuantity(invobj,itemq);
							k = ds_map_find_next(ingredients,k);
							break;
						}
						else
						{
							itemq -= invobj.quantity;
							scr_loseQuantity(invobj,invobj.quantity);
						}
					}
				}
			}
		}
	}

	with(obj_char)
	{
		craftingid = other.resitemid;
		if(other.cat.neededStructure != noone)
		{
			show_debug_message("other cat neededStructure exists");
			startStructureJob(x,y,other.cat.neededStructure,other.resitemid);
			
			var near = instance_nearest(x,y,other.cat.neededStructure);
			near.ts = 8/craftTimer;
			near.craftCrono = 300;
			near.craftid = other.resitemid;
			
		}
		else
		{
			//force sideways animation
			/*
			if (lastdirection == Direction.Up || lastdirection == Direction.Down)
			{
				lastdirection = choose(Direction.Left,Direction.Right);
			}
			*/
			instance_create_depth(other.x,other.y,other.depth-10,obj_fx_ui_circle);
			scr_setState(id,ActorState.Craft);
			show_debug_message("other cat neededStructure doesn't exists");
			scr_setState(id,ActorState.Craft);
		}
		ds_map_add(global.craftedobjects, other.resitemid, 0);
	}

	scr_updateRecipes();
}
else
{
	show_debug_message("cannot be crafted");
}