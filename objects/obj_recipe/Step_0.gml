// stick to sidebar
y = cat.sidebar.y + 28 + 38*(index+1) - cat.from*38;

// set crafting state
if(string_length(resitemid) != 0 && state == -1)
{
	event_perform(ev_other, ev_user1);
}

// make this object disappear if not craftable
if(cat.state == 0 || (cat.state == 1 && (index < cat.from || index > cat.from + 5)) || !unlocked)
{
	x = -100;
}
else
{
	if(global.isSurvival)x = cat.sidebar.listx + 25;
	else x = cat.sidebar.listx + 22;
	if (cat.sidebar.listx <= 3)
	{
		x = -100;
	}
}