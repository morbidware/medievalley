/// @description set state for crafting
var evercrafted = false;
if (ds_map_exists(global.craftedobjects, resitemid))
{
	evercrafted = true;
}
var canbecrafted = true;

// check if this item can be crafted
var key = ds_map_find_first(ingredients);
for(var i = 0; i < ds_map_size(ingredients); i++)
{
	var ingredient = key;
	var quantity = ds_map_find_value(ingredients,key);
	var tot = 0;
	for(var j = 0; j < global.inv_backpacklimit; j++)
	{
		var itm = ds_grid_get(inv.inventory,j,0);
		if (itm > 0)
		{
			if(itm.itemid == ingredient)
			{
				tot += itm.quantity;
			}
		}
	}
		
	if(ingredient == "gold")
	{
		tot = global.gold;
	}
	if(tot < quantity)
	{
		canbecrafted = false;
		break;
	}
	else 
	{
		key = ds_map_find_next(ingredients, key);
	}
}

//show_debug_message(resitemid+" - "+string(canbecrafted));

if(!evercrafted && !canbecrafted)
{
	state = 0;
}
else if(!evercrafted && canbecrafted)
{
	state = 1;
}
else if(evercrafted && !canbecrafted)
{
	state = 2;
}
else if(evercrafted && canbecrafted)
{
	state = 3;
}