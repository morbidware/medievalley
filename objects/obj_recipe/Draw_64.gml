// select pickable sprite
var spr = scr_asset_get_index("spr_pickable_"+scr_getGameItemValue(resitemid, "spritename"));

// if this is craftable, highlight it
if(state == 1 || state == 3)
{
	draw_sprite(spr_recipe_highlight,0,x,y);
}

// make it black if object is still hidden (either craftable or not)
if(state == 0 || state == 1)
{
	shader_set(shaderBlacken);
	//draw_sprite(spr,0,x,y);
	scr_draw_sprite_enclosed(spr,0,x,y,32,1);
	shader_reset();
}
// craftable but no ingredients
else if(state == 2)
{
	scr_draw_sprite_enclosed(spr,0,x,y,32,0.5);
}
// craftable, with ingredients
else if(state == 3)
{
	scr_draw_sprite_enclosed(spr,0,x,y,32,1);
	
}
image_alpha = 1.0;