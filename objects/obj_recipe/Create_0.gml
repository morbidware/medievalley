ingredients = ds_map_create();
unlocked = false;
depth = -100;

resobj = obj_inventory;
resitemid = "";
desc = "owo what's this?";

index = 0;
cat = noone;

rrot = random(360);
rrot = 0;

state = -1;
inv = instance_find(obj_playerInventory,0);
// 0: never crafted, not yet craftable
// 1: already crafted once
// 2: first time craftable, no ingredients
// 3: first time craftable, has ingredients