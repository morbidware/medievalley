{
    "id": "93f355f3-d76a-4841-a79d-f9b850cd184e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipe",
    "eventList": [
        {
            "id": "40b127c0-97fa-43f9-8d60-e6980c8d2f3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93f355f3-d76a-4841-a79d-f9b850cd184e"
        },
        {
            "id": "11ee36bb-803b-4470-b523-2417899b9685",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "93f355f3-d76a-4841-a79d-f9b850cd184e"
        },
        {
            "id": "015e90d2-191b-4a83-96f3-120bf80ed132",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93f355f3-d76a-4841-a79d-f9b850cd184e"
        },
        {
            "id": "324763df-ccb3-4ef6-aceb-7105a934a4a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "93f355f3-d76a-4841-a79d-f9b850cd184e"
        },
        {
            "id": "70c5e065-69c8-411d-8ae3-c033e7485c45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "93f355f3-d76a-4841-a79d-f9b850cd184e"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}