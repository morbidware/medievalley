event_inherited();

if (instance_exists(btnClose))
{
	btnClose.ox = 154;
	btnClose.oy = -136;
}
if (instance_exists(btnStats))
{
	btnStats.ox = -124;
	btnStats.oy = 96;
	if (showStats)
	{
		btnStats.text = "QUESTS";
	}
	else
	{
		btnStats.text = "STATS";
	}
}

// align entry buttons and detect highlighted mission
currenty = 0;
var highlighted = false;
var mx = mouse_get_relative_x();
var my = mouse_get_relative_y();

var createPanel = false;
var description = "";
var required = ds_map_create();
var done = ds_map_create();

for (var i = 0; i < ds_list_size(entries); i++)
{
	var entry = ds_list_find_value(entries,i);
	var p_start = ds_list_find_value(pagestarts,page);
	var p_end = ds_list_find_value(pagestarts,page+1);
	if (i >= p_start && i < p_end)
	{
		entry.x = x - 36;
		entry.y = y - 126 + currenty;
		currenty += entry.height;
		
		if (state == 0 && t >= 1)
		{
			if (mx > entry.x && mx < entry.x + entry.width && my > entry.y && my < entry.y + entry.height)
			{
				createPanel = true;
				required = entry.required;
				done = entry.done;
				description = entry.longdesc;
				
				highlighted = true;
				entry.highlight = true;
			}
			else
			{
				entry.highlight = false;
			}		
		}
	}
	else
	{
		entry.x = -10000;
		entry.y = -10000;
	}
}

if (createPanel)
{
	if (!instance_exists(obj_diary_missioninfo))
	{
		var mi = instance_create_depth(mx,my,0,obj_diary_missioninfo);
		mi.required = required;
		mi.done = done;
		mi.desc = description;
	}
	else
	{
		var mi = instance_find(obj_diary_missioninfo,0);
		mi.required = required;
		mi.done = done;
		mi.desc = description;
	}
}
else
{
	if (instance_exists(obj_diary_missioninfo))
	{
		instance_destroy(obj_diary_missioninfo);
	}
}