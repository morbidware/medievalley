if (live_call()) return live_result;

draw_set_color(c_white);
draw_set_alpha(1.0);
draw_sprite_ext(spr_diary_bg, 0, display_get_gui_width()/2 + 64, y, 2, 2, 0, c_white, 1);

if (!showStats)
{
	draw_set_font(global.FontStandard);
	draw_set_halign(fa_center);
	draw_set_halign(fa_middle);
	draw_set_color(c_black);
	draw_set_alpha(0.7);
	if (ds_list_size(entries) == 0)
	{
		draw_text(x+62,y-16,"No active Quests");
	}
	else if (pagecount > 1)
	{
		draw_text(x+62,y+128,string(page+1)+" / "+string(pagecount));
	}
}
else
{
	// Player stats
	draw_set_color(c_black);
	draw_set_alpha(1.0);
	draw_set_halign(fa_top);

	// alignment
	var leftx = x+50;
	var rightx = x+54;
	var rows = ds_list_create();
	var c = y-110;
	repeat(20){ds_list_add(rows,c); c+=16;}

	draw_set_halign(fa_right);
	draw_set_font(global.FontStandardBold);
	draw_text(leftx, ds_list_find_value(rows,0), "Time played:");
	draw_text(leftx, ds_list_find_value(rows,1), "Days:");
	draw_text(leftx, ds_list_find_value(rows,2), "Walked:");

	draw_text(leftx, ds_list_find_value(rows,3), "Crop area:");
	draw_text(leftx, ds_list_find_value(rows,4), "Crops:");

	draw_text(leftx, ds_list_find_value(rows,5), "Kills:");
	draw_text(leftx, ds_list_find_value(rows,6), "Deaths:");

	draw_text(leftx, ds_list_find_value(rows,7), "Gold earned:");
	draw_text(leftx, ds_list_find_value(rows,8), "Gold spent:");

	draw_set_halign(fa_left);
	draw_set_font(global.FontStandard);
	draw_text(rightx, ds_list_find_value(rows,0), timestring);
	draw_text(rightx, ds_list_find_value(rows,1), string(global.stat_days));
	draw_text(rightx, ds_list_find_value(rows,2), walkedstring);

	draw_text(rightx, ds_list_find_value(rows,3), string(croparea)+"%");
	draw_text(rightx, ds_list_find_value(rows,4), string(global.stat_crops));

	draw_text(rightx, ds_list_find_value(rows,5), string(global.stat_kills));
	draw_text(rightx, ds_list_find_value(rows,6), string(global.stat_deaths));

	draw_text(rightx, ds_list_find_value(rows,7), string(global.stat_goldearned));
	draw_text(rightx, ds_list_find_value(rows,8), string(global.stat_goldspent));
}

draw_set_color(c_white);
draw_set_alpha(1);