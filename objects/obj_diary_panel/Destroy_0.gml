event_inherited();

instance_destroy(btnClose);
instance_destroy(btnStats);
if (pagecount > 1)
{
	instance_destroy(btnLeft);
	instance_destroy(btnRight);
}
			
for (var i = 0; i < ds_list_size(entries); i++)
{
	var entry = ds_list_find_value(entries,i);
	if (entry > -1)
	{
		instance_destroy(entry);					
	}
}
ds_list_clear(entries);