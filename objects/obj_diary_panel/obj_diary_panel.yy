{
    "id": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_diary_panel",
    "eventList": [
        {
            "id": "70177f89-e12b-45e2-aed1-4264cb19ef36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "291c00b4-bcd6-4a63-872c-4bd74bfc77e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "79aa43c3-867a-446a-acd6-c39716738b94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "4ca0df10-8f4a-441f-97bb-34ddc65a09e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "61be560a-4d4b-45f8-ad58-90f1cee8bfa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "7a324f71-bf4b-4155-987f-fd8f4f4a9358",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "15d6be5e-a03d-471f-8f68-a52012e662df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        },
        {
            "id": "b19e8320-3bd9-4a6f-abd7-b1a11a633b08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "c7031086-9f5e-43d5-bd3b-ef54dbffbd69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}