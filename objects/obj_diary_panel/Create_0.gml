event_inherited();

depth = -900;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -800;
showStats = false;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

entries = ds_list_create();

w_width = sprite_get_width(spr_diary_bg);
w_height = sprite_get_height(spr_diary_bg);

btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;
btnClose.sprite_index = spr_btn_close_orange;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}


btnStats = instance_create_depth(0,0,-10001,obj_btn_generic);
btnStats.caller = id;
btnStats.evt = ev_user3;
btnStats.depth = depth-1;
btnStats.sprite_index = spr_popup_btn;

currentstart = 0;
currentend = 0;
pagestarts = ds_list_create();

page = 0;
pagecount = 0;

currentheight = 0;
maxheight = 200;

highlightedmission = noone;

// create entries
with (obj_mission)
{
	var entry = instance_create_depth(0,0,other.depth-1,obj_diary_entry);
	entry.title = title;
	entry.shortdesc = shortdesc;
	entry.longdesc = longdesc
	entry.required = required;
	entry.done = done;
	entry.x = -10000;
	entry.y = -10000;
	with (entry)
	{
		event_perform(ev_other,ev_user0);
	}
	ds_list_add(other.entries,entry);
}

// define pages
currentstart = 0;
currentend = 0;
ds_list_clear(pagestarts);
for (var i = 0; i < ds_list_size(entries); i++)
{
	var entry = ds_list_find_value(entries,i);
	currentheight += entry.height;
	
	if (currentheight > maxheight || i == ds_list_size(entries)-1)
	{
		currentend = i-1;
		show_debug_message(string(currentstart)+" - "+string(currentend));
		ds_list_add(pagestarts,currentstart);
		currentstart = i;
		currentheight = 0;
	}	
}
// next line is needed to show entries on last page but adds an extra page, which is removed later
ds_list_add(pagestarts,99999);
pagecount = ds_list_size(pagestarts)-1;

if (pagecount > 1)
{
	btnLeft = instance_create_depth(0,0,-10001,obj_btn_generic);
	btnLeft.sprite_index = spr_farmboard_btn_prev;
	btnLeft.ox = -20;
	btnLeft.oy = 126;
	btnLeft.caller = id;
	btnLeft.evt = ev_user1;

	btnRight = instance_create_depth(0,0,-10001,obj_btn_generic);
	btnRight.sprite_index = spr_farmboard_btn_next;
	btnRight.ox = 138;
	btnRight.oy = 126;
	btnRight.caller = id;
	btnRight.evt = ev_user2;
}

getUserCreationDate(global.userigid);

// ================== DIARY STATS

// ------------------ DISTANCE
// 16x16 px tile is 80x80cm
var meters = floor(global.stat_walked);
var kms = 0;
while (meters > 1000)
{
	meters -= 1000;
	kms++;
}
walkedstring = string(floor(kms))+"km "+string(floor(meters))+"m";

// ------------------ TIME PLAYED
var days = 0;
var hours = 0;
var minutes = global.stat_timeplayed;
while (minutes > 60)
{
	minutes -= 60;
	hours++;
	if (hours > 24)
	{
		hours -= 24;
		days++;
	}
}
timestring = string(floor(days))+"d "+string(floor(hours))+"h "+string(floor(minutes))+"m";

// ------------------ CROP COUNT
croparea = global.stat_croparea;
if (room == roomFarm)
{
	var ground = instance_find(obj_farmGround,0);
	if (ground > 0)
	{
		var total = (ground.cols - ground.border) * (ground.rows - ground.border);
		croparea = floor((global.stat_crops / total) * 100);
	}
}