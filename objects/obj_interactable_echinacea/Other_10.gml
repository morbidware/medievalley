/// @description finish interaction
// You can write your code in this editor

with(obj_baseGround)
{
	ds_grid_set(itemsgrid, floor(other.x/16), floor(other.y/16), -1);
}
if(!isNet)
{
	if(global.otheruserid != 0)
	{
		var pla = instance_find(obj_char,0);
		sendItemToUserStash(global.otherusername, pla.targetobject.itemid, pla.targetobject.quantity, pla.targetobject.durability);
	}
	else
	{
		with(obj_char)
		{
			scr_putItemIntoInventory(targetobject,InventoryType.Inventory,-1);
		}
	}
	
}
isNet = false;
instance_destroy();

/*growstage = 1;
growcrono = growtimer;*/