{
    "id": "ee49ee17-a110-4736-85ed-af4c46fe20f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_echinacea",
    "eventList": [
        {
            "id": "75ecaf2b-39b7-48a2-9b72-529e00dd2a4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ee49ee17-a110-4736-85ed-af4c46fe20f7"
        },
        {
            "id": "20f46a23-7896-485f-a140-01019bb29e24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ee49ee17-a110-4736-85ed-af4c46fe20f7"
        },
        {
            "id": "2a565d30-a506-4f8d-b3b8-eea895bdf573",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ee49ee17-a110-4736-85ed-af4c46fe20f7"
        },
        {
            "id": "06b744dd-1664-408d-809f-700ce04f8943",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "ee49ee17-a110-4736-85ed-af4c46fe20f7"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "881766fc-403e-40eb-91b4-ef8305bc4cb2",
    "visible": true
}