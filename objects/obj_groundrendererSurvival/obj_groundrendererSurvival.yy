{
    "id": "559a07b6-4b0c-4bd6-8baf-21762943e08a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_groundrendererSurvival",
    "eventList": [
        {
            "id": "1a4c049e-d67f-48ab-862e-7677df34beb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "559a07b6-4b0c-4bd6-8baf-21762943e08a"
        },
        {
            "id": "b668ef74-da97-4eb3-8615-5263956a51f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "559a07b6-4b0c-4bd6-8baf-21762943e08a"
        },
        {
            "id": "f1a1487d-db2c-4a85-bce8-a1f30e647920",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "559a07b6-4b0c-4bd6-8baf-21762943e08a"
        },
        {
            "id": "8c683fad-76fe-46bd-a9f4-4df3f3698b46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "559a07b6-4b0c-4bd6-8baf-21762943e08a"
        },
        {
            "id": "d67ed242-d98a-4401-a99d-72e8dd9d621a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "559a07b6-4b0c-4bd6-8baf-21762943e08a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}