/// @description Insert description here
// You can write your code in this editor
if(vbuffgrass != noone)
{
	vertex_delete_buffer(vbuffgrass);
	vertex_format_delete(vbuffgrass);
}
if(vbufftallgrass != noone)
{
	vertex_delete_buffer(vbufftallgrass);
	vertex_format_delete(vbufftallgrass);
}
if(vbufftallgrassvar != noone)
{
	vertex_delete_buffer(vbufftallgrassvar);
	vertex_format_delete(vbufftallgrassvar);
}
if(vbuffwater != noone)
{
	vertex_delete_buffer(vbuffwater);
	vertex_format_delete(vbuffwater);
}
if(vbuffplowed != noone)
{
	vertex_delete_buffer(vbuffplowed);
	vertex_format_delete(vbuffplowed);
}
if(vbuffcliff != noone)
{
	vertex_delete_buffer(vbuffcliff);
	vertex_format_delete(vbuffcliff);
}
if(vbuffcliffvar != noone)
{
	vertex_delete_buffer(vbuffcliffvar);
	vertex_format_delete(vbuffcliffvar);
}
