i = 0;
size = 0;

vbuffgrass = noone;
tilespriteGrass[0] = 0;
tileindexGrass[0] = 0;
tilexGrass[0] = 0;
tileyGrass[0] = 0;
grassSize = 0;
textureGrass = sprite_get_texture(spr_ground_grass_variation_47,0);

vbufftallgrass = noone;
tilespriteTallGrass[0] = 0;
tileindexTallGrass[0] = 0;
tilexTallGrass[0] = 0;
tileyTallGrass[0] = 0;
tallGrassSize = 0;
textureTallGrass = sprite_get_texture(spr_ground_tallgrass_tiles,0);

vbufftallgrassvar = noone;
tilespriteTallGrassVar[0] = 0;
tileindexTallGrassVar[0] = 0;
tilexTallGrassVar[0] = 0;
tileyTallGrassVar[0] = 0;
tallGrassSizeVar = 0;
textureTallGrassVar = sprite_get_texture(spr_ground_tallgrass_tiles,34);

vbuffwater = noone;
tilespriteWater[0] = 0;
tileindexWater[0] = 0;
tilexWater[0] = 0;
tileyWater[0] = 0;
waterSize = 0;
textureWater = sprite_get_texture(spr_ground_coast_tiles,0);

vbuffplowed = noone;
tilespritePlowed[0] = 0;
tileindexPlowed[0] = 0;
tilexPlowed[0] = 0;
tileyPlowed[0] = 0;
plowedSize = 0;
texturePlowed = sprite_get_texture(spr_ground_grass_variation_47,0);

vbuffcliff = noone;
tilespriteCliff[0] = 0;
tileindexCliff[0] = 0;
tilexCliff[0] = 0;
tileyCliff[0] = 0;
cliffSize = 0;
textureCliff = sprite_get_texture(spr_ground_cliff_variation,0);

vbuffcliffvar = noone;
tilespriteCliffVar[0] = 0;
tileindexCliffVar[0] = 0;
tilexCliffVar[0] = 0;
tileyCliffVar[0] = 0;
cliffSizeVar = 0;
textureCliffVar = sprite_get_texture(spr_ground_grass_variation_47,0);







color = c_white;
alpha = 1;

//Vertex format
vertex_format_begin();

vertex_format_add_position_3d();
vertex_format_add_texcoord();
vertex_format_add_color();

format = vertex_format_end();


