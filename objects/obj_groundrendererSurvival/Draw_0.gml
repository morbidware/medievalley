
if(vbuffcliff != noone)
{
	vertex_submit(vbuffcliff, pr_trianglelist, textureCliff);
}
if(vbuffgrass != noone)
{
	vertex_submit(vbuffgrass, pr_trianglelist, textureGrass);
}
if(vbufftallgrass != noone)
{
	vertex_submit(vbufftallgrass, pr_trianglelist, textureTallGrass);
}
if(vbufftallgrassvar != noone)
{
	vertex_submit(vbufftallgrassvar, pr_trianglelist, textureTallGrassVar);
}
if(vbuffwater != noone)
{
	vertex_submit(vbuffwater, pr_trianglelist, textureWater);
}
if(vbuffplowed != noone)
{
	vertex_submit(vbuffplowed, pr_trianglelist, texturePlowed);
}

if(vbuffcliffvar != noone)
{
	vertex_submit(vbuffcliffvar, pr_trianglelist, textureCliffVar);
}

