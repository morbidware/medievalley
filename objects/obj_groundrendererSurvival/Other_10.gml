/// @description Update Plowed
// You can write your code in this editor
/*if(vbuffplowed != noone)
{
	vertex_delete_buffer(vbuffplowed);
}*/
if(plowedSize != 0)
{
	//vbuffplowed = vertex_create_buffer();

	vertex_begin(vbuffplowed,format);
	i = 0;

	repeat(plowedSize)
	{
		var sprite = tilespritePlowed[i];
		var frame = tileindexPlowed[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexPlowed[i] * 16;
		var _y1 = tileyPlowed[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth-10;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbuffplowed, _x1, _y1, _depth);
		vertex_texcoord(vbuffplowed, _uvs[0], _uvs[1]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x2, _y1, _depth);
		vertex_texcoord(vbuffplowed, _uvs[2], _uvs[1]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x1, _y2, _depth);
		vertex_texcoord(vbuffplowed, _uvs[0], _uvs[3]);
		vertex_color(vbuffplowed, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbuffplowed, _x2, _y1, _depth);
		vertex_texcoord(vbuffplowed, _uvs[2], _uvs[1]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x1, _y2, _depth);
		vertex_texcoord(vbuffplowed, _uvs[0], _uvs[3]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x2, _y2, _depth);
		vertex_texcoord(vbuffplowed, _uvs[2], _uvs[3]);
		vertex_color(vbuffplowed, color, alpha);
	
		i++;
	}

	vertex_end(vbuffplowed);
	//vertex_freeze(vbuffplowed);
}