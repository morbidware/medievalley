/// @description Insert description here
// You can write your code in this editor


if(grassSize != 0)
{
	vbuffgrass = vertex_create_buffer();

	vertex_begin(vbuffgrass,format);
	i = 0;

	repeat(grassSize)
	{
		var sprite = tilespriteGrass[i];
		var frame = tileindexGrass[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexGrass[i] * 16;
		var _y1 = tileyGrass[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbuffgrass, _x1, _y1, _depth);
		vertex_texcoord(vbuffgrass, _uvs[0], _uvs[1]);
		vertex_color(vbuffgrass, color, alpha);
	
		vertex_position_3d(vbuffgrass, _x2, _y1, _depth);
		vertex_texcoord(vbuffgrass, _uvs[2], _uvs[1]);
		vertex_color(vbuffgrass, color, alpha);
	
		vertex_position_3d(vbuffgrass, _x1, _y2, _depth);
		vertex_texcoord(vbuffgrass, _uvs[0], _uvs[3]);
		vertex_color(vbuffgrass, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbuffgrass, _x2, _y1, _depth);
		vertex_texcoord(vbuffgrass, _uvs[2], _uvs[1]);
		vertex_color(vbuffgrass, color, alpha);
	
		vertex_position_3d(vbuffgrass, _x1, _y2, _depth);
		vertex_texcoord(vbuffgrass, _uvs[0], _uvs[3]);
		vertex_color(vbuffgrass, color, alpha);
	
		vertex_position_3d(vbuffgrass, _x2, _y2, _depth);
		vertex_texcoord(vbuffgrass, _uvs[2], _uvs[3]);
		vertex_color(vbuffgrass, color, alpha);
	
		i++;
	}

	vertex_end(vbuffgrass);
	vertex_freeze(vbuffgrass);
}
if(tallGrassSize != 0)
{
	vbufftallgrass = vertex_create_buffer();
	vertex_begin(vbufftallgrass,format);
	i = 0;

	repeat(tallGrassSize)
	{
		var sprite = tilespriteTallGrass[i];
		var frame = tileindexTallGrass[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexTallGrass[i] * 16;
		var _y1 = tileyTallGrass[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbufftallgrass, _x1, _y1, _depth);
		vertex_texcoord(vbufftallgrass, _uvs[0], _uvs[1]);
		vertex_color(vbufftallgrass, color, alpha);
	
		vertex_position_3d(vbufftallgrass, _x2, _y1, _depth);
		vertex_texcoord(vbufftallgrass, _uvs[2], _uvs[1]);
		vertex_color(vbufftallgrass, color, alpha);
	
		vertex_position_3d(vbufftallgrass, _x1, _y2, _depth);
		vertex_texcoord(vbufftallgrass, _uvs[0], _uvs[3]);
		vertex_color(vbufftallgrass, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbufftallgrass, _x2, _y1, _depth);
		vertex_texcoord(vbufftallgrass, _uvs[2], _uvs[1]);
		vertex_color(vbufftallgrass, color, alpha);
	
		vertex_position_3d(vbufftallgrass, _x1, _y2, _depth);
		vertex_texcoord(vbufftallgrass, _uvs[0], _uvs[3]);
		vertex_color(vbufftallgrass, color, alpha);
	
		vertex_position_3d(vbufftallgrass, _x2, _y2, _depth);
		vertex_texcoord(vbufftallgrass, _uvs[2], _uvs[3]);
		vertex_color(vbufftallgrass, color, alpha);
	
		i++;
	}

	vertex_end(vbufftallgrass);
	vertex_freeze(vbufftallgrass);
}

if(tallGrassSizeVar != 0)
{
	vbufftallgrassvar = vertex_create_buffer();
	vertex_begin(vbufftallgrassvar,format);
	i = 0;

	repeat(tallGrassSizeVar)
	{
		var sprite = tilespriteTallGrassVar[i];
		var frame = tileindexTallGrassVar[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexTallGrassVar[i] * 16;
		var _y1 = tileyTallGrassVar[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbufftallgrassvar, _x1, _y1, _depth);
		vertex_texcoord(vbufftallgrassvar, _uvs[0], _uvs[1]);
		vertex_color(vbufftallgrassvar, color, alpha);
	
		vertex_position_3d(vbufftallgrassvar, _x2, _y1, _depth);
		vertex_texcoord(vbufftallgrassvar, _uvs[2], _uvs[1]);
		vertex_color(vbufftallgrassvar, color, alpha);
	
		vertex_position_3d(vbufftallgrassvar, _x1, _y2, _depth);
		vertex_texcoord(vbufftallgrassvar, _uvs[0], _uvs[3]);
		vertex_color(vbufftallgrassvar, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbufftallgrassvar, _x2, _y1, _depth);
		vertex_texcoord(vbufftallgrassvar, _uvs[2], _uvs[1]);
		vertex_color(vbufftallgrassvar, color, alpha);
	
		vertex_position_3d(vbufftallgrassvar, _x1, _y2, _depth);
		vertex_texcoord(vbufftallgrassvar, _uvs[0], _uvs[3]);
		vertex_color(vbufftallgrassvar, color, alpha);
	
		vertex_position_3d(vbufftallgrassvar, _x2, _y2, _depth);
		vertex_texcoord(vbufftallgrassvar, _uvs[2], _uvs[3]);
		vertex_color(vbufftallgrassvar, color, alpha);
	
		i++;
	}

	vertex_end(vbufftallgrassvar);
	vertex_freeze(vbufftallgrassvar);
}

if(waterSize != 0)
{
	vbuffwater = vertex_create_buffer();
	vertex_begin(vbuffwater,format);
	i = 0;

	repeat(waterSize)
	{
		var sprite = tilespriteWater[i];
		var frame = tileindexWater[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexWater[i] * 16;
		var _y1 = tileyWater[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbuffwater, _x1, _y1, _depth);
		vertex_texcoord(vbuffwater, _uvs[0], _uvs[1]);
		vertex_color(vbuffwater, color, alpha);
	
		vertex_position_3d(vbuffwater, _x2, _y1, _depth);
		vertex_texcoord(vbuffwater, _uvs[2], _uvs[1]);
		vertex_color(vbuffwater, color, alpha);
	
		vertex_position_3d(vbuffwater, _x1, _y2, _depth);
		vertex_texcoord(vbuffwater, _uvs[0], _uvs[3]);
		vertex_color(vbuffwater, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbuffwater, _x2, _y1, _depth);
		vertex_texcoord(vbuffwater, _uvs[2], _uvs[1]);
		vertex_color(vbuffwater, color, alpha);
	
		vertex_position_3d(vbuffwater, _x1, _y2, _depth);
		vertex_texcoord(vbuffwater, _uvs[0], _uvs[3]);
		vertex_color(vbuffwater, color, alpha);
	
		vertex_position_3d(vbuffwater, _x2, _y2, _depth);
		vertex_texcoord(vbuffwater, _uvs[2], _uvs[3]);
		vertex_color(vbuffwater, color, alpha);
	
		i++;
	}

	vertex_end(vbuffwater);
	vertex_freeze(vbuffwater);
}

if(plowedSize != 0)
{
	vbuffplowed = vertex_create_buffer();

	vertex_begin(vbuffplowed,format);
	i = 0;

	repeat(plowedSize)
	{

		var sprite = tilespritePlowed[i];
		var frame = tileindexPlowed[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexPlowed[i] * 16;
		var _y1 = tileyPlowed[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth-1;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbuffplowed, _x1, _y1, _depth);
		vertex_texcoord(vbuffplowed, _uvs[0], _uvs[1]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x2, _y1, _depth);
		vertex_texcoord(vbuffplowed, _uvs[2], _uvs[1]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x1, _y2, _depth);
		vertex_texcoord(vbuffplowed, _uvs[0], _uvs[3]);
		vertex_color(vbuffplowed, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbuffplowed, _x2, _y1, _depth);
		vertex_texcoord(vbuffplowed, _uvs[2], _uvs[1]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x1, _y2, _depth);
		vertex_texcoord(vbuffplowed, _uvs[0], _uvs[3]);
		vertex_color(vbuffplowed, color, alpha);
	
		vertex_position_3d(vbuffplowed, _x2, _y2, _depth);
		vertex_texcoord(vbuffplowed, _uvs[2], _uvs[3]);
		vertex_color(vbuffplowed, color, alpha);
	
		i++;
	}

	vertex_end(vbuffplowed);
	//vertex_freeze(vbuffplowed);
}

if(cliffSize != 0)
{
	vbuffcliff = vertex_create_buffer();

	vertex_begin(vbuffcliff,format);
	i = 0;

	repeat(cliffSize)
	{

		var sprite = tilespriteCliff[i];
		var frame = tileindexCliff[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexCliff[i] * 16;
		var _y1 = tileyCliff[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth +100;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbuffcliff, _x1, _y1, _depth);
		vertex_texcoord(vbuffcliff, _uvs[0], _uvs[1]);
		vertex_color(vbuffcliff, color, alpha);
	
		vertex_position_3d(vbuffcliff, _x2, _y1, _depth);
		vertex_texcoord(vbuffcliff, _uvs[2], _uvs[1]);
		vertex_color(vbuffcliff, color, alpha);
	
		vertex_position_3d(vbuffcliff, _x1, _y2, _depth);
		vertex_texcoord(vbuffcliff, _uvs[0], _uvs[3]);
		vertex_color(vbuffcliff, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbuffcliff, _x2, _y1, _depth);
		vertex_texcoord(vbuffcliff, _uvs[2], _uvs[1]);
		vertex_color(vbuffcliff, color, alpha);
	
		vertex_position_3d(vbuffcliff, _x1, _y2, _depth);
		vertex_texcoord(vbuffcliff, _uvs[0], _uvs[3]);
		vertex_color(vbuffcliff, color, alpha);
	
		vertex_position_3d(vbuffcliff, _x2, _y2, _depth);
		vertex_texcoord(vbuffcliff, _uvs[2], _uvs[3]);
		vertex_color(vbuffcliff, color, alpha);
	
		i++;
	}

	vertex_end(vbuffcliff);
	vertex_freeze(vbuffcliff);
}

if(cliffSizeVar != 0)
{
	vbuffcliffvar = vertex_create_buffer();

	vertex_begin(vbuffcliffvar,format);
	i = 0;

	repeat(cliffSizeVar)
	{

		var sprite = tilespriteCliffVar[i];
		var frame = tileindexCliffVar[i];
		var width = sprite_get_width(sprite);
		var height = sprite_get_height(sprite);
		//Tile coordinates
		var _x1 = tilexCliffVar[i] * 16;
		var _y1 = tileyCliffVar[i] * 16;
		var _x2 = _x1 + width;
		var _y2 = _y1 + height;
	
		var _depth = depth;
	
		//texture coordinates
		var _uvs = sprite_get_uvs(sprite, frame);	

		//Triangle 1
		vertex_position_3d(vbuffcliffvar, _x1, _y1, _depth);
		vertex_texcoord(vbuffcliffvar, _uvs[0], _uvs[1]);
		vertex_color(vbuffcliffvar, color, alpha);
	
		vertex_position_3d(vbuffcliffvar, _x2, _y1, _depth);
		vertex_texcoord(vbuffcliffvar, _uvs[2], _uvs[1]);
		vertex_color(vbuffcliffvar, color, alpha);
	
		vertex_position_3d(vbuffcliffvar, _x1, _y2, _depth);
		vertex_texcoord(vbuffcliffvar, _uvs[0], _uvs[3]);
		vertex_color(vbuffcliffvar, color, alpha);
	
		//Triangle 2
		vertex_position_3d(vbuffcliffvar, _x2, _y1, _depth);
		vertex_texcoord(vbuffcliffvar, _uvs[2], _uvs[1]);
		vertex_color(vbuffcliffvar, color, alpha);
	
		vertex_position_3d(vbuffcliffvar, _x1, _y2, _depth);
		vertex_texcoord(vbuffcliffvar, _uvs[0], _uvs[3]);
		vertex_color(vbuffcliffvar, color, alpha);
	
		vertex_position_3d(vbuffcliffvar, _x2, _y2, _depth);
		vertex_texcoord(vbuffcliffvar, _uvs[2], _uvs[3]);
		vertex_color(vbuffcliffvar, color, alpha);
	
		i++;
	}

	vertex_end(vbuffcliffvar);
	vertex_freeze(vbuffcliffvar);
}
