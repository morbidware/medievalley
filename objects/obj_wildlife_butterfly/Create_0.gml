event_inherited();

direction = random(360);
originalspeed = random(0.1)+0.05;
sprite_index = choose(spr_wildlife_butterfly_blue,spr_wildlife_butterfly_red,spr_wildlife_butterfly_yellow);
image_xscale = choose(1,-1);
originalimagespeed = random(0.3)+0.5;

alivefrom = 0.0;
aliveto = 0.5;
offsety = 16;