{
    "id": "a907f899-5781-489c-8427-44b841860336",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wildlife_butterfly",
    "eventList": [
        {
            "id": "825038b5-23ff-4628-81bf-0c2b4845e531",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a907f899-5781-489c-8427-44b841860336"
        },
        {
            "id": "30a53573-c107-42c0-b523-763dde60085d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a907f899-5781-489c-8427-44b841860336"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "389843b1-8fa4-42bf-98e7-2828f05bb9b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
    "visible": true
}