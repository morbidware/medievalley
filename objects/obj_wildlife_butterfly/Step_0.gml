event_inherited();
scr_setDepth(DepthType.Elements);
if(ground != noone)
{
	//prevent going outside of the map
	if (x < ground.border*16)
	{
		direction = 0;
	}
	else if (x > room_width-(ground.border*16))
	{
		direction = 180;
	}
	else if (y < (ground.border*16))
	{
		direction = 270;	
	}
	else if (y > room_height-(ground.border*16))
	{
		direction = 90;
	}
}


//change direction at random
direction += random(30)-15;

speed = originalspeed * global.dt;
image_speed = originalimagespeed * global.dt;