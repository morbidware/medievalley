/// @description Decrease wait timer

timer -= global.dt;
if (timer <= 0)
{
	// depending by hour, birds may not sing
	var chance = 1;
	if (global.dayprogress > 0.75)
	{
		chance = -1;
	}
	else if ((global.dayprogress > 0 && global.dayprogress < 0.1) || (global.dayprogress > 0.65 && global.dayprogress < 0.75))
	{
		chance = 0.3;
	}
	else
	{
		chance = 1;
	}
	
	if (random(1) < chance)
	{
		event_perform(ev_other,ev_user0);
	}
	timer = 240 + random(240);
}