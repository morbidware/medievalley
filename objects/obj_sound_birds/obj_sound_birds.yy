{
    "id": "c340a562-d8ae-431d-99e8-d5733f1994f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sound_birds",
    "eventList": [
        {
            "id": "b403d6f7-6bf2-45b8-8ea3-8fa59b3ec8e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c340a562-d8ae-431d-99e8-d5733f1994f8"
        },
        {
            "id": "a4b09097-5e80-4a22-b20a-7c41993fc931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c340a562-d8ae-431d-99e8-d5733f1994f8"
        },
        {
            "id": "0f9e0f2d-b42a-4edb-89ac-5a90ff7579c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c340a562-d8ae-431d-99e8-d5733f1994f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}