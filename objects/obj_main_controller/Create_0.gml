/// @description Init globals

finishedLoading();

base_width = room_width;
base_height = room_height;
width = base_width;
height = base_height;
depth = -15900;
/*---------------------------*/
global.dayprogress = 0;
global.shadowlength = 0;
global.shadowalpha = 0;
global.shadowangle = 0;
/*--------------------------*/
randomize();
audio_master_gain(0);
display_reset(0, true);

global.aspect = window_get_height()/window_get_width();

// technical setup
global.ismobile = isMobile();
global.dt = 1;
if(global.ismobile)
{
	global.graphicsquality = 0;
}
else
{
	global.graphicsquality = 1;
}
global.enableshadowshader = true;
global.saveinterval = 60; //in seconds
global.clientcountinterval = 7; //in seconds
global.visitorscount = 0;
global.saveversion = 0;

global.lastSavedUnlockables = "";
global.lastSavedItems = "";
global.lastSavedMissions = "";
global.lastSavedPickables = "";

// timestamp is skipped only during the first phase of the game,
// and resynced the first time the player goes out of the farm
global.skiptimestamp = false;

// mouseover and hud lock functions
mx = 0;
my = 0;
window_set_cursor(cr_none);
global.mouseOverUI = false;
global.mouseOverInventory = false;
global.mouseOverCrafting = false;
global.mouseOverDock = false;
global.lockHUDparticipants = ds_list_create();
global.lockHUD = false;
global.onPopup = false;

// mouse delta stuff for specific UI interactions
mouseprevx = 0;
mouseprevy = 0;

global.cw = 0;
global.ch = 0;

global.mousedeltax = 0;
global.mousedeltay = 0;

// farm size and stuff
global.farmCols = 64;
global.farmRows = 64;
global.spawnpidgeon = false;

// house sizes and stuff
global.roomCols = 10;
global.roomRows = 10;
global.basementCols = 18;
global.basementRows = 5;
global.houseborder = 4;
global.houseCols = (global.houseborder*2) + global.roomCols + 2 + global.basementCols;
global.houseRows = (global.houseborder*2) + max(global.roomRows,global.basementRows);
if(global.houseCols < 30 )
{
	global.houseCols = 30;
}
if(global.houseRows < 17 )
{
	global.houseRows = 17;
}

// cross-room popup
global.popupTitle = "";
global.popupBody = "";

// wildland sizes and stuff
global.wildcellcolumns = 2;		// Columns and rows for each wild room
global.wildcellrows = 2;
global.mapsize = 32;		// Square size (cols & rows) for each submap
global.border = 3;
global.clearance = 2;
global.submapsContent = ds_map_create();
global.wildgrid = ds_grid_create(0,0);

// survival sizes and stuff
global.isSurvival = 1;
global.charx = 0;
global.chary = 0;
if(global.isSurvival)
{
	global.border = 0;
	global.clearance = 0;
}
global.landid = 0;
global.mobowner = 0
global.totslots = 134;
global.survivalcellcolumns = 5;		// Columns and rows for each wild room
global.survivalcellrows = 5;
global.survivalmapsizex = 1;	// Size of the wildmap in number of rooms
global.survivalmapsizey = 1;
global.survivalsubmaps = ds_map_create();
room_set_width(roomSurvival, ((global.survivalcellcolumns * global.mapsize) + (global.border * 2) + (global.clearance * 2)) * 16);
room_set_height(roomSurvival, ((global.survivalcellrows * global.mapsize) + (global.border * 2) + (global.clearance * 2)) * 16);

// survival position management
global.survivalposx = 0;		// Player position on the wildmap
global.survivalposy = 0;
global.survivalLastDirection = Direction.Down;
global.survivalmapsizex = 1;	// Size of the wildmap in number of rooms
global.survivalmapsizey = 1;
global.survivallandmarks = ds_map_create();

// challenge size and stuff
global.challengeCols = 30;
global.challengeRows = 22;
global.challengeType = -1;
global.hideChallengePrize = true;
global.challengePrizeType = 0;//0 Steam Key 1 Galacredits
global.newUserForChallenge = false;
global.lastChallengeDone = 0;

global.walkFrames = 0;
global.walkFramesPoints = 0;
global.interactions = 0;
global.finishedInteractions = 0;
global.missedInteractions = 0;
global.bonusInteractions = 0;
global.challengeHits = 0;

global.walkFramesPointsValue = 1;
global.interactionsValue = 10;
global.finishedInteractionsValue = 100;
global.bonusInteractionsValue = 200;
global.missedInteractionsValue = -20;
global.challengeHitsValue = -50;

global.challengePoints = 0;
global.csd = 1.0;
global.willEarnGalaCreds = 0;
global.malusSecondsBase = 4;
global.malusSecondsHigh = 6;
global.bonusSecondsMulti = 1;
global.challengeMalus_newUser = 0;
global.challengeMalus_storePurchase = 0;
global.challengeMalus_bundlePurchase = 0;
global.challengeMalus_keysWon = 0;
global.challengeMalus_giveawayCreated = 0;
global.challengeMalus_steamGroupMember = 0;
global.challengeMalus = 0;
global.challengeGalasilver = real(findUserDataParameter("silver_coins_tot"));
global.challengeGalasilverPrice = real(findUserDataParameter("each_second_price"));
global.challengeMalusSecondsRemoved = 0;
global.challengeData = ds_map_create();
global.challengeGoodNumber = 0;
room_set_width(roomChallenge,global.challengeCols*16);
room_set_height(roomChallenge,global.challengeRows*16);


// last wild death data (to save inventory)
global.lastdeathcell = -1;
global.lastdeathx = 0;
global.lastdeathy = 0;
global.lastdeathinventory = ds_grid_create(10,3);

// wildland position management
global.wildposx = 0;		// Player position on the wildmap
global.wildposy = 0;
global.wildLastDirection = Direction.Right;
global.wildmapsizex = 4;	// Size of the wildmap in number of rooms
global.wildmapsizey = 5;
global.wildlandmarks = ds_map_create();

//adaptive resolution
global.canvasWidth = 960;
global.canvasHeight = 540;

// user recognition and status
global.username = findUserDataParameter("_indiegala_username");
global.userigid = findUserDataParameter("_indiegala_user_id");
global.isChallenge = real(getIsChallenge());

if(os_browser == browser_not_a_browser)
{
	if(m_debugChallenge)
	{
		global.isChallenge = 1;
	}
	else
	{
		global.isChallenge = 0;
	}
}
if(global.isChallenge)
{
	getCSD();
}
global.userid = 0;
global.socketid = "";
global.itemsgridstring = "";
global.lastjoinedroom = "";
global.online = true;
global.visiting = false;
global.lastVisitedPlace = LastVisitedPlace.Farm;
global.firstload = true;
global.life = 100;
global.stamina = 100;
global.gold = 0;
global.galasilver = 0;
global.craftedobjects = ds_map_create();
global.awayFromFarm = 0;
global.showhints = 1;

// online stuff
global.timeout = 0;
global.maxtimeout = 10;
global.allowvisitors = 1;

// init mail data lists
global.mailIndices = ds_list_create();
global.mailTitles = ds_list_create();
global.mailBodies = ds_list_create();
global.mailSignatures = ds_list_create();
global.mailRead = ds_list_create();

// save data
global.gridstring = "";
global.storeystring = "";
global.wallsstring = "";
global.mobsstring = "";
global.interactables = "";
global.pickables = "";
global.backpack = "";
global.items = "";
global.missions = "";
global.unlockables = ds_map_create();
global.houseinteractables = "";
global.housepickables = "";

// player stats
global.stat_timeplayed = 0; // in real time when playing
global.stat_days = 0; // in real days since registration
global.stat_walked = 0; // in kms with one decimal
global.stat_croparea = 0;
global.stat_crops = 0;
global.stat_kills = 0;
global.stat_deaths = 0;
global.stat_goldearned = 0;
global.stat_goldspent = 0;

// id data for visitors
global.otherusername = "";
global.otheruserid = 0;
global.othersocketid = "";
global.othergridstring = "";
global.otherstoreystring = "";
global.otherwallsstring = "";
global.otherinteractables = "";
global.otherpickables = "";

// index limit (non-inclusive) for relative inventory categories
global.inv_inventorylimit = 10;//LAST SLOT IS 9
global.inv_backpacklimit = 30;//LAST SLOT IS 29
global.inv_robelimit = 34;//LAST SLOT IS 33
global.inv_stashlimit = 84;//LAST SLOT IS 83
global.inv_safelimit = 134;//LAST SLOT IS 133
global.inv_carrylimit = 30;

// specific inventory slots
global.inv_head = 30;
global.inv_torso = 31;
global.inv_pants = 32;
global.inv_carry = 33;

// databases
global.wardrobe_body = ds_list_create();
global.wardrobe_head = ds_list_create();
global.wardrobe_upper = ds_list_create();
global.wardrobe_lower = ds_list_create();
global.currentwardrobe = ds_list_create();
ds_list_add(global.currentwardrobe,"m");
ds_list_add(global.currentwardrobe,0);
ds_list_add(global.currentwardrobe,0);
ds_list_add(global.currentwardrobe,0);
ds_list_add(global.currentwardrobe,0);

// auto save timers
global.saveitemstimer = -1;
global.saveitemsinterval = 120; // in frames
global.savegroundtimer = -1;
global.savegroundinterval = 300; // in frames
global.saveuserdatatimer = -1;
global.saveuserdatainterval = 300; // in frames

// hint management
global.hintqueue = ds_queue_create();

enum InventoryType
{
	All,
	Carry,
	Inventory,
	Backpack,
	Stash,
	Safe
}

global.sockets = ds_list_create();
global.socketsusernames = ds_list_create();
global.socketsnumtrees = ds_list_create();
global.socketsnumrocks = ds_list_create();
global.socketsnumplants = ds_list_create();
global.socketsnumvisitors = ds_list_create();
room_set_width(roomFarm,global.farmCols*16);
room_set_height(roomFarm,global.farmRows*16);
clientrefreshtimer = 60;

global.Font = font_add_sprite_ext(spr_fnt_xpdr, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'- $+", true, 1);
global.FontPlain = font_add_sprite_ext(spr_fnt_xpdr_plain, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'-_>() $", true, 1);
global.Fontx2 = font_add_sprite_ext(spr_fnt_xpdr_x2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'- $+", true, 1);
global.FontNumbers = font_add_sprite_ext(spr_fnt_numbers, "1234567890% ", true, 1);
global.FontNumbersOutline = font_add_sprite_ext(spr_fnt_numbers_outline, "1234567890% ", true, 1);
global.FontTitles = font_add_sprite_ext(spr_fnt_titles, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!?+-,.:;=*()[]{}<>\/_%'|&@#", true, 1);

global.FontStandard = font_add_sprite_ext(spr_fnt_standard, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'-_>()$+=", true, 1);
global.FontStandardOutline = font_add_sprite_ext(spr_fnt_standardoutline, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'-_>()$+", true, -1);
global.FontStandardBold = font_add_sprite_ext(spr_fnt_standardbold, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'-_>()$+=", true, 1);
global.FontStandardBoldOutline = font_add_sprite_ext(spr_fnt_standardboldoutline, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'-_>()$+", true, -1);
global.FontStandardBoldOutlineX2 = font_add_sprite_ext(spr_fnt_standardboldoutlinex2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,:;?!%/'-_>()$+", true, -2);

display_set_gui_size(960,540);
scr_loadItemsCSV();
scr_loadWardrobeData();

if (os_browser == browser_not_a_browser || m_forceOffline)
{
	global.online = false;
}
if (m_showProfiler)
{
	show_debug_overlay(true);
}

instance_create_depth(0,0,0,obj_home);

if (!global.online)
{
	repeat(5)
	{
		ds_list_add(global.sockets,"Diego");
		ds_list_add(global.sockets,"Danjel");
		ds_list_add(global.sockets,"Roberto");
		ds_list_add(global.sockets,"Flavia");
		
		ds_list_add(global.socketsusernames,"Diego");
		ds_list_add(global.socketsusernames,"Danjel");
		ds_list_add(global.socketsusernames,"Roberto");
		ds_list_add(global.socketsusernames,"Flavia");
		
		ds_list_add(global.socketsnumtrees,"100");
		ds_list_add(global.socketsnumtrees,"100");
		ds_list_add(global.socketsnumtrees,"100");
		ds_list_add(global.socketsnumtrees,"100");
		
		ds_list_add(global.socketsnumrocks,"100");
		ds_list_add(global.socketsnumrocks,"100");
		ds_list_add(global.socketsnumrocks,"100");
		ds_list_add(global.socketsnumrocks,"100");
		
		ds_list_add(global.socketsnumplants,"100");
		ds_list_add(global.socketsnumplants,"100");
		ds_list_add(global.socketsnumplants,"100");
		ds_list_add(global.socketsnumplants,"100");
		
		ds_list_add(global.socketsnumvisitors,"1");
		ds_list_add(global.socketsnumvisitors,"1");
		ds_list_add(global.socketsnumvisitors,"1");
		ds_list_add(global.socketsnumvisitors,"1");
	}
}

global.dt = 0;
global.isFull = false;
if(variable_global_exists("lastFullState"))
{
	logga("there was a previous saved lastFullState: " + string(global.lastFullState));
	if(global.lastFullState == 1)
	{
		if(global.ismobile)
		{
			alerta("create of main");
			alarm_set(0,10);
			//gmcallback_gotCanvasResize(string(global.lastcw)+"$"+string(global.lastch));
		}
		else
		{
			goFull();
		}
	}
}
else
{
	logga("there was no previous saved lastFullState. Initializing as ususal.");
}
if(variable_global_exists("lastChallengeGalasilver"))
{
	logga("there was a previous saved lastChallengeGalasilver: " + string(global.lastChallengeGalasilver));
	global.challengeGalasilver = global.lastChallengeGalasilver;
}
else
{
	logga("there was no previous saved lastChallengeGalasilver. Initializing as ususal.");
}
global.uisavestatus = instance_create_depth(0,0,0,obj_ui_savestatus);
instance_create_depth(0,0,0,obj_daynightcycle);
instance_create_depth(0,0,0,obj_night_lights);
instance_create_depth(0,0,0,obj_summary_manager);
instance_create_depth(0,0,0,obj_ui_fullscreen);

global.connections = ds_list_create();

if(global.ismobile)
{
	//refreshFullScreen();
}

datetime_string = date_datetime_string(GM_build_date);