/// @description Deltatime, browser size, UI mouseover, misc
var asp = window_get_height()/window_get_width();
if(asp != global.aspect && global.ismobile)
{
	alerta("step of main");
	global.aspect = asp;
	display_set_gui_size(window_get_width(),window_get_height());
	camera_set_view_size(view_camera[0],window_get_width()*0.5,window_get_height()*0.5);
	view_set_wport(0,window_get_width());
	view_set_hport(0,window_get_height());
	with(obj_ui_black_stripes)
	{
		sw = display_get_gui_width();
		sh = display_get_gui_height();
	}
	with(obj_playerInventory)
	{
		startx = display_get_gui_width()/2 - (10/2)*38 + 18;
		starty = display_get_gui_height() - 36;
	}
}
if(global.ismobile  && global.cw != 0 && global.ch != 0 && (window_get_x() != global.cw/2.0 || window_get_y() != 0))
{
	window_set_position(global.cw/2.0,0);
}
// ------------------------------- Deltatime and timers
tempdt = clamp(60 * (delta_time * 0.000001), 0.01, 10);
global.dt = lerp(global.dt,tempdt,0.2);
if (global.timeout > 0)
{
	global.timeout -= global.dt/60;
}
global.stat_timeplayed += (global.dt / 60.0) / 60.0;

// always enable the first hint of the queue, if not enabled
with (obj_hint)
{
	if (!enabled && id == ds_queue_head(global.hintqueue) && !global.lockHUD)
	{
		enabled = true;
	}
}
if (clientrefreshtimer > 0)
{
	clientrefreshtimer -= global.dt;
	if (clientrefreshtimer <= 0)
	{
		if (global.socketid != "" && global.online)
		{
			getClientsInRoom(global.socketid);
		}
		clientrefreshtimer = 60;
	}
}

// ------------------------------- Browser size

if (os_browser != browser_not_a_browser && global.isFull)
{
	if (browser_width != width || browser_height != height)
	{
	    width = max(base_width, browser_width);
	    height = max(base_height, browser_height);
	    scale_canvas(base_width, base_height, width, height, false);
	}
}


global.viewx = view_get_x();
global.viewy = view_get_y();

// ------------------------------- Mouse delta
global.mousedeltax = lerp(global.mousedeltax, mouseprevx - mouse_get_relative_x(), 0.7);
global.mousedeltay = lerp(global.mousedeltay, mouseprevy - mouse_get_relative_y(), 0.7);
mouseprevx = mouse_get_relative_x();
mouseprevy = mouse_get_relative_y();
//show_debug_message("Mouse x: "+string(mx)+", y: "+string(my));

// ------------------------------- Debug fullscreen
if (keyboard_check_pressed(vk_f12) && os_browser == browser_not_a_browser)
{
	window_set_fullscreen(!window_get_fullscreen());
}

// ------------------------------- UI management
global.mouseOverUI = false;
mx = mouse_get_relative_x();
my = mouse_get_relative_y();

global.mouseOverInventory = false;
with (obj_playerInventory)
{
	var p_x = startx-18-8 - 40;
	var p_y = starty-18-8;
	var p_w = sprite_get_width(spr_inventory)*2 + 40;
	var p_h = sprite_get_height(spr_inventory)*2;
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverInventory = true;
		global.mouseOverUI = true;
	}
}

global.mouseOverDock = false;
with (obj_ui_dock)
{
	var p_x = x;
	var p_y = y;
	var p_w = sprite_get_width(spr_dock)*2;
	var p_h = sprite_get_height(spr_dock)*2;
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverDock = true;
		global.mouseOverUI = true;
	}
}
with (obj_mission_summary)
{
	var p_x = posx - (w/2)
	var p_y = posy - (h/2)
	var p_w = w;
	var p_h = h;;
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverDock = true;
		global.mouseOverUI = true;
	}
}

global.mouseOverCrafting = false;
var craft = "";
if(global.isSurvival)
{
	craft = obj_crafting_sidebar_survival;
}
else
{
	craft = obj_crafting_sidebar;
}
with (craft)
{
	var p_x = x;
	var p_y = y;
	var p_w = sprite_get_width(spr_crafting_sidebar_bg);
	var p_h = sprite_get_height(spr_crafting_sidebar_bg);
	if (isOpen)
	{
		p_w += sprite_get_width(spr_crafting_list_bg)-4;
	}
	
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverCrafting = true;
		global.mouseOverUI = true;
	}
}

// special case for fullscreen
var ww = sprite_get_width(spr_btn_exit_fullscreen);
if (global.isFull && mx > (display_get_gui_width()/2) - ww && mx < (display_get_gui_width()/2) + ww && my < 30)
{
	global.mouseOverUI = true;
}

// interactable size of ui_window objects is defined inside the objects themselves
with (obj_ui_window)
{
	var p_w = w_width*2;
	var p_h = w_height*2;
	var p_x = x - (p_w/2);
	var p_y = y - (p_h/2);
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverUI = true;
	}
}

with (obj_stash_panel)
{
	var p_w = w_width*2;
	var p_h = w_height*2;
	var p_x = x - (p_w/2);
	var p_y = y - (p_h/2);
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverInventory = true;
	}
}

with (obj_chest_panel)
{
	var p_w = w_width*2;
	var p_h = w_height*2;
	var p_x = x - (p_w/2);
	var p_y = y - (p_h/2);
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverInventory = true;
	}
}

with (obj_safe_panel)
{
	var p_w = w_width*2;
	var p_h = w_height*2;
	var p_x = x - (p_w/2);
	var p_y = y - (p_h/2);
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverInventory = true;
	}
}

with (obj_backpack_panel)
{
	var p_w = w_width*2;
	var p_h = w_height*2;
	var p_x = x - (p_w/2);
	var p_y = y - (p_h/2);
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverInventory = true;
	}
}

with (obj_safe_panel)
{
	var p_w = w_width*2;
	var p_h = w_height*2;
	var p_x = x - (p_w/2);
	var p_y = y - (p_h/2);
	if(other.mx > p_x && other.mx < p_x+p_w && other.my > p_y && other.my < p_y+p_h)
	{
		global.mouseOverInventory = true;
	}
}

if (ds_list_size(global.lockHUDparticipants) > 0)
{
	global.lockHUD = true;
}
else
{
	global.lockHUD = false;
}

