{
    "id": "63d15f9f-6204-4f0d-88c8-e70b22bb658f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_main_controller",
    "eventList": [
        {
            "id": "af5b2457-2b46-49aa-91cb-be18576d629c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        },
        {
            "id": "21d837b3-6067-49a0-9c23-a78b7d9841f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        },
        {
            "id": "6fbd3f38-4d7c-4acb-b682-e31581ae85f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        },
        {
            "id": "6d062e02-c0b3-4805-bb38-e49a4e8bc8fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        },
        {
            "id": "d0cb7a51-8661-4b64-8be9-c15ed0f42d7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        },
        {
            "id": "156bc04f-22e7-4f2d-b2bf-d6a4d855964a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        },
        {
            "id": "08b5a8a5-e721-4917-b7fa-05a0dc845986",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "63d15f9f-6204-4f0d-88c8-e70b22bb658f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}