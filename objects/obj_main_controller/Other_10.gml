/// @description Go to farm
// IMPORTANT: this function has to stay here since it could be called from different sources
// DOUBT: I believe this is pretty useless here
if(global.isSurvival)
{
	scr_prepareSurvival(WildType.Rural);
	room_goto(roomSurvival);
}
else
{
	if(global.isChallenge)
	{
		room_goto(roomChallenge);
	}
	else
	{
		room_goto(roomFarm);
	}
}