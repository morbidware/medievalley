/// @description Print version
if(global.isSurvival)
{
	var ver = "Build of: "+datetime_string+"\n";
}
else
{
	var ver = "Feudalife "+GM_version+" ALPHA\n"+datetime_string;
}

if (os_browser == browser_not_a_browser)
{
	ver = "LOCAL TEST BUILD\n"+datetime_string;
}
if (global.ismobile)
{
	ver += " (Mobile)";
}
draw_set_halign(fa_right);
draw_set_valign(fa_bottom);
draw_set_font(global.FontStandardOutline);
draw_set_color(c_white);
draw_text(display_get_gui_width()-16, display_get_gui_height()-10, ver);

if(ds_list_size(global.lockHUDparticipants) > 0)
{
	for(var i = 0; i < ds_list_size(global.lockHUDparticipants); i++)
	{
		var inst_id = ds_list_find_value(global.lockHUDparticipants,i);
		if(!instance_exists(inst_id))
		{
			ds_list_delete(global.lockHUDparticipants,i);
			i--;
		}
		else
		{
			//var obj_name = object_get_name(inst_id.object_index);
			//draw_text(display_get_gui_width()-16, 20+10*i, string(inst_id)+":"+string(obj_name));
		}
	}
}

draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_set_font(global.FontStandardOutline);
draw_set_color(c_white);
if(room == roomMain || room == roomInit)
{
	draw_text(16, display_get_gui_height()-20, "copyright IndieGala 2018-2021");
}
draw_text(16, display_get_gui_height()-10, "www.indiegala.com");

// Draw mouse sprite
if(!global.ismobile)
{
	draw_sprite_ext(spr_mouse_cursor,0,mouse_get_relative_x(),mouse_get_relative_y(),2,2,0,c_white,1);
}
draw_text(10,40,"fps: " + string(fps));
draw_text(10,60,"real fps: " + string(fps_real));
exit;
draw_set_font(global.FontStandardBoldOutlineX2);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_text(10,40,"GUI:"+string(display_get_gui_width())+" "+string(display_get_gui_height()));
draw_text(10,60,"WIN:"+string(window_get_width())+" "+string(window_get_height()));
draw_text(10,80,"VIEW:"+string(camera_get_view_width(view_camera[0]))+" "+string(camera_get_view_height(view_camera[0])));
draw_text(10,100,"PORT:"+string(view_get_wport(0))+" "+string(view_get_hport(0)));
draw_text(10,120,"APPSURF:"+string(surface_get_width(application_surface))+" "+string(surface_get_height(application_surface)));
draw_text(10,140,"WIN POS:"+string(window_get_x())+" "+string(window_get_y()));
draw_text(10,160,"C SIZE:"+string(global.cw)+" "+string(global.ch));
if(variable_global_exists("lastcw"))
{
	draw_text(10,180,"LC SIZE:"+string(global.lastcw)+" "+string(global.lastch));
}
