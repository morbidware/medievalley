/// @description Go to challenge
// IMPORTANT: this function has to stay here since it could be called from different sources
// DOUBT: I believe this is pretty useless here
if(!global.online)
{
	global.newUserForChallenge = false;
	room_goto(roomChallenge);
}
else
{
	if(global.newUserForChallenge)
	{
		room_goto(roomChallenge);
	}
	else
	{
		room_goto(roomChallenge);
	}
}
