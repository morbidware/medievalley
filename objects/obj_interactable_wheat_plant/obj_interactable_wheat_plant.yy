{
    "id": "1a940062-906b-4f37-a4b8-f0ee6147825f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_wheat_plant",
    "eventList": [
        {
            "id": "5bd72d27-552a-4106-a8f4-a16e56048c96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "1a940062-906b-4f37-a4b8-f0ee6147825f"
        },
        {
            "id": "10bdb6aa-0256-4b3f-889e-947d6b588528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "1a940062-906b-4f37-a4b8-f0ee6147825f"
        },
        {
            "id": "df686268-fdc5-4a4f-811a-684a4ca36d13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "1a940062-906b-4f37-a4b8-f0ee6147825f"
        },
        {
            "id": "081c4ac2-02f6-4d5c-8710-3dfc685f83af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "1a940062-906b-4f37-a4b8-f0ee6147825f"
        },
        {
            "id": "378e3511-38ea-4a84-a9b2-c8f5576c7fde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "1a940062-906b-4f37-a4b8-f0ee6147825f"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
    "visible": true
}