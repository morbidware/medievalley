event_inherited();
// Set a random seed so NPCs vay everytime
random_set_seed(round((global.dayprogress * 10000) + (global.life*1000) + (global.stamina*1000) + (global.gold*100)));
instance_create_depth(0,0,0,obj_marketGround);
player = instance_create_depth(8+16*16,8+16*27,0,obj_char);
player.lastdirection = Direction.Up;
player.prevx = 8+16*16;
player.prevy = 8+16*27;
instance_create_depth(0,0,0,obj_fadein);

audio_play_sound(bgm_market,0,true);
audio_play_sound(snd_market_ambient,0,true);
// hint
/*
var texts = ds_list_create();
ds_list_add(texts,"The market is empty today. More people will come later, maybe...");
var images = ds_list_create();
ds_list_add(images,noone);
scr_show_hint("",texts,images,false);
*/

instance_create_depth(player.x,player.y,0,obj_camera);
scr_gameItemCreate(16*14,8+16*23,obj_interactable_talking_npc,"talkingnpc");
scr_gameItemCreate(16*14,8+16*13,obj_interactable_selling_npc,"sellingnpc");
scr_gameItemCreate(16*22.5,8+16*8,obj_interactable_selling_npc_blueprints,"sellingnpcblueprints");
scr_gameItemCreate(16*5,8+16*8,obj_interactable_selling_npc_potions,"sellingnpcpotions");