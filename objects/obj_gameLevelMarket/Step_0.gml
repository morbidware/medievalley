/// @description
event_inherited();
if(instance_exists(obj_console)){exit;}

//detect when the player is changing room
if (!changingRoom)
{
	with (obj_char)
	{
		// EXIT DOWN
		if (y > room_height-32)
		{
			// disconnection
			if(string_length(string(global.otherusername)) > 0)
			{
				global.otherusername = "";
				global.otheruserid = 0;
				global.othersocketid = "";
				global.othergridstring = "";
				global.otherpickables = "";
				global.otherinteractables = "";
			}
			
			
			// change room
			other.changingRoom = true;
			event_perform_object(obj_gameLevel, ev_other, ev_user0);
		}
	}
}