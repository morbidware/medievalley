{
    "id": "0ae42038-8b5f-497a-bf70-0c689e14eb02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevelMarket",
    "eventList": [
        {
            "id": "41c79ff9-ac6f-4b4a-875f-83c60eef0679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ae42038-8b5f-497a-bf70-0c689e14eb02"
        },
        {
            "id": "40800f24-a71c-40a6-95fa-7ac28c1be133",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0ae42038-8b5f-497a-bf70-0c689e14eb02"
        },
        {
            "id": "06e04673-b3b2-4363-8eef-32622425d79d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "0ae42038-8b5f-497a-bf70-0c689e14eb02"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}