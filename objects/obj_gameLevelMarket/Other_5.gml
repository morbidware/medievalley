/// @description Stop sounds, save last visit
global.lastVisitedPlace = LastVisitedPlace.City;
audio_stop_sound(bgm_market);
audio_stop_sound(snd_market_ambient);