{
    "id": "1897bf41-d3a2-4fc1-8e0b-9cae5b261f5e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_reward_0002",
    "eventList": [
        {
            "id": "c7d3429f-a927-4c26-8b7b-a17584307369",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1897bf41-d3a2-4fc1-8e0b-9cae5b261f5e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c432edf4-bd16-4c2a-ade8-374a274ee197",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}