///@description sfx plus
if(global.sfxvolume < 1)
{
	global.sfxvolume += 0.1;
	global.sfxvolume = clamp(global.sfxvolume, 0.0, 1.0);
	audio_group_set_gain(audiogroup_sfx, global.sfxvolume, 0);
}