//if (live_call()) return live_result;

btnClose.oy = -height/2 + 18;

draw_set_color(c_white);
draw_set_alpha(1.0);
height = 420;

if(global.isSurvival)
{
	scr_nine_slice_at(spr_9slice_survival, display_get_gui_width()/2, y, width, height, 2);
}
else
{
	scr_nine_slice_at(spr_9slice_letter, display_get_gui_width()/2, y, width, height, 2);
}
//------------------------------------------------

draw_set_font(global.FontStandardBoldOutlineX2);
draw_set_valign(fa_middle);
draw_set_halign(fa_center);
draw_text(x,y-height/2+26,"SETTINGS");

//------------------------------------------------

draw_set_font(global.FontStandardBold);
draw_set_color(c_black);
draw_text(x,y-(height/2)+56,"MUSIC VOLUME");
draw_text(x,y-(height/2)+116,"SFX VOLUME");
draw_set_color(c_white);
if(global.isSurvival)
{
	draw_sprite(spr_settings_volume_survival, round(global.musicvolume * 10), x, y-(height/2)+76);
	draw_sprite(spr_settings_volume_survival, round(global.sfxvolume * 10), x, y-(height/2)+136);
}
else
{
	draw_sprite(spr_settings_volume, round(global.musicvolume * 10), x, y-(height/2)+76);
	draw_sprite(spr_settings_volume, round(global.sfxvolume * 10), x, y-(height/2)+136);
}

btnMasterMinus.oy = -(height/2)+76;
btnMasterPlus.oy = -(height/2)+76;
btnSfxMinus.oy = -(height/2)+136;
btnSfxPlus.oy = -(height/2)+136;

//------------------------------------------------

draw_set_color(c_black);
draw_text(x,y-(height/2)+186,"ALLOW ONLINE VISITORS");
btnVisitors.ox = 0;
btnVisitors.oy = -(height/2)+206;
if (global.allowvisitors == 1)
{
	btnVisitors.text = "YES";
}
else
{
	btnVisitors.text = "NO";
}

//------------------------------------------------

draw_set_color(c_black);
draw_text(x,y-(height/2)+246,"GRAPHICS QUALITY");
btnQuality.ox = 0;
btnQuality.oy = -(height/2)+266;
if (global.graphicsquality == 1)
{
	btnQuality.text = "HIGH";
}
else
{
	btnQuality.text = "LOW";
}

//------------------------------------------------

draw_set_color(c_black);
draw_text(x,y-(height/2)+306,"SHOW COMMAND AND TOOL HINTS");
btnHints.ox = 0;
btnHints.oy = -(height/2)+326;
if (global.showhints == 1)
{
	btnHints.text = "YES";
}
else
{
	btnHints.text = "NO";
}

//------------------------------------------------

draw_set_color(c_black);
draw_text(x,y-(height/2)+366,"USE MOBILE CONTROLS");
btnMobile.ox = 0;
btnMobile.oy = -(height/2)+386;
if (global.ismobile == 1)
{
	btnMobile.text = "YES";
}
else
{
	btnMobile.text = "NO";
}