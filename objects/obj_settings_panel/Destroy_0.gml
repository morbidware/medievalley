event_inherited();

instance_destroy(btnClose);
instance_destroy(btnMasterMinus);
instance_destroy(btnMasterPlus);
instance_destroy(btnSfxMinus);
instance_destroy(btnSfxPlus);
instance_destroy(btnVisitors);
instance_destroy(btnQuality);
instance_destroy(btnHints);
instance_destroy(btnMobile);