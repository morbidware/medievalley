///@description music plus
if(global.musicvolume < 1)
{
	global.musicvolume += 0.1;
	global.musicvolume = clamp(global.musicvolume, 0.0, 1.0);
	audio_group_set_gain(audiogroup_music, global.musicvolume, 0);
}