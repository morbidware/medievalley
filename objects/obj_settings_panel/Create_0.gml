event_inherited();
depth = -900;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -800;

width = 300;
height = 160;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
if(global.isSurvival)
{
	btnClose.sprite_index = spr_btn_close_survival;
	btnClose.image_xscale = btnClose.image_xscale / 2;
	btnClose.image_yscale = btnClose.image_yscale / 2;
}
else
{
	btnClose.sprite_index = spr_btn_close_silver;
}

btnClose.ox = width/2 - 18;
btnClose.oy = -height/2 + 18;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;

// ------------------------------------------

if(global.isSurvival)
{
	var sprminus = spr_settings_btn_minus_survival;
	var sprplus = spr_settings_btn_plus_survival;
}
else
{
	var sprminus = spr_settings_btn_minus;
	var sprplus = spr_settings_btn_plus;
}

btnMasterMinus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnMasterMinus.sprite_index = sprminus;
btnMasterMinus.ox = -80;
btnMasterMinus.oy = -8;
btnMasterMinus.caller = id;
btnMasterMinus.evt = ev_user1;
btnMasterMinus.depth = depth-1;

btnMasterPlus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnMasterPlus.sprite_index = sprplus;
btnMasterPlus.ox = 80;
btnMasterPlus.oy = -8;
btnMasterPlus.caller = id;
btnMasterPlus.evt = ev_user2;
btnMasterPlus.depth = depth-1;

// ------------------------------------------

btnSfxMinus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnSfxMinus.sprite_index = sprminus;
btnSfxMinus.ox = -80;
btnSfxMinus.oy = 48;
btnSfxMinus.caller = id;
btnSfxMinus.evt = ev_user3;
btnSfxMinus.depth = depth-1;

btnSfxPlus = instance_create_depth(0,0,-10001,obj_btn_generic);
btnSfxPlus.sprite_index = sprplus;
btnSfxPlus.ox = 80;
btnSfxPlus.oy = 48;
btnSfxPlus.caller = id;
btnSfxPlus.evt = ev_user4;
btnSfxPlus.depth = depth-1;

// ------------------------------------------

btnVisitors = instance_create_depth(0,0,-10001,obj_btn_generic);
btnVisitors.sprite_index = spr_popup_btn;
btnVisitors.ox = 0;
btnVisitors.oy = 88;
btnVisitors.caller = id;
btnVisitors.evt = ev_user5;
btnVisitors.depth = depth-1;

// ------------------------------------------

btnQuality = instance_create_depth(0,0,-10001,obj_btn_generic);
btnQuality.sprite_index = spr_popup_btn;
btnQuality.ox = 0;
btnQuality.oy = 128;
btnQuality.caller = id;
btnQuality.evt = ev_user6;
btnQuality.depth = depth-1;

// ------------------------------------------

btnHints = instance_create_depth(0,0,-10001,obj_btn_generic);
btnHints.sprite_index = spr_popup_btn;
btnHints.ox = 0;
btnHints.oy = 168;
btnHints.caller = id;
btnHints.evt = ev_user7;
btnHints.depth = depth-1;

// ------------------------------------------

btnMobile = instance_create_depth(0,0,-10001,obj_btn_generic);
btnMobile.sprite_index = spr_popup_btn;
btnMobile.ox = 0;
btnMobile.oy = 208;
btnMobile.caller = id;
btnMobile.evt = ev_user8;
btnMobile.depth = depth-1;