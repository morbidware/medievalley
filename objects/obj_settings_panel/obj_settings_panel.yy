{
    "id": "73033742-9038-44fe-a25f-563ccac191eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_settings_panel",
    "eventList": [
        {
            "id": "c226c936-8ea5-4820-9ecc-a730086120d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "e2fbb322-ee36-4933-b7fb-a028523d1da9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "345c2962-e77d-4b2a-ad04-73b1308c4611",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "6ed7383b-85e3-413a-9e27-4e6fd56518cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "3a9d7431-c5d0-452b-9da2-25a728f5ca82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "de3bec85-f596-4f91-96b1-89c98f88f355",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "3be3cec3-ecc9-449e-82e1-959e52f283db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "6f950b3e-c131-4e9d-97fb-ea11b31ffdef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "6ec610b7-b630-4b10-9663-f5089884472b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "a28a322f-955a-4437-92fd-568a5dae50e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "f1c64b80-6c2c-4b88-92b1-098d6a2a04b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        },
        {
            "id": "6e6da131-f166-4905-a83c-8dde4d33f355",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 7,
            "m_owner": "73033742-9038-44fe-a25f-563ccac191eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}