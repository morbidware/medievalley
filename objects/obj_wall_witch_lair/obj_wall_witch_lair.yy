{
    "id": "9496e153-331e-41af-983e-918d2c79e2c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wall_witch_lair",
    "eventList": [
        {
            "id": "e4101975-85ff-4702-8c2e-0510d716a886",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9496e153-331e-41af-983e-918d2c79e2c0"
        },
        {
            "id": "4c9b2213-6f69-40c7-8f32-83578c942f78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9496e153-331e-41af-983e-918d2c79e2c0"
        }
    ],
    "maskSpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
    "overriddenProperties": null,
    "parentObjectId": "71187ee3-21dc-4a51-b7ca-a4ebfb966176",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}