{
    "id": "5bdf45f7-4dfd-4b64-ab97-7eaf7c6fd97b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_farmboard",
    "eventList": [
        {
            "id": "28e808cb-6e7f-4461-bc61-f5e7d418c1ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5bdf45f7-4dfd-4b64-ab97-7eaf7c6fd97b"
        },
        {
            "id": "dd1b18f5-404b-42ca-b590-9c2fe7b79f97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5bdf45f7-4dfd-4b64-ab97-7eaf7c6fd97b"
        },
        {
            "id": "19da5ea3-cdd4-4bc5-b802-741e748b908c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "5bdf45f7-4dfd-4b64-ab97-7eaf7c6fd97b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8810aa63-fbdd-40ec-b4f3-fbab8943502e",
    "visible": true
}