{
    "id": "7af05e8b-11ad-47a2-94d3-a37dd1010deb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_nightwolf",
    "eventList": [
        {
            "id": "bbd34339-dcda-407b-bb72-806116457871",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "7af05e8b-11ad-47a2-94d3-a37dd1010deb"
        },
        {
            "id": "a50a7080-83e3-4c25-801d-88e45a315dc0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "7af05e8b-11ad-47a2-94d3-a37dd1010deb"
        }
    ],
    "maskSpriteId": "fe96c7e8-322c-44fa-86bf-6a83278ae13d",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73e72f19-6ffd-4b28-a5da-c77a2366206e",
    "visible": true
}