{
    "id": "1dc0c606-42a8-43c1-8ab1-b03688506459",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipe_sword",
    "eventList": [
        {
            "id": "2726f3c0-be2a-450c-b8cd-a10efb7517a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1dc0c606-42a8-43c1-8ab1-b03688506459"
        },
        {
            "id": "d34f5963-f2ab-4ee1-974e-2a51bc403111",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1dc0c606-42a8-43c1-8ab1-b03688506459"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "93f355f3-d76a-4841-a79d-f9b850cd184e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}