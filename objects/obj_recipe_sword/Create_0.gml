/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",2);
ds_map_add(ingredients,"flint",1);
ds_map_add(ingredients,"ironnugget",2);

resitemid = "sword";
unlocked = true;
desc = "The best defense is a good offense, said general Eduardo before falling to his death.";