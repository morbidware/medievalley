{
    "id": "93fc0e7a-6e9d-46fd-a7b0-f4d0a6390193",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_library",
    "eventList": [
        {
            "id": "0ba86097-bc08-4f54-bbca-f4bb2e2a2f59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "93fc0e7a-6e9d-46fd-a7b0-f4d0a6390193"
        },
        {
            "id": "aaea8808-bc33-4cbd-aa09-5d5b2d4a417c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "93fc0e7a-6e9d-46fd-a7b0-f4d0a6390193"
        },
        {
            "id": "55496169-967f-4ee2-b397-d7f7167e79c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "93fc0e7a-6e9d-46fd-a7b0-f4d0a6390193"
        },
        {
            "id": "d379f2ed-7827-445b-9a8a-364d70b3e8d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "93fc0e7a-6e9d-46fd-a7b0-f4d0a6390193"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9685b744-5ccd-46fb-9b1b-7d1219131e06",
    "visible": true
}