{
    "id": "605f262a-99e3-41d4-bb6f-1fe3b29e8d64",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_popup_newgame",
    "eventList": [
        {
            "id": "f900227f-d983-4173-adc7-3cfdbba6cb55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "605f262a-99e3-41d4-bb6f-1fe3b29e8d64"
        },
        {
            "id": "b8ade6da-01ed-4299-9e8f-14f060c2a90d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "605f262a-99e3-41d4-bb6f-1fe3b29e8d64"
        },
        {
            "id": "2ece9354-3505-4bb4-9144-8ea1d6724710",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "605f262a-99e3-41d4-bb6f-1fe3b29e8d64"
        },
        {
            "id": "d29fd9fc-b31a-4835-987e-8c2021894c3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "605f262a-99e3-41d4-bb6f-1fe3b29e8d64"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2ebf8beb-1b8b-4bd7-97dc-c95142cc658d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "823d17b0-4f0e-4c85-8cc9-bab9e222d64c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "4cb91fc8-6e59-4cb4-a3a5-7e8634e7fdde",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "486e5e32-433a-458d-ab83-b621e40f5c57",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "12ba2972-cd3f-4f4c-bc78-aa11beac4114",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}