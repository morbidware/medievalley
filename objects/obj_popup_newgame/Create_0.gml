event_inherited();

type = 2;
title = "ATTENTION!";
body = "Starting a new game will delete all previous progress. Are you sure?";