///@description Create buttons

btnYes = instance_create_depth(0,0,0,obj_btn_generic);
btnYes.sprite_index = spr_popup_btn_no;
btnYes.ox = -60;
btnYes.oy = 60;
btnYes.text = "YES";
btnYes.caller = id;
btnYes.evt = ev_user2;
btnYes.depth = depth - 1;
	
btnNo = instance_create_depth(0,0,0,obj_btn_generic);
btnNo.sprite_index = spr_popup_btn_yes;
btnNo.ox = 60;
btnNo.oy = 60;
btnNo.text = "NO";
btnNo.caller = id;
btnNo.evt = ev_user3;
btnNo.depth = depth - 1;