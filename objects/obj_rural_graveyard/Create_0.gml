event_inherited();

ds_map_add(grass,"wildgrass",80);
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}

ds_map_add(plants,"pine",40);
ds_map_add(plants,"hypericum",3);

// No boulders in this submap

ds_map_add(mobs,"wilddog",30);

landmark = obj_prefab_graveyard;
wall = obj_wall_invisible;

keeporientation = true;