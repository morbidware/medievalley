{
    "id": "4c0f3ac2-0fe2-4a1c-8315-f8419ae9314e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_bed",
    "eventList": [
        {
            "id": "d805dea7-0b54-4bfe-8d5d-3ea3d7ff4f1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "4c0f3ac2-0fe2-4a1c-8315-f8419ae9314e"
        },
        {
            "id": "82d6891c-c7fa-40ec-a7d9-ea3a5e6da4f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4c0f3ac2-0fe2-4a1c-8315-f8419ae9314e"
        },
        {
            "id": "b15fd1b0-b280-4be1-9ce4-5d13116e1bb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "4c0f3ac2-0fe2-4a1c-8315-f8419ae9314e"
        },
        {
            "id": "f83d3508-f2fd-42e1-8bbb-5da8a6c612ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "4c0f3ac2-0fe2-4a1c-8315-f8419ae9314e"
        },
        {
            "id": "a74f49af-3ed6-4278-a290-db418d9e9cdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4c0f3ac2-0fe2-4a1c-8315-f8419ae9314e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80f8f0e2-bcf7-4747-9f20-a7da2a4e04c1",
    "visible": true
}