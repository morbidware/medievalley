{
    "id": "4afa3e97-5d89-404f-9812-dba96ed0d392",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_list_survival",
    "eventList": [
        {
            "id": "b172eb51-27f3-45a0-be2d-3daa96c624d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4afa3e97-5d89-404f-9812-dba96ed0d392"
        },
        {
            "id": "4f56782f-0a80-438e-bcb9-e7f01061c936",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4afa3e97-5d89-404f-9812-dba96ed0d392"
        },
        {
            "id": "48241e85-20ca-434e-ac21-207e032b2482",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4afa3e97-5d89-404f-9812-dba96ed0d392"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
    "visible": true
}