{
    "id": "7c12befc-39b7-444c-8aa8-d51c3f00d929",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_witch_lair_rocks",
    "eventList": [
        {
            "id": "becbd184-5000-4c6c-9716-d902350bba55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c12befc-39b7-444c-8aa8-d51c3f00d929"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5e17ef47-27e0-4967-808f-9c2345513582",
    "visible": true
}