/// @description Initialization
//caching
if (global.online)
{
	socketid = getClientIdAtomic();
}
else
{
	socketid = "unassigned";
}
fg = instance_find(obj_baseGround,0);
game = instance_find(obj_gameLevel,0);

//customization
gender = "m";

custom_body = "none";
custom_body_index = 0;
custom_body_object = ds_list_create();
custom_body_sprite = noone;

custom_head = "none";
custom_head_index = 0;
custom_head_object = ds_list_create();
custom_head_sprite = noone;

custom_upper = "none";
custom_upper_index = 0;
custom_upper_object = ds_list_create();
custom_upper_sprite = noone;

custom_lower = "none";
custom_lower_index = 0;
custom_lower_object = ds_list_create();
custom_lower_sprite = noone;

renderhead = true;
renderlower = true;

if(global.ismobile)
{
	joystick = instance_create_depth(0,0,0,obj_joystick);
}
move = 0;
joystickDirection = 0;
if(global.ismobile)
{
	actionButton = instance_create_depth(0,0,0,obj_actionbutton);
	checkActionButton = false;
}


// touch controls
movetarget_x = -1;			// This is the position we want the player to move to.
movetarget_y = -1;
autotarget = noone;			// << Autotarget is either object or position, never both-
autotarget_x = -1;			// << though it's still safe to have both; in that case it will
autotarget_y = -1;			// << just use autotarget object and ingore position.
autotargetdistance = 0;
mobilepath = noone;
itemselection = noone;
lastmobileclickstatus = 0;

//navigation
spd = 1.5;
prevx = x;
prevy = y;
targetx = -1;
targety = -1;
navtarget = noone;
walking = false;
storey = 0;
onstairs = false;
currentspd = 0;
currentdir = 0;
col = floor(x-8/16);
row = floor(y-8/16);
cellstate = 0;

// pathfinding (for mobs) - this works like leaving breadcumbs behind so enemies will follow
pathspots_x = ds_list_create();
pathspots_y = ds_list_create();
ds_list_add(pathspots_x,x);
ds_list_add(pathspots_y,y);
newspottimer = 0;

// status
maxlife = 100;
defence = 0;
life = 100;
maxstamina = 100;
stamina = 100;
stomach = 100;
maxstomach = 100;
gold = 0;
galasilver = 0;
critchance = 5; //percentage
lvl = 1;
experience = 0;
tgtexperience = 100;
lasttimestamp = 0;
pagehidetimestamp = -1;

logga("global.gold " + string(global.gold));
if(!global.isChallenge)
{
	if(global.online && global.firstload)
	{
		if(global.isSurvival)
		{
			scr_parseUserdataSurvival(id);
			if(global.charx != -1 && global.chary != -1)
			{
				x = global.charx;
				y = global.chary;
			}
			else
			{
				x = 450;
				y = 750;
			}
		}
		else
		{
			scr_parseUserdata(id);
		}
	}
	else if (!global.firstload)
	{
		logga("LIFE: life = "+string(global.life)+" obj_char Create 96");
		life = global.life;
		stamina = global.stamina;
		gold = global.gold;
		if (m_rich)
		{
			global.gold = 99999;
			gold = 99999;
		}
		galasilver = global.galasilver;
	}
	global.firstload = false;
}
else
{
	gold = global.gold;
	galasilver = global.galasilver;
}

logga("obj_char Create: global.gold " + string(global.gold));
logga("obj_char Create: gold " + string(gold));

// select wardrobe parts now, after having loaded player data
logga("obj_char Create");
/*
logga("-"+string(ds_list_find_value(global.currentwardrobe,0)));
logga("-"+string(ds_list_find_value(global.currentwardrobe,1)));
logga("-"+string(ds_list_find_value(global.currentwardrobe,2)));
logga("-"+string(ds_list_find_value(global.currentwardrobe,3)));
logga("-"+string(ds_list_find_value(global.currentwardrobe,4)));

gender = string(ds_list_find_value(global.currentwardrobe,0));
logga("00");
scr_assignWardrobePartDirect(real(ds_list_find_value(global.currentwardrobe,1)),"body");
logga("000");
scr_assignWardrobePartDirect(real(ds_list_find_value(global.currentwardrobe,2)),"head");
logga("0000");
scr_assignWardrobePartDirect(real(ds_list_find_value(global.currentwardrobe,3)),"upper");
logga("00000");
scr_assignWardrobePartDirect(real(ds_list_find_value(global.currentwardrobe,4)),"lower");
logga("1");
*/
gender = "m";
scr_assignWardrobePartDirect(0,"body");
scr_assignWardrobePartDirect(0,"head");
scr_assignWardrobePartDirect(0,"upper");
scr_assignWardrobePartDirect(0,"lower");
// control input
left = 0;
right = 0;
up = 0;
down = 0;
enum CharControlType
{
	manual,
	auto
}
enum ConsumableType
{
	Eat,
	Drink,
	Cook,
	Learn
}
controlType = CharControlType.manual;
logga("2");
// interaction
instance_create_depth(0,0,0,obj_ui_actions);
state = ActorState.Idle;
nearest = 0;
interactiondistance = 16;
targetobject = noone;
cursorobject = noone;
highlightedobject = noone;
consumableobject = noone;
consumabletype = -1;
craftingid = "";
stopmagnet = false;
if (global.otheruserid != 0)
{
	stopmagnet = true;
}
logga("3");
// click detection
//clicking = false;
clickdelta = 0;
deltathreshold = 24;
logga("4");
// inventory management
inventory = instance_find(obj_playerInventory,0);
tempinventoryhit = noone;
tempslot = -1;
logga("5");
// sprite graphics
offsetx = 0;
offsety = 0;
anim_index = 0;
anim_speed = 0;
idle_time = 10;
shadowType = ShadowType.Sprite;
shadowBlobScale = 1;

// ground check
groundtype = GroundType.None;
itemontile = -1;
accessible = 0;

// step sound
stepid = 0;
stepinterval = 22;
steptimer = 0;

// timers
crono = 0;
interactTimer = 60;
eatTimer = 60;
attackTimer = 16;
hitTimer = 40;
dieTimer = 60;
pickTimer = 60;
pickTimerHard = 120;
pickUpTimer = 20;
chopTimer = 20;
craftTimer = 120;
hoeTimer = 20;
sowTimer = 30;
wateringTimer = 30;
shovelTimer = 30;
chargeTimer = 18;
sickleTimer = 20;
mineTimer = 24;
hammerTimer = 120;

if(global.isChallenge)
{
	switch(global.challengeType)
	{
		case ChallengeType.Chopping:
		chopTimer = ds_map_find_value(global.challengeData,"action_frames_normal");
		break;
		case ChallengeType.Sickling:
		sickleTimer = ds_map_find_value(global.challengeData,"action_frames_normal");
		break;
		case ChallengeType.Mining:
		mineTimer = ds_map_find_value(global.challengeData,"action_frames_normal");
		break;
		case ChallengeType.Watering:
		wateringTimer = ds_map_find_value(global.challengeData,"action_frames_normal");
		break;
		case ChallengeType.Fighting:
		maxlife = ds_map_find_value(global.challengeData,"player_life");
		logga("LIFE: maxlife = "+string(maxlife)+" obj_char Create 229");
		life = maxlife;
		logga("LIFE: life = "+string(life)+" obj_char Create 231");
		break;
		case ChallengeType.Picking:
		pickTimer = ds_map_find_value(global.challengeData,"action_frames_normal");
		pickTimerHard = ds_map_find_value(global.challengeData,"action_frames_wrong");
		hitTimer = ds_map_find_value(global.challengeData,"action_frames_pain");
		break;
		case ChallengeType.Hoeing:
		hoeTimer = ds_map_find_value(global.challengeData,"action_frames_normal");
		break;
	}
}
//utility
utilityCrono = 0;
sweatCrono = 0;
emoteCrono = 0;

inventory = noone;

if(global.isChallenge && instance_exists(obj_playerInventory))
{
	with(obj_inventory)
	{
		instance_destroy();
	}
	instance_destroy(instance_find(obj_playerInventory,0));
}

if (instance_exists(obj_playerInventory))
{
	inventory = instance_find(obj_playerInventory,0);
}
else
{
	inventory = instance_create_depth(0,0,0,obj_playerInventory);
}
with (inventory)
{
	event_perform(ev_other,ev_user0);
}

if(global.isSurvival)
{
	crafting = instance_create_depth(0,0,-2,obj_crafting_sidebar_survival);
}
else
{
	crafting = instance_create_depth(0,0,-2,obj_crafting_sidebar);
}

//lifebar = instance_create_depth(0,0,0,obj_lifebar);
if(global.isSurvival)
{
	dock = instance_create_depth(0,0,0,obj_ui_dock_survival);
}
else if(global.isChallenge)
{
	dock = instance_create_depth(0,0,0,obj_ui_challenge);
}
else
{
	dock = instance_create_depth(0,0,0,obj_ui_dock);
}
handState = HandState.Empty;
bookedInteraction = HandState.Empty;
objectsinrange = ds_list_create();
nearest = 999;

highlightCol = 0;
highlightRow = 0;
interactionCol = 0;
interactionRow = 0;

lastdirection = Direction.Right;

cullingtimer = 0;

aim_x = 0;
aim_y = 0;

datarefreshrate = 4; //send char data every n frames
senddatatimer = datarefreshrate;

scr_setSpriteAction("idle",Direction.Right,-1);

////////////////////////// SHADER STUFF (no longer used) ///////////////////////////
/*
colors_skin = ds_list_create();		// 3 shades
colors_pants = ds_list_create();	// 3 shades
colors_arms = ds_list_create();		// 3 shades
color_eyes = c_gray;				// single shade

// skin
s_skin1_r = shader_get_uniform(shaderCharacter,"skin1_r");
s_skin1_g = shader_get_uniform(shaderCharacter,"skin1_g");
s_skin1_b = shader_get_uniform(shaderCharacter,"skin1_b");

s_skin2_r = shader_get_uniform(shaderCharacter,"skin2_r");
s_skin2_g = shader_get_uniform(shaderCharacter,"skin2_g");
s_skin2_b = shader_get_uniform(shaderCharacter,"skin2_b");

s_skin3_r = shader_get_uniform(shaderCharacter,"skin3_r");
s_skin3_g = shader_get_uniform(shaderCharacter,"skin3_g");
s_skin3_b = shader_get_uniform(shaderCharacter,"skin3_b");

// eye
s_eye_r = shader_get_uniform(shaderCharacter,"eye_r");
s_eye_g = shader_get_uniform(shaderCharacter,"eye_g");
s_eye_b = shader_get_uniform(shaderCharacter,"eye_b");

// hair
s_hair1_r = shader_get_uniform(shaderCharacter,"hair1_r");
s_hair1_g = shader_get_uniform(shaderCharacter,"hair1_g");
s_hair1_b = shader_get_uniform(shaderCharacter,"hair1_b");

s_hair2_r = shader_get_uniform(shaderCharacter,"hair2_r");
s_hair2_g = shader_get_uniform(shaderCharacter,"hair2_g");
s_hair2_b = shader_get_uniform(shaderCharacter,"hair2_b");

s_hair3_r = shader_get_uniform(shaderCharacter,"hair3_r");
s_hair3_g = shader_get_uniform(shaderCharacter,"hair3_g");
s_hair3_b = shader_get_uniform(shaderCharacter,"hair3_b");

// sleeve
s_sleeve1_r = shader_get_uniform(shaderCharacter,"sleeve1_r");
s_sleeve1_g = shader_get_uniform(shaderCharacter,"sleeve1_g");
s_sleeve1_b = shader_get_uniform(shaderCharacter,"sleeve1_b");

s_sleeve2_r = shader_get_uniform(shaderCharacter,"sleeve2_r");
s_sleeve2_g = shader_get_uniform(shaderCharacter,"sleeve2_g");
s_sleeve2_b = shader_get_uniform(shaderCharacter,"sleeve2_b");

s_sleeve3_r = shader_get_uniform(shaderCharacter,"sleeve3_r");
s_sleeve3_g = shader_get_uniform(shaderCharacter,"sleeve3_g");
s_sleeve3_b = shader_get_uniform(shaderCharacter,"sleeve3_b");

// pants
s_pants1_r = shader_get_uniform(shaderCharacter,"pants1_r");
s_pants1_g = shader_get_uniform(shaderCharacter,"pants1_g");
s_pants1_b = shader_get_uniform(shaderCharacter,"pants1_b");

s_pants2_r = shader_get_uniform(shaderCharacter,"pants2_r");
s_pants2_g = shader_get_uniform(shaderCharacter,"pants2_g");
s_pants2_b = shader_get_uniform(shaderCharacter,"pants2_b");

s_pants3_r = shader_get_uniform(shaderCharacter,"pants3_r");
s_pants3_g = shader_get_uniform(shaderCharacter,"pants3_g");
s_pants3_b = shader_get_uniform(shaderCharacter,"pants3_b");

// shoes
s_shoes1_r = shader_get_uniform(shaderCharacter,"shoes1_r");
s_shoes1_g = shader_get_uniform(shaderCharacter,"shoes1_g");
s_shoes1_b = shader_get_uniform(shaderCharacter,"shoes1_b");

s_shoes2_r = shader_get_uniform(shaderCharacter,"shoes2_r");
s_shoes2_g = shader_get_uniform(shaderCharacter,"shoes2_g");
s_shoes2_b = shader_get_uniform(shaderCharacter,"shoes2_b");

s_shoes3_r = shader_get_uniform(shaderCharacter,"shoes3_r");
s_shoes3_g = shader_get_uniform(shaderCharacter,"shoes3_g");
s_shoes3_b = shader_get_uniform(shaderCharacter,"shoes3_b");
*/

var tex = sprite_get_uvs(sprite_index, image_index);
tex_left = tex[0];
tex_top = tex[1];
tex_right = tex[2];
tex_bottom = tex[3];

colorIndex = 0.0;
colorCrono = 0.0;

u_r = shader_get_uniform(shaderOutline,"u_r");
u_g = shader_get_uniform(shaderOutline,"u_g");
u_b = shader_get_uniform(shaderOutline,"u_b");

u_r2 = shader_get_uniform(shaderOutline,"u_r2");
u_g2 = shader_get_uniform(shaderOutline,"u_g2");
u_b2 = shader_get_uniform(shaderOutline,"u_b2");

u_bf = shader_get_uniform(shaderOutline,"u_bf");

/*
colors = ds_grid_create(3,7);

ds_grid_add(colors,0,0,255.0/255.0);
ds_grid_add(colors,1,0,0.0);
ds_grid_add(colors,2,0,0.0);

ds_grid_add(colors,0,1,255.0/255.0);
ds_grid_add(colors,1,1,127.0/255.0);
ds_grid_add(colors,2,1,0.0);

ds_grid_add(colors,0,2,255.0/255.0);
ds_grid_add(colors,1,2,255.0/255.0);
ds_grid_add(colors,2,2,0.0);

ds_grid_add(colors,0,3,0.0);
ds_grid_add(colors,1,3,255.0/255.0);
ds_grid_add(colors,2,3,0.0);

ds_grid_add(colors,0,4,0.0);
ds_grid_add(colors,1,4,0.0);
ds_grid_add(colors,2,4,255.0/255.0);

ds_grid_add(colors,0,5,75.0/255.0);
ds_grid_add(colors,1,5,0.0);
ds_grid_add(colors,2,5,130.0/255.0);

ds_grid_add(colors,0,6,148.0/255.0);
ds_grid_add(colors,1,6,0.0);
ds_grid_add(colors,2,6,211.0/255.0);
*/

flashCrono = 0;

var ii = image_index*10;
ii = round(ii);
ii = ii/10.0;
var sx = image_xscale*10;
sx = round(sx);
sx = sx/10.0;
var sy = image_yscale*10;
sy = round(sy);
sy = sy/10.0;

if(room != roomWild && global.online)
{
	sendCharData(round(x),round(y),custom_body_sprite, custom_head_sprite, custom_lower_sprite, custom_upper_sprite,anim_index,sx,sy,socketid,global.username,stamina);
}

indicator = noone;
if(global.isChallenge && global.challengeType == ChallengeType.Fighting && global.ismobile)
{
	indicator = instance_create_depth(0,0,0,obj_mob_out_of_screen);
}

firefly = noone;
fishingfloat=instance_create_depth(x,y,0,obj_fishingfloat);
fishingfloat.visible = false;
logga("--------------------------");