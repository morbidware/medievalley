{
    "id": "77364650-86b0-4dd9-80c5-06596631108c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_char",
    "eventList": [
        {
            "id": "778ea4ac-cb45-41c7-a304-02fe48c27c11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "10ddc8ea-6e39-4326-a313-a797d18ee82c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "5e8f3a2c-9fe3-4810-8412-34e7e40b906a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "10f98d8b-f075-4b22-9711-daee4b4af9d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "0a40d664-f32f-4015-81a8-18658e691d30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "c86a8c5f-0d4f-4272-a5d8-49d044876710",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "33b05780-e659-4fb5-b2ae-f8ac6bdb8393",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "e318189a-798e-46e9-83a1-27e7ce62f965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "3ed72568-6599-472f-a963-36e31595cb08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "749d6530-6618-49cc-b8c7-cdd8c27620a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        },
        {
            "id": "b5f786d5-963c-4a1d-b19a-7e55e933a478",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 23,
            "eventtype": 7,
            "m_owner": "77364650-86b0-4dd9-80c5-06596631108c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ea533252-0ed6-4cc2-9d4a-57d3dc92a987",
    "visible": true
}