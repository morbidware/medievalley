///@description Self drawing, ground highlight

// debug interaction circle
//draw_circle(aim_x,aim_y,interactiondistance,true);

// this enables the temporary stupid brother of the protagonist
//scr_drawChar();

// debug socket id
/*
if (global.online)
{
	draw_set_font(global.Font);
	draw_set_halign(fa_center);
	draw_text(x, y - 24, string(socketid));
}
*/
// draw highlighted tile first so it goes behind the player
if(handState == HandState.Hoe ||
	handState == HandState.Sow ||
	handState == HandState.Watering ||
	handState == HandState.Shovel ||
	handState == HandState.Build ||
	handState == HandState.Fish)
{	
	// get highlight column and row directly from aim point
	highlightCol = floor((aim_x) / 16);
	highlightRow = floor((aim_y) / 16);
	
	// save highlighted tile data
	groundtype = ds_grid_get(fg.grid, highlightCol, highlightRow);
	itemontile = ds_grid_get(fg.itemsgrid, highlightCol, highlightRow);
	accessible = ds_grid_get(fg.gridaccess, highlightCol, highlightRow);
	
	//determine if condition is met to show the green highlight, otherwise show the red one
	var condition = false;
	if (accessible == 1)
	{
		if (handState == HandState.Hoe && groundtype == GroundType.Soil && itemontile == -1)
		{
			 condition = true;
		}
		if (handState == HandState.Sow && groundtype == GroundType.Plowed && itemontile == -1)
		{
			 condition = true;
		}
		if (handState == HandState.Watering && groundtype == GroundType.Plowed)
		{
			 condition = true;
		}
		if (handState == HandState.Shovel)
		{
			if (itemontile > 0 && itemontile != undefined)
			{
				if (event_perform_ret(itemontile, ev_other, ev_user1) == true && itemontile.interactionPrefixRight == "Remove")
				{
					condition = true;
				}
			}
			else if (groundtype == GroundType.Grass || groundtype == GroundType.TallGrass)
			{
				condition = true;
			}
		}
		if (handState == HandState.Build && itemontile == -1)
		{
			 condition = true;
		}
		if(handState == HandState.Fish && groundtype == GroundType.Water)
		{
			condition = true;	
		}
		if (handState == HandState.Empty)
		{
			condition = false;
		}
	}
	
	if (/*!global.ismobile && */global.showhints)
	{
		if (condition == true)
		{
			draw_sprite(spr_tile_overlay_2, 0, (highlightCol * 16)+8, (highlightRow * 16)+8);
		}
		else
		{
			draw_sprite(spr_tile_overlay_2, 1, (highlightCol * 16)+8, (highlightRow * 16)+8);
		}
	}
}

// self draw and shaders
if(flashCrono > 0)
{
	shader_set(shaderFlash);
	if(flashCrono mod 2 == 0)
	{
		shader_set_uniform_f(shader_get_uniform(shaderFlash,"u_intensity"),1.0);
	}
	else
	{
		shader_set_uniform_f(shader_get_uniform(shaderFlash,"u_intensity"),0.0);
	}
}

// draw char sprites
if (custom_body != "none" && custom_body_sprite > 0)
{
	draw_sprite_ext(custom_body_sprite,floor(anim_index),x,y-16,image_xscale,image_yscale,image_angle,c_white,1);
}
if (custom_head != "none" && custom_head_sprite > 0 && renderhead)
{
	draw_sprite_ext(custom_head_sprite,floor(anim_index),x,y-16,image_xscale,image_yscale,image_angle,c_white,1);
}
if (custom_lower != "none" && custom_lower_sprite > 0 && renderlower)
{
	draw_sprite_ext(custom_lower_sprite,floor(anim_index),x,y-16,image_xscale,image_yscale,image_angle,c_white,1);
}
if (custom_upper != "none" && custom_upper_sprite > 0)
{
	draw_sprite_ext(custom_upper_sprite,floor(anim_index),x,y-16,image_xscale,image_yscale,image_angle,c_white,1);
}

if(flashCrono > 0)
{
	shader_reset();
	flashCrono --;
}

// DEBUG STUFF
/*
draw_set_alpha(1);
if (onstairs)
{
	draw_set_color(c_green);
	draw_circle(x,y-48,6,false);
}
else {
	draw_set_color(c_red);
	draw_circle(x,y-48,6,false);
}
draw_set_color(c_white);

draw_set_color(c_purple);
for (var i = 0; i < ds_list_size(pathspots_x); i++)
{
	draw_circle(ds_list_find_value(pathspots_x,i),ds_list_find_value(pathspots_y,i),2,false);
}
draw_set_color(c_white);
*/
if(room == roomChallenge && global.challengeType == ChallengeType.Fighting)
{
	logga("life: " + string(life) + " maxlife: " + string(maxlife));
	draw_set_alpha(1.0);
	var uiheight = 32;
	//var barsize = clamp(12 + floor(maxlife / 5), 12, 96);
	var barsize = 32;
	
	draw_set_color(c_black);
	draw_rectangle(x-(barsize/2)-1, y-uiheight-3, x+(barsize/2)+1, y-uiheight, false);
	
	draw_set_color(make_color_rgb(64,64,64));
	draw_rectangle(x-(barsize/2), y-uiheight-2, x+(barsize/2), y-uiheight-1, false);
	
	draw_set_color(c_red);
	draw_rectangle(x-(barsize/2), y-uiheight-2, x-(barsize/2) + round(barsize * (life/maxlife)), y-uiheight-1, false);
}