/// @description Play step sound
if (state != ActorState.Idle)
{
	exit;
}

if (stepid == 0)
{
	var s = audio_play_sound(snd_step_left,0,false);
	audio_sound_pitch(s, 0.9 + random (0.2));
	stepid = 1;
	steptimer = stepinterval+random(1);
}
else
{
	var s = audio_play_sound(snd_step_right,0,false);
	audio_sound_pitch(s, 0.9 + random (0.2));
	stepid = 0;
	steptimer = stepinterval+random(1);
}