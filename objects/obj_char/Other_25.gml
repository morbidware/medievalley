/// @description Object culling

// UNUSED, COMMENTED FOR BACKUP - ACTUAL CULLING IS ON OBJ_CAMERA
/*
// only deactivate objects that may cause severe slowdowns
instance_deactivate_object(obj_interactable);
instance_deactivate_object(obj_pickable);
instance_deactivate_object(obj_fx);
instance_deactivate_object(obj_groundtile);
instance_deactivate_object(obj_statictile);
instance_deactivate_object(obj_waterfoam);
instance_deactivate_object(obj_water);
instance_deactivate_object(obj_maplimit);
instance_deactivate_object(obj_wildlife);
instance_deactivate_object(obj_enemy);
instance_deactivate_object(obj_wall);
// always activate objects inside the view
var m = 50;
var mx = sin(degtorad(global.shadowangle)) * 20;
var my = cos(degtorad(global.shadowangle)) * 20;
instance_activate_region(global.viewx-m+mx, global.viewy-m+my, 480+(m*2)+(mx), 320+(m*2)+(my), true);
*/