/// @description Interaction Left press
// hand actions and UI actions

// stop interaction during fade
if (instance_exists(obj_fadein) || instance_exists(obj_fadeout))
{
	exit;
}
var craft = "";
if(global.isSurvival)
{
	craft = obj_crafting_sidebar_survival;
}
else
{
	craft = obj_crafting_sidebar;
}
global.lastMouseButton = "left";
targetobject = highlightedobject;
clickdelta = 0;
//clicking = true;

// -------------------------- CRAFTING --------------------------

// use crafting UI
if(global.mouseOverCrafting && !global.lockHUD && state == ActorState.Idle)
{
	var craftcatHit = collision_point(mouse_get_relative_x(),mouse_get_relative_y(),obj_crafting_cat,false,true);
	var craftbtnupHit = collision_point(mouse_get_relative_x(),mouse_get_relative_y(),obj_craft_btn_up ,false,true);
	var craftbtndownHit = collision_point(mouse_get_relative_x(),mouse_get_relative_y(),obj_craft_btn_down ,false,true);
	var recipeHit = collision_point(mouse_get_relative_x(),mouse_get_relative_y(),obj_recipe,false,true);
	if(craftcatHit)
	{
		for(var i = 0; i < instance_number(obj_crafting_cat); i++)
		{
			var craftingcat = instance_find(obj_crafting_cat,i);
			if(craftingcat != craftcatHit)
			{
				if(craftingcat.state == 1)
				{
					craftingcat.state = 0;
				}
			}
			else
			{
				if(craftingcat.state == 0)
				{
					// open category
					craftingcat.state = 1;
					with (craft)
					{
						event_perform(ev_other,ev_user2);
					}
				}
				else
				{
					craftingcat.state = 0;
				}
			}
		}
	}
	else if(craftbtnupHit)
	{
		with(craft)
		{
			event_perform(ev_other,ev_user0);
		}
	}
	else if(craftbtndownHit)
	{
		with(craft)
		{
			event_perform(ev_other,ev_user1);
		}
	}
	else if(recipeHit)
	{
		with(recipeHit)
		{
			event_perform(ev_other, ev_user0);
		}
	}
	exit;
}

// -------------------------- INVENTORY --------------------------

// pre-select item from inventory
if (global.mouseOverInventory && state == ActorState.Idle)
{
	tempslot = scr_getInventorySlot();
	tempinventoryhit = scr_getItemFromInventory(tempslot);
	
	//show_debug_message("SLOT: "+string(tempslot)+" - HIT: "+string(tempinventoryhit));
}

// -------------------------- INTERACTION --------------------------

// disable any action if mouse is over interface or if game is mobile
if (global.mouseOverInventory || global.mouseOverUI || global.mouseOverCrafting || global.mouseOverDock || global.lockHUD/* || global.ismobile*/)
{
	exit;
}
if(global.ismobile && !checkActionButton){
	exit;
}
// EXIT SLEEP (before state check, since it will be in Rest state now)
with (obj_interactable)
{
	
	if (other.highlightedobject == id && interactionPrefixLeft == "Exit")
	{
		
		event_perform(ev_other, ev_user2);
		other.crono = 0;
		scr_setState(other.id,ActorState.Idle);
		other.visible = true;
		isbusy = false;
		if(global.online)
		{
			sendCharResting(global.socketid,0.0);
			if (canSendNetEvents)
			{
				eventPerformInteractable(x,y,id,object_index,ev_other,ev_user2);
			}
		}
	}
}

// disable any other input when doing other actions
if((state != ActorState.Idle && crono <= 0) || crono > 0)
{
	exit;
}

// PICK UP - commented out as pickables are now "magnetized"
/*
with (obj_pickable)
{
	if (other.highlightedobject == id)
	{
		scr_setState(other.id, ActorState.PickUp);
	}
}
*/

// PICK
with (obj_interactable)
{
	if (other.highlightedobject == id && (interactionPrefixLeft == "Pick" || interactionPrefixLeft == "Heal")  && global.otheruserid == 0)
	{
		scr_setState(other.id, ActorState.Pick);
	}
	else if (other.highlightedobject == id && string_pos("plant",other.highlightedobject.itemid) > 0 && growstages > 3 && interactionPrefixLeft == "Pick")
	{
		scr_setState(other.id, ActorState.Pick);
	}
}

// CHECK
with (obj_interactable)
{
	if (other.highlightedobject == id && (interactionPrefixLeft == "Check" || interactionPrefixLeft == "Talk" || interactionPrefixLeft == "Trade") && global.otheruserid == 0)
	{
		scr_setState(other.id, ActorState.Pick);
	}
}

// SLEEP IN
with (obj_interactable)
{
	if (other.highlightedobject == id && isbusy == false && interactionPrefixLeft == "Sleep in")
	{
		
		// Pallet can be used by visitors too
		if (other.highlightedobject.itemid == "pallet")
		{
			scr_setState(other.id, ActorState.Rest);
			other.visible = false;
			isbusy = true;
			if(global.online)
			{
				sendCharResting(global.socketid,1.0);
			}
		}
		// All other sleeping objects can't be used by visitors
		if (global.otheruserid == 0)
		{
			scr_setState(other.id, ActorState.Rest);
			other.visible = false;
			isbusy = true;
			if(global.online)
			{
				sendCharResting(global.socketid,1.0);
			}
		}
	}
}

// ENTER HOUSE
with (obj_interactable)
{
	if (other.highlightedobject == id && interactionPrefixLeft == "Enter" && global.otheruserid == 0)
	{
		with(obj_gameLevel)
		{
			if (!changingRoom)
			{
				changingRoom = true;
				event_perform(ev_other,ev_user3);
			}
		}
	}
}


// REBUILD
with (obj_interactable)
{
	if (other.highlightedobject == id && (interactionPrefixLeft == "Rebuild" || interactionPrefixLeft == "Light up") && global.otheruserid == 0)
	{
		//show_debug_message("REBUILD");
		var k = ds_map_find_first(ingredients);
		if(event_perform_ret(id,ev_other,ev_user1))
		{
			for(var i = 0; i < ds_map_size(ingredients); i++)
			{
				var itemid = k;
				var itemq = ds_map_find_value(ingredients,k);
		
				show_debug_message("I NEED " + string(itemq) + " OF " + itemid);
		
				for(var j = 0; j < instance_number(obj_inventory); j++)
				{
					var invobj = instance_find(obj_inventory,j);
					if (invobj.itemid == itemid)
					{
						if(invobj.quantity >= itemq)
						{
							scr_loseQuantity(invobj,itemq);
							k = ds_map_find_next(ingredients,k);
							break;
						}
						else
						{
							itemq -= invobj.quantity;
							scr_loseQuantity(invobj,invobj.quantity);
						}
					}
				}
			}
			scr_setState(other.id, ActorState.Rebuild);
		}
	}
}

with(obj_interactable)
{
	if (other.highlightedobject == id && (interactionPrefixLeft == "Open" || interactionPrefixLeft == "Close") && global.otheruserid == 0)
	{
		event_perform(ev_other,ev_user4);
	}
}