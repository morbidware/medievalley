/// @description Mobile movement
if (global.ismobile && lastmobileclickstatus > 0)
{
	//onClick = device_mouse_check_button(0,mb_left);
	/*if ((global.mouseOverCrafting || global.mouseOverDock || global.mouseOverInventory || global.mouseOverUI || state != ActorState.Idle || global.lockHUD)){
		onClick = false;
	}*/
	//svar multi = 960 / camera_get_view_width(view_camera[0]);
	/*if(onClick && joystick == noone)
	{
		if(device_mouse_x_to_gui(0)< window_get_width()/2)
		{
			joystick = instance_create_depth(0,0,0,obj_joystick);
		}
		
	}*/
	/*else if(!onClick && joystick != noone)
	{
		//instance_destroy(joystick);
		//joystick = noone;	
		//move = 0
	}*/
	if (global.mouseOverCrafting || global.mouseOverDock || global.mouseOverInventory || global.mouseOverUI || state != ActorState.Idle || global.lockHUD)
	{
		if(move == 0){
			left = 0;
			right = 0;
			up = 0;
			down = 0;
		}
	}
	else
	{	
		/*
		if(move == 1){
			var angle = degtorad(joystickDirection);
			left = cos(angle);
			right = 0;
			up = -sin(angle);
			down = 0;
		}
		if(move == 0){
			left = 0;
			right = 0;
			up = 0;
			down = 0;
		}*/
		
		/*
		movetarget_x = mouse_x;
		movetarget_y = mouse_y;
		autotarget = noone;
		autotarget_x = -1;
		autotarget_y = -1;
		autotargetdistance = interactiondistance;
		
		if (!global.tooleditmode && lastmobileclickstatus == 1)
		{
			with (obj_interactable)
			{
				if (collision_point(other.movetarget_x,other.movetarget_y,id,true,false))
				{
					other.autotargetdistance = point_distance(other.movetarget_x,other.movetarget_y,x,y);
					other.autotarget = id;
				}
			}
			with (obj_enemy)
			{
				if (collision_point(other.movetarget_x,other.movetarget_y,id,true,false))
				{
					other.autotargetdistance = point_distance(other.movetarget_x,other.movetarget_y,x,y);
					other.autotarget = id;
				}
			}
		}
		
		// Only perform actions when the user clicks (not when keeps pressing)
		if (lastmobileclickstatus == 1)
		{
			// This happens when action target is an actual object
			if (autotarget != noone)
			{
				movetarget_x = autotarget.x;
				movetarget_y = autotarget.y;
				var angle = degtorad(point_direction(movetarget_x,movetarget_y,x,y));
				if (autotarget.isastructure)
				{
					movetarget_x += cos(angle) * clamp(autotarget.collisionradius,30,999);
					movetarget_y -= sin(angle) * clamp(autotarget.collisionradius,30,999);
				}
				else
				{
					movetarget_x += cos(angle) * clamp(autotarget.collisionradius,12,999);
					movetarget_y -= sin(angle) * clamp(autotarget.collisionradius,12,999);
				}
			}
			// This instead happens if the action target is a generic tile (like when using a hoe or shovel)
			else if (global.tooleditmode)
			{
				autotarget_x = round((movetarget_x + 8) / 16) * 16 - 8;
				autotarget_y = round((movetarget_y + 8) / 16) * 16 - 8;
				movetarget_x = autotarget_x;
				movetarget_y = autotarget_y;
			
				// targets on ground require more precision so we try to position the character better
				var angle = point_direction(movetarget_x,movetarget_y,x,y);
				if (angle >= 315 || angle < 45)
				{
					movetarget_x += 16;
				}
				else if (angle >= 45 && angle < 135)
				{
					movetarget_y -= 16;
				}
				else if (angle >= 135 && angle < 225)
				{
					movetarget_x -= 16;
				}
				else if (angle >= 255 && angle < 315)
				{
					movetarget_y += 16;
				}
			}
		}
		
		if (!instance_exists(obj_nav_target))
		{
			var tgt = instance_create_depth(movetarget_x, movetarget_y, 0, obj_nav_target);
			if (autotarget != noone || global.tooleditmode == true)
			{
				tgt.color = c_red;
			}
		}
		else with (obj_nav_target)
		{
			x = other.movetarget_x;
			y = other.movetarget_y;
		}
		*/
	}
	
	lastmobileclickstatus = 0;
}