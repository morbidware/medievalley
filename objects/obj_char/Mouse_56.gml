/// @description Interaction Left release
// hand actions and UI actions

global.lastMouseButton = "left";
targetobject = highlightedobject;
//clicking = false;

// determine click or drag
var isclick = false;
if (clickdelta < deltathreshold)
{
	isclick = true;
}
if (global.ismobile)
{
	if (isclick)
	{
		lastmobileclickstatus = 1;
	}
	else
	{
		lastmobileclickstatus = 2;
	}
	
	// EXIT SLEEP (mobile) - setting crono to 0 will trigger the exit condition of char_perform_rest
	/*if (state == ActorState.Rest && targetobject.isbusy)
	{
		crono = 0;
	}*/
}

// -------------------------- INVENTORY --------------------------

// handle interaction with inventory
if (global.mouseOverInventory && tempinventoryhit != noone)
{
	// in case of simple click on lower inventory bar only, select the highlighted item
	if (isclick && tempslot < global.inv_inventorylimit)
	{
		var inv = instance_find(obj_playerInventory,0);
		// When object, clicking on same object will deselect it
		if (global.ismobile)
		{
			if (inv.currentSelectedItemIndex == tempinventoryhit.slot)
			{
				inv.currentSelectedItemIndex = -1;
			}
			else
			{
				inv.currentSelectedItemIndex = tempinventoryhit.slot;
			}
		}
		else
		{
			inv.currentSelectedItemIndex = tempinventoryhit.slot;
		}
		with (obj_playerInventory)
		{
			event_perform(ev_other,ev_user0);
		}
	}
	// in case mouse has been dragged...
	else if (!isclick && cursorobject != noone)
	{
		// get the slot and item where the mouse ended movement
		
		var endSlot = scr_getInventorySlot();
		var endItem = scr_getItemFromInventory(endSlot);
		show_debug_message("LAST SLOT "+string(endSlot));
		
		// if there is nothing under the end slot, put item there
		if (endItem == noone)
		{
			scr_putItemIntoInventory(cursorobject, InventoryType.All, endSlot);
			cursorobject = noone;
		}
		// if slot was already occupied...
		else 
		{
			// if possible, merge the two items
			if (endItem.itemid == cursorobject.itemid)
			{
				// full merge if the quantity is within maximum
				if (endItem.quantity + cursorobject.quantity <= endItem.maxquantity)
				{
					var obj = scr_removeItemFromInventory(endSlot);
					obj.quantity += cursorobject.quantity;
					scr_putItemIntoInventory(obj, InventoryType.All, endSlot);
					instance_destroy(cursorobject);
					cursorobject = noone;
				}
				// merge partially if there is still space
				else if (endItem.quantity < endItem.maxquantity)
				{
					var obj = scr_removeItemFromInventory(endSlot);
					var leftquantity = obj.maxquantity - obj.quantity
					obj.quantity = obj.maxquantity;
					scr_putItemIntoInventory(obj, InventoryType.All, endSlot);
					
					cursorobject.quantity -= leftquantity;
					scr_putItemIntoInventory(cursorobject, InventoryType.All, tempslot);
					cursorobject = noone;
				}
				// else just switch item positions
				else
				{
					var obj = scr_removeItemFromInventory(endSlot);
					scr_putItemIntoInventory(obj, InventoryType.All, tempslot);
			
					scr_putItemIntoInventory(cursorobject, InventoryType.All, endSlot);
					cursorobject = noone;
				}
			}
			// else just switch item positions (duplicated, necessary)
			else
			{
				var obj = scr_removeItemFromInventory(endSlot);
				with(obj_wearable)
				{
					if(endItem.itemid == wear.itemid && other.cursorobject.wearable == endItem.wearable)
					{
						instance_destroy(id);	
					}
						
				}
				scr_putItemIntoInventory(obj, InventoryType.All, tempslot);
			
				scr_putItemIntoInventory(cursorobject, InventoryType.All, endSlot);
				cursorobject = noone;
			}
			
		}
	}
}

// -------------------------- STRUCTURES AND DROPPING --------------------------

// handle interaction not on inventory
else if (cursorobject != noone && tempinventoryhit != noone)
{	
	// if the cursorobject is a structure, place it
	if(tempinventoryhit.isastructure && tempinventoryhit.canBePlaced)
	{ 
		placeStructure(tempinventoryhit.x, tempinventoryhit.y, scr_asset_get_index("obj_interactable_"+tempinventoryhit.itemid), tempinventoryhit.itemid);
		structure = scr_gameItemCreate(tempinventoryhit.x, tempinventoryhit.y, scr_asset_get_index("obj_interactable_"+tempinventoryhit.itemid), tempinventoryhit.itemid);
		if(tempinventoryhit.itemid == "chest")
		{
			var inv = instance_find(obj_playerInventory,0);
			structure.startingslot = inv.slots;

			inv.slots += 9;
			ds_grid_resize(inv.inventory,ds_grid_width(inv.inventory)+9,ds_grid_height(inv.inventory));

			var i = structure.startingslot;
			repeat(9)
			{
				ds_grid_set(inv.inventory,i,0,-1);
				i++;
			}
			global.totslots = inv.slots;
		}
		if(global.isSurvival && global.online)
		{
			scr_storeInteractablesSurvival();
		}
		else
		{
			scr_storeAllInsocket();
		}
					
		// smoke effect
		repeat(10)
		{
			instance_create_depth(structure.x + random_range(-16,16), structure.y + random_range(-4,4),-(structure.y+1),obj_fx_cloudlet);
		}
		instance_destroy(tempinventoryhit);
		cursorobject = noone;
		global.saveitemstimer = 0;
	}
	
	// otherwise just drop the item on the ground
	else
	{
		if(global.otheruserid == 0)
		{
			var inst = scr_gameItemCreate(x,y+8,obj_pickable,cursorobject.itemid,cursorobject.quantity);
			inst.durability = cursorobject.durability;
			inst.dropped = true;
			if (global.online)
			{
				dropPickable(x,y+16,cursorobject.itemid,cursorobject.quantity);
			}
			instance_destroy(cursorobject);
			cursorobject = noone;
			if(global.isSurvival && global.online)
			{
				scr_storePickablesSurvival();	
			}
			else
			{
				scr_storeAllInsocket();
			}
			global.saveitemstimer = 0;
		}
		else
		{
			scr_putItemIntoInventory(cursorobject,InventoryType.All,tempinventoryhit.slot);
			cursorobject = noone;
		}
	}
}

// If there's still a cursorobject, put it back into inventory
if (cursorobject != noone)
{
	scr_putItemIntoInventory(cursorobject,InventoryType.All,tempinventoryhit.slot);
	cursorobject = noone;
}

// always reset mouse click
clickdelta = 0;
cursorobject = noone;
tempslot = -1;
tempinventoryhit = noone;
pickslot = -1;
pickslotx = -1;
picksloty = -1;