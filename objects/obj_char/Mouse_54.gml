/// @description Interaction Right press
// mostly actions with working tools

// stop interaction during fade
if (instance_exists(obj_fadein) || instance_exists(obj_fadeout))
{
	exit;
}
if(room == roomChallenge)
{
	var gameLevel = instance_find(obj_gameLevelChallenge,0);
	if(gameLevel.challengeState != 1)
	{
		exit;
	}	
}

//disable input when doing other actions
if(state != ActorState.Idle && crono <= 0)
{
	exit;
}


if (crono > 0 || global.mouseOverUI || global.lockHUD || global.mouseOverCrafting || global.mouseOverDock)
{
	exit;
}
if(global.ismobile && !checkActionButton)
{
	exit;
}
global.lastMouseButton = "right";
interactionCol = highlightCol;
interactionRow = highlightRow;
targetobject = highlightedobject;

//EAT (the only right-click action from inside the inventory)
if(consumableobject != noone && consumableobject != undefined)
{
	switch(consumabletype)
	{
		case ConsumableType.Cook:
			scr_setState(id, ActorState.Cook);
		break;
		
		case ConsumableType.Eat:
			scr_setState(id, ActorState.Eat);
		break;
		
		case ConsumableType.Learn:
			with(consumableobject)
			{
				event_perform(ev_other,ev_user0);
			}
			scr_loseQuantity(consumableobject,1);
			consumableobject = noone;
			consumabletype = -1;
		break;
		
		case ConsumableType.Drink:
			scr_setState(id, ActorState.Eat);
		break;
	}
	exit;
}
// HOE
if(handState == HandState.Hoe && global.otheruserid == 0)
{
	if(room == roomChallenge)
	{
		if((groundtype == GroundType.Soil||groundtype == GroundType.Grass||groundtype == GroundType.TallGrass) && itemontile == -1 && accessible)
		{
			scr_setState(id, ActorState.Hoe);
			var s = audio_play_sound(snd_woosh,0,false);
			audio_sound_pitch(s, 0.9 + random (0.2));
		}
		else
		{
			global.missedInteractions ++;//HOE
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.missedInteractionsValue;
			scr_setState(id, ActorState.Charge);
		}
	}
	else
	{
		if(groundtype == GroundType.Soil && itemontile == -1 && accessible)
		{
			scr_setState(id, ActorState.Hoe);
			var s = audio_play_sound(snd_woosh,0,false);
			audio_sound_pitch(s, 0.9 + random (0.2));
		}
		else
		{
			scr_setState(id, ActorState.Charge);
		}
	}
	
}
// AXE
if(handState == HandState.Axe)
{
	if (targetobject == noone || targetobject <= 0)
	{
		scr_setState(id, ActorState.Charge);
		if(global.isChallenge)
		{
			global.missedInteractions ++;//AXE
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.missedInteractionsValue;	
		}
	}
	else
	{
		scr_setState(id, ActorState.Chop);
		var s = audio_play_sound(snd_woosh,0,false);
		audio_sound_pitch(s, 0.9 + random (0.2));
	}
}
// SOW
else if(handState == HandState.Sow && global.otheruserid == 0)
{
	scr_setState(id, ActorState.Sow);
}
// WATERING	
else if(handState == HandState.Watering)
{
	var s = audio_play_sound(snd_watering,0,false);
	audio_sound_pitch(s, 0.85 + random (0.35));
	scr_setState(id, ActorState.Watering);
}
// SHOVEL
else if(handState == HandState.Shovel && global.otheruserid == 0)
{
	if(room != roomFarm)
	{
		scr_setState(id, ActorState.Charge);
	}
	else
	{
		scr_setState(id, ActorState.Shovel);
	}
}
// PICKAXE
else if(handState == HandState.Mine)
{
	if (targetobject == noone || targetobject <= 0)
	{
		scr_setState(id, ActorState.Charge);
		if(global.isChallenge)
		{
			global.missedInteractions ++;//MINE
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.missedInteractionsValue;	
		}
	}
	else
	{
		scr_setState(id, ActorState.Mine);
		var s = audio_play_sound(snd_woosh,0,false);
		audio_sound_pitch(s, 0.9 + random (0.2));
	}
}
// SICKLE
else if (handState == HandState.Sickle)
{
	if (targetobject == noone || targetobject <= 0)
	{
		scr_setState(id, ActorState.Charge);
		if(global.isChallenge)
		{
			global.missedInteractions ++;//SICKLE
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.missedInteractionsValue;	
		}
	}
	else
	{
		scr_setState(id, ActorState.Sickle);
		var s = audio_play_sound(snd_woosh,0,false);
		audio_sound_pitch(s, 0.9 + random (0.2));
	}
}
// MELEE
else if (handState == HandState.Melee && global.otheruserid == 0)
{
	scr_setState(id, ActorState.Charge);
}
// BUILD
else if (handState == HandState.Build && global.otheruserid == 0)
{
	if (!scr_checkIfPlayerIsLocked(true))
	{
		if(global.isSurvival)
		{
			scr_setState(id, ActorState.Build);
		}
		else if(!global.isSurvival)
		{
			scr_setState(id, ActorState.Build);
		}	
	}

}
// BREAK
else if (handState == HandState.Hammer && global.otheruserid == 0)
{
	if (targetobject == noone || targetobject <= 0)
	{
		scr_setState(id, ActorState.Charge);
	}
	else if (targetobject.isastructure)
	{
		scr_setState(id, ActorState.Hammer);
	}
}
//FISH
else if (handState == HandState.Fish && global.otheruserid == 0)
{
	var inst = instance_nearest(x,y,obj_equipped);
	if((groundtype == GroundType.Water && inst.twoStepAction == 1)||inst.twoStepAction == 0)
	{
		scr_setState(id, ActorState.Fish);
		var s = audio_play_sound(snd_woosh,0,false);
		audio_sound_pitch(s, 0.9 + random (0.2));
		
		if(inst.twoStepAction == 1) inst.twoStepAction = 0;
		else inst.twoStepAction = 1;
	}
}