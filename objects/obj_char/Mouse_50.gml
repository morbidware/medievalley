///@description Click/drag detection

// stop interaction during fade
if (instance_exists(obj_fadein) || instance_exists(obj_fadeout))
{
	exit;
}

// accumulate mouse dragging
if (clickdelta < deltathreshold)
{
	clickdelta += abs(global.mousedeltax)+abs(global.mousedeltay);
}
// if not online, when mouse is dragged above the threshold, grab the underlying item
else if (global.otheruserid == 0 && cursorobject == noone && tempinventoryhit != noone && state == ActorState.Idle)
{
	var obj = scr_removeItemFromInventory(tempslot);
	cursorobject = obj;
}

if (global.ismobile && !global.lockHUD && clickdelta > deltathreshold)
{
	lastmobileclickstatus = 2;
}