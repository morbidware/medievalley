///@description Aim, avoidance, object highlight
//logga("Start Char");
scr_setDepth(DepthType.Elements);
depth -= 1;
lasttimestamp = round(get_timer() / 1000000);

// mouse movement
if (global.ismobile && lastmobileclickstatus > 0)
{
	event_perform(ev_other,ev_user13);
	
}
if(global.ismobile){
	if (global.mouseOverInventory || global.mouseOverUI || global.mouseOverCrafting || global.mouseOverDock || global.lockHUD/* || global.ismobile*/)
	{
		exit;
	}
}
if(global.isSurvival && state != ActorState.Die){
	if(stomach > 0)
	{
		stomach -= 0.0025 * global.dt;
		stomach = clamp(stomach,0,maxstomach);
	}
	else if(stomach == 0)
	{
		life -= 0.0005 * global.dt;
		life = clamp(life,0,maxstomach);
		if(life == 0)
		{
			scr_setState(id,ActorState.Die);
		}
	}
}
// read storey from grid (clamp needed otherwise it will break when walking outside play area)
storey = ds_grid_get(fg.storeygrid, clamp(floor(x/16),0,fg.cols-1), clamp(floor(y/16),0,fg.rows-1));

anim_index += anim_speed * global.dt;
var c = sprite_get_number(custom_body_sprite);
if (anim_index > c)
{
	anim_index -= c;
}
if (anim_index < 0)
{
	anim_index += c;
}

if(instance_exists(obj_console)){exit;}


/*
// DEBUG STUFF - print the whole inventory
var inv = instance_find(obj_playerInventory,0);
if (inv != noone)
{
	var str = "";
	for (var i = 0; i < ds_grid_width(inv.inventory); i++)
	{
		str += string(ds_grid_get(inv.inventory,i,0))+", ";
	}
	show_debug_message(str);
}
*/

if (emoteCrono > 0)
{
	emoteCrono -= global.dt;
}

// set aim position
var ang = 0;
switch (lastdirection)
{
	case Direction.Up: ang = 0; break;
	case Direction.UpRight: ang = 45; break;
	case Direction.Right: ang = 90; break;
	case Direction.DownRight: ang = 135; break;
	case Direction.Down: ang = 180; break;
	case Direction.DownLeft: ang = 225; break;
	case Direction.Left: ang = 270; break;
	case Direction.UpLeft: ang = 315; break;
}
aim_x = round(x + sin(degtorad(ang)) * (interactiondistance * 0.9));
aim_y = round(y - cos(degtorad(ang)) * (interactiondistance * 0.9));
//main state function
scr_updateState(id);

if(stamina <= 0)
{
	sweatCrono--;
	if(sweatCrono <= 0)
	{
		sweatCrono = 30;
		var sw = instance_create_depth(x+random_range(-4,4),y-28,depth-2,obj_fx_sweat);
		sw.pla = id;
		sw.ox = sw.x - x;
		sw.oy = sw.y - y;
	}
}


// update current highlightedobject if Idle
if (state == ActorState.Idle)
{
	highlightedobject = noone;
	nearest = interactiondistance;
	with (obj_interactable)
	{
		highlight = false;
		var d = point_distance(x,y,other.aim_x,other.aim_y);
		if (d < other.nearest)
		{
			other.nearest = d;
			other.highlightedobject = id;
		}
		
	}

	// if player is in intro phase, allow only the mailbox to be highlighted
	if (highlightedobject != noone)
	{
		if (game.firstday && object_get_name(highlightedobject.object_index) != "obj_interactable_mailbox")
		{
			highlightedobject = noone;
		}
	}
	if (highlightedobject != noone)
	{
		highlightedobject.highlight = true;
	}
	if (itemontile > 0)
	{
		highlightedobject = itemontile;
	}
}

//avoid walls and objects with a collision radius
scr_wallCollisions(id,0,0);
with(obj_interactable)
{
	if (collisionradius > 0)
	{
		var pAx = other.x;
		var pAy = other.y;
		var pBx = x;
		var pBy = y;
		var dist = point_distance(pAx, pAy, pBx, pBy);
		if(dist < collisionradius)
		{
			var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
			var off = abs(dist - collisionradius) * 0.6;
			other.x += cos(angle+pi)*off;
			other.y -= sin(angle+pi)*off;
		}
	}
}
with(obj_pickable)
{
	if (collisionradius > 0)
	{
		var pAx = other.x;
		var pAy = other.y;
		var pBx = x;
		var pBy = y;
		var dist = point_distance(pAx, pAy, pBx, pBy);
		if(dist < collisionradius)
		{
			var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
			var off = abs(dist - collisionradius) * 0.6;
			other.x += cos(angle+pi)*off;
			other.y -= sin(angle+pi)*off;
		}
	}
}

//keep in room when in Fight Challenge
if(room == roomChallenge && global.challengeType == ChallengeType.Fighting)
{
	x = clamp(x,0,480);
	y = clamp(y,0,352);
}


// send char data only when walking or doing an action, every n frames
senddatatimer -= global.dt;

if (senddatatimer <= 0)
{
	senddatatimer += datarefreshrate;
	if(state != ActorState.Idle || walking == true)
	{
		if(state != ActorState.Idle)
		{
			senddatatimer = 1;
		}
		var ii = image_index*10;
		ii = round(ii);
		ii = ii/10.0;
		var sx = image_xscale*10;
		sx = round(sx);
		sx = sx/10.0;
		var sy = image_yscale*10;
		sy = round(sy);
		sy = sy/10.0;
		if(room != roomWild && room != roomHouse && global.online)
		{
			sendCharData(round(x),round(y),custom_body_sprite, custom_head_sprite, custom_lower_sprite, custom_upper_sprite,anim_index,sx,sy,socketid,global.username,stamina);
		}
	}
}

// manage walking sound
if (walking)
{
	steptimer -= global.dt;
	if (steptimer <= 0)
	{
		event_perform(ev_other,ev_user14);
	}
}
else
{
	steptimer = stepinterval/2;
}

// create "spots" when the character walks (literally like breadcumbs from Tom Thumb), deleting older ones
if (walking)
{
	newspottimer -= global.dt;
}
if (newspottimer <= 0)
{
	newspottimer = 20;
	while (ds_list_size(pathspots_x) >= 6)
	{
		ds_list_delete(pathspots_x,0);
		ds_list_delete(pathspots_y,0);
	}
	ds_list_add(pathspots_x,round(x));
	ds_list_add(pathspots_y,round(y));
}

// Save status values
if (state != ActorState.Die)
{
	global.stamina = stamina;
	global.life = life;
	
	if(global.gold != gold)
	{
		global.gold = gold;
		//scr_updateRecipes();
	}
	
	global.galasilver = galasilver;
}

/*var c = floor((x-8)/16);
var r = floor((y-8)/16);
var ground = instance_find(obj_baseGround,0);
if((c != col || r != row) && ground != noone)
{
	
	if(!cellstate)
	{
		cellstate = true;
		mp_grid_add_cell(ground.pathgrid,col,row);
	}
	col = c;
	row = r;
	var v = mp_grid_get_cell(ground.pathgrid,col,row);
	//show_debug_message(string(v) + " " + string(col) + " " + string(row) + " " + string(ground.rows) + " " + string(ground.cols));
	if(v == -1)
	{
		cellstate = mp_grid_clear_cell(ground.pathgrid,col,row);	
	}
}*/
//logga("End Char");