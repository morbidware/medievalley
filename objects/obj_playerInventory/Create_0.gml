logga("obj_playerInventory CREATE");

gamelevel = instance_find(obj_gameLevel,0);
slots = global.totslots;
logga("starting with totslots "+string(slots));
depth = -500;

var obj = noone;

currentSelectedItemIndex = 0;
if (global.ismobile)
{
	currentSelectedItemIndex = 0;
}
/*
btnZoom = instance_create_depth(0,0,depth,obj_btn_generic_persistent);
btnZoom.caller = id;
btnZoom.evt = ev_user1;
btnZoom.sprite_index = spr_btn_zoom;
*/

pla = instance_find(obj_char,0);
inventory = ds_grid_create(slots,1);
global.tooleditmode = false;

for(var i = 0; i < slots; i++)
{
	ds_grid_set(inventory,i,0,-1);
}

startx = display_get_gui_width()/2 - (10/2)*38 + 18;
starty = display_get_gui_height() - 36;

alarm_set(0,2);

if(room == roomChallenge)
{
	// Do not add any item!
}
else
{
	if(string_length(global.items) > 0)
	{	
		logga("Reading saved items");
		var items = ds_list_create();
		ds_list_read(items, global.items); 
		logga("items list length " + string(ds_list_size(items)));
		for(var i = 0; i < ds_list_size(items); i++)
		{
			logga("i"+string(i));
			var str = ds_list_find_value(items,i);
			logga("str"+string(str));
			var iid = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,iid+":","");
			var qu = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,qu+":","");
			var slo = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,slo+":","");
			var dur = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,dur+":","");
			logga("itemid"+string(iid));
			var invobj = scr_gameItemCreate(-100,-100,obj_inventory,iid,real(qu));
			invobj.durability = real(dur);
			invobj.slot = real(slo);
			ds_grid_set(inventory,real(slo),0,invobj);
		}
		scr_updateRecipes();
	}
	else
	{
		logga("Spawning new inventory");
		if(m_extraObjects || true)
		{
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"basefencedoor",99);
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"basefence",99);
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"grasssuit",1);	
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"grasshat",1);	
			/*	
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"tent");		
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"bonfire");		
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"groundtrap");
				
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"axe");
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"pickaxe");
			scr_gameItemCreate(pla.x,pla.y, obj_pickable,"food_cookedmeat",99);*/
			// ============================================== INVENTORY
			/*
			obj = scr_gameItemCreate(0,0, obj_pickable,"sword");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0, obj_pickable,"axe");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);

			obj = scr_gameItemCreate(0,0, obj_pickable,"pickaxe");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0, obj_pickable,"shovel");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0, obj_pickable,"hoe");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0, obj_pickable,"wateringcan");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0, obj_pickable,"sickle");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0, obj_pickable,"hammer");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0, obj_pickable,"chest");
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0, obj_pickable,"woodlog",20);
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			instance_destroy(obj);
			*/
			// ============================================== BACKPACK
			/*
			obj = scr_gameItemCreate(0,0, obj_pickable,"stone",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0, obj_pickable,"woodlog",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0, obj_pickable,"sapling",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0, obj_pickable,"ironnugget",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"tent",1);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"house",1);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0,obj_pickable,"pallet",1);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"potion_life",10);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"potion_speed",10);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"potion_stamina",10);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"potion_defence",10);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"potion_strength",10);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
		
			obj = scr_gameItemCreate(0,0,obj_pickable,"strawberryseeds",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0,obj_pickable,"carrotseeds",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0,obj_pickable,"redbeansseeds",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
			
			obj = scr_gameItemCreate(0,0,obj_pickable,"wheatseeds",99);
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			instance_destroy(obj);
			*/
			scr_storeItems();
		}
	}
}

