{
    "id": "78af66b8-9494-4554-b7be-c5bdf8d58fd5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_playerInventory",
    "eventList": [
        {
            "id": "c48efd7d-eb0f-4ade-a288-10fc0bf109f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "e1dcc506-a8b6-494d-9c39-13b7416c7fb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "c09cdf08-3f36-46d9-8325-0bb2e819b161",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "0f4caf9a-14bd-423c-921b-5fdce31d3593",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 61,
            "eventtype": 6,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "5c98ea14-2718-4f7b-bcf8-09071656fbb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "c7d2b42b-b9f4-4772-a98c-455cbcedd436",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "b7457c44-cabd-40ca-98b2-437c09932fea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "78577d11-f6aa-4948-b9e9-f2726ffb6799",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        },
        {
            "id": "ca5a7646-7cbc-4900-aefc-8900edd34ae0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "78af66b8-9494-4554-b7be-c5bdf8d58fd5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}