/// @description Quick select, mouseover
if (!instance_exists(pla))
{
	pla = instance_find(obj_char,0);
}

// keep dock hidden as long as the intro is active
with(obj_gameLevelFarm)
{
	if (onIntro)
	{
		other.starty = display_get_gui_height() + 28;
	}
}
if (ds_map_find_value(global.unlockables, "ui_inventory") < 1|| room == roomChallenge)
{
	starty = display_get_gui_height() + 48;
}

starty -= global.dt;
starty = clamp(starty,display_get_gui_height() - 36, 99999);

//zoo
/*
if (global.ismobile)
{
    btnZoom.ox = startx - 48;
    btnZoom.oy = starty + 2;
	btnZoom.disabled = false;
}
else
{
    btnZoom.oy = starty + 1000;
    btnZoom.disabled = true;
}
*/

// Align objects to grid
for (var i = 0; i < 10; i++)
{
	var obj = ds_grid_get(inventory,i,0);
	if (obj > -1)
	{
		if (instance_exists(obj))
		{
			if(obj.state == 0)
			{
				obj.x = startx + 2 + 38 * obj.slot;
				obj.y = starty + 2;
			}
		}
	}
}

// quick select
if(!global.lockHUD && pla.state == ActorState.Idle && pla.crono <= 0)
{
	if (keyboard_check_pressed(ord("1")))
	{
		currentSelectedItemIndex = 0;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("2")))
	{
		currentSelectedItemIndex = 1;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("3")))
	{
		currentSelectedItemIndex = 2;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("4")))
	{
		currentSelectedItemIndex = 3;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("5")))
	{
		currentSelectedItemIndex = 4;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("6")))
	{
		currentSelectedItemIndex = 5;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("7")))
	{
		currentSelectedItemIndex = 6;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("8")))
	{
		currentSelectedItemIndex = 7;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("9")))
	{
		currentSelectedItemIndex = 8;
		event_perform(ev_other,ev_user0);
	}
	else if (keyboard_check_pressed(ord("0")))
	{
		currentSelectedItemIndex = 9;
		event_perform(ev_other,ev_user0);
	}
}