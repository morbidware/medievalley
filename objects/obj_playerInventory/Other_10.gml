/// @description Update equipment
if (!instance_exists(pla))
{
	exit;
}
if(pla.state == ActorState.Idle)
{
	pla.targetobject = noone;
	pla.highlightedobject = noone;
}

pla.consumableobject = noone;
pla.consumabletype = -1;
global.tooleditmode = false;
with(obj_equipped)
{
	instance_destroy();
}
if (currentSelectedItemIndex < 0)
{
	pla.handState = HandState.Empty;
}
else
{
	var selection = ds_grid_get(inventory, currentSelectedItemIndex, 0);
	if (selection <= 0)
	{
		exit;
	}
	if(selection.itemid == "axe")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"axe");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		
		pla.handState = HandState.Axe;
	}
	else if(selection.itemid == "hoe")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"hoe");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Hoe;
		global.tooleditmode = true;
	}
	else if( string_pos("seed",selection.itemid) > 0 )
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,selection.itemid);
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Sow;
		global.tooleditmode = true;
	}
	else if(string_pos("basefence",selection.itemid) > 0 )
	{

		var inst = scr_gameItemCreate(0,0,obj_equipped,selection.itemid);

		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Build;
		global.tooleditmode = true;
	}
	else if(selection.itemid == "wateringcan")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"wateringcan");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Watering;
	}
	else if(selection.itemid == "shovel")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"shovel");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Shovel;
		global.tooleditmode = true;
	}
	else if(selection.itemid == "sword")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"sword");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false; 
		pla.handState = HandState.Melee;
	}
	else if(selection.itemid == "spear")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"spear");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false; 
		pla.handState = HandState.Melee;
	}
	else if(selection.itemid == "sickle")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"sickle");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Sickle;
	}
	else if(selection.itemid == "pickaxe")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"pickaxe");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Mine;
	}
	else if(selection.itemid == "hammer")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"hammer");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.visible = false;
		pla.handState = HandState.Hammer;
	}
	else if(selection.itemid == "fishingrod")
	{
		var inst = scr_gameItemCreate(0,0,obj_equipped,"fishingrod");
		inst.durability = selection.durability;
		inst.myslot = currentSelectedItemIndex;
		inst.twoStepAction = 1;
		inst.visible = false;
		pla.handState = HandState.Fish;
	}
	else {
		pla.handState = HandState.Empty;
	}
}
