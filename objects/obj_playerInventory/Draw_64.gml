if(global.isSurvival)
{
	draw_sprite_ext(spr_inventory, 1, startx-18-8, starty-18-8,2,2,0,c_white,1);
}
else
{
	draw_sprite_ext(spr_inventory, 0, startx-18-8, starty-18-8,2,2,0,c_white,1);	
}

if (currentSelectedItemIndex >= 0)
{
	if(global.isSurvival)
	{
		draw_sprite(spr_inventory_selection_survival, 0, 2 + startx + 38 * currentSelectedItemIndex, 2 + starty);
	}
	else
	{
		draw_sprite(spr_inventory_selection, 0, 2 + startx + 38 * currentSelectedItemIndex, 2 + starty);
	}
	
}