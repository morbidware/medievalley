/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(crono <= (5*(ticks-1))*60)
{
	ticks--;
	show_debug_message("get a life " + string(ticks));
	with(obj_char)
	{
		life += 3;
		if(life > 100)
		{
			life = 100;
		}
		global.saveuserdatatimer = 0;
	}
}
