{
    "id": "51f5c4bb-894e-45bb-8bdb-112e4ec64154",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_status_life",
    "eventList": [
        {
            "id": "25f1af83-d26d-4600-bdc2-96551f421fff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51f5c4bb-894e-45bb-8bdb-112e4ec64154"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a52de1de-4ab3-40a4-a337-03792d097632",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed622e12-63f5-4d5f-a282-682d6c2018d7",
    "visible": true
}