socketid = "";
name = "";
image_speed = 0;

tgt_x = 0;
tgt_y = 0;
eq_x = 0;
eq_y = 0;
eq_image_angle = 0;
eq_sprite_index = -1;
eq_timer = 0;
eq_image_xscale = 0;
eq_image_yscale = 0;
eq_maxtimer = 15; // Frames that the tool will still be visible even if not receiving tool updates
eq_image_index = 0;
spd = 1.5;
warpthreshold = 64;
enablelerp = true;
storey = 0;
ground = instance_find(obj_baseGround,0);
shadowType = ShadowType.Sprite;
shadowBlobScale = 1;

custom_body_sprite = -1;
custom_head_sprite = -1;
custom_lower_sprite = -1;
custom_upper_sprite = -1;

sprite_image_index = 0;
sprite_image_xscale = 0;
sprite_image_yscale = 0;

sprite_index_timer = 2;
sprite_index_crono = sprite_index_timer;

with(obj_char)
{
	var ii = image_index*10;
	ii = round(ii);
	ii = ii/10.0;
	var sx = image_xscale*10;
	sx = round(sx);
	sx = sx/10.0;
	var sy = image_yscale*10;
	sy = round(sy);
	sy = sy/10.0;
	if (global.online)
	{
		sendCharData(round(x),round(y),custom_body_sprite, custom_head_sprite, custom_lower_sprite, custom_upper_sprite,anim_index,sx,sy,socketid,global.username,stamina);
	}
}
resting = false;

offsetx = 0;
offsety = 0;

stamina = 0;
sweatCrono = 0;
utilityCrono = 0;
/*
var tex = sprite_get_uvs(sprite_index, image_index);
tex_left = tex[0];
tex_top = tex[1];
tex_right = tex[2];
tex_bottom = tex[3];
*/
