{
    "id": "ba97c21a-f2f2-48d8-ae2f-91f97aee85c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dummy",
    "eventList": [
        {
            "id": "561c2190-9805-40bd-9056-def0ef0eef6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba97c21a-f2f2-48d8-ae2f-91f97aee85c9"
        },
        {
            "id": "b40d07e4-ff1b-417f-b396-6b9084e053ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ba97c21a-f2f2-48d8-ae2f-91f97aee85c9"
        },
        {
            "id": "8552079e-5078-4f0a-8e49-aceaa1ace860",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ba97c21a-f2f2-48d8-ae2f-91f97aee85c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ea533252-0ed6-4cc2-9d4a-57d3dc92a987",
    "visible": true
}