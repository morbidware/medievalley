/// @description Clear cells
var ground = instance_find(obj_baseGround,0);

if (gridded == 1)
{
	ds_grid_set(ground.itemsgrid, floor(x/16), floor(y/16), -1);
}
mp_grid_clear_cell(ground.pathgrid, floor(x/16), floor(y/16));

if (ds_list_size(structurewalls) > 0)
{
	for (var i = 0; i < ds_list_size(structurewalls); i++)
	{
		instance_destroy(ds_list_find_value(structurewalls, i));
	}	
}
event_inherited();