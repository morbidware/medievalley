{
    "id": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable",
    "eventList": [
        {
            "id": "4e66a5ee-72a8-4976-b93a-d08f63dff957",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "f806a0d8-828d-4310-a99e-7a6bd34b00e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "6eb0ec64-28f8-452c-b0b7-c82316d7f57f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "be4f2862-7b49-4342-accc-151d76cf2739",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "d12a558c-4ad8-4b18-bb6d-623d7fdc34e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "f5eadf88-0f49-4707-80e3-6f6f0344ff9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "cbd02041-a7a3-426f-ac9b-4b440f3a1648",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "ca6f5f87-67ca-4258-a78f-e0bd3264b24f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "bba0496d-5833-4f18-b496-7ab55a6cdfce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "205334a9-3dee-41bc-9754-53fd08441f1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        },
        {
            "id": "f6833156-dcea-438f-8b5b-346d15fdc261",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "cf8deb36-79b4-4043-940c-ca9d662edda0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}