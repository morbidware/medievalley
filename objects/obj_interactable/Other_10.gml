/// @description Finish interaction
with(obj_char)
{
	scr_putItemIntoInventory(targetobject,InventoryType.Inventory,-1);
}
with (obj_baseGround)
{
	ds_grid_set(itemsgrid,floor(other.x/16),floor(other.y/16),-1);
}
instance_destroy();