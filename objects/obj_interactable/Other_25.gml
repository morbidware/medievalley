/// @description Custom setup
life = lifeperstage * growstage;

// Save on grid
if(gridded == 1)
{
	with(obj_baseGround)
	{
		var col = floor(other.x/16);
		var row = floor(other.y/16);
		ds_grid_set(itemsgrid, col, row, other.id);
	}
}

// prepare ground with walls a tiles for structure
if(isastructure)
{
	var cols = wallsw;
	var rows = wallsh;
	
	var startx = floor(x - ((cols/2)*16) + 8);
	var starty = floor(y - ((rows/2)*16) + 8);	
	
	for (var c = 0; c < cols; c++)
	{
		for (var r = 0; r < rows; r++)
		{
			//position for wall objects
			var posx = startx+(16*c);
			var posy = starty+(16*r);
		
			//coordinates for grass and wall grid
			var col = floor(posx/16);
			var row = floor(posy/16);
		
			if (shadowOnWalls)
			{
				var w = instance_create_depth(posx, posy, 0, obj_wall_shadow_only);
				w.storey_0 = true;
				w.storey_1 = true;
				w.storey_2 = true;
				w.storey_3 = true;
				ds_list_add(structurewalls, w);
			}
			else
			{
				var w = instance_create_depth(posx, posy, 0, obj_wall_invisible);
				w.storey_0 = true;
				w.storey_1 = true;
				w.storey_2 = true;
				w.storey_3 = true;
				ds_list_add(structurewalls, w);
			}
		
			ds_grid_set(instance_find(obj_baseGround,0).wallsgrid, col, row, w);
		}
	}
}