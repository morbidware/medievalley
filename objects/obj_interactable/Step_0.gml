/// @description Shader, grow cycle

// shader stuff
if (hasWaveShader)
{
	if (altshader)
	{
		if(si != sprite_index || ii != image_index)
		{
			si = sprite_index;
			ii = image_index;
			var tex = sprite_get_uvs(sprite_index, image_index);
			tex_left = tex[0];
			tex_top = tex[1];
			tex_right = tex[2];
			tex_bottom = tex[3];
		}
	}
	altshader = !altshader;
}

// scaling back to 1
if(image_yscale > 1.01)
{
	
	var off = (image_yscale-1.0)*0.1;
	image_yscale -= off * global.dt;
	image_xscale += off * global.dt;
	if(off < 0.001)
	{
		image_yscale = 1.0;
		image_xscale = 1.0;
	}
}

// shake effect
if(!global.isSurvival)
{
	if(shakeCrono > 0.01)
	{
		shakeCrono -= global.dt;
		if (shakeCrono <= 0.01)
		{
			shakeCrono = 0;
			image_angle = 0;
		}
		else
		{
			if (random(3) > 1)
			{
				image_angle = random_range(-3,3);
			}
		}
	}
	else
	{
		image_angle = 0;
	}
}

// grow cycle
if(growcrono > 0)
{
	
	growcrono -= global.dt;
	if(m_godMode)
	{
		if(growcrono > 40)
		{
			growcrono = 40;
		}
	}
	if(round(growcrono) == 40)
	{
		shakeCrono = 40;
	}
	else if(growcrono <= 0)
	{
		growcrono = 0;
		growstage++;
		image_xscale = 0.5;
		image_yscale = 1.5;
		life = clamp(lifeperstage * (growstage-1),0,15);
		if(growstage < growstages)
		{
			if(needswatering == 0)
			{
				growcrono = growtimer;
			}
		}
	}
}
growstage = clamp(growstage,1,growstages);

image_index = growstage-1;