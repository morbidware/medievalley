/// @description Check intersetcions
event_inherited();

// Destroy this interactable if it's on an illegal position
if (!ignoregroundlimits && room != roomHouse)
{
	if (ds_grid_get(instance_find(obj_baseGround,0).wallsgrid, floor(x/16), floor(y/16)) > 0 && !isastructure)
	{
		//show_debug_message("---------- WALL! KILLING "+string(object_get_name(object_index)));
		instance_destroy(id);
	}
	if ((room != roomWild && room != roomSurvival) && ds_grid_get(instance_find(obj_baseGround,0).gridaccess, floor(x/16), floor(y/16)) < 1 && !isastructure)
	{
		//show_debug_message("---------- ACCESS! KILLING "+string(object_get_name(object_index)));
		instance_destroy(id);
	}
}