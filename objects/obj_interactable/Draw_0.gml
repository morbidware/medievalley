/*
draw_set_colour(c_white);
draw_set_font(global.Font);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_text(x,y - 24, string(id));
*/

if(!canBeDrawn)
{
	exit;
}

if(needswatering && growcrono > 0)
{
	draw_sprite(spr_watered_stain,0,x,y);
}

if(!hasWaveShader || global.graphicsquality == 0)
{
	if(highlight)
	{
		shader_set(shaderHighlight);
	}

	if (randomoffset)
	{
		draw_sprite_ext(sprite_index, image_index, x+offsetx, y+offsety, image_xscale, image_yscale, image_angle, c_white, image_alpha);
	}
	else
	{
		draw_self();
	}
	
	if(highlight)
	{
		shader_reset();
	}
}
else
{
	shader_set(shaderWave);

	t += (1.5/room_speed)*global.dt;
	
	shader_set_uniform_f(u_top,tex_top);
	shader_set_uniform_f(u_bottom,tex_bottom);
	shader_set_uniform_f(u_left,tex_left);
	shader_set_uniform_f(u_right,tex_right);

	shader_set_uniform_f(u_time,t);
	shader_set_uniform_f(u_intensity,intensity);
	if(highlight)
	{
		shader_set_uniform_f(u_highlight,1.0);
	}
	else
	{
		shader_set_uniform_f(u_highlight,0.0);
	}
	
	shader_set_uniform_f(u_resx,sprite_get_width(sprite_index));
	shader_set_uniform_f(u_resy,sprite_get_height(sprite_index));
	
	if (randomoffset)
	{
		draw_sprite_ext(sprite_index, image_index, x+offsetx, y+offsety, image_xscale, image_yscale, image_angle, c_white, image_alpha);
	}
	else
	{
		draw_self();
	}
	
	shader_reset();
}

/*
var pla = instance_find(obj_char,0);
var d = point_distance(x,y,pla.x,pla.y);
if(d < 64)
{
	draw_set_font(global.Font);
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_text(x,y-8,name);
}
*/