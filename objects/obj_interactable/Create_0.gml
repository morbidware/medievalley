event_inherited();

isNet = false;
canSendNetEvents = true;

highlight = false;
mx = 0;
my = 0;

kind = GameItemKind.Interactable;

interactionPrefixLeft = "";
interactionPrefixRight = "";

image_speed = 0;

rotflers = 0;

shakeCrono = 0;

//WAVE SHADER
hasWaveShader = false;
t = random(room_speed);
intensity = 0.2+random(0.8);
altshader = true;

randomoffset = false;
offsetx = choose(-1,0,1);
offsety = choose(-1,0,1);

tex_left = -1;
tex_top = -1;
tex_right = -1;
tex_bottom = -1;

u_top = shader_get_uniform(shaderWave,"u_top");
u_bottom = shader_get_uniform(shaderWave,"u_bottom");
u_left = shader_get_uniform(shaderWave,"u_left");
u_right = shader_get_uniform(shaderWave,"u_right");
u_time = shader_get_uniform(shaderWave,"u_time");
u_intensity = shader_get_uniform(shaderWave,"u_intensity");
u_highlight = shader_get_uniform(shaderWave,"u_highlight");
u_resx = shader_get_uniform(shaderWave,"u_resx");
u_resy = shader_get_uniform(shaderWave,"u_resy");

si = sprite_index;
ii = image_index;

life = lifeperstage * growstage;

gridded = 0;

healthgain = 0;
staminagain = 0;

wasdisabled = false;
lasttimestamp = 0;
pagehidetimestamp = -1;

canBeDrawn = true;
isbusy = false;
startingslot = -1;

