{
    "id": "d6276304-b035-4fb8-8bcc-2dbe6b37ab2a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_houseGround",
    "eventList": [
        {
            "id": "75ef1817-ae55-4df1-8439-3a44ac08d48f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6276304-b035-4fb8-8bcc-2dbe6b37ab2a"
        },
        {
            "id": "0272011b-8b46-42ad-b308-547ecec60f18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d6276304-b035-4fb8-8bcc-2dbe6b37ab2a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}