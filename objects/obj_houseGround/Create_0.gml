/// @description Insert description here
// You can write your code in this editor
//event_inherited();

cols = global.houseCols;
rows = global.houseRows;

border = 0;
waterstep = 0;

roomcols = global.roomCols;
roomrows = global.roomRows;

basementcols = global.basementCols;
basementrows = global.basementRows;

// Ground grids
grid = ds_grid_create(cols,rows);		// groundType enum
spritegrid = ds_grid_create(cols,rows);	// sprite_index of the single tiles
indexgrid = ds_grid_create(cols,rows);	// image_index of the single tiles
storeygrid = ds_grid_create(cols,rows);	// storey value of the single tiles

// Other grids
gridaccess = ds_grid_create(cols,rows);	// tiles accessibili (1) oppure no (-1)
itemsgrid = ds_grid_create(cols,rows);	// items snappati alla griglia, default -1
wallsgrid = ds_grid_create(cols,rows);	// walls nella griglia, default -1
pathgrid = mp_grid_create(0,0,cols,rows,16,16);	// griglia per pathfinding
limitgrid = ds_grid_create(ceil(cols/2),ceil(rows/2));	// griglia tileset map limits, diviso 2 perché è a 32px

var r = 0;
var c = 0;

exitpointx = -1;
exitpointy = -1;

// prepare base grids
ds_grid_clear(grid, GroundType.Void);
ds_grid_clear(gridaccess, -1);
ds_grid_clear(storeygrid, 0);

// create house floor
var sx = global.houseborder;
var sy = global.houseborder;
ds_grid_set_region(grid, sx, sy, sx+roomcols, sy+roomrows, GroundType.HouseFloor);

// create accessible basement
sx = global.houseborder + roomcols + 2;
sy = global.houseborder;
ds_grid_set_region(grid, sx, sy, sx+basementcols, sy+basementrows, GroundType.HouseFloor);
ds_grid_set_region(gridaccess, sx, sy, sx+basementcols, sy+basementrows, 1);

//create corridor
sx = global.houseborder + roomcols;
sy = global.houseborder + 2;
ds_grid_set_region(grid, sx, sy, sx + 1, sy + 1, GroundType.HouseFloor);

//create exit
sx = global.houseborder + roomcols/2 - 1;
sy = global.houseborder + roomrows;
ds_grid_set_region(grid, sx, sy, sx + 1, sy + 1, GroundType.HouseFloor);

sx = global.houseborder + roomcols + 3;
sy = global.houseborder;
ds_grid_set_region(grid, sx, sy, sx+global.basementCols-2, sy+1, GroundType.HouseBasement);

sx = global.houseborder + roomcols/2 - 1;
sy = global.houseborder + roomrows;

exitpointx = (sx+1)*16;
exitpointy = (sy+2)*16;

//create carpet
sx = global.houseborder+(roomcols/2)+2;
sy = global.houseborder+(roomrows/2)+2;
var carpet = instance_create_depth(sx*16,sy*16,0,obj_statictile);
carpet.sprite_index = spr_house_carpet;

// add walls where there's void
c = 0;
repeat(cols)
{
	r = 0;	
	repeat(rows)
	{
		if (ds_grid_get(grid, c, r) == GroundType.Void)
		{
			var w = instance_create_depth(c*16+8,r*16+8,0,obj_wall_invisible);
			w.storey_0 = true;
			w.storey_1 = true;
			w.storey_2 = true;
			w.storey_3 = true;
			ds_grid_set(wallsgrid,c,r,w);
		}
		r++;
	}
	c++;
}

scr_updateTileSprites();

instance_create_depth(0,0,0,obj_drawGround);