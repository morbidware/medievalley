{
    "id": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_storeDataSurvival",
    "eventList": [
        {
            "id": "635cbac6-80f1-4f3b-8d4e-e7a6fffd4aee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159"
        },
        {
            "id": "56b69b18-c53e-4e47-a0a0-a524c08cb778",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159"
        },
        {
            "id": "058b6a06-a36c-4d6f-b5a4-a4f77d41f5be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 2,
            "m_owner": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159"
        },
        {
            "id": "a8d6550e-2fb5-41fe-bc00-0dd2776de57f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159"
        },
        {
            "id": "59eb12c7-1f65-468c-9678-ecc61cb9da0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159"
        },
        {
            "id": "dbf68b0e-e9df-4d47-9b50-9f54cc9f81e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "878ce4c3-2ba5-4ef7-b9cb-22f37fc26159"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}