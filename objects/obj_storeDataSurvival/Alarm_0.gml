///@description Save 1/4
//show_debug_message("---------- SAVING ALL GAME DATA (1/4)...");
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 0");

if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}
logga("storing ground");
scr_storeGroundSurvival();
scr_storeStoreySurvival();
scr_storeWallsSurvival();
if(global.mobowner == 1)scr_storeMobsSurvival();

global.uisavestatus.saving = true;