///@description Init
// stop immediately if we are visiting another farm
if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}
logga("obj_storeDataSurvival CREATE");
logga("obj_storeDataSurvival started saving");
// enable all items to save
instance_activate_object(obj_interactable);
instance_activate_object(obj_pickable);
//SAVE MOBS
// give camera more time before a new cull step happens
var cam = instance_find(obj_camera,0);
if (instance_exists(cam))
{
	cam.culltimer += 10;
}

// save across multiple frames
alarm_set(0,1);
alarm_set(1,3);
alarm_set(2,6);
alarm_set(3,9);

// delete
alarm_set(11,12);

global.uisavestatus.saving = true;