/// @description Save 3/4
//show_debug_message("---------- / GAME DATA (3/4)...");
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::OBJ STORE DATA ALARM 2");

if (!scr_allowedToSave())
{
	instance_destroy(id);
	return;
}

scr_storePickablesSurvival();

scr_storeUserDataSurvival();
//scr_storeDeathDataSurvival();
//scr_storeMissionsSurvival();

global.uisavestatus.saving = true;