{
    "id": "03b20b75-00e3-4d9a-bf6f-5d6422413c69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_corn_plant",
    "eventList": [
        {
            "id": "020958bc-fc29-4601-82f2-421fb42c6c7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "03b20b75-00e3-4d9a-bf6f-5d6422413c69"
        },
        {
            "id": "b5ed89d8-4f90-43ca-b1f0-8d7db37c7ba0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "03b20b75-00e3-4d9a-bf6f-5d6422413c69"
        },
        {
            "id": "a97593cf-3933-4b65-a32d-df588da121e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "03b20b75-00e3-4d9a-bf6f-5d6422413c69"
        },
        {
            "id": "5d8cb849-88e0-419b-8331-65b23b6861e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "03b20b75-00e3-4d9a-bf6f-5d6422413c69"
        },
        {
            "id": "68a4fbb2-c9dc-4a95-9d9e-08733bf835f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "03b20b75-00e3-4d9a-bf6f-5d6422413c69"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
    "visible": true
}