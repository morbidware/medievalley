/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",2);
ds_map_add(ingredients,"flint",2);

resitemid = "sickle";
unlocked = true;
desc = "Usable to mow away wildgrass.";