/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(!global.isSurvival)
{
	ds_map_add(ingredients,"sapling",6);
	ds_map_add(ingredients,"woodlog",3);
}
else
{
	ds_map_add(ingredients,"woodlog",3);
	ds_map_add(ingredients,"woodplank",3);
}
resitemid = "basefencedoor";
unlocked = true;
desc = "Every safe place needs a door";