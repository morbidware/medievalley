{
    "id": "42b62b5d-ecf7-4b08-bacd-0e7a19d7f426",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_recipe_fencedoor",
    "eventList": [
        {
            "id": "4907a653-f199-4492-a0e9-acae4efb9549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42b62b5d-ecf7-4b08-bacd-0e7a19d7f426"
        },
        {
            "id": "a3b928e5-acf6-4857-9d19-a4dbbf292308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42b62b5d-ecf7-4b08-bacd-0e7a19d7f426"
        }
    ],
    "maskSpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "overriddenProperties": null,
    "parentObjectId": "93f355f3-d76a-4841-a79d-f9b850cd184e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}