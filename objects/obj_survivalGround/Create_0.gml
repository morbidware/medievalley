/// @description Create map from sprites

// map setup
var sizecols = global.survivalcellcolumns;//10
var sizerows = global.survivalcellrows;//10
waterstep = 0;
tempstorey = 0;
border = global.border;



// set map size
//ogni map room è composta da 10 spr_mappe, dato che ognuna è da 32 pixel, avremo
//32*10 = 320x320 come mappa di base + doppio border (3) e doppio clearance (2) = 330x330
cols = (global.mapsize * sizecols) + (global.border * 2) + (global.clearance * 2);
rows = (global.mapsize * sizerows) + (global.border * 2) + (global.clearance * 2);
mapgrid = ds_grid_create(cols,rows);
var r = 0;
var c = 0;

// create and clean necessary surfaces
surf = surface_create(cols, rows);
surface_set_target(surf);
draw_set_color(c_white);
draw_rectangle(0,0,cols,rows,false);
surface_reset_target();

heightsurf = surface_create(cols, rows);
surface_set_target(heightsurf);
draw_set_color(c_black);
draw_rectangle(0,0,cols,rows,false);
surface_reset_target();

// Ground grids
grid = ds_grid_create(cols,rows);		// groundType enum
spritegrid = ds_grid_create(cols,rows);	// sprite_index of the single tiles
indexgrid = ds_grid_create(cols,rows);	// image_index of the single tiles
storeygrid = ds_grid_create(cols,rows);	// storey value of the single tiles

ds_grid_clear(spritegrid,-1);
ds_grid_clear(indexgrid,-1);

// Other grids
gridaccess = ds_grid_create(cols,rows);	// tiles accessibili (1) oppure no (-1)
itemsgrid = ds_grid_create(cols,rows);	// items snappati alla griglia, default -1
wallsgrid = ds_grid_create(cols,rows);	// walls nella griglia, default -1
pathgrid = mp_grid_create(0,0,cols,rows,16,16);	// path per AI
limitgrid = ds_grid_create(ceil(cols/2),ceil(rows/2));	// griglia tileset map limits, diviso 2 perché è a 32px

// Other stuff
spawnpointsx = ds_list_create();
spawnpointsy = ds_list_create();
hspx = -1;
hspy = -1;
t = 0;

//fill all grids with default value
c = 0;
repeat (cols)
{
	r = 0;
	repeat (rows)
	{
		ds_grid_set(gridaccess, c, r, 1);
		if(c == 0 || r == 0)
		{
			ds_grid_set(grid, c, r, GroundType.Water);
		}
		else
		{
			ds_grid_set(grid, c, r, GroundType.Grass);
		}
		ds_grid_set(wallsgrid, c, r, -1);
		r++;
	}
	c++;
}
var newmap = true;
logga("==== GENERATING SURVIVAL ====");
if(string_length(global.gridstring) > 0)
{
	newmap = false;
	logga("reading saved ground string");
	ds_grid_read(grid, global.gridstring);
	ds_grid_read(storeygrid, global.storeystring);
	
	c = 0;
	repeat(cols)
	{
		r = 0;
		repeat(rows)
		{
			ds_grid_set(itemsgrid, c, r, -1);
			r++;
		}
		c++;
	}
}
else
{
	logga("generating new map");
	c = 0;
	while(c < sizecols)//2
	{
		r = 0;
		while(r < sizerows)//2
		{
			var celldata = ds_grid_get(global.survivalgrid, global.survivalposx, global.survivalposy);
			var cellgrid = ds_grid_create(1,1);
			ds_grid_read(cellgrid, celldata);
			var submap = ds_grid_get(cellgrid, c, r);
			//show_debug_message("Submap value: "+string(submap));
			if (submap > 0)
			{
				logga("generating new map");
				//show_debug_message("Drawing a "+string(c)+", "+string(r)+ " ---------- "+string(sprite_get_name(submap)));
				scr_createSubmap(submap, floor(c*32) + global.border + global.clearance, floor(r*32) + global.border + global.clearance);
			}
			r++;
		}
		c++;
		logga(string(c));
	}
	//scr_storeGround();
	//scr_storeStoreySurvival();
}

logga("==== GENERATION COMPLETED ====");
if(string_length(global.wallsstring) > 0)
{
	var walls = ds_list_create();
	ds_list_read(walls, global.wallsstring); 
	show_debug_message("Reading saved walls");
	logga("Reading saved walls");
	logga("walls list length " + string(ds_list_size(walls)));
	/*
	diagonalTopLeft = false;
	diagonalTopRight = false;
	diagonalBottomLeft = false;
	diagonalBottomRight = false;

	storey_0 = false;
	storey_1 = false;
	storey_2 = false;
	storey_3 = false;
	storey_stairs_in = false;
	storey_stairs_out = false;
	blocking = true;
	*/
	for(var i = 0; i < ds_list_size(walls); i++)
	{
		var str = ds_list_find_value(walls,i);
		
		var obj = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,obj+":","");
		
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");

		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		
		if(obj == "obj_stair")
		{
			logga("recreating stair");
			var ty = str;
			
			var inst = instance_create_depth(real(px),real(py),0,obj_stair);
			inst.type = real(ty);
		}
		else
		{
			//diagonalTopLeft
			var dtl = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,dtl+":","");

			//diagonalTopRight
			var dtr = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,dtr+":","");

			//diagonalBottomLeft
			var dbl = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,dbl+":","");

			//diagonalBottomRight
			var dbr = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,dbr+":","");
			//storey_0
			var s0 = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,s0+":","");
			//storey_1
			var s1 = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,s1+":","");
			//storey_2
			var s2 = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,s2+":","");
			//storey_3
			var s3 = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,s3+":","");
			//storey_stairs_in
			var ssi = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,ssi+":","");
			//storey_stairs_out
			var sso = string_copy(str,0,string_pos(":",str)-1);
			str = string_replace(str,sso+":","");
			//storey_stairs_out
			var bl = str;
		
			var obj_to_create = obj_wall;
			if(obj == "obj_wall_invisible")
			{
				obj_to_create = obj_wall_invisible;
			}
			if(obj == "obj_wall_shadow_only")
			{
				obj_to_create = obj_wall_shadow_only;
			}
			if(obj == "obj_wall_witch_lair")
			{
				obj_to_create = obj_wall_witch_lair;
			}
			if(obj == "obj_wall_abandoned_castle")
			{
				obj_to_create = obj_wall_abandoned_castle;
			}
			var inst = instance_create_depth(real(px),real(py),0,obj_to_create);
			inst.diagonalTopLeft = bool(dtl);
			inst.diagonalTopRight = bool(dtr);
			inst.diagonalBottomLeft = bool(dbl);
			inst.diagonalBottomRight = bool(dbr);
			inst.storey_0 = bool(s0);
			inst.storey_1 = bool(s1);
			inst.storey_2 = bool(s2);
			inst.storey_3 = bool(s3);
			inst.storey_stairs_in = bool(ssi);
			inst.storey_stairs_out = bool(sso);
			inst.blocking = bool(bl);
			var inst_col = round((inst.x-8)/16);
			var inst_row = round((inst.y-8)/16);
		
			ds_grid_set(wallsgrid, inst_col, inst_row, inst);
		}
		
	}
}
else
{
	logga("Generating new walls");
	// add invisible walls on map border
	c = 0;
	repeat(cols)//72
	{
		r = 0;
		repeat (rows)//72
		{
			if (c == 0 || c == cols-1 || r == 0 || r == rows-1)
			{
				var w = instance_create_depth(8+c*16,8+r*16,0,obj_wall_invisible);
				w.storey_0 = true;
				w.storey_1 = true;
				w.storey_2 = true;
				w.storey_3 = true;
				ds_grid_set(wallsgrid, c, r, w);
			}
			r++;
		}
		c++;
	}
}
// choose one spawnpoint
logga("choose spawnpoint");
if(!newmap)
{

	
}
else
{
	var spindex = floor(random(ds_list_size(spawnpointsx)-1));
	hspx = ds_list_find_value(spawnpointsx,spindex);
	hspy = ds_list_find_value(spawnpointsy,spindex);
}
logga("spawnpoint hspx:"+string(hspx)+" hspy:"+string(hspy));
logga("define openings");
// enable openings depending by current position on the survival grid
openLeft = false;
openRight = false;
openUp = false;
openDown = false;
/*
if (global.survivalposx == 0 && global.survivalposy == floor(global.survivalmapsizey/2)) openLeft = true;
if (global.survivalposx > 0) openLeft = true;
if (global.survivalposx < ds_grid_width(global.survivalgrid)-1) openRight = true;
if (global.survivalposy > 0) openUp = true;
if (global.survivalposy < ds_grid_height(global.survivalgrid)-1) openDown = true;
*/
// execute operations to finalize the map and navmesh
logga("createMapLimits()");
scr_createMapLimits(spr_limit_grass,ceil(global.border/2),openLeft,openRight,openUp,openDown);
logga("scr_findLimitSprites()");
scr_findLimitSprites();


/*
var m = 10;
x1 = clamp(floor((hspx-240)/16) - m,0,cols-1);
y1 = clamp(floor((hspy-135)/16) - m,0,rows-1);
var x2 = clamp(x1+30+(m*2.0),0,cols-1);
var y2 = clamp(y1+17+(m*2.0),0,rows-1);
*/
logga("grid "+ string(ds_grid_width(grid)) + ":" + string(ds_grid_height(grid)));

//logga("scr_updateTileSprites("+string(x1)+","+string(y1)+","+string(x2-x1)+","+string(y2-y1)+")");
//scr_updateTileSprites(x1,y1,x2-x1,y2-y1);
scr_updateTileSprites(0,0,ds_grid_width(grid),ds_grid_height(grid));
logga("scr_findWallEdges");
scr_findWallEdges();
logga("scr_setCliffNavigation");
scr_setCliffNavigation();
logga("scr_createStairs");
if(newmap)
{
	scr_createStairs();
}
else
{
	scr_recreateStairs();	
}

// if the player died in this land cell, respawn his stuff
/*
if ((global.survivalposy * global.survivalmapsizey) + global.survivalposx = global.lastdeathcell)
{
	var px = global.lastdeathx;
	var py = global.lastdeathy;
	var items = global.lastdeathinventory;
	for (var i = 0; i < ds_grid_width(items); i++)
	{
		var itemid = ds_grid_get(global.lastdeathinventory,i,0);
		if (itemid != noone)
		{
			var quantity = ds_grid_get(global.lastdeathinventory,i,1);
			var durability = ds_grid_get(global.lastdeathinventory,i,2);
			var itm = scr_gameItemCreate(px,py,obj_pickable,itemid,quantity,0,0,durability);
			itm.isDeathPickable = true;
			itm.x += random(24)-12;
			itm.y += random(24)-12;
		}
	}
}
*/
with(obj_water)
{
	event_perform(ev_other,ev_user0);
}

global.gridstring = scr_compressFarmString(grid);
global.storeystring = scr_compressFarmString(storeygrid);

//now that the entire survival is prepared, fill access grid with 0 (none accessible)
c = 0;
repeat (cols)
{
	r = 0;
	repeat (rows)
	{
		//ds_grid_set(gridaccess, c, r, -1);
		r++;
	}
	c++;
}

surface_free(surf);
surface_free(heightsurf);

instance_create_depth(0,0,0,obj_drawGroundSurvival);
instance_create_depth(0,0,0,obj_drawWaterSurvival);
//instance_create_depth(0,0,0,obj_drawUnderwater);
logga("obj_survivalGround Create END");
//alarm_set(0,100);