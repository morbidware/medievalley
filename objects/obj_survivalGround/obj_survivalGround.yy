{
    "id": "38bf9293-2932-4a6a-a1de-104fd9d6bbbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_survivalGround",
    "eventList": [
        {
            "id": "f67aa541-7222-4c61-8dae-4b6b971224b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38bf9293-2932-4a6a-a1de-104fd9d6bbbb"
        },
        {
            "id": "4f81b9ef-05ee-4728-80d1-6d3e22671921",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "38bf9293-2932-4a6a-a1de-104fd9d6bbbb"
        },
        {
            "id": "af497926-681a-4b3b-908f-bbf741bebb25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "38bf9293-2932-4a6a-a1de-104fd9d6bbbb"
        },
        {
            "id": "ca7e5eee-8b25-4023-81b3-c1c5b9c6198e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "38bf9293-2932-4a6a-a1de-104fd9d6bbbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "77040b30-4feb-436d-8801-61dcc0d4f657",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}