{
    "id": "9282d3fc-cd65-4373-90c6-6996f5f778df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_table",
    "eventList": [
        {
            "id": "302e65cf-e479-47ae-abc3-5ccab5101df7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "9282d3fc-cd65-4373-90c6-6996f5f778df"
        },
        {
            "id": "f45f4476-93a5-44e3-ba9e-454349927923",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9282d3fc-cd65-4373-90c6-6996f5f778df"
        },
        {
            "id": "554da3b6-0de4-4063-8caf-ceced09194f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "9282d3fc-cd65-4373-90c6-6996f5f778df"
        },
        {
            "id": "29b982ec-e81f-4ca5-9faa-0b248c1bd3b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "9282d3fc-cd65-4373-90c6-6996f5f778df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "24629d2e-be98-464c-8019-92117e0dd7e7",
    "visible": true
}