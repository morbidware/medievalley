{
    "id": "512554d3-c68e-4eea-bc78-fdb9c1a0a073",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadows_lv3",
    "eventList": [
        {
            "id": "c0234d63-9896-41eb-9280-72f89b4893ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "512554d3-c68e-4eea-bc78-fdb9c1a0a073"
        },
        {
            "id": "231ff349-10c7-4f08-a8ca-e8ab1ef54226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "512554d3-c68e-4eea-bc78-fdb9c1a0a073"
        },
        {
            "id": "898ef2ba-ccc4-4a2d-9f2c-0c6ca31a4850",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "512554d3-c68e-4eea-bc78-fdb9c1a0a073"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}