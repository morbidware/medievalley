/// @description Init surface
// used to init and target a surface only if it's needed
if (!surface_exists(surf))
{
	//surf = surface_create(display_get_width(), display_get_height());
	surf = surface_create(512, 512);
}
if (!assigned)
{
	gpu_set_blendmode(bm_add);
	surface_set_target(surf);
	draw_clear_alpha(c_white,0);
	assigned = true;
}