{
    "id": "ff2e639f-d418-4a70-b71b-8b2658210a01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_pumpkin_plant",
    "eventList": [
        {
            "id": "14551b9f-a0ab-4f45-a94d-f37cce5c786e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ff2e639f-d418-4a70-b71b-8b2658210a01"
        },
        {
            "id": "3c48936d-f1da-4c8e-93cb-4d476e6a8880",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ff2e639f-d418-4a70-b71b-8b2658210a01"
        },
        {
            "id": "35410fba-ebed-4a0d-8b44-9a8197cc6bfa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "ff2e639f-d418-4a70-b71b-8b2658210a01"
        },
        {
            "id": "75442a1c-8a16-4d6a-bd20-de85b857644a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "ff2e639f-d418-4a70-b71b-8b2658210a01"
        },
        {
            "id": "00dff09f-ea56-4471-a117-62c45c28241f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "ff2e639f-d418-4a70-b71b-8b2658210a01"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
    "visible": true
}