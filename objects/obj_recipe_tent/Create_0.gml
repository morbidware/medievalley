/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(!global.isSurvival)
{	
	ds_map_add(ingredients,"woodlog",10);
	ds_map_add(ingredients,"stone",6);
}
else
{
	ds_map_add(ingredients,"woodlog",9);
	ds_map_add(ingredients,"refinedstone",3);
	ds_map_add(ingredients,"rope",4);
}
	

resitemid = "tent";
unlocked = true;
desc = "A simple but resistant structure. Lets you recover stamina and health.";