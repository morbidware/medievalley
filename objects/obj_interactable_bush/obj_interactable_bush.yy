{
    "id": "e6e6688e-b171-4b17-84c6-131a397e245d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_bush",
    "eventList": [
        {
            "id": "48a301ef-5dae-4e88-a90a-740c1ae5c7f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        },
        {
            "id": "3473a21f-0142-4607-a074-7614c7cf05a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        },
        {
            "id": "87b80223-199e-4f1c-a76f-47a6e042cfd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        },
        {
            "id": "a9ef3921-60b0-4fd8-88c1-a28107cdf24e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        },
        {
            "id": "087b7ff2-02b0-4fa8-966d-0b80907463e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        },
        {
            "id": "362a3f61-3a71-4c78-9c4a-8e6cd95e8fdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        },
        {
            "id": "0289c181-88e6-4e51-8a47-c8955957a7e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "e6e6688e-b171-4b17-84c6-131a397e245d"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58f26c55-1885-4013-8b21-e980eda92529",
    "visible": true
}