{
    "id": "7cc79256-67e0-4a6d-b8c9-01d181b95ffb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_mailbox",
    "eventList": [
        {
            "id": "b8587569-382c-458e-8180-442d65ec98c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "7cc79256-67e0-4a6d-b8c9-01d181b95ffb"
        },
        {
            "id": "6d7c4913-7db5-442c-8a83-ad13a439ca6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "7cc79256-67e0-4a6d-b8c9-01d181b95ffb"
        },
        {
            "id": "321ce2dc-355a-4f8b-aac8-db312c2e0514",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "7cc79256-67e0-4a6d-b8c9-01d181b95ffb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4c9f42ae-fa9a-4ac9-9ee7-6e9f847a3b6a",
    "visible": true
}