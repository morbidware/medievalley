{
    "id": "934e9d2c-3ff1-4c5e-bd33-be1186ec3984",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_challenge",
    "eventList": [
        {
            "id": "1cfb9e04-e519-47c4-b1cd-226a7c4ec646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "934e9d2c-3ff1-4c5e-bd33-be1186ec3984"
        },
        {
            "id": "6d45deef-ebb6-42fa-a17f-3b17509b78da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "934e9d2c-3ff1-4c5e-bd33-be1186ec3984"
        },
        {
            "id": "096da8a1-ce86-42bd-8cbd-91d40ac6eb16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "934e9d2c-3ff1-4c5e-bd33-be1186ec3984"
        },
        {
            "id": "019e8b61-722c-40c5-85e3-3dd2ec554402",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "934e9d2c-3ff1-4c5e-bd33-be1186ec3984"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}