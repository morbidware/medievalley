if(global.musicvolume > 0)
{
	global.musicvolume = 0;
	global.sfxvolume = 0;
	btnMute.sprite_index = spr_dock_btn_audio_off;
}
else
{
	global.musicvolume = 1;
	global.sfxvolume = 1;
	btnMute.sprite_index = spr_dock_btn_audio_on;
}

audio_group_set_gain(audiogroup_music, global.musicvolume, 0);
audio_group_set_gain(audiogroup_sfx, global.sfxvolume, 0);
