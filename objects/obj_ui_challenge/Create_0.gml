/// @description Insert description here
// You can write your code in this editor
gameLevel = instance_find(obj_gameLevelChallenge,0);
if(global.ismobile)
{
	btnMute = instance_create_depth(0,0,-10001,obj_btn_generic);
	btnMute.sprite_index = spr_dock_btn_audio_on;

	btnMute.caller = id;
	btnMute.evt = ev_user0;
	btnMute.depth = depth-1;

	btnMute.image_xscale = btnMute.image_xscale * 2;
	btnMute.image_yscale = btnMute.image_yscale * 2;
	btnMute.ox = 40;
	btnMute.oy = 50;
}