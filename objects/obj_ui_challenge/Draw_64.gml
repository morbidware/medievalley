/// @description Insert description here
// You can write your code in this editor

draw_set_font(global.FontStandardBoldOutlineX2);
draw_set_halign(fa_center);
draw_set_valign(fa_top);
draw_set_color(c_white);
if(gameLevel.challengeState == 0)
{
	draw_sprite(spr_fnt_numbers_64, ceil(gameLevel.precrono)-1, display_get_gui_width()/2.0 ,display_get_gui_height()/2.0);
}
else if(gameLevel.challengeState == 1 || gameLevel.challengeState == 2)
{
	draw_sprite_ext(spr_timer_label,0,display_get_gui_width()/4.0,4,2.0,2.0,0,c_white,1.0);
	
	draw_text(display_get_gui_width()/4.0,10,string(ceil(gameLevel.crono)));

	draw_text(display_get_gui_width()/2.0,10,"YOUR SCORE");
	draw_text(display_get_gui_width()/2.0,25,string(global.challengePoints));
	/*
	if(global.challengePrizeType == 1 && !global.hideChallengePrize)
	{
		draw_text(display_get_gui_width()/2.0,10,"YOUR SCORE");
		draw_text(display_get_gui_width()/2.0,25,string(global.challengePoints));
	}
	*/
	draw_set_halign(fa_right);
	draw_sprite_ext(spr_task_label,0,display_get_gui_width()-16 ,4,2.0,2.0,0,c_white,1.0);
	draw_text(display_get_gui_width()-16 ,10,string(gameLevel.total)+"/"+string(gameLevel.required));
	
	var pla = instance_find(obj_char,0);
	if(pla != noone && !global.willEarnGalaCreds)
	{
		draw_sprite_ext(spr_money_label_big,0,(display_get_gui_width()/4)*3 ,4,2.0,2.0,0,c_white,1.0);
		draw_text((display_get_gui_width()/4)*3 ,10,string(pla.gold));
	}
}
