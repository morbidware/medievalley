/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"stone",10);
ds_map_add(ingredients,"woodlog",2);

resitemid = "grindstone";
unlocked = true;
desc = "Raw food can be refined and crumbled down with this structure.";