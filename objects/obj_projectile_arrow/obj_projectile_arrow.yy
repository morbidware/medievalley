{
    "id": "3e9794f4-5271-4a7e-9d2f-8018ee7768ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_projectile_arrow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a0a911a5-9aae-44d0-ae57-69d15e975e55",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4254a5b-deba-49db-95e1-77bcb722dcac",
    "visible": true
}