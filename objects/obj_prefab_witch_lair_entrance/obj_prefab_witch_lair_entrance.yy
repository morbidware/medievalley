{
    "id": "ad1a0bbc-7740-4f2a-adfa-35c743463b0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_witch_lair_entrance",
    "eventList": [
        {
            "id": "9b98710d-b097-4248-96f1-ed9659eaf756",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad1a0bbc-7740-4f2a-adfa-35c743463b0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ada7ea6e-036b-4172-bdae-826807e629ed",
    "visible": true
}