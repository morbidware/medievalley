/// @description Go to Wildlands
show_debug_message("Going to wildlands...");
room_set_width(roomWild, ((global.wildcellcolumns * 32) + (global.border * 2) + (global.clearance * 2)) * 16);
room_set_height(roomWild, ((global.wildcellrows * 32) + (global.border * 2) + (global.clearance * 2)) * 16);
var f = instance_create_depth(0,0,0,obj_fadeout);
f.gotoroom = roomWild;
scr_storeData();
if(room != roomWild && room != roomSurvival && global.online)
{
	joinRoom(global.socketid+"_null");
	global.lastjoinedroom = global.socketid+"_null";
}
global.skiptimestamp = false;