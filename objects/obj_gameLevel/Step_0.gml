/// @description Autosave, debug keys, console
// increase absence time from farm
if (room != roomFarm || (room == roomFarm && global.otheruserid != 0))
{
	global.awayFromFarm += global.dt;
}

// ================================== AUTOSAVE ==================================
if(!global.isSurvival)
{
	if (scr_allowedToSave())
	{
		saveinterval -= global.dt / 60;
		if(saveinterval <= 0)
		{
			logga("scr_storeData() saveinterval <= 0");
			//scr_storeData();
			//saveinterval = global.saveinterval;
		}
	
		global.clientcountinterval -= global.dt / 60;
		if (clientcountinterval <= 0)
		{
			getClientsInRoom(global.socketid);
			clientcountinterval = global.clientcountinterval;
		}

		// pickables and items autosave
		if (round(global.saveitemstimer) >= global.saveitemsinterval)
		{
			global.saveitemstimer = -1;
			//show_debug_message("--------- SAVING ITEMS & PICKABLES...");
			if (room == roomHouse)
			{
				scr_storeHousePickables();
				scr_storeHouseInteractables();
			}
			else if (room == roomFarm)
			{
				scr_storePickables();
				scr_storeInteractables();
			}
			else if(room == roomSurvival)
			{
				scr_storeInteractablesSurvival();
			}
			logga("scr_storeItems() pickables and items autosave");
			scr_storeItems();
			logga("scr_storeDeathData() pickables and items autosave");
			scr_storeDeathData();
		}
		else if (round(global.saveitemstimer) >= 0)
		{
			global.saveitemstimer += global.dt
		}

		// userdata autosave
		if (round(global.saveuserdatatimer) >= global.saveuserdatainterval)
		{
			global.saveuserdatatimer = -1;
			//show_debug_message("--------- SAVING USERDATA...");
			logga("scr_storeUserdata() userdata autosave");
			scr_storeUserdata();
		}
		else if (round(global.saveuserdatatimer) >= 0)
		{
			global.saveuserdatatimer += global.dt
		}

		// ground autosave (only in farm)
		if (room == roomFarm)
		{
			if (round(global.savegroundtimer) >= global.savegroundinterval && (room == roomFarm))
			{
				global.savegroundtimer = -1;
				//show_debug_message("--------- SAVING GROUND...");
				scr_storeGround();
			}
			else if (round(global.savegroundtimer) >= 0)
			{
				global.savegroundtimer += global.dt
			}
		}
	}
	else
	{
		saveinterval = global.saveinterval;
	}

}

// ================================== STANDARD KEY SHORTCUTS ==================================

// OPEN INVENTORY
if(keyboard_check_pressed(ord("I")) && !instance_exists(obj_console) && global.game.player.state == ActorState.Idle)
{
	if(!global.lockHUD)
	{
		// Either open the inventory or close it with the same key
		if (!instance_exists(obj_backpack_panel))
		{
			instance_create_depth(0,0,0,obj_backpack_panel);
		}
		else
		{
			with (obj_backpack_panel)
			{
				if(t >= 1.0)
				{
					event_perform(ev_other,ev_user0);
				}
			}
		}
	}
	else
	{
		if (instance_exists(obj_backpack_panel))
		{
			with (obj_backpack_panel)
			{
				if(t >= 1.0)
				{
					event_perform(ev_other,ev_user0);
				}
			}
		}
	}
}

// OPEN DIARY
if(keyboard_check_pressed(ord("J")) && !instance_exists(obj_console) && global.game.player.state == ActorState.Idle)
{
	if(!global.lockHUD)
	{
		// Either open the diary or close it with the same key
		if (!instance_exists(obj_diary_panel))
		{
			instance_create_depth(0,0,0,obj_diary_panel);
		}
		else
		{
			with (obj_diary_panel)
			{
				if(t >= 1.0)
				{
					event_perform(ev_other,ev_user0);
				}
			}
		}
	}
	else
	{
		if (instance_exists(obj_diary_panel))
		{
			with (obj_diary_panel)
			{
				if(t >= 1.0)
				{
					event_perform(ev_other,ev_user0);
				}
			}
		}
	}
}

// OPEN EMOTES
if(keyboard_check_pressed(ord("E")) && !instance_exists(obj_console) && global.game.player.state == ActorState.Idle)
{
	if(!global.lockHUD)
	{
		// Either open the inventory or close it with the same key
		if (!instance_exists(obj_ui_emotedrawer))
		{
			instance_create_depth(0,0,0,obj_ui_emotedrawer);
		}
		else
		{
			with (obj_ui_emotedrawer)
			{
				state = 1;
			}
		}
	}
	else
	{
		if (instance_exists(obj_ui_emotedrawer))
		{
			with (obj_ui_emotedrawer)
			{
				state = 1;
			}
		}
	}
}

/*
if(keyboard_check_pressed(ord("O")) && !instance_exists(obj_console))
{
	if(!global.lockHUD)
	{
		// Either open the inventory or close it with the same key
		if (!instance_exists(obj_settings_panel))
		{
			instance_create_depth(0,0,0,obj_settings_panel);
		}
		else
		{
			with (obj_settings_panel)
			{
				if(t >= 1.0)
				{
					event_perform(ev_other,ev_user0);
				}
			}
		}
	}
	else
	{
		if (instance_exists(obj_settings_panel))
		{
			with (obj_settings_panel)
			{
				if(t >= 1.0)
				{
					event_perform(ev_other,ev_user0);
				}
			}
		}
	}
}
*/

// ================================ DEBUG-ONLY KEY SHORTCUTS ================================

// open cosole
if(keyboard_check_pressed(vk_lcontrol) && m_enableConsole)
{
	if(!instance_exists(obj_console))
	{
		instance_create_depth(0,0,0,obj_console);
	}
}

// complete mission
if (keyboard_check_pressed(ord("L")) && os_browser == browser_not_a_browser)
{
	with (obj_mission)
	{
		done = required;
		checkTimer = 1;
		event_perform(ev_other,ev_user1);
	}
}

// quick go to wild
if (keyboard_check_pressed(ord("U")) && os_browser == browser_not_a_browser && !instance_exists(obj_console))
{
	changingRoom = true;
	scr_prepareWild(WildType.Rural);
	event_perform(ev_other, ev_user1);
}

// quit game
if(keyboard_check_pressed(vk_escape) && os_browser == browser_not_a_browser)
{
	if(instance_exists(obj_console))
	{
		with(obj_console)
		{
			instance_destroy();
		}
	}
	game_end();
}

// restart game
if(m_enableConsole && keyboard_check_pressed(ord("R")) && !instance_exists(obj_console))
{
	//room_restart();
	global.reset = true;
	game_restart();
}