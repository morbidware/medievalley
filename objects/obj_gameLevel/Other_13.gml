/// @description Go to House
show_debug_message("Going to house...");
room_set_width(roomHouse, global.houseCols * 16);
room_set_height(roomHouse, global.houseRows * 16);
var f = instance_create_depth(0,0,0,obj_fadeout);
f.gotoroom = roomHouse;
scr_storeData();
if(room != roomHouse && global.online)
{
	joinRoom(global.socketid+"_null");
	global.lastjoinedroom = global.socketid+"_null";
}
global.skiptimestamp = false;