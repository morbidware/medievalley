/// @description Go to Market
logga("Going to market...");
var f = instance_create_depth(0,0,0,obj_fadeout);
f.gotoroom = roomMarket;
scr_storeData();
if (room != roomMarket && global.online)
{
	joinRoom("market");
	global.lastjoinedroom = "market";
}
global.skiptimestamp = false;