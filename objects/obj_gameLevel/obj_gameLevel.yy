{
    "id": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevel",
    "eventList": [
        {
            "id": "c4410f6d-aae5-4177-9bd1-44fb9ba388b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        },
        {
            "id": "514bdf4d-d2aa-49f9-b9c9-da313733d979",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        },
        {
            "id": "3034bafa-8787-41ee-9815-662af6b83224",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        },
        {
            "id": "00fcff35-cb65-4434-a8a3-1bd6bee2a8f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        },
        {
            "id": "a6dbc3b9-c815-405a-a3bc-b5a3948334e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        },
        {
            "id": "7bb3eea5-536c-4cef-9d7f-7811be4b5b85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        },
        {
            "id": "15377135-e1ea-44e1-9ee1-2420c7675572",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}