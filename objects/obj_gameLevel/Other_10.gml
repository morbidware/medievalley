/// @description Go to Farm
show_debug_message("Going to farm...");
room_set_width(roomFarm,global.farmCols*16);
room_set_height(roomFarm,global.farmRows*16);
scr_storeData();
if(room != roomFarm && global.online)
{
	global.otherusername = "";
	global.otheruserid = 0;
	global.othersocketid = "";
	global.othergridstring = "";
	global.otherpickables = "";
	global.otherinteractables = "";
	joinRoom(global.socketid);
	global.lastjoinedroom = global.socketid;
	with(instance_create_depth(0,0,0,obj_fadeout))
	{
		gotoroom = noone;
	}
	if (global.visitorscount > 0)
	{
		updateFarm();
	}
	else
	{
		getPlayerData(global.username,global.userigid);
	}
}
else
{
	with (instance_create_depth(0,0,0,obj_fadeout))
	{
		gotoroom = roomFarm;
	}
}