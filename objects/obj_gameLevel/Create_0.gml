/// @description Init enums
if(global.ismobile)
{
	camera_set_view_size(view_camera[0],window_get_width()*0.5, window_get_height()*0.5);
	view_set_wport(0,window_get_width());
	view_set_hport(0,window_get_height());
	display_set_gui_size(window_get_width(),window_get_height());
}
enum GroundType
{
	None,
	Void,
	TallGrass,
	Grass,
	Soil,
	Plowed,
	Water,
	Cliff,
	HouseFloor,
	HouseBasement
}

enum ActorState
{
	Idle,
	Interact,
	Eat,
	Attack,
	Hit,
	Die,
	Pick,
	PickUp,
	Chop,
	Craft,
	Hoe,
	Sow,
	Watering,
	Shovel,
	Charge,
	Sickle,
	Mine,
	Build,
	Rest,
	Rebuild,
	Cook,
	Hammer,
	Fish,
	None
}

enum GameItemKind
{
	Pickable,
	Interactable,
	Inventory,
}

enum GameItemFamily
{
	Tree,
	Plant,
	Seed,
	Food,
	Weapon,
	Clothing
}

enum HandState
{
	Empty,
	Axe,
	Mine,
	Shovel,
	Melee,
	Hoe,
	Ranged,
	Sow,
	Watering,
	Sickle,
	Build,
	Hammer,
	Fish
}

enum Direction
{
	Right,
	UpRight,
	Up,
	UpLeft,
	Left,
	DownLeft,
	Down,
	DownRight
}

enum MobState
{
	Idle,
	Roam,
	Chase,
	Flee,
	Charge,
	Attack,
	Suffer,
	Die,
	AfterAttack,
	RunAway,
}

enum DepthType
{
	Underwater,
	Reflection,
	Water,
	WaterFx,
	StaticBg,
	
	Lv0,
	Lv0Detail,
	Lv0Shadow,
	
	Lv1,
	Lv1Detail,
	Lv1Shadow,
	
	Lv2,
	Lv2Detail,
	Lv2Shadow,
	
	Lv3,
	Lv3Detail,
	Lv3Shadow,
	
	Elements,
	Overlay
}

enum ShadowType
{	
	None,
	Blob,
	Sprite,
	Mob,
	Square
}

enum LastVisitedPlace
{
	Farm,
	City,
	Wildlands,
	House,
	Survival
}

enum WildType
{
	Rural,
	Mountain,
	Coast
}

enum StructurePlacement
{
	All,
	Farm,
	Basement
}

enum ChallengeType
{
	Chopping,
	Sickling,
	Mining,
	Watering,
	Fighting,
	Picking,
	Hoeing
}

global.game = id;
global.lastMouseButton = "";
global.dt = 0;
firstday = false;
saveinterval = global.saveinterval;
clientcountinterval = global.clientcountinterval;
changingRoom = false;

if(global.gridstring == "")
{
	show_debug_message("gridstring doesn't exists");
	global.gridstring = "";
}

if(global.pickables == 0)
{
	global.pickables = ds_list_create();
}

if(global.interactables == 0)
{
	global.interactables = ds_list_create();
}

//this is temp here because when selecting a farm to visit the panel stays and lockHud is true
//global.lockHUD = false;

if (room == roomFarm || room == roomWild || room == roomSurvival || room == roomMarket || room == roomChallenge)
{
	instance_create_depth(0,0,0,obj_fx_char_reflection);
	instance_create_depth(0,0,0,obj_shadows_lv0);
	instance_create_depth(0,0,0,obj_shadows_lv1);
	instance_create_depth(0,0,0,obj_shadows_lv2);
	instance_create_depth(0,0,0,obj_shadows_lv3);
	instance_create_depth(0,0,0,obj_shadows_blob);
}