/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"potion_base",1);
ds_map_add(ingredients,"porcinimushroom",5);
resitemid = "potion_life";
unlocked = true;
desc = "Recover your health with this potion.";