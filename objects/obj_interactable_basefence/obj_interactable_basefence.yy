{
    "id": "984ad143-803a-4493-b590-47d369b5a990",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_basefence",
    "eventList": [
        {
            "id": "ba950d56-c7fe-460e-b2f5-06ed182fd5c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        },
        {
            "id": "df29e899-d34b-492f-91ef-f1e00b5ba56e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        },
        {
            "id": "640968de-c164-4ba4-b10e-7a4504c56015",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        },
        {
            "id": "356bb134-afc0-4634-8c9a-934548e64cfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        },
        {
            "id": "297f3029-a1df-4ba5-ba5c-9738678356f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        },
        {
            "id": "8aab5acb-2797-4e9a-a730-d7f523d1d508",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        },
        {
            "id": "b1e7fd59-6277-4eed-939b-9e9a47b90e3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "984ad143-803a-4493-b590-47d369b5a990"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
    "visible": true
}