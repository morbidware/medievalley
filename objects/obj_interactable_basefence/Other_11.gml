/// @description Can be interacted

interactionPrefixLeft = "";

var handState = global.game.player.handState;
if(handState == HandState.Axe)
{
	global.retval = true;
	interactionPrefixRight = "Chop";
}
else
{
	global.retval = false;
}
