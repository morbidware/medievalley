{
    "id": "d14343a5-5479-45d6-bacc-8e95fb065fac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_status_stamina",
    "eventList": [
        {
            "id": "7589403a-6061-4180-b4a5-85d790363e88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d14343a5-5479-45d6-bacc-8e95fb065fac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a52de1de-4ab3-40a4-a337-03792d097632",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f9ef179f-672a-480c-901b-f28ba350d7e0",
    "visible": true
}