/// @description Insert description here
// You can write your code in this editor
event_inherited();
if(crono <= (5*(ticks-1))*60)
{
	ticks--;
	show_debug_message("get a life " + string(ticks));
	with(obj_char)
	{
		stamina += 3;
		if(stamina > 100)
		{
			stamina = 100;
		}
		global.saveuserdatatimer = 0;
	}
}
