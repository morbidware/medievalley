{
    "id": "8f7ef41f-205b-469b-b854-61aed456fa52",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_cauliflower_plant",
    "eventList": [
        {
            "id": "4909a2d8-cad1-4b7a-880c-8dc3b8080db3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8f7ef41f-205b-469b-b854-61aed456fa52"
        },
        {
            "id": "4f9ed18e-d004-46ad-9449-f10d62932dd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8f7ef41f-205b-469b-b854-61aed456fa52"
        },
        {
            "id": "461664c6-9de1-46b5-8447-194218aac3de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "8f7ef41f-205b-469b-b854-61aed456fa52"
        },
        {
            "id": "daff20f8-b7f7-4beb-b47a-e2ff95da7da7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "8f7ef41f-205b-469b-b854-61aed456fa52"
        },
        {
            "id": "d40d9b94-f01e-43b7-8099-f2a925a903c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "8f7ef41f-205b-469b-b854-61aed456fa52"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
    "visible": true
}