///@description Helter Shelter - Build a Tent
event_inherited();

missionid = "0005";
title = "Helter Shelter";
shortdesc = "Build a Tent";
longdesc = "Chop trees and mine rocks to gather resources and build a tent.";

ds_map_add(rewards,obj_reward_0005,1);
ds_map_add(required,"tent",1);

// NOTICE: this mission is created together with 0004, so hints are for both.
ds_list_add(hintTexts, "Time to plant seeds and grow food from the ground...", "...and also build a more sturdy shelter for you.", "Check Journal and summaries to see what's next to do!");
ds_list_add(hintImages, spr_pickable_redbeans_seeds, spr_pickable_tent, spr_dock_btn_diary);

scr_storeMissions();