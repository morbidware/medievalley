{
    "id": "09261612-a879-4f5a-86c4-25794948ed65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_food",
    "eventList": [
        {
            "id": "88232be7-68c2-4cdc-9497-d5cf213b1243",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09261612-a879-4f5a-86c4-25794948ed65"
        },
        {
            "id": "0edd3441-cf08-401c-8a1a-94c0c2175a2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09261612-a879-4f5a-86c4-25794948ed65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65d64d9f-5aaf-4dab-a90e-62918878b9c8",
    "visible": true
}