{
    "id": "a6ed28d0-3344-4ab7-b80f-eea32d7d899e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_refined",
    "eventList": [
        {
            "id": "df666202-2e33-4b2f-aa07-01b60729f161",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a6ed28d0-3344-4ab7-b80f-eea32d7d899e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0781a3c-9d89-4967-bcc1-d48262d7a5dd",
    "visible": true
}