{
    "id": "f067217b-f2d4-4525-9b88-4f9d22fd47f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_tool_trail",
    "eventList": [
        {
            "id": "8f330021-5508-4dbf-a9f2-c40d2e5d5f14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f067217b-f2d4-4525-9b88-4f9d22fd47f2"
        },
        {
            "id": "cd30376b-0841-4f5d-b084-a4d7a11d2eac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f067217b-f2d4-4525-9b88-4f9d22fd47f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3af956ea-94ea-4101-ba51-3464113e98e0",
    "visible": true
}