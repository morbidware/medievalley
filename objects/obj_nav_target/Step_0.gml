///@description Animation

size += 0.02 * global.dt;
alpha -= 0.03 * global.dt;

if (alpha <= 0)
{
	alpha = 1;	
	size = 1;
}

image_xscale = size;
image_yscale = size;
image_alpha = alpha;

scr_setDepth(DepthType.Elements);
depth -= 1;