{
    "id": "be0315d7-2c5c-445d-a9f1-b4054de639ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_nav_target",
    "eventList": [
        {
            "id": "5f4863de-063e-4f45-839d-5d4ba2778061",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "be0315d7-2c5c-445d-a9f1-b4054de639ac"
        },
        {
            "id": "86deae46-3766-4efe-a445-863bb2746296",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "be0315d7-2c5c-445d-a9f1-b4054de639ac"
        },
        {
            "id": "d62a007d-77d2-4fd3-9f76-54c3895a9ed2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "be0315d7-2c5c-445d-a9f1-b4054de639ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aebff56e-045d-4e3b-988e-8d20ad89baa3",
    "visible": true
}