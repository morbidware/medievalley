image_angle += sx*5.0*global.dt;
x += sx;
y += sy;
image_alpha -= alphaloss*global.dt;
sx *= 0.95;
sy *= 0.95;
if(image_alpha <= 0)
{
	instance_destroy();
}