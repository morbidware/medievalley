event_inherited();
sprite_index = spr_cloudlet;
image_index = choose(0,1,1,2);
image_speed = 0;
sy = random_range(-1.5,1.5);
sx = random_range(-1.5,1.5);
image_alpha = 1.0;
alphaloss = 0.01 + random(0.01);