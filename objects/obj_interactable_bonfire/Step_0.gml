if(growstage == 1)
{
	image_speed = 0.0;
	image_index = 0;
}
else if(growstage == 2)
{
	image_speed = 0.7;
	if(image_index >= image_number - 1)
	{
		image_index = 1;
	}
	
	progress += global.dt;
	if(progress >= 21600) // 6 minutes
	{
		growstage = 1;
		progress = 0;
	}
}

flickertimer -= 1;
if (flickertimer < 0)
{
	flickertimer = 2;
	flickertarget = 1.4 + random(0.15);
}
flickerscale = lerp(flickerscale,flickertarget,0.3);
