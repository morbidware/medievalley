{
    "id": "fe01e687-09f7-4f60-8367-34a35cff5afa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_bonfire",
    "eventList": [
        {
            "id": "748fc28f-e725-4d55-b4ef-4a2516068fb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "92806bb9-8504-4bb8-9e37-92140cc37e83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "d7d6ae42-c73a-47c0-88a9-b9064d5569ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "42ffd56e-a6c3-435f-afbe-ac2e35c930c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "31e23d27-46a6-4614-8364-8e4472d14bba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "3a560d92-8ac6-434f-8216-e28a1b0104e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "154a75f2-18fa-4aef-ad42-851133f2a17f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        },
        {
            "id": "86df7559-8228-49d4-99df-678dc2cde7cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "fe01e687-09f7-4f60-8367-34a35cff5afa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
    "visible": true
}