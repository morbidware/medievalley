/// @description finish interaction
if (global.lastMouseButton == "right")
{
	repeat(10)
	{
		instance_create_depth(x,y,-y,obj_fx_cloudlet);
	}
	
	// Gives back some wood only if fire is not yet exhausted
	if (growstage > 1)
	{
		scr_dropItemQuantity(id,"woodlog",1);
	}
	scr_dropItemQuantity(id,"stone",2);
	instance_destroy();
}