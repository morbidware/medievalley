///@description Turn on/off

/// Turn on
if(growstage == 1)
{
	repeat(10)
	{
		instance_create_depth(x,y,-y,obj_fx_cloudlet);
	}
	growstage = 2;
}
// Turn off
else if(growstage == 2)
{
	repeat(10)
	{
		instance_create_depth(x,y,-y,obj_fx_cloudlet);
	}
	eventPerformInteractable(x,y,id,object_index,ev_other,ev_user2);
	growstage = 1;
}