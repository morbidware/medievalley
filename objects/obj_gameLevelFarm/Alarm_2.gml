/// @description Spawn pidegon
if (global.spawnpidgeon)
{
	instance_create_depth(-100,-100,0,obj_wildlife_mail_pidgeon);
	global.spawnpidgeon = false;
}