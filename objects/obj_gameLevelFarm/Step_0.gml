/// @description Debug, rooms, regrow routine
event_inherited();
timeline_speed = global.dt;

if(instance_exists(obj_console)){exit;}

if(m_enableConsole && keyboard_check_pressed(ord("Y")))
{
	getUserFarm(2,ds_list_find_value(global.sockets,1));
}

// regrow items in map
regrowtimer -= global.dt / 60;
if (regrowtimer <= 0 && !firstday)
{
	// First activate item, then perform regrow the next frame so to leave room for activating all objects
	instance_activate_object(obj_pickable);
	instance_activate_object(obj_interactable);
	alarm_set(0,1);
	regrowtimer = regrowtime;
}

//detect when the player is changing room
if (!onIntro)
{
	if (!changingRoom)
	{
		with (obj_char)
		{
			// EXIT UP
			if (y < 32)
			{
				// stop player if he can't go there now
				if (global.otheruserid != 0 && global.online)
				{
					y = 32;
					if (instance_number(obj_hint) == 0)
					{
						var texts = ds_list_create();
						ds_list_add(texts,"To visit the Town, go back to your farm first.");
						var images = ds_list_create();
						ds_list_add(images,spr_dock_btn_home);
						scr_show_hint("YOU ARE ONLINE!",texts,images,true);
					}
				}
				else if (ds_map_find_value(global.unlockables, "access_town") < 1)
				{
					y = 32;
					if (instance_number(obj_hint) == 0)
					{
						var texts = ds_list_create();
						ds_list_add(texts,"You can't go there now.");
						var images = ds_list_create();
						ds_list_add(images,noone);
						scr_show_hint("",texts,images,false);
					}
				}
				else
				{
					// change room
					other.changingRoom = true;
					event_perform_object(obj_gameLevel, ev_other, ev_user2);
				}
			}
			
			// EXIT RIGHT
			if (x > room_width-32)
			{
				// stop player if he can't go there now
				if (global.otheruserid != 0 && global.online)
				{
					x = room_width-32;
					if (instance_number(obj_hint) == 0)
					{
						var texts = ds_list_create();
						ds_list_add(texts,"To visit the Wild Lands, go back to your farm first.");
						var images = ds_list_create();
						ds_list_add(images,spr_dock_btn_home);
						scr_show_hint("YOU ARE ONLINE!",texts,images,true);
					}
				}
				else if (ds_map_find_value(global.unlockables, "access_wild") < 1)
				{
					x = room_width-32;
					if (instance_number(obj_hint) == 0)
					{
						var texts = ds_list_create();
						ds_list_add(texts,"You can't go there now.");
						var images = ds_list_create();
						ds_list_add(images,noone);
						scr_show_hint("",texts,images,false);
					}
				}
				else
				{
					other.changingRoom = true;
					//leaveRoom(global.socketId);
					scr_prepareWild(WildType.Rural);
					event_perform_object(obj_gameLevel, ev_other, ev_user1);
				}
			}
		}
	}
}
