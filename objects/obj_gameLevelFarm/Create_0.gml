/// @description Init and populate map
event_inherited();
logga("--------------------------------------------------------");
logga("--------------------------------------------------------");
logga("--------------------------CREATE NEW FARM");
logga("--------------------------------------------------------");
logga("--------------------------------------------------------");
// Firstday is active as long as the inventory hasn't been unlocked
onIntro = false;
firstday = false;

logga("ui_inventory === " + string(round(ds_map_find_value(global.unlockables,"ui_inventory"))));
if (round(ds_map_find_value(global.unlockables,"ui_inventory")) == 0)
{
	logga("firstday true");
	firstday = true;
	global.allowvisitors = 0;
	global.skiptimestamp = true;
}

// If firstday, always start from the beginning of the day
if (firstday)
{
	global.dayprogress = 0.05;
}

if (global.online)
{
	logga("username " + global.username);
	logga("other username " + global.otherusername);
	logga("userid " + string(global.userid));
	logga("other userid " + string(global.otheruserid));
	logga("global.dayprogress " + string(global.dayprogress));
}

logga("creating farmground...");
farmground = instance_create_depth(0,0,0,obj_farmGround);
logga("creating player...");
player = instance_create_depth(528, 528, 0, obj_char);

// amount of objects and regrow
regrowtime = 60 * 2; // In seconds
regrowtimer = 0;
regrowdivider = 8;
//
flintcount = 0;
saplingcount = 0;
intsaplingcount = 0;
treecount = 0;
bouldercount = 0;
wildgrasscount = 0;		
//
maxflint = 22;
maxsapling = 22;
maxintsapling = 8;
maxtrees = 100;
maxboulders = 7;
maxwildgrass = 200;

instance_create_depth(0,0,0,obj_fadein);
instance_create_depth(0,0,0,obj_sound_birds);

audio_play_sound(choose(bgm_farm_a, bgm_farm_b),0,true);

audio_play_sound(snd_white_noise,0,true);

// ======================================| PICKABLES |======================================

logga("create pickables");
if(string_length(global.otherpickables) > 0)
{	
	show_debug_message("Reading OTHER saved pickables");
	logga("Reading OTHER saved pickables");
	//logga(global.otherpickables);
	var pickables = ds_list_create();
	ds_list_read(pickables, global.otherpickables); 
	logga("OTHER pickables list length " + string(ds_list_size(pickables)));
	for(var i = 0; i < ds_list_size(pickables); i++)
	{
		var str = ds_list_find_value(pickables,i);
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		var qu = str;
		/*
		logga("str is " + str);
		logga("px is " + px);
		logga("py is " + py);
		logga("iid is " + iid);
		logga("qu is " + qu);
		logga("-----------------------");
		*/
		scr_gameItemCreate(real(px),real(py),obj_pickable,iid,real(qu));
	}
}
else if(string_length(global.pickables) > 0)
{	
	show_debug_message("Reading saved pickables");
	logga("Reading saved pickables");
	//logga(global.pickables);
	var pickables = ds_list_create();
	ds_list_read(pickables, global.pickables); 
	logga("pickables list length " + string(ds_list_size(pickables)));
	for(var i = 0; i < ds_list_size(pickables); i++)
	{
		var str = ds_list_find_value(pickables,i);
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		var qu = str;
		/*
		logga("str is " + str);
		logga("px is " + px);
		logga("py is " + py);
		logga("iid is " + iid);
		logga("qu is " + qu);
		logga("-----------------------");
		*/
		scr_gameItemCreate(real(px),real(py),obj_pickable,iid,real(qu));
	}
}
else
{
	show_debug_message("Spawning new pickables");
	logga("Spawning new pickables");
	
	scr_populate_with(obj_pickable,"flint",floor(maxflint*0.3),GroundType.Soil,false,false);
	scr_populate_with(obj_pickable,"flint",floor(maxflint*0.7),GroundType.Grass,false,false);
	scr_populate_with(obj_pickable,"sapling",floor(maxsapling*0.3),GroundType.Soil,false,false);
	scr_populate_with(obj_pickable,"sapling",floor(maxsapling*0.7),GroundType.Grass,false,false);
}

// ======================================| INTERACTABLES |======================================

show_debug_message("You have been out of the farm for "+string(global.awayFromFarm / 60)+" seconds.");
logga("You have been out of the farm for "+string(global.awayFromFarm / 60)+" seconds.");
logga("create interactables");
if(string_length(global.otherinteractables) > 0)
{
	
	var interactables = ds_list_create();
	ds_list_read(interactables, global.otherinteractables); 
	show_debug_message("Reading saved OTHER interactables");
	logga("Reading saved OTHER interactables");
	logga("OTHER interactables list length " + string(ds_list_size(interactables)));
	for(var i = 0; i < ds_list_size(interactables); i++)
	{
		var str = ds_list_find_value(interactables,i);
		
		//logga("str is " + str);
		//show_debug_message(string(str));
		
		var obj = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,obj+":","");
		
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		//growcrono
		var gc = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gc+":","");
		//growstage
		var gs = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gs+":","");
		//quantity
		var gq = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gq+":","");
		//durability
		var gd = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gd+":","");
		//gridded
		var gg = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gg+":","");
		//life
		var gl = str;
		
		/*
		logga("iid is " + iid);
		logga("px is " + px);
		logga("py is " + py);
		logga("gc is " + gc);
		logga("gs is " + gs);
		logga("gq is " + gq);
		logga("gd is " + gd);
		logga("gg is " + gg);
		logga("gl is " + gl);
		logga("-----------------------");
		*/
		var inst = scr_gameItemCreate(real(px), real(py), scr_asset_get_index(obj), iid,real(gq), real(gc), real(gs), real(gd), real(gg), real(gl));
	}
}
else if(string_length(global.interactables) > 0)
{
	var interactables = ds_list_create();
	ds_list_read(interactables, global.interactables); 
	show_debug_message("Reading saved interactables");
	logga("Reading saved interactables");
	logga("interactables list length " + string(ds_list_size(interactables)));
	for(var i = 0; i < ds_list_size(interactables); i++)
	{
		var str = ds_list_find_value(interactables,i);
		
		logga("str is " + str);
		//show_debug_message(string(str));
		
		var obj = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,obj+":","");
		
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		//growcrono
		var gc = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gc+":","");
		//growstage
		var gs = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gs+":","");
		//quantity
		var gq = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gq+":","");
		//durability
		var gd = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gd+":","");
		//gridded
		var gg = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gg+":","");
		//life
		var gl = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gl+":","");
		//startingslot
		var gss = str;
		
		/*
		logga("iid is " + iid);
		logga("px is " + px);
		logga("py is " + py);
		logga("gc is " + gc);
		logga("gs is " + gs);
		logga("gq is " + gq);
		logga("gd is " + gd);
		logga("gg is " + gg);
		logga("gl is " + gl);
		logga("-----------------------");
		*/
		
		var growcrono = real(gc);
		if (round(growcrono) > 1)
		{
			growcrono = clamp(growcrono - global.awayFromFarm, 1, 99999999);
		}
		var inst = scr_gameItemCreate(real(px), real(py), scr_asset_get_index(obj), iid,real(gq), growcrono, real(gs), real(gd), real(gg), real(gl));
		if(real(gss) > -1)
		{
			logga("assign a startingslot = " + string(gss));
			inst.startingslot = real(gss);
		}
	}
}
else
{
	show_debug_message("Spawning new interactables");
	logga("Spawning new interactables");
	scr_populate_with(obj_interactable_sapling,"sapling",maxintsapling,GroundType.Soil,true,false);
	logga("trees");
	scr_populate_with(obj_interactable_pine,"pine",floor(maxtrees*0.15),GroundType.TallGrass,true,false);
	scr_populate_with(obj_interactable_pine,"pine",floor(maxtrees*0.35),GroundType.Grass,true,false);
	scr_populate_with(obj_interactable_beechtree,"beechtree",floor(maxtrees*0.15),GroundType.TallGrass,true,false);
	scr_populate_with(obj_interactable_beechtree,"beechtree",floor(maxtrees*0.35),GroundType.Grass,true,false);
	logga("rocks");
	scr_populate_with(obj_interactable_rock,"rock",floor(maxboulders*0.6),GroundType.Soil,true,false);
	scr_populate_with(obj_interactable_rockbig,"rockbig",floor(maxboulders*0.4),GroundType.Soil,true,false);
	logga("wildgrass");
	scr_populate_with(obj_interactable_wildgrass,"wildgrass",floor(maxwildgrass*0.6),GroundType.Grass,false,false);
	scr_populate_with(obj_interactable_wildgrass,"wildgrass",floor(maxwildgrass*0.4),GroundType.TallGrass,false,false);
}
global.awayFromFarm = 0;

// ======================================| EXTRA OBJECTS |======================================

if(m_extraObjects)
{
	if (!instance_exists(obj_interactable_farmboard))
	{
		logga("Creating farmboard");
		scr_gameItemCreate(8+(16*((global.farmCols/2)+3)), 8+(6*16), obj_interactable_farmboard, "farmboard");
	}
	if (!instance_exists(obj_interactable_stash))
	{
		logga("Creating stash");
		scr_gameItemCreate(8+(16*16), 8+(16*10), obj_interactable_stash, "stash");
	}
	if (!instance_exists(obj_interactable_house))
	{
		logga("Creating house");
		with(scr_gameItemCreate(8+(16*22), 8+(16*6), obj_interactable_house, "house"))
		{
			growstage = 2;
		}
	}
}
if (!instance_exists(obj_interactable_mailbox))
{
	logga("Creating mailbox");
	scr_gameItemCreate(8+(16*(global.farmCols/2-1)), 8+(6*16), obj_interactable_mailbox, "mailbox");
}

// ======================================| MISSIONS |======================================
if (m_testMissions == true)
{
	instance_create_depth(0,0,0,obj_mission_test1);
	instance_create_depth(0,0,0,obj_mission_test2);
}
if (m_enableProgress && global.othersocketid == "")
{
	if(!firstday)
	{
		if(!instance_exists(obj_mission))
		{
			var missions = ds_list_create();
			ds_list_read(missions, global.missions); 
			logga("Reading saved missions");
			logga("missions list length " + string(ds_list_size(missions)));
			for(var i = 0; i < ds_list_size(missions); i++)
			{
				var str = ds_list_find_value(missions,i);
				logga("string is " + str);
				var mid = string_copy(str,0,string_pos(":",str)-1);
				logga("missionid:"+mid);
				str = string_copy(str,string_pos(":",str)+1,(string_length(str)-string_pos(":",str)));
			
				var md = str;
				logga("missiondonestring:"+str);
				var inst = instance_create_depth(0, 0, 0, scr_asset_get_index("obj_mission_"+mid));
				ds_map_read(inst.done,md);
				inst.active = true;
				var sm = instance_find(obj_summary_manager,0);
				ds_list_add(sm.activeMissions,inst.missionid);
			}
		}
	}
	else
	{
		timeline_index = tml_game_intro;
		timeline_running = true;
		onIntro = true;
	}
}
else
{
	firstday = false;
}

instance_create_depth(0,0,-1000,obj_ui_black_stripes);
var cam = instance_create_depth(player.x,player.y,0,obj_camera);

// ======================================| TILES ACCESSIBILITY |======================================

// make accessible again the tiles that were temporarily locked (i.e. the road)
for (var i = 0; i < ds_list_size(farmground.templock_x); i++)
{
	var tx = ds_list_find_value(farmground.templock_x,i);
	var ty = ds_list_find_value(farmground.templock_y,i);
	ds_grid_set(farmground.gridaccess, tx, ty, 1);
}


// make unaccessible the tile that will host the farmboard later
ds_grid_set(farmground.gridaccess, 8+(((global.farmCols/2)+2)), 8+(6), 0);
ds_grid_set(farmground.gridaccess, 8+(((global.farmCols/2)+3)), 8+(6), 0);
ds_grid_set(farmground.gridaccess, 8+(((global.farmCols/2)+4)), 8+(6), 0);

// ======================================| FXS |======================================

repeat(10)
{
	var rx = random(room_width);
	var ry = random(room_height);
	instance_create_depth(rx, ry, -ry, obj_fx_ground_dust);
}

if(string_length(global.othersocketid) > 0)
{
	joinRoom(global.othersocketid);
	global.lastjoinedroom = global.othersocketid;
}
//else
//{
//	joinRoom(global.socketid);
//}


// ======================================| PLAYER SPAWN POSITION |======================================

if (!firstday)
{
	switch (global.lastVisitedPlace)
	{
		case LastVisitedPlace.Farm:
			player.x = (global.farmCols/2)*16 +16;
			player.y = 128;
			player.lastdirection = Direction.Down;
			break;
		case LastVisitedPlace.House:
			var house = instance_find(obj_interactable_house,0);
			player.x = house.x;
			player.y = house.y + 32;
			player.lastdirection = Direction.Down;
			break;
		case LastVisitedPlace.City:
			player.x = (global.farmCols/2)*16 +16;
			player.y = 56;
			player.lastdirection = Direction.Down;
			break;
		case LastVisitedPlace.Wildlands:
			player.x = global.farmCols*16 - 56;
			player.y = (global.farmRows/2)*16 +16;
			player.lastdirection = Direction.Left;
			break;
	}
}
else
{
	player.x = (global.farmCols/2)*16 +16;
	player.y = 2;
	player.lastdirection = Direction.Down;
}
player.prevx = player.x;
player.prevy = player.y;
cam.x = player.x;
cam.y = player.y;

scr_saveVersionActions();

with (obj_mission)
{
	event_perform(ev_other,ev_user1);
}

alarm_set(1,2);
alarm_set(2,30);

// Update crop count here since it's possible to have some inconsistency left if the player quitted
// the game before it could update the stats on the database
global.stat_crops = 0;
with (obj_interactable)
{
	if (growstages > 4 && needswatering == 1 && string_pos("plant",itemid) > 0)
	{
		global.stat_crops++;
	}
}