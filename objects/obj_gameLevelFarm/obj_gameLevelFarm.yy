{
    "id": "52975f86-d6d0-4a21-bfd6-b292f45dd63f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevelFarm",
    "eventList": [
        {
            "id": "8679369f-296a-403c-b25a-a0c2980f5552",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52975f86-d6d0-4a21-bfd6-b292f45dd63f"
        },
        {
            "id": "35dd3065-6f99-440b-a41a-483558d85be0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "52975f86-d6d0-4a21-bfd6-b292f45dd63f"
        },
        {
            "id": "373c2388-ab28-4af6-b2d4-faa1eab3181d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "52975f86-d6d0-4a21-bfd6-b292f45dd63f"
        },
        {
            "id": "957a4e93-d206-432b-85e9-56466a8f6b85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "52975f86-d6d0-4a21-bfd6-b292f45dd63f"
        },
        {
            "id": "77ae4e55-0798-4a07-952b-43042a9e1dac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "52975f86-d6d0-4a21-bfd6-b292f45dd63f"
        },
        {
            "id": "26fe9c3b-1259-4a12-a16c-3aaf2402f807",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "52975f86-d6d0-4a21-bfd6-b292f45dd63f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}