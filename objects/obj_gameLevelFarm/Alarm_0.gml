/// @description Actual item regrow

flintcount = 0;
saplingcount = 0;
intsaplingcount = 0;
treecount = 0;
bouldercount = 0;
wildgrasscount = 0;	

with (obj_pickable)
{
	switch (itemid)
	{
		case "flint": other.flintcount++; break;
		case "sapling": other.saplingcount++; break;
	}
}
with (obj_interactable)
{
	switch (itemid)
	{
		case "sapling": other.intsaplingcount++; break;
		case "rock": other.bouldercount++; break;
		case "rockbig": other.bouldercount++; break;
		case "beechtree": other.treecount++; break;
		case "pine": other.treecount++; break;
		case "wildgrass": other.wildgrasscount++; break;
	}
}
	
if (flintcount < ceil(maxflint / regrowdivider))
{
	var r = 1; if (flintcount == 0) { r = 3;}
	repeat (r)
	{
		scr_populate_with(obj_pickable,"flint",1,choose(GroundType.Soil,GroundType.Grass),false,true);
		show_debug_message("Regrowing a flint");
	}
}
if (saplingcount < ceil(maxsapling / regrowdivider))
{
	var r = 1; if (flintcount == 0) { r = 2;}
	repeat(r)
	{
		scr_populate_with(obj_pickable,"sapling",1,choose(GroundType.Soil,GroundType.Grass),false,true);
		show_debug_message("Regrowing a sapling");
	}
}
if (intsaplingcount < ceil(maxintsapling / regrowdivider))
{
	var r = 1; if (flintcount == 0) { r = 2;}
	repeat(r)
	{
		scr_populate_with(obj_interactable_sapling,"sapling",1,GroundType.Soil,true,false);
		show_debug_message("Regrowing an interactable sapling");
	}
}
if (bouldercount < ceil(maxboulders / regrowdivider))
{
	if (random(1) < 0.5)
	{
		scr_populate_with(obj_interactable_rock,"rock",1,GroundType.Soil,false,true);
		show_debug_message("Regrowing an rock");
	}
	else
	{
		scr_populate_with(obj_interactable_rockbig,"rockbig",1,GroundType.Soil,false,true);
		show_debug_message("Regrowing a bigrock");
	}	
}
if (treecount < ceil(maxtrees / regrowdivider))
{
	if (random(1) < 0.5)
	{
		scr_populate_with(obj_interactable_pine,"pine",1,choose(GroundType.Grass,GroundType.Grass,GroundType.TallGrass),false,true);
		show_debug_message("Regrowing a pine");
	}
	else
	{
		scr_populate_with(obj_interactable_beechtree,"beechtree",1,choose(GroundType.Grass,GroundType.Grass,GroundType.TallGrass),false,true);
		show_debug_message("Regrowing a beechtree");
	}
}