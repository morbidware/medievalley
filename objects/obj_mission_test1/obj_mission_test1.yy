{
    "id": "2ddabc3b-bc9b-41fd-b853-79160853ad13",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mission_test1",
    "eventList": [
        {
            "id": "ba6cc7b1-3a45-4374-8fb9-918d61031b51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ddabc3b-bc9b-41fd-b853-79160853ad13"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8299d0fd-0188-4419-ba64-b8c0f3b131ce",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}