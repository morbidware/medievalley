if(string_length(desc) == 0)
{
	exit;
}
draw_set_alpha(1);
draw_set_color(c_white);
scr_nine_slice_at(spr_9slice_pointer_simple, x + offsetx - w/2 - 32, y, w, 32, 2);

draw_sprite_ext(spr_recipeinfo_pointer, 0, x - 39 + offsetx, y, -1.0, 1.0, 0.0, c_white, 1.0);

draw_set_color(c_black);
draw_set_font(global.FontStandard);
draw_set_halign(fa_right);
draw_set_valign(fa_middle);
draw_text(x + offsetx - 48, y, desc);
w = string_width(desc)+32;

