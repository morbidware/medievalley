event_inherited();

ds_map_add(grass,"wildgrass",60);
ds_map_add(grass,"porcinimushroom",2);
ds_map_add(grass,"mint",2);
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}

ds_map_add(plants,"pine",20);
ds_map_add(plants,"beechtree",30);
ds_map_add(plants,"olivetree",10);
ds_map_add(plants,"porcinimushroom",3);
ds_map_add(plants,"mint",3);

ds_map_add(boulders,"rock",1);
ds_map_add(boulders,"sapling",4);

ds_map_add(mobs,"wilddog",25);
ds_map_add(mobs,"wolf",25);
