{
    "id": "77353de0-85cc-40ee-a95c-82803bfbd713",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_weapons",
    "eventList": [
        {
            "id": "26febc05-b3a2-4b35-a315-28a7834ac9e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77353de0-85cc-40ee-a95c-82803bfbd713"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e5ee584-bbc9-416d-a603-fddcd5753ce9",
    "visible": true
}