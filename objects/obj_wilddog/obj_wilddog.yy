{
    "id": "09685dc7-51ef-499a-a9ea-bd6770c9b812",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wilddog",
    "eventList": [
        {
            "id": "3d75a287-b201-4888-b857-4990c56b6240",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "09685dc7-51ef-499a-a9ea-bd6770c9b812"
        },
        {
            "id": "5d313729-dd7c-4558-a373-1a04ab248494",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "09685dc7-51ef-499a-a9ea-bd6770c9b812"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8529da63-6d84-4441-af44-b7e18e765c4b",
    "visible": true
}