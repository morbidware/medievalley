/// @description Load audio
audio_group_load(audiogroup_music);
audio_group_load(audiogroup_sfx);
global.musicvolume = 1;
if (os_browser == browser_not_a_browser && m_muteMusicOnDebug)
{
	global.musicvolume = 0;
}
global.sfxvolume = 1;
audio_group_set_gain(audiogroup_music, global.musicvolume, 0);
audio_group_set_gain(audiogroup_sfx, global.sfxvolume, 0);