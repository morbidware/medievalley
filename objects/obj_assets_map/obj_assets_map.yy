{
    "id": "bb0d5b8c-d452-4c2d-a277-77b3c06fb631",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_assets_map",
    "eventList": [
        {
            "id": "143d3410-7e4a-4a62-a323-ce899c14c1b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb0d5b8c-d452-4c2d-a277-77b3c06fb631"
        },
        {
            "id": "d4a71035-3770-4486-9007-e76298a0cc58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bb0d5b8c-d452-4c2d-a277-77b3c06fb631"
        },
        {
            "id": "04c0696a-fbfc-427f-a382-1c70173bae56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "bb0d5b8c-d452-4c2d-a277-77b3c06fb631"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}