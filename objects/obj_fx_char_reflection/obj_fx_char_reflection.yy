{
    "id": "3e07dc7a-b96c-4429-a651-0f4be8a6ce4b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_char_reflection",
    "eventList": [
        {
            "id": "e65e2026-123b-402b-bfc5-328420e54d5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3e07dc7a-b96c-4429-a651-0f4be8a6ce4b"
        },
        {
            "id": "469bd9cf-e1fb-4d07-a5a5-48a7ef0b7ab8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e07dc7a-b96c-4429-a651-0f4be8a6ce4b"
        },
        {
            "id": "de1bae81-f315-4375-93f8-1e42ef2fb43e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3e07dc7a-b96c-4429-a651-0f4be8a6ce4b"
        },
        {
            "id": "a2271f3a-d612-4bb6-92da-2e71c107bf31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e07dc7a-b96c-4429-a651-0f4be8a6ce4b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}