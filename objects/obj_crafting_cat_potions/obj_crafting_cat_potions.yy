{
    "id": "3fa1613f-9f62-4309-9321-6745465058f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_potions",
    "eventList": [
        {
            "id": "34634e54-a44c-4643-9a2c-aceb26be25e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3fa1613f-9f62-4309-9321-6745465058f3"
        },
        {
            "id": "2180abe5-507b-40dd-bfb6-9f18e02489a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3fa1613f-9f62-4309-9321-6745465058f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bcca0029-513c-4907-bc27-032a7c731b79",
    "visible": true
}