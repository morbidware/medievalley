{
    "id": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_alchemytable",
    "eventList": [
        {
            "id": "dd7f6681-972c-4166-8bd9-8ba1c7bcae02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b"
        },
        {
            "id": "937b8f28-00b4-4e2a-9e88-125793944f8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b"
        },
        {
            "id": "63b7859d-a237-49d0-98ca-c2e1643d6520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b"
        },
        {
            "id": "63ff0293-e59d-49dd-86b2-7dae35e8e9ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b"
        },
        {
            "id": "09d7f498-51f6-4043-8468-bd353c0fd2c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b"
        },
        {
            "id": "c76a8bf8-9d92-4c69-988a-c2a96c732204",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "8b37a8c8-2f8a-4e34-9e7d-3aae25c0518b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5fc54534-4562-4a48-87bb-6d7248dd72ad",
    "visible": true
}