/// @description finish interaction
if (global.lastMouseButton == "right")
{
	repeat(10)
	{
		instance_create_depth(x,y,-y,obj_fx_cloudlet);
	}
	scr_dropItemQuantity(id,"stone",3);
	scr_dropItemQuantity(id,"woodlog",3);
	scr_dropItemQuantity(id,"ironnugget",1);
	instance_destroy();
}