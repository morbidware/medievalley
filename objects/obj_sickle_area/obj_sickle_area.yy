{
    "id": "1948187c-e8d1-479e-adba-0a66bf7eb45a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sickle_area",
    "eventList": [
        {
            "id": "93d8924e-85ca-4d95-a829-f8f317adb999",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1948187c-e8d1-479e-adba-0a66bf7eb45a"
        },
        {
            "id": "d3101dbd-d171-45c2-9453-3a958a2b06a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1948187c-e8d1-479e-adba-0a66bf7eb45a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "02fcd46b-39b7-4fc5-b482-3fc893fe455c",
    "visible": false
}