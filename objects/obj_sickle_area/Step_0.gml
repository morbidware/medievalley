/// @description Insert description here

depth = -y;
with(obj_interactable_wildgrass)
{
	if(other.hasCutSomething == false)
	{
		if(place_meeting(x,y,obj_sickle_area)) 
		{
			other.hasCutSomething = true;
			if (global.online)
			{
				eventPerformInteractable(x,y,id,object_index,ev_other,ev_user2);
			}
			event_perform(ev_other,ev_user2);
		}
	}
}