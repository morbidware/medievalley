{
    "id": "6bd88988-3d3c-4a4c-ae25-4c5a4ba346a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_wardrobe",
    "eventList": [
        {
            "id": "1b450756-1378-4d0e-b9e9-dbfcc68fadee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6bd88988-3d3c-4a4c-ae25-4c5a4ba346a9"
        },
        {
            "id": "0b7909ef-457c-46ae-a534-a5d209f3d398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6bd88988-3d3c-4a4c-ae25-4c5a4ba346a9"
        },
        {
            "id": "61fbac7e-3697-4695-84dd-4e7fc045e701",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "6bd88988-3d3c-4a4c-ae25-4c5a4ba346a9"
        },
        {
            "id": "d305e3e7-474d-4499-8f72-9cb43d6d9a9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6bd88988-3d3c-4a4c-ae25-4c5a4ba346a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61102b52-09c2-4bd0-a048-f4a30ec1b55a",
    "visible": true
}