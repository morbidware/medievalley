{
    "id": "717f30b4-aa65-455c-b6a1-c3770d2088ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_roadsign",
    "eventList": [
        {
            "id": "331dbedf-a13a-439b-85ff-0612ec382783",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "717f30b4-aa65-455c-b6a1-c3770d2088ce"
        },
        {
            "id": "c210d1f2-208a-46cf-8e1b-d64ea4a6dc78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "717f30b4-aa65-455c-b6a1-c3770d2088ce"
        },
        {
            "id": "7eb3707b-d5c4-4b99-9b81-97809163867a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "717f30b4-aa65-455c-b6a1-c3770d2088ce"
        },
        {
            "id": "daee6e16-c76c-4935-b018-ed0dd1760528",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "717f30b4-aa65-455c-b6a1-c3770d2088ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b345e203-7df0-440c-acc4-89d04157e9ac",
    "visible": true
}