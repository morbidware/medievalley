{
    "id": "21a8773d-a11d-4356-9f36-faf786a2d8da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_sugarbeet_plant",
    "eventList": [
        {
            "id": "c826c7c8-e461-453c-bcc7-22df9f48261f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "21a8773d-a11d-4356-9f36-faf786a2d8da"
        },
        {
            "id": "51767b6d-e05d-40a7-b210-f31e1a5be1f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "21a8773d-a11d-4356-9f36-faf786a2d8da"
        },
        {
            "id": "f6f30d48-11a2-4f6e-9e79-52ca4c1db995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "21a8773d-a11d-4356-9f36-faf786a2d8da"
        },
        {
            "id": "7d692251-f3a0-4fbf-865d-aa883a89ccd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "21a8773d-a11d-4356-9f36-faf786a2d8da"
        },
        {
            "id": "7e2ab477-369f-43b4-b066-64ce494ffbfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "21a8773d-a11d-4356-9f36-faf786a2d8da"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
    "visible": true
}