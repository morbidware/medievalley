{
    "id": "0a50228c-da52-48bb-aae1-c72cca747926",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_sleeping_z",
    "eventList": [
        {
            "id": "aadfadba-6db7-4295-aa5d-ab1ae7ae9c2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a50228c-da52-48bb-aae1-c72cca747926"
        },
        {
            "id": "d4bad385-6ce6-423c-b86b-0a4965d12e54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0a50228c-da52-48bb-aae1-c72cca747926"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "65903efa-44f9-43c4-93a3-85399dcedb78",
    "visible": true
}