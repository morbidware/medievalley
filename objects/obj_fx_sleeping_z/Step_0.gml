angle += 0.1 - (amplitude / 1000.0) * global.dt;
amplitude += 0.1 * global.dt;
if(angle > pi*2.0)
{
	angle -= pi*2.0;
}
x = original_x+cos(angle)*amplitude;
y -= 0.5 * global.dt;

if(amplitude < 3)
{
	if(image_alpha < 1.0)
	{
		image_alpha += 0.05 * global.dt;
	}
}
else
{
	if(image_alpha > 0.0)
	{
		image_alpha -= 0.02 * global.dt;
		if(image_alpha <= 0.0)
		{
			instance_destroy();
		}
	}
}
