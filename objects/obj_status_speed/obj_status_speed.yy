{
    "id": "1c17c375-8fe8-4ab0-b562-609ea2b57e7b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_status_speed",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a52de1de-4ab3-40a4-a337-03792d097632",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99de2647-e417-473b-b708-f5035e2f3a6e",
    "visible": true
}