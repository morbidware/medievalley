{
    "id": "e87e5cd9-f715-498f-88dd-5369f2c42f92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rural_corner_bl",
    "eventList": [
        {
            "id": "b5efb95d-5d25-4ab6-ba8c-ec1abc084608",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e87e5cd9-f715-498f-88dd-5369f2c42f92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "92e15c9b-b197-4392-83fb-90e6eba834a6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}