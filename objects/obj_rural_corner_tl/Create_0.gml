event_inherited();

keeporientation = true;
canmirrorx = false;
/*
ds_map_add(grass,"wildgrass",60);
ds_map_add(grass,"hypericum",2);
ds_map_add(grass,"lavender",2);
ds_map_add(grass,"echinacea",2);

ds_map_add(plants,"olivetree",30);
ds_map_add(plants,"beechtree",40);

ds_map_add(boulders,"rock",1);
ds_map_add(boulders,"rockiron",3);


ds_map_add(mobs,"wilddog",20);

*/
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",20);
}

ds_map_add(mobs,"nightwolf",100);

ds_map_add(boulders,"sapling",50);
ds_map_add(items,"flint",100);