{
    "id": "5a1a228b-6ab2-415a-a59e-fb25d7b52bfc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rural_corner_tl",
    "eventList": [
        {
            "id": "c2f8634b-2a47-4a6d-8c59-11d36b703420",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a1a228b-6ab2-415a-a59e-fb25d7b52bfc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "92e15c9b-b197-4392-83fb-90e6eba834a6",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}