/// @description Select one
var mx = mouse_get_relative_x();
var my = mouse_get_relative_y();
if (mx < x-16 || mx > x+16 || my < y-16 || my > y+16)
{
	return;
}
if (value <= 0)
{
	with (obj_merchant_panel)
	{
		outcometext = "Can't sell this item.";
		outcometextTimer = 180;
	}
	return;
}

quantitytomove = 1;
event_perform(ev_other,ev_user0);