draw_set_color(c_white);
draw_set_alpha(1);

scr_draw_sprite_enclosed(sprite_index, image_index, x, y, 32, 1);
if (quantity > 1)
{
	draw_set_font(global.FontNumbersOutline);
	draw_set_halign(fa_right);
	draw_set_valign(fa_middle);
	draw_text(x+14,y+10,scr_fix_text(string(quantity)));
}
else if(durability > 0)
{
	//DRAW DURABILITY
	//outline
	draw_set_color(make_color_rgb(60,40,10));
	draw_rectangle(x-12,y+8,x+12,y+13,false);
	//fill and reset
	var w = (durability/100)*20;
	draw_set_color(make_color_rgb(0,240,0));
	if(w < 15)
	{
		draw_set_color(make_color_rgb(120,240,0));
	}
	if(w < 10)
	{
		draw_set_color(make_color_rgb(240,120,0));
	}
	if(w < 5)
	{
		draw_set_color(make_color_rgb(240,0,0));
	}
	draw_rectangle(x-10,y+10,x-10+w,y+11,false);
	draw_set_color(c_white);
}
draw_set_color(c_white);

// Draw item name
var mx = mouse_get_relative_x();
var my = mouse_get_relative_y();
if (point_in_rectangle(mx, my, x-16, y-16, x+16, y+16))
{
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_set_valign(fa_middle);
	draw_set_halign(fa_center);
	if (status == 0 || status == 1)
	{
		if (value > 0)
		{
			draw_set_font(global.FontStandardBoldOutline);
			draw_text(x, y-34, itemname);
			draw_set_font(global.FontStandardOutline);
			draw_text(x, y-22, "Sell $" + string(floor(value*merchantpanel.sellpercent)));
		}
	}
	else
	{
		draw_set_font(global.FontStandardBoldOutline);
		draw_text(x, y-34, itemname);
		draw_set_font(global.FontStandardOutline);
		draw_text(x, y-22, "Buy $" + string(floor(value)));
	}
}