{
    "id": "2af9e62f-f660-4429-8b5d-60081e22dbb9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trade",
    "eventList": [
        {
            "id": "5543d03f-ddb2-4601-9377-cbe70ae62f09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        },
        {
            "id": "e48703c3-23e8-4ec2-8fe5-1d37dfdf3e05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        },
        {
            "id": "680a7e75-05fe-40e7-ae11-c1a0a6b34602",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        },
        {
            "id": "d6cc4a90-f483-486e-9df6-0b6f04471fef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        },
        {
            "id": "7597865c-8825-4f10-9aae-994eba0a3ad4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        },
        {
            "id": "5181db52-1ce3-40ab-b1e3-dc2d106b3981",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        },
        {
            "id": "3b52ef4d-a41a-4681-950b-beb397667f77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2af9e62f-f660-4429-8b5d-60081e22dbb9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}