/// @description Switch status
switch (status)
{
	case 0 : targetstatus = 1; break;
	case 1 : targetstatus = 0; break;
	case 2 : targetstatus = 3; break;
	case 3 : targetstatus = 2; break;
}

// ----------------- STATUS SWITCH

// Check if any identical item with same slot already has the target status, and update that
var found = false;
for (var i = 0; i < ds_list_size(merchantpanel.tradeObjects); i++)
{
	var item = ds_list_find_value(merchantpanel.tradeObjects, i);
	if (item <= 0 || item == noone || item == undefined)
	{
		continue;
	}
	if (item.status = targetstatus && item.itemid == itemid && item.quantity+quantitytomove <= item.maxquantity && item.originalslot == originalslot)
	{
		item.quantity += quantitytomove;
		quantity -= quantitytomove;
		found = true;
		break;
	}
}

// If no elegible item exists, create a new item with the target status
if (!found)
{
	// Check if the item can be added to the target status
	var canAdd = false;
	switch (status)
	{
		// In case of selling...
		case 0:
			if (merchantpanel.selling < merchantpanel.maxsell)
			{
				canAdd = true;
			}
			break;
		case 1: canAdd = true; break;
		// In case of buying...
		case 2:
			if (merchantpanel.buying < merchantpanel.maxbuy)
			{
				canAdd = true;
			}
			break;
		case 3: canAdd = true; break;
	}
	if (canAdd)
	{
		var newItem = instance_create_depth(-100,-100,depth,obj_trade);
		newItem.status = targetstatus;
		newItem.quantity = quantitytomove;
		newItem.maxquantity = maxquantity;
		newItem.itemid = itemid;
		newItem.itemname = scr_getGameItemValue(itemid,"name");
		newItem.value = value;
		newItem.originalslot = originalslot;
		newItem.durability = durability;
		newItem.sprite_index = sprite_index;
		ds_list_add(merchantpanel.tradeObjects,newItem);
		quantity -= quantitytomove;
	}
}

// Delete this entry if empty
if (quantity == 0)
{
	for(var i = 0; i < ds_list_size(merchantpanel.tradeObjects); i++)
	{
		if (ds_list_find_value(merchantpanel.tradeObjects,i) == id)
		{
			ds_list_delete(merchantpanel.tradeObjects,i);
			instance_destroy(id);
			return;
		}
	}
}