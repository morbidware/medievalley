{
    "id": "5678017b-4079-4435-bcd9-d354bdf5e08c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_particle_wood",
    "eventList": [
        {
            "id": "10b01737-6acd-4020-a1c9-ccd3541cb7e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5678017b-4079-4435-bcd9-d354bdf5e08c"
        },
        {
            "id": "d2218bfc-cfda-4f0a-bef2-9f76d7449681",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5678017b-4079-4435-bcd9-d354bdf5e08c"
        },
        {
            "id": "12a8b618-21c7-4795-b9d3-150581177a67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5678017b-4079-4435-bcd9-d354bdf5e08c"
        },
        {
            "id": "a015e865-2e21-4866-bfab-e2159d8fc383",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5678017b-4079-4435-bcd9-d354bdf5e08c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "41c22f9d-cfec-4e93-9435-aa381b4d4c75",
    "visible": true
}