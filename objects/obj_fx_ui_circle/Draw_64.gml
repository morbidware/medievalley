/// @description Insert description here
// You can write your code in this editor
//draw_set_color(c_yellow);
draw_set_color(c_white);
draw_set_alpha(a);
var angle = 0;
var angleoffset = (pi*2.0)/s;
for(var i = 0; i < s; i++)
{
	var ax = x + cos(angle+angleoffset*i)*r;
	var ay = y - sin(angle+angleoffset*i)*r;
	
	var bx = x + cos(angle+angleoffset*i+angleoffset)*r;
	var by = y - sin(angle+angleoffset*i+angleoffset)*r;
	
	draw_line_width(ax,ay,bx,by,(64-r)/10.0);
}
r += (64-r)*0.1;
a-=0.1;
draw_set_alpha(1.0);
//draw_set_color(c_white);

if(a <= 0)
{
	instance_destroy();
}