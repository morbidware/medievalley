/// @description can be interacted
interactionPrefixLeft = "";
interactionPrefixRight = "";
global.retval = false;

if(room == roomChallenge)
{
	exit;
}

if(growstage >= growstages)
{
	interactionPrefixLeft = "Pick";
	global.retval = true;
}
if(global.game.player.handState == HandState.Shovel &&
	global.game.player.highlightCol = floor(x/16) &&
	global.game.player.highlightRow = floor(y/16))
{
	interactionPrefixRight = "Remove";
	global.retval = true;
}