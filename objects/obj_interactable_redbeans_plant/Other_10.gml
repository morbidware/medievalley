/// @description finish interaction

// if shoveled, return the seed
if(global.game.player.handState == HandState.Shovel &&
	global.game.player.highlightCol = floor(x/16) &&
	global.game.player.highlightRow = floor(y/16) &&
	global.lastMouseButton == "right")
{
	scr_dropItems(id,string_replace(itemid,"plant","seeds"));
	global.stat_crops--;
	instance_destroy();
	return;
}

// harvest
else if (growstage >= growstages && global.lastMouseButton == "left")
{
	scr_dropItems(id,"redbeans","redbeans","redbeans");
	if(!isNet)
	{
		scr_getGoldOnline(ActorState.Pick);
	}
	isNet = false;
	growstage = 1;
	growcrono = 0;
	needswatering = 1;
}