{
    "id": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_redbeans_plant",
    "eventList": [
        {
            "id": "cfcd72ec-43ca-40fa-b6b9-33adb7cd1a9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761"
        },
        {
            "id": "8caac635-2230-4161-a5f3-0fb9f0d0522b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761"
        },
        {
            "id": "652d4f5b-7c36-4e42-b1ed-35fcdb033788",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761"
        },
        {
            "id": "f6eac6ba-93af-491f-b814-8eeae3287cce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761"
        },
        {
            "id": "296a2a40-07ee-49a5-b375-d5577e1e21d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761"
        },
        {
            "id": "942597fb-3be1-4380-9c35-4f7e8cf1ec83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "99f1fb2d-af8b-417f-8fc8-cdd7c0f96761"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
    "visible": true
}