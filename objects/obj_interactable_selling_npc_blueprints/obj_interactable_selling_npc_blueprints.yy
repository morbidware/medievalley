{
    "id": "f7a5455e-1129-4703-9b2b-2dff38fb13a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_selling_npc_blueprints",
    "eventList": [
        {
            "id": "0bd0ed8a-7b93-4718-a464-e9a568049754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "f7a5455e-1129-4703-9b2b-2dff38fb13a8"
        },
        {
            "id": "fefaa105-8cd7-4979-80cb-23bd1ab193bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7a5455e-1129-4703-9b2b-2dff38fb13a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "96fed520-bd73-4ff8-be08-104b556f20b4",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1cea9112-bec0-4cf1-967a-0a0c96f2aaf4",
    "visible": true
}