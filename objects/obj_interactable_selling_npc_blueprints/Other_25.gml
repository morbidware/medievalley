/// @description Custom setup
event_inherited();
/*
inv = ds_map_create();
ds_map_add(inv,"blueprint_house",99);
ds_map_add(inv,"blueprint_kitchen",99);
ds_map_add(inv,"blueprint_forge",99);
ds_map_add(inv,"blueprint_alchemytable",99);
ds_map_add(inv,"blueprint_grindstone",99);
*/

inv = ds_grid_create(2,5);
ds_grid_set(inv,0,0,"blueprint_house");
ds_grid_set(inv,1,0,1);

ds_grid_set(inv,0,1,"blueprint_kitchen");
ds_grid_set(inv,1,1,1);

ds_grid_set(inv,0,2,"blueprint_forge");
ds_grid_set(inv,1,2,1);

ds_grid_set(inv,0,3,"blueprint_alchemytable");
ds_grid_set(inv,1,3,1);

ds_grid_set(inv,0,4,"blueprint_grindstone");
ds_grid_set(inv,1,4,1);

ignoregroundlimits = true;

