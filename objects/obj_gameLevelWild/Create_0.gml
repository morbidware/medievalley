/// @description Init
// You can write your code in this editor
event_inherited();

// IMPORTANT: set seed for wildlands, so things will always stay the same across plays
random_set_seed(global.userid * (global.wildposx + global.wildposy));

wildground = instance_create_depth(0,0,0,obj_wildGround);
instance_create_depth(wildground.hspx,wildground.hspy,0,obj_fadein);
player = instance_create_depth(wildground.hspx+8,wildground.hspy,0,obj_char);
var cam = instance_create_depth(player.x,player.y,0,obj_camera);
scr_gameItemCreate(wildground.hspx,wildground.hspy, obj_interactable_roadsign, "roadsign");

audio_play_sound(snd_white_noise,0,true);

// set player start position
if (global.lastVisitedPlace == LastVisitedPlace.Farm)
{
	var height = wildground.rows * 16;
	player.x = 64;
	player.y = height / 2;
	player.lastdirection = Direction.Right;
}
else if (global.lastVisitedPlace == LastVisitedPlace.Wildlands)
{
	player.lastdirection = global.wildLastDirection;
	var width = wildground.cols * 16;
	var height = wildground.rows * 16;
	switch (global.wildLastDirection)
	{
		case Direction.Right:
			player.x = 64;
			player.y = height / 2;
			break;
		case Direction.Left:
			player.x = width - 64;
			player.y = height / 2;
			break;
		case Direction.Up:
			player.x = width / 2;
			player.y = height - 64;
			break;
		case Direction.Down:
			player.x = width / 2;
			player.y = 64;
			break;
	}
}
player.prevx = player.x;
player.prevy = player.y;
cam.x = player.x;
cam.y = player.y;

audio_play_sound(bgm_wild,0,true);