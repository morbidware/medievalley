{
    "id": "b638298d-e0ed-4c13-a827-2835b0b9777d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevelWild",
    "eventList": [
        {
            "id": "28e09b69-26f4-4e25-af43-754649acfe47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b638298d-e0ed-4c13-a827-2835b0b9777d"
        },
        {
            "id": "d7cc4c8e-3bbd-486f-bf4b-33a68db58156",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b638298d-e0ed-4c13-a827-2835b0b9777d"
        },
        {
            "id": "3bfb92c3-bea7-4762-8d75-120ec3b58f1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "b638298d-e0ed-4c13-a827-2835b0b9777d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}