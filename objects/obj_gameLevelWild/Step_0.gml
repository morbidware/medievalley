/// @description Change scene
// You can write your code in this editor
event_inherited();
if(instance_exists(obj_console)){exit;}

with(obj_char)
{
	other.x = x;
	other.y = y;
}

// Wild fast travel
if (os_browser == browser_not_a_browser)
{
	if (keyboard_check_pressed(vk_pageup))
	{
		other.changingRoom = true;
		global.wildposy--;
		global.wildLastDirection = Direction.Up;
		event_perform_object(obj_gameLevel, ev_other, ev_user1);
	}
	else if (keyboard_check_pressed(vk_pagedown))
	{
		other.changingRoom = true;
		global.wildposy++;
		global.wildLastDirection = Direction.Down;
		event_perform_object(obj_gameLevel, ev_other, ev_user1);
	}
	else if (keyboard_check_pressed(vk_delete))
	{
		other.changingRoom = true;
		global.wildposx--;
		global.wildLastDirection = Direction.Left;
		event_perform_object(obj_gameLevel, ev_other, ev_user1);
	}
	else if (keyboard_check_pressed(vk_end))
	{
		other.changingRoom = true;
		global.wildposx++;
		global.wildLastDirection = Direction.Right;
		event_perform_object(obj_gameLevel, ev_other, ev_user1);
	}
}

// Reload wild
/*
if (m_enableConsole && os_browser == browser_not_a_browser && keyboard_check_pressed(ord("T")))
{
	var f = instance_create_depth(0,0,0,obj_fadeout);
	room_set_width(roomWild, ((global.wildcellcolumns * 32) + (global.border * 2) + (global.clearance * 2)) * 16);
	room_set_height(roomWild, ((global.wildcellrows * 32) + (global.border * 2) + (global.clearance * 2)) * 16);
	f.gotoroom = roomWild;
}
*/

if (!changingRoom)
{
	with (obj_char)
	{
		// EXIT LEFT
		if (x < 32)
		{
			if (global.wildposx == 0)
			{
				other.changingRoom = true;
				event_perform_object(obj_gameLevel, ev_other, ev_user0);
			}
			else
			{
				other.changingRoom = true;
				global.wildposx--;
				global.wildLastDirection = Direction.Left;
				event_perform_object(obj_gameLevel, ev_other, ev_user1);
			}
		}
		// EXIT RIGHT
		else if (x > room_width-32)
		{
			other.changingRoom = true;
			global.wildposx++;
			global.wildLastDirection = Direction.Right;
			event_perform_object(obj_gameLevel, ev_other, ev_user1);
		}
		// EXIT UP
		else if (y < 32)
		{
			other.changingRoom = true;
			global.wildposy--;
			global.wildLastDirection = Direction.Up;
			event_perform_object(obj_gameLevel, ev_other, ev_user1);
		}
		// EXIT DOWN
		else if (y > room_height-32)
		{
			other.changingRoom = true;
			global.wildposy++;
			global.wildLastDirection = Direction.Down;
			event_perform_object(obj_gameLevel, ev_other, ev_user1);
		}
	}
}