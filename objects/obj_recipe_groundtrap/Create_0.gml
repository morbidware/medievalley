/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",1);
ds_map_add(ingredients,"flint",4);
ds_map_add(ingredients,"ironnugget",4);

resitemid = "groundtrap";
unlocked = true;
desc = "Use this to gather food without effort";