/// @description finish interaction
// You can write your code in this editor


if (global.lastMouseButton == "right")
{
	repeat(10)
	{
		instance_create_depth(x,y,-y,obj_fx_cloudlet);
	}
	
	var inv = instance_find(obj_playerInventory,0);
	for(var i = startingslot; i < startingslot + capacity; i++)
	{
		var v = ds_grid_get(inv.inventory,i,0);
		if(v != -1)
		{
			scr_dropItemQuantity(id,v.itemid,v.quantity);
			ds_grid_set(inv.inventory,i,0,-1);
		}
		
	}
	scr_dropItemQuantity(id,"woodplank",2);
	//COPY PASTE THE GRID REGION
	ds_grid_set_grid_region(inv.inventory, inv.inventory, startingslot+capacity, 0, ds_grid_width(inv.inventory)-1, 0, startingslot, 0);
	//ACTIVATE ALL CHESTS
	instance_activate_object(obj_interactable_chest);
	//CYCLE ALL CHESTS
	with(obj_interactable_chest)
	{
		logga("checking chest with starting slot " + string(startingslot));
		//IF NOT ME
		if(id != other.id)
		{
			//IF ITS STARTING SLOT IS GREATER THAN MINE
			if(startingslot > other.startingslot)
			{
				//DECREASE IT BY CAPACITY
				startingslot -= other.capacity;
				logga("startingslot corrected from "+string(startingslot+other.capacity)+" to " + string(startingslot));
			}
		}
	}
	//CYCLE ALL OBJ INVENTORY
	with(obj_inventory)
	{
		logga("checking obj_inventory with slot" + string(slot));
		//IF SLOT IS GREATER THAN MY STARTING SLOT
		if(slot >= other.startingslot+other.capacity)
		{
			//DECREASE BY CAPACITY
			slot -= other.capacity;
			logga("obj_inventory slot corrected from "+string(slot+other.capacity)+" to "+string(slot));
		}
	}
	//DECREASE TOTAL SLOTS
	global.totslots -= capacity;
	logga("global.totslots resized from "+string(global.totslots+capacity)+" to "+string(global.totslots));
	//RESIZE INVENTORY GRID
	ds_grid_resize(inv.inventory,ds_grid_width(inv.inventory)-capacity,ds_grid_height(inv.inventory));
	//DESTROY THIS CHEST
	instance_destroy();
}
else
{
	var chest_panel = instance_create_depth(0,0,0,obj_chest_panel);
	chest_panel.startingslot = startingslot;
}