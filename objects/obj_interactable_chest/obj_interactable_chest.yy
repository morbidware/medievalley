{
    "id": "d2cc09ea-28a8-4ada-8812-d67327da5f57",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_chest",
    "eventList": [
        {
            "id": "f93faf98-4c76-4da0-a19d-f23048c3503e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "d2cc09ea-28a8-4ada-8812-d67327da5f57"
        },
        {
            "id": "040f7db7-8eb4-4b01-99b3-f18285d01a39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d2cc09ea-28a8-4ada-8812-d67327da5f57"
        },
        {
            "id": "9302f682-470a-4b3f-8081-ac1e443e7995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "d2cc09ea-28a8-4ada-8812-d67327da5f57"
        },
        {
            "id": "e2a727d2-bf26-4852-a9a9-9c60d5c654b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "d2cc09ea-28a8-4ada-8812-d67327da5f57"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c651e1bf-cd87-4d24-9d71-05702123115c",
    "visible": true
}