/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"sapling",3);
ds_map_add(ingredients,"rope",1);
ds_map_add(ingredients,"ironnugget",1);

resitemid = "fishingrod";
unlocked = true;
desc = "Usable to fish food";