///@description Crate mission
event_inherited();

missionid = "test2";
title = "Castlevania";
shortdesc = "Dracula is drinking.";
longdesc = "What is a man? A miserable, little pile of secrets. But enough talk! Have at you!";
silent = true;

ds_map_add(required,"sapling",10);
ds_map_add(required,"stone",10);
ds_map_add(required,"redbeansseeds",10);