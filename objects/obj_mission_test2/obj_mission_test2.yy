{
    "id": "14ee7fae-c2cc-457f-9dea-d946f4dbab37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mission_test2",
    "eventList": [
        {
            "id": "e66a60d9-a3db-44a3-a551-b1c3b2d8b2a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "14ee7fae-c2cc-457f-9dea-d946f4dbab37"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8299d0fd-0188-4419-ba64-b8c0f3b131ce",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}