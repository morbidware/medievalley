{
    "id": "617f1119-34ab-428c-b1dc-21fa7a422547",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_hop_plant",
    "eventList": [
        {
            "id": "6492bec4-d523-4632-92c3-4d24fe6ce43b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "617f1119-34ab-428c-b1dc-21fa7a422547"
        },
        {
            "id": "71130b5d-4d6e-40bd-8795-2ef81d4c3310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "617f1119-34ab-428c-b1dc-21fa7a422547"
        },
        {
            "id": "f2da0514-193e-4c4a-9b52-d11c64e0528c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "617f1119-34ab-428c-b1dc-21fa7a422547"
        },
        {
            "id": "1a5c7bfd-88c7-445a-af86-27e9a19a0585",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "617f1119-34ab-428c-b1dc-21fa7a422547"
        },
        {
            "id": "4eb8a556-2abe-48b7-a881-73b25c8f6d39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "617f1119-34ab-428c-b1dc-21fa7a422547"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
    "visible": true
}