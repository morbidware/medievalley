/// @description Emit reward
with(obj_gameLevelFarm)
{
	if (!instance_exists(obj_interactable_farmboard))
	{
		logga("Creating farmboard");
		scr_gameItemCreate(8+(16*((global.farmCols/2)+3)), 8+(6*16), obj_interactable_farmboard, "farmboard");
	}
}
scr_addMail(4,0,true,120); // Mail about farms