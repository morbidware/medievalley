/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"food_olive",4);

resitemid = "oliveoil";
unlocked = true;
desc = "An easy, classic seasoning that goes well with most foods.";