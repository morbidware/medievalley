{
    "id": "ad88bcc7-b6b4-4d5c-965b-f0263c7cf065",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_groundtrap",
    "eventList": [
        {
            "id": "fcc526dd-a69e-4ec5-aa4a-4f4855b6e5e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad88bcc7-b6b4-4d5c-965b-f0263c7cf065"
        },
        {
            "id": "ff030b9a-a819-4c53-a8d3-d125019f7452",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ad88bcc7-b6b4-4d5c-965b-f0263c7cf065"
        },
        {
            "id": "3716b8cc-fa10-489b-be5b-7d9a301b0cbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ad88bcc7-b6b4-4d5c-965b-f0263c7cf065"
        },
        {
            "id": "93f0b849-604d-4b18-86f4-13ea5a6ef06e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "ad88bcc7-b6b4-4d5c-965b-f0263c7cf065"
        },
        {
            "id": "02c83bf5-7d87-4ffa-ac15-bcee3ab0b36f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad88bcc7-b6b4-4d5c-965b-f0263c7cf065"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98274654-1ffd-47da-a654-81df74d42bac",
    "visible": true
}