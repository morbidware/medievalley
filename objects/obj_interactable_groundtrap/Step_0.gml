/// @description Change shadow type
//event_inherited();
if (growstage == 11)
{
	shadowType = ShadowType.Sprite;
}
else
{
	shadowType = ShadowType.Square;
}
if(maxTrap == 0)
{
	nearEnemy = instance_nearest(x,y,obj_enemy);
}
else if(nearEnemy != noone && instance_exists(nearEnemy))
{
	nearEnemy.x = x;
	nearEnemy.y = y;
}
if(nearEnemy != noone && distance_to_object(nearEnemy)<3)
	{
		with(nearEnemy)
		{
			if(trapped == 0)
			{
				trapped = 1;
			
				scr_mobGetDamage(id,other.id);
			}
		}
		maxTrap = 1;
		sprite_index = spr_interactable_groundtrap_closed;
	}


	
