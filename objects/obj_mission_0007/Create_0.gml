///@description "Wild Hunt - Hunt down some animals.
event_inherited();

missionid = "0007";
title = "Wild Hunt";
shortdesc = "Hunt down some animals.";
longdesc = "Hunt down some animals in the Wild lands and collect 10 pieces of meat.";
silent = false;

ds_map_add(rewards,obj_reward_0007,1);
ds_map_add(required,"food_meat",5);

ds_list_clear(hintTexts);
ds_list_add(hintTexts, "It's time to go to the Wild lands.", "Using your spear, try to hunt down some animals.", "Collect 10 pieces of meat.");
ds_list_clear(hintImages);
ds_list_add(hintImages, noone, spr_pickable_spear, spr_pickable_meat);

scr_storeMissions();