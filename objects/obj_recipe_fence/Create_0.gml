/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();
if(!global.isSurvival)
{
	ds_map_add(ingredients,"sapling",4);
	ds_map_add(ingredients,"woodlog",2);
}
else
{
	ds_map_add(ingredients,"woodlog",2);
	ds_map_add(ingredients,"woodplank",2);
}


resitemid = "basefence";
unlocked = true;
desc = "Ahh.. safe place.";