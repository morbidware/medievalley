draw_set_alpha(1);

if (!mirrored)
{
	offsetx = 36;
	draw_sprite(spr_recipeinfo_bg, 0, x + offsetx, y + offsety);
	draw_sprite(spr_recipeinfo_pointer, 0, x + offsetx + 7, y + offsety + sprite_get_height(spr_recipeinfo_bg)/2);
}
else
{
	offsetx = -190;
	draw_sprite(spr_recipeinfo_bg, 0, x + offsetx, y + offsety);
	draw_sprite_ext(spr_recipeinfo_pointer, 0, x + offsetx + 167, y + offsety + sprite_get_height(spr_recipeinfo_bg)/2, -1, 1, 0, c_white, 1);
}

var size = 48;
var reqcount = ds_map_size(required);
var reqoffsetx = x + offsetx + (sprite_get_width(spr_recipeinfo_bg)/2) + 7 - (reqcount * (size/2)) + 18;

// print required items and completion status
var key = ds_map_find_first(required);
var maxheight = 0;
for (var i = 0; i < reqcount; i++)
{
	var quantity = ds_map_find_value(required,key);
	var quantitydone = 0;
	if (ds_map_exists(done,key))
	{
		quantitydone = ds_map_find_value(done,key);
	}
	
	var spr = noone;
	var name = "";
	if (key == "Special")
	{
		spr = spr_required_special;
		name = "Special";
	}
	else
	{
		name = scr_getGameItemValue(key,"name");
		spr = scr_asset_get_index("spr_pickable_"+scr_getGameItemValue(key,"spritename"));
		if (spr == spr_missing)
		{
			spr = scr_asset_get_index("spr_interactable_"+scr_getGameItemValue(key,"spritename"));
		}
	}
	
	scr_draw_sprite_enclosed(spr, sprite_get_number(spr)-1, reqoffsetx + (i * size), y + offsety + 36, 32, 1);
	
	draw_set_font(global.FontStandardOutline);
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	
	var nameheight = string_height_ext(name, 10, 32);
	if (nameheight > maxheight)
	{
		maxheight = nameheight;
	}
	draw_text_ext(reqoffsetx + (i * size), y + offsety + 56, name, 10, 32);
	
	draw_set_font(global.FontStandardOutline);
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	if (quantitydone < quantity)
	{
		draw_set_color(c_red);
	}
	draw_text(reqoffsetx + (i * size), y + offsety + 64 + nameheight, string(quantitydone)+"/"+string(quantity));
	key = ds_map_find_next(required,key);
}

draw_set_color(c_black);
draw_set_font(global.FontStandard);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
scr_draw_text_ext(x + offsetx + 20, y + offsety + 78 + maxheight, desc, 11, 130);

draw_set_color(c_white);