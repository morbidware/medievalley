/// @description Change category sprrite
var opencct = noone;
for(var i = 0; i < ds_list_size(cats); i++)
{
	var inst = ds_list_find_value(cats,i);
	if(inst.state == 1)
	{
		opencct = inst;
		break;
	}
}

if (opencct)
{
	var lastEntry = 0;
	for(var i = 0; i < ds_list_size(opencct.entries); i++)
	{
		var entry = ds_list_find_value(opencct.entries,i);
		if(entry.unlocked)
		{
			lastEntry = i+1;
		}
	}
	// Space both up and down
	if(opencct.from > 0 && opencct.from < lastEntry-6)
	{
		listObject.image_index = 0;
	}
	// No space up and down
	else if(opencct.from == 0 && opencct.from >= lastEntry-6)
	{
		listObject.image_index = 1;
	}
	// Space only up
	else if(opencct.from > 0 && opencct.from >= lastEntry-6)
	{
		listObject.image_index = 2;
	}
	// space only down
	else if(opencct.from == 0 && opencct.from < lastEntry-6)
	{
		listObject.image_index = 3;
	}	
}