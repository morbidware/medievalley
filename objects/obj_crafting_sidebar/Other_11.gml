/// @description Press down
var opencct = noone;
for(var i = 0; i < ds_list_size(cats); i++)
{
	var inst = ds_list_find_value(cats,i);
	if(inst.state == 1)
	{
		opencct = inst;
		break;
	}
}

if (opencct)
{
	var lastEntry = 0;
	for(var i = 0; i < ds_list_size(opencct.entries); i++)
	{
		var entry = ds_list_find_value(opencct.entries,i);
		if(entry.unlocked)
		{
			lastEntry = i+1;
		}
	}
	if(opencct.from < lastEntry-6)
	{
		opencct.from ++;
	}
}

event_perform(ev_other,ev_user2); 