{
    "id": "6815da87-b101-48bc-8fbb-b8474ae7f3f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_sidebar",
    "eventList": [
        {
            "id": "b4a1642e-c817-4ba9-8324-230331aee41e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        },
        {
            "id": "64579f83-43ac-4698-963f-19ba1ffa6ded",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        },
        {
            "id": "c5551233-45a6-47d6-ba12-4a9b46c0f04d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        },
        {
            "id": "9e4a53b8-09b1-4f56-b3d1-8310deee84e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        },
        {
            "id": "3c4522ee-7523-43e1-9016-b5897f7c4329",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        },
        {
            "id": "111708df-5f24-4e8f-b263-16b76397cd60",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        },
        {
            "id": "85d7f5ea-4dfa-4929-9b61-b6c5e103e028",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6815da87-b101-48bc-8fbb-b8474ae7f3f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "da65ad71-52fc-483c-8544-12794da102cd",
    "visible": true
}