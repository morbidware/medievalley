/// @description Press up
var opencct = noone;
for(var i = 0; i < ds_list_size(cats); i++)
{
	var inst = ds_list_find_value(cats,i);
	if(inst.state == 1)
	{
		opencct = inst;
		break;
	}
}

if (opencct)
{
	if(opencct.from > 0)
	{
		opencct.from --;
	}
}

event_perform(ev_other,ev_user2);