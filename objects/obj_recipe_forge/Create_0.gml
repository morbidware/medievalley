/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"stone",4);
ds_map_add(ingredients,"woodlog",2);
ds_map_add(ingredients,"ironnugget",8);

resitemid = "forge";
unlocked = true;
desc = "Essential for crafting strong iron tools, especially weapons.";