/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"potion_base",1);
ds_map_add(ingredients,"echinacea",5);
resitemid = "potion_strength";
unlocked = true;
desc = "This potion will make you stronger in combat for a short period of time.";