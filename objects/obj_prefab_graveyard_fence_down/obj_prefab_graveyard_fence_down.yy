{
    "id": "36c3b206-fa7c-4d00-8899-fbf08bcfd6a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_fence_down",
    "eventList": [
        {
            "id": "0ad9622d-8869-4c8b-9c3f-1cae18f06cc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36c3b206-fa7c-4d00-8899-fbf08bcfd6a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85b6680f-6763-47ce-a896-f70ceaceaf9d",
    "visible": true
}