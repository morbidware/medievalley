/// @description Open, close, input
if(keyboard_check_pressed(vk_lcontrol))
{
	instance_destroy();
	exit;
}

if(keyboard_check_pressed(vk_backspace))
{
	if(string_length(str)>0)
	{
		str = string_copy(str,1,string_length(str)-1);
		keyboard_string = "";
	}
	exit;
}

if(keyboard_check_pressed(vk_enter))
{
	event_perform(ev_other, ev_user0);
	exit;
}

if(keyboard_check_pressed(vk_up))
{
	str = lastCommand;
	exit;
}

if(keyboard_check_pressed(vk_anykey))
{
	if(string_length(keyboard_string)>0)
	{
		str += keyboard_string;
		keyboard_string = "";
	}
	exit;
}