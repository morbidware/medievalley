{
    "id": "6e38f617-852c-42df-bc94-be82693817c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_console",
    "eventList": [
        {
            "id": "f4656cd0-016f-4f7d-bc2b-de6094cc377d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "6e38f617-852c-42df-bc94-be82693817c4"
        },
        {
            "id": "df614f72-41e6-48f5-8bef-3f233eca7ea1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e38f617-852c-42df-bc94-be82693817c4"
        },
        {
            "id": "6af51e79-8e33-4c19-841e-ed6ea3facb37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e38f617-852c-42df-bc94-be82693817c4"
        },
        {
            "id": "8884bf5d-125b-4625-9b07-27f8aecc6e9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6e38f617-852c-42df-bc94-be82693817c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}