/// @description process cmd
show_debug_message(str);
if(string_length(str)>0)
{
	lastCommand = str;
}
var pla = instance_find(obj_char,0);
var args = ds_list_create();
var func = string_copy(str,1,string_pos(":",str)-1);
if(func == "")
{
	func = str;
}
str = string_delete(str,1,string_pos(":",str));

var del = ",";
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(args, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(args, str);

show_debug_message("function: "+func);
for(var i = 0; i < ds_list_size(args); i++)
{
	show_debug_message("arg "+string(i)+": "+ds_list_find_value(args,i));
}

switch (func)
{
	case "help":
		out = "create_p,itemname,quantity - complete_missions";
		break;
	
	case "create_p":
		scr_gameItemCreate(pla.x,pla.y,obj_pickable,ds_list_find_value(args,0),real(ds_list_find_value(args,1)));
		out = "create pickable/s " + string(ds_list_find_value(args,0)) + " " + string(ds_list_find_value(args,1));
		break;
		
	case "create_m":
		instance_create_depth(0,0,0,scr_asset_get_index("obj_mission_"+ds_list_find_value(args,0)));
		out = "create mission obj_mission_" + string(ds_list_find_value(args,0));
		break;
		
	case "complete_missions":
		with(obj_mission)
		{
			done = required;
			event_perform(ev_other,ev_user0);
		}
		out = "complete missions";
		break;
	case "complete_mission":
		with(instance_find(obj_mission,ds_list_find_value(args,0)))
		{
			done = required;
			event_perform(ev_other,ev_user0);
		}
		out = "complete mission";
		break;
	case "get_damage":
		scr_heroGetDamage(pla.id,real(ds_list_find_value(args,0)));
		out = "hero get damage " + string(ds_list_find_value(args,0));
		break;
	case "get_gold":
		global.gold += real(ds_list_find_value(args,0));
		with(obj_char)
		{
			gold = global.gold;
		}
		scr_updateRecipes();
		out = "hero get gold " + string(ds_list_find_value(args,0));
		break;
	case "fast_build":
		with(obj_interactable_house)
		{
			growcrono = 0;
			growstage = 2;
		}
		break;
	default:
		out = "COMMAND NOT FOUND";
		break;
}

str = "";
ds_list_destroy(args);