/// @description Sprite, exclamation, lifebar
//draw_text(x,y-40,string(state));
//draw_path(path,x,y,1);
if(flashCrono > 0)
{	
	shader_set(shaderFlash);
	if(flashCrono mod 2 == 0)
	{
		shader_set_uniform_f(shader_get_uniform(shaderFlash,"u_intensity"),1.0);
	}
	else
	{
		shader_set_uniform_f(shader_get_uniform(shaderFlash,"u_intensity"),0.0);
	}
}
else if(state == MobState.Charge || state == MobState.Attack)
{
	var r1 = 1.0;
	var g1 = 1.0;
	var b1 = 0.0;

	var r2 = 1.0;
	var g2 = 1.0;
	var b2 = 0.0;
	
	if(state == MobState.Attack)
	{
		g1 = 0.0;
		g2 = 0.0;
	}

	var bf = 1.0;

	shader_set(shaderOutline);

	shader_set_uniform_f(u_r,r1);
	shader_set_uniform_f(u_g,g1);
	shader_set_uniform_f(u_b,b1);

	shader_set_uniform_f(u_r2,r2);
	shader_set_uniform_f(u_g2,g2);
	shader_set_uniform_f(u_b2,b2);

	shader_set_uniform_f(u_bf,bf);
}
draw_self();

if(flashCrono > 0)
{
	shader_reset();
}
else if(state == MobState.Charge || state == MobState.Attack)
{
	shader_reset();
}
flashCrono --;

if (showexclamation)
{
	
	draw_sprite_ext(spr_enemy_exclamation_mark,0,x,y-uiheight-6,1,1,0,exclamationcolor,1);
}

if(targeted && state != MobState.Die)
{
	draw_set_alpha(1.0);
	var barsize = clamp(12 + floor(maxlife / 5), 12, 96);
	
	draw_set_color(c_black);
	draw_rectangle(x-(barsize/2)-1, y-uiheight-3, x+(barsize/2)+1, y-uiheight, false);
	
	draw_set_color(make_color_rgb(64,64,64));
	draw_rectangle(x-(barsize/2), y-uiheight-2, x+(barsize/2), y-uiheight-1, false);
	
	draw_set_color(c_red);
	draw_rectangle(x-(barsize/2), y-uiheight-2, x-(barsize/2) + round(barsize * (life/maxlife)), y-uiheight-1, false);
}