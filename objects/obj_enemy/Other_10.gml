/// @description Custom mob properties

// These are the properties that distinguish mobs the most
// In Children objects of this one, this event should be directly overwritten

/*
life = 10;				// Health
spd = 1;				// Movement speed
sightradius = 128;		// Radius for detecting the player
chaseradius = 96;		// Radius for start chasing the player or fleeing if ranged
attackradius = 16;		// Radius of attack
attackpause = 1;		// Seconds between each consecutive attack
damage = 10;			// Damage for each single attack
alerttime = 7;			// How many seconds the mob will wait before going from alert to an action state
chasetime = 15;			// How many seconds the mob will chase the player
pacific = false;		// If true, the mob will attack only when attacked
cautious = false;		// If true, the mob will stay at least 'chaseradius' far from the player
ranged = false;			// If true, the mob behavior better fits a ranged attack style
tool = noone;			// Tool used during attack	
lazy = 0.4;				// Probability of the mob to not move from its place when idling
maxflees = 3;			// Number of times a cautious mob will flee before dropping the cautious status
*/