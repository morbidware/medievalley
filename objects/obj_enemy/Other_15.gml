/// @description upd->Flee
// The mob flees away from the player.

crono -= global.dt;

// Find a spot where to flee and move there
alertcrono = 0;
scr_setMobTarget(fleeradius,true);
var dist = point_distance(x,y,targetx,targety);

// If crono is over or target is reached, change action
if(dist < spd || crono <= 0)
{
	targetx = -1;
	targety = -1;
	if (cautious || ranged)
	{
		
		scr_setMobState(id, MobState.Idle);
	}
	else
	{
		scr_setMobState(id, MobState.Chase);
	}
}
else
{
	var angle = degtorad(point_direction(x,y,targetx,targety));
	if(dist < spd)
	{
		spd = dist;
	}
	x += cos(angle) * (spd) * global.dt;
	y -= sin(angle) * (spd) * global.dt;
	scr_setMobSpriteAction(mobid,"run",lastdirection,-1);
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);