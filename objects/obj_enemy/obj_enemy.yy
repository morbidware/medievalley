{
    "id": "4637c718-e68d-4e37-b64d-0cc396872957",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "5e693118-4715-4876-ac4d-0fa25427cbbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "770b9e1f-8e90-4a70-88f6-5130b1f89a9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "77a7d4ea-212f-419c-a879-6fa5087785e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "6ef7f8a1-bc6a-4574-ac17-1cd3759e920b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "9d31a4fa-4a77-4733-92fb-5f5337747dff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "10478795-7137-4386-bb98-b06016e431b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "a4447ae4-ab81-4280-a0f3-17f2eea9da05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 15,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "8939c70b-de20-480a-97ab-b360f58c0995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "d48c1160-c1ad-4a9b-97ef-468297d2efb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "9578a52b-789f-4ff6-979a-98723f4bf473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "5dfc902d-c4d2-45ef-8db8-5f467b0e7576",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 19,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "3b3b18c7-d588-4259-83a9-7ff878e002df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "7874c61c-5d35-4f03-8295-03e70cdc9df6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "d59deef5-ece6-4d83-a021-7db81a3252dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "df36c570-c935-45f6-b024-5cb845b2166f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        },
        {
            "id": "9325d336-940b-4193-8cf1-74a19a516111",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 20,
            "eventtype": 7,
            "m_owner": "4637c718-e68d-4e37-b64d-0cc396872957"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}