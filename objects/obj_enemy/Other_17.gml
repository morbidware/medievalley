/// @description upd->Attack
// The mob attacks the player.

// note that attack radius for ranged mobs should be the maxdistance of their projectile
var dist = point_distance(x,y,hero.x,hero.y)
if (dist <= attackradius && crono >= attackTimer)
{
	if (!ranged)
	{
		if(hero != noone && hero.socketid == global.socketid && hero.state != ActorState.Hit && hero.state != ActorState.Die)
		{
			
			scr_heroGetDamage(hero, damage);

		}
		else
		{	
			sendDummyDataToOwner(damage,hero.socketid);
		}
	}
	else
	{
		if(hero != noone && hero.state != ActorState.Die)
		{
			// shoot the projectile, trying to approximtely anticipate the hero movement
			var arrow = instance_create_depth(x,y,depth,obj_projectile_arrow);
			attackwait = attackpause;
			//var hero_x = hero.x + (cos(degtorad(hero.currentdir)) * hero.currentspd * (dist/6));
			//var hero_y = hero.y - (sin(degtorad(hero.currentdir)) * hero.currentspd * (dist/6));
			var hero_x = hero.x;
			var hero_y = hero.y;
			arrow.dir = point_direction(x,y,hero_x,hero_y)+random_range(-15,15);
		}
		crono = 0;
	}
}

// ranged enemies' attack phase only lasts one frame
crono -= global.dt;
if (crono <= 0)
{
	attackwait = attackpause;
	if (ranged && point_distance(x,y,hero.x,hero.y) < chaseradius)
	{
		scr_setMobState(id, MobState.Flee);
	}
	else if (cautious)
	{
		scr_setMobState(id, MobState.Flee);
	}
	else
	{
		scr_setMobState(id, MobState.Chase);
		crono = afterattackTimer + global.dt;
	
		if (ranged)
		{
			alertcrono = alerttime;
		}
	}
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);