/// @description upd->Charge
// The mob charges its attack.

// create tool, if the mob has one
if (tool != noone && mobtool == noone)
{
	mobtool = instance_create_depth(x,y,depth,tool);
	mobtool.dir = lastdirection;
	mobtool.owner = id;
}
	
// at the end of the crono, destroy the tool and attack
crono -= global.dt;
if(crono <= 0)
{
	if (mobtool != noone)
	{
		instance_destroy(mobtool);
		mobtool = noone;
	}
	scr_setMobState(id, MobState.Attack);
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);