/// @description upd->RunAway


/*if(rx < camera_get_view_x(view_camera[0]) || rx > camera_get_view_x(view_camera[0]) && 
ry < camera_get_view_y(view_camera[0]) || ry > camera_get_view_y(view_camera[0]))
*/
if((x < hero.x - window_get_width()/2 || x > hero.x + window_get_width()/2) || 
(y < hero.y - window_get_width()/2 || y > hero.y + window_get_width()/2))
{
	instance_destroy(id);
}

if((quitx > hero.x - window_get_width()/2 && quity > hero.y - window_get_width()/2 && 
quitx < hero.x + window_get_width()/2 && quity < hero.y + window_get_width()/2) ||
(quitx == 0 || quity == 0))
{
	
	var survGround = instance_find(obj_survivalGround,0);
	if(survGround)
	{
		var angle = random(360);
		var condition = false
		while(condition != true)
		{
			var rx = hero.x + lengthdir_x(window_get_width(),angle);
			var ry = hero.y + lengthdir_y(window_get_width(),angle);
				
			var c = floor(rx/16);
			var r = floor(ry/16);
			c = clamp(c,0,global.survivalcellcolumns * 32);
			r = clamp(r,0,global.survivalcellrows * 32);
			rx = 8 + c * 16;
			ry = 8 + r * 16;
		
			if(c < global.survivalcellcolumns * 32 && r < global.survivalcellrows * 32 )
			{
				var groundtype = ds_grid_get(survGround.grid,round(c),round(r));
				var hasWall = ds_grid_get(survGround.wallsgrid,round(c),round(r));
				if(groundtype != GroundType.Water && groundtype != GroundType.Void && groundtype != GroundType.None)
				{
					if(hasWall == -1)
					{
						condition = true;	
					}
				}
				
			}
			angle += 10;
		}
		quitx = rx;
		quity = ry;
	}
}

var endx = path_get_x(path,1);
var endy = path_get_y(path,1);
show_debug_message(string(quitx) + " " + string(quity));
show_debug_message(string(endx) + " "+ string(endy));
pathresettimer += global.dt;
if(point_distance(x,y,endx,endy) > 60 || pathresettimer >= 60)
{
	path_end();
	if (path_exists(path))
	{
		path_clear_points(path);
		path_delete(path);
	}
	path = path_add();
	path_set_kind(path,0);
	var cell = mp_grid_get_cell(gr.pathgrid, floor(x/16.0), floor(y/16.0)) == -1;
	//logga("cell: " + string(cell));
	if(cell == 1)
	{
		logga("Could NOT exec mp_grid_path()");
		targetx = -1;
		targety = -1;
		scr_setMobState(id,MobState.Roam);
	}
	else
	{
		//logga("Could exec mp_grid_path()");
		mp_grid_path(gr.pathgrid, path, x, y, quitx, quity, 1);
		
		pathpos = 4.0 / path_get_length(path);
		
		if (pathresettimer >= 60)
		{
			pathresettimer = 0;
		}
		
	}
}

// If path is valid, navigate it
if (path_get_number(path) > 0)
{
	// Clear the nearest path spot
	spotx = 0;
	spoty = 0;
	
	// Get point on path and vector movement
	var px = path_get_x(path,pathpos);
	var py = path_get_y(path,pathpos);
	
	// Advance point on spline when reached
	if (point_distance(x,y,px,py) < 4)
	{
		pathpos += (spd+1) / path_get_length(path);
	}
	
	// Advance enemy towards path
	var angle = point_direction(x,y,px,py);
	x += cos(degtorad(angle)) * spd * global.dt;
	y -= sin(degtorad(angle)) * spd * global.dt;
	scr_setMobSpriteAction(mobid,"run",lastdirection,-1);
}
else
{
	//logga("path not valid");
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);
/*
// If path is not valid, follow the path spots left by the player
else
{
	// Clear path
	if (path_exists(path))
	{
		path_end();
		path_clear_points(path);
		path_delete(path);
	}
	
	// Find the nearest path spot to follow
	if (spotx <= 0 || spoty <= 0)
	{
		var maxdist = 99999;
		for (var i = 0; i < ds_list_size(hero.pathspots_x); i++)
		{
			var dist = point_distance(x,y,ds_list_find_value(hero.pathspots_x,i),ds_list_find_value(hero.pathspots_y,i))
			if (dist < maxdist)
			{
				maxdist = dist;
				spotx = ds_list_find_value(hero.pathspots_x,i);
				spoty = ds_list_find_value(hero.pathspots_y,i);
			}
		}
	}
	else
	// If a path spot is already present...
	{
		// If this mob reached the path spot, select the next point in the list
		var dist =point_distance(x,y,spotx,spoty);
		if (dist < 8)
		{
			for (var i = 0; i < ds_list_size(hero.pathspots_x); i++)
			{
				if (spotx == ds_list_find_value(hero.pathspots_x,i) && spoty == ds_list_find_value(hero.pathspots_y,i))
				{
					spotx = ds_list_find_value(hero.pathspots_x, clamp(i+1, 0, ds_list_size(hero.pathspots_x)-1));
					spoty = ds_list_find_value(hero.pathspots_y, clamp(i+1, 0, ds_list_size(hero.pathspots_y)-1));
				}
			}
		}
		
		var s = spd;
		if(dist < s)
		{
			s = dist;	
		}
		// Reach the current spot
		var angle = point_direction(x,y,spotx,spoty);
		x += cos(degtorad(angle)) * s * global.dt;
		y -= sin(degtorad(angle)) * s * global.dt;
		scr_setMobSpriteAction(mobid,"run",lastdirection,-1);
	}
}