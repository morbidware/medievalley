/// @description upd->Roam
// The mob slowly roams to another position.
crono -= global.dt;
scr_setMobTarget(64,false);
// Find a destination and move there
if(forcedx > 0 || forcedy > 0)
{
	ds_list_clear(targetpoints_x);
	ds_list_clear(targetpoints_y);
	ds_list_clear(invalidpoints_x);
	ds_list_clear(invalidpoints_y);
	targetx = forcedx;
	targety = forcedy;
	logga("forcedx:"+string(targetx)+" forcedy:"+string(targety));
}
var dist = point_distance(x,y,targetx,targety);
if((dist > spd/2.0 && crono > 0) || (dist > spd/2.0 && (forcedx > 0 || forcedy > 0)))
{
	var angle = degtorad(point_direction(x,y,targetx,targety));
	if(dist < spd/2.0)
	{
		spd = dist;
	}
	x += cos(angle) * (spd/2.0) * global.dt;
	y -= sin(angle) * (spd/2.0) * global.dt;
	scr_setMobSpriteAction(mobid,"walk",lastdirection,-1);
}
// If arrived, or crono is over, decide what to do
else
{
	targetx = -1;
	targety = -1;
	forcedx = -1;
	forcedy = -1;
	scr_setMobState(id, MobState.Idle);
}
