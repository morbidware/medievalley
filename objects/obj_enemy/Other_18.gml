/// @description upd->Suffer
// The mob gets damage.

// when hit, stop forced motion
forcedx = -1;
forcedy = -1;

// destory the mob tool, in case it was hit while attacking
if (mobtool != noone)
{
	instance_destroy(mobtool);
	mobtool = noone;
}

crono -= global.dt;
if(crono <= 0)
{
	if (pacific)
	{
		scr_setMobState(id, MobState.Idle);
	}
	else if (cautious)
	{
		scr_setMobState(id, MobState.Flee);
	}
	else
	{
		scr_setMobState(id, MobState.Flee);	
	}		
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);