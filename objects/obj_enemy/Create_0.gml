/// @description Initialization

// Base stuff
mobid = string_replace(object_get_name(object_index),"obj_","");
state = MobState.Idle;
player = instance_find(obj_char,0);
hero = player;
canBeDeactivated = true;

// Mob stats and behavior
targeted = false;
maxlife = 10;
life = 10;
damage = 10;
attackradius = 16;
sightradius = 180;
chaseradius = 150;
maxflees = 3;
flees = 0;
fleeradius = 48;
alerttime = 7;
chasetime = 15;
pacific = false;
cautious = false;
ranged = false;
tool = noone;
lazy = 0.4;
uiheight = 32;
fireFear = 0;
trapped = 0;
// Movement
spd = 1;
dir = 0;
storey = 0;
prevx = x;
prevy = y;
walking = false;
lastdirectiontimer = 0;
lastdirection = Direction.Right;
onstairs = false;
forcedx = -1;
forcedy = -1;

// Pathfinding
gr = instance_find(obj_baseGround,0);
path = path_add();
pathresettimer = 0;
pathpos = 0;
spotx = 0;
spoty = 0;
quitx = 0;
quity = 0;
cellstate = 0;
col = 0;
row = 0;
// Targeting
chasetimer = 0;
alertcrono = 0;
targetx = -1;
targety = -1;
attackwait = 0;

// Timers
idleTimer = 60;
roamTimer = 180;
chargeTimer = 30;
attackTimer = 20;
afterattackTimer = 60;
sufferTimer = 20;
dieTimer = 40;
fleeTimer = 80;
savedCrono = 0;
savedState = 0;
flashCrono = 0;
stuckTimer = 0;
crono = random(idleTimer);

// Exclamation mark (alert)
showexclamation = false;
exclamationcolor = c_yellow;

// Extra
offsetx = 0;
offsety = 0;
fg = instance_find(obj_baseGround,0);
hero = instance_find(obj_char,0);
mobtool = noone;
shadowType = ShadowType.Mob;
shadowBlobScale = 1;

// mobile workarounds
interactionPrefixLeft = "";
interactionPrefixRight = "";
collisionradius = 0; // only needed to make mobile auto targeting work... will always be 0

// Customize mob properties
event_perform(ev_other,ev_user0);

// Shader and sprite setup
image_speed = 0.2;
image_index = random(image_number);
var tex = sprite_get_uvs(sprite_index, image_index);
tex_left = tex[0];
tex_top = tex[1];
tex_right = tex[2];
tex_bottom = tex[3];

// debug
targetpoints_x = ds_list_create();
targetpoints_y = ds_list_create();
invalidpoints_x = ds_list_create();
invalidpoints_y = ds_list_create();

u_r = shader_get_uniform(shaderOutline,"u_r");
u_g = shader_get_uniform(shaderOutline,"u_g");
u_b = shader_get_uniform(shaderOutline,"u_b");

u_r2 = shader_get_uniform(shaderOutline,"u_r2");
u_g2 = shader_get_uniform(shaderOutline,"u_g2");
u_b2 = shader_get_uniform(shaderOutline,"u_b2");

u_bf = shader_get_uniform(shaderOutline,"u_bf");

// Animations
animSpeed = 0.4;
idleAnimSpeed = -1;
chaseAnimSpeed = -1;
afterAttackAnimSpeed = -1;
fleeAnimSpeed = -1;

