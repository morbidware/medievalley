/// @description upd->Idle
// The mob stays still, on the lookout.

crono -= global.dt;
if(crono <= 0)
{
	if(global.online && global.isSurvival && instance_find(id,0) != obj_nightwolf)
	{
		var dummy = instance_nearest(x,y,obj_dummy);
		if(distance_to_object(dummy)<sightradius && hero != dummy){
			if(distance_to_object(dummy)<distance_to_object(obj_char))
			{
				hero = dummy;	
			}
			else
			{
				hero = player;
				
			}
		}
		else
		{
			hero = player
		}
	}
	// Move to another place...
	if(random(1) > lazy)
	{
		scr_setMobState(id, MobState.Roam);
	}
	// ...or continue waiting
	else
	{
		crono = idleTimer;
	}
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);