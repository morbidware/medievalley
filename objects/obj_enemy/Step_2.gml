/// @description Detect direction

// Only update every n frames or if distance is high enough, to avoid ugly glitching
lastdirectiontimer--;
if (lastdirectiontimer > 0 || point_distance(prevx,prevy,x,y) < 2)
{
	exit;
}
lastdirectiontimer = 10; 

// if charging or attacking, lastdirection always points towards the player
var angle = 0;
if (state = MobState.Charge || state = MobState.Attack)
{
	angle = point_direction(x,y,hero.x,hero.y);
}
else
{
	angle = point_direction(prevx,prevy,x,y);
}
prevx = x;
prevy = y;

if (angle < 60 || angle > 300)
{
	lastdirection = Direction.Right;
}
else if (angle > 60 && angle < 120)
{
	lastdirection = Direction.Up;
}
else if (angle > 120 && angle < 240)
{
	lastdirection = Direction.Left;
}
else if (angle > 240)
{
	lastdirection = Direction.Down;
}