/// @description Debug draw
exit

// Mob status
draw_set_font(global.Font);
draw_set_color(c_white);
var dbgtxt = "None";
switch(state)
{
	case MobState.Idle: dbgtxt = "Idle " + string(crono); break;
	case MobState.Attack: dbgtxt = "Atk"; break;
	case MobState.Chase: dbgtxt = "Chase " + string(path_position); break;
	case MobState.Flee: dbgtxt = "Flee"; break;
	case MobState.Roam: dbgtxt = "Roam"; break;
	case MobState.Charge: dbgtxt = "Charge"; break;
	case MobState.Suffer: dbgtxt = "Suffer"; break;
	case MobState.Die: dbgtxt = "Die"; break;
}
draw_text(x-20,y-20,dbgtxt);

// Navigation path
if (path_exists(path))
{
	draw_set_color(c_black);
	var px = path_get_x(path,pathpos);
	var py = path_get_y(path,pathpos);
	draw_circle(px,py,3,false);
	draw_path(path,x,y,true);
}

// Roam and flee target
if (state == MobState.Roam || state == MobState.Flee)
{
	draw_set_color(c_white)
	draw_line(x,y,targetx,targety);
	draw_circle(targetx,targety,3,false);
}

// Target points
var i = 0;
repeat (ds_list_size(targetpoints_x))
{
	var px = ds_list_find_value(targetpoints_x,i);
	var py = ds_list_find_value(targetpoints_y,i);
	
	draw_set_color(c_white);
	draw_point(px,py);
	i++
}
i = 0;
repeat (ds_list_size(invalidpoints_x))
{
	var px = ds_list_find_value(invalidpoints_x,i);
	var py = ds_list_find_value(invalidpoints_y,i);
	
	draw_set_color(c_black);
	draw_point(px,py);
	i++
}
draw_set_color(c_white);

if(mask_index > -1)
{
	draw_sprite_ext(mask_index,0,x,y,1,1,0,c_red,0.5);
}
else
{
	draw_set_color(c_red);
	draw_set_alpha(0.5);
	draw_rectangle(bbox_left,bbox_top,bbox_right,bbox_bottom,false);
	draw_set_alpha(1.0);
}