/// @description upd->Die
// The mob dies and drops its loot.

// destory the mob tool, in case of death while attacking

if (mobtool != noone)
{
	instance_destroy(mobtool);
	mobtool = noone;
}

if (crono == dieTimer)
{
	if (path_exists(path))
	{
		path_clear_points(path);
		path_end();
		path_delete(path);
	}
}
if(crono > 0)
{
	crono -= global.dt;
	if(crono <= 0)
	{
		scr_progressMission(mobid);
		if (room != roomChallenge)
		{
			global.stat_kills++;
		}
		global.saveuserdatatimer = 0;
		instance_destroy();
	}
}
if(global.isSurvival)
{
	scr_storeMobsSurvival();	
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);