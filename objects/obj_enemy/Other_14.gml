/// @description upd->Chase
// The mob chases the player.
crono -= global.dt;
if(stuckTimer > 0){
	stuckTimer -= global.dt/60;
}

if (state = MobState.Chase && distance_to_object(hero) > chaseradius)
{
	showexclamation = false;
	scr_setMobState(id, MobState.Idle);
	chasetimer = 0;
	exit;
}
chasetimer += global.dt;

// Reset conditions
var do_clearpath = false;
var do_chase = false;
var do_idle = false;
var do_attack = false;

// If hero exists...
if(hero != noone)
{	
	// If hero is in range and alive...
	var distToHero = point_distance(x,y,hero.x,hero.y);
	if((distToHero < sightradius) && hero.life > 0)
	{
		// If hero is too far, start chase
		if(distToHero > attackradius)
		{
			do_chase = true;
		}
		// Otherwise attack
		else
		{
			
			if(crono < global.dt){
				do_clearpath = true;
				do_attack = true;
			}
			
		}
	}
	// Hero not in range or dead, stop here
	else
	{
		do_clearpath = true;
		do_idle = true;
	}
}
// Can't find hero, stop here
else
{
	do_clearpath = true;
	do_idle = true;
}

// Determine actions by conditions
if (do_clearpath)
{
	if (path_exists(path))
	{
		path_end();
		path_clear_points(path);
		path_delete(path);
	}
	path = path_add();
	pathpos = 0;
}

if (do_chase)
{
	// If player is very close and on same storey, but not close enough for attack, chase it in a straight line (if free of walls)
	var distToHero = point_distance(x,y,hero.x,hero.y);
	if (distToHero > attackradius && distToHero < attackradius+30 && hero.storey == storey && !collision_line(x,y,hero.x,hero.y,obj_wall,false,true))
	{
		
		var angle = point_direction(x,y,hero.x,hero.y);
		x += cos(degtorad(angle)) * spd * global.dt;
		y -= sin(degtorad(angle)) * spd * global.dt;
		scr_setMobSpriteAction(mobid,"run",lastdirection,-1);		
	}
	// If the player is still quite far, chase it via path or via spots
	else
	{
		
		// If player is too far from last point of the path or the timer is over, recalculate path
		var endx = path_get_x(path,1);
		var endy = path_get_y(path,1);

		pathresettimer += global.dt;
		if(point_distance(hero.x,hero.y,endx,endy) > 60 || pathresettimer >= 60)
		{
			stuckTimer = 0;
			
			if (path_exists(path))
			{
				path_end();
				path_clear_points(path);
				path_delete(path);
			}
			path = path_add();
			path_set_kind(path,0);
			
			var cell = mp_grid_get_cell(gr.pathgrid, floor(x/16), floor(y/16.0)) == -1;

			//logga("cell: " + string(cell));
			if(cell == 1)
			{
				//logga("Could NOT exec mp_grid_path()");
				targetx = -1;
				targety = -1;
				path_clear_points(path);
				scr_setMobState(id,MobState.Roam);
			}
			else
			{
				
				//logga("Could exec mp_grid_path()");
				
				var checkhero = mp_grid_get_cell(gr.pathgrid, floor(hero.x/16.0), floor(hero.y/16.0)) == -1;
				
				
				if(checkhero != 1)
				{
					
					checkhero = mp_grid_path(gr.pathgrid, path, 8+floor(x/16)*16, 8+floor(y/16)*16, 8+floor(hero.x/16)*16, 8+floor(hero.y/16)*16, 1);
					
					if(!checkhero)
					{			
						
						path_clear_points(path);
					}
					else
					{
						
						pathpos = 4.0 / path_get_length(path);
						if (pathresettimer >= 60)
						{
							pathresettimer = 0;
						}
						
					}
				}
				else
				{
					
					path_clear_points(path);			
				}
			}
		}
	
		// If path is valid, navigate it
		if (path_get_number(path) > 0)
		{
			
			//logga("path: "+ string(path_get_number(path)));
			//show_debug_message("path");
			// Clear the nearest path spot
			spotx = 0;
			spoty = 0;
			
			// Get point on path and vector movement
			var px = path_get_x(path,pathpos);
			var py = path_get_y(path,pathpos);

			// Advance point on spline when reached
			if (point_distance(x,y,px,py) < 4)
			{
				pathpos += (spd + 1) / path_get_length(path);
				stuckTimer = clamp(stuckTimer,0.1,pathpos);
			}
			
			var angle = point_direction(x,y,px,py);
			if(stuckTimer < 0)
			{
				var nearWall = instance_nearest(x,y,obj_interactable_basefence);
				if(nearWall)
				{
					angle = -point_direction(x,y,nearWall.x,nearWall.y);
					//stuckTimer = clamp(stuckTimer,0.1,pathpos);
				}
				
			}
			/*var c = floor(px);
			var r = floor(py);
			var ground = instance_find(obj_baseGround,0);
			if((c != col || r != row) && ground != noone)
			{
				if(!cellstate)
				{
					mp_grid_add_cell(ground.pathgrid,col,row);
					cellstate = true;
				}
				col = c;
				row = r;
				var v = mp_grid_get_cell(ground.pathgrid,col,row);
				//show_debug_message(string(v) + " " + string(col) + " " + string(row) + " " + string(ground.rows) + " " + string(ground.cols));
				if(v == -1)
				{
					cellstate = mp_grid_clear_cell(ground.pathgrid,col,row);
				}
			}*/
			/*	
			show_debug_message("Posizione path: "+string(pathpos));
			show_debug_message("Lunghezza path: "+string(path_get_length(path)));
			show_debug_message("Posizione prossimo punto del path: "+string(px) + " " +string(py));
			show_debug_message("Posizione player: "+string(hero.x) +" "+ string(hero.y));
			*/
			// Advance enemy towards path
			
			x += cos(degtorad(angle)) * spd * global.dt;
			y -= sin(degtorad(angle)) * spd * global.dt;
			scr_setMobSpriteAction(mobid,"run",lastdirection,-1);
		}
		// If path is not valid, follow the path spots left by the player
		else
		{
			
			//logga("not valid");
			// Clear path
			if (path_exists(path))
			{
				
				path_end();
				path_clear_points(path);
				path_delete(path);
			}
			path = path_add();
			pathpos = 0;
			//logga("pathspot: "+ds_list_size(hero.pathspots_x));
			// Find the nearest path spot to follow
			
			if (spotx <= 0 || spoty <= 0)
			{

				var maxdist = 99999;
				if(hero != obj_dummy)
				{
					for (var i = 0; i < ds_list_size(hero.pathspots_x); i++)
					{

						var dist = point_distance(x,y,ds_list_find_value(hero.pathspots_x,i),ds_list_find_value(hero.pathspots_y,i))
						if (dist < maxdist)
						{

							maxdist = dist;
							spotx = ds_list_find_value(hero.pathspots_x,i);
							spoty = ds_list_find_value(hero.pathspots_y,i);
						}
					}
				}
				

			}
			else
			// If a path spot is already present...
			{

				// If this mob reached the path spot, select the next point in the list
				var dist =point_distance(x,y,spotx,spoty);
				if (dist < 8)
				{
					
					for (var i = 0; i < ds_list_size(hero.pathspots_x); i++)
					{
						if (spotx == ds_list_find_value(hero.pathspots_x,i) && spoty == ds_list_find_value(hero.pathspots_y,i))
						{
							spotx = ds_list_find_value(hero.pathspots_x, clamp(i+1, 0, ds_list_size(hero.pathspots_x)-1));
							spoty = ds_list_find_value(hero.pathspots_y, clamp(i+1, 0, ds_list_size(hero.pathspots_y)-1));
						}
					}
					
				}
				
				var s = spd;
				if(dist < s)
				{
					s = dist;	
				}
				// Reach the current spot
				var angle = point_direction(x,y,spotx,spoty);
				x += cos(degtorad(angle)) * s * global.dt;
				y -= sin(degtorad(angle)) * s * global.dt;
				scr_setMobSpriteAction(mobid,"run",lastdirection,-1);
				
			}
		}
	}
	
}

if (do_idle)
{
	scr_setMobState(id, MobState.Idle);
}

if (do_attack)
{
	scr_setMobState(id, MobState.Charge);
}
//sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);