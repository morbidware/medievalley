/// @description Depth, collisions, alert, state
scr_setDepth(DepthType.Elements);


// -------- DETECT STOREY

// Read storey from grid (clamp needed otherwise it will break when walking outside play area)
storey = ds_grid_get(fg.storeygrid, clamp(floor(x/16),0,fg.cols-1), clamp(floor(y/16),0,fg.rows-1));

// stop any action if challenge is not running
if(room == roomChallenge)
{
	var gl = instance_find(obj_gameLevelChallenge,0);
	if(gl.challengeState != 1)
	{
		exit;
	}
}

// Force mob to move to a target, avoiding all other actions
if(forcedx > 0 || forcedy > 0)
{
	if(state == MobState.Idle)
	{
		scr_setMobState(id, MobState.Roam);
		targetx = forcedx;
		targety = forcedy;
	}
}

// -------- ALERT MANAGEMENT
with(obj_nightwolf)
{
	if(!trapped && state != MobState.RunAway && state != MobState.Die && global.dayprogress >0 && global.dayprogress <0.86)
	{
		var survGround = instance_find(obj_survivalGround,0);
		if(survGround)
		{
			var angle = random(360);
			var condition = false
			while(condition != true)
			{
				var rx = hero.x + lengthdir_x(window_get_width()/2,angle);
				var ry = hero.y + lengthdir_y(window_get_width()/2,angle);
					
				var c = floor(rx/16);
				var r = floor(ry/16);
				c = clamp(c,0,global.survivalcellcolumns * 32);
				r = clamp(r,0,global.survivalcellrows * 32);
				rx = 8 + c * 16;
				ry = 8 + r * 16;
			
				if(c < global.survivalcellcolumns * 32 && r < global.survivalcellrows * 32 )
				{
					var groundtype = ds_grid_get(survGround.grid,round(c),round(r));
					var hasWall = ds_grid_get(survGround.wallsgrid,round(c),round(r));
					if(groundtype != GroundType.Water && groundtype != GroundType.Void && groundtype != GroundType.None)
					{
						if(hasWall == -1)
						{
							condition = true;	
						}
					}
					
				}
				angle += 10;
			}
			targetx = rx;
			targetx = ry;
			scr_setMobState(id, MobState.RunAway);
		}
	}
}
if(trapped && state != MobState.Suffer && state != MobState.Die)
{
	scr_setMobState(id, MobState.Idle);
}
// If idling or roaming, detect player and react accordingly
attackwait -= global.dt / 60;
if (hero == noone)
{
	showexclamation = false;
	hero = instance_find(obj_char,0);
}
else if (forcedx > 0 || forcedy > 0)
{
	showexclamation = false;
}
else if (hero.life > 0 && hero.storey == storey && (state == MobState.Idle || state == MobState.Roam))
{
	var dist = point_distance(x,y,hero.x,hero.y);
	
	// If the player is very near and on the same storey...
	if (dist < chaseradius)
	{
		if (!pacific)
		{
			showexclamation = true;
			exclamationcolor = c_red;
		}
		if (cautious || ranged)
		{
			if (attackwait > 0 || (flees > 0 && cautious))
			{
				scr_setMobState(id, MobState.Flee);
				if (flees > 0)
				{
					flees--;
				}
				else
				{
					cautious = false;
				}
			}
			else
			{
				
				if(attackwait <= 0)scr_setMobState(id, MobState.Charge);
			}
		}
		else if (!pacific && attackwait <= 0 && distance_to_object(hero) < chaseradius)
		{
			scr_setMobState(id, MobState.Chase);
		}
	}
	// If the player is not so near, but at least inside the alert area...
	else if (dist < sightradius)
	{
		if (!pacific)
		{
			showexclamation = true;
			exclamationcolor = c_yellow;
		}
		alertcrono += global.dt / 60;
		if (alertcrono > alerttime)
		{
			if (!pacific)
			{
				exclamationcolor = c_red;
			}
			if (cautious)
			{
				if (flees > 0)
				{
					scr_setMobState(id, MobState.Flee);
					flees--;
				}
				else
				{
					cautious = false;
				}
			}
			else if (!pacific && attackwait <= 0)
			{
				if (ranged)
				{
					scr_setMobState(id, MobState.Charge);
				}
				else if(distance_to_object(hero) < chaseradius)
				{
					scr_setMobState(id, MobState.Chase);
				}
			}
		}
	}
	// Reduce alert if player is outside sight radius
	else
	{
		alertcrono -= global.dt / 60;
		showexclamation = false;
	}
	alertcrono = clamp(alertcrono, 0, alerttime);
}
else if (hero.life <= 0 || (hero.storey != storey && state != MobState.Chase && state != MobState.Attack))
{
	showexclamation = false;
}
if (fireFear && state != MobState.RunAway)
{
	with(obj_interactable_bonfire)
	{	
		var distToMob = distance_to_object(other.id);
		var dirToMob = point_direction(x,y,other.x,other.y);
		var angle = degtorad(dirToMob);
		if(distToMob < 100 && distance_to_object(other.hero) < 50)
		{
			other.showexclamation = false;
			other.targetx = x + lengthdir_x(distToMob,angle);
			other.targetx = y + lengthdir_y(distToMob,angle);
			scr_setMobState(other.id, MobState.Flee);
			other.chasetimer = 0;
		}
		else if(distToMob > 100 && distance_to_object(other.hero) <= 100)
		{
			other.showexclamation = false;
			scr_setMobState(other.id, MobState.Idle);
			other.chasetimer = 0;
		}
	}
	with(hero.firefly)
	{
		var distToMob = distance_to_object(other.id);
		var dirToMob = point_direction(x,y,other.x,other.y);
		var angle = degtorad(dirToMob);
		if(distToMob < 100 && distance_to_object(other.hero) < 50)
		{
			other.showexclamation = false;
			other.targetx = x + lengthdir_x(distToMob,angle);
			other.targetx = y + lengthdir_y(distToMob,angle);
			scr_setMobState(other.id, MobState.Flee);
			other.chasetimer = 0;
		}
		else if(distToMob > 100 && distance_to_object(other.hero) <= 100)
		{
			other.showexclamation = false;
			scr_setMobState(other.id, MobState.Idle);
			other.chasetimer = 0;
		}
	}
}
// -------- AVOIDANCE

// Avoid other enemies
with(obj_enemy)
{
	if(id != other.id)
	{
		var pAx = other.x;
		var pAy = other.y;
		var pBx = x
		var pBy = y;
		var dist = point_distance(pAx, pAy, pBx, pBy);
		if(dist < 8)
		{
			var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
			var off = abs(dist - 12) * 0.6;
			other.x += cos(angle+pi)*off;
			other.y -= sin(angle+pi)*off;
		}
	}
}

// Avoid obstacles with a collision radius
with(instance_nearest(x,y,obj_interactable))
{
	if (collisionradius > 0)
	{
		var pAx = other.x;
		var pAy = other.y;
		var pBx = x;
		var pBy = y;
		var dist = point_distance(pAx, pAy, pBx, pBy);
		if(dist < (collisionradius))
		{
			var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
			var off = abs(dist - collisionradius) * 0.6;
			other.x += cos(angle+pi)*off;
			other.y -= sin(angle+pi)*off;
		}
	}
}



scr_wallCollisions(id,-1,-1);
scr_updateMobState(id);


/*var c = floor((x-8)/16);
var r = floor((y-8)/16);
var ground = instance_find(obj_baseGround,0);
if((c != col || r != row) && ground != noone)
{
	if(!cellstate)
	{
		mp_grid_add_cell(ground.pathgrid,col,row);
		cellstate = true;
	}
	col = c;
	row = r;
	var v = mp_grid_get_cell(ground.pathgrid,col,row);
	//show_debug_message(string(v) + " " + string(col) + " " + string(row) + " " + string(ground.rows) + " " + string(ground.cols));
	if(v == -1)
	{
		cellstate = mp_grid_clear_cell(ground.pathgrid,col,row);
	}
}*/

if(global.online)sendMobData(x,y,sprite_index,image_index,image_xscale,id,global.socketid,life,state,targetx,targety);