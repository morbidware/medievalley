/// @description Custom mob behavior
maxlife = 50;
life = 50;				// Health
spd = 0.8;				// Movement speed
sightradius = 100;		// Radius for detecting the player
chaseradius = 60;		// Radius for start chasing the player or fleeing if ranged
attackradius = 16;		// Radius of attack
attackpause = 2;		// Seconds between each consecutive attack
damage = 25;			// Damage for each single attack
alerttime = 4;			// How many seconds the mob will wait before going from alert to an action state
chasetime = 4;			// How many seconds the mob will chase the player
pacific = false;		// If true, the mob will attack only when attacked
cautious = false;		// If true, the mob will stay at least 'chaseradius' far from the player
ranged = false;			// if true, the mob behavior better fits a ranged attack style
lazy = 0.6;				// Probability of the mob to not move from its place when idling

attackTimer = 20
chargeTimer = 25