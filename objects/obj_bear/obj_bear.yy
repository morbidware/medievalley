{
    "id": "211df24a-2cf8-4328-9b39-f3ffba1ac57e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bear",
    "eventList": [
        {
            "id": "8f5acf45-e1a4-4f43-ba4c-a95e3d90b7db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "211df24a-2cf8-4328-9b39-f3ffba1ac57e"
        },
        {
            "id": "6a6946a1-04a4-409a-a7db-38aba4fccb7b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "211df24a-2cf8-4328-9b39-f3ffba1ac57e"
        }
    ],
    "maskSpriteId": "fe96c7e8-322c-44fa-86bf-6a83278ae13d",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
    "visible": true
}