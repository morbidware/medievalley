{
    "id": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_basefencedoor",
    "eventList": [
        {
            "id": "52725257-86bb-4e49-b56e-fd2d43b041e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "b2a9e741-f2a4-4f25-a513-c69761606e02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "e24df70f-faff-47aa-8463-464d311e5fe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "d17b8421-a26d-48ab-8aa6-b4072a2e18df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "2e747c30-fea2-4f23-9e1b-8cb98a3a0c3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "50c9a830-8d0f-4af5-95f5-b6a51f4f732d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "f3c25c11-80a7-49a0-a234-1a14e6c00378",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        },
        {
            "id": "f4bb0781-f09b-4110-a6ab-7f67de8fd44a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "d9c194f0-1b16-4bdd-bcb0-64496cc3b4d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb26fb67-4526-41d8-beab-57209fa1246e",
    "visible": true
}