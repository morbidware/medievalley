/// @description Can be interacted

var handState = global.game.player.handState;
if(handState == HandState.Axe)
{
	global.retval = true;
	interactionPrefixRight = "Chop";
}
else
{
	interactionPrefixRight = ""
	global.retval = false;
}

if(isOpen == false)
{
	interactionPrefixLeft = "Open";
	global.retval = true;
}
else
{
	interactionPrefixLeft = "Close";
	global.retval = true;
}