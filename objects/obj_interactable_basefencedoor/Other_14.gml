/// @description Open and Close
// You can write your code in this editor
if(!isOpen)
{
	with (obj_baseGround)
	{
		ds_grid_set(itemsgrid,other.col,other.row,-1);
		ds_grid_set(wallsgrid,other.col,other.row,-1);	
	}
	instance_destroy(mywall);
	
	sprite_index = spr_interactable_basefencedoor_open;
	isOpen = true;
	interactionPrefixLeft = "Close";
}
else
{
	sprite_index = spr_interactable_basefencedoor_closed;
	mywall = instance_create_depth(x,y,depth,obj_wall_invisible);
	mywall.storey_0 = true;
	mywall.storey_1 = true;
	mywall.storey_2 = true;
	mywall.storey_3 = true;
	ds_grid_set(instance_find(obj_baseGround,0).wallsgrid, col, row, mywall);
	
	isOpen = false;
	interactionPrefixLeft = "Open";
}