/// @description Finish interaction
if (!isNet)
{
	scr_dropItems(id,"basefencedoor");
}

with (obj_baseGround)
{
	ds_grid_set(itemsgrid,other.col,other.row,-1);
	ds_grid_set(wallsgrid,other.col,other.row,-1);	
}
instance_destroy(mywall);

event_perform(ev_other,ev_user3);
instance_destroy();