/// @description Init
event_inherited();

scr_setDepth(DepthType.Elements);
col = floor(x/16);
row = floor(y/16);
life = lifeperstage;
//growcrono = -1;

mywall = instance_create_depth(x,y,depth,obj_wall_invisible);
mywall.storey_0 = true;
mywall.storey_1 = true;
mywall.storey_2 = true;
mywall.storey_3 = true;
ds_grid_set(instance_find(obj_baseGround,0).wallsgrid, col, row, mywall);

shadowLength = 12;

isOpen = false;


