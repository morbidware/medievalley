/// @description Draw and highlight shader

if(highlight)
{
    shader_set(shaderHighlight);
    /*
    var UV = sprite_get_uvs(sprite_index, 0);
    shader_set_uniform_f(shader_get_uniform(shaderHighlight, "UV"), UV[0], UV[1], UV[2], UV[3]);
    show_debug_message("left: " + string(UV[0]) + " top: " + string(UV[1]) + " right: " + string(UV[2]) + " bottom: " + string(UV[3]));
    */
}

draw_self();

if(highlight)
{
    shader_reset();
}
/*
var pla = instance_find(obj_char,0);
var d = point_distance(x,y,pla.x,pla.y);
if(d < 64)
{
    draw_set_font(global.Font);
    draw_set_halign(fa_center);
    draw_set_valign(fa_bottom);
    draw_text(x,y-8,name);
}
*/