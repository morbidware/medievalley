{
    "id": "dbf0b3d5-f74c-41c0-a2f1-fd210088b91b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pickable",
    "eventList": [
        {
            "id": "a7324837-1c3e-4f9b-9338-52c323c5f19d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dbf0b3d5-f74c-41c0-a2f1-fd210088b91b"
        },
        {
            "id": "08c55e88-1a10-4bb4-a7d4-6f4a5a7750ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "dbf0b3d5-f74c-41c0-a2f1-fd210088b91b"
        },
        {
            "id": "40496696-abdd-4639-9096-970962ba1e4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dbf0b3d5-f74c-41c0-a2f1-fd210088b91b"
        },
        {
            "id": "7569a5b5-d2e8-4947-9182-6b2b8fa04db1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dbf0b3d5-f74c-41c0-a2f1-fd210088b91b"
        },
        {
            "id": "0c8eef52-1fc6-4a07-8445-8074e04f76d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "dbf0b3d5-f74c-41c0-a2f1-fd210088b91b"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "cf8deb36-79b4-4043-940c-ca9d662edda0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "visible": true
}