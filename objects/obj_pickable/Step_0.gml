/// @description Repulsion, magnet, highlight

// Inherit the parent event
event_inherited();

if(sprite_index == spr_null)
{
    //show_debug_message("spr_pickable_"+spritename);
    sprite_index = scr_asset_get_index("spr_pickable_"+spritename);
}

if (magnetcrono > 0)
{
	magnetcrono -= global.dt;
}

// move towards player if he is not doing anything
if (!game.firstday)
{
	if(state == 0)
	{
		var player = instance_find(obj_char,0);
		if(player.state != ActorState.Die && player.state != ActorState.Hit && !player.stopmagnet)
		{
			var pAx = player.x;
			var pAy = player.y;
			var pBx = x;
			var pBy = y;

			var dist = point_distance(pAx, pAy, pBx, pBy);
			
			if(dist < magnetdistance && !dropped && global.otheruserid == 0)
			{
				//logga("dist: "+string(dist));
				if (scr_canAddToInventory(id) && magnetcrono <= 0)
				{
					state = 1;
					magnettarget = player;
					magnettargetsocket = player.socketid;
					magnetPickable(x,y,itemid,magnettargetsocket);
				}
			}
				
			if (dist > magnetdistance && dropped)
			{
				dropped = false;
			}
		}
	}
	else if(state == 1)
	{
		if(magnetspeed < maxspeed)
		{
			magnetspeed += 0.1*global.dt;
		}
		var player = instance_find(magnettarget,0);
		var pAx = player.x;
		var pAy = player.y;
		var pBx = x;
		var pBy = y;
		var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
		var dist = point_distance(pAx, pAy, pBx, pBy);
		if(magnetspeed > dist)
		{
			magnetspeed = dist;
		}
		x += cos(angle+pi)*magnetspeed;
		y -= sin(angle+pi)*magnetspeed;
		image_angle += cos(angle+pi)*magnetspeed;
		if (dist < 3)
		{
			if(magnettarget == instance_find(obj_char,0))
			{
				var s = audio_play_sound(snd_magnet_pick_up,0,false);
				audio_sound_pitch(s, 0.85 + random (0.35));
				
				scr_putItemIntoInventory(id,InventoryType.Carry,-1);	
				if (isDeathPickable)
				{
					for (var i = 0; i < ds_grid_width(global.lastdeathinventory); i++)
					{
						var _id = ds_grid_get(global.lastdeathinventory,i,0);
						var _quantity = ds_grid_get(global.lastdeathinventory,i,1);
						var _durability = ds_grid_get(global.lastdeathinventory,i,2);
						
						if (itemid == _id && round(quantity) == round(_quantity) && round(durability) == round(_durability))
						{
							ds_grid_set(global.lastdeathinventory,i,0,noone);
						}
					}
				}
			}
			
			instance_destroy(id);
		}
	}
}

// repulsion against other objects
if (repulsion)
{
	var found = false;
	with(obj_gameItem)
	{    
		var pAx = other.x;
		var pAy = other.y;
		var pBx = x;
		var pBy = y;

		if (other.id != id && (kind == GameItemKind.Pickable || collisionradius > 0))
		{
		    var dist = point_distance(pAx, pAy, pBx, pBy);
		    if(dist < 10)
		    {
		        var angle = degtorad(point_direction(pAx,pAy,pBx,pBy));
		        var off = abs(dist - 10) * 0.1 * global.dt;
		        other.x += cos(angle+pi)*off;
		        other.y -= sin(angle+pi)*off;
				if (off < 0.15)
				{
					found = false;
				}
				else
				{
					found = true;
				}
		    }
		}
	}
	if (!found)
	{
		repulsion = false;
	}
}

scr_setDepth(DepthType.Elements);