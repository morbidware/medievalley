/// @description Init
// You can write your code in this editor

// Inherit the parent event
event_inherited();

highlight = false;
repulsion = true;
magnetdistance = 32;
if(m_megaMagnet)
{
	magnetdistance = 100000;
}
magnetspeed = 0;
magnetcrono = 30;
magnettargetsocket = "";
magnettarget = noone;
maxspeed = 24;
canpick = false;
dropped = false;
mx = 0;
my = 0;

kind = GameItemKind.Pickable;
player = instance_find(obj_char,0);
game = instance_find(obj_gameLevel,0);
isDeathPickable = false;