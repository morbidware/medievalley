/// @description Custom appearance
// image_index doesn't depend by growstage but by life
if (life > floor(lifeperstage / 2))
{
	image_index = 1;
}
else
{
	image_index = 0;
}
event_inherited();

//draw_text(x,y,string(life));