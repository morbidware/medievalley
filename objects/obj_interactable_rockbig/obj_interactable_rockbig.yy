{
    "id": "24e1aa03-6b8e-447b-8244-83388116b0df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_rockbig",
    "eventList": [
        {
            "id": "867b3c4f-5818-4a83-adda-f46135054da0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        },
        {
            "id": "db2e3071-e9b2-4c62-b521-de5467757da0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        },
        {
            "id": "e42e7e58-0484-47c3-8aae-49f8554c456d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        },
        {
            "id": "254d8674-5724-4ade-99de-c8c17bd5f0c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        },
        {
            "id": "9adce7ce-01e5-44ff-a3f1-cdb38f8e8277",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        },
        {
            "id": "d6efc00a-084e-4310-9e10-1089302ed160",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        },
        {
            "id": "8f44f904-d5ae-4618-ba24-e7478cb0aa25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "24e1aa03-6b8e-447b-8244-83388116b0df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3aebbf65-b5fa-4658-821c-f61a32bf7b9e",
    "visible": true
}