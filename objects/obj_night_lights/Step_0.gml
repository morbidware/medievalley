/// @description Sync alpha to dayprogress
if(w != display_get_gui_width() || h != display_get_gui_height())
{
	w = display_get_gui_width();
	h = display_get_gui_height();
	if(surface_exists(surf))
	{
		surface_free(surf);
	}
	surf = surface_create(w,h);
}
if (room == roomHouse)
{
	alpha = 0;
	exit;
}

if (global.dayprogress >= 0.75 && global.dayprogress < 0.77) // 0.75 to 0.77
{
	alpha = (global.dayprogress - 0.75) * 50;
}
else if (global.dayprogress >= 0.77 && global.dayprogress < 0.98) // 0.77 to 0.98
{
	alpha = 1;
}
else if (global.dayprogress >= 0.98 && global.dayprogress <= 1) // 0.98 to 1
{
	alpha = 1 - ((global.dayprogress - 0.98) * 50);
}
else
{
	alpha = 0;
}