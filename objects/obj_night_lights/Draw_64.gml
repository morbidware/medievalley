/// @description Draw night surface
if (global.dayprogress < 0.75 || room == roomHouse)
{
	return;
}

shader_set(shaderNightLight);
draw_surface(surf,0,0);
shader_reset();