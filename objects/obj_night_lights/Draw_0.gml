/// @description Draw point lights

if(!surface_exists(surf))
{
	surf = surface_create(w,h);
}

surface_set_target(surf);
draw_set_color(c_black);
draw_set_alpha(alpha);
draw_rectangle(0,0,w,h,false);

with(obj_char)
{
	var rr = 1 + (1 - other.alpha);
	draw_sprite_ext(spr_pp_light_small, 0, pos_get_relative_x(), pos_get_relative_y()-32, rr, rr, 0, c_white, 1);
}
with(obj_wildlife_mail_pidgeon)
{
	draw_sprite_ext(spr_pp_light_3, 0, pos_get_relative_x(), pos_get_relative_y()-32, 1, 1, 0, c_white, 1);
}
with(obj_wildlife_firefly)
{
	draw_sprite_ext(spr_pp_light_small, 0, pos_get_relative_x(), pos_get_relative_y(), 1, 1, 0, c_white, image_alpha);
	if(distToPlayer != 0)
	{
		draw_sprite_ext(spr_pp_light_3, 0, pos_get_relative_x(), pos_get_relative_y(), 1, 1, 0, c_white, 1);
	}
}

with(obj_interactable_bonfire)
{
	if(growstage == 2)
	{
		draw_sprite_ext(spr_pp_light_3, 0, pos_get_relative_x(), pos_get_relative_y(), flickerscale, flickerscale, 0, c_white, image_alpha);
	}
}

with(obj_wolf)
{
	draw_sprite_ext(scr_asset_get_index(sprite_get_name(sprite_index)+"_eyes"),image_index, pos_get_relative_x(),pos_get_relative_y(),image_xscale*2.0,image_yscale*2.0,0,c_white,1.0);
}

with(obj_nightwolf)
{
	draw_sprite_ext(scr_asset_get_index(sprite_get_name(sprite_index)+"_eyes"),image_index, pos_get_relative_x(),pos_get_relative_y(),image_xscale*2.0,image_yscale*2.0,0,c_white,1.0);
}

surface_reset_target();

draw_set_alpha(1.0);