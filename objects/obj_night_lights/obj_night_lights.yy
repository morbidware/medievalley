{
    "id": "81862d8d-1b81-4775-b67f-ff26993073d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_night_lights",
    "eventList": [
        {
            "id": "aa7825e2-60dc-4853-9b45-2d5a9211c626",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "81862d8d-1b81-4775-b67f-ff26993073d9"
        },
        {
            "id": "04b7f317-6dc2-4ee5-ba26-3401a6479bfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "81862d8d-1b81-4775-b67f-ff26993073d9"
        },
        {
            "id": "6c0b8aac-c034-4df7-a3cd-00cd438202ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "81862d8d-1b81-4775-b67f-ff26993073d9"
        },
        {
            "id": "f69f6cc1-ddb4-48ea-aa87-6febc05869cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "81862d8d-1b81-4775-b67f-ff26993073d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}