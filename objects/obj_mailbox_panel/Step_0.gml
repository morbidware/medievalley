event_inherited();

for(var i = 0; i < ds_list_size(mails); i++)
{
	var mmin = page*5;
	var mmax = mmin+5;
	var btnMail = ds_list_find_value(mails,i);
	if (instance_exists(btnMail))
	{
		if (i >= mmin && i < mmax)
		{
			btnMail.ox = floor(-width/2) + 14;
			btnMail.oy = floor(-height/2) + 32 + 30*(i - (page*5));
		}
		else
		{
			btnMail.ox = 9999;
		}
	}	
}