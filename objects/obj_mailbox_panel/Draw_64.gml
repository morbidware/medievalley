draw_set_color(c_white);
draw_set_alpha(1.0);
scr_nine_slice_at(spr_9slice_mailbox_bg, display_get_gui_width()/2, y, width, height, 2);

btnClose.ox = width/2 - 18;
btnClose.oy = -height/2 + 18;

if (ds_list_size(mails) == 0)
{
	draw_text(x,y+6,"Mailbox is empty.");
}

if (pages > 0)
{
	draw_set_font(global.FontStandardBoldOutline);
	draw_set_valign(fa_middle);
	draw_set_halign(fa_center);
	draw_text(x,y+86,string(page+1)+" / "+string(pages));
}