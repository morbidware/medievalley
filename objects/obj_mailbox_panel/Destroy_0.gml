event_inherited();

with(obj_btn_mail)
{
	instance_destroy();
}
instance_destroy(btnClose);
if (instance_exists(btnLeft))
{
	instance_destroy(btnLeft);
}
if (instance_exists(btnRight))
{
	instance_destroy(btnRight);
}

with (obj_wildlife_mail_pidgeon)
{
	// Make pidgeon depart only if all mail have been read
	if (ds_list_size(global.mailRead) == ds_list_size(global.mailTitles))
	{
		image_xscale = -image_xscale;
		status = Status.Depart;
		sndflap = audio_play_sound(snd_flap_wings,0,true);
	}
}

// Init first day and first mission
with(obj_gameLevelFarm)
{
	//logga("GLOBAL.MAILREAD = " + string(ds_list_find_value(global.mailRead,0)));
	if(firstday && ds_list_find_value(global.mailRead,0) == 1)
	{
		firstday = false;
		global.allowvisitors = 1;
		instance_create_depth(0,0,0,obj_mission_0001);
		ds_map_set(global.unlockables, "ui_dock", 1);
		ds_map_set(global.unlockables, "ui_inventory", 1);
		ds_map_set(global.unlockables, "ui_craft", 1);
		
		scr_storeUnlockables();
	}
}