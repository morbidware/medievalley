event_inherited();

depth = -900;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -800;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

width = scr_adaptive_x(400);
height = scr_adaptive_y(198);
w_width = width;
w_height = height;
page = 0;
pages = 0;

btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;
btnClose.sprite_index = spr_btn_close_silver;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}
btnLeft = noone;
btnRight = noone;

mails = ds_list_create();
for(var i = 0; i < ds_list_size(global.mailTitles); i++)
{
	var btnMail = instance_create_depth(0,0,-10001,obj_btn_mail);
	btnMail.caller = id;
	btnMail.title = ds_list_find_value(global.mailTitles,i);
	btnMail.body = ds_list_find_value(global.mailBodies,i);
	btnMail.signature = ds_list_find_value(global.mailSignatures,i);
	btnMail.read = ds_list_find_value(global.mailRead,i);
	btnMail.globalindex = i;
	ds_list_add(mails,btnMail);
}

if (ds_list_size(mails) > 5)
{
	btnLeft = instance_create_depth(0,0,-10001,obj_btn_generic);
	btnLeft.sprite_index = spr_farmboard_btn_prev;
	btnLeft.ox = -64;
	btnLeft.oy = 86;
	btnLeft.caller = id;
	btnLeft.evt = ev_user1;
	btnLeft.depth = depth-1;

	btnRight = instance_create_depth(0,0,-10001,obj_btn_generic);
	btnRight.sprite_index = spr_farmboard_btn_next;
	btnRight.ox = 64;
	btnRight.oy = 86;
	btnRight.caller = id;
	btnRight.evt = ev_user2;
	btnRight.depth = depth-1;
	
	pages = ceil(ds_list_size(mails) / 5);
	height = 228;
}

audio_play_sound(snd_menu_enter,0,false);