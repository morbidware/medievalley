{
    "id": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mailbox_panel",
    "eventList": [
        {
            "id": "079e162b-3768-4d38-9fda-37c845234dd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        },
        {
            "id": "6b490861-24e5-4dbb-a148-c39498d9b0c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        },
        {
            "id": "18409552-bbe5-4cd1-9322-7be68bf269f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        },
        {
            "id": "27b20f2c-3f3b-4aea-b8aa-2b21f7392237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        },
        {
            "id": "144cfd94-179d-4ea2-a23a-0ddc30457b6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        },
        {
            "id": "42e55b2c-38fe-4748-ae7b-6e9de365c37c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        },
        {
            "id": "ae481dc3-9930-48d0-a77e-c6bc7ea4082b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab1a472d-e261-42fb-9fed-eda8cd68fbe5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}