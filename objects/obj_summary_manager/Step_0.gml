/// @description Show info

// do nothing if diary is open (this prevents missioninfo panels to always close)
if (instance_exists(obj_diary_panel))
{
	exit;
}

var createPanel = false;
var description = "";
ds_map_clear(required);
ds_map_clear(done);

for (var i = 0; i < ds_list_size(summaries); i++)
{
	var summary = ds_list_find_value(summaries,i);
	
	var mx = mouse_get_relative_x();
	var my = mouse_get_relative_y();

	if (mx > summary.posx - (summary.w/2) && mx < summary.posx + (summary.w/2) && my > summary.posy - (summary.h/2) && my < summary.posy + (summary.h/2))
	{
		createPanel = true;
		required = summary.required;
		done = summary.done;
		description = summary.description;
	}
	
	if (summary.fadex > 0)
	{
		summary.fadex -= 2;
	}
	else
	{
		summary.fadex = 0;
	}
}

if (createPanel)
{
	if (!instance_exists(obj_diary_missioninfo))
	{
		var mi = instance_create_depth(mx,my,0,obj_diary_missioninfo);
		mi.required = required;
		mi.done = done;
		mi.mirrored = true;
		mi.desc = description;
	}
	else
	{
		var mi = instance_find(obj_diary_missioninfo,0);
		mi.required = required;
		mi.done = done;
		mi.mirrored = true;
		mi.desc = description;
	}
}
else
{
	if (instance_exists(obj_diary_missioninfo))
	{
		instance_destroy(obj_diary_missioninfo);
	}
}
	