{
    "id": "0d6a7e32-87be-41f8-9db8-1803923114e5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_summary_manager",
    "eventList": [
        {
            "id": "c809036a-cdcd-4e37-8692-606f4dc2f1b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d6a7e32-87be-41f8-9db8-1803923114e5"
        },
        {
            "id": "42c0252a-56bb-4a4c-8e0f-f9acba2baf98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0d6a7e32-87be-41f8-9db8-1803923114e5"
        },
        {
            "id": "edeebb56-80b4-4b2d-aa38-0a1d5be36572",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d6a7e32-87be-41f8-9db8-1803923114e5"
        },
        {
            "id": "f9a0fe89-36f1-41b4-b3f3-d3c6416f40ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0d6a7e32-87be-41f8-9db8-1803923114e5"
        },
        {
            "id": "c141b26d-a1a0-419c-8ab4-d4b586231cd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d6a7e32-87be-41f8-9db8-1803923114e5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}