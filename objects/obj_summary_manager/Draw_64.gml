///@description Draw summaries

// Only show top 3 summaries
var size = ds_list_size(summaries);
if (size > 3)
{
	size = 3;
}
for (var i = 0; i < size; i++)
{
	var s = ds_list_find_value(summaries,i);

	s.posx = display_get_gui_width() - 54 + s.fadex;
	s.posy = display_get_gui_height()/2 + 74 + (s.index * (s.h+6));
	
	// Background
	draw_set_alpha(1);
	draw_set_color(c_white);
	scr_nine_slice_at(spr_9slice_summary_bg,s.posx,s.posy,s.w,s.h,2);

	// Mission title
	draw_set_color(make_color_rgb(60,40,10));
	draw_set_font(global.FontStandard);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	scr_draw_text_ext(s.posx - (s.w/2) + 14, s.posy - (s.h/2) + 10, s.title, 11, s.w - 30);

	// Mission progress
	if (s.completion >= 1)
	{
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_sprite_ext(spr_mission_check,0,s.posx + 32, s.posy-4, 2, 2, 0, c_white, 1);
		draw_set_alpha(0.5);
		draw_set_color(c_black);
		scr_draw_text_ext(s.posx - (s.w/2) + 14, s.posy - (s.h/2) + 36, "Go to farm.", 10, s.w - 30);
	}
	else
	{
		draw_set_alpha(1);
		draw_rectangle(s.posx - (s.w/2) + 10, s.posy + 8, s.posx + (s.w/2) - 12, s.posy + 16, false);
		draw_set_color(c_green);
		var barw = (s.posx + (s.w/2) - 12) - (s.posx - (s.w/2) + 12);
		draw_rectangle(s.posx - (s.w/2) + 12, s.posy + 10, s.posx - (s.w/2) + 12 + (barw * s.completion), s.posy + 14, false);
		draw_set_color(c_white);
	}
}
draw_set_alpha(1);