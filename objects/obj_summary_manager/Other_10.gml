/// @description Refresh summaries

// Clear all summaries
instance_destroy(obj_mission_summary);
ds_list_clear(tempSummaries);
ds_list_clear(summaries);
index = 0;

// Recreate summaries
with (obj_mission)
{
	if (!active)
	{
		continue;
	}
	
	var summary = instance_create_depth(0,0,0,obj_mission_summary);
	
	summary.title = title;
	summary.completion = completion;
	if (completeOnHold)
	{
		ds_map_copy(summary.done, required);
	}
	else
	{	
		ds_map_copy(summary.done, done);
	}
	ds_map_copy(summary.required, required);
	summary.index = other.index;
	summary.description = longdesc;
	other.index++;
	
	// If this mission is new, add to active missions and fade it
	// The point of this list is to not trigger fadein everytime summaries are refreshed
	if (round(ds_list_find_index(other.activeMissions, missionid)) < 0)
	{
		summary.fadex = 150;
		ds_list_add(other.activeMissions, missionid);
	}
	ds_list_add(other.summaries,summary);
}

// Only show the top three nearest to completion
var value = 0;
while (true)
{
	var chosen = noone;
	var removeindex = -1;
	for (var i = 0; i < ds_list_size(tempSummaries); i++)
	{
		var summary = ds_list_find_value(tempSummaries,i);
		if (summary.completion >= value)
		{
			value = summary.completion;
			chosen = summary;
		}
	}
	
	if (chosen != noone && removeindex >= 0)
	{
		ds_list_add(summaries,chosen);
		ds_list_delete(summaries,removeindex);
	}
	else
	{
		break;
	}
	
	if (ds_list_size(summaries) == 3 || ds_list_size(tempSummaries) == 0)
	{
		break;
	}
}