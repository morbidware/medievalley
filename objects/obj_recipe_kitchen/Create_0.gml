/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

ds_map_add(ingredients,"stone",8);
ds_map_add(ingredients,"woodlog",4);

resitemid = "kitchen";
unlocked = true;
desc = "Has everything you need to cook any recipe.";