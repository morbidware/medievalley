{
    "id": "61121a65-01d0-41ca-ac32-0e2fdf5bcec5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_backpack_panel",
    "eventList": [
        {
            "id": "1e2417a3-6258-446c-b414-a92d51968fdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "61121a65-01d0-41ca-ac32-0e2fdf5bcec5"
        },
        {
            "id": "3bf982f0-c905-4c10-a8d1-be292fda4c06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "61121a65-01d0-41ca-ac32-0e2fdf5bcec5"
        },
        {
            "id": "ebd52d84-b7a9-4b93-b658-5486ae16b4c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "61121a65-01d0-41ca-ac32-0e2fdf5bcec5"
        },
        {
            "id": "d649795a-3d20-427d-bd55-b5e0d7c62e4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "61121a65-01d0-41ca-ac32-0e2fdf5bcec5"
        },
        {
            "id": "13905033-d0b0-4742-95b7-2b85558a4e31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "61121a65-01d0-41ca-ac32-0e2fdf5bcec5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}