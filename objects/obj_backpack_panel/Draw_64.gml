draw_sprite_ext(spr_backpack_bg, 0, x, y, 2, 2, 0, c_white, 1);
draw_sprite_ext(spr_backpack_head, 0, x-98, y-60, 2, 2, 0, c_white, 0.5);
draw_sprite_ext(spr_backpack_torso, 0, x-98, y-22, 2, 2, 0, c_white, 0.5);
draw_sprite_ext(spr_backpack_legs, 0, x-98, y+16, 2, 2, 0, c_white, 0.5);
draw_sprite_ext(spr_backpack_bag, 0, x-98, y+54, 2, 2, 0, c_white, 0.5);
