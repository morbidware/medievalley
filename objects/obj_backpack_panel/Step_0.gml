// Fade in
if(state == 0)
{
	if(t < 1.0)
	{
		t += (1.0/room_speed)*global.dt;
		y = scr_easeOutBack(t, display_get_gui_height()/2-display_get_gui_height(), display_get_gui_height(), 1.0);
	}
}
// Fade out
else if(state == 1)
{
	y = scr_easeInBack(t, display_get_gui_height()/2, -display_get_gui_height(), 1.0);
	if(t < 1.0)
	{
		t += (1.0/room_speed)*global.dt;
		if(t >= 1.0)
		{
			instance_destroy();
		}
	}
}
x = display_get_gui_width()/2;

//event_inherited();

startx = -56;
starty = -60;

if (instance_exists(btnClose))
{
	btnClose.ox = 126;
	btnClose.oy = -90;
}

// Align objects to grid
var c = 0;
var r = 0;
var index = -1;
var obj = noone;
repeat (rows)
{
	c = 0;
	repeat (cols)
	{
		index = global.inv_inventorylimit + (r * cols) + c;
		obj = ds_grid_get(inv.inventory,index,0);
		if (obj > -1 && instance_exists(obj))
		{
			if(index < 30){	
				obj.x = x + startx + (spacing * c) + (cellsize * c);
				obj.y = y + starty + (spacing * r) + (cellsize * r);
			}
		}
		c++;
	}
	r++;
}
index = 30
repeat(3)
{
	obj = ds_grid_get(inv.inventory,index,0);
	if (obj > -1 && instance_exists(obj))
	{
		if(index = 30)
		{
			obj.x = x - 98;
			obj.y = y - 60;
		}
		else if( index = 31)
		{
			obj.x = x - 98;
			obj.y = y - 22;
		}
		else if(index = 32)
		{
			obj.x = x - 98;
			obj.y = y + 16;
		}
	}
	index++;
}
	

with (obj_char)
{
	highlightedobject = noone;	
}