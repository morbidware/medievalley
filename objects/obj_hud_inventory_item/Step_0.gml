/// @description Insert description here
// You can write your code in this editor
depth = -y;
if(!composed)
{
	composed = true;
	var inv = instance_find(obj_playerInventory,0);
	var emptySlotIndex = -1;
	var sameSlotIndex = -1;
	var targetIndex = -1;
	for(var i = 0; i < inv.slots; i++)
	{
		var v = ds_grid_get(inv.inventory,i,0);
		if(v == "noone" && emptySlotIndex < 0)
		{
			emptySlotIndex = i;
		}
		if(v == name)
		{
			sameSlotIndex = i;
		}
	}
	
	if(sameSlotIndex > 0)
	{
		targetIndex = sameSlotIndex;
	}
	else if(emptySlotIndex > 0)
	{
		targetIndex = emptySlotIndex;
	}
	
	targetx = inv.startx + 18 * targetIndex;
	targety = inv.starty;
}
if(state == 0)
{
	direction = point_direction(x,y,targetx,targety);
	var distance = point_distance(x,y,targetx,targety);
	speed = distance*0.2;
	if(speed < 0.1)
	{
		x = targetx;
		y = targety;
		state = 1;
	}
}