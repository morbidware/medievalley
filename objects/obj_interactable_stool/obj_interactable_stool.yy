{
    "id": "6e86fdd1-b238-4344-872d-1706ce1e8b1b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_stool",
    "eventList": [
        {
            "id": "5a897fd0-1ec4-45db-b354-8d1ee542acdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "6e86fdd1-b238-4344-872d-1706ce1e8b1b"
        },
        {
            "id": "198c491f-2382-4116-9d33-8b06fc2a6c46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6e86fdd1-b238-4344-872d-1706ce1e8b1b"
        },
        {
            "id": "9a959d60-99a5-4f87-b276-d7c2016edf98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "6e86fdd1-b238-4344-872d-1706ce1e8b1b"
        },
        {
            "id": "e559ceeb-414b-4550-a798-14ad9b410e1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "6e86fdd1-b238-4344-872d-1706ce1e8b1b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97840556-11cd-482b-b911-53179d43218e",
    "visible": true
}