{
    "id": "6050b08d-2015-46a1-9d6d-2b25ca29d89f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_graveyard_tomb_6",
    "eventList": [
        {
            "id": "314fdc87-5149-4cbb-9b31-c51b98f4aefc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6050b08d-2015-46a1-9d6d-2b25ca29d89f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ba469bc-5710-46c1-8fa5-7814c2e8b869",
    "visible": true
}