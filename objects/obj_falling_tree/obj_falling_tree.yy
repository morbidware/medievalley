{
    "id": "708bf884-95b5-477b-96ca-b0bb84c5579c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_falling_tree",
    "eventList": [
        {
            "id": "fba6d7ec-a71e-4ba6-ae69-3e3bdc6978a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "708bf884-95b5-477b-96ca-b0bb84c5579c"
        },
        {
            "id": "c9f8b509-3a0c-4767-8c93-000d8e597427",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "708bf884-95b5-477b-96ca-b0bb84c5579c"
        },
        {
            "id": "3822a7c8-7779-4b0e-a454-871170a5987d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "708bf884-95b5-477b-96ca-b0bb84c5579c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}