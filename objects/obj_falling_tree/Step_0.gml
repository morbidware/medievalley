/// @description Insert description here
// You can write your code in this editor

scr_setDepth(DepthType.Elements);
depth += 1;

if(abs(rot) < 90)
{
	rotspeed += rotacc * global.dt;
	if(rotspeed > rotmaxspeed)
	{
		rotspeed = rotmaxspeed;
	}

	rot += rotspeed*rotdir;
}
else
{
	rot = 90*rotdir;
	if (image_alpha >= 1)
	{
		size = clamp(treeimageindex * 25,16,75);
		if (rot == 90)
		{
			if(!global.ismobile)
			{
				part_emitter_region(ps,pe_leaf,x-size,x-10,y-10,y+5,ps_shape_rectangle,ps_distr_linear);
			}
		}
		else
		{
			if(!global.ismobile)
			{
				part_emitter_region(ps,pe_leaf,x+10,x+size,y-10,y+5,ps_shape_rectangle,ps_distr_linear);
			}
		}
		if(!global.ismobile)
		{
			part_emitter_burst(ps,pe_leaf,pt_leaf,30);
		}
	}
	image_alpha -= 0.02 * global.dt;
	if(image_alpha <= 0)
	{
		instance_destroy();
	}
}