/// @description Insert description here
// You can write your code in this editor
treesprite = 0;
treeimageindex = 0;
rot = 0;
rotspeed = 0;
rotacc = 0.015;
rotmaxspeed = 2;

rotdir = 1;

var nearest = instance_nearest(x,y,obj_char);
var nearestdummy = noone;
nearestdummy = instance_nearest(x,y,obj_dummy);
if(nearestdummy != noone)
{
	if(point_distance(x,y,nearestdummy.x,nearestdummy.y) < point_distance(x,y,nearest.x,nearest.y))
	{
		nearest = nearestdummy;
	}
}

with(nearest)
{
	if(x < other.x)
	{
		other.rotdir = -1;
	}
}

//leaves particles
if(!global.ismobile)
{
	ps = part_system_create();
	part_system_depth(ps,-y-1);

	pt_leaf = part_type_create();
	part_type_sprite(pt_leaf,spr_particle_leaf,false,false,false);
	part_type_orientation(pt_leaf,9,360,0,0,0);
	part_type_alpha3(pt_leaf,1,1,0);
	part_type_color_mix(pt_leaf,make_color_rgb(150,150,150),make_color_rgb(255,255,255));
	part_type_gravity(pt_leaf,0.015,270);
	part_type_direction(pt_leaf,60,120,0,0);
	part_type_speed(pt_leaf,0.4,0.8,0,0);
	part_type_life(pt_leaf,30,70);

	pe_leaf = part_emitter_create(ps);
}	

audio_play_sound(snd_tree_falling,0,false);