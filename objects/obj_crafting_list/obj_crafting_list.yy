{
    "id": "b8cd9724-0d12-4d8b-8b6e-98b8515d17e1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_list",
    "eventList": [
        {
            "id": "6b62dfda-5349-4e06-a0d0-78e61f600421",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "b8cd9724-0d12-4d8b-8b6e-98b8515d17e1"
        },
        {
            "id": "dc39e386-8d31-4b19-9484-c63a33340e7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b8cd9724-0d12-4d8b-8b6e-98b8515d17e1"
        },
        {
            "id": "28a2a9b3-c720-4a83-8dc3-24153a1fbb89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8cd9724-0d12-4d8b-8b6e-98b8515d17e1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "05f89c86-5da4-4ee9-a33d-59933e612198",
    "visible": true
}