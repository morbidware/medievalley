{
    "id": "c85df3cc-d2a7-43bc-babc-ffbf7e2ba019",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_status_strength",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a52de1de-4ab3-40a4-a337-03792d097632",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61804e57-55f4-4880-a549-2d570983bf71",
    "visible": true
}