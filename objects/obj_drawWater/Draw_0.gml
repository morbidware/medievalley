// Most of this is an extremely simplified version of obj_groundrendering + obj_groundrenderer

// get view position in tile space
c = 0;
r = 0;
col = floor(global.viewx/16) - margin;
row = floor(global.viewy/16) - margin;

// draw moving water
c = col;
repeat (30 + (margin*2))
{
	r = row;
	repeat (17 + (margin*2))
	{
		posx = c*16+8+offsetx;
		posy = r*16+8+offsety;
		//draw_sprite_ext(spr_water,0,posx,posy,1,1,0,c_white,0.7);
		draw_sprite(spr_water,0,posx,posy);
		r++;
	}
	c++;
}