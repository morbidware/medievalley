{
    "id": "86e4fb4b-ac92-4765-b829-e56b410287f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drawWater",
    "eventList": [
        {
            "id": "662144dd-088a-4192-96c5-4856db15c16d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86e4fb4b-ac92-4765-b829-e56b410287f9"
        },
        {
            "id": "57a1f0b1-5e9f-46fa-8f1a-d8062e40e4e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "86e4fb4b-ac92-4765-b829-e56b410287f9"
        },
        {
            "id": "6a428a17-c38b-4351-9c41-a5bd28d2edd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "86e4fb4b-ac92-4765-b829-e56b410287f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}