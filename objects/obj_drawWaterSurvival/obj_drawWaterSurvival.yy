{
    "id": "d2ffa8d6-2a62-40fa-9979-b360ee5d27c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drawWaterSurvival",
    "eventList": [
        {
            "id": "fe31ec0f-aea6-4c9f-a6f2-29e97bc0e529",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2ffa8d6-2a62-40fa-9979-b360ee5d27c2"
        },
        {
            "id": "bfa793a6-3f36-47b4-aeab-187fa4680fa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d2ffa8d6-2a62-40fa-9979-b360ee5d27c2"
        },
        {
            "id": "c6935c6f-e8d3-4013-bbf6-499e8bb73e4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2ffa8d6-2a62-40fa-9979-b360ee5d27c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}