// Most of this is an extremely simplified version of obj_groundrendering + obj_groundrenderer

// get view position in tile space
col = floor(global.viewx/16) - margin;
row = floor(global.viewy/16) - margin;

posx = col*16+8+offsetx;
posy = row*16+8+offsety;
		
draw_sprite(spr_waterBG,0,posx,posy);