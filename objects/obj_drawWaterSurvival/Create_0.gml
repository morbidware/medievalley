scr_setDepth(DepthType.Water);
if(global.isSurvival)
{
	margin = 2;
}
else
{
	margin = 4;
}
offsetx = 0;
offsety = 0;
c = 0;
r = 0;
col = 0;
row = 0;

angle = 0;

// create water foam and walls
ground = instance_find(obj_baseGround,0);
repeat(ground.cols)
{
	r = 0;
	repeat(ground.rows)
	{
		var type = ds_grid_get(ground.grid,c,r);
		if (type == GroundType.Water)
		{	
			var index = ds_grid_get(ground.indexgrid,c,r);
			if (index != 47)
			{
				//var f = instance_create_depth(c*16 + 8,r*16 + 8, 0, obj_fx_waterfoam);
				//f.image_index = index;
				
				var w = instance_create_depth(c*16 + 8, r*16 + 8, 0, obj_wall_invisible);
				w.storey_0 = true;
				w.storey_1 = true;
				w.storey_2 = true;
				w.storey_3 = true;
				ds_grid_set(ground.wallsgrid,c,r,w);
			}
		}
		r++;
	}
	c++;
}