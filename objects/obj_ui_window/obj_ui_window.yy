{
    "id": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_window",
    "eventList": [
        {
            "id": "ed7aa2a1-3a9c-4e12-afd6-05bff5804808",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd"
        },
        {
            "id": "52b897cf-5f20-4ee9-822a-5d8ec7c063c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd"
        },
        {
            "id": "9ec15278-ecc9-4da4-8a6d-6bbc027f0c9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}