// Remove highlighted object to avoid interactions while this menu is open
with (obj_char)
{
	highlightedobject = noone;	
}

// Fade in
if(state == 0)
{
	if(t < 1.0)
	{
		t += (1.0/room_speed)*global.dt;
		y = scr_easeOutBack(t, display_get_gui_height()/2-display_get_gui_height(), display_get_gui_height(), 1.0);
		
		// Mission info panels could stay active while opening a windows so it's better to destroy them in this moment
		with (obj_diary_missioninfo)
		{
			instance_destroy(id);
		}
	}
}
// Fade out
else if(state == 1)
{
	y = scr_easeInBack(t, display_get_gui_height()/2, display_get_gui_height(), 1.0);
	if(t < 1.0)
	{
		t += (1.0/room_speed)*global.dt;
		if(t >= 1.0)
		{
			instance_destroy();
		}
	}
}
x = display_get_gui_width()/2;