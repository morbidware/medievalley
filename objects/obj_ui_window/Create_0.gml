// x,y, width and height of the window intractable area - NOTE: children object must assign these!
p_x = 0;
p_y = 0;
p_w = 0;
p_h = 0;

state = 0;					// 0: fadein - 1: fadeout
t = 0;						// time for fade animation
w_width = 0;
w_height = 0;
scr_lockHudRequest(id);

depth = -210;