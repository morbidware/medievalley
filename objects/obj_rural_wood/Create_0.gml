event_inherited();

ds_map_add(grass,"wildgrass",50);

/*ds_map_add(plants,"appletree",30);
ds_map_add(plants,"beechtree",50);
ds_map_add(plants,"porcinimushroom",3);
ds_map_add(plants,"mint",3);*/
if(global.isSurvival)
{
	ds_map_add(grass,"blueberry_plant",10);
	ds_map_add(grass,"bush",30);
}

// No boulders in this submap

ds_map_add(mobs,"bear",0.2);