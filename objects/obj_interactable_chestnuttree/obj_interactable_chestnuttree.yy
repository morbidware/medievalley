{
    "id": "0e7850d7-24ed-4ce1-a89d-3eded637d90a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_chestnuttree",
    "eventList": [
        {
            "id": "84462d28-d4d5-453b-a208-6677dfa480a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        },
        {
            "id": "164da626-fb96-4aeb-9dd1-2c92177f29c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        },
        {
            "id": "e25181d6-b925-42dc-96e7-5fffa103d4aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        },
        {
            "id": "2c039b22-570d-4762-b6a5-d5b552ae0b8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        },
        {
            "id": "97581885-4888-431e-a234-a51979dad2ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        },
        {
            "id": "ca92a4fb-22bb-4ad3-951e-8b5af170ca55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        },
        {
            "id": "c5eb42bf-9925-44eb-bd35-84eb1ba4f8ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "0e7850d7-24ed-4ce1-a89d-3eded637d90a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
    "visible": true
}