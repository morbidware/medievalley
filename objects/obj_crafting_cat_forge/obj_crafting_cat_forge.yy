{
    "id": "fb54c08b-f767-4e4e-9513-67a3a9e6ce65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_forge",
    "eventList": [
        {
            "id": "756fa68a-6d3b-4402-b43a-96c55f0d105c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb54c08b-f767-4e4e-9513-67a3a9e6ce65"
        },
        {
            "id": "6816b50f-c0df-4ab5-b676-610c5a4681fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fb54c08b-f767-4e4e-9513-67a3a9e6ce65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "92a43f6f-1771-47eb-8b77-f821eca5b048",
    "visible": true
}