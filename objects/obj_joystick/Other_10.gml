/// @description Movement
// You can write your code in this editor
var player = instance_find(obj_char,0);

if(player.move == 0)
{
	player.left = 0;
	player.right = 0;
	player.up = 0;
	player.down = 0;
	
}
if(player.move == 1)
{
	var angle = degtorad(player.joystickDirection);
	if(!OnDrag){
		
		if(angle > 3.81 && angle <= 5.43)
		{
			player.left = 0;
			player.right = 0;
			player.up = 0;
			player.down = 1;
		}
		else if(angle > 5.43 && angle <= 7.06)
		{
			player.left = 0;
			player.right = 1;
			player.up = 0;
			player.down = 0;
		}
		else if(angle > 7.06 && angle <= 8.68)
		{
			player.left = 0;
			player.right = 0;
			player.up = -1;
			player.down = 0;
		}
		else if(angle > 8.68 || angle <= 3.81)
		{
			player.left = -1;
			player.right = 0;
			player.up = 0;
			player.down = 0;
		}
	}
	else
	{
		player.left = cos(angle);
		player.right = 0;
		player.up = -sin(angle);
		player.down = 0;
	}
}
