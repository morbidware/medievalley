{
    "id": "8a477698-0259-4d5d-bb32-aef6ba8df0eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_joystick",
    "eventList": [
        {
            "id": "3cfb66aa-aeb2-4f2f-b264-3f30a42916b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a477698-0259-4d5d-bb32-aef6ba8df0eb"
        },
        {
            "id": "882e8ee6-2325-4bb8-8a81-b00d98dd1f9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a477698-0259-4d5d-bb32-aef6ba8df0eb"
        },
        {
            "id": "70bb153e-0f7d-490b-829d-b36cc9f410fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8a477698-0259-4d5d-bb32-aef6ba8df0eb"
        },
        {
            "id": "326e01ec-ee11-4f1d-8f94-b65508babf67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8a477698-0259-4d5d-bb32-aef6ba8df0eb"
        },
        {
            "id": "ec1b98d4-60b0-4130-9fc5-57c4c58752d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8a477698-0259-4d5d-bb32-aef6ba8df0eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}