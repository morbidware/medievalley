/// @description Reset vars
var player = instance_find(obj_char,0);

player.move = 0;
player.left = 0;
player.right = 0;
player.up = 0;
player.down = 0;
OnDrag = false;
release = true;
joystickHandleX = joystickPositionX;
joystickHandleY = joystickPositionY;
player.actionButton.clickCounter = 0;
