var player = instance_find(obj_char,0);

var clickX = noone;
var	clickY = noone;

if(joystickPositionX != scr_adaptive_x(150) || joystickPositionY != scr_adaptive_y(380))
{
	joystickPositionX = scr_adaptive_x(150);
	joystickPositionY = scr_adaptive_y(380);
}

if(mouse_get_relative_x()<window_get_width()/2)
{
	clickX = mouse_get_relative_x();
	clickY = mouse_get_relative_y();
}
else
{
	//Reset
	event_perform(ev_other,ev_user1);
	exit;
}

var distToCenter = point_distance(clickX,clickY,joystickPositionX,joystickPositionY);
player.joystickDirection = point_direction(clickX,clickY,joystickPositionX,joystickPositionY) + 180;

if(device_mouse_check_button_pressed(0,mb_left) && clickX< window_get_width()/2)
{
	player.actionButton.clickCounter = 1;
	startClickX = clickX;
	startClickY = clickY;
	release = false;
}
if(device_mouse_check_button_released(0,mb_left))
{
	//Reset
	event_perform(ev_other,ev_user1);
}
if(release == true)
{
	player.actionButton.clickCounter = 0;
	exit;	
}

if(device_mouse_check_button(0,mb_left) && clickX < window_get_width()/2)
{	
	//Check click on sprite
	if(distToCenter<joystickSprWidth)
	{	
		joystickHandleX = clickX;
		joystickHandleY = clickY;
		
		if( distToCenter<joystickdeadZone)
		{
			player.move = 0;
		}
		else
		{
			player.move = 1;
		}	
	}
	//Continue movement also if you are dragging out the sprite
	if(distToCenter>joystickSprWidth && player.move == 1)
	{
		joystickHandleX = joystickPositionX + lengthdir_x(joystickSprWidth,player.joystickDirection);
		joystickHandleY = joystickPositionY + lengthdir_y(joystickSprWidth,player.joystickDirection);
	}
	//Detect drag
	if(point_distance(startClickX,startClickY,clickX,clickY)>10)
	{	
		OnDrag = true;
	}
}

//Movement
event_perform(ev_other,ev_user0);