///@description A little rest - Craft the tent.
event_inherited();

missionid = "0002";
title = "A little rest";
shortdesc = "Craft the tent.";
longdesc = "Mow wildgrass and get saplings to gather resources and build a Pallet.";
silent = false;

ds_map_add(rewards,obj_reward_0002,1);
ds_map_add(required,"pallet",1);

ds_list_clear(hintTexts);
ds_list_add(hintTexts, "Mow wildgrass with the Sickle.", "You can craft the Pallet from the Structures menu.");
ds_list_clear(hintImages);
ds_list_add(hintImages, spr_pickable_wildgrass, spr_pickable_pallet);

scr_storeMissions();