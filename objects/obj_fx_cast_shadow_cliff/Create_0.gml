event_inherited();

// This object is needed to scripts like "obj_shadows_#" to render cliff shadows at specific positions
storey = 0;

// shadow setup
shadowType = ShadowType.Square;
shadowLength = 56;
offsetx = 0;
offsety = 0;