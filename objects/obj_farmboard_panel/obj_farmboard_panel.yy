{
    "id": "5dabc150-708c-42f5-87c9-f9e29e6edd34",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_farmboard_panel",
    "eventList": [
        {
            "id": "bbce38c5-8d1a-4742-8dbc-028549f37375",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "8fbef422-354f-4b75-af49-deb1a9a1b7e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "a08aaa2d-df7b-4bc9-911d-39a71e414a5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "e86f4955-5bfc-4398-a906-691b299675b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "5be0951a-475d-4cf4-91b3-0c9defe4523d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "248cbded-0f8d-4baa-ad00-629d93701eb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "1f86ee25-226e-4e29-88d3-aeaf859ca449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "6a87fbab-5138-4e3a-8808-a73d886ad133",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        },
        {
            "id": "68ca94d6-6c16-4952-bc12-c33c1c6bf6a6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5dabc150-708c-42f5-87c9-f9e29e6edd34"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}