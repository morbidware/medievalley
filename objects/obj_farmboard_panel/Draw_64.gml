logga("FARMBOARD PANEL DRAW GUI");
// ----------------------------------------- Background
draw_set_color(c_white);
draw_set_alpha(1);
draw_sprite_ext(spr_farmboard_bg, 0, display_get_gui_width()/2, y, 2, 2, 0, c_white, 1);

// ----------------------------------------- Page counter
draw_set_font(global.FontStandard);
draw_set_color(c_black);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
var startpage = 1+floor(from/10);
var totpages = ceil(ds_list_size(global.socketsusernames)/10);
if(totpages == 0)
{
	totpages = 1;
}
draw_text(x, y + 200, string(startpage)+"/"+string(totpages));
draw_set_color(c_white);

// ----------------------------------------- Draw entry mouseover highlight
if(!fetching && !refreshing && !global.onPopup)
{
	if(mouse_get_relative_x() > sx && mouse_get_relative_x() < sx+sockw && mouse_get_relative_y() > sy && mouse_get_relative_y() < sy+toth)
	{
		currentIndex = from+floor((mouse_get_relative_y() - sy)/sockh);
	
		draw_set_color(c_white);
		draw_set_alpha(0.3);
		draw_rectangle(sx,sy+(sockh*currentIndex),sx+sockw,sy+(sockh*currentIndex)+sockh-7, false);
		draw_set_alpha(1);
	}
}

// ----------------------------------------- Draw entries
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_top);
if (fetching || refreshing)
{
	draw_set_alpha(0.3);
}
var c = 0;
var to = from+10;
if(to > ds_list_size(global.socketsusernames))
{
	to = ds_list_size(global.socketsusernames);
}
for(var i = from; i < to; i++)
{
	draw_set_color(c_black);
	draw_set_font(global.FontStandardBold);
	draw_text(sx+10.0,sy + sockh*c + (sockh/2.0) - 6, scr_fix_text(ds_list_find_value(global.socketsusernames,i))+ "'s farm");
	var px = sx+390.0;
	var py = sy + sockh*c + (sockh/2.0) - 5;
			
	draw_set_font(global.FontStandard);
	draw_sprite(spr_farmboard_icons,0,px-40,py+3);
	draw_text(px-40+12,py-1,string(ds_list_find_value(global.socketsnumplants,i)));
			
	draw_sprite(spr_farmboard_icons,2,px-90,py+3);
	draw_text(px-90+12,py-1,string(ds_list_find_value(global.socketsnumrocks,i)));
			
	draw_sprite(spr_farmboard_icons,1,px-140,py+3);
	draw_text(px-140+12,py-1,string(ds_list_find_value(global.socketsnumtrees,i)));
			
	draw_sprite(spr_farmboard_icons,3,px-200,py+3);
	var visitors = real(ds_list_find_value(global.socketsnumvisitors,i));
	if (visitors > 0)
	{
		draw_text(px-200+12, py-1, "BUSY");
	}
	else
	{
		draw_text(px-200+12, py-1, "OPEN");
	}
	c++;
}

// ----------------------------------------- Status messages
draw_set_font(global.FontStandardBold);
draw_set_color(c_black);
draw_set_alpha(1);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
var message = "";
if(refreshing)
{
	message = "Refreshing farms list...";
}
else if (fetching)
{
	message = "Connecting to farm..";
}
else if (error != "")
{
	message = error;
}
else if (c == 0)
{
	message = "No farms available to visit now.";
}
draw_text(sx + 196, sy - 14, string_upper(message));
draw_set_color(c_white);

// ----------------------------------------- Loading spinner
if (fetching || refreshing)
{
	wheelangle -= 5 * global.dt;
	if (wheelangle < 0)
	{
		wheelangle += 360;
	}
	else if (wheelangle > 360)
	{
		wheelangle -= 360;
	}
	draw_sprite_ext(spr_farmboard_loading_wheel,0, sx + 18, sy - 14, 1, 1, wheelangle, c_white, 1.0);
}
logga("FARMBOARD PANEL DRAW GUI END");