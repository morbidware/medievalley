event_inherited();
depth = -900;
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -800;

width = 480;
height = 500;

w_width = width;
w_height = height;

x = display_get_gui_width()/2;
y = display_get_gui_height()/2-display_get_gui_height();

sx = x;//updates in step event
sy = y;//updates in step event

btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
btnClose.sprite_index = spr_btn_close_silver;
btnClose.ox = width/2 - 18;
btnClose.oy = -height/2 + 18;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.depth = depth-1;

btnPrev = instance_create_depth(0,0,-10001,obj_btn_generic);
btnPrev.sprite_index = spr_farmboard_btn_prev;
btnPrev.ox = -43;
btnPrev.oy = 200;
btnPrev.caller = id;
btnPrev.evt = ev_user1;
btnPrev.depth = depth-1;

btnNext = instance_create_depth(0,0,-10001,obj_btn_generic);
btnNext.sprite_index = spr_farmboard_btn_next;
btnNext.ox = 43;
btnNext.oy = 200;
btnNext.caller = id;
btnNext.evt = ev_user2;
btnNext.depth = depth-1;

/*
btnHome = instance_create_depth(0,0,-10001,obj_btn_generic);
btnHome.sprite_index = spr_farmboard_btn_home;
btnHome.ox = -width/2 + 18;
btnHome.oy = -height/2 + 18;
btnHome.caller = id;
btnHome.evt = ev_user3;
btnHome.depth = depth-1;
*/

crono = 600;
refreshing = false;
sockw = 395;
sockh = 38;
toth = ds_list_size(global.sockets)*sockh;

fetching = false;
error = "";

from = 0;

currentIndex = 0;

if(global.online)
{
	global.timeout = global.maxtimeout;
	getSocketList();
}
else
{
	refreshing = false;
}

wheelangle = 0;
willgo = false;


logga("???????????????????????????????????");
logga("GLOBAL SOCKETS");
for(var i = 0; i < ds_list_size(global.sockets); i++)
{
	logga(string(ds_list_find_value(global.sockets,i)));
}
logga("GLOBAL SOCKETS USERNAMES");
for(var i = 0; i < ds_list_size(global.socketsusernames); i++)
{
	logga(string(ds_list_find_value(global.socketsusernames,i)));
}
logga("GLOBAL SOCKETS NUM PLANTS");
for(var i = 0; i < ds_list_size(global.socketsnumplants); i++)
{
	logga(string(ds_list_find_value(global.socketsnumplants,i)));
}
logga("???????????????????????????????????");