event_inherited();
logga("FARMBOARD PANEL STEP");
toth = (ds_list_size(global.sockets)-from)*sockh;
if(toth > sockh*10)
{
	toth = sockh*10;
}

sx = x - 198;
sy = y - 192;

if (!fetching && !refreshing)
{
	if(crono > 0)
	{
		crono -= global.dt;
	}
	if (crono <= 0)
	{
		crono = 600;
		global.timeout = global.maxtimeout;
		refreshing = true;
		getSocketList();
	}
}

// Manage timeout
if (fetching && global.timeout <= 0)
{
	fetching = false;
	error = "Could not connect to selected farm.";
	crono = 300;
}
if (refreshing && global.timeout <= 0)
{
	refreshing = false;
	error = "Could not refresh farms list.";
	crono = 300;
}
logga("FARMBOARD PANEL STEP END");