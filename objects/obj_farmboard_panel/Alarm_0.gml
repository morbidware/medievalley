///@description Connect to selected farm
//logga(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::FARMBOARD ALARM 0");
var s = ds_list_find_value(global.sockets, currentIndex);

// Don't connect if farm is busy
if (real(ds_list_find_value(global.socketsnumvisitors, currentIndex)) > 0)
{
	fetching = false;
	error = "This farm is already occupied.";
	logga("Selected farm is busy: "+string(s));
	exit;
}
// If we selected our same socket...
else if(s == global.socketid)
{	
	fetching = false;
	error = "Press the Home button on the dock to go back to your farm.";
	logga("Own farm selected: "+string(s)+" - use home button to go back there");
	exit;
}
// If we selected the socket we are already visiting...
else if(s == global.othersocketid)
{	
	fetching = false;
	error = "You already are in this farm.";
	logga("Already in that farm: "+string(s));
	exit;
}
// Otherwise, attempt connection on selected socket
else
{
	logga("Trying to connect to selected farm: "+string(s));
	scr_lockHudRequest(id);
	getUserData(s); // this waits for 'gmcallback_gotUserData'
}