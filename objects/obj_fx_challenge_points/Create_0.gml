if(global.ismobile)
{
	instance_destroy();
	exit;
}

event_inherited();

col = c_white;
str = "";
spd = 1;
fnt = global.Fontx2;
val = 0;
composed = false;
rot = random_range(-10,10);
x = display_get_gui_width()/2.0;
y = 50;
