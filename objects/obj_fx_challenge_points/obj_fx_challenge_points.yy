{
    "id": "e82e5914-be24-4dae-8503-b72e30c533a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_challenge_points",
    "eventList": [
        {
            "id": "d1ae7192-07d3-4469-bc4f-891151a041f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e82e5914-be24-4dae-8503-b72e30c533a1"
        },
        {
            "id": "0e59b3b8-67e4-4053-b204-4a9c005ddd62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e82e5914-be24-4dae-8503-b72e30c533a1"
        },
        {
            "id": "8fbf2aae-df79-4fd0-acf1-a30de010cf43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e82e5914-be24-4dae-8503-b72e30c533a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}