/// @description can be interacted
global.retval = false;
var handState = global.game.player.handState;
if(handState == HandState.Hammer)
{
	global.retval = true;
	interactionPrefixRight = "Dismantle";
}
else
{
	interactionPrefixRight = "";
}