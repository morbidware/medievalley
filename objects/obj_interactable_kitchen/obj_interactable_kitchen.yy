{
    "id": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_kitchen",
    "eventList": [
        {
            "id": "d7adf4f3-0c75-47f4-9506-29ca6e239f00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e"
        },
        {
            "id": "9efb565a-9478-40a0-953f-71df9f3e144f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e"
        },
        {
            "id": "e1cc1064-fb7c-4499-8a30-2e2a5d35b4a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e"
        },
        {
            "id": "696c29a9-b694-4617-8795-6a64163ed588",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e"
        },
        {
            "id": "12860eb0-d99b-48ac-ad3d-dfafbee0efa1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e"
        },
        {
            "id": "3da9afdc-bb8d-4658-bd6a-80f9a4dec6b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "9e3b7da5-8ad2-4d20-b51c-ed79b60d3a3e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9806e477-9f89-40c3-a4ae-54a424c401b0",
    "visible": true
}