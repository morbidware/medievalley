/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

if(craftCrono > 0)
{
	if (!audio_is_playing(snd_kitchen))
	{
		audio_play_sound(snd_kitchen,0,true);
	}
	
	craftCrono --;
	
	fxcrono ++;
	if(fxcrono mod(4) == 0)
	{
		instance_create_depth(x-24,y-30,-(y+1),obj_fx_smoke);
		repeat(2)
		{
			instance_create_depth(x-21+random_range(-4,4),y+4+random_range(-4,4),-(y+1),obj_fx_spark);
		}
	}
	
	/*
	t += ts;
	if(t > 8)
	{
		t = 0.0;
	}
	*/
	if(craftCrono <= 0)
	{
		repeat(3)
		{
			instance_create_depth(x,y+32,-(y+33),obj_fx_cloudlet);
		}
		scr_gameItemCreate(x,y+32,obj_pickable,craftid);
		craftid = "";
		audio_stop_sound(snd_kitchen);
		/*
		t = 0.0;
		ts = 0;
		*/
	}
}