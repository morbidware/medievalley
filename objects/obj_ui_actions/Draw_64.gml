if (!global.showhints)
{
	exit;
}

draw_set_font(global.FontStandardBoldOutline);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_color(c_white);
draw_set_alpha(1);

// --------------------- DRAW INTERACTABLES ACTION ---------------------
with (obj_interactable)
{
	if(highlight)
	{
		var canBeInteracted = event_perform_ret(id, ev_other, ev_user1);
		if(global.otheruserid != 0)
		{
			// stop visitor interaction on items different from the following:
			if (string_pos("plant",itemid) == 0 &&
				string_pos("tree",itemid) == 0 &&
				string_pos("pine",itemid) == 0 &&
				string_pos("rock",itemid) == 0 &&
				string_pos("wildgrass",itemid) == 0 &&
				string_pos("pallet",itemid) == 0)
			{
				canBeInteracted = false;
			}
		}
	
		if(canBeInteracted && !global.ismobile)
		{
			//draw both the interactions if available
			if (string_length(interactionPrefixLeft) > 0 && string_length(interactionPrefixRight) > 0)
			{
				var stringLeft = interactionPrefixLeft + " " + name;
				draw_text(pos_get_relative_x(), pos_get_relative_y(), stringLeft);
				draw_sprite_ext(spr_mouse_left, 0, pos_get_relative_x() - string_width(stringLeft)/2-12, pos_get_relative_y()-2, 2, 2, 0, c_white, 1);
			
				var stringRight = interactionPrefixRight + " " + name;
				draw_text(pos_get_relative_x(), pos_get_relative_y()+20, stringRight);
				draw_sprite_ext(spr_mouse_right, 0, pos_get_relative_x() - string_width(stringRight)/2-12, pos_get_relative_y()+22, 2, 2, 0, c_white, 1);
			}
			//draw the first interaction available
			else
			{
				var str = interactionPrefixLeft + " " + name;
				var spr = spr_mouse_left;
				if (string_length(interactionPrefixLeft) == 0)
				{
					str = interactionPrefixRight + " " + name;
					spr = spr_mouse_right;
				}
				draw_text(pos_get_relative_x(), pos_get_relative_y()+10, str);
				draw_sprite_ext(spr, 0, pos_get_relative_x() - string_width(str)/2-12, pos_get_relative_y()+10, 2, 2, 0, c_white, 1);
			}
		}
		else if(canBeInteracted && global.ismobile)
		{
			if(string_length(interactionPrefixLeft) != 0)
			{
				var actionString = interactionPrefixLeft + " " + name;	
			}
			else if(string_length(interactionPrefixRight) != 0)
			{
				var actionString = interactionPrefixRight + " " + name;
			}
			draw_text(pos_get_relative_x(), pos_get_relative_y()+10, actionString);
		}
	}
}

// --------------------- DRAW PICKABLES ACTION ---------------------
with (obj_pickable)
{
	if(highlight)
	{
		var str = "";
	    if(quantity == 1)
	    {
			str = "Pick Up " + name;
	        draw_text(other.px, other.py, str);
			draw_sprite_ext(spr_mouse_left, 0, other.px - string_width(str)/2 - 12, other.py, 2, 2, 0, c_white, 1);
	    }
	    else
	    {
			str = "Pick Up " + name + " x" + string(quantity);
	       draw_text(other.px, other.py, str);
			draw_sprite_ext(spr_mouse_left, 0, other.px - string_width(str)/2 - 12, other.py, 2, 2, 0, c_white, 1);
	    }
	}
}