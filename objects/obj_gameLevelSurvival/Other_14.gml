/// @description Create mob
// You can write your code in this editor
if(string_length(global.mobsstring) > 0)
{
	
	var mobs = ds_list_create();
	ds_list_read(mobs, global.mobsstring); 
	show_debug_message("Reading saved mobs");
	logga("Reading saved mobs");
	logga("mobs list length " + string(ds_list_size(mobs)));
	for(var i = 0; i < ds_list_size(mobs); i++)
	{
		var str = ds_list_find_value(mobs,i);
		
		var obj = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,obj+":","");
		
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		
		var lf = str;
		
		var inst = instance_create_depth(real(px),real(py),0,scr_asset_get_index(obj));
		inst.life = real(lf);
	}
}
