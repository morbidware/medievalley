{
    "id": "48dfe77f-6625-44d7-b72d-e9263dd051bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gameLevelSurvival",
    "eventList": [
        {
            "id": "6f5f77de-33aa-4b1b-8f7f-22db32ebb3f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "48dfe77f-6625-44d7-b72d-e9263dd051bb"
        },
        {
            "id": "fa9408d2-94f1-4784-9a8e-5f7961575de8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48dfe77f-6625-44d7-b72d-e9263dd051bb"
        },
        {
            "id": "fd602952-8ca8-4605-8272-7a937c92b9bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "48dfe77f-6625-44d7-b72d-e9263dd051bb"
        },
        {
            "id": "af94eb2b-7d00-4ba5-bafa-2c619860a43b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 14,
            "eventtype": 7,
            "m_owner": "48dfe77f-6625-44d7-b72d-e9263dd051bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "098fe6c0-260a-4ee1-9cc0-e3f1904a693d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}