/// @description Change scene
// You can write your code in this editor
event_inherited();
//logga("Start GameLevelSurvival");
if(instance_exists(obj_console)){exit;}

with(obj_char)
{
	other.x = x;
	other.y = y;
}

if(global.dayPhase == DayPhase.Night && global.mobowner && repopulate)
{
	scr_populate_with(obj_pickable,"flint",16,GroundType.Soil,false,true);
	scr_populate_with(obj_pickable,"redbeansseeds",5,GroundType.Grass,false,true);
	
	repopulate = false;
	flintcount = 0;
	seedcount = 0;
	
	if(global.online)scr_storePickablesSurvival();
}

//logga("End GameLevelSurvival");