/// @description Init
// You can write your code in this editor
event_inherited();

// IMPORTANT: set seed for wildlands, so things will always stay the same across plays
random_set_seed(global.userid);
randomize();

survivalground = instance_create_depth(0,0,0,obj_survivalGround);
repopulate = false;
flintcount = 0;
seedcount = 0;
//logga("survivalground.hspx " + string(survivalground.hspx));
//logga("survivalground.hspy " + string(survivalground.hspy));
/*instance_create_depth(survivalground.hspx,survivalground.hspy,0,obj_fadein);*/
instance_create_depth(room_width*0.5,room_height*0.5,0,obj_fadein);

if(string_length(global.interactables) > 0)
{
	var interactables = ds_list_create();
	ds_list_read(interactables, global.interactables); 
	show_debug_message("Reading saved interactables");
	logga("Reading saved interactables");
	logga("interactables list length " + string(ds_list_size(interactables)));
	for(var i = 0; i < ds_list_size(interactables); i++)
	{
		var str = ds_list_find_value(interactables,i);
		
		var obj = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,obj+":","");
		
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		//growcrono
		var gc = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gc+":","");
		//growstage
		var gs = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gs+":","");
		//quantity
		var gq = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gq+":","");
		//durability
		var gd = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gd+":","");
		//gridded
		var gg = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gg+":","");
		//life
		var gl = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,gl+":","");
		//startingslot
		var  gss = str;
		
		/*
		logga("iid is " + iid);
		logga("px is " + px);
		logga("py is " + py);
		logga("gc is " + gc);
		logga("gs is " + gs);
		logga("gq is " + gq);
		logga("gd is " + gd);
		logga("gg is " + gg);
		logga("gl is " + gl);
		logga("-----------------------");
		*/
		
		var growcrono = real(gc);
		if (round(growcrono) > 1)
		{
			growcrono = clamp(growcrono - global.awayFromFarm, 1, 99999999);
		}
		var inst = scr_gameItemCreate(real(px), real(py), scr_asset_get_index(obj), iid,real(gq), growcrono, real(gs), real(gd), real(gg), real(gl));
		if(real(gss) > -1)
		{
			inst.startingslot = real(gss);
		}
	}
}

if(string_length(global.pickables) > 0)
{	
	show_debug_message("Reading saved pickables");
	logga("Reading saved pickables");
	//logga(global.pickables);
	var pickables = ds_list_create();
	ds_list_read(pickables, global.pickables); 
	logga("pickables list length " + string(ds_list_size(pickables)));
	for(var i = 0; i < ds_list_size(pickables); i++)
	{
		var str = ds_list_find_value(pickables,i);
		var px = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,px+":","");
		var py = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,py+":","");
		var iid = string_copy(str,0,string_pos(":",str)-1);
		str = string_replace(str,iid+":","");
		var qu = str;
		/*
		logga("str is " + str);
		logga("px is " + px);
		logga("py is " + py);
		logga("iid is " + iid);
		logga("qu is " + qu);
		logga("-----------------------");
		*/
		scr_gameItemCreate(real(px),real(py),obj_pickable,iid,real(qu));
	}
}


player = instance_create_depth(survivalground.hspx+8,survivalground.hspy,0,obj_char);


//player = instance_create_depth(room_width*0.5+8,room_height*0.5,0,obj_char);
var cam = instance_create_depth(player.x,player.y,0,obj_camera);

//scr_gameItemCreate(survivalground.hspx,survivalground.hspy, obj_interactable_roadsign, "roadsign");

audio_play_sound(snd_white_noise,0,true);

player.prevx = player.x;
player.prevy = player.y;
cam.x = player.x;
cam.y = player.y;


audio_play_sound(bgm_wild,0,true);

joinRoom(string(global.landid));
global.lastjoinedroom = string(global.landid);
getClientsInIsland(global.lastjoinedroom);

logga("finished gameLevelSurvival");