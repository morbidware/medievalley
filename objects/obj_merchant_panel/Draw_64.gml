draw_sprite_ext(spr_merchant_bg, 0, x, y, 2, 2, 0, c_white, 1);

draw_set_font(global.FontStandardOutline);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_middle);

draw_text(x-276,y-91,"YOU");
draw_text(x-68,y-91,"SELL");
draw_text(x-68,y+19,"BUY");
draw_text(x+100,y-91,"MERCHANT");

draw_set_halign(fa_right);
if (currentvalue > 0)
{
	draw_text(x+4,y+130,"+"+string(currentvalue));
}
else
{
	if (player.gold + currentvalue >= 0)
	{
		draw_set_color(c_white);
	}
	else
	{
		draw_set_color(c_red);
	}
	draw_text(x+4,y+130,string(currentvalue));
}
draw_set_halign(fa_center);
draw_set_color(c_white);
draw_set_font(global.FontStandardOutline);
draw_text(x+2,y+150,outcometext);
draw_set_halign(fa_left);
