/// @description Create trade items
logga("obj_merchant_panel User Event 3");
ds_list_clear(tradeObjects);
instance_destroy(obj_trade);
/*
if (ds_map_size(merchantInventory) > 0)
{
	var key = ds_map_find_first(merchantInventory);
	repeat (ds_map_size(merchantInventory))
	{
		var tradeitem = instance_create_depth(depth-1,-100,-100,obj_trade);
		tradeitem.status = 2;
		tradeitem.itemid = key;
		tradeitem.itemname = scr_getGameItemValue(key,"name");
		tradeitem.quantity = ds_map_find_value(merchantInventory,key);
		tradeitem.maxquantity = scr_getGameItemValue(tradeitem.itemid,"maxquantity");
		tradeitem.value = round(scr_getGameItemValue(tradeitem.itemid,"value"));
		tradeitem.sprite_index = scr_asset_get_index("spr_pickable_"+scr_getGameItemValue(tradeitem.itemid,"spritename"));
		tradeitem.parentdepth = depth-1;
		ds_list_add(tradeObjects,tradeitem);
	
		key = ds_map_find_next(merchantInventory,key);
	}
}
*/

if (ds_grid_height(merchantInventory) > 0)
{
	var i = 0;
	repeat (ds_grid_height(merchantInventory))
	{
		var tradeitem = instance_create_depth(depth-1,-100,-100,obj_trade);
		tradeitem.status = 2;
		tradeitem.itemid = ds_grid_get(merchantInventory,0,i);
		tradeitem.itemname = scr_getGameItemValue(tradeitem.itemid,"name");
		tradeitem.quantity = ds_grid_get(merchantInventory,1,i);
		tradeitem.maxquantity = scr_getGameItemValue(tradeitem.itemid,"maxquantity");
		tradeitem.value = round(scr_getGameItemValue(tradeitem.itemid,"value"));
		tradeitem.sprite_index = scr_asset_get_index("spr_pickable_"+scr_getGameItemValue(tradeitem.itemid,"spritename"));
		tradeitem.parentdepth = depth-1;
		
		if (tradeitem.sprite_index != spr_missing)
		{
			ds_list_add(tradeObjects,tradeitem);
		}
		i++;
	}
}
// Create items for the player (from inventory and backpack, 10+20 slots)
var inv = instance_find(obj_playerInventory,0);
for (var i = 0; i < global.inv_backpacklimit; i++)
{
	var item = ds_grid_get(inv.inventory,i,0);
	if (item != -1)
	{
		var tradeitem = instance_create_depth(depth-1,-100,-100,obj_trade);
		tradeitem.status = 0;
		tradeitem.itemid = item.itemid;
		tradeitem.itemname = scr_getGameItemValue(item.itemid,"name");
		tradeitem.quantity = item.quantity;
		tradeitem.originalslot = item.slot;
		tradeitem.maxquantity = real(scr_getGameItemValue(tradeitem.itemid,"maxquantity"));
		tradeitem.value = round(real(scr_getGameItemValue(tradeitem.itemid,"value")));
		tradeitem.durability = item.durability;
		tradeitem.value = scr_getItemValueForDurability(tradeitem.itemid, tradeitem.value, tradeitem.durability);
		tradeitem.sprite_index = scr_asset_get_index("spr_pickable_"+scr_getGameItemValue(tradeitem.itemid,"spritename"));
		tradeitem.parentdepth = depth-1;
		ds_list_add(tradeObjects,tradeitem);
	}
}

logga("obj_merchant_panel User Event 3 end");