event_inherited();

// keep autosave timers from saving
global.saveuserdatatimer = 0;
global.saveitemstimer = 0;

if (instance_exists(btnClose))
{
	btnClose.ox = 296;
	btnClose.oy = -116;
}
if (instance_exists(btnApply))
{
	btnApply.ox = 42;
	btnApply.oy = 128;
}
if (instance_exists(btnCancel))
{
	btnCancel.ox = 68;
	btnCancel.oy = 128;
}

if (outcometextTimer > 0)
{
	outcometextTimer -= global.dt;
}
else
{
	outcometext = "";
}

// Align trade objects to grid and calculate values
var player = 0;
var sell = 0;
var merchant = 0;
var buy = 0;
var value = 0;
var col = 0;
var row = 0;
var obj = noone;
for (var i = 0; i < ds_list_size(tradeObjects); i++)
{
	obj = ds_list_find_value(tradeObjects,i);
	switch (obj.status)
	{
		case 0:
			row = 0;
			col = player;
			while (col > 4) {
				col -= 5;
				row++;
			}
			obj.x = x + startx_player + (col * 38);
			obj.y = y + starty_player + (row * 38);
		
			player++;
			break;
			
		case 1:
			row = 0;
			col = sell;
			while (col > 3) {
				col -= 4;
				row++;
			}
			obj.x = x + startx_sell + (col * 38);
			obj.y = y + starty_sell + (row * 38);
			
			sell++;
			value += floor(obj.value * obj.quantity * sellpercent);
			break;
			
		case 2:
		
			row = 0;
			col = merchant;
			while (col > 4) {
				col -= 5;
				row++;
			}
			obj.x = x + startx_merchant + (col * 38);
			obj.y = y + starty_merchant + (row * 38);
			
			merchant++;
			break;
			
		case 3:
		row = 0;
			col = buy;
			while (col > 3) {
				col -= 4;
				row++;
			}
			obj.x = x + startx_buy + (col * 38);
			obj.y = y + starty_buy + (row * 38);
			
			buy++;
			value -= round (obj.value * obj.quantity);
			break;
	}
}
currentvalue = value;
selling = sell;
buying = buy;

//show_debug_message(string(instance_number(obj_trade)));