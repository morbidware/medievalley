{
    "id": "df84539b-26f3-4257-b0df-ea6281e58f99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_merchant_panel",
    "eventList": [
        {
            "id": "3aef1bf7-7b31-4147-aecf-982a47f80e4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "e27d7fb8-cdb7-431c-8b6f-2bc4aa57833a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "1874593a-ea1e-4846-b25f-9cad3ca741ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "6d69d668-18cf-43dc-b373-e33a6599d7c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "9ddb4ee4-a7bf-493d-88bb-9790a0aa8ca7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "62dde1a0-2042-4a69-a682-6ee46ff405e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "458e8c87-1a8b-48d4-afe0-e2e1672534ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "b3c5e68e-621c-4aa0-8886-c7d85e4ca580",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        },
        {
            "id": "b10a8b2d-4033-431f-afb2-7528ac064960",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 7,
            "m_owner": "df84539b-26f3-4257-b0df-ea6281e58f99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}