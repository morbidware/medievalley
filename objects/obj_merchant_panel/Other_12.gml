/// @description Cancel transaction

for (var i = 0; i < ds_list_size(tradeObjects); i++)
{
	var obj = ds_list_find_value(tradeObjects,i);
	with (obj)
	{
		if (status == 1 || status == 3)
		{
			quantitytomove = quantity;
			event_perform(ev_other,ev_user0);
			i--;
		}
	}
}