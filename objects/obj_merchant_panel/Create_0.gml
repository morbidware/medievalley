event_inherited();

depth = -900;
player = instance_find(obj_char,0);
var darken = instance_create_depth(0,0,0,obj_ui_darken);
darken.owner = id;
darken.depth = -800;
scr_lockHudRequest(id);

x = display_get_gui_width()/2;
y = display_get_gui_height()/2;

merchantInventory = noone;
tradeObjects = ds_list_create();

// TEST INVENTORY -------
/*
ds_map_add(merchantInventory,"food_apple",4);
ds_map_add(merchantInventory,"redbeansseeds",2);
ds_map_add(merchantInventory,"food_olive",10);
ds_map_add(merchantInventory,"woodlog",3);
*/
// ----------------------

// Wait two frames to create trade items, so to have time to fill the merchantInventory
alarm_set(0,2);

startx_player = -262;
starty_player = -56;

startx_sell = -56;
starty_sell = -56;

startx_merchant = 112;
starty_merchant = -56;

startx_buy = -56;
starty_buy = +54;

selling = 0;
buying = 0;
maxsell = 8;
maxbuy = 8;
currentvalue = 0;
sellpercent = 0.3;

btnClose = instance_create_depth(0,0,-10001,obj_btn_generic);
btnClose.caller = id;
btnClose.evt = ev_user0;
btnClose.sprite_index = spr_btn_close_orange;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}

btnApply = instance_create_depth(0,0,-10001,obj_btn_generic);
btnApply.caller = id;
btnApply.evt = ev_user1;
btnApply.sprite_index = spr_btn_merchant_apply;

btnCancel = instance_create_depth(0,0,-10001,obj_btn_generic);
btnCancel.caller = id;
btnCancel.evt = ev_user2;
btnCancel.sprite_index = spr_btn_merchant_cancel;

outcometext = "";
outcometextTimer = 0;

w_width = sprite_get_width(spr_merchant_bg);
w_height = sprite_get_height(spr_merchant_bg);