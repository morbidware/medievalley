/// @description Apply transaction

var inv = instance_find(obj_playerInventory,0);


// ------------------------------------ NULL CHECK
var transactionitems = 0;
for (var i = 0; i < tradeObjects; i++)
{
	var item = ds_list_find_value(tradeObjects, i);
	if (item <= 0 || item == noone || item == undefined)
	{
		continue;
	}	
	if (item.status == 1 || item.status == 3)
	{
		transactionitems++;
	}
}
if (transactionitems == 0)
{
	return;
}

// ------------------------------------ INVENTORY SPACE CHECK
var freespaces = global.inv_backpacklimit;
for (var i = 0; i < tradeObjects; i++)
{
	var item = ds_list_find_value(tradeObjects, i);
	if (item <= 0 || item == noone || item == undefined)
	{
		continue;
	}
	// Space is occupied if an item is in inventory or in selling space
	if (item.status == 0 || item.status == 3)
	{
		freespaces--;
	}
	
	// TODO: we may also check here the available space in case of merging items
	// but it's pretty complex and not mandatory (space chek is still ok now)
}
if (freespaces < 0)
{
	outcometext = "Not enough space.";
	outcometextTimer = 180;
	return;
}

// ------------------------------------ MONEY CHECK
if (player.gold + currentvalue >= 0)
{
	outcometext = "Thank you.";
	outcometextTimer = 180;
}
else
{
	outcometext = "Not enough money.";
	outcometextTimer = 180;
	return;
}

// ------------------------------------ PERFORM TRANSACTION
var testgrid = ds_grid_create(ds_grid_width(merchantInventory),ds_grid_height(merchantInventory));
ds_grid_copy(testgrid,merchantInventory);
for (var i = 0; i < ds_list_size(tradeObjects); i++)
{
	logga("check tradeobject " + string(i));
	var tradeobj = ds_list_find_value(tradeObjects,i);
	
	// Sell
	if (tradeobj.status == 1) // 1: object in selling area
	{
		for (var j = 0; j < global.inv_backpacklimit; j++)
		{
			var item = ds_grid_get(inv.inventory,j,0);
			if (item <= 0)
			{
				continue;
			}
			if (item.itemid == tradeobj.itemid && item.quantity >= tradeobj.quantity && item.slot == tradeobj.originalslot)
			{
				item.quantity -= tradeobj.quantity;
				if (item.quantity <= 0)
				{
					instance_destroy(scr_removeItemFromInventory(item.slot));
				}
				instance_destroy(tradeobj);
				ds_list_delete(tradeObjects,i);
				i--;
				break;
			}
		}
	}
	
	// Buy
	else if (tradeobj.status == 3) // 3: object in buying area
	{
		var item = scr_gameItemCreate(-100,-100,obj_pickable,tradeobj.itemid,tradeobj.quantity);
		logga("putting obj in inventory");
		scr_putItemIntoInventory(item,InventoryType.All,-1);
		logga("finished putting obj in inventory");
		instance_destroy(item);
		
		var candidatey = 0; 
		repeat(ds_grid_height(testgrid))
		{
			if(ds_grid_get(testgrid,0,candidatey) == tradeobj.itemid)
			{
				break;
			}
			else
			{
				candidatey ++;
			}
		}
		logga("bo");
		var quantityleft = ds_grid_get(testgrid,1,candidatey) - tradeobj.quantity;
		logga("1");
		if (quantityleft > 0)
		{
			logga("11");
			//ds_map_replace(merchantInventory, tradeobj.itemid,quantityleft);
			ds_grid_set(testgrid,1,candidatey,quantityleft);
			logga("111");
		}
		else
		{
			logga("2");
			//ds_map_delete(merchantInventory, tradeobj.itemid)
			logga("ds_grid_height(merchantInventory): " + string(ds_grid_height(merchantInventory)));
			logga("candidatey: " + string(candidatey));
			repeat(ds_grid_height(testgrid) - candidatey-1)
			{
				logga("alpha");
				ds_grid_set(testgrid,0,candidatey,ds_grid_get(testgrid,0,candidatey+1));
				ds_grid_set(testgrid,1,candidatey,ds_grid_get(testgrid,1,candidatey+1));
				candidatey++;
				logga("beta");
			}
			logga("22");
			ds_grid_resize(testgrid,2,clamp(ds_grid_height(testgrid)-1,1,99));
			logga("222");
		}
		show_debug_message(tradeobj.id);
		show_debug_message(string(ds_list_size(tradeObjects)));
		show_debug_message(string(ds_grid_height(merchantInventory)));
		instance_destroy(tradeobj);
		ds_list_delete(tradeObjects,i);
		i--;
		logga("boo");
	}
}

player.gold += currentvalue;
if (currentvalue >= 0)
{
	global.stat_goldearned += abs(currentvalue);
}
else
{
	global.stat_goldspent += abs(currentvalue);
}

// Refresh merchant UI and save
event_perform(ev_other,ev_user3);
scr_storeItems();
scr_storeUserdata();