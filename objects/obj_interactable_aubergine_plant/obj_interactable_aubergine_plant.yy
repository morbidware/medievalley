{
    "id": "5698c6ed-6e44-4e05-8926-adc4447c0af2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_aubergine_plant",
    "eventList": [
        {
            "id": "a80aada1-cb04-4112-8930-921f2fef4451",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "5698c6ed-6e44-4e05-8926-adc4447c0af2"
        },
        {
            "id": "cd29d6f6-29db-47f1-bf45-2a597829d522",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5698c6ed-6e44-4e05-8926-adc4447c0af2"
        },
        {
            "id": "a15e05e3-4db6-4b55-8107-2ec7b14d47dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "5698c6ed-6e44-4e05-8926-adc4447c0af2"
        },
        {
            "id": "edcf7c00-372d-440c-a120-9036a9a5d106",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "5698c6ed-6e44-4e05-8926-adc4447c0af2"
        },
        {
            "id": "2a7a3b3c-4d7b-4a25-95d8-84855cd46d64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "5698c6ed-6e44-4e05-8926-adc4447c0af2"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
    "visible": true
}