{
    "id": "eb3a0f65-cc76-4819-91b5-159ecdb3204d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crafting_cat_furniture",
    "eventList": [
        {
            "id": "b7323e9e-4f8e-461c-9231-c672d3370acd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eb3a0f65-cc76-4819-91b5-159ecdb3204d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5cfc131d-4caa-4df2-86f5-c4aeda9901ac",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
    "visible": true
}