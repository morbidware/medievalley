{
    "id": "8bad1958-f211-4730-ab23-0b8d70dd9bd3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mobtool_bow",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0fe9bf88-7784-470a-8861-bf4456d19bd1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "39392d95-480d-4b27-ab12-93eba590c976",
    "visible": true
}