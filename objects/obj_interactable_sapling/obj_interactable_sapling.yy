{
    "id": "819a1df4-69ea-4fa2-8895-4c1bb91b686c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_sapling",
    "eventList": [
        {
            "id": "5f05a29a-343f-41f4-9364-d1c8138f994b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "819a1df4-69ea-4fa2-8895-4c1bb91b686c"
        },
        {
            "id": "e068cbfb-2984-48a5-823f-84774fa6de18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "819a1df4-69ea-4fa2-8895-4c1bb91b686c"
        },
        {
            "id": "b3988ce9-3e97-48c1-a26b-d0e70cdff6ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "819a1df4-69ea-4fa2-8895-4c1bb91b686c"
        },
        {
            "id": "8d06f337-ef1c-47e5-a5ca-8ab1301b73ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "819a1df4-69ea-4fa2-8895-4c1bb91b686c"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4cfb8be6-909a-46bc-9abb-cd494464577b",
    "visible": true
}