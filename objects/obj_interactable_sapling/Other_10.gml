/// @description finish interaction
// You can write your code in this editor


if(room == roomChallenge)
{
	global.finishedInteractions ++;
	if(hard)
	{
		with(obj_char)
		{
			scr_setState(id, ActorState.Hit);
		}
	}
	else
	{
		with(obj_char)
		{
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.finishedInteractionsValue;
		}
	}
	with(obj_gameLevelChallenge)
	{
		if(challengeState == 1)
		{
			total ++;
			//tempGold += other.growstage * other.lifeperstage;
		}
	}
}

if (global.otheruserid != 0)
{
	exit;
}
if(global.isSurvival)
{
	if(!isNet)
	{
		scr_dropItems(id,"sapling","sapling");
	}
	isNet = false;
}
else
{
	scr_dropItems(id,"sapling","sapling");
}

instance_destroy();