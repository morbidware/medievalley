///@description Movement
event_inherited();

// keep mailbox enabled

if (status == Status.WaitFarm)
{
	exit;
}
else if (status == Status.Wait)
{
	speed = 0;
	delay -= global.dt;
	if (delay <= 0)
	{
		started = true;
		scr_camera_set_view_target(id);
		status = Status.Arrive;
		scr_lockHudRequest(id);
	}
	exit;
}
else if (status == Status.Arrive)
{
	speed = clamp(point_distance(x,y,mailbox.x,mailbox.y-25) / 40, 0.5, 3);
	direction = point_direction(x,y,mailbox.x,mailbox.y-25);
	
	
	if (point_distance(x,y,mailbox.x,mailbox.y-25) < 2)
	{
		speed = 0;
		image_speed = 0.07 * global.dt;
		sprite_index = spr_mail_pidgeon_idle;
		x = mailbox.x;
		y = mailbox.y-25;
		status = Status.Stay;
		audio_stop_sound(sndflap);
	}
}

else if (status == Status.Stay)
{
	if (wait > 0)
	{
		wait -= global.dt;
	}
	else if (wait > -10)  
	{
		wait = -100;
		scr_camera_set_view_target(instance_find(obj_char,0));
		scr_lockHudLeave(id);
	}	
}

else if (status == Status.Depart)
{
	image_speed = 0.7 * global.dt;
	sprite_index = spr_mail_pidgeon_fly;
	speed = clamp(point_distance(x,y,mailbox.x,mailbox.y-25) / 40, 0.5, 3) * global.dt;
	direction = point_direction(x,y,startx,starty);
	if (x < 0 || y < 0 || x > room_width+16 || y > room_height+16)
	{
		audio_stop_sound(sndflap);
		instance_destroy(id);
	}
}

scr_setDepth(DepthType.Elements);
depth -= 32;