{
    "id": "d15693a5-5de5-4615-a12b-41444613a711",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_wildlife_mail_pidgeon",
    "eventList": [
        {
            "id": "a106387f-765f-4e06-b2f5-627b5f775ded",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d15693a5-5de5-4615-a12b-41444613a711"
        },
        {
            "id": "2585b11a-2444-456f-93aa-a3cbcbeffdc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d15693a5-5de5-4615-a12b-41444613a711"
        },
        {
            "id": "4059d44d-9409-41c4-a3d7-3826481775eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "d15693a5-5de5-4615-a12b-41444613a711"
        },
        {
            "id": "e6784cd2-40eb-49db-abf6-030c110fcb10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "d15693a5-5de5-4615-a12b-41444613a711"
        },
        {
            "id": "38faffdf-6ceb-43b6-a1fb-570656b43f84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d15693a5-5de5-4615-a12b-41444613a711"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "389843b1-8fa4-42bf-98e7-2828f05bb9b0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
    "visible": true
}