/// @description Start animation

//start from one of the sides of the screen
if (choose(0,1) == 0)
{
	image_xscale = 1;
	startx = -64;
	starty = mailbox.y + 32 + random(128);
}
else
{
	image_xscale = -1;
	startx = room_width+64;
	starty = mailbox.y + 32 + random(128);
}

x = startx;
y = starty;

status = Status.Wait;
delay = 1;
wait = 120;