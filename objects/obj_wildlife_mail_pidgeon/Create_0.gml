///@description Init
event_inherited();
offsety = 24; // offset for shadow
delay = 1;

enum Status { WaitFarm, Wait, Arrive, Stay, Depart };
status = Status.Wait;

mailbox = instance_find(obj_interactable_mailbox,0);
startx = 0;
starty = 0;
wait = 0;

alivefrom = 0.0;
aliveto = 1.0;

image_speed = 0.7;
sprite_index = spr_mail_pidgeon_fly;

//if not in the farm, wait until the player goes back there
if (room_get_name(room) != "roomFarm")
{
	persistent = true;
	visible = false;
	status = Status.WaitFarm;
}
else
{
	//if another pidgeon is here, make it go away
	with (obj_wildlife_mail_pidgeon)
	{
		if (id != other.id && status == Status.Stay)
		{
			status = Status.Depart;
			image_xscale = -image_xscale;
		}
	}
	event_perform(ev_other,ev_user0);
}

sndflap = audio_play_sound(snd_flap_wings,0,true);