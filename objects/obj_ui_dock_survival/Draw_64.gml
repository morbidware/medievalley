var clockx = x+49;
var clocky = y+77;

// Gold counter
/*
draw_sprite_ext(spr_money_label,0,x+50,y+6,2,2,0,c_white,1);
draw_set_font(global.FontStandardOutline);
draw_set_color(c_white);
draw_set_valign(fa_middle);
draw_set_halign(fa_right);
draw_text(x+90,y+7,string(hero.gold));
*/
draw_set_halign(fa_left);
draw_sprite_ext(spr_dock_survival,0,x,y+24,2,2,0,c_white,1);
draw_sprite_ext(spr_dock_stats_bg_survival,0,x+51-16-8,y+118,2,2,0,c_white,1);
draw_sprite_ext(spr_dock_sundial_survival,0,clockx,clocky,2,2,0,c_white,1);

// Sunlight dial cursor
var a = (global.dayprogress) * (pi*2.0) - (pi/2.0);
draw_set_color(c_black);
draw_set_alpha(0.5);
var cosa = cos(a);
var sina = sin(a);
draw_line_width(clockx,clocky,clockx+cosa*12,clocky+sina*12,4);
draw_line_width(clockx,clocky,clockx+cosa*6,clocky+sina*6,6);
draw_set_alpha(1.0);
draw_set_color(c_white);
draw_sprite_ext(spr_dock_sundial_dial,0,clockx,clocky-2,2,2,0,c_white,1);

// Stamina and life
var h = sprite_get_height(spr_dock_life_bar);
var l = round((h / 100) * hero.life);
draw_sprite_part_ext(spr_dock_life_bar, 1, 0, h-l, 10, l, x+53-16-8, y+132+((h-l)*2), 2, 2, c_white, 1);
var s = round((h / 100) * hero.stamina);
draw_sprite_part_ext(spr_dock_stamina_bar, 1, 0, h-s, 10, s, x+69-16-8, y+132+((h-s)*2), 2, 2, c_white, 1);
var st = round((h / 100) * hero.stomach);
draw_sprite_part_ext(spr_dock_stomach_bar, 1, 0, h-st, 10, st, x+69-16-8+16, y+132+((h-st)*2), 2, 2, c_white, 1);

// Align buttons
btnSet.ox = 44;
btnSet.oy = 6;
//
btnSave.ox = 44+26;
btnSave.oy = 6;