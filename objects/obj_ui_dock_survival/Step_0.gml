// keep dock hidden as long as the intro is active
x -= global.dt;
x = clamp(x,scr_adaptive_x(960)-88-12, 99999);

if(btnSet.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnSet.x,btnSet.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "SETTINGS (O)";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.x == btnSet.x)
		{
			instance_destroy(mi);
		}
	}
}

if(btnSave.image_index == 1)
{
	if(!instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_create_depth(btnSave.x,btnSave.y,0,obj_ui_dock_buttoninfo);
		mi.desc = "SAVE DATA";
	}
}
else
{
	if(instance_exists(obj_ui_dock_buttoninfo))
	{
		var mi = instance_find(obj_ui_dock_buttoninfo,0);
		if(mi.x == btnSave.x)
		{
			instance_destroy(mi);
		}
	}
}