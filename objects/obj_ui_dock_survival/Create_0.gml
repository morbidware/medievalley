if(instance_number(obj_ui_dock_survival) > 1)
{
	logga("DUPLICATED UI DOCK!");
	instance_destroy();
	exit;
}

x = scr_adaptive_x(960)-88-12;
y = 12;
depth = 0;

hero = instance_find(obj_char,0);

// Settings
btnSet = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnSet.sprite_index = spr_dock_btn_settings_survival;
btnSet.caller = id;
btnSet.evt = ev_user0;

// Save
btnSave = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnSave.sprite_index = spr_dock_btn_save_survival;
btnSave.caller = id;
btnSave.evt = ev_user2;
