{
    "id": "3535d879-afd2-43be-b077-00ac25d503d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_dock_survival",
    "eventList": [
        {
            "id": "91d88046-abc8-4d4f-aee8-eb6ecbd69ac7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3535d879-afd2-43be-b077-00ac25d503d4"
        },
        {
            "id": "8a390d97-82bc-45ef-9d0f-7b0bcea19695",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3535d879-afd2-43be-b077-00ac25d503d4"
        },
        {
            "id": "66d69428-f2b4-4f5a-8284-5727c58edc79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3535d879-afd2-43be-b077-00ac25d503d4"
        },
        {
            "id": "ee068152-ff10-4e27-851f-30e920416829",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "3535d879-afd2-43be-b077-00ac25d503d4"
        },
        {
            "id": "b22c72ff-763e-43f2-ae58-65664284f860",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3535d879-afd2-43be-b077-00ac25d503d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}