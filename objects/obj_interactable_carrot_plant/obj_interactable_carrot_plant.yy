{
    "id": "8de306c6-de63-4198-bf00-5b45a0b011f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_carrot_plant",
    "eventList": [
        {
            "id": "a237ff19-7f9d-4a55-93b6-e44ad2cbef79",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8de306c6-de63-4198-bf00-5b45a0b011f2"
        },
        {
            "id": "097e9ab9-8fe3-4ffe-b314-8c1e8e86922d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8de306c6-de63-4198-bf00-5b45a0b011f2"
        },
        {
            "id": "c3f55997-a0d7-41d1-94ea-ebffc000dd36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "8de306c6-de63-4198-bf00-5b45a0b011f2"
        },
        {
            "id": "d499430f-d860-4ce5-9aec-74526d9d1eaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "8de306c6-de63-4198-bf00-5b45a0b011f2"
        },
        {
            "id": "8faf1205-8a4b-4d82-af33-84e7b7dc4b56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "8de306c6-de63-4198-bf00-5b45a0b011f2"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "08713f60-aa11-45dd-860e-879098a6af27",
    "visible": true
}