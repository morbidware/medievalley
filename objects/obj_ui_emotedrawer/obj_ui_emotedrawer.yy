{
    "id": "a9337023-8611-4efd-a37e-bd4936e9996b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ui_emotedrawer",
    "eventList": [
        {
            "id": "1b432bfc-6a0b-453d-bb7d-24a8fca012c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a9337023-8611-4efd-a37e-bd4936e9996b"
        },
        {
            "id": "48579389-0bfa-4f99-ad5e-b5008007b50f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a9337023-8611-4efd-a37e-bd4936e9996b"
        },
        {
            "id": "5cb2c31c-ba62-4312-a5fe-33081f273f64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a9337023-8611-4efd-a37e-bd4936e9996b"
        },
        {
            "id": "bce9cf92-b013-474d-9f8a-15b8ff4a65a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "a9337023-8611-4efd-a37e-bd4936e9996b"
        },
        {
            "id": "f0580cc3-d59e-4443-b1a4-7976eb9ae408",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "a9337023-8611-4efd-a37e-bd4936e9996b"
        },
        {
            "id": "7aa66949-90b5-47fc-a351-cc905e9183e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a9337023-8611-4efd-a37e-bd4936e9996b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}