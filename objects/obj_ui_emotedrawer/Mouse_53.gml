if (player.emoteCrono > 0)
{
	return;
}

if (mouse_get_relative_x() < x - (w/2) + 10 || mouse_get_relative_x() > x + (w/2) - 10)
{
	exit;
}
if (mouse_get_relative_y() < y - (h/2) + 10 || mouse_get_relative_y() > y + (h/2) - 10)
{
	exit;
}


v = mouse_get_relative_x() - x + ((emote_width * size) / 2) + (emote_width  /2);
v = round(v / emote_width) - 1;
v = clamp(v,0,size-1);

scr_sendEmote(global.username,v);
state = 1;
player.emoteCrono = 120;