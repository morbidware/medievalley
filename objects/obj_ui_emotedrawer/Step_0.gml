// Event is NOT inherited since emote drawer does not animate like other windows
with (obj_char)
{
	highlightedobject = noone;
}

if (state == 0)
{
	alpha += 0.1;
}
else if (state == 1)
{
	alpha -= 0.2;
	if (alpha < 0)
	{
		instance_destroy(id);
	}
}
alpha = clamp(alpha,0,1);