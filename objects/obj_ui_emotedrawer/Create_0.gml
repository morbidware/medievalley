event_inherited();

alpha = 0;
emotes = ds_list_create();
size = 0;
emote_width = (sprite_get_width(spr_emote_greet) * 2) + 2;
w = 0;
h = 54;
v = 0;
player = instance_find(obj_char,0);

btnClose = instance_create_depth(0,0,depth - 1, obj_btn_generic);
btnClose.sprite_index = spr_btn_close_orange;
if(global.ismobile){
	btnClose.image_xscale = btnClose.image_xscale * 2;
	btnClose.image_yscale = btnClose.image_yscale * 2;
}
btnClose.caller = id;
btnClose.evt = ev_user0;

ds_list_add(emotes,spr_emote_greet);
ds_list_add(emotes,spr_emote_thumbsup);
ds_list_add(emotes,spr_emote_thumbsdown);
ds_list_add(emotes,spr_emote_heart);
ds_list_add(emotes,spr_emote_wood);
ds_list_add(emotes,spr_emote_rock);
ds_list_add(emotes,spr_emote_water);