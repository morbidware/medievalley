size = ds_list_size(emotes);
w = (size * emote_width) + (emote_width / 2);
x =  display_get_gui_width()/2;
y = display_get_gui_height()/2 + 48;
draw_set_alpha(alpha);
draw_set_color(c_white);
scr_nine_slice_at(spr_9slice_summary_bg, x, y, w, h, 2);

draw_set_font(global.FontStandardBoldOutlineX2);
draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_text(x - (w / 2) + 12, y - 26, "Emotes");

var i = 0;
var tx = 0;
repeat (size)
{
	draw_set_alpha(1);
	tx = x - ((emote_width * size) / 2) + (emote_width * i) + (emote_width  /2);
	draw_sprite_ext(ds_list_find_value(emotes,i), 0, tx, y, 2, 2, 0, c_white, alpha);
	draw_set_color(c_black);
	draw_set_alpha(0.4);
	if (i < size-1)
	{
		draw_line_width(tx + 20, y-18, tx+20, y+18, 2);
	}
	i++;
}

draw_set_color(c_white);
draw_set_alpha(1);

btnClose.ox = (w/2) -3;
btnClose.oy = -(h/2) +3;