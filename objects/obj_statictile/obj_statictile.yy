{
    "id": "e0007196-f2fc-4c64-8264-47a51f0752dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_statictile",
    "eventList": [
        {
            "id": "1167cb68-57d9-4adf-b854-80f8b0ad27a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0007196-f2fc-4c64-8264-47a51f0752dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c4ace89-5095-40b4-a9f5-de7c661e52d4",
    "visible": true
}