{
    "id": "2e317193-38df-4d46-b95f-f087155f8ce9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_abandoned_castle_b",
    "eventList": [
        {
            "id": "c02d8cc8-7607-4070-b497-b18ac5cace58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2e317193-38df-4d46-b95f-f087155f8ce9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7bcdf71-c91e-4337-8af0-3e1b20a59493",
    "visible": true
}