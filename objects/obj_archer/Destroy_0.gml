/// @description Insert description here
// You can write your code in this editor
with(obj_gameLevelChallenge)
{
	if(challengeState == 1)
	{
		total ++;
		//tempGold += 40;
		global.finishedInteractions ++;
		logga("[[ global.finishedInteractions ++ ]]");
		with(obj_char)
		{
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.finishedInteractionsValue;
		}
	}
}