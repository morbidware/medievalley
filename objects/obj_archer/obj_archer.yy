{
    "id": "44ad8f76-3e49-4871-85ad-f99da673dd88",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_archer",
    "eventList": [
        {
            "id": "7832d07e-2b58-4194-bb7a-319cac84a11e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "44ad8f76-3e49-4871-85ad-f99da673dd88"
        },
        {
            "id": "79a74420-4010-4bc1-a366-94557160e5ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "44ad8f76-3e49-4871-85ad-f99da673dd88"
        },
        {
            "id": "1a7abfb7-fe37-4f7d-9c3e-d8dbda53e2d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "44ad8f76-3e49-4871-85ad-f99da673dd88"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4637c718-e68d-4e37-b64d-0cc396872957",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5d3721ad-3511-4849-bdfb-c9d0200cd90c",
    "visible": true
}