/// @description Custom mob behavior
maxlife = 20;
life = 20;					// Health
spd = 0.8;					// Movement speed
fleeradius = 96;
chargeTimer = 60;
sightradius = 200;			// Radius for detecting the player
chaseradius = 80;			// Radius for start chasing the player or fleeing if ranged
attackradius = 200;			// Radius of attack
attackpause = 2;			// Seconds between each consecutive attack
damage = 10;				// Damage for each single attack
alerttime = 2;				// How many seconds the mob will wait before going from alert to an action state
chasetime = 3;				// How many seconds the mob will chase the player
pacific = false;			// If true, the mob will attack only when attacked
cautious = false;			// If true, the mob will stay at least 'chaseradius' far from the player
ranged = true;				// If true, the mob behavior better fits a ranged attack style
tool = obj_mobtool_bow;		// Tool used during attack
lazy = 0.4;					// Probability of the mob to not move from its place when idling
maxflees = 2;				// Number of times a cautious mob will flee before dropping the cautious status

attacktimer = 40;
uiheight = 36;

if(global.isChallenge)
{
	maxlife = ds_map_find_value(global.challengeData,"enemy_life");
	life = maxlife;
}