{
    "id": "333a829f-e9cd-43ce-b0fb-055182cb0e9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chest_panel",
    "eventList": [
        {
            "id": "1e8f6723-2797-4b4e-b5a9-b49f5504ab80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "333a829f-e9cd-43ce-b0fb-055182cb0e9a"
        },
        {
            "id": "f52e4044-7087-4f09-829e-37163465f68d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "333a829f-e9cd-43ce-b0fb-055182cb0e9a"
        },
        {
            "id": "239bd31e-7fe1-45e8-802e-02ce4229b8f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "333a829f-e9cd-43ce-b0fb-055182cb0e9a"
        },
        {
            "id": "3a886fc9-2655-4d7a-9185-66fdd291ab11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "333a829f-e9cd-43ce-b0fb-055182cb0e9a"
        },
        {
            "id": "3d8239c1-b0ef-4907-bca6-3ec83710ab91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "333a829f-e9cd-43ce-b0fb-055182cb0e9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84bf0c78-ea5a-457f-a613-a8bedcf58cdd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}