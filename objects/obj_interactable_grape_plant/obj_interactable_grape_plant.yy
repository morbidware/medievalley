{
    "id": "62ffe3ae-505b-4409-9b9f-6e7bba615056",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_grape_plant",
    "eventList": [
        {
            "id": "fe55ddd5-ab14-447b-a721-9c8852d1f774",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "62ffe3ae-505b-4409-9b9f-6e7bba615056"
        },
        {
            "id": "c11a3273-d083-40c5-830f-3683bf810a7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "62ffe3ae-505b-4409-9b9f-6e7bba615056"
        },
        {
            "id": "5c177fd2-78a4-40c1-b7a0-259aa0f57e47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "62ffe3ae-505b-4409-9b9f-6e7bba615056"
        },
        {
            "id": "ee102886-8f98-402f-a8b1-e356c12d7e9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "62ffe3ae-505b-4409-9b9f-6e7bba615056"
        },
        {
            "id": "47aad6c3-1327-4611-9473-7f93dbc2d7e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "62ffe3ae-505b-4409-9b9f-6e7bba615056"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
    "visible": true
}