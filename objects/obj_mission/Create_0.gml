///@description Mission init

depth = -700;
missionid = "mission0000";
rewards = ds_map_create();
required = ds_map_create();
done = ds_map_create();
active = false;
silent = false;

title = "";
shortdesc = "";
longdesc = "";
hintTexts = ds_list_create();
hintImages = ds_list_create();
completionTexts = ds_list_create();
completionImages = ds_list_create();

ds_list_clear(completionTexts);
ds_list_add(completionTexts, "Quest complete!");
ds_list_clear(completionImages);
ds_list_add(completionImages, spr_mission_check);

found = false;

titlealpha = 0;
titletimer = 180;

tasks = 0;
completedtasks = 0;
completion = 0;
checkTimer = 0;
completeOnHold = false;

scr_setDepth(DepthType.Overlay);
alarm_set(0,2);

//show_debug_message(">>> Mission created: "+missionid+" ("+object_get_name(object_index)+")");