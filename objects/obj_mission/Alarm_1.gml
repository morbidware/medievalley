/// @description End mission
with (obj_summary_manager)
{
	// Delete mission id from list of active missions
	var index = ds_list_find_index(activeMissions, other.missionid);
	if (index > -1)
	{
		ds_list_delete(activeMissions,index);
	}
	
	// Refresh mission summaries and save
	alarm_set(0,2);
}
instance_destroy(id);