{
    "id": "8299d0fd-0188-4419-ba64-b8c0f3b131ce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mission",
    "eventList": [
        {
            "id": "72b3455c-8ea4-4b0c-8f04-57db4d1f6269",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "a16a50f1-70e3-4bd4-8310-0034797b5f83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "73fac6ff-c038-49c6-a689-d0eb9283e243",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "afd27c7e-49a0-496b-8819-d84431e7d7fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "ec1ef071-0bae-4d4e-9de2-92d883f8b1ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "d33b19de-a33d-451a-a338-881073896575",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "69267a9e-a61d-42ee-b287-b9ddf7099f69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "4758baea-238b-4d81-b812-459099b13f78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        },
        {
            "id": "34c98e1d-22dc-482e-b774-94fb18d3c771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "8299d0fd-0188-4419-ba64-b8c0f3b131ce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}