/// @description Title fade

// Title fadein and fadeout (only when there are no hints)
if (!active && instance_number(obj_hint) == 0)
{
	// Skip title and hints if the mission is silent
	if (!silent)
	{
		if (titlealpha < 1 && titletimer > 0)
		{
			titlealpha += global.dt * 0.01;
		}
		else if (titlealpha >= 1 && titletimer > 0)
		{
			titlealpha = 1;
			titletimer -= global.dt;
		}
		else if (titlealpha >= 0 && titletimer <= 0)
		{
			titletimer = 0;
			titlealpha -= global.dt * 0.01;
		}
		else if (titlealpha <= 0 && titletimer <= 0)
		{
			active = true;
			with (obj_summary_manager)
			{
				event_perform(ev_other,ev_user0);
			}
			scr_show_hint(title,hintTexts,hintImages,false);	
		}
	}
	else
	{
		active = true;
	}
}