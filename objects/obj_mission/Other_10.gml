/// @description Completion reward
var p = instance_find(obj_char,0);
var r = ds_map_find_first(rewards);
for (var i = 0; i < ds_map_size(rewards); i++)
{
	repeat (round(ds_map_find_value(rewards,r)))
	{
		instance_create_depth(p.x, p.y+8, p.depth-1, r);
	}
	r = ds_map_find_next(rewards,r);
}

if (ds_exists(completionTexts,ds_type_list) && ds_exists(completionImages,ds_type_list))
{
	scr_show_hint(title,completionTexts,completionImages,true);
}
alarm_set(1,2);

gasend(string(global.userigid), missionid + ":" + title, 1);