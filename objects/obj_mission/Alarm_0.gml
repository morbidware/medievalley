/// @description Save mission, update summary
scr_storeMissions();
scr_storeUnlockables();
with (obj_summary_manager)
{
	event_perform(ev_other,ev_user0);
}