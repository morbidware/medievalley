/// @description Check completion
//show_debug_message("----- MISSION CHECK "+string(missionid));

// If on hold, mark as complete and stop here
if (completeOnHold)
{
	completion = 1;
	exit;
}

// Create temporary 'tempDone' map made by the actual 'done' map plus the required items inside the quick inventory
var tempDone = ds_map_create();
var inv = instance_find(obj_playerInventory,0);
for (var i = 0; i < ds_grid_width(inv.inventory); i++)
{
	var obj = ds_grid_get(inv.inventory,i,0);
	if (obj >= 0)
	{
		var r = ds_map_find_first(required);
		repeat (ds_map_size(required))
		{
			if (obj.itemid == r){
				if (ds_map_exists(tempDone,obj.itemid))
				{
					var val = ds_map_find_value(tempDone,obj.itemid);
					ds_map_replace(tempDone,obj.itemid,val+obj.quantity);
				}
				else
				{
					ds_map_add(tempDone,obj.itemid,obj.quantity);
				}
				var maxvalue = ds_map_find_value(required,obj.itemid);
				if (ds_map_find_value(tempDone,obj.itemid) > maxvalue)
				{
					ds_map_replace(tempDone,obj.itemid,maxvalue)
				}
			}
			r = ds_map_find_next(required,r);
		}
	}
}

// Update completion amount
completedtasks = 0;
if (ds_map_size(tempDone) > 0)
{
	var f = ds_map_find_first(tempDone);
	repeat(ds_map_size(tempDone))
	{
		completedtasks += ds_map_find_value(tempDone,f);
		//show_debug_message("----- ADDING COMPLETED TASK... "+string(f)+", "+string(ds_map_find_value(tempDone,f)));
		f = ds_map_find_next(tempDone,f);
	}
}
tasks = 0;
f = ds_map_find_first(required);
repeat(ds_map_size(required))
{
	tasks += ds_map_find_value(required,f);
	//show_debug_message("----- ADDING REQUIRED... "+string(f)+", "+string(ds_map_find_value(required,f)));
	f = ds_map_find_next(required,f);
}
completion = completedtasks / tasks;
done = tempDone;

// Refresh missions summary
with (obj_summary_manager)
{
	event_perform(ev_other,ev_user0);
}
	
// check if 'tempDone' map contains all stuff from the required map
var r = ds_map_find_first(required);
repeat(ds_map_size(required))
{
	// always ignore special
	if (r == "special")
	{
		continue;
	}
		
	found = false;
	var d = ds_map_find_first(tempDone);
	repeat(ds_map_size(tempDone))
	{
		if (d == r)
		{
			if (ds_map_find_value(tempDone,d) >= ds_map_find_value(required,r))
			{
				found = true;
				break;
			}
		}
		d = ds_map_find_next(tempDone,d);
	}
			
	// early quit if something hasn't been found
	if (!found)
	{
		exit;
	}
			
	r = ds_map_find_next(required,r);
}

// Arrived at this point, mission is completed
// Perform reward event now, or put mission on hold if the user is not in the farm
if (room != roomFarm)
{
	var texts = ds_list_create();
	ds_list_add(texts, "Well done! Now go back to your farm to complete this Quest.");
	var images = ds_list_create();
	ds_list_add(images, spr_interactable_roadsign);
	scr_show_hint(title, texts, images, true);
	completion = 1;
	checkTimer = -1;
	completeOnHold = true;
}
else
{
	event_perform(ev_other,ev_user0);
	checkTimer = -1;
}