if (titlealpha <= 0 || active)
{
	exit;
}

draw_set_halign(fa_center);
draw_set_valign(fa_middle);

var w = display_get_gui_width();
var h = display_get_gui_height() + 96;
var offset = 0;
if (titletimer > 0)
{
	offset = - (1-titlealpha) * 32;
}
else
{
	offset = (1-titlealpha) * 32;
}

draw_set_color(c_black);
draw_set_alpha(titlealpha * 0.5);
draw_rectangle(0, h/2-31, w, h/2+31, false);
draw_set_color(c_white);
draw_set_alpha(titlealpha);
draw_rectangle(0, h/2-32, w, h/2-30, false);
draw_set_font(global.FontStandardOutline);
draw_text(w/2 + offset, h/2-10, "New quest");
draw_set_font(global.FontStandardBoldOutlineX2);
draw_text(w/2 + offset, h/2+6, title);
draw_rectangle(0, h/2+30, w, h/2+32, false);

draw_set_color(c_white);
draw_set_alpha(1);