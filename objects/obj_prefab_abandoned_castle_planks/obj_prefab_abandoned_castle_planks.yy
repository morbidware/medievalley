{
    "id": "d10e6dcc-e3fb-452d-9f4d-36658f36aa04",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_prefab_abandoned_castle_planks",
    "eventList": [
        {
            "id": "b2a532d3-a56f-4768-b94d-778ef30bfeb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d10e6dcc-e3fb-452d-9f4d-36658f36aa04"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87bc2133-86cd-4501-a599-973f2f43f6c3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d9bf76e-a91a-4ecc-8382-21750937d8bc",
    "visible": true
}