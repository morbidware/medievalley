{
    "id": "74584c5e-c16b-4d30-9c26-cf8c56ddffbe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fx_particle_rock",
    "eventList": [
        {
            "id": "cb17fe3b-7b5c-49f6-b0d1-fdc0e3d0a478",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74584c5e-c16b-4d30-9c26-cf8c56ddffbe"
        },
        {
            "id": "045946b0-9d08-4ff4-803e-fc4f415a6726",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "74584c5e-c16b-4d30-9c26-cf8c56ddffbe"
        },
        {
            "id": "3c5f7ca3-d564-4747-8937-8ac05b484a4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "74584c5e-c16b-4d30-9c26-cf8c56ddffbe"
        },
        {
            "id": "69b280d6-1b08-4804-8d4e-b992d7883e0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "74584c5e-c16b-4d30-9c26-cf8c56ddffbe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6f74f90d-8167-4060-8b17-12e3dc9fbfee",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "66b16eaf-4b3f-46bb-852e-150c9a3c2c11",
    "visible": true
}