{
    "id": "265f21ae-32ce-4105-8d46-35c93ebd3184",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_water",
    "eventList": [
        {
            "id": "ec7b3483-9b38-4457-9959-91b52e1ff1a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "265f21ae-32ce-4105-8d46-35c93ebd3184"
        },
        {
            "id": "f696ac0a-ed20-40a1-b8d8-e0d4d7acd7e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "265f21ae-32ce-4105-8d46-35c93ebd3184"
        },
        {
            "id": "b5f7c315-e4ec-431b-ad9e-be30c9fede1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "265f21ae-32ce-4105-8d46-35c93ebd3184"
        },
        {
            "id": "9bedd2ef-cc74-4eac-9d81-2cf09e4d1753",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "265f21ae-32ce-4105-8d46-35c93ebd3184"
        },
        {
            "id": "c8a29c96-20d9-44cd-b38b-cd7b36f6e931",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "265f21ae-32ce-4105-8d46-35c93ebd3184"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e0007196-f2fc-4c64-8264-47a51f0752dd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
    "visible": true
}