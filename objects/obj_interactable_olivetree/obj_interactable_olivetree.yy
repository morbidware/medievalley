{
    "id": "0478e7b5-7a65-45db-b432-03092cfbc33e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_interactable_olivetree",
    "eventList": [
        {
            "id": "68d691c1-946b-4ac6-9989-3554ea88be38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        },
        {
            "id": "e8ef96b4-07fc-4686-bd37-710251b95fc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        },
        {
            "id": "c69e3a6a-309f-4213-b880-4054f9f87048",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        },
        {
            "id": "0307bf55-bc49-4d4a-b2fa-15602036f441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        },
        {
            "id": "636b8f70-8e11-41b1-9b18-cb92f2238824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        },
        {
            "id": "c53eff7a-92c6-44a7-9c2a-46ac37f7bb69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 25,
            "eventtype": 7,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        },
        {
            "id": "e30a7b84-0597-4542-9a20-72d65ed5686d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 24,
            "eventtype": 7,
            "m_owner": "0478e7b5-7a65-45db-b432-03092cfbc33e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "20f7b641-ee4e-49f1-a1fe-300b721c2f3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
    "visible": true
}