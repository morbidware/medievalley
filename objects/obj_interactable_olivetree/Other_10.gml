/// @description finish interaction

// shovel the stump
if(growstage == 1 &&
	global.game.player.handState == HandState.Shovel &&
	global.game.player.highlightCol = floor(x/16) &&
	global.game.player.highlightRow = floor(y/16))
{
	scr_dropItems(id,"woodlog");
	instance_destroy();
	return;
}

if (global.lastMouseButton == "right")
{	
	if(!isNet)
	{
		switch (growstage)
		{
			case 2: scr_dropItems(id,"woodlog"); break;
			case 3: scr_dropItems(id,"woodlog","woodlog"); break;
			case 4: scr_dropItems(id,"woodlog","woodlog","woodlog"); break;
			case 5: scr_dropItems(id,"woodlog","woodlog","woodlog"); break;
			case 6: scr_dropItems(id,"woodlog","woodlog","woodlog","food_olive","food_olive","food_olive"); break;
		}
	}
	
	//GENERATE GOLD
	if(!isNet)
	{
		scr_getGoldOnline(ActorState.Chop);
	}
	
	isNet = false;
	
	var fallingtree = instance_create_depth(x,y,0,obj_falling_tree);
	fallingtree.treesprite = sprite_index;
	fallingtree.treeimageindex = image_index;
	if(growstage == growstages)
	{
		growstage = 1;
		growcrono = growtimer;
	}
	else
	{
		with (obj_baseGround)
		{
			ds_grid_set(itemsgrid,floor(other.x/16),floor(other.y/16),-1);
		}
		instance_destroy();
	}
}
else if (global.lastMouseButton == "left")
{
	if (!isNet)
	{
		scr_dropItems(id,"food_olive","food_olive","food_olive");
	}
	growstage = growstage-2;
	growcrono = growtimer;
}