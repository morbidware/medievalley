{
    "id": "8fbcf0e2-dc5c-45a3-a049-ef85a62d08d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fishingfloat",
    "eventList": [
        {
            "id": "57f1cc7a-c3a3-41d3-a04f-7faa1165ef2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8fbcf0e2-dc5c-45a3-a049-ef85a62d08d6"
        },
        {
            "id": "23900be8-3510-42cf-b7dd-f5c17f9e2040",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8fbcf0e2-dc5c-45a3-a049-ef85a62d08d6"
        }
    ],
    "maskSpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5b5d839-7c3b-42bc-8ef8-1a4463d855fa",
    "visible": true
}