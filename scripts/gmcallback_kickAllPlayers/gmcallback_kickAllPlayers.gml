// Return to own farm
//logga("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::CLIENT KICKED CALLBACK");
if (global.otheruserid != 0)
{	
	global.otherusername = "";
	global.otheruserid = 0;
	global.othersocketid = "";
	global.othergridstring = "";
	global.otherpickables = "";
	global.otherinteractables = "";
	joinRoom(global.socketid);
	global.lastjoinedroom = global.socketid;
	getClientsInRoom(global.socketid);
	global.popupTitle = "SORRY";
	global.popupBody = "The player you visited has closed its Farm.";
	with(instance_create_depth(0,0,0,obj_fadeout))
	{
		gotoroom = noone;
	}
	if (global.visitorscount > 0)
	{
		updateFarm();
	}
	else
	{
		getPlayerData(global.username,global.userigid);
	}
}