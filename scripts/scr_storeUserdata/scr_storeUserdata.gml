if(global.isSurvival)
{
	scr_storeUserDataSurvival();
	exit;
}
logga("scr_storeUserData()");
if (!scr_allowedToSave()) { return; }
global.uisavestatus.saving = true;
if (global.online)
{
	var player = instance_find(obj_char,0);
	// some things require further elaboration before saving
	var mailmap = ds_map_create();
	for (var i = 0; i < ds_list_size(global.mailIndices); i++)
	{
		ds_map_add(mailmap, string(ds_list_find_value(global.mailIndices, i)), string(ds_list_find_value(global.mailRead, i)));
	}
	
	var wardrobe = ds_list_write(global.currentwardrobe);
	wardrobe = string_replace_all(wardrobe,"000000","_");
		
	var charLife = player.life;
	var charStamina = player.stamina;
	
	logga("global.userid:"+string(global.userid));
	logga("charLife:"+string(charLife));
	logga("charStamina:"+string(charStamina));
	logga("gold:"+string(player.gold));
	logga("galasilver:"+string(player.galasilver));
	logga("global.musicvolume:"+string(global.musicvolume));
	logga("global.sfxvolume:"+string(global.sfxvolume));
	logga("global.totslots:"+string(global.totslots));
	
	if(global.isChallenge)
	{
		charLife = 100;//this is to save normal values even if changed in the db 
		charStamina = 100;
	}
	logga("global.userid:"+string(global.userid));
	logga("charLife:"+string(charLife));
	logga("charStamina:"+string(charStamina));
	logga("gold:"+string(player.gold));
	logga("galasilver:"+string(player.galasilver));
	logga("global.musicvolume:"+string(global.musicvolume));
	logga("global.sfxvolume:"+string(global.sfxvolume));
	logga("global.totslots:"+string(global.totslots));
	sendUserdataString(global.userid, string(charLife)+":"+string(charStamina)+":"+string(player.gold)+":"+string(player.galasilver)+":"+string(global.musicvolume)+":"+string(global.sfxvolume)+":"+ds_map_write(mailmap)+":"+ds_map_write(global.craftedobjects)+":"+wardrobe+":"+string(global.saveversion)+":"+string(global.allowvisitors)+":"+string(global.lastChallengeDone)+":"+string(global.graphicsquality)+":"+string(global.showhints)+":"+string(global.totslots), global.username);
	scr_storeStats();
	gmcallback_updateLastSave();
}
global.uisavestatus.saving = false;

logga("scr_storeUserData() end");