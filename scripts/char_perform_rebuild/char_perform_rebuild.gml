//technically no animation here

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	scr_setState(id,ActorState.Idle);
	if (targetobject != noone)
	{
		if (global.online)
		{
			eventPerformInteractable(targetobject.x,targetobject.y,targetobject,targetobject.object_index,ev_other,ev_user2);
		}
		with(targetobject)
		{
			event_perform(ev_other,ev_user2);
		}
	}
}