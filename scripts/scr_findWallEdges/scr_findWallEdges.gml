with(obj_wall)
{
	if(place_meeting(x,y-1,obj_wall))
	{
		hasTop = false;
	}
	if(place_meeting(x,y+1,obj_wall))
	{
		hasBottom = false;
	}
	if(place_meeting(x-1,y,obj_wall))
	{
		hasLeft = false;
	}
	if(place_meeting(x+1,y,obj_wall))
	{
		hasRight = false;
	}
}

scr_findWallSprites();