///@function gmcallback_gotUsercreationDate(string)
///@param date
// format is YYYY-MM-DD
var time = argument0;
var year = real(string_copy(time,1,4));
var month = real(string_copy(time,6,2));
var day = real(string_copy(time,9,2));
var creationdate = date_create_datetime(year,month,day,0,0,0);

var secondsFromEpoch = real(getTimestamp()) / 1000.0;
var epoch = date_create_datetime(1900,1,0,0,0,0);
var currentdate = date_inc_second(epoch,secondsFromEpoch);

logga("----- Creation date: "+string(date_get_year(creationdate)+"-"+date_get_month(creationdate)+"-"+string(date_get_day(creationdate))));
logga("----- seconds from Epoch: "+string(secondsFromEpoch));
logga("----- Current date: "+string(date_get_year(currentdate)+"-"+date_get_month(currentdate)+"-"+string(date_get_day(currentdate))));

global.stat_days = floor(date_day_span(creationdate,currentdate)-1); // fixed with -1 for some fucking reason...
if (global.stat_days < 0)
{
	global.stat_days = 0;
}