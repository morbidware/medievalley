///@function scr_addMail(mailIndex, isRead, callPidgeon, delay)
///@param mailIndex
///@param isRead
///@param callPidgeon
///@param delay

// if this mail exists already, don't add it
if (ds_list_find_index(global.mailIndices, argument0) >= 0)
{
	exit;
}

var title;
var body;
var signature;

// select letter to add by id
switch (argument0)
{
	case 0:
		title = "The Farm is now yours";
		body = "I, King Thistle, on behalf of my powers, grant you all rights on this piece of land, declaring it of your sole property. May God bless this soil with its generosity.";
		signature = "King Thistle";
		break;
		
	case 1:
		title = "About seeds";
		body = "You just received some Red Bean seeds. All seeds can be planted on plowed ground, and will need water and some time to grow. Note that seeds won't grow when you are offline!";
		signature = "Your friend Bryce";
		break;
		
	case 2:
		title = "The Wild Lands";
		body = "Perils and rare items await outside your Farm, to the East. Get equipped to fight wild animals and stay alert at all times: if you faint, you will lose all your stuff there... If you need to go home from there, either backtrack or search for road signs!aw";
		signature = "Your friend Bryce";
		break;
		
	case 3:
		title = "The Town is safe again";
		body = "To all citizens! After the disgraceful events of past days, we are glad to announce that it's now safe to enter the Town. All activities will gradually return to normal.";
		signature = "King Thistle";
		break;
	
	case 4:
		title = "Working in other Farms";
		body = "You can now visit other Farms! There you will get paid for cutting down trees, harvesting, watering plants and mining rocks: it's the quickest way to get some Gold. Check the new Farmboard to begin!";
		signature = "Your friend Bryce";
		break;
}

ds_list_add(global.mailIndices,argument0);
ds_list_add(global.mailTitles,string(title));
ds_list_add(global.mailBodies,string(body));
ds_list_add(global.mailSignatures,string(signature));
ds_list_add(global.mailRead,argument1);

// If requested, create pidgeon and show now that mail has arrived
if (argument2 == true)
{
	// If we are not in the farm, put pidegon in "buffer" instead
	if (room != roomFarm)
	{
		global.spawnpidgeon = true;
	}
	else
	{
		var p = instance_create_depth(-100,-100,0,obj_wildlife_mail_pidgeon);
		p.delay = argument3;
	}
}