///@function scr_anim_shovel(object,direction,crono,maxcrono,steps,sidesprite,frontsprite)
///@param object the sword object
///@param direction direction of the player
///@param crono current crono timer for animation
///@param maxcrono total crono of this animation
///@param steps frames for the animation
///@param side side graphics of the tool
///@param front side graphics of the tool

var t = 1-((argument2-1)/argument3);
var a;
var t2 = (ceil(t * argument4) / argument4) * 80;
var t3 = (ceil(t * argument4) / argument4);

switch(argument1)
{
	case Direction.Right:
	a = 260 + t2;
	x = argument0.x + 3 + cos(degtorad(a)) * 4;
	y = argument0.y - 6 - sin(degtorad(a)) * 13;
	image_angle = 220 + (t2*1.4);
	image_yscale = 1;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Left:
	a = 100 - t2;
	x = argument0.x - 3 - cos(degtorad(a)) * 4;
	y = argument0.y - 6 + sin(degtorad(a)) * 13;
	image_angle = 140 - (t2*1.4);
	image_yscale = 1;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Up:
	image_angle = 0;
	x = argument0.x;
	y = argument0.y + 10 - (t3 * 15);
	image_yscale = 0.2 + (t3*0.8);
	if (argument6 != noone)
		sprite_index = argument6;
	break;
	
	case Direction.Down:
	image_angle = 180;
	x = argument0.x;
	y = argument0.y + 18 - (t3 * 15);
	image_yscale = 1.3 - (t3 * 1.5);
	if (argument6 != noone)
		sprite_index = argument6;
	break;
}
y -= 16;
if(global.online)
{
	if(argument0.visible)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,image_xscale,image_yscale);
	}
	if(t >= 1.0)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,0.0,0.0);
	}
}