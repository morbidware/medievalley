// tool animation
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.shovelTimer;
	if (other.stamina <= 0)
		t *= 2;
	scr_anim_shovel(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	
	// execute actions if conditions are met
	if (accessible)
	{
		if (targetobject > 0 && targetobject != undefined)
		{
			if (event_perform_ret(targetobject, ev_other, ev_user1) == true && targetobject.interactionPrefixRight == "Remove")
			{
				with(targetobject)
				{
					if (global.online)
					{
						eventPerformInteractable(x,y,id,object_index,ev_other,ev_user2);
					}
					event_perform(ev_other,ev_user2);
				}
			}
		}
		else
		{
			switch (groundtype)
			{
				case GroundType.TallGrass:
					if (global.online)
					{
						changeGroundTile(interactionCol, interactionRow, GroundType.Grass);
					}
					ds_grid_set(fg.grid, interactionCol, interactionRow, GroundType.Grass);
					if(global.isSurvival)
					{
						var c =interactionCol-1;
						var r =interactionRow-1;
						for(i = 0;i<3;i++)
						{
							for(j = 0;j<3;j++)
							{
						
								scr_updateTileSpritesSurvival(c+i,r+j);
							}
						}
						
					}
					else
					{
						scr_updateTileSprites();
					}
					scr_loseDurability(id,true);
					break;
				case GroundType.Grass:
					if (global.online)
					{
						changeGroundTile(interactionCol, interactionRow, GroundType.Soil);
					}
					ds_grid_set(fg.grid, interactionCol, interactionRow, GroundType.Soil);
					if(global.isSurvival)
					{
						var c =interactionCol-1;
						var r =interactionRow-1;
						for(i = 0;i<3;i++)
						{
							for(j = 0;j<3;j++)
							{
						
								scr_updateTileSpritesSurvival(c+i,r+j);
							}
						}
					}
					else
					{
						scr_updateTileSprites();
					}
					scr_loseDurability(id,true);
					break;
			}
			global.savegroundtimer = 0;
		}
	}
	if(global.isSurvival && global.online)
	{
		scr_storeGroundSurvival()
	}
	else
	{
		scr_storeAllInsocket();
	}
}