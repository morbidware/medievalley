///@function scr_directionFromPointAngle(x1,y1,x2,y2)
///@descriptions Returns a Direction enum value based on angle between point and target
///@param x1 origin x
///@param y1 origin y
///@param x2 target x
///@param y2 target y

var angle = point_direction(argument0,argument1,argument2,argument3);
if (angle < 22 || angle > 338)
{
	return Direction.Right;
}
else if (angle >= 22 && angle < 68)
{
	return Direction.UpRight;
}
else if (angle >= 68 && angle < 112)
{
	return Direction.Up;
}
else if (angle >= 112 && angle < 158)
{
	return Direction.UpLeft;
}
else if (angle >= 158 && angle < 202)
{
	return Direction.Left;
}
else if (angle >= 202 && angle < 248)
{
	return Direction.DownLeft;
}
else if (angle >= 248 && angle < 292)
{
	return Direction.Down;
}
else if (angle >= 292 && angle < 338)
{
	return Direction.DownRight;
}