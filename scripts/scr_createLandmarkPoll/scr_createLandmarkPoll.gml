///@function scr_createLandmarkPoll(wildtype)
///@description Returns a list with available landmark maps for the specified wildland type
///@param wildtype

var list = ds_list_create();
switch (argument0)
{	
	case WildType.Rural:
		ds_list_add(list,spr_rural_graveyard);
		ds_list_add(list,spr_rural_abandoned_castle);
		ds_list_add(list,spr_rural_witch_lair);
		break;
	case WildType.Mountain:
		break;
	case WildType.Coast:
		break;
}

return list;