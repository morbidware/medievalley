///@function scr_dropItemQuantity(fromobject,itemid,amount)
///@description this function creates "dropped" objects and positions them in a wat they don't overlap
///@param fromobject
///@param itemid (string)
///@param amount
var increment = 360 / argument2;
for (var i = 0; i < argument2; i++) 
{	
	var itemid = argument1;
	var angle = increment * i;
	var dx = argument0.x + (sin(degtorad(angle)) * 8);
	var dy = argument0.y + (cos(degtorad(angle)) * 5);
	if (global.online)
	{
		dropPickable(dx,dy,itemid,1);
	}
	scr_gameItemCreate(dx, dy, obj_pickable, itemid);
}

if(global.isSurvival && global.online)
{
	scr_storePickablesSurvival();
}
{
	scr_storeAllInsocket();
}