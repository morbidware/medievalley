///@description gmcallback_dropPickable
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = ds_list_find_value(res,0);
var yy = ds_list_find_value(res,1);
var pid = ds_list_find_value(res,2);
var quant = ds_list_find_value(res,3);

logga("xx:" + string(xx) + " is number" + string(is_real(xx)));
logga("yy:" + string(yy) + " is number" + string(is_real(yy)));
logga("pid:" + string(pid) + " is string" + string(is_string(string(pid))));
logga("quant:" + string(quant) + " is number" + string(is_real(quant)));

ds_list_destroy(res);
scr_gameItemCreate(real(xx), real(yy),obj_pickable,string(pid),real(quant));
if(global.isSurvival)scr_storePickablesSurvival();