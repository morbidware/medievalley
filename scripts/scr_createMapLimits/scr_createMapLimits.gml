///@function scr_createMapLimits(sprite,bordersize,exitleft,exitright,exittop,exitbottom)
///@param sprite
///@param bordersize
///@param exitleft
///@param exitright
///@param exittop
///@param exitbottom
///@sprite amount of border
if(global.isSurvival)
{
	exit;
}
var w = sprite_get_width(argument0);
var h = sprite_get_height(argument0);
var s = argument1;

var exitleft = argument2;
var exitright = argument3;
var exittop = argument4;
var exitbottom = argument5;

var gw = ds_grid_width(limitgrid);
var gh = ds_grid_height(limitgrid);

//place limit items on the grid
for (var c = 0; c < gw; c++)
	{
		for (var r = 0; r < gh; r++)
	{
		if (c < s || c >= gw-s || r < s || r >= gh-s)
		{
			
			//don't place limits on specified exits
			if (exitleft)
			{
				var mid = floor(gh/2);
				if (r >= mid && r <= mid && c <= s) continue;
			}
			if (exitright)
			{
				var mid = floor(gh/2);
				if (r >= mid && r <= mid && c >= gw-s) continue;
			}
			if (exittop)
			{
				var mid = floor(gw/2);
				if (c >= mid && c <= mid && r <= s) continue;
			}
			if (exitbottom)
			{
				var mid = floor(gw/2);
				if (c >= mid && c <= mid && r >= gh-s) continue;
			}
			
			//create limit object
			var t = instance_create_depth(c*w,r*h,0,obj_maplimit);
			ds_grid_set(limitgrid,c,r,t);
			t.image_index = 13;
		}		
	}
}