///@function scr_updateMobState
///@description update Mob
///@param mob_id

//starting from usr evt 3 because
//usr evt 0 is for customization
//usr evt 1 is for dropping items
//usr evt 2 is for target reached
with(argument0)
{
	switch(state)
	{
		case MobState.Roam:
		event_perform(ev_other,ev_user2);
		break;
		case MobState.Idle:
		event_perform(ev_other,ev_user3);
		break;
		case MobState.Chase:
		event_perform(ev_other,ev_user4);
		break;
		case MobState.Flee:
		event_perform(ev_other,ev_user5);
		break;
		case MobState.Charge:
		event_perform(ev_other,ev_user6);
		break;
		case MobState.Attack:
		event_perform(ev_other,ev_user7);
		break;
		case MobState.Suffer:
		event_perform(ev_other,ev_user8);
		break;
		case MobState.Die:
		event_perform(ev_other,ev_user9);
		break;
		case MobState.RunAway:
		event_perform(ev_other,ev_user10);
		break;
	}
}