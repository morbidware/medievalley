///@function scr_fix_text(text)
///@param text
// This function adds an empty space after '9' and 'z', since these two chars have 0 width on browser somehow...
var text = string(argument0);
if (os_browser != browser_not_a_browser)
{
	var str = "";
	var i = 1;
	var ch = "";
	repeat(string_length(text))
	{
		ch = string(string_char_at(text,i));
		str += ch;
		if (ch == "9" || ch == "z" || ch == "Z")
		{
			str += " ";
		}
		i++;
	}
	return str;
}
return text;