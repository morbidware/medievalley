///@description gmcallback_leaveFarm
logga("leave farm");
with(obj_dummy)
{
	if(socketid == argument0)
	{
		logga("destroy leaving dummy " + string(argument0));
		instance_destroy();
	}
	
	// Save and update clients if in our farm
	if (global.othersocketid == "")
	{
		getClientsInRoom(global.socketid);
		scr_storeData();
	}
}