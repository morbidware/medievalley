//ds_list_clear(global.pickables);

if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
with(obj_debugger)
{
	str += "scr_storeHousePickables\n";
}
var pickableslist = ds_list_create();
for(var i = 0; i < instance_number(obj_pickable); i++)
{
	var p = instance_find(obj_pickable,i);
	//logga("found a pickable " + string(i) + " " + p.itemid);
	if(p.destroy == false)
	{
		var storestr = string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.quantity)+":"+string(p.durability);
		//logga(storestr);
		ds_list_add(pickableslist,storestr);
	}
	else
	{
		var storestr = "destroy == true -> "+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.quantity)+":"+string(p.durability);
		//logga(storestr);
	}
}
var uncompressed_string = ds_list_write(pickableslist);
var compressed_string = uncompressed_string;
compressed_string = string_replace_all(compressed_string,"000000","_");
compressed_string = string_replace_all(compressed_string,"A666C696E743A313A3001","%");

if (global.online)
{
	if(global.housepickables != uncompressed_string)
	{	
		logga("WILL UPDATE HOUSE PICKABLES ON SERVER");
		global.housepickables = ds_list_write(pickableslist);
		sendHousePickablesString(global.userid, compressed_string, global.username);
	}
	gmcallback_updateLastSave();
}
else
{
	
	global.housepickables = ds_list_write(pickableslist);
	
	gmcallback_updateLastSave();
}
ds_list_destroy(pickableslist);
global.uisavestatus.saving = false;