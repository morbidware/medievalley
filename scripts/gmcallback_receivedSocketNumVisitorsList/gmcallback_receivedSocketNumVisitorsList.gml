///@description gmcallback_receivedSocketNumVisitorsList
logga("GMCB:RICEVUTO SOCKET NUMVISITORS LIST");
var str = argument0; 
var del = ":";
var pos = string_pos(del, str);
var dellen = string_length(del);

ds_list_clear(global.socketsnumvisitors);

while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	ds_list_add(global.socketsnumvisitors, real(stri));
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
   
}
ds_list_add(global.socketsnumvisitors, str);

var myIndex = ds_list_find_index(global.socketsusernames,global.username);
ds_list_delete(global.sockets,myIndex);
ds_list_delete(global.socketsusernames,myIndex);
ds_list_delete(global.socketsnumtrees,myIndex);
ds_list_delete(global.socketsnumrocks,myIndex);
ds_list_delete(global.socketsnumplants,myIndex);
ds_list_delete(global.socketsnumvisitors,myIndex);

with (obj_farmboard_panel)
{
	// remove from entries all farms with -1 visitors (-1 means the farm is closed to public)
	for (var i = 0; i < ds_list_size(global.sockets); i++)
	{
		if (ds_list_find_value(global.socketsnumvisitors,i) == "-1")
		{
			ds_list_delete(global.sockets,i);
			ds_list_delete(global.socketsusernames,i);
			ds_list_delete(global.socketsnumtrees,i);
			ds_list_delete(global.socketsnumrocks,i);
			ds_list_delete(global.socketsnumplants,i);
			ds_list_delete(global.socketsnumvisitors,i);
			i--;
		}
	}
	
	error = "";
	refreshing = false;
}