/*
if(!instance_exists(obj_sickle_area))
{
	var area = instance_create_depth(x, y, 0, obj_sickle_area);
	if(lastdirection == Direction.Up)
	{
		area.image_angle = 90;
	}
	else if(lastdirection == Direction.Down)
	{
		area.image_angle = -90;
	}
	else if(lastdirection == Direction.Left)
	{
		area.image_angle = 180;
	}
}
*/

//animate equipped object
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.sickleTimer;
	if (other.stamina <= 0)
	{
		t *= 2;
	}
	scr_anim_melee(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}

crono -= global.dt;

if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	
	// execute action if conditions are met
	if (targetobject != noone)
	{
		if (event_perform_ret(targetobject, ev_other, ev_user1) == true && targetobject.interactionPrefixRight == "Mow")
		{
			if (global.online)
			{
				eventPerformInteractable(targetobject.x,targetobject.y,targetobject,targetobject.object_index,ev_other,ev_user2);
			}

			with (targetobject)
			{
				event_perform(ev_other,ev_user2);
			}
			if(global.othersocketid == "")
			{
				scr_loseDurability(id,true);
			}
		}
	}
	global.saveitemstimer = 0;
	if(!global.isSurvival)scr_storeAllInsocket();
}