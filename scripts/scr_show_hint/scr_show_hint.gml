///@function scr_show_hint(title,texts,images,override)
///@param title
///@param texts
///@param images	
///@param override

// if override is requested and there is at least one hint active, override it
if (argument3 == true && instance_exists(obj_hint))
{
	with (obj_hint)
	{
		if (enabled = true)
		{
			idletimer = hintduration;
			fadetimer = 0;
			pageindex = 0;
			title = argument0;
			ds_list_clear(texts);
			ds_list_clear(images);
			for (var i = 0; i < ds_list_size(argument1); i++)
			{
				ds_list_add(texts,ds_list_find_value(argument1,i));
				ds_list_add(images,ds_list_find_value(argument2,i));
			}
			exit;
		}
	}
}
// else just create a new hint
else
{
	// create new
	var hint = instance_create_depth(0,0,0,obj_hint);
	hint.title = argument0;
	for (var i = 0; i < ds_list_size(argument1); i++)
	{
		ds_list_add(hint.texts,ds_list_find_value(argument1,i));
		ds_list_add(hint.images,ds_list_find_value(argument2,i));
	}
	ds_queue_enqueue(global.hintqueue,hint.id);
}