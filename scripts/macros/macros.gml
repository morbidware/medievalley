// -------------------------------- MAIN SETTINGS --------------------------------

// Enable/disable additional items in the farm and in the inventory.
// (def: false)
#macro m_extraObjects false

// Enable/disable game story and missions. If disabled, everything is unlocked from the beginning.
// (def: true)
#macro m_enableProgress true

// Enable/disable unlockables.
// (def: false)
#macro m_unlockAll false

// Unlocks all UI from the start
// (def: false)
#macro m_unlockUI false

// -------------------------------- OTHER SETTINGS --------------------------------

// Enable/disable total magnet.
// (def: false)
#macro m_megaMagnet false

// Start with or without money.
// (def: false)
#macro m_rich false

// Enable/disable power functions for tools: harder, better, faster, stronger. Player is invincible & tireless.
// (def: false)
#macro m_godMode false

// If true, adds test missions at the beginning of the game.
// (def: false)
#macro m_testMissions false

// Enable/disable mobs and hostile fauna.
// (def: true)
#macro m_enableMobs true

// Enable/disable interactables.
// (def: true)
#macro m_enableInteractables true

// Enable/disable pickables.
// (def: true)
#macro m_enablePickables true

// If true, music starts at volume 0 when testing off-browser (volume can still be changed in Settings).
// (def: true)
#macro m_muteMusicOnDebug true

// Enable/disable all online functionality. Disable when testing locally.
// (def: false)
#macro m_forceOffline false

// Enable/disable compact performance profiler.
// (def: false)
#macro m_showProfiler false

// Enable/disable developer console when pressing TAB and special hotkeys.
// (def: false)
#macro m_enableConsole false

// Enable/disable live coding plugin (using 1/0 instead of true/false due to how the plugin uses the macro).
// (def: 0)
#macro m_liveCoding 0

// Duration of a game day in real minutes.
// (def: 12)
#macro m_dayLength 12

// If true, day time will be halted and fixed at midday (this does not affect plants growing).
// (def: false)
#macro m_forceDay false

// Enable/disable debug challenge.
// (def: false)
#macro m_debugChallenge false

// Force a challenge type
// (def: -1)
#macro m_debugChallengeType -1

// Fast building
// (def: false)
#macro m_fastBuilding false