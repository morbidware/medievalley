//no animation needed here
	
utilityCrono -= global.dt;
if(utilityCrono <= 0)
{
	utilityCrono = 60;
	instance_create_depth(targetobject.x,targetobject.y,targetobject.depth - 2,obj_fx_sleeping_z);
	stamina = clamp(stamina + targetobject.staminagain, 0, 100);
	life = clamp(life + targetobject.healthgain, 0, 100);
	global.saveuserdatatimer = 0;
}

if(crono <= 0)
{
	scr_setState(id,ActorState.Idle);
	visible = true;
	if (targetobject != noone)
	{
		if (event_perform_ret(targetobject, ev_other, ev_user1) == true)
		{
			with (targetobject)
			{
				event_perform(ev_other,ev_user2);
			}
		}
	}
}