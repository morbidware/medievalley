///@function scr_draw_sprite_enclosed(spriteid,imageindex,posx,posy,size,alpha)
///@description This function draws a sprite perfectly centered and resized to stay encolsed in a square
///@param spriteid
///@param imageindex
///@param posx
///@param posy
///@param size
///@param alpha

var w = sprite_get_width(argument0);
var h = sprite_get_height(argument0);
var px = sprite_get_xoffset(argument0) - (w / 2);
var py = sprite_get_yoffset(argument0) - (h / 2);
	
var maxside = w;
if (h > w) maxside = h;
var scale = argument4/maxside;
px *= scale;
py *= scale;
	
draw_sprite_ext(argument0, argument1, argument2 + px, argument3 + py, scale, scale, 0, c_white, argument5);