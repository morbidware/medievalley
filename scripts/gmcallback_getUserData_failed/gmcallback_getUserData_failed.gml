///@description gmcallback_getUserData_failed
if (global.timeout <= 0)
{
	exit;
}
with(obj_farmboard_panel)
{
	fetching = false;
	error = "COULD NOT CONNECT TO SELECTED FARM.";
}
/*
with(instance_create_depth(0,0,0,obj_popup))
{
	title = "WHOOPS";
	body = "There was a problem while connecting to this farm.";
	type = 1;
	btnText = "CLOSE";
	caller = id;
}
*/