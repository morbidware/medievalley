///@function scr_createStairsWall(c,r,isout)
///@description Create wall object for Stairs at specified column and row. The wall is returned.
///@param c
///@param r
///@param isout

var c = argument0;
var r = argument1;
var isout = argument2;

var ground = instance_find(obj_baseGround,0);
var w = ds_grid_get(ground.wallsgrid,c,r);
if (w < 1)
{
	// Wall doesn't exist
	w = instance_create_depth(c*16+8,r*16+8,0,obj_wall_invisible);
	if (isout == 1)
	{
		w.storey_stairs_out = true;
	}
	else
	{
		w.storey_stairs_in = true;
	}
}
else
{
	// Wall already exists
	if (isout == 1)
	{
		w.storey_stairs_out = true;
	}
	else
	{
		w.storey_stairs_in = true;
	}
}
w.blocking = false;
ds_grid_set(ground.wallsgrid,c,r,w);
return w;