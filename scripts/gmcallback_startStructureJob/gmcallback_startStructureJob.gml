///@description gmcallback_startStructureJob
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = real(ds_list_find_value(res,0));
var yy = real(ds_list_find_value(res,1));
var obj = real(ds_list_find_value(res,2));
var iid = string(ds_list_find_value(res,3));

ds_list_destroy(res);

logga("xx " + string(xx));
logga("yy " + string(yy));
logga("obj " + string(obj) + " " + object_get_name(string(obj)));
logga("iid " + string(iid));

var pla = instance_find(obj_char,0);

var near = instance_nearest(xx,yy,obj);
near.ts = 8/pla.craftTimer;
near.craftCrono = 300;
near.craftid = iid;