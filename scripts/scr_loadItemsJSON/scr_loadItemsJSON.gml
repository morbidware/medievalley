///@function scr_loadItemsJSON()
///@description used to load all items Models

//////////////////
/*
global.lang = "En";
var theJsonFile = file_text_open_read("lang.json");
var theData = "";
var lineCounter = 0;
while (!file_text_eof(theJsonFile))
{
    theData += file_text_read_string(theJsonFile);
	lineCounter++;
    file_text_readln(theJsonFile);
}
file_text_close(theJsonFile);

var theJsonMap = json_decode(theData);
var map = json_decode(ds_map_find_value(theJsonMap,"default"));
show_debug_message("JSON: map "+string(map));
show_debug_message("JSON: length "+string(ds_map_size(map)));
show_debug_message("JSON: first "+string(ds_map_find_first(map)));
show_debug_message("JSON: last "+string(ds_map_find_last(map)));
var langmap = ds_map_find_value(map, global.lang);
global.localized = langmap;
*/
//////////////////
//open file for reading
var file = file_text_open_read("items.json"); 
//prepare empty string
var json = "";
//read file line by line until the end of file
while (!file_text_eof(file))
{
	json += file_text_readln(file);
}
json = string_replace_all(json, chr(34), chr(92)+chr(34))
//close file
file_text_close(file);
//jsondecode file [MAP]
var resultMap = json_decode(json);
//get first node [MAP]
var map = json_decode(ds_map_find_value(resultMap, "default"));
//assign map to global.itemsModels
global.itemsModels = map;
//destroy both temp MAPS
//ds_map_destroy(resultMap);//DIEGO
//ds_map_destroy(map);
var k = ds_map_find_first(global.itemsModels);
//cycle through the map
show_debug_message("------------------");
show_debug_message("-------------ITEMS");
show_debug_message("------------------");
for(var i = 0; i < ds_map_size(global.itemsModels); i++)
{
	//get one item from the map [MAP]
	show_debug_message("k:" + string(k));
	var item = ds_map_find_value(global.itemsModels,k);
	k = ds_map_find_next(global.itemsModels,k);
	var kk = ds_map_find_first(item);
	for(var j = 0; j < ds_map_size(item); j++)
	{
		show_debug_message(string(kk)+":"+string(ds_map_find_value(item,kk)));
		kk = ds_map_find_next(item,kk);
	}
	show_debug_message("------------------");
}
show_debug_message("------------------");
//destroy temp MAP
//ds_map_destroy(item);//DIEGO