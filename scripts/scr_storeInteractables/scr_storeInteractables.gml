//ds_list_clear(global.interactables);
if(global.isSurvival)
{
	scr_storeInteractablesSurvival();
	exit;
}
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
var interactableslist = ds_list_create();
var numPlants = 0;
var numTrees = 0;
var numRocks = 0;
instance_activate_object(obj_interactable);
for(var i = 0; i < instance_number(obj_interactable); i++)
{
	var p = instance_find(obj_interactable,i);
	if(p.destroy == false)
	{
		var storestr = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.growcrono)+":"+string(p.growstage)+":"+string(p.quantity)+":"+string(p.durability)+":"+string(p.gridded)+":"+string(p.life)+":"+string(p.startingslot);
		ds_list_add(interactableslist,storestr);
		if(string_pos("plant",p.itemid)!=0)
		{
			numPlants ++;	
		}
		if(string_pos("pine",p.itemid)!=0 || string_pos("tree",p.itemid)!=0)
		{
			numTrees ++;	
		}
		if(string_pos("rock",p.itemid)!=0)
		{
			numRocks ++;	
		}
	}
}
var uncompressed_string = ds_list_write(interactableslist);
var compressed_string = uncompressed_string;
compressed_string = string_replace_all(compressed_string,"000000","_");
compressed_string = string_replace_all(compressed_string,"303A313A313A303A3","%");
compressed_string = string_replace_all(compressed_string,"70696E653A3","!");
compressed_string = string_replace_all(compressed_string,"77696C6467726173733A","&");
compressed_string = string_replace_all(compressed_string,"6F626A5F696E74657261637461626C655F",",");

if (global.online)
{
	// If visiting a farm and owner is not here, save it for him
	if(global.otheruserid != 0 && !instance_exists(obj_dummy))
	{
		if(global.otherinteractables != uncompressed_string)
		{
			logga("WILL UPDATE OTHER INTERACTABLES ON SERVER");
			global.otherinteractables = ds_list_write(interactableslist);
			sendInteractablesString(global.otheruserid, compressed_string, global.otherusername);
		}
		gmcallback_updateLastSave();
	}
	else
	{
		if(global.interactables != uncompressed_string)
		{
			logga("WILL UPDATE INTERACTABLES ON SERVER");
			global.interactables = ds_list_write(interactableslist);
			sendInteractablesString(global.userid, compressed_string, global.username);
		}
		sendNumTreesString(global.socketid, string(numTrees), global.username);
		sendNumRocksString(global.socketid, string(numRocks), global.username);
		sendNumPlantsString(global.socketid, string(numPlants), global.username);
		gmcallback_updateLastSave();
	}
}
else
{
	global.interactables = ds_list_write(interactableslist);
	//sendInteractablesString(global.userid, ds_list_write(interactableslist), global.username);
	gmcallback_updateLastSave();
}
ds_list_destroy(interactableslist);
/*
with(obj_interactable)
{
	var storestr = object_get_name(object_index)+":"+string(x)+":"+string(y)+":"+itemid+":"+string(growcrono)+":"+string(growstage)+":"+string(quantity)+":"+string(durability);
	show_debug_message("Storing interactable: "+storestr);
	logga("Storing interactable: "+storestr);
	ds_list_add(global.interactables,storestr);
}

sendInteractablesString(global.userid, ds_list_write(global.interactables));
*/
global.uisavestatus.saving = false;