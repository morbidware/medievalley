///@function gmcallback_gotChallengeDataError()
if(global.challengeType == 0)
{
	ds_map_add(global.challengeData,"good_number",27);
	ds_map_add(global.challengeData,"action_frames_normal",20);
	ds_map_add(global.challengeData,"action_frames_wrong",0);
	ds_map_add(global.challengeData,"action_frames_pain",0);
	ds_map_add(global.challengeData,"player_damage",0);
	ds_map_add(global.challengeData,"player_life",0);
	ds_map_add(global.challengeData,"enemy_damage",0);
	ds_map_add(global.challengeData,"enemy_life",0);
	ds_map_add(global.challengeData,"grow_crono",0);
}
else if(global.challengeType == 1)
{
	ds_map_add(global.challengeData,"good_number",99);
	ds_map_add(global.challengeData,"action_frames_normal",20);
	ds_map_add(global.challengeData,"action_frames_wrong",0);
	ds_map_add(global.challengeData,"action_frames_pain",0);
	ds_map_add(global.challengeData,"player_damage",0);
	ds_map_add(global.challengeData,"player_life",0);
	ds_map_add(global.challengeData,"enemy_damage",0);
	ds_map_add(global.challengeData,"enemy_life",0);
	ds_map_add(global.challengeData,"grow_crono",0);
}
else if(global.challengeType == 2)
{
	ds_map_add(global.challengeData,"good_number",27);
	ds_map_add(global.challengeData,"action_frames_normal",24);
	ds_map_add(global.challengeData,"action_frames_wrong",0);
	ds_map_add(global.challengeData,"action_frames_pain",0);
	ds_map_add(global.challengeData,"player_damage",0);
	ds_map_add(global.challengeData,"player_life",0);
	ds_map_add(global.challengeData,"enemy_damage",0);
	ds_map_add(global.challengeData,"enemy_life",0);
	ds_map_add(global.challengeData,"grow_crono",0);
}
else if(global.challengeType == 3)
{
	ds_map_add(global.challengeData,"good_number",0);
	ds_map_add(global.challengeData,"action_frames_normal",30);
	ds_map_add(global.challengeData,"action_frames_wrong",0);
	ds_map_add(global.challengeData,"action_frames_pain",0);
	ds_map_add(global.challengeData,"player_damage",0);
	ds_map_add(global.challengeData,"player_life",0);
	ds_map_add(global.challengeData,"enemy_damage",0);
	ds_map_add(global.challengeData,"enemy_life",0);
	ds_map_add(global.challengeData,"grow_crono",60);
}
else if(global.challengeType == 4)
{
	ds_map_add(global.challengeData,"good_number",0);
	ds_map_add(global.challengeData,"action_frames_normal",0);
	ds_map_add(global.challengeData,"action_frames_wrong",0);
	ds_map_add(global.challengeData,"action_frames_pain",0);
	ds_map_add(global.challengeData,"player_damage",8);
	ds_map_add(global.challengeData,"player_life",100);
	ds_map_add(global.challengeData,"enemy_damage",10);
	ds_map_add(global.challengeData,"enemy_life",20);
	ds_map_add(global.challengeData,"grow_crono",0);
}
else if(global.challengeType == 5)
{
	ds_map_add(global.challengeData,"good_number",27);
	ds_map_add(global.challengeData,"action_frames_normal",60);
	ds_map_add(global.challengeData,"action_frames_wrong",120);
	ds_map_add(global.challengeData,"action_frames_pain",40);
	ds_map_add(global.challengeData,"player_damage",0);
	ds_map_add(global.challengeData,"player_life",0);
	ds_map_add(global.challengeData,"enemy_damage",0);
	ds_map_add(global.challengeData,"enemy_life",0);
	ds_map_add(global.challengeData,"grow_crono",0);
}
else if(global.challengeType == 6)
{
	ds_map_add(global.challengeData,"good_number",0);
	ds_map_add(global.challengeData,"action_frames_normal",20);
	ds_map_add(global.challengeData,"action_frames_wrong",0);
	ds_map_add(global.challengeData,"action_frames_pain",0);
	ds_map_add(global.challengeData,"player_damage",0);
	ds_map_add(global.challengeData,"player_life",0);
	ds_map_add(global.challengeData,"enemy_damage",0);
	ds_map_add(global.challengeData,"enemy_life",0);
	ds_map_add(global.challengeData,"grow_crono",0);
}

var k = ds_map_find_first(global.challengeData);
repeat(ds_map_size(global.challengeData))
{
	var v = ds_map_find_value(global.challengeData,k);
	logga(k+" "+string(v));
	k = ds_map_find_next(global.challengeData,k);
}