///@description scr_splitStringToList(str)
var str = argument0;
var delimiter = argument1;
var res = ds_list_create();
var tempStr = "";

for(var i = 1; i <= string_length(str); i++)
{
	var char = string_char_at(str,i);
	if(char == delimiter)
	{
		ds_list_add(res, tempStr);
		show_debug_message(tempStr);
		tempStr = "";
	}
	else
	{
		tempStr += char;
	}
}
ds_list_add(res, tempStr);
show_debug_message(tempStr);
tempStr = "";

return res;