var yview = camera_get_view_y(view_camera[0]);
var offset = 0;
var mult = window_get_height() / camera_get_view_height(view_camera[0]);
if (global.online)
{
	offset = getYOffset();
}
if(argument_count == 1)
{
	return ((mouse_y-yview)*mult)-16 - (offset*mult);
}
return ((mouse_y-yview)*mult) - (offset*mult);