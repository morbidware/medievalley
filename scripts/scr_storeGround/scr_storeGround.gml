if(global.isSurvival)
{
	scr_storeGroundSurvival();
	exit;
}
logga("scr_storeGround()");
if (!scr_allowedToSave()) { return; }
global.uisavestatus.saving = true;

var fground = instance_find(obj_farmGround,0);
var uncompressed_string = ds_grid_write(fground.grid);
var compressed_string = scr_compressFarmString(fground.grid);

//compressed_string = string_replace_all(uncompressed_string,"00000000000000000000000","_");
//compressed_string = string_replace_all(compressed_string,"0000000000","%");

logga("STORE GROUND");
logga(compressed_string);
with(obj_baseGround)
{
	if (global.online)
	{
		// If visiting a farm and owner is not here, save it for him
		if(global.otheruserid != 0 && !instance_exists(obj_dummy))
		{
			if(global.othergridstring != uncompressed_string)
			{
				logga("WILL UPDATE OTHER GROUND ON SERVER");
				global.othergridstring = ds_grid_write(grid);
				sendFarmString(global.otheruserid, compressed_string, global.otherusername);
			}
		}
		else
		{
			if(global.gridstring != uncompressed_string)
			{
				logga("WILL UPDATE GROUND ON SERVER");
				global.gridstring = ds_grid_write(grid);
				sendFarmString(global.userid, compressed_string, global.username);
			}
			gmcallback_updateLastSave();
		}
	}
	else
	{
		global.gridstring = ds_grid_write(grid);
		gmcallback_updateLastSave();
	}
}

global.uisavestatus.saving = false;