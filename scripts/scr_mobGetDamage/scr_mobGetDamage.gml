///@function scr_mobGetDamage(id,weaponid)
var enemy = argument0;
var weapon = argument1;
var dmg = floor(random_range(weapon.mindamage,weapon.maxdamage));
if(global.isChallenge)
{
	var v = ds_map_find_value(global.challengeData,"player_damage");
	dmg = floor(random_range(v-1,v+1));
}

with(obj_enemy)
{
	targeted = false;
}
enemy.targeted = true;

if(instance_exists(obj_status_strength))
{
	dmg *= 1.25;
}
if (m_godMode)
{
	dmg = 9999999; // SEPHIROTH'S SUPERNOVA AYYYYY
}
var crit = false;
var pla = instance_find(obj_char,0);
if(random(100) < pla.critchance)
{
	crit = true;
	dmg *= 1.50;
	dmg = floor(dmg);
}
enemy.life -= dmg;
if(enemy.life > 0)
{
	if(enemy.state == MobState.Charge || enemy.state == MobState.Attack)
	{
		crono = sufferTimer;
		flashCrono = crono;
	}
	else
	{
		scr_setMobState(enemy, MobState.Suffer);
	}
}
else
{
	scr_setMobState(enemy, MobState.Die);
}
/*
var dmg_fx = instance_create_depth(enemy.x,enemy.y-(sprite_get_height(enemy.sprite_index)/2),enemy.depth-1,obj_fx_damage);
dmg_fx.col = c_white;
dmg_fx.str = "-"+string(dmg);
if(crit)
{
	dmg_fx.fnt = global.Fontx2;
	dmg_fx.col = c_red;
}
*/