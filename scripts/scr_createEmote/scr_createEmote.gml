///@function scr_createEmote(username,emoteType)
///@param username
///@param emoteType

var emote = instance_create_depth(0,0,0,obj_emote);
emote.owner = instance_find(obj_char,0);
var i = 0;
repeat(instance_number(obj_dummy))
{
	var dummy = instance_find(obj_dummy,i);
	if (dummy.name == argument0)
	{
		emote.owner = dummy;
		break;
	}
	i++;
}

switch (argument1)
{
	case 0: emote.emotesprite = spr_emote_greet; break;
	case 1: emote.emotesprite = spr_emote_thumbsup; break;
	case 2: emote.emotesprite = spr_emote_thumbsdown; break;
	case 3: emote.emotesprite = spr_emote_heart; break;
	case 4: emote.emotesprite = spr_emote_wood; break;
	case 5: emote.emotesprite = spr_emote_rock; break;
	case 6: emote.emotesprite = spr_emote_water; break;
}