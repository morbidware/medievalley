///@function scr_storeStats()
if(global.isSurvival)
{
	exit;
}
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;

if (global.online)
{
	var str = string(global.stat_timeplayed);
	str += ":"+string(global.stat_days);
	str += ":"+string(global.stat_walked);
	str += ":"+string(global.stat_croparea);
	str += ":"+string(global.stat_crops);
	str += ":"+string(global.stat_kills);
	str += ":"+string(global.stat_deaths);
	str += ":"+string(global.stat_goldearned);
	str += ":"+string(global.stat_goldspent);
	
	logga ("STORING USERSTATS");
	sendUserstatsString(global.userid, str, global.username);
	gmcallback_updateLastSave();
}

global.uisavestatus.saving = false;