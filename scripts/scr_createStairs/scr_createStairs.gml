///@function scr_createStairs()

// This script modifies the terrain to accomodate stairs for cliffs.
// It's important to do this after the entire terrain creation so further edits will not influence the terrain.

var ground = instance_find(obj_baseGround,0);
with (obj_stair)
{
	var c = floor(x/16);
	var r = floor(y/16);
	var storey = ds_grid_get(ground.storeygrid,c,r);
	ds_grid_set(ground.grid, c, r, GroundType.Grass);

	
	// STAIR UP (size 2x4)
	if (type == 0)
	{
		sprite_index = spr_stairs_up;
		switch (storey)
		{
			case 0: scr_setDepth(DepthType.Lv0Detail); break;
			case 1: scr_setDepth(DepthType.Lv1Detail); break;
			case 2: scr_setDepth(DepthType.Lv2Detail); break;
			case 3: scr_setDepth(DepthType.Lv3Detail); break;
		}
		
		//set groundtype under stair
		ds_grid_set(ground.grid, c+1, r, GroundType.Grass);
		// set storey for this stair
		ds_grid_set(ground.storeygrid,c,r,storey);
		ds_grid_set(ground.storeygrid,c+1,r,storey);
		ds_grid_set(ground.storeygrid,c,r+1,storey-1);
		ds_grid_set(ground.storeygrid,c+1,r+1,storey-1);
		ds_grid_set(ground.storeygrid,c,r+2,storey-1);
		ds_grid_set(ground.storeygrid,c+1,r+2,storey-1);
		ds_grid_set(ground.storeygrid,c,r+3,storey-1);
		ds_grid_set(ground.storeygrid,c+1,r+3,storey-1);
		
		// remove unwanted walls along stair
		ds_grid_set(ground.wallsgrid,c,r,0);
		ds_grid_set(ground.wallsgrid,c+1,r,0);
		ds_grid_set(ground.wallsgrid,c,r+1,0);
		ds_grid_set(ground.wallsgrid,c+1,r+1,0);
		ds_grid_set(ground.wallsgrid,c,r+2,0);
		ds_grid_set(ground.wallsgrid,c+1,r+2,0);
		ds_grid_set(ground.wallsgrid,c,r+3,0);
		ds_grid_set(ground.wallsgrid,c+1,r+3,0);
		
		// add internal stair walls (in)
		scr_createStairsWall(c,r,0);
		scr_createStairsWall(c+1,r,0);
		scr_createStairsWall(c,r+1,0);
		scr_createStairsWall(c+1,r+1,0);
		scr_createStairsWall(c,r+2,0);
		scr_createStairsWall(c+1,r+2,0);
		scr_createStairsWall(c,r+3,0);
		scr_createStairsWall(c+1,r+3,0);
		
		// add side stair walls (out)
		scr_createStairsWall(c-1,r,1);
		scr_createStairsWall(c+2,r,1);
		scr_createStairsWall(c-1,r+1,1);
		scr_createStairsWall(c+2,r+1,1);
		scr_createStairsWall(c-1,r+2,1);
		scr_createStairsWall(c+2,r+2,1);
		scr_createStairsWall(c-1,r+3,1);
		scr_createStairsWall(c+2,r+3,1);
		
		// set mp grid block
		mp_grid_add_cell(ground.pathgrid,c-1,r+1);
		mp_grid_add_cell(ground.pathgrid,c+2,r+1);
		mp_grid_add_cell(ground.pathgrid,c-1,r+2);
		mp_grid_add_cell(ground.pathgrid,c+2,r+2);
		mp_grid_add_cell(ground.pathgrid,c-1,r+3);
		mp_grid_add_cell(ground.pathgrid,c+2,r+3);
	}

	// STAIR DOWN (size 2x2 block)
	if (type == 1)
	{
		sprite_index = spr_stairs_down;
		switch (storey)
		{
			case 0: scr_setDepth(DepthType.Lv0Detail); break;
			case 1: scr_setDepth(DepthType.Lv1Detail); break;
			case 2: scr_setDepth(DepthType.Lv2Detail); break;
			case 3: scr_setDepth(DepthType.Lv3Detail); break;
		}
		
		// set storey for this stair
		ds_grid_set(ground.storeygrid,c,r+1,storey);
		ds_grid_set(ground.storeygrid,c+1,r+1,storey);
		
		ds_grid_set(ground.storeygrid,c,r,storey-1);
		ds_grid_set(ground.storeygrid,c+1,r,storey-1);
		
				
		// remove unwanted walls along stair
		ds_grid_set(ground.wallsgrid,c,r,0);
		ds_grid_set(ground.wallsgrid,c+1,r,0);
		ds_grid_set(ground.wallsgrid,c,r+1,0);
		ds_grid_set(ground.wallsgrid,c+1,r+1,0);
				
		// add internal stair walls (in)
		scr_createStairsWall(c,r,0);
		scr_createStairsWall(c+1,r,0);
		scr_createStairsWall(c,r+1,0);
		scr_createStairsWall(c+1,r+1,0);
				
		// add side stair walls (out)
		scr_createStairsWall(c-1,r,1);
		scr_createStairsWall(c+2,r,1);
		scr_createStairsWall(c-1,r+1,1);
		scr_createStairsWall(c+2,r+1,1);
		
		// set mp grid block
		mp_grid_add_cell(ground.pathgrid,c-1,r);
		mp_grid_add_cell(ground.pathgrid,c+2,r);
		mp_grid_add_cell(ground.pathgrid,c-1,r+1);
		mp_grid_add_cell(ground.pathgrid,c+2,r+1);
		
		// lower the storey for when the base tile is created
		
		storey--;
	}	
}

