///@description scr_handleMobData
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = ds_list_find_value(res,0);
var yy = ds_list_find_value(res,1);
var si = ds_list_find_value(res,2);
var ii = ds_list_find_value(res,3);
var sx = ds_list_find_value(res,4);
var mid = ds_list_find_value(res,5);
var sid = ds_list_find_value(res,6);
var lf = ds_list_find_value(res,7);
var stt = ds_list_find_value(res,8);
var trgtx = ds_list_find_value(res,9);
var trgty = ds_list_find_value(res,10);
ds_list_destroy(res);


var candidate = noone;
for(var i = 0; i < instance_number(obj_mob_dummy); i++)
{
    var inst = instance_find(obj_mob_dummy,i);
    if(inst.mid == mid)
    {
       candidate = inst;
       break; 
    }
}

if(candidate == noone)
{
	
	instance_activate_object(mid);
	candidate = instance_create_depth(xx,yy,0,obj_mob_dummy);
    candidate.tgt_x= real(xx);
    candidate.tgt_y = real(yy);
	candidate.mid = real(mid);
	candidate.ownersocket = string(sid);
	candidate.dummy_sprite_index = string(si);
	candidate.dummy_image_index = real(ii);
	candidate.dummy_image_xscale = real(sx);
	candidate.life = real(lf);
	candidate.state = real(stt);
	candidate.targetx = real(trgtx);
	candidate.targety = real(trgty);
	instance_deactivate_object(mid);

}
else
{
	candidate.tgt_x = real(xx);
    candidate.tgt_y = real(yy);
	candidate.dummy_sprite_index = string(si);
	candidate.dummy_image_index = real(ii);
	candidate.dummy_image_xscale = real(sx);
	candidate.life = real(lf);
	candidate.state = real(stt);
	candidate.targetx = real(trgtx);
	candidate.targety = real(trgty);
}