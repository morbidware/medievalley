///@function gmcallback_gotUserHistory
///@param str
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var new_user = string(ds_list_find_value(res,0));
var store_purchase = string(ds_list_find_value(res,1));
var bundle_purchase = string(ds_list_find_value(res,2));
var keys_won = real(ds_list_find_value(res,3));
var giveaway_created = string(ds_list_find_value(res,4));
var steamgroup_member = string(ds_list_find_value(res,5));

if(new_user == "true")
{
	global.challengeMalus_newUser = true;
}
else
{
	global.challengeMalus_newUser = false;
}

if(store_purchase == "true")
{
	global.challengeMalus_storePurchase = true;
}
else
{
	global.challengeMalus_storePurchase = false;
}

if(bundle_purchase == "true")
{
	global.challengeMalus_bundlePurchase = true;
}
else
{
	global.challengeMalus_bundlePurchase = false;
}

if(giveaway_created == "true")
{
	global.challengeMalus_giveawayCreated = true;
}
else
{
	global.challengeMalus_giveawayCreated = false;
}

if(steamgroup_member == "true")
{
	global.challengeMalus_steamGroupMember = true;
}
else
{
	global.challengeMalus_steamGroupMember = false;
}

global.challengeMalus_keysWon = keys_won;

blogga("gmcallback_gotUserHistory: NEW USER " + string(new_user));
blogga("gmcallback_gotUserHistory: STORE PURCHASE " + string(store_purchase));
blogga("gmcallback_gotUserHistory: BUNDLE PURCHASE " + string(bundle_purchase));
blogga("gmcallback_gotUserHistory: KEYS WON " + string(keys_won));
blogga("gmcallback_gotUserHistory: GIVEAWAY CREATED " + string(giveaway_created));
blogga("gmcallback_gotUserHistory: STEAM GROUP MEMBER " + string(steamgroup_member));

if(keys_won == 0)
{
	room_goto(roomChallenge);
}
else
{
	var letter = instance_create_depth(0,0,0,obj_challenge_malus_letter);
}