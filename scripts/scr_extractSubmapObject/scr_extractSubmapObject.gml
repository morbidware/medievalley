///@function scr_extractSubmapObject(dsmap,notnoone,notnoone)
///@param dsmap
///@param notnoone
///@description This function reads a ds_map with objects and spawn chance, then randomly extracts one object (or none as well). If force, it will avoid extracting "noone".
//show_debug_message("--------");
if(!global.isSurvival)
{
	var map = argument[0];
	var key = ds_map_find_first(map);
	// Force extract something
	if (argument[1] == true && ds_map_size(map) > 0)
	{
		repeat(random(ds_map_size(map)-1))
		{
			key = ds_map_find_next(map,key);
		}
		return key;
	}

	// Random
	var r = random(100.0);
	var v = 0;
	repeat(ds_map_size(map)){
	
		var value = ds_map_find_value(map,key);
		if (r >= v && r < v+value)
		{
			//show_debug_message("r was "+string(r)+", v was "+string(v)+", value was "+string(value)+", returned key was "+key);
			return key;
		}
		//show_debug_message("r was "+string(r)+", v was "+string(v)+", value was "+string(value)+", key was "+key+" but nothing was returned");
		v += value;
		key = ds_map_find_next(map,key);
	}
	return "noone";
}
else
{
		
	var map = argument[0];
	var forceMinCondition = argument[1];
	var forceMaxCondition = argument[2];
	var mapKey = ds_map_find_first(map);

	if(forceMaxCondition)
	{
		return "noone";
	}
	//Force
	if (forceMinCondition && ds_map_size(map) > 0)
	{
		var randomMapKey = random(ds_map_size(map)-1);
		repeat(randomMapKey)
		{
			mapKey = ds_map_find_next(map,mapKey);
		}
		return mapKey;
	}
	//Random	
	repeat(ds_map_size(map))
	{
		var chanceRandom = random(100);
		var chanceMapKey = ds_map_find_value(map,mapKey);
		if(chanceRandom <= chanceMapKey)
		{
			return mapKey;
		}
		mapKey = ds_map_find_next(map,mapKey);
	}

	return "noone";
	
}