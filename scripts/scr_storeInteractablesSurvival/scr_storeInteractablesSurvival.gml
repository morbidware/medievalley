//ds_list_clear(global.interactables);
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
var interactableslist = ds_list_create();
var numPlants = 0;
var numTrees = 0;
var numRocks = 0;
instance_activate_object(obj_interactable);
for(var i = 0; i < instance_number(obj_interactable); i++)
{
	var p = instance_find(obj_interactable,i);
	if(p.destroy == false)
	{
		var storestr = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.growcrono)+":"+string(p.growstage)+":"+string(p.quantity)+":"+string(p.durability)+":"+string(p.gridded)+":"+string(p.life)+":"+string(p.startingslot);
		ds_list_add(interactableslist,storestr);
	}
}
var uncompressed_string = ds_list_write(interactableslist);
var compressed_string = uncompressed_string;
compressed_string = string_replace_all(compressed_string,"000000","_");
compressed_string = string_replace_all(compressed_string,"303A313A313A303A3","%");
compressed_string = string_replace_all(compressed_string,"70696E653A3","!");
compressed_string = string_replace_all(compressed_string,"77696C6467726173733A","&");
compressed_string = string_replace_all(compressed_string,"6F626A5F696E74657261637461626C655F",",");

if (global.online)
{
	if(global.interactables != uncompressed_string)
	{
		logga("WILL UPDATE INTERACTABLES ON SERVER");
		global.interactables = ds_list_write(interactableslist);
		sendInteractablesStringSurvival(global.landid, compressed_string, global.username);
	}
	gmcallback_updateLastSave();
}
else
{
	global.interactables = ds_list_write(interactableslist);
	gmcallback_updateLastSave();
}
ds_list_destroy(interactableslist);

global.uisavestatus.saving = false;