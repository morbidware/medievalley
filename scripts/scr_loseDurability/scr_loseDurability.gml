///@function scr_loseDurability(id,willbedestroyed)
///@param id
///@param optional_willbedestroyed
if (m_godMode)
{
	exit;
}
if(room == roomChallenge)
{
	exit;
}
with(argument[0])
{
		var equipped = instance_nearest(x,y,obj_equipped);
		for(var i = 0; i < instance_number(obj_inventory); i++)
		{
			var inv = instance_find(obj_inventory,i);
			if(inv.slot == equipped.myslot)
			{
				inv.durability -= inv.lossperusage;
				if(inv.durability <= 0)
				{
					instance_destroy(inv);
					audio_play_sound(snd_breakTool,0,false);
					ds_grid_set(inventory.inventory,equipped.myslot,0,-1);
					if(argument_count == 2)
					{
						with(instance_nearest(other.x,other.y,obj_equipped))
						{
							instance_destroy();
						}
						with(obj_char)
						{
							handState = HandState.Empty;
						}
						with(obj_playerInventory)
						{
							event_perform(ev_other,ev_user0);
						}
					}
				}
				break;
			}
		}
}