
with(obj_mob_dummy)
{
	var obj = sprite_get_name(dummy_sprite_index);
	if(string_pos("bear",obj))
	{
		var mob = instance_create_depth(x,y,0,obj_bear);
	}
	if(string_pos("wolf",obj))
	{
		var mob = instance_create_depth(x,y,0,obj_wolf);
	}
	if(string_pos("wilddog",obj))
	{
		var mob = instance_create_depth(x,y,0,obj_wilddog);
	}
	mob.life = life;
	mob.targetx = targetx;
	mob.targety = targety;
	scr_setMobState(mob,state);
	
	//instance_create_depth(x,y,0,obj_wilddog);
	instance_destroy(id);
}

