///@function easeInBack(t, b, c, d)
/*
float Back::easeOut(float t,float b , float c, float d) {	

	float s = 1.70158f;

	return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;

}
*/
var t = argument0;
var b = argument1;
var c = argument2;
var d = argument3;
var s = 1.70158;

var t2 = t/d-1;

return c*((t2)*t2*((s+1)*t2 + s) + 1) + b;
