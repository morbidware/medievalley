///@description gmcallback_getPlayerData_failed
with(obj_farmboard_panel)
{
	fetching = false;
}
with(instance_create_depth(0,0,0,obj_popup))
{
	title = "WHOOPS";
	body = "There was a problem while connecting to your account.";
	type = 1;
	btnText = "CLOSE";
	caller = id;
}
with(obj_home)
{
	btnStart.disabled = false;
	btnContinue.disabled = false;
	btnChallenge.disabled = false;
}
with(obj_loading_panel)
{
	state = 1;
}