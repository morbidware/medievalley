///@description gmcallback_changeGroundTile
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var col = real(ds_list_find_value(res,0));
var row = real(ds_list_find_value(res,1));
var gt = real(ds_list_find_value(res,2));

ds_list_destroy(res);

var ground = instance_find(obj_baseGround,0);
var tile = ds_grid_get(ground.grid,col,row);
var type = GroundType.None;
switch (gt)
{
	case 0: type = GroundType.None; break;
	case 1: type = GroundType.Void; break;
	case 2: type = GroundType.TallGrass; break;
	case 3: type = GroundType.Grass; break;
	case 4: type = GroundType.Soil; break;
	case 5: type = GroundType.Plowed; break;
	case 6: type = GroundType.Water; break;
	case 7: type = GroundType.Cliff; break;
	case 8: type = GroundType.HouseFloor; break; 
	case 9: type = GroundType.HouseBasement; break;
}

ds_grid_set(ground.grid, col, row, type);

if(tile == GroundType.Soil)
{
	if(type == GroundType.Plowed)
	{
		var s = audio_play_sound(snd_hoe,0,false);
		audio_sound_pitch(s, 0.85 + random (0.35));
	}
}
if(global.isSurvival)
{
	var c =col-1;
	var r =row-1;
	var i,j;
	for(i = 0;i<3;i++)
	{
		for(j = 0;j<3;j++)
		{
			scr_updateTileSpritesSurvival(c+i,r+j);
		}
	}
	var _col = col;
	var _row = row;
	if(global.isSurvival && global.online)
	{
		with(obj_drawGroundSurvival)
		{
			self.col = _col;
			self.row = _row;

			event_perform(ev_other,ev_user0);
			
		}
	}
}
else
{
	scr_updateTileSprites();
}

