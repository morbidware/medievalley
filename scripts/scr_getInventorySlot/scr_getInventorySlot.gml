///@function scr_getInventorySlot()
///@description If mouse is over any kind of inventory, this will return the slot index; returns -1 if undetectable

var index = -1;
var mx = mouse_get_relative_x();
var my = mouse_get_relative_y();

if (!global.mouseOverInventory)
{
	return -1;	
}

var inventory = instance_find(obj_playerInventory,0);
if (inventory != noone)
{
	var fromx = inventory.startx - 14;
	var fromy = inventory.starty - 14;
	var tox = inventory.startx - 14 + 38 * 10;
	var toy = inventory.starty - 14 + 40;
	
	if(mx > fromx && mx < tox && my > fromy && my < toy)
	{
		index = floor((mx-(inventory.startx-14))/38);
	}
}

var backpack = instance_find(obj_backpack_panel,0);
if (backpack != noone)
{
	var fromx = backpack.x + backpack.startx - 16;
	var fromy = backpack.y + backpack.starty - 16;
	var tox = backpack.x + backpack.startx + 5*36 + 16;
	var toy = backpack.y + backpack.starty + 4*36 + 16;

	if(mx > fromx && mx < tox && my > fromy && my < toy)
	{
		var valx = floor((mx-(backpack.x+backpack.startx-16))/36);
		var valy = floor((my-(backpack.y+backpack.starty-16))/36);
		index = global.inv_inventorylimit+(valy*5)+valx;
		if(index >= global.inv_backpacklimit)
		{
			index = -1;
		}
	}
	if(mx > backpack.x - 98 - 16 && mx < backpack.x - 98 + 16)
	{
		if(my > backpack.y -60 -16 && my < backpack.y -60 + 16)
		{
			index = 30;	
		}
		else if(my > backpack.y -22 -16 && my < backpack.y -22 + 16)
		{
			index = 31;	
		}
		else if(my > backpack.y +16 -16 && my < backpack.y +16 + 16) 
		{
			index = 32;
		}
	}
	
}

var stash = instance_find(obj_stash_panel,0);
if (stash!= noone)
{
	var fromx = stash.x + stash.startx - 16;
	var fromy = stash.y + stash.starty - 16;
	var tox = stash.x + stash.startx + 10*36 + 16;
	var toy = stash.y + stash.starty + 5*36 + 16;
	
	if(mx > fromx && mx < tox && my > fromy && my < toy)
	{
		var valx = floor((mx-(stash.x+stash.startx-16))/38);
		var valy = floor((my-(stash.y+stash.starty-16))/38);
		index = global.inv_robelimit+(valy*10)+valx;
		if(index >= global.inv_stashlimit)
		{
			index = -1;
		}
	}
}

var chest = instance_find(obj_chest_panel,0);
if (chest!= noone)
{
	var fromx = chest.x + chest.startx - 16;
	var fromy = chest.y + chest.starty - 16;
	var tox = chest.x + chest.startx + 3*36 + 16;
	var toy = chest.y + chest.starty + 3*36 + 16;
	
	if(mx > fromx && mx < tox && my > fromy && my < toy)
	{
		var valx = floor((mx-(chest.x+chest.startx-16))/38);
		var valy = floor((my-(chest.y+chest.starty-16))/38);
		index = chest.startingslot+(valy*3)+valx;
		if(index >= chest.startingslot+9)
		{
			index = -1;
		}
	}
}

var safe = instance_find(obj_safe_panel,0);
if (safe != noone)
{
	var fromx = safe.x + safe.startx - 16;
	var fromy = safe.y + safe.starty - 16;
	var tox = safe.x + safe.startx + 10*36 + 16;
	var toy = safe.y + safe.starty + 5*36 + 16;
	
	if(mx > fromx && mx < tox && my > fromy && my < toy)
	{
		var valx = floor((mx-(safe.x+safe.startx-16))/38);
		var valy = floor((my-(safe.y+safe.starty-16))/38);
		index = global.inv_stashlimit+(valy*10)+valx;
		if(index >= global.inv_safelimit)
		{
			index = -1;
		}
	}
}
//show_debug_message("scr_findInventorySlot :: Selected slot index: "+string(index));
return index;