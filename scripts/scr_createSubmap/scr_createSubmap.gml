///@function scr_createSubmap(map,x,y)
///@description This script reads the submap and creates all the tiles and objects from it
///@param map
///@param x
///@param y
// setup the surface and drawing parameters for current submap

if(global.isSurvival)
{
	//logga("-----------------------------");
	var map = argument0;
	var hmap = scr_asset_get_index(sprite_get_name(map)+"_hmap");
	var mirrorx = choose(-1,1);
	var mirrory = choose(-1,1);
	var px = argument1;
	var py = argument2;
	var sprw = sprite_get_width(map);
	var sprh = sprite_get_height(map);

	var submapname = sprite_get_name(map);
	var submapobject = instance_create_depth(0,0,0,scr_asset_get_index(string_replace(submapname,"spr_","obj_")));
	if (submapobject.keeporientation == true || hmap != spr_missing)
	{
		mirrory = 1;	
	}
	if (!submapobject.canmirrorx)
	{
		mirrorx = 1;	
	}
	// draw submap (note that submaps with hmap can't be mirrored on Y axis)
	var srf = surface_create(sprw,sprh);
	surface_set_target(srf);
	draw_clear_alpha(c_white,0);
	var ii = clamp(irandom(sprite_get_number(map))-1,0,sprite_get_number(map)-1);
	draw_sprite_ext(map, ii, sprw/2, sprh/2, mirrorx, mirrory, 0, c_white, 1);
	surface_reset_target();

	var srfh = surface_create(sprw,sprh);
	surface_set_target(srfh);
	draw_clear_alpha(c_black,0);
	// draw hmap, if exists
	if (hmap != spr_missing)
	{
		draw_clear_alpha(c_black,0);
		draw_sprite_ext(hmap, ii, sprw/2, sprh/2, mirrorx, mirrory, 0, c_white, 1);
	}
	surface_reset_target();
	////////////////////////////////////////////////////////////////////////////////////
	//bigsurf
	//surface_set_target(surf);
	//draw_sprite_ext(map, 0, px + sprw/2, py + sprh/2, mirrorx, mirrory, 0, c_white, 1);
	//surface_reset_target();
	//bigsurfh
	//surface_set_target(heightsurf);
	//draw_sprite_ext(hmap, 0, px + sprw/2, py + sprh/2, mirrorx, mirrory, 0, c_white, 1);
	//surface_reset_target();
	///////////////////////////////////////////////////////////////////////////////////
	// Read elements that can be spawned by this submap
	var map_grass = submapobject.grass;
	var map_plants = submapobject.plants;
	var map_boulders = submapobject.boulders;
	var map_chests = submapobject.chests;
	var map_items = submapobject.items;
	var map_mobs = submapobject.mobs;
	var map_boss = submapobject.boss;
	var map_landmark = submapobject.landmark;
	var map_wall = submapobject.wall;

	// amounts spawned, for minimum spawn checl
	var spawned_mobs = 0;
	var spawned_boulders = 0;
	var spawned_items = 0;
	var minmobs = 3;
	var minboulders = 2;

	var c = 0;
	var r = 0;

	var g = ds_grid_create(sprw,sprh);
	var gg = ds_grid_create(sprw,sprh);

	var nam = sprite_get_name(map)+string(mirrorx)+string(mirrory)+string(ii);//name of the map
	var namh = sprite_get_name(map)+"hmap"+string(mirrorx)+string(mirrory)+string(ii);//name of the height map
	var v = ds_map_find_value(global.survivalsubmaps,nam);//is it already in the global map?
	var vh = ds_map_find_value(global.survivalsubmaps,namh);//is it already in the global map?
	if(is_undefined(v))
	{
		show_debug_message(nam+" does not exist -> adding");
		repeat(sprite_get_height(map))
		{
			c = 0;
			repeat(sprite_get_width(map))
			{
				var px1 = surface_getpixel(srf,c,r);
				var px2 = surface_getpixel(srfh,c,r)
				ds_grid_set(g, c, r, px1);
				ds_grid_set(gg, c, r, px2);
				//logga("setting " + string(c) + " " + string(r) + " = " + string(px1) + " and " + string(px2));
				c++;
			}
			r++;
		}
		ds_map_add(global.survivalsubmaps,nam,g);
		//ds_grid_destroy(g);
		ds_map_add(global.survivalsubmaps,namh,gg);
		//ds_grid_destroy(gg);
	
		v = ds_map_find_value(global.survivalsubmaps,nam);
		vh = ds_map_find_value(global.survivalsubmaps,namh);
	}
	else
	{
		show_debug_message(nam+" already exists -> skipping");
	}

	c = px;
	r = py;

	var cc = 0;
	var rr = 0;
	var colss = sprite_get_width(map);
	var rowss = sprite_get_height(map);
	repeat(sprite_get_height(map))
	{
		c = px;
		cc = 0;
		repeat(sprite_get_width(map))
		{
			// read surface color
		
			var col = ds_grid_get(v,cc,rr);
		
			var blue = color_get_blue(col);
			var green = color_get_green(col);
			var red = color_get_red(col);
			var col_topleft = ds_grid_get(vh, clamp(cc-1,0,colss-1), clamp(rr-1,0,rowss-1));
			var col_top = ds_grid_get(vh, clamp(cc,0,colss-1), clamp(rr-1,0,rowss-1));
			var col_topright = ds_grid_get(vh, clamp(cc+1,0,colss-1), clamp(rr-1,0,rowss-1));
			var col_left = ds_grid_get(vh, clamp(cc-1,0,colss-1), clamp(rr,0,rowss-1));
			var col_right = ds_grid_get(vh, clamp(cc+1,0,colss-1), clamp(rr,0,rowss-1));
			var col_bottomleft = ds_grid_get(vh, clamp(cc-1,0,colss-1), clamp(rr+1,0,rowss-1));
			var col_bottom = ds_grid_get(vh, clamp(cc,0,colss-1), clamp(rr+1,0,rowss-1));
			var col_bottomright = ds_grid_get(vh, clamp(cc+1,0,colss-1), clamp(rr+1,0,rowss-1));
		
		
		
			var col_prev = ds_grid_get(v, clamp(cc-1,0,cols-1), clamp(rr,0,rows-1));
			var color_top = ds_grid_get(v, clamp(cc,0,colss-1), clamp(rr-1,0,rowss-1));
		
			// read hmap color to get storey, skip if 0
			var storey = floor(color_get_red(ds_grid_get(vh, cc, rr)) / 32);
			//logga("pixel" + string(ds_grid_get(vh, cc, rr)));
			//logga("red" + string(color_get_red(ds_grid_get(vh, cc, rr))));
			//logga("storey " + string(storey));
			ds_grid_set(storeygrid, c, r, storey);
			tempstorey = storey;
		
			if (storey > 0) {
			
				// get storeys of surrouding tiles
				var s_topleft = floor(color_get_red(col_topleft) / 32);
				var s_top = floor(color_get_red(col_top) / 32);
				var s_topright = floor(color_get_red(col_topright) / 32);
				var s_left = floor(color_get_red(col_left) / 32);
				var s_right = floor(color_get_red(col_right) / 32);
				var s_bottomleft = floor(color_get_red(col_bottomleft) / 32);
				var s_bottom = floor(color_get_red(col_bottom) / 32);
				var s_bottomright = floor(color_get_red(col_bottomright) / 32);
			
				// generate cliff if this tile is higher than surrounding ones
				var docliff = false;
				if (storey > s_topleft) docliff = true;
				else if (storey > s_top) docliff = true;
				else if (storey > s_topright) docliff = true;
				else if (storey > s_left) docliff = true;
				else if (storey > s_right) docliff = true;
				else if (storey > s_bottomleft) docliff = true;
				else if (storey > s_bottom) docliff = true;
				else if (storey > s_bottomright) docliff = true;
			
				// set the read colors as cliff colors, unless the current color is for cliff stairs
				if (docliff)
				{
					if (!(blue == 0 && green == 64 && red == 64) && !(blue == 0 && green == 32 && red == 64))
					{
						blue = 0;
						green = 128;
						red = 128;
					}
				
					// walls on storey-1
					scr_createStoreyWall(c,r,storey-1,-1);
					scr_createStoreyWall(c,r+3,storey-1,-1);
				
					// outer walls on same storey
					if (storey > s_top) scr_createStoreyWall(c,r-1,storey,-1);
					if (storey > s_left) scr_createStoreyWall(c-1,r,storey,-1);
					if (storey > s_right) scr_createStoreyWall(c+1,r,storey,-1);
					if (storey > s_bottom) scr_createStoreyWall(c,r+1,storey,-1);
				
					// set unaccessible
					ds_grid_set(gridaccess,c,r,-1);
					ds_grid_set(gridaccess,c,r+1,-1);
					ds_grid_set(gridaccess,c,r+2,-1);
					ds_grid_set(gridaccess,c,r+3,-1);
				}
			}
		
			// BLUE: hero spawn point
			if(blue == 255 && green == 0 && red == 0)
			{
				ds_list_add(spawnpointsx,floor(8+c*16));
				ds_list_add(spawnpointsy,floor(8+r*16));
			}
		
			// RED: mob
			else if(blue == 0 && green == 0 && red == 255 && m_enableMobs)
			{
				// randomly choose a foe
				if (map_mobs != noone)
				{
					var mob = scr_extractSubmapObject(map_mobs, spawned_mobs < minmobs,false);
					if (mob != "noone" && spawned_mobs != 1)
					{
						instance_create_depth(8+c*16, 8+r*16, 0, scr_asset_get_index("obj_"+mob));
						spawned_mobs++;
					}
				}
			}
		
			// PURPLE: boss
			else if(blue == 255 && green == 0 && red == 255 && m_enableMobs)
			{
				if (map_boss != noone)
				{
					instance_create_depth(8+c*16, 8+r*16, 0, scr_asset_get_index("obj_"+map_boss));
				}
			}
		
			// BLACK: wall
			else if(blue == 0 && green == 0 && red == 0)
			{
				var w = instance_create_depth(8+c*16,8+r*16,0,map_wall);
				w.shadowType = ShadowType.Square;
				switch(storey)
				{
					case 0: w.storey_0 = true; break;
					case 1: w.storey_1 = true; break;
					case 2: w.storey_2 = true; break;
					case 3: w.storey_3 = true; break;
				}			
				ds_grid_set(wallsgrid, c, r, w);
			}
		
			// GREY: soil and boulders
			else if(blue == 128 && green == 128 && red == 128)
			{
				//logga("soil at "+string(c)+":"+string(r));
				ds_grid_set(grid, c, r, GroundType.Soil);
				if (map_boulders != noone && m_enableInteractables)
				{
					var boulder = scr_extractSubmapObject(map_boulders, spawned_boulders < minboulders,false);
					if (boulder != "noone")
					{
						scr_placeSubmapObject(boulder,scr_asset_get_index("obj_interactable_"+boulder),8+c*16, 8+r*16, 72);
						spawned_boulders++;
					}
				}
			
			}
		
			// AQUA BLUE: water
			else if(blue == 255 && green == 128 && red == 0)
			{
				//logga("water at "+string(c)+":"+string(r));
				ds_grid_set(grid, c, r, GroundType.Water);
				/*
				if (random(8) < 1)
				{
					instance_create_depth(c*16+8, r*16+8, 0, obj_fx_watersparkle);
				}
				*/
			
			}
		
			// LIGHT GREEN: wildgrass
			else if(blue == 0 && green == 255 && red == 0)
			{
				//logga("wildgrass at "+string(c)+":"+string(r));
				ds_grid_set(grid, c, r, GroundType.Grass);
				if (map_grass != noone && m_enableInteractables)
				{
					var grass = scr_extractSubmapObject(map_grass, false, false);
					if (grass != "noone")
					{
						scr_placeSubmapObject(grass,scr_asset_get_index("obj_interactable_"+grass),8+c*16, 8+r*16, 4);
					}
				}
			
			}
		
			// DARK GREEN: tallgrass and trees
			else if(blue == 0 && green == 128 && red == 0)
			{
				//logga("tallgrass at "+string(c)+":"+string(r));
				ds_grid_set(grid, c, r, GroundType.TallGrass);
				if (map_plants != noone && m_enableInteractables)
				{
					var plant = scr_extractSubmapObject(map_plants, false,false);
					if (plant != "noone")
					{
						scr_placeSubmapObject(plant,scr_asset_get_index("obj_interactable_"+plant),8+c*16, 8+r*16, 36);
					}
				}
			
			}
		
			// ORANGE: items
			else if(blue == 0 && green == 128 && red == 255 && m_enablePickables)
			{
				var t_red = color_get_red(color_top);
				var t_green = color_get_green(color_top);
				var t_blue = color_get_blue(color_top);
				//logga("t_red:"+string(t_red)+" t_green:"+string(t_green)+" t_blue:"+string(t_blue));
				if(t_red == 0 && t_green == 128 && t_blue == 0)
				{
					ds_grid_set(grid, c, r, GroundType.TallGrass);
				}
				else if(t_red == 0 && t_green == 255 && t_blue == 0)
				{
					ds_grid_set(grid, c, r, GroundType.Grass);
				}
				else if(t_red == 128 && t_green == 128 && t_blue == 128)
				{
					ds_grid_set(grid, c, r, GroundType.Soil);
				}
			
				//logga("items");
				if (map_items != noone)
				{
					var item = scr_extractSubmapObject(map_items, false,false);
					if (item != "noone")
					{
						scr_placeSubmapObject(item,scr_asset_get_index("obj_pickable"),8+c*16, 8+r*16, 8);
						spawned_items++;
					}
				}
			}
		
			// DARK ORANGE: loot
			else if(blue == 0 && green == 64 && red == 128 && m_enableInteractables)
			{
				//logga("loot");
				if (map_chests != noone)
				{
					var chest = scr_extractSubmapObject(map_chests, false,false);
					if (chest != "noone")
					{
						scr_placeSubmapObject(chest,scr_asset_get_index("obj_interactable_"+chest),8+c*16, 8+r*16, 16);
					}
				}
			}
		
			//DARK PURPLE: landmarks
			else if(blue == 128 && green == 64 && red == 64)
			{
				//logga("landmark");
				instance_create_depth(c*16+8,r*16+8,0,map_landmark);	
			}
		
			// BROWN: cliff
			else if(blue == 0 && green == 128 && red == 128)
			{
				//logga("cliff at "+string(c)+":"+string(r));
				ds_grid_set(grid, c, r, GroundType.Cliff);
				var sh = instance_create_depth(c*16+8,r*16+8+48,0,obj_fx_cast_shadow_cliff);
				sh.storey = storey;
			
			}
		
			// DARK BROWN: stairs up
			else if (blue == 0 && green == 64 && red == 64)
			{
				//logga("stairs up");
				var sh = instance_create_depth(c*16+8,r*16+8+48,0,obj_fx_cast_shadow_cliff);
				sh.storey = storey;
			
				// only create the stair if this is the first encountered pixel with stair color
				var prevcol = col_prev;
				if !(color_get_blue(prevcol) == 0 && color_get_green(prevcol) == 64 && color_get_red(prevcol) == 64)
				{
					var st = instance_create_depth(8+c*16,8+r*16,0,obj_stair);
					st.type = 0;
				}
			}
		
			// DARK REDDISH BROWN: stairs down
			else if (blue == 0 && green == 32 && red == 64)
			{
				//logga("stairs down");
				var sh = instance_create_depth(c*16+8,r*16+8+48,0,obj_fx_cast_shadow_cliff);
				sh.storey = storey;
			
				// only create the stair if this is the first encountered pixel with stair color
				var prevcol = col_prev;
				if !(color_get_blue(prevcol) == 0 && color_get_green(prevcol) == 32 && color_get_red(prevcol) == 64)
				{
					var st = instance_create_depth(8+c*16,8+r*16,0,obj_stair);
					st.type = 1;
				}
			
			}
		
			ds_grid_set(itemsgrid, c, r, -1);
			c++;
			cc++;
		}
		r++;
		rr++;
	}

	instance_deactivate_object(obj_interactable);
	instance_deactivate_object(obj_pickable);
	instance_deactivate_object(obj_enemy);
}
else
{
	/////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////

	// setup the surface and drawing parameters for current submap
	surface_set_target(surf);
	draw_clear_alpha(c_white,0);
	var map = argument0;
	var hmap = scr_asset_get_index(sprite_get_name(map)+"_hmap");
	var mirrorx = choose(-1,1);
	var mirrory = choose(-1,1);
	var px = argument1;
	var py = argument2;
	var sprw = sprite_get_width(map);
	var sprh = sprite_get_height(map);

	var submapname = sprite_get_name(map);
	var submapobject = instance_create_depth(0,0,0,scr_asset_get_index(string_replace(submapname,"spr_","obj_")));

	// draw submap (note that submaps with hmap can't be mirrored on Y axis)
	if (submapobject.keeporientation == true)
	{
		draw_sprite_ext(map, 0, px + sprw/2, py + sprh/2, 1, 1, 0, c_white, 1);
	}
	else if (hmap != spr_missing)
	{
		draw_sprite_ext(map, 0, px + sprw/2, py + sprh/2, mirrorx, 1, 0, c_white, 1);
	}
	else
	{
		draw_sprite_ext(map, 0, px + sprw/2, py + sprh/2, mirrorx, mirrory, 0, c_white, 1);
	}
	surface_reset_target();

	// draw hmap, if exists
	if (hmap != spr_missing)
	{
		surface_set_target(heightsurf);
		draw_clear_alpha(c_black,0);
		draw_sprite_ext(hmap, 0, px + sprw/2, py + sprh/2, mirrorx, 1, 0, c_white, 1);
		surface_reset_target();
	}

	// Read elements that can be spawned by this submap
	var map_grass = submapobject.grass;
	var map_plants = submapobject.plants;
	var map_boulders = submapobject.boulders;
	var map_chests = submapobject.chests;
	var map_items = submapobject.items;
	var map_mobs = submapobject.mobs;
	var map_boss = submapobject.boss;
	var map_landmark = submapobject.landmark;
	var map_wall = submapobject.wall;

	// amounts spawned, for minimum spawn checl
	var spawned_mobs = 0;
	var spawned_boulders = 0;
	var spawned_items = 0;
	var minmobs = 3;
	var minboulders = 2;
	var minitems = 4;

	var c = px;
	var r = py;

	repeat(sprite_get_height(map))
	{
		c = px;
		repeat(sprite_get_width(map))
		{
			// read surface color
			var col = surface_getpixel(surf, c, r);
			var blue = color_get_blue(col);
			var green = color_get_green(col);
			var red = color_get_red(col);
		
			// read hmap color to get storey, skip if 0
			var storey = floor(color_get_red(surface_getpixel(heightsurf, c, r)) / 32);
			ds_grid_set(storeygrid, c, r, storey);
			tempstorey = storey;
			logga("pixel" + string(surface_getpixel(heightsurf, c, r)));
			logga("red" + string(color_get_red(surface_getpixel(heightsurf, c, r))));
			logga("storey "+string(storey));
			if (storey > 0) {
			
				// get storeys of surrouding tiles
				var s_topleft = floor(color_get_red(surface_getpixel(heightsurf, clamp(c-1,0,cols-1), clamp(r-1,0,rows-1))) / 32);
				var s_top = floor(color_get_red(surface_getpixel(heightsurf, clamp(c,0,cols-1), clamp(r-1,0,rows-1))) / 32);
				var s_topright = floor(color_get_red(surface_getpixel(heightsurf, clamp(c+1,0,cols-1), clamp(r-1,0,rows-1))) / 32);
				var s_left = floor(color_get_red(surface_getpixel(heightsurf, clamp(c-1,0,cols-1), clamp(r,0,rows-1))) / 32);
				var s_right = floor(color_get_red(surface_getpixel(heightsurf, clamp(c+1,0,cols-1), clamp(r,0,rows-1))) / 32);
				var s_bottomleft = floor(color_get_red(surface_getpixel(heightsurf, clamp(c-1,0,cols-1), clamp(r+1,0,rows-1))) / 32);
				var s_bottom = floor(color_get_red(surface_getpixel(heightsurf, clamp(c,0,cols-1), clamp(r+1,0,rows-1))) / 32);
				var s_bottomright = floor(color_get_red(surface_getpixel(heightsurf, clamp(c+1,0,cols-1), clamp(r+1,0,rows-1))) / 32);
			
				// generate cliff if this tile is higher than surrounding ones
				var docliff = false;
				if (storey > s_topleft) docliff = true;
				else if (storey > s_top) docliff = true;
				else if (storey > s_topright) docliff = true;
				else if (storey > s_left) docliff = true;
				else if (storey > s_right) docliff = true;
				else if (storey > s_bottomleft) docliff = true;
				else if (storey > s_bottom) docliff = true;
				else if (storey > s_bottomright) docliff = true;
			
				// set the read colors as cliff colors, unless the current color is for cliff stairs
				if (docliff)
				{
					if (!(blue == 0 && green == 64 && red == 64) && !(blue == 0 && green == 32 && red == 64))
					{
						blue = 0;
						green = 128;
						red = 128;
					}
				
					// walls on storey-1
					scr_createStoreyWall(c,r,storey-1,-1);
					scr_createStoreyWall(c,r+3,storey-1,-1);
				
					// outer walls on same storey
					if (storey > s_top) scr_createStoreyWall(c,r-1,storey,-1);
					if (storey > s_left) scr_createStoreyWall(c-1,r,storey,-1);
					if (storey > s_right) scr_createStoreyWall(c+1,r,storey,-1);
					if (storey > s_bottom) scr_createStoreyWall(c,r+1,storey,-1);
				
					// set unaccessible
					ds_grid_set(gridaccess,c,r,-1);
					ds_grid_set(gridaccess,c,r+1,-1);
					ds_grid_set(gridaccess,c,r+2,-1);
					ds_grid_set(gridaccess,c,r+3,-1);
				}
			}
		
			// BLUE: hero spawn point
			if(blue == 255 && green == 0 && red == 0)
			{
				ds_list_add(spawnpointsx,floor(8+c*16));
				ds_list_add(spawnpointsy,floor(8+r*16));
			}
		
			// RED: mob
			else if(blue == 0 && green == 0 && red == 255 && m_enableMobs)
			{
				// randomly choose a foe
				if (map_mobs != noone)
				{
					var mob = scr_extractSubmapObject(map_mobs, spawned_mobs < minmobs);
					if (mob != "noone")
					{
						instance_create_depth(8+c*16, 8+r*16, 0, scr_asset_get_index("obj_"+mob));
						spawned_mobs++;
					}
				}
			}
		
			// PURPLE: boss
			else if(blue == 255 && green == 0 && red == 255 && m_enableMobs)
			{
				if (map_boss != noone)
				{
					instance_create_depth(8+c*16, 8+r*16, 0, scr_asset_get_index("obj_"+map_boss));
				}
			}
		
			// BLACK: wall
			else if(blue == 0 && green == 0 && red == 0)
			{
				var w = instance_create_depth(8+c*16,8+r*16,0,map_wall);
				w.shadowType = ShadowType.Square;
				switch(storey)
				{
					case 0: w.storey_0 = true; break;
					case 1: w.storey_1 = true; break;
					case 2: w.storey_2 = true; break;
					case 3: w.storey_3 = true; break;
				}			
				ds_grid_set(wallsgrid, c, r, w);
			}
		
			// GREY: soil and boulders
			else if(blue == 128 && green == 128 && red == 128)
			{
				ds_grid_set(grid, c, r, GroundType.Soil);
				if (map_boulders != noone)
				{
					var boulder = scr_extractSubmapObject(map_boulders, spawned_boulders < minboulders);
					if (boulder != "noone")
					{
						scr_placeSubmapObject(boulder,scr_asset_get_index("obj_interactable_"+boulder),8+c*16, 8+r*16, 72);
						spawned_boulders++;
					}
				}
			}
		
			// AQUA BLUE: water
			else if(blue == 255 && green == 128 && red == 0)
			{
				ds_grid_set(grid, c, r, GroundType.Water);
				/*
				if (random(8) < 1)
				{
					instance_create_depth(c*16+8, r*16+8, 0, obj_fx_watersparkle);
				}
				*/
			}
		
			// LIGHT GREEN: wildgrass
			else if(blue == 0 && green == 255 && red == 0)
			{
				ds_grid_set(grid, c, r, GroundType.Grass);
				if (map_grass != noone)
				{
					var grass = scr_extractSubmapObject(map_grass, false);
					if (grass != "noone")
					{
						scr_placeSubmapObject(grass,scr_asset_get_index("obj_interactable_"+grass),8+c*16, 8+r*16, 4);
					}
				}
			}
		
			// DARK GREEN: tallgrass and trees
			else if(blue == 0 && green == 128 && red == 0)
			{
				ds_grid_set(grid, c, r, GroundType.TallGrass);
				if (map_plants != noone)
				{
					var plant = scr_extractSubmapObject(map_plants, false );
					if (plant != "noone")
					{
						scr_placeSubmapObject(plant,scr_asset_get_index("obj_interactable_"+plant),8+c*16, 8+r*16, 36);
					}
				}
			}
		
			// ORANGE: items
			else if(blue == 0 && green == 128 && red == 255)
			{
				if (map_items != noone)
				{
					var item = scr_extractSubmapObject(map_items, spawned_items < minitems);
					if (item != "noone")
					{
						scr_placeSubmapObject(item,scr_asset_get_index("obj_pickable"),8+c*16, 8+r*16, 8);
						spawned_items++;
					}
				}
			}
		
			// DARK ORANGE: loot
			else if(blue == 0 && green == 64 && red == 128)
			{
				if (map_chests != noone)
				{
					var chest = scr_extractSubmapObject(map_chests, false);
					if (chest != "noone")
					{
						scr_placeSubmapObject(chest,scr_asset_get_index("obj_interactable_"+chest),8+c*16, 8+r*16, 16);
					}
				}
			}
		
			//DARK PURPLE: landmarks
			else if(blue == 128 && green == 64 && red == 64)
			{
				instance_create_depth(c*16+8,r*16+8,0,map_landmark);	
			}
		
			// BROWN: cliff
			else if(blue == 0 && green == 128 && red == 128)
			{
				ds_grid_set(grid, c, r, GroundType.Cliff);
				var sh = instance_create_depth(c*16+8,r*16+8+48,0,obj_fx_cast_shadow_cliff);
				sh.storey = storey;
			}
		
			// DARK BROWN: stairs up
			else if (blue == 0 && green == 64 && red == 64)
			{
				var sh = instance_create_depth(c*16+8,r*16+8+48,0,obj_fx_cast_shadow_cliff);
				sh.storey = storey;
			
				// only create the stair if this is the first encountered pixel with stair color
				var prevcol = surface_getpixel(surf, c-1, r);
				if !(color_get_blue(prevcol) == 0 && color_get_green(prevcol) == 64 && color_get_red(prevcol) == 64)
				{
					var st = instance_create_depth(8+c*16,8+r*16,0,obj_stair);
					st.type = 0;
				}
			}
		
			// DARK REDDISH BROWN: stairs down
			else if (blue == 0 && green == 32 && red == 64)
			{
				var sh = instance_create_depth(c*16+8,r*16+8+48,0,obj_fx_cast_shadow_cliff);
				sh.storey = storey;
			
				// only create the stair if this is the first encountered pixel with stair color
				var prevcol = surface_getpixel(surf, c-1, r);
				if !(color_get_blue(prevcol) == 0 && color_get_green(prevcol) == 32 && color_get_red(prevcol) == 64)
				{
					var st = instance_create_depth(8+c*16,8+r*16,0,obj_stair);
					st.type = 1;
				}
			}
		
			ds_grid_set(itemsgrid, c, r, -1);
			c++;
		}
		r++;
	}
}