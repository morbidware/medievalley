///@description gmcallback_gotUserData
with(obj_debugger)
{
	str += "scr_updateUserFarm\n";
}
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var farmstring = string(ds_list_find_value(res,0));
var pickablesstring = string(ds_list_find_value(res,1));
var interactablesstring = string(ds_list_find_value(res,2));
ds_list_destroy(res);

global.othergridstring = farmstring;
global.otherpickables = pickablesstring;
global.otherinteractables = interactablesstring;