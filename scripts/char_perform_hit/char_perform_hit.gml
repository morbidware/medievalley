with(obj_atk_melee_area)
{
	instance_destroy();
}
with(obj_sickle_area)
{
	instance_destroy();
}
with(obj_fx_tool_trail)
{
	instance_destroy();
}
with(instance_nearest(x,y,obj_equipped))
{
	visible = false;
}

if(crono > 0)
{
	image_angle = random_range(-5,5);

	crono --;
	if(crono <= 0)
	{
		image_angle = 0;
		
		scr_setState(id, ActorState.Idle);
	}
}