///@function scr_storeDeathData()
if(global.isSurvival)
{
	exit;
}
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;

if (global.online)
{
	var data = ds_list_create();
	ds_list_add(data, global.lastdeathcell);
	ds_list_add(data, global.lastdeathx);
	ds_list_add(data, global.lastdeathy);
	ds_list_add(data, ds_grid_write(global.lastdeathinventory));

	var txt = ds_list_write(data);
	txt = string_replace_all(txt, "00000000", "_");
	txt = string_replace_all(txt, "303030303030", "%");

	sendDeathDataString(global.userid, txt, global.username);
	gmcallback_updateLastSave();
}

global.uisavestatus.saving = false;