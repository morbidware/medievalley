///@description gmcallback_receivedCharResting
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var sid = ds_list_find_value(res,0);
var val = real(ds_list_find_value(res,1));
ds_list_destroy(res);

var candidate = noone;

for(var i = 0; i < instance_number(obj_dummy); i++)
{
    var inst = instance_find(obj_dummy,i);
    if(inst.socketid == sid)
    {
       candidate = inst;
       break; 
    }
}

if(candidate != noone)
{
	logga("FOUND A CANDIDATE");
	if(val > 0)
	{
		candidate.resting = true;
		with (instance_nearest(candidate.x,candidate.y,obj_interactable))
		{
			isbusy = true;
		}
	}
	else
	{
		candidate.resting = false;
		with (instance_nearest(candidate.x,candidate.y,obj_interactable))
		{
			isbusy = true;
		}
	}
}
else
{
	logga("NOT FOUND A CANDIDATE");
}