var p = argument0;
var storeobj = "";
var compressed_string = "";
var interactablelistbox = ds_list_create();

storeobj = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.growcrono)+":"+string(p.growstage)+":"+string(p.quantity)+":"+string(p.durability)+":"+string(p.gridded)+":"+string(p.life)+":"+string(p.startingslot);

ds_list_read(interactablelistbox,global.interactables);
logga("uno"+string(interactablelistbox));

var pos = ds_list_find_index(interactablelistbox,storeobj);
ds_list_delete(interactablelistbox,pos);
logga(string(pos));
logga(string(interactablelistbox));

compressed_string = ds_list_write(interactablelistbox);
compressed_string = string_replace_all(compressed_string,"000000","_");
compressed_string = string_replace_all(compressed_string,"303A313A313A303A3","%");
compressed_string = string_replace_all(compressed_string,"70696E653A3","!");
compressed_string = string_replace_all(compressed_string,"77696C6467726173733A","&");
compressed_string = string_replace_all(compressed_string,"6F626A5F696E74657261637461626C655F",",");

if (global.online && compressed_string != "")
{
	global.interactables = ds_list_write(interactablelistbox);
	sendInteractablesStringSurvival(global.landid,compressed_string, global.username);
}

ds_list_destroy(interactablelistbox);