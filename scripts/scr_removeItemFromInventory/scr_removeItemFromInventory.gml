///@function scr_removeItemFromInventory(slot)
///@param slot
var slot = argument0;

// get the item at specified slot, then clear slot
var inv = instance_find(obj_playerInventory,0);
var obj = ds_grid_get(inv.inventory,slot,0);
ds_grid_set(inv.inventory,slot,0,-1);

// change item state
obj.slot = -1;
obj.state = 1;

with(obj_mission)
{
	event_perform(ev_other,ev_user1);
}
scr_storeItemsSurvival();

return obj;