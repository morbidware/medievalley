// create attack area
logga("char_perform_attack()");

if(!instance_exists(obj_atk_melee_area))
{
	
	var fx = instance_create_depth(x, y, 0, obj_fx_tool_trail);
	var area = instance_create_depth(x, y, 0, obj_atk_melee_area);
	if(lastdirection == Direction.Up)
	{
		area.image_angle = 90;
		area.y -= 16;
		fx.y -= 24;
	}
	else if(lastdirection == Direction.Down)
	{
		area.image_angle = -90;
		area.y -= 12;
		fx.y -= 2;
	}
	else if(lastdirection == Direction.Left)
	{
		area.image_angle = 180;
		area.x += 2;
		fx.y -= 8;
		fx.x -= 14;
		fx.image_yscale = -1;
	}
	else if(lastdirection == Direction.Right)
	{
		area.x -= 2;
		fx.y -= 8;
		fx.x += 14;
	}
	fx.image_angle = area.image_angle;
	fx.image_speed = 0;
	
}

// tool animation
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.attackTimer;
	if (other.stamina <= 0)
	{
		t *= 2;
	}
	scr_anim_melee(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}
with (instance_nearest(x,y,obj_fx_tool_trail))
{
	image_index = floor(((other.attackTimer - other.crono) / other.attackTimer) * (image_number - 1));
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	var area = instance_nearest(x,y,obj_atk_melee_area);
	if(area.hasCutSomething)
	{
		scr_loseDurability(id,true);
	}
	else
	{
		
		if(global.isChallenge && global.challengeType == 4)
		{
			global.missedInteractions++;
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.missedInteractionsValue;
		}
	}
	
	with(area)
	{
		instance_destroy();
	}
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	with(instance_nearest(x,y,obj_fx_tool_trail))
	{
		instance_destroy(id);
	}
	scr_setState(id,ActorState.Idle);
	
	//targetobject = noone;
}
logga("char_perform_attack() end");