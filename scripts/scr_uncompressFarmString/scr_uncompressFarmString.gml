///@function scr_uncompressFarmString
var str = argument0;
logga("scr_uncompressFarmString");
if(str == "")
{
	return "";
}
var cols = global.farmCols;
var rows = global.farmRows;
if(global.isSurvival)
{
	var cols = (global.survivalcellcolumns * global.mapsize) + (global.border * 2) + (global.clearance * 2);
	var rows = (global.survivalcellrows * global.mapsize) + (global.border * 2) + (global.clearance * 2);
}
var g = ds_grid_create(cols,rows);
for(var r = 0; r < rows; r++)
{
	for(var c = 0; c < cols; c++)
	{
		var pos = string_pos(":",str);
		if(pos == 0)
		{
			ds_grid_set(g,c,r,real(str));
		}
		else
		{
			ds_grid_set(g,c,r,real(string_copy(str,1,pos-1)));
			str = string_delete(str,1,pos);
		}
	}
}
var finalstr = ds_grid_write(g);
ds_grid_destroy(g);
logga("/--\|/--\|/--\|/--\|/--\|/--\|/--\|/--\|");
logga(finalstr);
logga("/--\|/--\|/--\|/--\|/--\|/--\|/--\|/--\|");
return finalstr;