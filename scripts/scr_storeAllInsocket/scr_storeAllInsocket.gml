///@function scr_storeAllInSocket
if(global.isSurvival)
{
	global.uisavestatus.saving = true;

	instance_activate_object(obj_pickable);
	instance_activate_object(obj_interactable);
	//instance_activate_object(obj_inventory);

	with(obj_debugger)
	{
		str += "scr_storeAllInSockets\n";
	}
	var pickableslist = ds_list_create();
	for(var i = 0; i < instance_number(obj_pickable); i++)
	{
		var p = instance_find(obj_pickable,i);
		if(p.destroy == false)
		{
			var storestr = string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.quantity)+":"+string(p.durability);
			ds_list_add(pickableslist,storestr);
		}
	}
	var pickables_uncompressed_string = ds_list_write(pickableslist);
	var pickables_compressed_string = pickables_uncompressed_string;
	pickables_compressed_string = string_replace_all(pickables_compressed_string,"000000","_");
	pickables_compressed_string = string_replace_all(pickables_compressed_string,"A666C696E743A313A3001","%");

	var interactableslist = ds_list_create();
	
	for(var i = 0; i < instance_number(obj_interactable); i++)
	{
		var p = instance_find(obj_interactable,i);
		if(p.destroy == false)
		{
			var storestr = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.growcrono)+":"+string(p.growstage)+":"+string(p.quantity)+":"+string(p.durability)+":"+string(p.life)+":"+string(p.startingslot);
			ds_list_add(interactableslist,storestr);
		}
	}
	var interactables_uncompressed_string = ds_list_write(interactableslist);
	var interactables_compressed_string = interactables_uncompressed_string;
	interactables_compressed_string = string_replace_all(interactables_compressed_string,"000000","_");
	interactables_compressed_string = string_replace_all(interactables_compressed_string,"303A313A313A303A3","%");
	interactables_compressed_string = string_replace_all(interactables_compressed_string,"70696E653A3","!");
	interactables_compressed_string = string_replace_all(interactables_compressed_string,"77696C6467726173733A","&");
	interactables_compressed_string = string_replace_all(interactables_compressed_string,"6F626A5F696E74657261637461626C655F",",");
	/*var itemslist = ds_list_create();

	for(var i = 0; i < instance_number(obj_inventory); i++)
	{
		var p = instance_find(obj_inventory,i);
		var storestr = p.itemid+":"+string(p.quantity)+":"+string(p.slot)+":"+string(p.durability)+":"+string(0);
		ds_list_add(itemslist,storestr);
	}*/

	var fground = instance_find(obj_survivalGround,0);
	var uncompressed_string = ds_grid_write(fground.grid);
	var compressed_string = scr_compressFarmString(fground.grid);
	if (global.online)
	{
		if(global.otheruserid != 0 && global.othersocketid != "")
		{
			logga("==== STORE IN 'OTHER' SOCKET");
			storeAllInSocketSurvival(compressed_string,pickables_compressed_string,interactables_compressed_string,global.landid);
		}
		else
		{
			logga("==== STORE IN 'MY' SOCKET");
			storeAllInSocketSurvival(compressed_string,pickables_compressed_string,interactables_compressed_string,global.landid);
		}
		//storeItemsInSocket(ds_list_write(itemslist));
	}

	ds_list_destroy(pickableslist);
	ds_list_destroy(interactableslist);
	//ds_list_destroy(itemslist);
	global.uisavestatus.saving = false;
}
else
{
	var level = instance_find(obj_gameLevel,0)
	if (room != roomFarm || instance_exists(obj_fadeout) || level.changingRoom == true)
	{
		return;
	}

	global.uisavestatus.saving = true;

	instance_activate_object(obj_pickable);
	instance_activate_object(obj_interactable);
	instance_activate_object(obj_inventory);

	with(obj_debugger)
	{
		str += "scr_storeAllInSockets\n";
	}
	var pickableslist = ds_list_create();
	for(var i = 0; i < instance_number(obj_pickable); i++)
	{
		var p = instance_find(obj_pickable,i);
		if(p.destroy == false)
		{
			var storestr = string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.quantity)+":"+string(p.durability);
			ds_list_add(pickableslist,storestr);
		}
	}

	var interactableslist = ds_list_create();
	for(var i = 0; i < instance_number(obj_interactable); i++)
	{
		var p = instance_find(obj_interactable,i);
		if(p.destroy == false)
		{
			var storestr = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.growcrono)+":"+string(p.growstage)+":"+string(p.quantity)+":"+string(p.durability)+":"+string(p.life)+":"+string(p.startingslot);
			ds_list_add(interactableslist,storestr);
		}
	}

	var itemslist = ds_list_create();

	for(var i = 0; i < instance_number(obj_inventory); i++)
	{
		var p = instance_find(obj_inventory,i);
		var storestr = p.itemid+":"+string(p.quantity)+":"+string(p.slot)+":"+string(p.durability)+":"+string(0);
		ds_list_add(itemslist,storestr);
	}

	var ground = instance_find(obj_baseGround,0);
	var gridstr = scr_compressFarmString(ground.grid);

	if (global.online)
	{
		if(global.otheruserid != 0 && global.othersocketid != "")
		{
			logga("==== STORE IN 'OTHER' SOCKET");
			storeAllInSocket(gridstr,ds_list_write(pickableslist),ds_list_write(interactableslist),global.otherusername);
		}
		else
		{
			logga("==== STORE IN 'MY' SOCKET");
			storeAllInSocket(gridstr,ds_list_write(pickableslist),ds_list_write(interactableslist),global.username);
		}
		storeItemsInSocket(ds_list_write(itemslist));
	}

	ds_list_destroy(pickableslist);
	ds_list_destroy(interactableslist);
	ds_list_destroy(itemslist);
	global.uisavestatus.saving = false;
}
