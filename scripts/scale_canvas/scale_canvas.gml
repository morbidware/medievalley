/// scale_canvas(basewidth, baseheight, currentwidth, currentheight, center);
//@param basewidth;
//@param baseheight;
//@param currentwidth
//@param currentheight
//@param centerwindow
if(global.ismobile)
{
	exit;
}
var base_w = argument0;
var base_h = argument1;
var curr_w = argument2;
var curr_h = argument3;
var centered = argument4;

var aspect = (base_w / base_h);

if ((curr_w / aspect) > curr_h)
{
    window_set_size((curr_h * aspect), curr_h);
}
else
{
    window_set_size(curr_w, (curr_w / aspect));
}

if (centered) window_center();

surface_resize(application_surface, max(window_get_width(), base_w), max(window_get_height(), base_h));
/*
logga("window get width " + string(window_get_width()));
logga("window get height " + string(window_get_height()));
logga("surface get width " + string(surface_get_width(application_surface)));
logga("surface get height " + string(surface_get_height(application_surface)));
*/