// tool animation
logga("char_perform_charge");
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	scr_anim_charge(other,other.lastdirection);
}

//override image index to always be the first one of the charge animation
image_index = 0;

// action
crono -= global.dt;
if(crono <= 0)
{
	
	var s = audio_play_sound(snd_woosh,0,0);
	audio_sound_pitch(s, 0.9 + random (0.2));
	scr_setState(id,ActorState.Attack);
}
logga("char_perform_charge end");