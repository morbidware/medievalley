/// @function draw_nine_slice_at(spr,x,y,w,h,scale)
/// @description Draws a nine-slice element using sprite frames from 0 to 8 for the nine graphic parts.
/// @param spr
/// @param x
/// @param y
/// @param w
/// @param h
/// @param scale

scr_nine_slice(argument0, argument1-argument3/2, argument2-argument4/2, argument1+argument3/2, argument2+argument4/2, argument5);