///@description gmcallback_receivedSocketNumPlantsList
logga("GMCB:RICEVUTO SOCKET NUMTREES LIST");
var str = argument0; 
var del = ":";
var pos = string_pos(del, str);
var dellen = string_length(del);

ds_list_clear(global.socketsnumtrees);

while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	ds_list_add(global.socketsnumtrees, real(stri));
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(global.socketsnumtrees, str);
/*
var myIndex = ds_list_find_index(global.socketsusernames,global.username);
ds_list_delete(global.sockets,myIndex);
ds_list_delete(global.socketsusernames,myIndex);
ds_list_delete(global.socketsnumtrees,myIndex);
*/