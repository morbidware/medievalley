///@description gmcallback_dropPickable
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while(pos)
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = real(ds_list_find_value(res,0));
var yy = real(ds_list_find_value(res,1));
var pid = string(ds_list_find_value(res,2));
var sid = string(ds_list_find_value(res,3));

ds_list_destroy(res);
logga("************§§§§§§§§§§§§§§§§§§§§§*************");
var plist = ds_priority_create();
logga("instance number obj_pickable " + string(instance_number(obj_pickable)));
for(var i = 0; i < instance_number(obj_pickable); i++)
{
	var pick = instance_find(obj_pickable,i);
	logga("pick is " + pick.itemid + " state is " + string(pick.state));
	if(pick.itemid == pid && pick.state != 1)
	{
		logga("ADDING " + pick.itemid);
		ds_priority_add(plist,pick,point_distance(pick.x,pick.y,xx,yy));
	}
}
logga("finished doing my priority list");
var nearestPickable = ds_priority_delete_min(plist);
logga("NOW GETTING NEAREST PICKABLE " + string(nearestPickable));
ds_priority_destroy(plist);
logga("nearest:"+string(nearestPickable));
if(nearestPickable != noone && nearestPickable != 0)
{
	logga("NEAREST PICKABLE IS != NOONE " + nearestPickable.itemid);
	var l = instance_number(obj_dummy);
	for(var i = 0; i < l; i++)
	{
		var candidateDummy = instance_find(obj_dummy,i);
		if(candidateDummy.socketid == sid)
		{
			nearestPickable.state = 1;
			nearestPickable.magnettarget = candidateDummy;
		}
	}
}

/*
var nearestPickable = instance_nearest(xx,yy,obj_pickable);
if(nearestPickable.itemid == pid)
{
	logga("");
	var l = instance_number(obj_dummy);
	for(var i = 0; i < l; i++)
	{
		var candidateDummy = instance_find(obj_dummy,i);
		if(candidateDummy.socketid == sid)
		{
			nearestPickable.state = 1;
			nearestPickable.magnettarget = candidateDummy;
		}
	}
}
*/