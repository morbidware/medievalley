///@function scr_setState(hero_id,actor_state)
///@description update Hero
///@param hero_id
///@param actor_state
with(argument0)
{
	var oldState = state;
	var newState = argument1;
	if(oldState != newState)
	{
		
		state = newState;
		anim_index = 0;
		switch(state)
		{
			case ActorState.Idle:
			logga("scr_setState to Idle");
			idle_time = 0;
			anim_speed = 0.1;
			break;
			
			case ActorState.Interact:
			logga("scr_setState to Interact");
			scr_setCrono(interactTimer);
			break;
			
			case ActorState.Eat:
			logga("scr_setState to Eat");
			scr_setCrono(eatTimer);
			break;
			
			case ActorState.Attack:
			logga("scr_setState to Attack");
			scr_setCrono(attackTimer);
			scr_setSpriteAction("melee",lastdirection,crono);
			break;
			
			case ActorState.Hit:
			if(global.isChallenge)
			{
				global.challengeHits ++;
				var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
				p.val = global.challengeHitsValue;	
			}
			logga("scr_setState to Hit");
			scr_setCrono(hitTimer);
			flashCrono = crono;
			scr_setSpriteAction("gethit",lastdirection,crono);
			image_angle = 0;
			break;
			
			case ActorState.Die:
			logga("scr_setState to Die");
			scr_setCrono(dieTimer);
			scr_setSpriteAction("gethit",lastdirection,crono);
			break;
			
			case ActorState.Pick:
			logga("scr_setState to Pick");
			scr_setCrono(pickTimer);
			if(highlightedobject.object_index == obj_interactable_sapling)
			{
				if(highlightedobject.hard)
				{
					scr_setCrono(pickTimerHard);
				}
			}
			scr_setSpriteAction("pick",lastdirection,crono);
			break;
			
			case ActorState.PickUp:
			logga("scr_setState to PickUp");
			scr_setCrono(pickUpTimer);
			scr_setSpriteAction("pick",lastdirection,crono);
			break;
			
			case ActorState.Chop:
			logga("scr_setState to Chop");
			scr_setCrono(chopTimer);
			scr_setSpriteAction("hammer",lastdirection,crono);
			break;
			
			case ActorState.Craft:
			logga("scr_setState to Craft");
			scr_setCrono(craftTimer);
			anim_speed = 0.1;
			break;
			
			case ActorState.Hoe:
			logga("scr_setState to Hoe");
			scr_setCrono(hoeTimer);
			scr_setSpriteAction("hammer",lastdirection,crono);
			break;
			
			case ActorState.Sow:
			logga("scr_setState to Sow");
			scr_setCrono(sowTimer);
			scr_setSpriteAction("hammer",lastdirection,crono);
			break;
			
			case ActorState.Watering:
			logga("scr_setState to Watering");
			scr_setCrono(wateringTimer);
			scr_setSpriteAction("watering",lastdirection,crono);
			break;
			
			case ActorState.Shovel:
			logga("scr_setState to Shovel");
			scr_setCrono(shovelTimer);
			scr_setSpriteAction("pick",lastdirection,crono);
			break;
			
			case ActorState.Charge:
			logga("scr_setState to Charge");
			scr_setCrono(chargeTimer);
			scr_setSpriteAction("melee",lastdirection,9999);
			break;
			
			case ActorState.Sickle:
			logga("scr_setState to Sickle");
			scr_setCrono(sickleTimer);
			scr_setSpriteAction("melee",lastdirection,crono);
			break;
			
			case ActorState.Mine:
			logga("scr_setState to Mine");
			scr_setCrono(mineTimer);
			scr_setSpriteAction("hammer",lastdirection,crono);
			break;
			
			case ActorState.Rest:
			logga("scr_setState to Rest");
			scr_setCrono(1);
			scr_setSpriteAction("pick",lastdirection,30);
			break;
			
			case ActorState.Rebuild:
			logga("scr_setState to Rebuild");
			scr_setCrono(120);
			scr_setSpriteAction("pick",lastdirection,crono);
			break;
			
			case ActorState.Cook:
			logga("scr_setState to Cook");
			scr_setCrono(craftTimer);
			anim_speed = 0.1;
			break;
			
			case ActorState.Hammer:
			logga("scr_setState to Hammer");
			scr_setCrono(hammerTimer);
			scr_setSpriteAction("hammer",lastdirection,-1);
			anim_speed = 0.1;
			break;
			
			case ActorState.Fish:
			logga("scr_setState to Fish");
			scr_setCrono(eatTimer);
			show_debug_message("Fishing");
			break;
		}
		
		if(room == roomChallenge)
		{
			if(state == ActorState.Chop || state == ActorState.Sickle || state == ActorState.Watering || state == ActorState.Mine || state == ActorState.Attack || state == ActorState.Pick || state == ActorState.Hoe)
			{
				var glc = instance_find(obj_gameLevelChallenge,0);
				
				if(glc.challengeState == 1)
				{
					if(state == ActorState.Watering || state == ActorState.Hoe || state == ActorState.Attack)
					{
						exit;
					}
					
					global.interactions ++;
					var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
					p.val = global.interactionsValue;
				}
			}
		}
		//show_debug_message("Set state to "+string(state));
	}
}