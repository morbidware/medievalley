///@function gmcallback_removeMalus(status)
///@param str
var status = real(argument0);
if(status == 1)
{
	var l = instance_create_depth(0,0,0,obj_challenge_malus_letter_callback);
	l.body = "Malus removed successfully!";
	
	with(obj_challenge_malus_letter)
	{
		global.challengeGalasilver -= seconds_malus_to_remove*global.challengeGalasilverPrice;
		global.challengeMalusSecondsRemoved += seconds_malus_to_remove;
		seconds_malus -= seconds_malus_to_remove;
		seconds_malus_to_remove = 0;
		if(seconds_malus == 0)
		{
			btnPlay.text = "Play";
			seconds_malus_to_remove = 0;
			instance_destroy(btnMalus);
			instance_destroy(btnMinus);
			instance_destroy(btnPlus);
			btnPlay.ox = 0;
		}
		else
		{
			btnMalus.text = "Remove "+string(seconds_malus_to_remove)+" s\n"+string(seconds_malus_to_remove*global.challengeGalasilverPrice)+" Galasilver";
		}
	}
}
else if(status == 2)
{
	var l = instance_create_depth(0,0,0,obj_challenge_malus_letter_callback);
	l.body = "You have not enough credits to perform the operation!";
	with(obj_challenge_malus_letter)
	{
		seconds_malus_to_remove = 0;
		btnMalus.text = "Remove "+string(seconds_malus_to_remove)+" s\n"+string(seconds_malus_to_remove*global.challengeGalasilverPrice)+" Galasilver";
	}
}
else if(status == 0)
{
	var l = instance_create_depth(0,0,0,obj_challenge_malus_letter_callback);
	l.body = "Malus wasn't removed! :(";
}