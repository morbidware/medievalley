///@description gmcallback_receivedMobDummyData
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var mid= ds_list_find_value(res,0);
var dmg = ds_list_find_value(res,1);
var sid = ds_list_find_value(res,2);

ds_list_destroy(res);

if(string(sid) == global.socketid)
{
	dmg = real(dmg);
	var enemy = real(mid);
	with(obj_enemy)
	{
		targeted = false;
	}
	enemy.targeted = true;

	if(instance_exists(obj_status_strength))
	{
		dmg *= 1.25;
	}
	if (m_godMode)
	{
		dmg = 9999999; // SEPHIROTH'S SUPERNOVA AYYYYY
	}
	var crit = false;
	var pla = instance_find(obj_char,0);
	if(random(100) < pla.critchance)
	{
		crit = true;
		dmg *= 1.50;
		dmg = floor(dmg);
	}
	enemy.life -= dmg;
	if(enemy.life > 0)
	{
		/*if(enemy.state == MobState.Charge || enemy.state == MobState.Attack)
		{
			crono = sufferTimer;
			flashCrono = crono;
		}
		else
		{*/
			scr_setMobState(enemy, MobState.Suffer);
		//}
	}
	else
	{
		scr_setMobState(enemy, MobState.Die);
	}
	var s = audio_play_sound(snd_sword_hit,0,0);
	audio_sound_pitch(s, 0.85 + random (0.35));
	
}