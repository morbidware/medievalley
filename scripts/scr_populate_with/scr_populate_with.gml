///@function scr_populate_with(obj,itemid,quantity,terrain,gridded,smoothpopup)
///@param object
///@param itemid
///@param quantity
///@param terrain
///@param gridded
///@param smoothpopup
with(obj_baseGround)
{
	var object = argument0;
	var itemid = argument1;
	var quantity = argument2;
	var terrain = argument3;
	var gridded = argument4;
	var smooth = argument5;
	
	var col = 0;
	var row = 0;
	var pos = -1;
	var found = false;
	var margin = 0;
	var tooclose = false;
	var listsize = 0;
	var attempts = 0;
	var gridrows = ds_list_create();
	var gridcols = ds_list_create();
	
	// Create a grid with only tiles that are usable for spawning the requested object
	repeat(cols)
	{
		row = 0;
		repeat(rows)
		{
			var add = true;
			if (ds_grid_get(gridaccess,col,row) < 1) // Must be accessible
			{
				add = false;
			}
			if (ds_grid_get(grid,col,row) != terrain) // Must be same terrain as specified
			{
				add = false;
			}
			if (ds_grid_get(wallsgrid,col,row) > 0) // Must not be occipied by wall
			{
				add = false;
			}
			if (gridded && ds_grid_get(itemsgrid,col,row) > 0) // Must not be occupied by another element
			{
				add = false;
			}
			if (col > floor(cols/2)-3) // Must not cover path (horizontal)
			{
				if (row > floor(rows/2)-3 && row < floor(rows/2)+3)
				{
					add = false;
				}	
			}
			if (row < floor(rows/2)+3) // Must not cover path (vertical)
			{
				if (col > floor(cols/2)-3 && col < floor(cols/2)+3)
				{
					add = false;
				}	
			}
			if (col > floor(cols/2)-4 && col < floor(cols/2)+4 && row < 16) // Must not cover the mailbox
			{
				add = false;
			}
			
			if (add == true)
			{
				ds_list_add(gridcols,col);
				ds_list_add(gridrows,row);
			}
			row++;
		}
		col++;
	}
	
	// Repeat for all objects to create
	listsize = ds_list_size(gridrows);
	repeat(quantity)
	{
		pos = -1;
		do
		{
			// If too many attempts have been made, quit
			attempts++;
			if (attempts > listsize)
			{
				return;
			}
			
			found = false;
			tooclose = false;
			
			// Get random tile if not chosen yet, otherwise step forward by some fraction of all the tiles
			if (pos < 0)
			{
				pos = floor(random(listsize));
			}
			else
			{
				pos += ceil(listsize/11);
			}
			if (pos >= listsize)
			{
				pos -= listsize;
			}
			
			// Get random tile from the available ones
			col = ds_list_find_value(gridcols,pos);
			row = ds_list_find_value(gridrows,pos);
			
			// Change margin depending by object type
			if (object == obj_pickable || itemid == "wildgrass")
			{
				margin = 8;
			}
			else
			{
				margin = 32;
			}
			
			// Check if walls or other objects are too close
			var i = 0;
			repeat(instance_number(obj_wall))
			{
				var obj = instance_find(obj_wall,i);
				if (point_distance((col*16)+8,(row*16)+8,obj.x,obj.y) < margin)
				{
					tooclose = true;
				}
				i++;
			}
			i = 0;
			repeat(instance_number(obj_interactable))
			{
				var obj = instance_find(obj_interactable,i);
				if (point_distance((col*16)+8,(row*16)+8,obj.x,obj.y) < margin)
				{
					tooclose = true;
				}
				i++;
			}
			if (tooclose)
			{
				continue;
			}
			
			// All ok at this point: place object
			if(gridded)
			{
				if (smooth)
				{
					with (scr_gameItemCreate((col*16)+8,(row*16)+8,object,itemid,-1,-1,-1,-1,true))
					{
						smoothspawn = true;
					}
				}
				else
				{
					scr_gameItemCreate((col*16)+8,(row*16)+8,object,itemid,-1,-1,-1,-1,true);
				}
				found = true;
				attempts = 0;
			}
			else
			{
				if (smooth)
				{
					with (scr_gameItemCreate((col*16)+random(16),(row*16)+random(16),object,itemid,-1,-1,-1,-1,false))
					{
						smoothspawn = true;
					}
				}
				else
				{
					scr_gameItemCreate((col*16)+random(16),(row*16)+random(16),object,itemid,-1,-1,-1,-1,false)
				}
				found = true;
				attempts = 0;
			}
		}
		until (found);
		if(global.online && global.isSurvival)
		{
			dropPickable((col*16)+8,(row*16)+8,itemid,1);
		}
	}
}