///@function scr_parseUserdata()
///@argument char_id
logga("scr_parseUserdataSurvival()");
var char = argument0;
var str = global.userdata; 
logga(str);
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
	pos -= 1;
	ds_list_add(res, string_copy(str, 1, pos));
	str = string_delete(str, 1, pos + dellen);
	pos = string_pos(del, str);
}
ds_list_add(res, str);

/*
charLife
charStamina
charStomach
charx
chary
global.totslots
global.musicvolume
global.sfxvolume
*/

//get life 
global.charLife = real(ds_list_find_value(res,0));
global.charStamina = real(ds_list_find_value(res,1));
char.life = global.charLife;
char.stamina = global.charStamina;
char.stomach = real(ds_list_find_value(res,2));
global.charx = real(ds_list_find_value(res,3));
global.chary = real(ds_list_find_value(res,4));
global.totslots = round(real(ds_list_find_value(res,5)));
with(obj_playerInventory)
{
	slots = global.totslots;
	ds_grid_resize(inventory,global.totslots,1);
}
if (ds_list_size(res) > 6)
{
	global.musicvolume = real(ds_list_find_value(res,6));
	audio_group_set_gain(audiogroup_music, global.musicvolume, 0);
}
if (ds_list_size(res) > 7)
{
	global.sfxvolume = real(ds_list_find_value(res,7));
	audio_group_set_gain(audiogroup_sfx, global.sfxvolume, 0);
}
ds_list_destroy(res);
logga("-------------------------");