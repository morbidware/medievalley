///@function scr_gameItemCreate(x,y,object,itemid,quantity,growrono,growstage,durability,gridded,life)
///@param x
///@param y 
///@param object
///@param itemid for example "seed"
///@param opt_quantity
///@param opt_growcrono
///@param opt_growstage
///@param opt_durability
///@param opt_gridded
///@param opt_life
var inst = instance_create_depth(argument[0],argument[1],0,argument[2]);
with(inst)
{
	//nid			= floor(scr_getGameItemValue(argument[3], "id"			));
	itemid		=		scr_getGameItemValue(argument[3], "itemid"		);
	name		=		scr_getGameItemValue(argument[3], "name"		);
	equippable	= round(scr_getGameItemValue(argument[3], "equippable"	));
	spritename	=		scr_getGameItemValue(argument[3], "spritename"	);
	maxquantity = round(scr_getGameItemValue(argument[3], "maxquantity"	));
	
	growtimer	= floor(scr_getGameItemValue(argument[3], "growtimer"	));
	growstages	= floor(scr_getGameItemValue(argument[3], "growstages"	));
	
	lossperusage= scr_getGameItemValue(argument[3], "lossperusage");
	durability	= round(scr_getGameItemValue(argument[3], "durability"	));
	lifeperstage = round(scr_getGameItemValue(argument[3], "lifeperstage"));
	
	needswatering = round(scr_getGameItemValue(argument[3], "needswatering"));
	
	mindamage = round(scr_getGameItemValue(argument[3], "mindamage"));
	maxdamage = round(scr_getGameItemValue(argument[3], "maxdamage"));
	collisionradius = round(scr_getGameItemValue(argument[3], "collisionradius"));
	consumable = floor(real(scr_getGameItemValue(argument[3], "consumable")));
	
	if(inst.object_index == obj_pickable)
	{
		collisionradius = 0;
	}
	
	isastructure = floor(scr_getGameItemValue(argument[3], "isastructure"));
	wallsw = round(scr_getGameItemValue(argument[3], "wallsw"));
	wallsh = round(scr_getGameItemValue(argument[3], "wallsh"));
	
	wearable = floor(real(scr_getGameItemValue(argument[3], "wearable")));
	defence = floor(real(scr_getGameItemValue(argument[3], "defence")));
	wearIndex = floor(real(scr_getGameItemValue(argument[3], "wearindex")));
	foodGain = floor(real(scr_getGameItemValue(argument[3], "foodgain")));
	
	// run early setup, needed before saved values are assigned
	event_perform(ev_other,ev_user14);
	
	if(argument_count > 4 && argument[4] > -1)
	{
		quantity = argument[4];
	}
	if(argument_count > 5 && argument[5] > -1)
	{
		growcrono = argument[5];
	}
	if(argument_count > 6 && argument[6] > 0)
	{
		growstage = argument[6];
	}
	if(argument_count > 7 && argument[7] > -1)
	{
		durability = argument[7];
	}
	if(argument_count > 8 && argument[8] > -1)
	{
		gridded = argument[8];
		if (gridded == 1)
		{
			var ground = instance_find(obj_baseGround,0);
			ds_grid_set(ground.itemsgrid, floor(x/16), floor(y/16), inst);
		}
	}
	
	// finish setup after saved values have been assigned
	event_perform(ev_other,ev_user15);
	
	if(argument_count > 9 && argument[9] > -1)
	{
		life = argument[9];
	}
	
	/*
	show_debug_message("----------------------");
	show_debug_message("CREATING NEW GAMEITEM");
	show_debug_message("nid = "	+ string(nid));
	show_debug_message("itemid = "	+ itemid);
	show_debug_message("name = "	+ name);
	show_debug_message("equippable = "	+ string(equippable));
	show_debug_message("spritename = "	+ spritename);
	show_debug_message("maxquantity = " + string(maxquantity));
	show_debug_message("quantity = " + string(quantity));
	show_debug_message("growtimer = " + string(growtimer));
	show_debug_message("growstages = " + string(growstages));
	show_debug_message("growstage = " + string(growstage));
	show_debug_message("lifeperstage = " + string(lifeperstage));
	show_debug_message("needswatering = " + string(needswatering));
	show_debug_message("mindamage = " + string(mindamage));
	show_debug_message("maxdamage = " + string(maxdamage));
	show_debug_message("collisionradius = " + string(collisionradius));
	show_debug_message("consumable = " + string(consumable));
	show_debug_message("isastructure = " + string(isastructure));
	show_debug_message("wallsh = " + string(wallsh));
	show_debug_message("wallsw = " + string(wallsw));
	show_debug_message("----------------------");
	*/
}
return inst;
