///@function scr_placeSubmapObject(itemid,object,x,y,clearanceradius);
///@param object
///@param x
///@param y
///@param clearanceradius
var oid = argument0;
var obj = argument1;
var px = argument2;
var py = argument3;
var clearanceradius = argument4;

// Check if there is enough free space around this object; if not, quit
global.submapItemCheck = ds_list_create();
with(obj_interactable){ ds_list_add(global.submapItemCheck,id); }
//with(obj_wall) { ds_list_add(global.submapItemCheck,id); }
var c = 0;
repeat (ds_list_size(global.submapItemCheck))
{
	var o = ds_list_find_value(global.submapItemCheck,c);
	c++;
	if (point_distance(px,py,o.x,o.y) < clearanceradius)
	{
		exit;
	}
}
ds_list_destroy(global.submapItemCheck);
var inst = noone
if (oid == noone || oid == "")
{
	inst = instance_create_depth(px,py,0,obj);
}
else
{
	inst = scr_gameItemCreate(px,py,obj,oid);
}