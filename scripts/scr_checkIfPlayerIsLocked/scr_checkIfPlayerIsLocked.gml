///@function scr_checkIfPlayerIsLocked(checkHighlighted)
///@description this function tries to detect if the player would be locked out in its current position
///@param checkHighlighted
var player = instance_find(obj_char,0);

if(global.isSurvival)
{
	var grid = mp_grid_create(0,0,global.survivalcellcolumns * 32,global.survivalcellrows * 32,16,16);
}
else
{
	var grid = mp_grid_create(0,0,global.farmCols,global.farmRows,16,16);
}

var walls = instance_number(obj_wall);
for (var i = 0; i < walls; i++)
{
	var wall = instance_find(obj_wall,i);
	mp_grid_add_cell(grid, wall.x/16, wall.y/16);
}
var interactables = instance_number(obj_interactable);
for (var i = 0; i < interactables; i++)
{
	var interactable = instance_find(obj_interactable,i);
	if (interactable.collisionradius > 0)
	{
		mp_grid_add_cell(grid, interactable.x/16, interactable.y/16);
	}
}
if (argument0 == true)
{
	mp_grid_add_cell(grid, player.highlightCol, player.highlightRow);
}

var lockpath = path_add();
if(global.isSurvival)
{
	if(scr_checkIfPlayerIsOnSameTile())
	{
		return true;
	}
	if (mp_grid_path(grid, lockpath, ((global.survivalcellcolumns * 32)*16)/2 + 16, 8, player.x, player.y, false))
	{
		//show_debug_message("Position is safe.");
		return false
	}
	else
	{
		//show_debug_message("Position is not safe.");
		return true;
	}
}
else
{
	if (mp_grid_path(grid, lockpath, (global.farmCols*16)/2 + 16, 8, player.x, player.y, false))
	{
		//show_debug_message("Position is safe.");
		return false;
	}
	else
	{
		//show_debug_message("Position is not safe.");
		return true;
	}
}
	
