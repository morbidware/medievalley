// tool animation
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.hoeTimer;
	if (other.stamina <= 0)
	{
		t *= 2;
	}
	scr_anim_hammer(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	if(room == roomChallenge)
	{
		// execute action if conditions are met
		if((groundtype == GroundType.Soil||groundtype == GroundType.TallGrass||groundtype == GroundType.Grass) && itemontile == -1 && accessible)
		{
			interactionCol = highlightCol;
			interactionRow = highlightRow;
			if(groundtype == GroundType.TallGrass)
			{
				
				if (global.online)
				{
					changeGroundTile(interactionCol, interactionRow, GroundType.Grass);
				}
				ds_grid_set(fg.grid, interactionCol, interactionRow, GroundType.Grass);
				with(obj_gameLevelChallenge)
				{
					if(challengeState == 1)
					{
						global.interactions ++;
						{
							var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
							p.val = global.interactionsValue;
						}
					}
				}
			}
			else if(groundtype == GroundType.Grass)
			{
				
				if (global.online)
				{
					changeGroundTile(interactionCol, interactionRow, GroundType.Soil);
				}
				ds_grid_set(fg.grid, interactionCol, interactionRow, GroundType.Soil);
				with(obj_gameLevelChallenge)
				{
					if(challengeState == 1)
					{
						global.interactions ++;
						with(obj_char)
						{
							var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
							p.val = global.interactionsValue;
						}
					}
				}
			}
			else if(groundtype == GroundType.Soil)
			{
				
				if (global.online)
				{
					changeGroundTile(interactionCol, interactionRow, GroundType.Plowed);
				}
				ds_grid_set(fg.grid, interactionCol, interactionRow, GroundType.Plowed);
				with(obj_gameLevelChallenge)
				{
					if(challengeState == 1)
					{
						total ++;
						global.interactions ++;
						global.finishedInteractions ++;
						with(obj_char)
						{
							var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
							p.val = global.finishedInteractionsValue;
						}
					}
				}
			}
			else
			{
				with(obj_gameLevelChallenge)
				{
					if(challengeState == 1)
					{
						global.missedInteractions ++;
						var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
						p.val = global.missedInteractions;
					}
				}
			}
			//show_debug_message("GOT " + string(ds_grid_get(fg.grid, interactionCol, interactionRow)));
			scr_updateTileSprites();
			scr_loseDurability(id,true);
			var s = audio_play_sound(snd_hoe,0,false);
			audio_sound_pitch(s, 0.85 + random (0.35));
			if(!global.ismobile)
			{
				instance_create_depth(interactionCol*16+8,interactionRow*16+8,depth,obj_fx_particle_soil);
			}
			global.savegroundtimer = 0;
		}
	}
	else
	{
		// execute action if conditions are met
		if(groundtype == GroundType.Soil && itemontile == -1 && accessible)
		{
			interactionCol = highlightCol;
			interactionRow = highlightRow;
			if (global.online)
			{
				changeGroundTile(interactionCol, interactionRow, GroundType.Plowed)
			}
			ds_grid_set(fg.grid, interactionCol, interactionRow, GroundType.Plowed);

			//show_debug_message("Posizione Tile: "+ string(interactionCol)+"y, "+string(interactionRow)+"x    Posizione Evidenziata: "+string(highlightCol)+"y, "+string(highlightRow)+"x");

			
			//show_debug_message("GOT " + string(ds_grid_get(fg.grid, interactionCol, interactionRow)));
			
			if(global.isSurvival)
			{
				var c =interactionCol-1;
				var r =interactionRow-1;
				for(i = 0;i<3;i++)
				{
					for(j = 0;j<3;j++)
					{
						
						scr_updateTileSpritesSurvival(c+i,r+j);
					}
				}
			}
			else
			{
				scr_updateTileSprites();
			}
			
			scr_loseDurability(id,true);
			var s = audio_play_sound(snd_hoe,0,false);
			audio_sound_pitch(s, 0.85 + random (0.35));
			if(!global.ismobile)
			{
				instance_create_depth(interactionCol*16+8,interactionRow*16+8,depth,obj_fx_particle_soil);
			}
			global.savegroundtimer = 0;
		}
	}
	
	if(global.isSurvival && global.online)
	{
		scr_storeGroundSurvival();
		with(obj_drawGroundSurvival)
		{
			col = other.interactionCol;
			row = other.interactionRow;

			event_perform(ev_other,ev_user0);
			
		}
	}
	else
	{
		scr_storeAllInsocket();
		
	}
}