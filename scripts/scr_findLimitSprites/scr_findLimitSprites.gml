///@function scr_findLimitSprites()
///@description this function assigns the correct sprites to map limit tiles. Also places walls there
///@param spr limit sprite
with(obj_baseGround)
{
	var left = false;
	var right = false;
	var top = false;
	var bottom = false;
	var topleft = false;
	var topright = false;
	var bottomleft = false;
	var bottomright = false;
	
	var gw = ds_grid_width(limitgrid);
	var gh = ds_grid_height(limitgrid);
	var createwall = true;
	
	for (var c = 0; c < gw; c++)
	{
		for (var r = 0; r < gh; r++)
		{
			var tile = ds_grid_get(limitgrid, c, r);
			if (tile <= 0)
			{
				continue;
			}
			
			createwall = true;
		
			left = false;
			if (c == 0)
			{
				left = true;
			}
			else if (c-1 >= 0)
			{
				if (ds_grid_get(limitgrid, c-1, r) > 0)
				{
					left = true;
				}
			}
			
			right = false;
			if (c == gw-1)
			{
				right = true;
			}
			else if (c+1 < gw)
			{
				if (ds_grid_get(limitgrid, c+1, r) > 0)
				{
					right = true;
				}
			}
			
			top = false;
			if (r == 0)
			{
				top = true;
			}
			else if (r-1 >= 0)
			{
				if (ds_grid_get(limitgrid, c, r-1) > 0)
				{
					top = true;
				}
			}
			
			bottom = false;
			if (r == gh-1)
			{
				bottom = true;
			}
			else if (r+1 < gh)
			{
				if (ds_grid_get(limitgrid, c, r+1) > 0)
				{
					bottom = true;
				}
			}
			
			topleft = false;
			if (c-1 >= 0 && r-1 >= 0)
			{
				if (ds_grid_get(limitgrid, c-1, r-1) > 0)
				{
					topleft = true;
				}
			}
			if(c == 0 || r == 0)
			{
				topleft = true;
			}
			
			topright = false;
			if (c+1 < gw && r-1 >= 0)
			{
				if (ds_grid_get(limitgrid, c+1, r-1) > 0)
				{
					topright = true;
				}
			}
			if(c == gw-1 || r == 0)
			{
				topright = true;
			}
			
			bottomleft = false;
			if (c-1 >= 0 && r+1 < gh)
			{
				if (ds_grid_get(limitgrid, c-1, r+1) > 0)
				{
					bottomleft = true;
				}
			}
			if(c == 0 || r == gh-1)
			{
				bottomleft = true;
			}
			
			bottomright = false;
			if (c+1 < gw && r+1 < gh)
			{
				if (ds_grid_get(limitgrid, c+1, r+1) > 0)
				{
					bottomright = true;
				}
			}
			if(c == gw-1 || r == gh-1)
			{
				bottomright = true;
			}
			
			if(c == 0 && r == 0)
			{
				show_debug_message("left " + string(left));
				show_debug_message("right " + string(right));
				show_debug_message("top " + string(top));
				show_debug_message("bottom " + string(bottom));
				show_debug_message("topleft " + string(topleft));
				show_debug_message("topright " + string(topright));
				show_debug_message("bottomleft " + string(bottomleft));
				show_debug_message("bottomright " + string(bottomright));
			}
			//top row
			if (left && right && top && bottom && topleft && topright && bottomleft && !bottomright) tile.image_index = 0;
			else if (left && right && top && !bottom) tile.image_index = 1;
			else if (left && right && top && bottom && topleft && topright && !bottomleft && bottomright) tile.image_index = 2;
			
			//bottom row
			else if (left && right && top && bottom && topleft && !topright && bottomleft && bottomright) tile.image_index = 3;
			else if (left && right && !top && bottom) tile.image_index = 4;
			else if (left && right && top && bottom && !topleft && topright && bottomleft && bottomright) tile.image_index = 5;
			
			//sides
			else if (left && !right && top && bottom) tile.image_index = 6;
			else if (!left && right && top && bottom) tile.image_index = 7;
			
			//outward angles
			else if (!left && right && !top && bottom && !topleft && bottomright) tile.image_index = 8;
			else if (left && !right && !top && bottom && !topright && bottomleft) tile.image_index = 9;
			else if (!left && right && top && !bottom && topright && !bottomleft) tile.image_index = 10;
			else if (left && !right && top && !bottom && topleft && !bottomright) tile.image_index = 11;
			
			//fill
			else if (left && right && top && bottom) { tile.image_index = 12; createwall = false; }
			
			if (createwall)
			{
				var w1 = instance_create_depth(tile.x + 8,tile.y + 8, 0, obj_wall);
				var w2 = instance_create_depth(tile.x + 8,tile.y + 24, 0, obj_wall);
				var w3 = instance_create_depth(tile.x + 24,tile.y + 8, 0, obj_wall);
				var w4 = instance_create_depth(tile.x + 24,tile.y + 24, 0, obj_wall);
				
				w1.visible = false;
				w2.visible = false;
				w3.visible = false;
				w4.visible = false;
				
				w1.storey_0 = true;
				w2.storey_0 = true;
				w3.storey_0 = true;
				w4.storey_0 = true;
				
				ds_grid_set(wallsgrid, clamp(c*2,0,cols-1), clamp(r*2,0,rows-1), w1);
				ds_grid_set(wallsgrid, clamp(c*2,0,cols-1), clamp(r*2+1,0,rows-1), w2);
				ds_grid_set(wallsgrid, clamp(c*2+1,0,cols-1), clamp(r*2,0,rows-1), w3);
				ds_grid_set(wallsgrid, clamp(c*2+1,0,cols-1), clamp(r*2+1,0,rows-1), w4);
			}
		}
	}
}