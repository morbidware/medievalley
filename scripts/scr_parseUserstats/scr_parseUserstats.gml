///@function scr_parseUserstats(stats)
///@argument stats
var str = argument0; 
logga (str);
if (str == "" || str == noone)
{
	exit;
}
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
	pos -= 1;
	ds_list_add(res, string_copy(str, 1, pos));
	str = string_delete(str, 1, pos + dellen);
	pos = string_pos(del, str);
}
ds_list_add(res, str);

global.stat_timeplayed = real(ds_list_find_value(res,0));
logga("-"+string(global.stat_timeplayed));
global.stat_days = real(ds_list_find_value(res,1));
logga("-"+string(global.stat_days));
global.stat_walked = real(ds_list_find_value(res,2));
logga("-"+string(global.stat_walked));
global.stat_croparea = real(ds_list_find_value(res,3));
logga("-"+string(global.stat_croparea));
global.stat_crops = real(ds_list_find_value(res,4));
logga("-"+string(global.stat_crops));
global.stat_kills = real(ds_list_find_value(res,5));
logga("-"+string(global.stat_kills));
global.stat_deaths = real(ds_list_find_value(res,6));
logga("-"+string(global.stat_deaths));
global.stat_goldearned = real(ds_list_find_value(res,7));
logga("-"+string(global.stat_goldearned));
global.stat_goldspent = real(ds_list_find_value(res,8));
logga("-"+string(global.stat_goldspent));
ds_list_destroy(res);