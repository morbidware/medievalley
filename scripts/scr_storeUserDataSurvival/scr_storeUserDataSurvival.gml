logga("scr_storeUserDataSurvival()");
if (!scr_allowedToSave()) { return; }
global.uisavestatus.saving = true;
if (global.online)
{
	var player = instance_find(obj_char,0);
		
	var charLife = player.life;
	var charStamina = player.stamina;
	var charStomach = player.stomach;
	var charx = player.x;
	var chary = player.y;
	logga("global.userid:"+string(global.userid));
	logga("charLife:"+string(charLife));
	logga("charStamina:"+string(charStamina));
	logga("charStomach:"+string(charStomach));
	logga("charx:"+string(charx));
	logga("chary:"+string(chary));
	logga("global.musicvolume:"+string(global.musicvolume));
	logga("global.sfxvolume:"+string(global.sfxvolume));
	logga("global.totslots:"+string(global.totslots));
	
	sendUserdataSurvivalString(global.userid, string(charLife)+":"+string(charStamina)+":"+string(charStomach)+":"+string(charx)+":"+string(chary)+":"+string(global.totslots)+":"+string(global.musicvolume)+":"+string(global.sfxvolume), global.username);
	//scr_storeStats();
	gmcallback_updateLastSave();
}
global.uisavestatus.saving = false;

logga("scr_storeUserDataSurvival() end");