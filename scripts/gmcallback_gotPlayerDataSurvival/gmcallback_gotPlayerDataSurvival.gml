///@description gmcallback_gotPlayerData
logga("gotPlayerDataSurvival Start");
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);
var usrname = string(ds_list_find_value(res,0));
var usrid = real(ds_list_find_value(res,1));
var socketid = string(ds_list_find_value(res,2));
var userdata = string(ds_list_find_value(res,3));

var farmstring = string(ds_list_find_value(res,4));
farmstring = scr_uncompressFarmString(farmstring);

var storeystring = string(ds_list_find_value(res,5));
storeystring = scr_uncompressFarmString(storeystring);

var wallsstring = string(ds_list_find_value(res,6));

var mobsstring = string(ds_list_find_value(res,7));

var pickablesstring = string(ds_list_find_value(res,8));

pickablesstring = string_replace_all(pickablesstring,"_","000000");
pickablesstring = string_replace_all(pickablesstring,"%","A666C696E743A313A3001");

var interactablesstring = string(ds_list_find_value(res,9));

interactablesstring = string_replace_all(interactablesstring,"_","000000");
interactablesstring = string_replace_all(interactablesstring,"%","303A313A313A303A3");
interactablesstring = string_replace_all(interactablesstring,"!","70696E653A3");
interactablesstring = string_replace_all(interactablesstring,"&","77696C6467726173733A");
interactablesstring = string_replace_all(interactablesstring,",","6F626A5F696E74657261637461626C655F");

var itemsstring = string(ds_list_find_value(res,10));

ds_list_destroy(res);

global.username = usrname;
global.userid = usrid;
global.farmid = usrid; 
global.socketid = socketid;
global.gridstring = farmstring;//this is uncompressed so it's a ds_grid_write (a string to read later)
global.storeystring = storeystring;//this is uncompressed so it's a ds_grid_write (a string to read later)
global.wallsstring = wallsstring;
global.mobsstring = mobsstring;
logga("global.wallsstring " + global.wallsstring);
global.pickables = pickablesstring;
global.interactables = interactablesstring;
global.items = itemsstring;
global.userdata = userdata;
global.lastjoinedroom = global.socketid;

with(obj_loading_panel)
{
	state = 1;
}
scr_prepareSurvival(WildType.Rural);
room_goto(roomSurvival);
logga("gotPlayerDataSurvivalEnd");