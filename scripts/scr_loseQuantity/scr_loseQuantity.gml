///@function scr_loseQuantity(id,amount)
///@param id
///@param quantity to lose
if (m_godMode)
{
	//exit;
}
with(obj_char)
{
	for(var i = 0; i < instance_number(obj_inventory); i++)
	{
		var inv = instance_find(obj_inventory,i);
		if(inv.slot == argument0.slot)
		{
			inv.quantity -= argument1;
			if(inv.quantity <= 0)
			{
				ds_grid_set(inventory.inventory,inv.slot,0,-1);
				var compareId = inv.itemid;
				instance_destroy(inv);
				with(instance_nearest(x,y,obj_equipped))
				{
					if (itemid == compareId)
					{
						instance_destroy();
					}
				}
				with(obj_char)
				{
					handState = HandState.Empty;
				}
			}
			break;
		}
	}
}