///@function scr_debugWardrobe()
///@description Enables a set of debug keys to switch wardrobe elements on the go

if (instance_exists(obj_console))
{
	return;
}

if (!m_enableConsole)
{
	return;
}
/*
// G gender - reloads all wardrobe
if (keyboard_check_pressed(ord("G")))
{
	if (gender == "m")
	{
		gender = "f";
	}
	else
	{
		gender = "m";
	}
	scr_selectWardrobePart(custom_body_index,false,"body");
	scr_selectWardrobePart(custom_head_index,false,"head");
	scr_selectWardrobePart(custom_upper_index,false,"upper");
	scr_selectWardrobePart(custom_lower_index,false,"lower");
}

// O-L body
if (keyboard_check_pressed(ord("O")))
{
	scr_selectWardrobePart(-1,false,"body");
}
else if (keyboard_check_pressed(ord("L")))
{
	scr_selectWardrobePart(-1,true,"body");
}

// Y-U head
if (keyboard_check_pressed(ord("Y")))
{
	scr_selectWardrobePart(-1,false,"head");
}
else if (keyboard_check_pressed(ord("U")))
{
	scr_selectWardrobePart(-1,true,"head");
}

// H-J upper
if (keyboard_check_pressed(ord("H")))
{
	scr_selectWardrobePart(-1,false,"upper");
}
else if (keyboard_check_pressed(ord("J")))
{
	scr_selectWardrobePart(-1,true,"upper");
}

// N-M lower
if (keyboard_check_pressed(ord("N")))
{
	scr_selectWardrobePart(-1,false,"lower");
}
else if (keyboard_check_pressed(ord("M")))
{
	scr_selectWardrobePart(-1,true,"lower");
}
*/