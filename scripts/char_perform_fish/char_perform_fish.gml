// tool animation
logga("char_perform_fish");
var inst = instance_nearest(x,y,obj_equipped);
with(inst)
{
	visible = true;
	
	//var t = other.hammerTimer;
	//if (other.stamina <= 0)
	//	t *= 2;
	//scr_anim_hammer(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
		
	// stop animation
	scr_setState(id,ActorState.Idle);
	inst.visible = false;
	
	if(inst.twoStepAction == 0)
	{
		fishingfloat.visible = true;
	
	}
	else
	{

		if(fishingfloat.attachedFish == true)
		{
			if(random(10)>3)
			{
				//canche di cosa può uscire
				show_debug_message("Caught fish");
			}
			else
			{
				show_debug_message("Lost fish");
			}
			fishingfloat.attachedFish = false;
		}
		fishingfloat.visible = false;
	}
	scr_loseDurability(id,true);
	//scr_storeAllInsocket();
}

logga("char_perform_fish end");