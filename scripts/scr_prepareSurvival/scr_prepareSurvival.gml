///@function scr_prepareSurvival(wildtype)
///@param wildtype

// prepare global survival map
global.survivalgrid = ds_grid_create(global.survivalmapsizex,global.survivalmapsizey);
global.survivalposx = 0;
global.survivalposy = floor(ds_grid_height(global.survivalgrid)/2);
global.survivalLastDirection = Direction.Down;

// Set seed by user id for all next operations (so the result is identical every time)
random_set_seed(global.userid);
randomize();
if(string_length(global.gridstring) > 0)
{
	logga("Survival already exists");
	exit;
}
else
{
	logga("Prepare new Survival");
}
// Prepare submaps
var allMaps = scr_createWildPoll(argument0);
var allLandmarks = scr_createLandmarkPoll(argument0);

// Choose the cells that will host a landmark
var landmarkCells = ds_list_create();
var cellRange = floor((ds_grid_width(global.survivalgrid) * ds_grid_height(global.survivalgrid)) / ds_list_size(allLandmarks));
for (var i = 0; i < ds_list_size(allLandmarks); i++)
{
	ds_list_add(landmarkCells, (i*cellRange) +floor(random(cellRange-1)));
}

// iterate wildland cells
var c = 0;
repeat(global.survivalmapsizex)
{
	var r = 0;
	repeat(global.survivalmapsizey)
	{
		// prepare this cell
		var cellnumber = (c * global.survivalmapsizey) + r;
		//show_debug_message("------ CELL NUMBER "+string(cellnumber)+" - "+string(c)+", "+string(r));
		var cellGrid = ds_grid_create(global.survivalcellcolumns, global.survivalcellrows);
		ds_grid_add_region(cellGrid,0,0,global.survivalcellcolumns-1, global.survivalcellrows-1, 0);
		
		// check if a landmark must be placed here
		var landmark = noone;
		var landmark_x = -1;
		var landmark_y = -1;
		for (var i = 0; i < ds_list_size(landmarkCells); i++)
		{
			if (ds_list_find_value(landmarkCells,i) == cellnumber)
			{
				landmark = ds_list_find_value(allLandmarks,i);
				landmark_x = round(random(global.survivalcellcolumns)-1);
				landmark_y = round(random(global.survivalcellrows)-1);
				break;
			}
		}
		
		// fill the cell with submaps
		var cellRow = 0;
		while(cellRow < global.survivalcellrows)
		{
			var cellCol = 0;
			while(cellCol < global.survivalcellcolumns)
			{
				// Early check if this submap space is free
				var cell = round(ds_grid_get(cellGrid, cellCol, cellRow))
				//show_debug_message("Cell value: "+string(cell));
				if (cell != 0.0)
				{
					cellCol++;
					continue;
				}
		
				// Choose one of the submaps to draw; a submap of 32x32 is 1 tile big in the "ground.mapgrid" ds_grid
				if (ds_list_size(allMaps) < 1)
				{
					allMaps = scr_createWildPoll(argument0);
				}
				var mapi = random(ds_list_size(allMaps));
				var selectedmapindex = mapi;
				var attempts = 0;
				var map = noone;
				
				// proceed until too many attemps are made (should never happen actually)
				while (attempts < ds_list_size(allMaps))
				{
					// if this submap is already occupied, skip early
					if (ds_grid_get(cellGrid, cellCol, cellRow) < 0)
					{
						continue;
					}
					
					if (landmark != noone && landmark_x == cellCol && landmark_y == cellRow)
					{
						// Load landmark (this won't increase index and attempts)
						map = landmark;
					}
					else
					{
						// Load submap
						map = ds_list_find_value(allMaps,floor(mapi));
						if(cellCol == 0 && cellRow == 0)
						{
							map = spr_rural_corner_tl;
						}
						else if(cellCol == global.survivalcellcolumns-1 && cellRow == 0)
						{
							map = spr_rural_corner_tr;
						}
						else if(cellCol == 0 && cellRow == global.survivalcellrows-1)
						{
							map = spr_rural_corner_bl;
						}
						else if(cellCol == global.survivalcellcolumns-1 && cellRow == global.survivalcellrows-1)
						{
							map = spr_rural_corner_br;
						}
						else if(cellCol == 0)
						{
							map = spr_rural_edge_l;
						}
						else if(cellCol == global.survivalcellcolumns-1)
						{
							map = spr_rural_edge_r;
						}
						else if(cellRow == 0)
						{
							map = spr_rural_edge_t;
						}
						else if(cellRow == global.survivalcellrows-1)
						{
							map = spr_rural_edge_b;
						}
						// Increase index and attempts even if this attempt is still going on
						mapi++;
						attempts++;
						if (mapi >= ds_list_size(allMaps))
						{
							mapi = 0;
						}
					}
					
					// If there is a problem with the selected map, retry
					if (map < 0 || map == undefined || map == noone)
					{
						continue;	
					}
					
					// get width and height of this submap (1 unit every 32 tiles/pixels)
					var mapw = ceil(sprite_get_width(map)/32);
					var maph = ceil(sprite_get_height(map)/32);
			
					//show_debug_message("> Try draw '"+sprite_get_name(map)+"' at "+string(c)+", "+string(r)+"; size "+string(mapw)+", "+string(maph));
				
					// Don't use submaps larger than 1x1 if a landmark is in this cell
					if (landmark != noone && (mapw > 1 || maph > 1))
					{
						//show_debug_message("Skipped: map larger than 1x1 and Landmark must be drawn.");
						continue;
					}
				
					// If this submap is larger than the space left in this cell, restart
					if (mapw > global.survivalcellcolumns-cellCol || maph > global.survivalcellrows-cellRow)
					{
						show_debug_message("Skipped 1: no free space remaining.");
						continue;
					}
					
					// If this submap is larger than the space left in this cell, restart
					else if (cellRow > 0 && cellCol > 0 && cellRow < global.survivalcellrows - 1 && cellCol < global.survivalcellcolumns - 1 && (cellRow + maph > global.survivalcellrows - 1 || cellCol + mapw > global.survivalcellcolumns-1))
					{
						show_debug_message("Skipped 2: no free space remaining.");
						continue;
					}
					
					
					// If submap size is larger than 1x1, check if the required space is not occupied by another submap (occupied space is -1)
					var skip = false;
					if (mapw > 1 || maph > 1)
					{
						for (var sw = 0; sw < mapw-1; sw++)
						{
							for(var sh = 0; sh < maph-1; sh++)
							{
								if (ds_grid_get(cellGrid, cellCol+sw, cellRow+sh) < 0)
								{
									//show_debug_message("Skipped: space already occupied.");
									skip = true;
								}
							}
						}
					}
					if (skip)
					{
						continue;
					}
					
					// Occupy all the space for this submap and save it into the cell
					//show_debug_message("Map size: "+string(mapw)+", "+string(maph));
					for (var sw = 0; sw < mapw; sw++)
					{
						for(var sh = 0; sh < maph; sh++)
						{
							if (sh > 0 || sw > 0)
							{
								//show_debug_message("Setting space "+string(cellCol+sw)+", "+string(cellRow+sh));
								ds_grid_set(cellGrid, cellCol+sw, cellRow+sh, -1);
							}
						}
					}
					ds_grid_set(cellGrid, cellCol, cellRow, map);
					ds_list_delete(allMaps,selectedmapindex);
					//show_debug_message("Added "+string(sprite_get_name(map)));
				
					// Jump to next free column
					cellCol += mapw;
					break;
				}
		
				// In case of too many attempts, retry
				if (attempts >= ds_list_size(allMaps))
				{
					//show_debug_message("WILDLAND GENERATION FAILED: too many attempts.");
					allMaps = scr_createWildPoll(WildType.Rural);
					attempts = 0;
				}
			}
			cellRow++;
		}
		// Cell finished, save it and move to next one
		ds_grid_set(global.survivalgrid, c, r, ds_grid_write(cellGrid));

		r++;
	}
	c++;
}