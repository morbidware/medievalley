///@function event_perform_ret(object, type, numb)
///this script allows us to receive a return type from an event
///@param object
///@param type - event type
///@param numb - event number
//show_debug_message("event perform ret");
var retval = noone;
global.retval = noone;
with(argument0)
{
	event_perform(argument1, argument2);
}
retval = global.retval;
global.retval = noone;
return retval;