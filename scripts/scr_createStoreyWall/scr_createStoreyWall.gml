///@function scr_createStoreyWall(c,r,storey,diagonal)
///@description Create wall object for Cliff at specified column, row and storey. The wall is returned.
///@param c
///@param r
///@param storey

var c = argument0;
var r = argument1;
var storey = argument2;
var diagonal = argument3;

var ground = instance_find(obj_baseGround,0);
var w = ds_grid_get(ground.wallsgrid,c,r);
if (w < 1)
{
	// Wall doesn't exist
	w = instance_create_depth(c*16+8,r*16+8,0,obj_wall_invisible);
	switch(storey)
	{
		case 0: w.storey_0 = true; break;
		case 1: w.storey_1 = true; break;
		case 2: w.storey_2 = true; break;
		case 3: w.storey_3 = true; break;
	}
	switch(diagonal)
	{
		case 0: w.diagonalTopLeft = true; break;
		case 1: w.diagonalTopRight = true; break;
		case 2: w.diagonalBottomLeft = true; break;
		case 3: w.diagonalBottomRight = true; break;
	}
}
else
{
	// Wall already exists
	switch(storey)
	{
		case 0: w.storey_0 = true; break;
		case 1: w.storey_1 = true; break;
		case 2: w.storey_2 = true; break;
		case 3: w.storey_3 = true; break;
	}
	switch(diagonal)
	{
		case 0: w.diagonalTopLeft = true; break;
		case 1: w.diagonalTopRight = true; break;
		case 2: w.diagonalBottomLeft = true; break;
		case 3: w.diagonalBottomRight = true; break;
	}
}
w.blocking = false;
ds_grid_set(ground.wallsgrid,c,r,w);
return w;