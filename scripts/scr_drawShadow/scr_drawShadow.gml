///@function scr_drawShadow(storey)
///@param storey
var storeyoffsetx = 0;
var storeyoffsety = 0;
var storey = argument0;
var st = shadowType;
if (storey < 0 && st != ShadowType.None)
{
	st = ShadowType.Blob;
}
storey = clamp(storey,0,4);
if (storey != 0)
{
	storeyoffsety = 16 * 3; // cliff tile height
	storeyoffsetx -= 60 * sin(degtorad(global.shadowangle)) * global.shadowlength;
	storeyoffsety -= 60 * cos(degtorad(global.shadowangle)) * global.shadowlength;
}
switch(st)
{
	case ShadowType.None:
		// No shadow ezpz
		break;
	
	case ShadowType.Blob:
		// Simple blob shadow
		var diff = ((abs(sprite_width) / 48) + 0.1) * shadowBlobScale;
		draw_sprite_ext(spr_shadow_blob, 0, x+offsetx-global.viewx+storeyoffsetx, y+offsety-global.viewy+storeyoffsety, diff, diff, 0, c_black, 1);
		break;
	
	case ShadowType.Sprite:
		// Draws the same object sprite, rotated and scaled to fake ground projection
		if (sprite_index < 0)
		{
			break;
		}
		draw_sprite_ext(sprite_index, image_index, x+offsetx-global.viewx+storeyoffsetx, y+offsety-global.viewy+storeyoffsety, 1, global.shadowlength, global.shadowangle, c_black, 1);
		break;
		
	case ShadowType.Square:
		// Square shadow with variable length, for walls, cliffs, and tall structures
		draw_sprite_ext(spr_shadow_square, 0, x+offsetx-global.viewx+storeyoffsetx, y+offsety-global.viewy+storeyoffsety, 1.1, global.shadowlength * (shadowLength / 19), global.shadowangle, c_black, 1);
		draw_sprite_ext(spr_shadow_square_top, 0, x+offsetx-global.viewx+(global.shadowsin*shadowLength*global.shadowlength)+storeyoffsetx, y+offsety-global.viewy+(global.shadowcos*shadowLength*global.shadowlength)+storeyoffsety, 1, 1, 0, c_black, 1);
		break;
		
	case ShadowType.Mob:
		// Draws a vaguely circular shadow, approximated to fit any mob
		draw_sprite_ext(spr_shadow_enemy, 0, x+offsetx-global.viewx+storeyoffsetx, y+offsety-global.viewy+storeyoffsety, 1, global.shadowlength, global.shadowangle, c_black, 1);
		break;
}