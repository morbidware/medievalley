///@function scr_anim_melee(object,direction,crono,maxcrono,steps,sidesprite,frontsprite)
///@param object the sword object
///@param direction direction of the player
///@param crono current crono timer for animation
///@param maxcrono total crono of this animation
///@param steps frames for the animation
///@param side side graphics of the tool
///@param front side graphics of the tool
logga("scr_anim_melee()");
var t = 1-((argument2-1)/argument3);
var a;

t = ceil(t * argument4) / argument4;

sprite_index = argument5;

switch(argument1)
{
	case Direction.Right:
	t *= 210;
	a = 90 - t;
	x = argument0.x + cos(degtorad(a)) * 6;
	y = argument0.y - sin(degtorad(a)) * 6;
	image_angle = a;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Left:
	t *= 210;
	a = 270 + t;
	x = argument0.x - cos(degtorad(a)) * 6;
	y = argument0.y + sin(degtorad(a)) * 6;
	image_angle = a;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Up:
	t *= 180;
	a = 180 - t;
	x = argument0.x + cos(degtorad(a)) * 6;
	y = argument0.y - 2 - sin(degtorad(a)) * 3;
	image_angle = a + 10 - (t * 0.2);
	if (argument6 != noone)
		sprite_index = argument6;
	break;
	
	case Direction.Down:
	t *= 160;
	a = 20 - t;
	x = argument0.x + cos(degtorad(a)) * 6;
	y = argument0.y + 2 - sin(degtorad(a)) * 6;
	image_angle = a - (t * 0.5);
	if (argument6 != noone)
		sprite_index = argument6;
	break;
}
y -= 16;

if(global.online)
{
	if(argument0.visible)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,image_xscale,image_yscale);
	}
	if(t >= 1.0)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,0.0,0.0);
	}
}
logga("scr_anim_melee() end");