///@function scr_createIsle(id, col, row, w, h, gt1, gt2)
///@param id
///@param col
///@param row
///@param width
///@param height
///@param noiseterrain
///@param fillterrain
with(argument0)
{
	var islecol = round(argument1);
	var islerow = round(argument2);
	var islewidth = round(argument3 / 2);
	var isleheight = round(argument4 / 2);
	
	var gt1 = argument5;
	var gt2 = argument6;
	
	//logga("---- ISLE AT "+string(islecol)+", "+string(islerow)+" - W "+string(islewidth*2)+", H "+string(isleheight*2)+" - GROUND "+string(gt1)+" > "+string(gt2));
	
	var c = 0;
	var r = 0;
	var px = 0;
	var py = 0;
	repeat (islewidth * 2) {
		c = 0;
		repeat (isleheight * 2) {
			
			px = clamp(islecol - islewidth + c, 0, cols-1);
			py = clamp(islerow - isleheight + r, 0, rows-1);
			
			// ignora casella se uno dei quadrati vicini è tallgrass, e questa casella non è tallgrass
			if (gt2 != GroundType.TallGrass)
			{
				if (ds_grid_get(grid,px-1,py-1) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px,py-1) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px+1,py-1) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px-1,py) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px+1,py) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px-1,py+1) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px,py+1) == GroundType.TallGrass) {c++; continue};
				if (ds_grid_get(grid,px+1,py+1) == GroundType.TallGrass) {c++; continue};
			}
			
			if (c == 0 || c == islewidth-1 || r == 0 || r == isleheight-1)
			{
				if (ds_grid_get(grid,px,py) != gt2)
				{
					ds_grid_set(grid,px,py,choose(gt1,gt2,gt2));				
				}
			}
			else
			{
				ds_grid_set(grid,px,py,gt2);				
			}
			c++;
		}
		r++;
	}
}