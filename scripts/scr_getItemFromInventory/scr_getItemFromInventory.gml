///@function scr_getItemFromInventory(slot)
///@param slot
///@description Returns the item in the inventory corresponding to the provided slot; returns 'noone' if something is wrong

if (argument0 < 0)
{
	return noone;
}

var inv = instance_find(obj_playerInventory,0);
var item = ds_grid_get(inv.inventory,argument0,0);
if (item >= 0)
{
	if (item.slot == argument0)
	{
		//show_debug_message("FOUND "+string(item));
		return item;
	}
}
else
{
	//show_debug_message("NOT FOUND");
	return noone;
}

//return noone;