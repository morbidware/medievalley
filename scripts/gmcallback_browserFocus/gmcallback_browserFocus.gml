// Resync daytime
if (room == roomInit || room == roomMain)
{
	exit;
}
else if(room == roomChallenge)
{
	with(obj_gameLevelChallenge)
	{
		event_perform(ev_other,ev_user4);	
	}
}
else 
{
	with (obj_daynightcycle)
	{
		event_perform(ev_other,ev_user0);	
	}
}