///@description scr_handleCharData
var str = argument0;
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = ds_list_find_value(res,0);
var yy = ds_list_find_value(res,1);
var ss1 = ds_list_find_value(res,2);
var ss2 = ds_list_find_value(res,3);
var ss3 = ds_list_find_value(res,4);
var ss4 = ds_list_find_value(res,5);
var ii = ds_list_find_value(res,6);
var sx = ds_list_find_value(res,7);
var sy = ds_list_find_value(res,8);
var sid = ds_list_find_value(res,9);
var nam = ds_list_find_value(res,10);
var sta = ds_list_find_value(res,11);
ds_list_destroy(res);

//logga("scr_handleCharData xx:"+xx+" yy:"+yy+" ss1:"+ss1+" ss2:"+ss2+" ss3:"+ss3+" ss4:"+ss4+" ii:"+ii+" sx:"+sx+" sy:"+sy+" sid:"+sid);

var candidate = noone;

for(var i = 0; i < instance_number(obj_dummy); i++)
{
    var inst = instance_find(obj_dummy,i);
    if(inst.socketid == sid)
    {
       candidate = inst;
       break; 
    }
}

if(candidate == noone)
{
	
    candidate = instance_create_depth(xx,yy,0,obj_dummy);
    candidate.x = real(xx);
    candidate.y = real(yy);
	candidate.tgt_x = real(xx);
    candidate.tgt_y = real(yy);
    candidate.socketid = string(sid);
    candidate.custom_body_sprite = real(ss1);
	candidate.custom_head_sprite = real(ss2);
	candidate.custom_lower_sprite = real(ss3);
	candidate.custom_upper_sprite = real(ss4);
	candidate.sprite_image_index = real(ii);
	candidate.sprite_image_xscale = real(sx);
	candidate.sprite_image_yscale = real(sy);
	candidate.sprite_index_crono = candidate.sprite_index_timer;
	candidate.name = nam;
	candidate.stamina = real(sta);
	getClientsInRoom(global.socketid);
	with (obj_char)
	{
		if (state = ActorState.Rest)
		{
			sendCharResting(global.socketid,1.0);
		}
	}
}
else
{
    candidate.tgt_x = real(xx);
    candidate.tgt_y = real(yy);
    candidate.socketid = string(sid);
    candidate.custom_body_sprite = real(ss1);
	candidate.custom_head_sprite = real(ss2);
	candidate.custom_lower_sprite = real(ss3);
	candidate.custom_upper_sprite = real(ss4);
	candidate.sprite_image_index = real(ii);
	candidate.sprite_image_xscale = real(sx);
	candidate.sprite_image_yscale = real(sy);
	candidate.sprite_index_crono = candidate.sprite_index_timer;
	candidate.name = nam;
	candidate.stamina = real(sta);
}