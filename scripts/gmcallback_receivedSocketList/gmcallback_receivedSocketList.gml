///@description gmcallback_receivedSocketsList
logga("GMCB:RICEVUTO SOCKET LIST " + string(argument0));
if(argument0 == "$$$$$")
{
	with (obj_farmboard_panel)
	{
		error = "";
		refreshing = false;
	}
	exit;
}
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var strsockets = string(ds_list_find_value(res,0));
var strusernames = string(ds_list_find_value(res,1));
var strnumtrees = string(ds_list_find_value(res,2));
var strnumrocks = string(ds_list_find_value(res,3));
var strnumplants = string(ds_list_find_value(res,4));
var strnumvisitors = string(ds_list_find_value(res,5));

ds_list_destroy(res);
////////////////////////////////////////////////////////
str = strsockets; 
del = ":";
pos = string_pos(del, str);
dellen = string_length(del);

ds_list_clear(global.sockets);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.sockets, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	//logga(str);
}
ds_list_add(global.sockets, str);
////////////////////////////////////////////////////////
str = strusernames; 
pos = string_pos(del, str);

ds_list_clear(global.socketsusernames);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.socketsusernames, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	//logga(str);
}
ds_list_add(global.socketsusernames, str);
////////////////////////////////////////////////////////
str = strnumtrees; 
pos = string_pos(del, str);

ds_list_clear(global.socketsnumtrees);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.socketsnumtrees, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	//logga(str);
}
ds_list_add(global.socketsnumtrees, str);
////////////////////////////////////////////////////////
str = strnumrocks; 
pos = string_pos(del, str);

ds_list_clear(global.socketsnumrocks);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.socketsnumrocks, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	//logga(str);
}
ds_list_add(global.socketsnumrocks, str);
////////////////////////////////////////////////////////
str = strnumplants; 
pos = string_pos(del, str);

ds_list_clear(global.socketsnumplants);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.socketsnumplants, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	//logga(str);
}
ds_list_add(global.socketsnumplants, str);
////////////////////////////////////////////////////////
str = strnumvisitors; 
pos = string_pos(del, str);

ds_list_clear(global.socketsnumvisitors);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.socketsnumvisitors, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	//logga(str);
}
ds_list_add(global.socketsnumvisitors, str);
////////////////////////////////////////////////////////

var myIndex = ds_list_find_index(global.sockets,global.socketid);
//logga("myIndex is: " + string(myIndex));
if(myIndex >= 0)
{
	ds_list_delete(global.sockets,myIndex);
	ds_list_delete(global.socketsusernames,myIndex);
	ds_list_delete(global.socketsnumtrees,myIndex);
	ds_list_delete(global.socketsnumrocks,myIndex);
	ds_list_delete(global.socketsnumplants,myIndex);
	ds_list_delete(global.socketsnumvisitors,myIndex);
}

for(var i = 0; i < ds_list_size(global.sockets); i++)
{
	if(ds_list_find_value(global.socketsnumvisitors,i) == -1)
	{
		ds_list_delete(global.sockets,i);
		ds_list_delete(global.socketsusernames,i);
		ds_list_delete(global.socketsnumtrees,i);
		ds_list_delete(global.socketsnumrocks,i);
		ds_list_delete(global.socketsnumplants,i);
		ds_list_delete(global.socketsnumvisitors,i);
		i--;
	}
}

with (obj_farmboard_panel)
{
	error = "";
	refreshing = false;
}
logga("GMCB:RICEVUTO SOCKET LIST END");
/*
var str = argument0; 
var del = ":";
var pos = string_pos(del, str);
var dellen = string_length(del);

ds_list_clear(global.sockets);
while(pos)
{
    pos -= 1;
	var stri = string_copy(str, 1, pos);
	
	ds_list_add(global.sockets, stri);
	
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
	logga(str);
}
ds_list_add(global.sockets, str);
*/

