///@function scr_loadWardrobeData
///@description loads all the wardrobe entires in the wardrobe list

// Define end of line by OS
global.streol = 2;
if(os_type == os_macosx && os_browser == browser_not_a_browser)
{
	global.streol = 1;
}

// Open file for reading
var filecsv = file_text_open_read("wardrobe.csv");

// Read file lines
var delimiter = ";";
var count = 0;
while(!file_text_eof(filecsv))
{
	// Read the line, removing tabs
	var line = file_text_readln(filecsv);
	line = string_replace_all(line,"\t","");
	//show_debug_message("--- Submap line is "+line);
	
	// Ignore comments and lines too short (which are probably empty)
	if (string_length(line) < 10 || string_pos("//",line) > 0)
	{
		count++;
		continue;
	}
	
	// Read variables from the line by gradually truncating it
	var list = ds_list_create();
	while(string_length(line) > 0)
	{
		var delimiterPos = string_pos(delimiter,line);
		var value;
		
		if(delimiterPos == 0)
		{
			// No delimiters left: read the whole remaining line
			delimiterPos = string_length(line);
			value = string_copy(line,1,delimiterPos-global.streol);
		}
		else
		{
			// Delimiters found: read till first delimiter
			value = string_copy(line,1,delimiterPos-1);
		}
		
		// Add to list, truncate line and move cursor over
		ds_list_add(list,value);
		line = string_copy(line,delimiterPos+1, string_length(line));
		count++;
	}
	
	// Detect in which wardrobe list to add this entry
	switch (ds_list_find_value(list,2))
	{
		case "body": ds_list_add(global.wardrobe_body,ds_list_write(list)); break;
		case "head": ds_list_add(global.wardrobe_head,ds_list_write(list)); break;
		case "upper": ds_list_add(global.wardrobe_upper,ds_list_write(list)); break;
		case "lower": ds_list_add(global.wardrobe_lower,ds_list_write(list)); break;
	}
	show_debug_message(ds_list_size(global.wardrobe_head));
}
file_text_close(filecsv);
