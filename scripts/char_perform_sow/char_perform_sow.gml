// tool animation
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.sowTimer;
	if (other.stamina <= 0)
		t *= 2;
	var strippedid = string_replace(itemid, "seeds", "_seeds");
	scr_anim_watering(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+strippedid),scr_asset_get_index("spr_equipped_"+strippedid));
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	
	// execute action if conditions are met
	if(groundtype == GroundType.Plowed && itemontile == -1 && accessible)
	{
		var eq = instance_nearest(x,y,obj_equipped);
		var strippedid = string_replace(eq.itemid, "seeds", "");
		var obj = scr_asset_get_index("obj_interactable_"+strippedid+"_plant");
		//show_debug_message("--------------------TRYING TO PLANT:");
		//show_debug_message("obj_interactable_"+strippedid+"_plant");
		//show_debug_message("------------------------------------");
		if(obj > -1)
		{
			var itm = scr_gameItemCreate(8+interactionCol*16, 8+interactionRow*16, obj, strippedid+"plant");
			if (global.online)
			{
				sowPlant(interactionCol,interactionRow,obj,strippedid+"plant");
			}
			itm.growstage = 1;
			itm.gridded = 1;
			if (itm.needswatering == 1)
			{
				itm.growcrono = 0;	
			}
			else
			{
				itm.growcrono = itm.growtimer;
			}
			ds_grid_set(fg.itemsgrid, interactionCol, interactionRow, itm);
			global.stat_crops++;
		}
		
		with(obj_playerInventory)
		{
			scr_loseQuantity(ds_grid_get(inventory, currentSelectedItemIndex, 0),1);
		}
	}
	global.saveitemstimer = 0;
	if(global.isSurvival && global.online)
	{
		scr_storeGroundSurvival()
	}
	else
	{
		scr_storeAllInsocket();
	}
}