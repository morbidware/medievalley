if(crono > 0)
{
	with (obj_equipped)
	{
		visible = false;
	}
	
	/*
	image_yscale -= 1.0/dieTimer;	
	image_xscale += 1.0/dieTimer;
	*/
	image_angle = random_range(-5,5);
	crono --;
	if(crono <= 0)
	{
		if (room != roomChallenge)
		{
			global.stat_deaths++;
		}
		
		image_yscale = 1.0;	
		image_xscale = 1.0;
		image_angle = 0;
		
		visible = false;
		
		instance_create_depth(x,y,0,obj_prefab_tomb);
		if(room == roomSurvival)
		{
			
			with(obj_inventory)
			{
				if (slot < global.inv_inventorylimit)
				{
					scr_removeItemFromInventory(slot);
					scr_gameItemCreate(other.x, other.y, obj_pickable, itemid, quantity);
					instance_destroy();
				}
			}
			reloadPage();
		}
		if(room == roomWild)
		{
			// store death data
			global.lastdeathcell = (global.wildposy * global.wildmapsizey) + global.wildposx;
			global.lastdeathx = x;
			global.lastdeathy = y;
			global.lastdeathinventory = ds_grid_create(global.inv_inventorylimit,3);
			for (var i = 0; i < global.inv_inventorylimit; i++)
			{
				var item = scr_getItemFromInventory(i);
				if (item != noone)
				{
					ds_grid_set(global.lastdeathinventory, i, 0, item.itemid);
					ds_grid_set(global.lastdeathinventory, i, 1, item.quantity);
					ds_grid_set(global.lastdeathinventory, i, 2, item.durability);
				}
				else
				{
					ds_grid_set(global.lastdeathinventory, i, 0, noone);
				}
			}
			scr_storeDeathData();
			with(obj_inventory)
			{
				if (slot < global.inv_inventorylimit)
				{
					scr_removeItemFromInventory(slot);
					scr_gameItemCreate(other.x, other.y, obj_pickable, itemid, quantity);
					instance_destroy();
				}
			}
			with (obj_status)
			{
				instance_destroy();
			}		
			with (obj_atk_melee_area)
			{
				instance_destroy();
			}
			scr_storeItems();
		}
		else if (room == roomChallenge)
		{
			with(obj_gameLevelChallenge)
			{
				crono = 0.1;
			}
		}
		
	}
}