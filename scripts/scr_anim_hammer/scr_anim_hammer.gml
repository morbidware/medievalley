///@function scr_anim_hammer(object,direction,crono,maxcrono,steps,sidesprite,frontsprite)
///@param object the sword object
///@param direction direction of the player
///@param crono current crono timer for animation
///@param maxcrono total crono of this animation
///@param steps frames for the animation
///@param side side graphics of the tool
///@param front side graphics of the tool
logga("scr_anim_hammer");
var t = 1-((argument2-1)/argument3);
var a;
var t2 = (ceil(t * argument4) / argument4) * 140;
var t3 = (ceil(t * argument4) / argument4);

switch(argument1)
{
	case Direction.Right:
	a = 110 - t2;
	x = argument0.x - 8 + cos(degtorad(a)) * 14;
	y = argument0.y - 2 - sin(degtorad(a)) * 6;
	image_angle = 110 - (t2*1.2);
	image_yscale = 1;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Left:
	a = 250 + t2;
	x = argument0.x + 8 - cos(degtorad(a)) * 14;
	y = argument0.y - 2 + sin(degtorad(a)) * 6;
	image_angle = 250 + (t2*1.2);
	image_yscale = 1;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Up:
	image_angle = 0;
	x = argument0.x;
	y = argument0.y - 15 + (t3 * 14);
	image_yscale = 1 - (t3 * 1.6);
	if (argument6 != noone)
		sprite_index = argument6;
	break;
	
	case Direction.Down:
	image_angle = 0;
	x = argument0.x;
	y = argument0.y - 7 + (t3 * 16);
	image_yscale = 1 - (t3 * 1.6);
	if (argument6 != noone)
		sprite_index = argument6;
	break;
}
y -= 16;
if(global.online)
{
	if(argument0.visible)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,image_xscale,image_yscale);
	}
	if(t >= 1.0)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,0.0,0.0);
	}
}
logga("scr_anim_hammer end");