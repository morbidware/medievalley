///@description gmcallback_eventPerformInteractable
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = real(ds_list_find_value(res,0));
var yy = real(ds_list_find_value(res,1));
var iid = real(ds_list_find_value(res,2));
var oid = real(ds_list_find_value(res,3));
var evt = real(ds_list_find_value(res,4));
var evn = real(ds_list_find_value(res,5));

ds_list_destroy(res);
/*
logga("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
logga("xx " + xx);
logga("yy " + yy);
logga("iid " + iid);
logga("oid " + oid + " " + object_get_name(oid));
logga("evt " + evt);
logga("evn " + evn);
logga("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
*/
if(evt == 7 && evn == 12)
{
	with(instance_nearest(xx,yy,oid))
	{
		isNet = true;
		event_perform(ev_other, ev_user2); // calling ev_user0 causes a crash, so a different and free event has been chosen
	}
}
/*
if(evt == 7 && evn == 10)
{
	with(instance_nearest(xx,yy,oid))
	{
		event_perform(ev_other, ev_user0);
	}
}
*/

