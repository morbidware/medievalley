///@function gmcallback_pageHidden(isHidden)
///@param isHidden

var timestamp = round(get_timer() / 1000000);
// Page is hidden
if (argument0 == 1)
{
	var i = 0;
	repeat (instance_number(obj_interactable))
	{
		var inst = instance_find(obj_interactable, i);
		inst.pagehidetimestamp = timestamp;
		i++
	}
	var char = instance_find(obj_char,0);
	if (instance_exists(char))
	{
		char.pagehidetimestamp = timestamp;
	}
	
}
// Page is shown
else
{
	// Resync daytime
	if (room == roomInit || room == roomChallenge || room == roomMain)
	{
		exit;
	}
	else with (obj_daynightcycle)
	{
		logga("Updating timestamp...");
		event_perform(ev_other,ev_user0);	
	}
	
	var i = 0;
	repeat (instance_number(obj_interactable))
	{
		var inst = instance_find(obj_interactable, i);
		if (!inst.pagehidetimestamp > 0)
		{
			if (timestamp - inst.pagehidetimestamp > 1 && inst.growcrono > 1)
			{
				inst.growcrono = clamp(inst.growcrono - ((timestamp - inst.pagehidetimestamp) / (1.0/60.0)), 1, 99999999);
			}
			inst.pagehidetimestamp = -1;
		}
		i++;
	}
	var char = instance_find(obj_char,0);
	if (char.pagehidetimestamp > 0)
	{
		if (char.state == ActorState.Rest && instance_exists(char.targetobject))
		{
			var stepsPassed = (timestamp - char.pagehidetimestamp);
			char.stamina = clamp(char.stamina + (char.targetobject.staminagain * stepsPassed), 0, 100);
			char.life = clamp(char.life + (char.targetobject.healthgain * stepsPassed), 0, 100);
			global.saveuserdatatimer = 0;
		}
		char.pagehidetimestamp = -1;
	}
}