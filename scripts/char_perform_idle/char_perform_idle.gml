//get input
prevx = x;
prevy = y;
if(controlType == CharControlType.manual)
{
	if (!global.ismobile)
	{
		if(keyboard_check(ord("A"))||keyboard_check(ord("D"))||keyboard_check(ord("S"))||keyboard_check(ord("W")))
		{
			left = - keyboard_check(ord("A"));
			right = keyboard_check(ord("D"));
			up = - keyboard_check(ord("W"));
			down = keyboard_check(ord("S"));
			movetarget_x = -1;
			movetarget_y = -1;
		}
		else
		{
			left = - keyboard_check(vk_left);
			right = keyboard_check(vk_right);
			up = - keyboard_check(vk_up);
			down = keyboard_check(vk_down);
			movetarget_x = -1;
			movetarget_y = -1;
		}
	}
	else
	{
		
					/*
		if (movetarget_x > 0 && movetarget_y > 0)
		{
			var angle = degtorad(point_direction(x,y,movetarget_x,movetarget_y));
			left = cos(angle);
			right = 0;
			up = -sin(angle);
			down = 0;
		}*/
	}
}
var h = left + right;
var v = up + down;
if (instance_exists(obj_fadein) || instance_exists(obj_fadeout))
{
	h = 0;
	v = 0;
}

if(room == roomChallenge)
{
	var gameLevel = instance_find(obj_gameLevelChallenge,0);
	if(gameLevel != noone)
	{
		if(gameLevel.challengeState != 1)
		{
			h = 0;
			v = 0;
		}
	}
}
// walk if UI is not locked
if((h != 0 || v != 0) && !global.lockHUD)
{
	if (global.lockHUD)
	{
		exit;
	}
	
	if (navtarget)
	{
		instance_destroy(navtarget);
		navtarget = noone;
	}
	
	//close crafting ui
	with (obj_crafting_cat)
	{
		state = 0;
	}
	
	targetx = -1;
	targety = -1;
	
	var angle = point_direction(0,0,h,v);
	currentdir = angle;
	var walkSpeed = spd;
	
	if(stamina == 0){walkSpeed/=2;}
	
	if(instance_exists(obj_status_speed))
	{
		walkSpeed *= 1.25;
	}
	
	if (h != 0 || v != 0)
	{
		x += cos(degtorad(angle)) * walkSpeed * global.dt;
		y -= sin(degtorad(angle)) * walkSpeed * global.dt;
	}
	currentspd = walkSpeed * global.dt;

	if (abs(v) > abs(h) + 0.1)
	{
		if (v > 0)
		{
			lastdirection = Direction.Down;
		}
		else if (v < 0)
		{
			lastdirection = Direction.Up;
		}
	}
	else
	{
		if (h > 0)
		{
			lastdirection = Direction.Right;
		}
		else if (h < 0)
		{
			lastdirection = Direction.Left;
		}
	}
	
	scr_setSpriteAction("walk",lastdirection,-1);
	if (!walking)
	{
		anim_index = 0;
	}
	anim_speed = 0.1;
	walking = true;
	if(room == roomChallenge)
	{
		global.walkFrames ++;
	}
	idle_time = 0;
	
	// if mobile and reaching the target, stop
	if (global.ismobile)
	{
		if (point_distance(x,y,movetarget_x,movetarget_y) <= clamp(currentspd / 2,1,999))
		{
			movetarget_x = -1;
			movetarget_y = -1;
			left = 0;
			right = 0;
			up = 0;
			down = 0;
			instance_destroy(obj_nav_target);
			if (autotarget != noone)
			{
				scr_chooseAutoActionObject(autotarget);
			}
			else if (autotarget_x > 0 && autotarget_y > 0)
			{
				scr_chooseAutoActionPosition(autotarget_x,autotarget_y);
			}
		}
	}
}
else
{
	idle_time += (1/60) * global.dt;
	walking = false;
	currentspd = 0;

	// If the pidgeon is around and the camera isn't pointing the player, look at it
	var cam = instance_find(obj_camera,0);
	if (instance_exists(obj_wildlife_mail_pidgeon) && cam.target != id)
	{
		var pidgeon = instance_find(obj_wildlife_mail_pidgeon,0);
		scr_setSpriteAction("walk",scr_directionFromPointAngle(x,y,pidgeon.x,pidgeon.y),-1);
		anim_speed = 0;
		anim_index = 1;
	}
	// For the first second staying still, use the 2nd frame of the walking animation
	else if (idle_time < 1 && !global.isSurvival)
	{
		scr_setSpriteAction("walk",lastdirection,-1);
		anim_speed = 0;
		anim_index = 1;
	}
	// Else switch to idle animation
	else
	{
		var newdir = lastdirection;
		switch (newdir)
		{
			case Direction.Down: newdir = Direction.Right; break;
			case Direction.Up: newdir = Direction.Left; break;
		}
		scr_setSpriteAction("idle",newdir,-1);
		anim_speed = 0.02;
		if(room != roomWild && global.online)
		{
			var sx = image_xscale*10;
			sx = round(sx);
			sx = sx/10.0;
			var sy = image_yscale*10;
			sy = round(sy);
			sy = sy/10.0;
			sendCharData(round(x),round(y), custom_body_sprite, custom_head_sprite, custom_lower_sprite, custom_upper_sprite, anim_index, sx, sy, socketid, global.username,stamina);
		}
	}
}
// Add walked distance for game stats. Note that a 16x16 px tile is 80x80cm
global.stat_walked += (point_distance(x,y,prevx,prevy) / 16) * 0.8;
//animate equipped object (originally this was at the end of this script)
with(instance_nearest(x,y,obj_equipped))
{
	scr_anim_idle(other,other.lastdirection);
}