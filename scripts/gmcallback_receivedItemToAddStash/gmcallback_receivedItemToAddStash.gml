///@description gmcallback_dropPickable
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var iid = string(ds_list_find_value(res,0));
var q = real(ds_list_find_value(res,1));
var d = real(ds_list_find_value(res,2));

ds_list_destroy(res);

var inst = scr_gameItemCreate(0,0,obj_pickable,iid,q,-1,-1,d);
scr_putItemIntoInventory(inst,InventoryType.Stash,-1);
instance_destroy(inst);