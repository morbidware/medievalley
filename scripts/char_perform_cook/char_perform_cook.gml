if (lastdirection == Direction.Up || lastdirection = Direction.Left)
{
	scr_setSpriteAction("crafting",Direction.Left,-1);
}
else
{
	scr_setSpriteAction("crafting",Direction.Right,-1);
}

crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	scr_setState(id,ActorState.Idle);
	
	// check if this item can be cooked, otherwise output some mush
	var cooked = noone;
	var newid = string_replace(consumableobject.itemid,"food_","food_cooked");
	if (ds_map_exists(global.assets, string("spr_pickable_"+newid)))
	{
		cooked = scr_gameItemCreate(0,0,obj_pickable,newid);
	}
	else
	{
		cooked = scr_gameItemCreate(0,0,obj_pickable,"food_cookedgoop");
	}
	scr_loseQuantity(consumableobject,1);
	scr_putItemIntoInventory(cooked,InventoryType.All,-1);
	consumableobject = noone;
	consumabletype = -1;
}
