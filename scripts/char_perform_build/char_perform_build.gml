//technically no animation here

// action
crono -= global.dt;
if(crono <= 0)
{
	logga("all work good");
	//decrease stamina
	scr_decreaseStamina(id, 1);
	scr_setState(id,ActorState.Idle);
	
	var farmGround = instance_find(obj_baseGround,0);
	
	var eq = instance_nearest(x,y,obj_equipped);
	var strippedid = string_replace(eq.itemid, "piece", "");
	var obj = scr_asset_get_index("obj_interactable_"+strippedid);
		
	if(obj > -1)
	{
		placeStructure(8+interactionCol*16, 8+interactionRow*16, obj, strippedid);
		var itm = scr_gameItemCreate(8+interactionCol*16, 8+interactionRow*16, obj, strippedid);
		ds_grid_set(farmGround.itemsgrid, interactionCol, interactionRow, itm);
	}
	with(obj_playerInventory)
	{
		scr_loseQuantity(ds_grid_get(inventory, currentSelectedItemIndex, 0),1);
	}
	with(obj_interactable_basefence)
	{
		event_perform(ev_other,ev_user3);	
	}
	if(global.isSurvival  && global.online)
	{
		scr_storeInteractablesSurvival();
	}
	else
	{
		scr_storeAllInsocket();
	}
}