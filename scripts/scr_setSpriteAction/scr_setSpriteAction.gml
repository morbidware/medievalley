///@function scr_setSpriteAction(action,lastdirection,animspeed)
///@param action
///@param lastdirection
///@param animspeed
switch(argument1)
{
	case Direction.Up:
		directionprefix = "up";
		image_xscale = 1;
		break;
	
	case Direction.Down:
		directionprefix = "down";
		image_xscale = 1;
		break;
	
	case Direction.Left:
		image_xscale = -1;
		directionprefix = "right"; // use mirrored right sprite
		break;
	
	case Direction.Right:
		image_xscale = 1;
		directionprefix = "right";
		break;
}

// First, set sprites
var strip = "_strip4";
if (string(argument0) == "gethit")
{
	strip = "";
}
var surv = "_survival";
if (!global.isSurvival)
{
	strip = "";
}
custom_body_sprite = scr_asset_get_index(string(custom_body)+"_body_"+string(argument0)+"_"+string(directionprefix)+strip+surv);
custom_head_sprite = scr_asset_get_index(string(custom_head)+"_head_"+string(argument0)+"_"+string(directionprefix)+strip+surv);
custom_upper_sprite = scr_asset_get_index(string(custom_upper)+"_upper_"+string(argument0)+"_"+string(directionprefix)+strip+surv);
custom_lower_sprite = scr_asset_get_index(string(custom_lower)+"_lower_"+string(argument0)+"_"+string(directionprefix)+strip+surv);

// If specified set speed based on frames
if (argument2 > 0)
{
	anim_speed = sprite_get_number(custom_body_sprite) / argument2;
}