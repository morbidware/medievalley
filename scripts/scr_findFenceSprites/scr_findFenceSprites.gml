///@function scr_findFenceSprites()
///@description this function changes the sprite of a fence based on nearby fence objects
var hastop = false;
var hasbottom = false;
var hasleft = false;
var hasright = false;

with (obj_interactable_basefencedoor) {
	
	//detect alignment
	if (col == other.col-1 && row == other.row)
	{
		hasleft = true;
		growstage = 1;
		continue;
	}
	if (col == other.col+1 && row == other.row)
	{
		hasright = true;
		growstage = 1;
		continue;
	}
	if (col == other.col && row == other.row-1)
	{
		hastop = true;
		growstage = 2;
		continue;
	}
	if (col == other.col && row == other.row+1)
	{
		hasbottom = true;
		growstage = 2;
		continue;
	}
	
}

with (obj_interactable_basefence) {
	
	//detect alignment
	if (col == other.col-1 && row == other.row)
	{
		hasleft = true;
		continue;
	}
	if (col == other.col+1 && row == other.row)
	{
		hasright = true;
		continue;
	}
	if (col == other.col && row == other.row-1)
	{
		hastop = true;
		continue;
	}
	if (col == other.col && row == other.row+1)
	{
		hasbottom = true;
		continue;
	}
	if (!hastop && hasbottom && !hasleft && hasright) growstage = 1;
	else if (!hastop && hasbottom && hasleft && !hasright) growstage = 2;
	else if (hastop && !hasbottom && !hasleft && hasright) growstage = 3;
	else if (hastop && !hasbottom && hasleft && !hasright) growstage = 4;
	else if (!hastop && !hasbottom && hasleft && hasright) growstage = 5;
	else if (hastop && hasbottom && !hasleft && !hasright) growstage = 6;
	else if (!hastop && !hasbottom && hasleft && !hasright) growstage = 7;
	else if (!hastop && !hasbottom && !hasleft && hasright) growstage = 8;
	else if (hastop && !hasbottom && !hasleft && !hasright) growstage = 9;
	else if (!hastop && hasbottom && !hasleft && !hasright) growstage = 10;
	else if (!hastop && !hasbottom && !hasleft && !hasright) growstage = 11;
	else if (hastop && hasbottom && hasleft && !hasright) growstage = 12;
	else if (hastop && !hasbottom && hasleft && hasright) growstage = 13;
	else if (hastop && hasbottom && !hasleft && hasright) growstage = 14;
	else if (!hastop && hasbottom && hasleft && hasright) growstage = 15;
	else if (hastop && hasbottom && hasleft && hasright) growstage = 16;
}



	


	
