/// @function draw_nine_slice(spr,x1,y1,x2,y2,scale)
/// @description Draws a nine-slice element using sprite frames from 0 to 8 for the nine graphic parts.
/// @param spr
/// @param x1
/// @param y1
/// @param x2
/// @param y2
/// @param scale
var spr, x1, y1, x2, y2, scale;
spr = argument0;
x1 = round(argument1);
y1 = round(argument2);
x2 = round(argument3);
y2 = round(argument4);
scale = argument5;

// Early get sprite size and clamp minimum nine-slice size
var w = sprite_get_width(spr) * scale;
var h = sprite_get_height(spr) * scale;
var size_w = clamp(x2-x1,w * 2,999999);
var size_h = clamp(y2-y1,h * 2,999999);

// Set sprite offset to 0,0
sprite_set_offset(spr,0,0);

// Scale values for central elements
var fill_scale_w = ((size_w - w - w) / w) * scale;
var fill_scale_h = ((size_h - h - h) / h) * scale;

// Top row
draw_sprite_ext(spr, 0, x1, y1, scale, scale, 0, draw_get_color(), draw_get_alpha());
draw_sprite_ext(spr, 1, x1 + w, y1, fill_scale_w, scale, 0, draw_get_color(), draw_get_alpha());
draw_sprite_ext(spr, 2, x2 - w, y1, scale, scale, 0, draw_get_color(), draw_get_alpha());

// Middle row
draw_sprite_ext(spr, 3, x1, y1 + h, scale, fill_scale_h, 0, draw_get_color(), draw_get_alpha());
draw_sprite_ext(spr, 4, x1 + w, y1 + h, fill_scale_w, fill_scale_h, 0, draw_get_color(), draw_get_alpha());
draw_sprite_ext(spr, 5, x2 - w, y1 + h, scale, fill_scale_h, 0, draw_get_color(), draw_get_alpha());

// Bottom row
draw_sprite_ext(spr, 6, x1, y2 - h, scale, scale, 0, draw_get_color(), draw_get_alpha());
draw_sprite_ext(spr, 7, x1 + w, y2 - h, fill_scale_w, scale, 0, draw_get_color(), draw_get_alpha());
draw_sprite_ext(spr, 8, x2 - w, y2 - h, scale, scale, 0, draw_get_color(), draw_get_alpha());
