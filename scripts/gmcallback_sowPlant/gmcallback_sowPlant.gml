///@description gmcallback_sowPlant
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var col = real(ds_list_find_value(res,0));
var row = real(ds_list_find_value(res,1));
var obj = real(ds_list_find_value(res,2));
var str = string(ds_list_find_value(res,3));

ds_list_destroy(res);

logga("col " + string(col));
logga("row " + string(row));
logga("obj " + string(obj));
logga("str " + string(str));

var farmGround = instance_find(obj_baseGround,0);
var itm = scr_gameItemCreate(8+col*16, 8+row*16, obj, str);
itm.growstage = 1;
if (itm.needswatering == 1)
{
	itm.growcrono = 0;	
}
else
{
	itm.growcrono = itm.growtimer;
}
ds_grid_set(farmGround.itemsgrid, col, row, itm);