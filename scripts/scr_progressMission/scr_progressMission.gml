///@function scr_progressMission(newId);
///@param newId

with (obj_mission)
{
	// add specified item to map of done progress
	if (ds_map_exists(required, argument0))
	{
		if (ds_map_exists(done, argument0))
		{
			var v = ds_map_find_value(done, argument0);
			ds_map_replace(done, argument0, v+1);
			scr_storeMissions();
			if (ds_map_find_value(done,argument0) == ds_map_find_value(required,argument0))
			{
				with(obj_btn_generic)
				{
				    if(sprite_index == spr_dock_btn_diary)
				    {
				        instance_create_depth(x+14,y+14,depth-1,obj_fx_ui_circle);
				    }
				}
			}
		}
		else
		{
			ds_map_add(done, argument0, 1);
			scr_storeMissions();
			if (ds_map_find_value(done,argument0) == ds_map_find_value(required,argument0))
			{
				with(obj_btn_generic)
				{
				    if(sprite_index == spr_dock_btn_diary)
				    {
				        instance_create_depth(x+14,y+14,depth-1,obj_fx_ui_circle);
				    }
				}
			}
		}
	}
	
	// Check mission completion
	event_perform(ev_other, ev_user1);
}

// Refresh missions summary
with (obj_summary_manager)
{
	event_perform(ev_other,ev_user0);
}