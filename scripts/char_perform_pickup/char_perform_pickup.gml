crono -= global.dt;

if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	
	// if targetobject is changed and no longer a pickable, stop here
    if (targetobject.object_index != obj_pickable)
    {
        exit;
    }
	
	// action
	if(global.otheruserid != 0)
	{
		//sendItemToUserStash(global.otherusername, targetobject.itemid, targetobject.quantity, targetobject.durability);
	}
	else
	{
		scr_putItemIntoInventory(targetobject,InventoryType.Inventory,-1);
	}
	if (global.online)
	{
		removePickable(targetobject.x,targetobject.y,targetobject.object_index);
	}
	instance_destroy(targetobject);
	
	if(global.isSurvival && global.online)
	{
		scr_storePickablesSurvival();
	}
	{
		scr_storeAllInsocket();
	}
}