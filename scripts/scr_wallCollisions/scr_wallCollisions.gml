///@function scr_wallCollisions(id,xoffset,yoffset)
///@param id
///@param xoffset
///@param yoffset
var p = argument0;
var xoff = argument1;
var yoff = argument2;
if (!instance_exists(obj_baseGround) || p == noone || p == undefined)
{
	exit;
}

var walls = ds_list_create();

var pcol = (p.x+xoff)/16;
var prow = (p.y+yoff)/16;

var baseground = instance_find(obj_baseGround,0);

var topleft = ds_grid_get(baseground.wallsgrid, clamp(pcol-1,0,baseground.cols-1), clamp(prow-1,0,baseground.rows-1));
if(topleft > 0)
{
	ds_list_add(walls,topleft);
}
var topcenter = ds_grid_get(baseground.wallsgrid, clamp(pcol,0,baseground.cols-1), clamp(prow-1,0,baseground.rows-1));
if(topcenter > 0)
{
	ds_list_add(walls,topcenter);
}
var topright = ds_grid_get(baseground.wallsgrid, clamp(pcol+1,0,baseground.cols-1), clamp(prow-1,0,baseground.rows-1));
if(topright > 0)
{
	ds_list_add(walls,topright);
}
var midleft = ds_grid_get(baseground.wallsgrid, clamp(pcol-1,0,baseground.cols-1), clamp(prow,0,baseground.rows-1));
if(midleft > 0)
{
	ds_list_add(walls,midleft);
}
var midcenter = ds_grid_get(baseground.wallsgrid, clamp(pcol,0,baseground.cols-1), clamp(prow,0,baseground.rows-1));
if(midcenter > 0)
{
	ds_list_add(walls,midcenter);
}
var midright = ds_grid_get(baseground.wallsgrid, clamp(pcol+1,0,baseground.cols-1), clamp(prow,0,baseground.rows-1));
if(midright > 0)
{
	ds_list_add(walls,midright);
}

var bottomleft = ds_grid_get(baseground.wallsgrid, clamp(pcol-1,0,baseground.cols-1), clamp(prow+1,0,baseground.rows-1));
if(bottomleft > 0)
{
	ds_list_add(walls,bottomleft);
}
var bottomcenter = ds_grid_get(baseground.wallsgrid, clamp(pcol,0,baseground.cols-1), clamp(prow+1,0,baseground.rows-1));
if(bottomcenter > 0)
{
	ds_list_add(walls,bottomcenter);
}
var bottomright = ds_grid_get(baseground.wallsgrid, clamp(pcol+1,0,baseground.cols-1), clamp(prow+1,0,baseground.rows-1));
if(bottomright > 0)
{
	ds_list_add(walls,bottomright);
}

//show_debug_message("checking against " + string(ds_list_size(walls)) + " walls");

for(var i = 0; i < ds_list_size(walls); i++)
{
	var w = ds_list_find_value(walls,i);
	
	// Collision mask
	var collide = false;
	if (p.onstairs && w.storey_stairs_out) collide = true;
	if (!p.onstairs && w.storey_stairs_in) collide = true;
	if (p.storey == 0 && w.storey_0 == true) collide = true;
	if (p.storey == 1 && w.storey_1 == true) collide = true;
	if (p.storey == 2 && w.storey_2 == true) collide = true;
	if (p.storey == 3 && w.storey_3 == true) collide = true;
	if (!collide) continue;
	
	var charx = p.x+xoff;
	var chary = p.y+yoff;
	var wallx = w.x;
	var wally = w.y;
	
	var offsetx = wallx-charx;
	var offsety = wally-chary;
	
	// If entity is inside the wall size...
	if(abs(offsetx) < 16 && abs(offsety) < 16)
	{
		// Diagonal collision
		// ----------------- TOP LEFT
		if (w.diagonalTopLeft && charx > wallx-8 && chary > wally-8)
		{
			if (charx-16 < wallx+offsety && chary-16 < wally+offsetx)
			{
				p.x = wallx+offsety+16;
				p.y = wally+offsetx+16;
			}
		}
		
		// ----------------- TOP RIGHT
		else if (w.diagonalTopRight && charx < wallx+8 && chary > wally-8)
		{
			if (charx+16 > wallx-offsety && chary-16 < wally-offsetx)
			{
				p.x = wallx-offsety-16;
				p.y = wally-offsetx+16;
			}
		}
		
		// ----------------- BOTTOM LEFT
		else if (w.diagonalBottomLeft && charx > wallx-8 && chary < wally+8)
		{	
			if (charx-16 < wallx-offsety && chary+16 > wally+offsetx)
			{
				p.x = wallx-offsety+16;
				p.y = wally-offsetx-16;
			}
		}

		// ----------------- BOTTOM RIGHT
		else if (w.diagonalBottomRight && charx < wallx+8 && chary < wally+8)
		{
			if (charx+16 > wallx+offsety && chary+16 > wally+offsetx)
			{
				p.x = wallx+offsety-16;
				p.y = wally+offsetx-16;
			}
		}
		
		
		// ----------------- HORIZONTAL
		else if(abs(offsetx)<abs(offsety) )
		{
			if(chary < wally)
			{
				p.y = wally-16-yoff;
			}
			else
			{
				p.y = wally+16-yoff;			
			}
		}
		
		
		// ----------------- VERTICAL
		else if(abs(offsety)<abs(offsetx) )
		{
			if(charx < wallx)
			{
				p.x = wallx-16-xoff;
			}
			else
			{
				p.x = wallx+16-xoff;
			}
		}
		
		// ----------------- I DON'T REMEMBER?
		else
		{
			if(w.hasTop && chary < wally)
			{
				p.y = wally-16-yoff;
			}
			if(w.hasBottom && chary > wally)
			{
				p.y = wally+16-yoff;
			}
			if(w.hasLeft && charx < wallx)
			{
				p.x = wallx-16;
			}
			if(w.hasRight && charx > wallx)
			{
				p.x = wallx+16;
			}
		}
	}
}
ds_list_destroy(walls);