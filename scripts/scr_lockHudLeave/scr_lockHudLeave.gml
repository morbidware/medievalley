///@function scr_lockHudLeave(id)
///@description Removes the caller from the list of participants that need the HUD to be locked
///@param id

var index = ds_list_find_index(global.lockHUDparticipants,argument0);
if (index > -1)
{
	ds_list_delete(global.lockHUDparticipants,index)
}