///@function scr_canAddToInventory(object)
///@param object the id of the instance
///@description This script checks if it's possible to add a certain item in the inventory
var object = argument0;
var inv = instance_find(obj_playerInventory,0);

// ------------------- INVENTORY CHECK -------------------

// find first empty slot
var emptySlotIndex = -1;
for(var i = 0; i < global.inv_inventorylimit; i++)
{
	var slotValue = ds_grid_get(inv.inventory,i,0);
	if(slotValue == -1)
	{
		emptySlotIndex = i;
		break;
	}
}

// Find identical item in the inventory, if any
var sameObject = noone;
for(var i = 0; i < global.inv_inventorylimit; i++)
{
	var slotValue = ds_grid_get(inv.inventory,i,0);
	if(slotValue > -1)
	{	
		if(slotValue.itemid == object.itemid && slotValue.quantity + object.quantity < slotValue.maxquantity)
		{
			sameObject = slotValue;
			break;
		}
	}
}

// if there's an identical object and can add, return true
if(sameObject != noone)
{
	return true;
}

// if there's an empty slot, return true
else if(emptySlotIndex >= 0)
{
	return true;
}

if(global.isSurvival)
{
	return false;	
}
// ------------------- BACKPACK CHECK -------------------

// if still not done, try the same checks with the backpack

// find first empty slot
emptySlotIndex = -1;
for(var i = 10; i < global.inv_backpacklimit; i++)
{
	var slotValue = ds_grid_get(inv.inventory,i,0);
	if(slotValue == -1)
	{
		emptySlotIndex = i;
		break;
	}
}

// Find identical item in the inventory, if any
sameObject = noone;
for(var i = 10; i < global.inv_backpacklimit; i++)
{
	var slotValue = ds_grid_get(inv.inventory,i,0);
	if(slotValue > -1)
	{	
		if(slotValue.itemid == object.itemid && slotValue.quantity + object.quantity < slotValue.maxquantity)
		{
			sameObject = slotValue;
			break;
		}
	}
}

// if there's an identical object and can add, return true
if(sameObject != noone)
{
	return true;
}

// if there's an empty slot, return true
else if(emptySlotIndex >= 0)
{
	return true;
}

// if nothing worked, just return false
return false;