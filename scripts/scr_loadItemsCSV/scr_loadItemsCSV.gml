///@function scr_loadItemsCSV()
///@description used to load all items Models from a CSV

global.streol = 2;
if(os_type == os_macosx && os_browser == browser_not_a_browser)
{
	global.streol = 1;
}

//open file for reading
var filecsv = file_text_open_read("items.csv");
//create a list to store columns names
var cols = ds_list_create();
//get the line of the csv 
var line = file_text_readln(filecsv);
show_debug_message(line);
var delimiter = ";";
//now cycle the string and split by delimiter to fill cols list
while(string_length(line) > 0)
{
	var delimiterPos = string_pos(delimiter,line);
	if(delimiterPos == 0)
	{
		delimiterPos = string_length(line);
		ds_list_add(cols, string_copy(line,1,delimiterPos-global.streol));
		//show_debug_message("copy: " + string_copy(line,1,delimiterPos-global.streol));
	}
	else
	{
		ds_list_add(cols, string_copy(line,1,delimiterPos-1));
		//show_debug_message("copy: " + string_copy(line,1,delimiterPos-1));
	}
	line = string_copy(line,delimiterPos+1, string_length(line));
	//show_debug_message("remainder: " + string_copy(line,1,delimiterPos));
}
//now create a map for the json
var jsonmap = ds_map_create();
//now for each remaining line in the csv
while(!file_text_eof(filecsv))
{
	//get the line
	var itemline = file_text_readln(filecsv);
	//show_debug_message(itemline);
	//create a map to save all line values
	var itemmap = ds_map_create();
	//cursor reads from cols list values for the keys
	var cursor = 0;
	//skip lines too short or containing a comment
	show_debug_message(">>>>>>>> "+itemline);
	if (string_length(itemline) < 10 || string_pos("//",itemline) > 0)
	{
		show_debug_message("...skip");
		continue;	
	}
	
	while(string_length(itemline) > 0)
	{
		var itemDelimiterPos = string_pos(delimiter,itemline);
		var valuetoinsert;
		if(itemDelimiterPos == 0)
		{
			itemDelimiterPos = string_length(itemline);
			valuetoinsert = string_copy(itemline,1,itemDelimiterPos-global.streol);
		}
		else
		{
			valuetoinsert = string_copy(itemline,1,itemDelimiterPos-1);
		}
		
		if(is_real(valuetoinsert))
		{
			valuetoinsert = floor(valuetoinsert);
		}
		//show_debug_message(ds_list_find_value(cols,cursor) + ":" + string(valuetoinsert));
		ds_map_add(itemmap, ds_list_find_value(cols,cursor), valuetoinsert);
		itemline = string_copy(itemline,itemDelimiterPos+1, string_length(itemline));
		cursor ++;
	}
	ds_map_add(jsonmap, ds_map_find_value(itemmap,"itemid"), itemmap);
	//ds_map_destroy(itemmap);
}

global.itemsModels = ds_map_create();
ds_map_copy(global.itemsModels, jsonmap);
//ds_map_destroy(cols);//DIEGO
//ds_map_destroy(jsonmap);//DIEGO
file_text_close(filecsv);

//show_debug_message("first:" + ds_map_find_first(jsonmap));
//show_debug_message("second:" + ds_map_find_last(jsonmap));