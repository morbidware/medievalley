///@function scr_lockHudRequest(id)
///@description Adds the caller to the list of participants that need the HUD to be locked
///@param id
if (ds_list_find_index(global.lockHUDparticipants,argument0) < 0)
{
	ds_list_add(global.lockHUDparticipants,argument0);
}