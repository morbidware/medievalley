// tool animation
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.chopTimer;
	if (other.stamina <= 0)
	{
		t *= 2;
	}
	scr_anim_hammer(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	
	// execute action if conditions are met
	if (targetobject != noone)
	{
		if (event_perform_ret(targetobject, ev_other, ev_user1) == true && targetobject.interactionPrefixRight == "Chop")
		{
			if (global.online)
			{
				eventPerformInteractable(targetobject.x,targetobject.y,targetobject,targetobject.object_index,ev_other,ev_user2);
			}
			
			with (targetobject)
			{
				event_perform(ev_other,ev_user2);
			}
			if(global.othersocketid == "")
			{
				scr_loseDurability(id,true);
			}
		}
	}
	
	if(!global.isSurvival)scr_storeAllInsocket();
}