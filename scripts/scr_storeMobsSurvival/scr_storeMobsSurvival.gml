//ds_list_clear(global.interactables);
if (!scr_allowedToSave()) { return; }
logga("scr_storeMobsSurvival");
global.uisavestatus.saving = true;
var mobslist = ds_list_create();
instance_activate_object(obj_enemy);
for(var i = 0; i < instance_number(obj_enemy); i++)
{
	var p = instance_find(obj_enemy,i);
	/*
	diagonalTopLeft = false;
	diagonalTopRight = false;
	diagonalBottomLeft = false;
	diagonalBottomRight = false;

	storey_0 = false;
	storey_1 = false;
	storey_2 = false;
	storey_3 = false;
	storey_stairs_in = false;
	storey_stairs_out = false;
	blocking = true;
	*/
	if(p != obj_nightwolf)
	{
		var storestr = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+string(p.life);
		ds_list_add(mobslist,storestr);
	}
	
}

var uncompressed_string = ds_list_write(mobslist);

if (global.online)
{
	if(global.mobsstring != uncompressed_string)
	{
		logga("WILL UPDATE MOBS ON SERVER");
		global.mobsstring = uncompressed_string;
		sendMobsStringSurvival(global.landid, uncompressed_string, global.username);
	}
	gmcallback_updateLastSave();
}
else
{
	global.interactables = uncompressed_string;
	gmcallback_updateLastSave();
}
ds_list_destroy(mobslist);

global.uisavestatus.saving = false;