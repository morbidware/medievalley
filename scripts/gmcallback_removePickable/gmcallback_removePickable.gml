///@description gmcallback_receivedCharData
/*
with(argument0)
{
	instance_destroy();
}
*/
logga("?????????????????????????????? gmcallback_removePickable");
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = real(ds_list_find_value(res,0));
var yy = real(ds_list_find_value(res,1));
var oid = real(ds_list_find_value(res,2));

ds_list_destroy(res);

with(instance_nearest(xx,yy,oid))
{
	logga("destroy INST");
	instance_destroy();
}
logga("?????????????????????????????? gmcallback_removePickable END");