var str = argument0; 
logga(str);
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
	pos -= 1;
	ds_list_add(res, string_copy(str, 1, pos));
	str = string_delete(str, 1, pos + dellen);
	pos = string_pos(del, str);
}
ds_list_add(res, str);

global.gold = real(ds_list_find_value(res,2));
logga("gmcallback_gotUserGold: global.gold " + string(global.gold));
if (global.isChallenge)
{
	if(ds_list_size(res) > 11)
	{
		logga("gmcallback_gotUserGold: will parse challenge data now");
		global.lastChallengeDone = round(real(ds_list_find_value(res,11)));
		getChallengeData(global.lastChallengeDone);
	}
	else 
	{
		logga("gmcallback_gotUserGold: cannot parse challenge data now");
		global.lastChallengeDone = 0;
		getChallengeData(global.lastChallengeDone);
	}
}
ds_list_destroy(res);