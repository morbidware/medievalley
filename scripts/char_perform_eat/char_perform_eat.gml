// animation
var r = random_range(-0.1,0.1);
image_xscale = 1-r;
image_yscale = 1+r;

//action
crono --;
if(crono <= 0)
{
	// stop animation
	image_xscale = 1;
	image_yscale = 1;
	scr_setState(id, ActorState.Idle);
	
	// execute action if condition is met
	with(consumableobject)
	{
		event_perform(ev_other,ev_user0);
	}
	scr_loseQuantity(consumableobject,1);
	consumableobject = noone;
	consumabletype = -1;
	global.saveuserdatatimer = 0;
}