///@function scr_drawChar()
///@description please place this inside the char draw event!

shader_set(shaderCharacter);

// skin
shader_set_uniform_f(s_skin1_r,0.5);	shader_set_uniform_f(s_skin1_g,0);		shader_set_uniform_f(s_skin1_b,0);
shader_set_uniform_f(s_skin2_r,0.7);	shader_set_uniform_f(s_skin2_g,0);		shader_set_uniform_f(s_skin2_b,0);
shader_set_uniform_f(s_skin3_r,1);		shader_set_uniform_f(s_skin3_g,0);		shader_set_uniform_f(s_skin3_b,0);

// eye
shader_set_uniform_f(s_eye_r,0.5);	shader_set_uniform_f(s_eye_g,0.5);	shader_set_uniform_f(s_eye_b,1);

// hair
shader_set_uniform_f(s_hair1_r,0.5);	shader_set_uniform_f(s_hair1_g,0.5);	shader_set_uniform_f(s_hair1_b,0.4);
shader_set_uniform_f(s_hair2_r,0.7);	shader_set_uniform_f(s_hair2_g,0.7);	shader_set_uniform_f(s_hair2_b,0.6);
shader_set_uniform_f(s_hair3_r,1);	shader_set_uniform_f(s_hair3_g,1);	shader_set_uniform_f(s_hair3_b,0.9);

// sleeve
shader_set_uniform_f(s_sleeve1_r,0);	shader_set_uniform_f(s_sleeve1_g,0.5);	shader_set_uniform_f(s_sleeve1_b,0);
shader_set_uniform_f(s_sleeve2_r,0);	shader_set_uniform_f(s_sleeve2_g,0.7);	shader_set_uniform_f(s_sleeve2_b,0);
shader_set_uniform_f(s_sleeve3_r,0);	shader_set_uniform_f(s_sleeve3_g,1);	shader_set_uniform_f(s_sleeve3_b,0);

// pants
shader_set_uniform_f(s_pants1_r,0.5);	shader_set_uniform_f(s_pants1_g,0.5);		shader_set_uniform_f(s_pants1_b,0);
shader_set_uniform_f(s_pants2_r,0.7);	shader_set_uniform_f(s_pants2_g,0.7);		shader_set_uniform_f(s_pants2_b,0);
shader_set_uniform_f(s_pants3_r,1);	shader_set_uniform_f(s_pants3_g,1);		shader_set_uniform_f(s_pants3_b,0);

// shoes
shader_set_uniform_f(s_shoes1_r,0.1);	shader_set_uniform_f(s_shoes1_g,0);		shader_set_uniform_f(s_shoes1_b,0.5);
shader_set_uniform_f(s_shoes2_r,0.3);	shader_set_uniform_f(s_shoes2_g,0);		shader_set_uniform_f(s_shoes2_b,0.7);
shader_set_uniform_f(s_shoes3_r,0.5);	shader_set_uniform_f(s_shoes3_g,0);		shader_set_uniform_f(s_shoes3_b,1);

/*
draw_sprite(spr_char_head, 0, x + 16, y);
draw_sprite(spr_char_hair, 0, x + 16, y);
draw_sprite(spr_char_sleeve, 0, x + 16, y);
draw_sprite(spr_char_pants, 0, x + 16, y);
draw_sprite(spr_char_shoes, 0, x + 16, y);
*/

shader_reset();

//draw_sprite(spr_char_torso, 0, x + 16, y);