//ds_list_clear(global.interactables);
if(global.isSurvival)
{
	exit;
}
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
var missionslist = ds_list_create();
for(var i = 0; i < instance_number(obj_mission); i++)
{
	var m = instance_find(obj_mission,i);
	
	// If a mission is completed but NOT on hold, don't save it
	if (m.completion == 1 && !m.completeOnHold)
	{
		continue;
	}
	
	//logga("save mission " + m.missionid + ":" + ds_map_write(m.done));
	var storestr = m.missionid+":"+ds_map_write(m.done);
	ds_list_add(missionslist,storestr);
}

if (global.online)
{
	var missionsstring = ds_list_write(missionslist);
	if(missionsstring != global.lastSavedMissions)
	{
		global.lastSavedMissions = missionsstring;
		sendMissionsString(global.userid, missionsstring, global.username);
	}
	else
	{
		logga("skipping store missions");
	}
	gmcallback_updateLastSave();
}

ds_list_destroy(missionslist);
global.uisavestatus.saving = false;