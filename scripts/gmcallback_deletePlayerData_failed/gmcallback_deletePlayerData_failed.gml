///@description gmcallback_dropPickable
with(obj_farmboard_panel)
{
	fetching = false;
}
with(instance_create_depth(0,0,0,obj_popup))
{
	title = "WHOOPS";
	body = "There was a problem while connecting to your account.";
	type = 1;
	btnText = "CLOSE";
	event_perform(ev_other,ev_user15);
}