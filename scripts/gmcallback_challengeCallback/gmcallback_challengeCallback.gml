///@function gmcallback_challengeCallback(str)
///@param str
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var status = ds_list_find_value(res,0);
var text = ds_list_find_value(res,1);

var is_galacredit_winner = ds_list_find_value(res,2);
var is_new_user = ds_list_find_value(res,3);
var earned_amount = ds_list_find_value(res,4);

logga("status:"+status);
logga("text:"+text);
logga("is_galacredit_winner:"+is_galacredit_winner);
logga("is_new_user:"+is_new_user);
logga("earned_amount:"+earned_amount);

var letter = instance_find(obj_letter,0);
if(status == "ok")
{
	if(text == "success")
	{
		logga("infamous if");
		if(is_galacredit_winner == "" && is_new_user == "" && earned_amount == "")
		{
			//WON KEY
			logga("-- won key --");
			letter.title = "The Challenge is over!";
			letter.body = "Astonishing! You won a Steam Key!\nGo to your profile and discover which game it is!\nYour Key will be in your profile under:\nBUNDLES LIBRARY->Indiegala Giveaway->Gameplay Giveaway";
			letter.signature = "King Thistle";
			letter.assignedGold = 0;
			letter.hasCup = true;
			letter.willRedirect = true;
			letter.redirectURL = "https://www.indiegala.com/profile";
				
			letter.ctaText = "Go to your Profile";
			letter.ctaURL = letter.redirectURL;
					
			letter.ctaText2 = "Play Again";
			letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
		}
		else
		{
			logga("infamous else");
			if(real(earned_amount) == 0)
			{
				//WON NOTHING
				logga("-- won nothing --");
				letter.title = "The Challenge is over!";
				letter.body = "Unfortunately you won nothing. Better luck next time!";
				letter.signature = "King Thistle";
				letter.assignedGold = 0;
				letter.hasCup = false;
				letter.willRedirect = true;
				letter.redirectURL = "https://feudalife.indiegala.com/challenges";
				
				letter.ctaText = "Play Again";
				letter.ctaURL = letter.redirectURL;
					
				letter.ctaText2 = "Back to Indiegala";
				letter.ctaURL2 = "https://www.indiegala.com";
			}
			else
			{
				logga("if(real(is_galacredit_winner))");	
				if(bool(is_galacredit_winner))
				{
					//WON GALACREDIT
					logga("-- won galacredit --");
					global.willEarnGalaCreds = 1;
					letter.title = "The Challenge is over!";
					letter.body = "Congratulations! You won some GalaCredits!";
					letter.signature = "King Thistle";
					letter.assignedGold = earned_amount;
					letter.hasCup = false;
					letter.willRedirect = true;
					letter.redirectURL = "https://www.indiegala.com/profile";
				
					letter.ctaText = "Go to your Profile";
					letter.ctaURL = letter.redirectURL;
					
					letter.ctaText2 = "Play Again";
					letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
				}
				else
				{
					//WON GOLD
					logga("-- won gold --");
					global.willEarnGalaCreds = 0;
					var char = instance_find(obj_char,0);
					char.gold += real(earned_amount);
					scr_storeUserdata();
					
					letter.title = "The Challenge is over!";
					letter.body = "Congratulations! You won some gold to spend in Feudalife!";
					letter.signature = "King Thistle";
					letter.assignedGold = earned_amount;
					letter.hasCup = false;
					letter.willRedirect = true;
					letter.redirectURL = "https://feudalife.indiegala.com/";
					
					letter.ctaText = "Play Feudalife";
					letter.ctaURL = letter.redirectURL;
					
					letter.ctaText2 = "Play Again";
					letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
				}
				logga("none of the above");
			}
		}
	}
	else
	{
		if(text == "inconsistent data")
		{
			//CHEATER
			logga("-- cheater --");
			letter.title = "Inconsitent Data Detected!";
			letter.body = "There was some inconsistency while processing your game data.";
			letter.signature = "King Thistle";
			letter.assignedGold = 0;
			letter.hasCup = false;
			letter.willRedirect = true;
			letter.redirectURL = "https://www.indiegala.com/profile";
					
			letter.ctaText = "Go to your Profile";
			letter.ctaURL = letter.redirectURL;
					
			letter.ctaText2 = "";
			letter.ctaURL2 = "";
		}
		else
		{
			//THERE WAS SOME PROBLEM
			logga("-- problem --");
			letter.title = "Warning!";
			letter.body = "There was an error processing your request.\nPlease check your internet connection and contact support if you experience this issue.";
			letter.signature = "King Thistle";
			letter.assignedGold = 0;
			letter.hasCup = false;
			letter.willRedirect = true;
			letter.redirectURL = "https://feudalife.indiegala.com/";
					
			letter.ctaText = "Play Feudalife";
			letter.ctaURL = letter.redirectURL;
					
			letter.ctaText2 = "Play Again";
			letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
		}
	}
}
else
{
	if(text == "inconsistent data")
	{			
		//CHEATER
		logga("-- cheater --");
		letter.title = "Inconsitent Data Detected!";
		letter.body = "There was some inconsistency while processing your game data.";
		letter.signature = "King Thistle";
		letter.assignedGold = 0;
		letter.hasCup = false;
		letter.willRedirect = true;
		letter.redirectURL = "https://www.indiegala.com/profile";
					
		letter.ctaText = "Go to your Profile";
		letter.ctaURL = letter.redirectURL;
					
		letter.ctaText2 = "";
		letter.ctaURL2 = "";
	}
	else
	{
		//THERE WAS SOME PROBLEM
		logga("-- problem --");
		letter.title = "Warning!";
		letter.body = "There was an error processing your request.\nPlease check your internet connection and contact support if you experience this issue.";
		letter.signature = "King Thistle";
		letter.assignedGold = 0;
		letter.hasCup = false;
		letter.willRedirect = true;
		letter.redirectURL = "https://feudalife.indiegala.com/";
					
		letter.ctaText = "Play Feudalife";
		letter.ctaURL = letter.redirectURL;
					
		letter.ctaText2 = "Play Again";
		letter.ctaURL2 = "https://feudalife.indiegala.com/challenges";
	}
}
letter.hold = false;