///@description gmcallback_waterTile
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var col = real(ds_list_find_value(res,0));
var row = real(ds_list_find_value(res,1));

ds_list_destroy(res);

logga("col " + string(col));
logga("row " + string(row));

var farmGround = instance_find(obj_baseGround,0);
var itemontile = ds_grid_get(farmGround.itemsgrid, col, row);
if (itemontile > 0)
{
	var s = audio_play_sound(snd_watering,0,false);
	audio_sound_pitch(s, 0.85 + random (0.35));
	with(itemontile)
	{
		if(growcrono == 0 && growstage < growstages && needswatering == 1)
		{
			growcrono = growtimer;
		}
	}
}