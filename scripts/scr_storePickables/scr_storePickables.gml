//ds_list_clear(global.pickables);
if(global.isSurvival)
{
	exit;
}
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
with(obj_debugger)
{
	str += "scr_storePickables\n";
}
instance_activate_object(obj_pickable);
var pickableslist = ds_list_create();
for(var i = 0; i < instance_number(obj_pickable); i++)
{
	var p = instance_find(obj_pickable,i);
	if(p.destroy == false)
	{
		var storestr = string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.quantity)+":"+string(p.durability);
		ds_list_add(pickableslist,storestr);
	}
	else
	{
		var storestr = "destroy == true -> "+string(p.x)+":"+string(p.y)+":"+p.itemid+":"+string(p.quantity)+":"+string(p.durability);
	}
}

var uncompressed_string = ds_list_write(pickableslist);
var compressed_string = uncompressed_string;
compressed_string = string_replace_all(compressed_string,"000000","_");
compressed_string = string_replace_all(compressed_string,"A666C696E743A313A3001","%");

if (global.online)
{
	// If visiting a farm and owner is not here, save it for him
	if(global.otheruserid != 0 && !instance_exists(obj_dummy))
	{
		if(global.otherpickables != uncompressed_string)
		{
			logga("WILL UPDATE OTHER PICKABLES ON SERVER");
			global.otherpickables = ds_list_write(pickableslist);
			sendPickablesString(global.otheruserid, compressed_string, global.otherusername);
		}
	}
	else
	{
		if(global.pickables != uncompressed_string)
		{
			logga("WILL UPDATE PICKABLES ON SERVER");
			global.pickables = ds_list_write(pickableslist);
			sendPickablesString(global.userid, compressed_string, global.username);
		}
		gmcallback_updateLastSave();
	}
}
else
{
	global.pickables = ds_list_write(pickableslist);
	//sendPickablesString(global.userid, ds_list_write(pickableslist), global.username);
	gmcallback_updateLastSave();
}
ds_list_destroy(pickableslist);
global.uisavestatus.saving = false;