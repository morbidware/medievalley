///@function scr_chooseAutoActionObject(target)
///@param target
var target = argument0;
var action = false;
var valid = false;
var count = 0;
var type = object_get_parent(target.object_index);
repeat(11)
{
	switch (count)
	{
		case 0: handState = HandState.Axe; break;
		case 1: handState = HandState.Hammer; break;
		case 2: handState = HandState.Hoe; break;
		case 3: handState = HandState.Mine; break;
		case 4: handState = HandState.Melee; break;
		case 5: handState = HandState.Shovel; break;
		case 6: handState = HandState.Sickle; break;
		case 7: handState = HandState.Sow; break;
		case 8: handState = HandState.Watering; break;
		case 9: handState = HandState.Build; break;
		case 10: handState = HandState.Empty; break;
	}
	
	// Handle all conditions that can validate the action
	if (type == obj_interactable)
	{
		action = event_perform_ret(target,ev_other,ev_user1);
	}
	if (action == true)
	{
		break;
	}
	else
	{
		switch (type)
		{
			case obj_interactable: if (handState == HandState.Watering) {valid = true;} break;
			case obj_enemy: if (handState == HandState.Melee) {valid = true;} break;
		}
		if (valid == true)
		{
			break;
		}
	}
	count++;
}

targetobject = target;
highlightedobject = target;
var inv = instance_find(obj_playerInventory,0);
lastdirection = scr_directionFromPointAngle(x,y,target.x,target.y);
switch(lastdirection)
{
	case Direction.UpLeft: lastdirection = Direction.Left; break;
	case Direction.UpRight: lastdirection = Direction.Right; break;
	case Direction.DownLeft: lastdirection = Direction.Left; break;
	case Direction.DownRight: lastdirection = Direction.Right; break;
}
interactionCol = floor(targetobject.x / 16);
interactionRow = floor(targetobject.y / 16);
highlightCol = interactionCol;
highlightRow = interactionRow;

// ---------------- GENERIC ACTIONS NOT RETURNED BY OBJECTS
if (action == false)
{
	if (type == obj_interactable)
	{
		if (target.needswatering == true)
		{
			var i = 0;
			repeat(global.inv_inventorylimit)
			{
				var item = ds_grid_get(inv.inventory,i,0);
				if (item > 0)
				{
					if (item.itemid == "wateringcan")
					{
						inv.currentSelectedItemIndex = i;
						with (obj_playerInventory)
						{
							event_perform(ev_other,ev_user0);
						}
						scr_setState(id,ActorState.Watering);
						break;
					}
				}
				i++;
			}
		}
	}
	else if (type == obj_enemy)
	{
		var i = 0;
		repeat(global.inv_inventorylimit)
		{
			var item = ds_grid_get(inv.inventory,i,0);
			if (item > 0)
			{
				if (item.itemid == "sword" || item.itemid == "spear")
				{
					inv.currentSelectedItemIndex = i;
					with (obj_playerInventory)
					{
						event_perform(ev_other,ev_user0);
					}
					scr_setState(id,ActorState.Charge);
					break;
				}
			}
			i++;
		}
	}
}
else
// ---------------- ACTIONS RETURNED BY OBJECTS
{
	if (target.interactionPrefixLeft == "Enter" && global.otheruserid == 0)
	{
		with(obj_gameLevel)
		{
			if (!changingRoom)
			{
				changingRoom = true;
				event_perform(ev_other,ev_user3);
			}
		}
	}
	else if (target.interactionPrefixRight == "Mow")
	{
		var i = 0;
		repeat(global.inv_inventorylimit)
		{
			var item = ds_grid_get(inv.inventory,i,0);
			if (item > 0)
			{
				if (item.itemid == "sickle")
				{
					inv.currentSelectedItemIndex = i;
					with (obj_playerInventory)
					{
						event_perform(ev_other,ev_user0);
					}
					scr_setState(id,ActorState.Sickle);
					break;
				}
			}
			i++;
		}
	}
	else if (target.interactionPrefixRight == "Chop")
	{
		var i = 0;
		repeat(global.inv_inventorylimit)
		{
			var item = ds_grid_get(inv.inventory,i,0);
			if (item > 0)
			{
				if (item.itemid == "axe")
				{
					inv.currentSelectedItemIndex = i;
					with (obj_playerInventory)
					{
						event_perform(ev_other,ev_user0);
					}
					scr_setState(id,ActorState.Chop);
					break;
				}
			}
			i++;
		}
	}
	else if (target.interactionPrefixRight == "Mine")
	{
		var i = 0;
		repeat(global.inv_inventorylimit)
		{
			var item = ds_grid_get(inv.inventory,i,0);
			if (item > 0)
			{
				if (item.itemid == "pickaxe")
				{
					inv.currentSelectedItemIndex = i;
					with (obj_playerInventory)
					{
						event_perform(ev_other,ev_user0);
					}
					scr_setState(id,ActorState.Mine);
					break;
				}
			}
			i++;
		}
	}
	else if (target.interactionPrefixLeft == "Rebuild" || target.interactionPrefixLeft == "Light up")
	{
		with (target)
		{
			var k = ds_map_find_first(ingredients);
			if(event_perform_ret(id,ev_other,ev_user1))
			{
				for(var i = 0; i < ds_map_size(ingredients); i++)
				{
					var itemid = k;
					var itemq = ds_map_find_value(ingredients,k);
		
					show_debug_message("I NEED " + string(itemq) + " OF " + itemid);
		
					for(var j = 0; j < instance_number(obj_inventory); j++)
					{
						var invobj = instance_find(obj_inventory,j);
						if (invobj.itemid == itemid)
						{
							if(invobj.quantity >= itemq)
							{
								scr_loseQuantity(invobj,itemq);
								k = ds_map_find_next(ingredients,k);
								break;
							}
							else
							{
								itemq -= invobj.quantity;
								scr_loseQuantity(invobj,invobj.quantity);
							}
						}
					}
				}
				scr_setState(other.id, ActorState.Rebuild);
			}
		}
	}
	else if (target.interactionPrefixLeft == "Sleep in" && !target.isbusy)
	{
		// Pallet can be used by visitors too
		if (target.itemid == "pallet")
		{
			scr_setState(id, ActorState.Rest);
			visible = false;
			target.isbusy = true;
			if(global.online)
			{
				sendCharResting(global.socketid,1.0);
			}
		}
		// All other sleeping objects can't be used by visitors
		if (global.otheruserid == 0)
		{
			scr_setState(id, ActorState.Rest);
			visible = false;
			target.isbusy = true;
			if(global.online)
			{
				sendCharResting(global.socketid,1.0);
			}
		}
	}
	else if ((target.interactionPrefixLeft == "Pick" ||
			 target.interactionPrefixLeft == "Check" ||
			 target.interactionPrefixLeft == "Trade" ||
			 target.interactionPrefixLeft == "Talk") && global.otheruserid == 0)
	{
		with (obj_playerInventory)
		{
			event_perform(ev_other,ev_user0);
		}
		scr_setState(id,ActorState.Pick);
	}
}