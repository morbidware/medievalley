///@function scr_setDepth(DepthType)
///@param DepthType
switch (argument0)
{
	case DepthType.Underwater: depth = -y+16000; break;
	case DepthType.Reflection: depth = 13400; break;
	case DepthType.Water: depth = 13300; break;
	case DepthType.WaterFx: depth = 13200; break;
	case DepthType.StaticBg: depth = 13100; break;
	
	case DepthType.Lv0: depth = -y+13000; break;
	case DepthType.Lv0Detail: depth = 10400; break;
	case DepthType.Lv0Shadow: depth = 10300; break;
	
	case DepthType.Lv1: depth = -y+10000; break;
	case DepthType.Lv1Detail: depth = 7400; break;
	case DepthType.Lv1Shadow: depth = 7300; break;
	
	case DepthType.Lv2: depth = -y+7000; break;
	case DepthType.Lv2Detail: depth = 4400; break;
	case DepthType.Lv2Shadow: depth = 4300; break;
	
	case DepthType.Lv3: depth = -y+4000; break;
	case DepthType.Lv3Detail: depth = 1400; break;
	case DepthType.Lv3Shadow: depth = 1300; break;
	
	case DepthType.Elements: 
	if(depth != -y)
	{
		depth = -y;
	}
	break;
	case DepthType.Overlay: depth = -15000; break;
	default:
	logga("setDepth with some problems");
	if(depth != -y)
	{
		depth = -y;
	}
	break;
}