///@function decreaseStamina()
///@argument id
///@argument q
with(argument0)
{
	var quantity = argument1 * 0.3;
	
	if(instance_exists(obj_status_stamina))
	{
		quantity = argument1 * 0.2;
	}
	
	if(room == roomChallenge)
	{
		quantity = 0;
	}
	
	stamina -= quantity;
	
	if(stamina <= 0)
	{
		stamina = 0;
	}
	global.saveuserdatatimer = 0;
}