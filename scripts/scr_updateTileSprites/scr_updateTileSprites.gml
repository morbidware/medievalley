///@function scr_updateTileSprites()
///@description Updates all the sprite tiles on the ground to match the neighboring tiles
if(argument_count == 4)
{
	var cc = argument[0];
	var rr = argument[1];
	var ccols = argument[2];
	var rrows = argument[3];

	with (obj_baseGround)
	{
		var type = GroundType.None;
		var ntype = GroundType.None;
		var index = 0;
		var storey = 0;
		//var connections = ds_list_create();
		var i = 0;
		var rand = 0;
	
		var left = false;
		var right = false;
		var top = false;
		var bottom = false;
		var bottomleft = false;
		var bottomright = false;
		var topleft = false;
		var topright = false;
	
		var storey_left = 0;
		var storey_right = 0;
		var storey_top = 0;
		var storey_bottom = 0;
		var storey_bottomleft = 0;
		var storey_bottomright = 0;
		var storey_topleft = 0;
		var storey_topright = 0;
	
		var i_top = 0;
		var i_bottom = 0;
		var i_left = 0;
		var i_right = 0;
		
		r = rr;
		c = cc;
		repeat(ccols)
		{
			r = rr;
			repeat(rrows)
			{
				left = false;
				right = false;
				top = false;
				bottom = false;
				bottomleft = false;
				bottomright = false;
				topleft = false;
				topright = false;
			
				//tile = tilegrid[#  c, r];
				type = grid[#  c, r];
				storey = storeygrid[#  c, r];
				var a = spritegrid[# c,r];
				if(a != -1)
				{
					r++;
					continue;
				}
				//no particular equation here; just trying to get a good random number for tile variations
				//rand = scr_repeatValue((c*c*1.3)+((r*r)/0.85)+(c+r), 20);
				rand = irandom(20);
			
				// soil and void are a special kind of ground and only need minimal setup
				if (type == GroundType.Soil)
				{
					// soil tile variation
					if (rand < 1)
					{
						spritegrid[# c,r] = spr_ground_soil_variation;
						//indexgrid[# c,r] = scr_repeatValue(rand*100,100);
						indexgrid[# c,r] = irandom(100);
					}
					else
					{
						spritegrid[# c,r] = spr_ground_soil;
						indexgrid[# c,r] = 0;
					}
					r++;
					continue;
				}
				else if (type == GroundType.Void)
				{
					spritegrid[# c,r] = spr_ground_void;
					indexgrid[# c,r] = 0;
					r++;
					continue;
				}
			
				// define to what other tiles each tile can connect
				ds_list_clear(global.connections);
				switch (type)
				{
					case GroundType.TallGrass:
						ds_list_add(global.connections,GroundType.TallGrass);
						spritegrid[# c,r] = spr_ground_tallgrass_tiles;
						break;
					case GroundType.Grass:
						ds_list_add(global.connections,GroundType.Grass);
						ds_list_add(global.connections,GroundType.TallGrass);
						ds_list_add(global.connections,GroundType.Cliff);
						spritegrid[# c,r] = spr_ground_grass_tiles;
						break;
					case GroundType.Plowed:
						ds_list_add(global.connections,GroundType.Plowed);
						spritegrid[# c,r] = spr_ground_plowed_tiles;
						break;
					case GroundType.Water:
						ds_list_add(global.connections,GroundType.Water);
						spritegrid[# c,r] = spr_ground_coast_tiles;
						break;
					case GroundType.Cliff:
						ds_list_add(global.connections,GroundType.Cliff);
						ds_list_add(global.connections,GroundType.Grass);
						ds_list_add(global.connections,GroundType.TallGrass);
						ds_list_add(global.connections,GroundType.Soil);
						ds_list_add(global.connections,GroundType.Plowed);
						ds_list_add(global.connections,GroundType.Water);
						spritegrid[# c,r] = spr_ground_cliff_tiles;
						break;
					case GroundType.HouseFloor:
						ds_list_add(global.connections,GroundType.HouseFloor);
						ds_list_add(global.connections,GroundType.HouseBasement);
						spritegrid[# c,r] = spr_ground_house_tiles;
						break;
					case GroundType.HouseBasement:
						ds_list_add(global.connections,GroundType.HouseFloor);
						ds_list_add(global.connections,GroundType.HouseBasement);
						spritegrid[# c,r] = spr_ground_basement_tiles;
						break;
				}
			
				// check surrounding tiles; values are clamped so checking outside the grid will return a valid neighbor
				ntype = grid[#  clamp(c-1, 0, cols-1), clamp(r-1, 0, rows-1)];
				storey_topleft = storeygrid[#  clamp(c-1, 0, cols-1), clamp(r-1, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){topleft = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) topleft = true;
				*/
				ntype = grid[#  clamp(c, 0, cols-1), clamp(r-1, 0, rows-1)];
				storey_top = storeygrid[#  clamp(c, 0, cols-1), clamp(r-1, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){top = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) top = true;
				*/
				ntype = grid[#  clamp(c+1, 0, cols-1), clamp(r-1, 0, rows-1)];
				storey_topright = storeygrid[#  clamp(c+1, 0, cols-1), clamp(r-1, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){topright = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) topright = true;
				*/
				ntype = grid[#  clamp(c-1, 0, cols-1), clamp(r, 0, rows-1)];
				storey_left = storeygrid[#  clamp(c-1, 0, cols-1), clamp(r, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){left = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) left = true;
				*/
				ntype = grid[#  clamp(c+1, 0, cols-1), clamp(r, 0, rows-1)];
				storey_right = storeygrid[#  clamp(c+1, 0, cols-1), clamp(r, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){right = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) right = true;
				*/
				ntype = grid[#  clamp(c-1, 0, cols-1), clamp(r+1, 0, rows-1)];
				storey_bottomleft = storeygrid[#  clamp(c-1, 0, cols-1), clamp(r+1, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){bottomleft = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) bottomleft = true;
				*/
				ntype = grid[#  clamp(c, 0, cols-1), clamp(r+1, 0, rows-1)];
				storey_bottom = storeygrid[#  clamp(c, 0, cols-1), clamp(r+1, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){bottom = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) bottom = true;
				*/
				ntype = grid[#  clamp(c+1, 0, cols-1), clamp(r+1, 0, rows-1)];
				storey_bottomright = storeygrid[#  clamp(c+1, 0, cols-1), clamp(r+1, 0, rows-1)];
				if(ds_list_find_index(global.connections,ntype) > -1){bottomright = true;}
				/*
				for (i = 0; i < ds_list_size(global.connections); i++)
					if (ntype == ds_list_find_value(global.connections,i)) bottomright = true;
				*/
				// if this tile is a cliff, surroundings with different storeys are not considered connected
				if (type == GroundType.Cliff)
				{
					if (storey > storey_topleft) topleft = false;
					if (storey > storey_top) top = false;
					if (storey > storey_topright) topright = false;
					if (storey > storey_left) left = false;
					if (storey > storey_right) right = false;
					if (storey > storey_bottomleft) bottomleft = false;
					if (storey > storey_bottom) bottom = false;
					if (storey > storey_bottomright) bottomright = false;
				}
			
				// assign tile sprite depending by neighobrs found
				if(top && bottom && left && right && topleft && topright && bottomleft && bottomright) indexgrid[# c,r] = 47;
				else if(!top && bottom && left && right /*&& topleft && topright*/&& bottomleft && bottomright) indexgrid[# c,r] = 20;
				else if(top && !bottom && left && right && topleft && topright /*&& bottomleft && bottomright*/) indexgrid[# c,r] = 28;
				else if(top && bottom && !left && right /*&& topleft*/ && topright /*&& bottomleft*/ && bottomright) indexgrid[# c,r] = 16;
				else if(top && bottom && left && !right && topleft /*&& topright*/ && bottomleft /*&& bottomright*/) indexgrid[# c,r] = 24;
				else if(!top && bottom && !left && right /*&& topleft && topright && bottomleft*/ && bottomright) indexgrid[# c,r] = 34;
				else if(!top && bottom && left && !right /*&& topleft && topright*/ && bottomleft /*&& bottomright*/) indexgrid[# c,r] = 36;
				else if(top && !bottom && !left && right /*&& topleft*/ && topright /*&& bottomleft && bottomright*/) indexgrid[# c,r] = 40;
				else if(top && !bottom && left && !right && topleft /*&& topright && bottomleft && bottomright*/) indexgrid[# c,r] = 38;
				/////////////
				/////////////
				else if(top && bottom && left && right && !topleft && topright && bottomleft && bottomright) indexgrid[# c,r] = 1;
				else if(top && bottom && left && right && topleft && !topright && bottomleft && bottomright) indexgrid[# c,r] = 2;
				else if(top && bottom && left && right && !topleft && !topright && bottomleft && bottomright) indexgrid[# c,r] = 3;
				else if(top && bottom && left && right && topleft && topright && bottomleft && !bottomright) indexgrid[# c,r] = 4;
				else if(top && bottom && left && right && !topleft && topright && bottomleft && !bottomright) indexgrid[# c,r] = 5;
				else if(top && bottom && left && right && topleft && !topright && bottomleft && !bottomright) indexgrid[# c,r] = 6;
				else if(top && bottom && left && right && !topleft && !topright && bottomleft && !bottomright) indexgrid[# c,r] = 7;
				else if(top && bottom && left && right && topleft && topright && !bottomleft && bottomright) indexgrid[# c,r] = 8;
				else if(top && bottom && left && right && !topleft && topright && !bottomleft && bottomright) indexgrid[# c,r] = 9;
				else if(top && bottom && left && right && topleft && !topright && !bottomleft && bottomright){indexgrid[# c,r] = 10;}
				else if(top && bottom && left && right && !topleft && !topright && !bottomleft && bottomright){indexgrid[# c,r] = 11;}
				else if(top && bottom && left && right && topleft && topright && !bottomleft && !bottomright){indexgrid[# c,r] = 12;}
				else if(top && bottom && left && right && !topleft && topright && !bottomleft && !bottomright){indexgrid[# c,r] = 13;}
				else if(top && bottom && left && right && topleft && !topright && !bottomleft && !bottomright){indexgrid[# c,r] = 14;}
				else if(top && bottom && left && right && !topleft && !topright && !bottomleft && !bottomright){indexgrid[# c,r] = 15;}
				else if(top && bottom && !left && right /*&& !topleft*/ && !topright /*&& !bottomleft*/ && bottomright){indexgrid[# c,r] = 17;}
				else if(top && bottom && !left && right /*&& !topleft*/ && topright /*&& !bottomleft*/ && !bottomright){indexgrid[# c,r] = 18;}
				else if(top && bottom && !left && right /*&& !topleft*/ && !topright /*&& !bottomleft*/ && !bottomright){indexgrid[# c,r] = 19;}
				else if(!top && bottom && left && right /*&& !topleft && !topright*/ && bottomleft && !bottomright){indexgrid[# c,r] = 21;}
				else if(!top && bottom && left && right /*&& !topleft && !topright*/ && !bottomleft && bottomright){indexgrid[# c,r] = 22;}
				else if(!top && bottom && left && right /*&& !topleft && !topright*/ && !bottomleft && !bottomright){indexgrid[# c,r] = 23;}
				else if(top && bottom && left && !right && topleft /*&& !topright*/ && !bottomleft /*&& !bottomright*/){indexgrid[# c,r] = 25;}
				else if(top && bottom && left && !right && !topleft /*&& !topright*/ && bottomleft /*&& !bottomright*/){indexgrid[# c,r] = 26;}
				else if(top && bottom && left && !right && !topleft /*&& !topright*/ && !bottomleft /*&& !bottomright*/){indexgrid[# c,r] = 27;}
				else if(top && !bottom && left && right && !topleft && topright /*&& !bottomleft && !bottomright*/){indexgrid[# c,r] = 29;}
				else if(top && !bottom && left && right && topleft && !topright /*&& !bottomleft && !bottomright*/){indexgrid[# c,r] = 30;}
				else if(top && !bottom && left && right && !topleft && !topright /*&& !bottomleft && !bottomright*/){indexgrid[# c,r] = 31;}
				else if(top && bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 32;}
				else if(!top && !bottom && left && right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 33;}
				else if(!top && bottom && !left && right /*&& !topleft && topright && bottomleft*/ && !bottomright){indexgrid[# c,r] = 35;}
				else if(!top && bottom && left && !right /*&& topleft && !topright*/ && !bottomleft /*&& bottomright*/){indexgrid[# c,r] = 37;}
				else if(top && !bottom && left && !right && !topleft /*&& topright && bottomleft && !bottomright*/){indexgrid[# c,r] = 39;}
				else if(top && !bottom && !left && right /*&& topleft*/ && !topright /*&& !bottomleft && bottomright*/){indexgrid[# c,r] = 41;}
				else if(!top && bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 42;}
				else if(!top && !bottom && !left && right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 43;}
				else if(top && !bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 44;}
				else if(!top && !bottom && left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 45;}
				else if(!top && !bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){indexgrid[# c,r] = 46;}
			
				// special cases for tilesets with diagonal tiles and variation on all tiles
				if (type = GroundType.Cliff)
				{
					// bottom right
					var storey = storeygrid[# c,r];
					if (top && !bottom && left && !right && topleft && topright && bottomleft && !bottomright)
					{
						indexgrid[# c,r] = 49;
						var w = scr_createStoreyWall(c,r,storey,3);
					}
					// bottom left
					else if (top && !bottom && !left && right && topleft && topright && !bottomleft && bottomright)
					{
						indexgrid[# c,r] = 50;
						var w = scr_createStoreyWall(c,r,storey,2);
					}
					// top left
					else if (!top && bottom && !left && right && !topleft && topright && bottomleft && bottomright)
					{
						indexgrid[# c,r] = 53;
						var w = scr_createStoreyWall(c,r,storey,0);
					}
					// top right
					else if (!top && bottom && left && !right && topleft && !topright && bottomleft && bottomright)
					{
						indexgrid[# c,r] = 54;
						var w = scr_createStoreyWall(c,r,storey,1);
					}
				
					if (random(1) < 0.5)
					{
						spritegrid[# c,r] = spr_ground_cliff_variation;
					}
				}
			
				// tile detail variations
				if (ds_grid_get(indexgrid,c,r) == 47)
				{
					if (rand < 1)
					{
						switch (type)
						{
							case GroundType.Grass:
								spritegrid[# c,r] = spr_ground_grass_variation;
								//indexgrid[# c,r] = scr_repeatValue(rand*123,100);
								indexgrid[# c,r] = irandom(100);
								break;
							case GroundType.TallGrass:
								spritegrid[# c,r] = spr_ground_tallgrass_variation;
								//indexgrid[# c,r] = scr_repeatValue(rand*123,100);
								indexgrid[# c,r] = irandom(100);
								break;
						}
					}
					else
					{
						switch (type)
						{
							case GroundType.Grass:
								spritegrid[# c,r] = spr_ground_grass_variation_47;
								//indexgrid[# c,r] = scr_repeatValue(rand,100);
								indexgrid[# c,r] = irandom(100);
								break;
							case GroundType.TallGrass:
								spritegrid[# c,r] = spr_ground_tallgrass_variation_47;
								//indexgrid[# c,r] = scr_repeatValue(rand,100);
								indexgrid[# c,r] = irandom(100);
								break;
						}
					}
				}
				r++;
			}	
			c++;
		}
	
		// additional pass for tilesets that have diagonal tiles (this will add the small inner tiles for diagonals)
		c = cc;
		repeat(ccols)
		{
			r = rr;
			repeat(rrows)
			{
				
				type = grid[#  c, r];
				index = indexgrid[#  c, r];
			
				if (type == GroundType.Cliff && (index == 1 || index ==  2 || index == 4 || index == 8))
				{
				
					// get surrounding tile objects (no diagonals); values are clamped so checking outside the grid will return an identical neighbor
					i_top = indexgrid[#  clamp(c, 0, cols-1), clamp(r-1, 0, rows-1)];
					i_bottom = indexgrid[#  clamp(c, 0, cols-1), clamp(r+1, 0, rows-1)];
					i_left = indexgrid[#  clamp(c-1, 0, cols-1), clamp(r, 0, rows-1)];
					i_right = indexgrid[#  clamp(c+1, 0, cols-1), clamp(r, 0, rows-1)];
				
					switch (index)
					{
						case 1: if (i_top == 53 && i_left == 53) {indexgrid[# c,r] = 55;} break;
						case 2: if (i_top == 54 && i_right == 54) {indexgrid[# c,r] = 56;} break;
						case 4: if (i_bottom == 49 && i_right == 49) {indexgrid[# c,r] = 51;} break;
						case 8: if (i_bottom == 50 && i_left == 50) {indexgrid[# c,r] = 52;} break;
					}
				}	
				r++;
			
			}
			c++;
		}
	
	}
	
}
else
{
	#region normale
	with (obj_baseGround)
	{
		var type = GroundType.None;
		var ntype = GroundType.None;
		var index = 0;
		var storey = 0;
		var connections = ds_list_create();
		var i = 0;
		var rand = 0;
	
		var left = false;
		var right = false;
		var top = false;
		var bottom = false;
		var bottomleft = false;
		var bottomright = false;
		var topleft = false;
		var topright = false;
	
		var storey_left = 0;
		var storey_right = 0;
		var storey_top = 0;
		var storey_bottom = 0;
		var storey_bottomleft = 0;
		var storey_bottomright = 0;
		var storey_topleft = 0;
		var storey_topright = 0;
	
		var i_top = 0;
		var i_bottom = 0;
		var i_left = 0;
		var i_right = 0;
	
		r = 0;
		c = 0;
		repeat(cols)
		{
			r = 0;
			repeat(rows)
			{
				left = false;
				right = false;
				top = false;
				bottom = false;
				bottomleft = false;
				bottomright = false;
				topleft = false;
				topright = false;
			
				//tile = ds_grid_get(tilegrid, c, r);
				type = ds_grid_get(grid, c, r);
				storey = ds_grid_get(storeygrid, c, r);
			
				//no particular equation here; just trying to get a good random number for tile variations
				rand = scr_repeatValue((c*c*1.3)+((r*r)/0.85)+(c+r), 20);
			
				// soil and void are a special kind of ground and only need minimal setup
				if (type == GroundType.Soil)
				{
					// soil tile variation
					if (rand < 1)
					{
						ds_grid_set(spritegrid,c,r,spr_ground_soil_variation);
						ds_grid_set(indexgrid,c,r,scr_repeatValue(rand*100,100));
					}
					else
					{
						ds_grid_set(spritegrid,c,r,spr_ground_soil);
						ds_grid_set(indexgrid,c,r,0);
					}
					r++;
					continue;
				}
				else if (type == GroundType.Void)
				{
					ds_grid_set(spritegrid,c,r,spr_ground_void);
					ds_grid_set(indexgrid,c,r,0);
					r++;
					continue;
				}
			
				// define to what other tiles each tile can connect
				ds_list_clear(connections);
				switch (type)
				{
					case GroundType.TallGrass:
						ds_list_add(connections,GroundType.TallGrass);
						ds_grid_set(spritegrid,c,r,spr_ground_tallgrass_tiles);
						break;
					case GroundType.Grass:
						ds_list_add(connections,GroundType.Grass);
						ds_list_add(connections,GroundType.TallGrass);
						ds_list_add(connections,GroundType.Cliff);
						ds_grid_set(spritegrid,c,r,spr_ground_grass_tiles);
						break;
					case GroundType.Plowed:
						ds_list_add(connections,GroundType.Plowed);
						ds_grid_set(spritegrid,c,r,spr_ground_plowed_tiles);
						break;
					case GroundType.Water:
						ds_list_add(connections,GroundType.Water);
						ds_grid_set(spritegrid,c,r,spr_ground_coast_tiles);
						break;
					case GroundType.Cliff:
						ds_list_add(connections,GroundType.Cliff);
						ds_list_add(connections,GroundType.Grass);
						ds_list_add(connections,GroundType.TallGrass);
						ds_list_add(connections,GroundType.Soil);
						ds_list_add(connections,GroundType.Plowed);
						ds_list_add(connections,GroundType.Water);
						ds_grid_set(spritegrid,c,r,spr_ground_cliff_tiles);
						break;
					case GroundType.HouseFloor:
						ds_list_add(connections,GroundType.HouseFloor);
						ds_list_add(connections,GroundType.HouseBasement);
						ds_grid_set(spritegrid,c,r,spr_ground_house_tiles);
						break;
					case GroundType.HouseBasement:
						ds_list_add(connections,GroundType.HouseFloor);
						ds_list_add(connections,GroundType.HouseBasement);
						ds_grid_set(spritegrid,c,r,spr_ground_basement_tiles);
						break;
				}
			
				// check surrounding tiles; values are clamped so checking outside the grid will return a valid neighbor
				ntype = ds_grid_get(grid, clamp(c-1, 0, cols-1), clamp(r-1, 0, rows-1));
				storey_topleft = ds_grid_get(storeygrid, clamp(c-1, 0, cols-1), clamp(r-1, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) topleft = true;
			
				ntype = ds_grid_get(grid, clamp(c, 0, cols-1), clamp(r-1, 0, rows-1));
				storey_top = ds_grid_get(storeygrid, clamp(c, 0, cols-1), clamp(r-1, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) top = true;
			
				ntype = ds_grid_get(grid, clamp(c+1, 0, cols-1), clamp(r-1, 0, rows-1));
				storey_topright = ds_grid_get(storeygrid, clamp(c+1, 0, cols-1), clamp(r-1, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) topright = true;
			
				ntype = ds_grid_get(grid, clamp(c-1, 0, cols-1), clamp(r, 0, rows-1));
				storey_left = ds_grid_get(storeygrid, clamp(c-1, 0, cols-1), clamp(r, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) left = true;
			
				ntype = ds_grid_get(grid, clamp(c+1, 0, cols-1), clamp(r, 0, rows-1));
				storey_right = ds_grid_get(storeygrid, clamp(c+1, 0, cols-1), clamp(r, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) right = true;
			
				ntype = ds_grid_get(grid, clamp(c-1, 0, cols-1), clamp(r+1, 0, rows-1));
				storey_bottomleft = ds_grid_get(storeygrid, clamp(c-1, 0, cols-1), clamp(r+1, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) bottomleft = true;
			
				ntype = ds_grid_get(grid, clamp(c, 0, cols-1), clamp(r+1, 0, rows-1));
				storey_bottom = ds_grid_get(storeygrid, clamp(c, 0, cols-1), clamp(r+1, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) bottom = true;
			
				ntype = ds_grid_get(grid, clamp(c+1, 0, cols-1), clamp(r+1, 0, rows-1));
				storey_bottomright = ds_grid_get(storeygrid, clamp(c+1, 0, cols-1), clamp(r+1, 0, rows-1));
				for (i = 0; i < ds_list_size(connections); i++)
					if (ntype == ds_list_find_value(connections,i)) bottomright = true;
			
				// if this tile is a cliff, surroundings with different storeys are not considered connected
				if (type == GroundType.Cliff)
				{
					if (storey > storey_topleft) topleft = false;
					if (storey > storey_top) top = false;
					if (storey > storey_topright) topright = false;
					if (storey > storey_left) left = false;
					if (storey > storey_right) right = false;
					if (storey > storey_bottomleft) bottomleft = false;
					if (storey > storey_bottom) bottom = false;
					if (storey > storey_bottomright) bottomright = false;
				}
			
				// assign tile sprite depending by neighobrs found
				if(top && bottom && left && right && topleft && topright && bottomleft && bottomright) ds_grid_set(indexgrid,c,r,47);
				else if(!top && bottom && left && right /*&& topleft && topright*/&& bottomleft && bottomright) ds_grid_set(indexgrid,c,r,20);
				else if(top && !bottom && left && right && topleft && topright /*&& bottomleft && bottomright*/) ds_grid_set(indexgrid,c,r,28);
				else if(top && bottom && !left && right /*&& topleft*/ && topright /*&& bottomleft*/ && bottomright) ds_grid_set(indexgrid,c,r,16);
				else if(top && bottom && left && !right && topleft /*&& topright*/ && bottomleft /*&& bottomright*/) ds_grid_set(indexgrid,c,r,24);
				else if(!top && bottom && !left && right /*&& topleft && topright && bottomleft*/ && bottomright) ds_grid_set(indexgrid,c,r,34);
				else if(!top && bottom && left && !right /*&& topleft && topright*/ && bottomleft /*&& bottomright*/) ds_grid_set(indexgrid,c,r,36);
				else if(top && !bottom && !left && right /*&& topleft*/ && topright /*&& bottomleft && bottomright*/) ds_grid_set(indexgrid,c,r,40);
				else if(top && !bottom && left && !right && topleft /*&& topright && bottomleft && bottomright*/) ds_grid_set(indexgrid,c,r,38);
				/////////////
				/////////////
				else if(top && bottom && left && right && !topleft && topright && bottomleft && bottomright) ds_grid_set(indexgrid,c,r,1);
				else if(top && bottom && left && right && topleft && !topright && bottomleft && bottomright) ds_grid_set(indexgrid,c,r,2);
				else if(top && bottom && left && right && !topleft && !topright && bottomleft && bottomright) ds_grid_set(indexgrid,c,r,3);
				else if(top && bottom && left && right && topleft && topright && bottomleft && !bottomright) ds_grid_set(indexgrid,c,r,4);
				else if(top && bottom && left && right && !topleft && topright && bottomleft && !bottomright) ds_grid_set(indexgrid,c,r,5);
				else if(top && bottom && left && right && topleft && !topright && bottomleft && !bottomright) ds_grid_set(indexgrid,c,r,6);
				else if(top && bottom && left && right && !topleft && !topright && bottomleft && !bottomright) ds_grid_set(indexgrid,c,r,7);
				else if(top && bottom && left && right && topleft && topright && !bottomleft && bottomright) ds_grid_set(indexgrid,c,r,8);
				else if(top && bottom && left && right && !topleft && topright && !bottomleft && bottomright) ds_grid_set(indexgrid,c,r,9);
				else if(top && bottom && left && right && topleft && !topright && !bottomleft && bottomright){ds_grid_set(indexgrid,c,r,10);}
				else if(top && bottom && left && right && !topleft && !topright && !bottomleft && bottomright){ds_grid_set(indexgrid,c,r,11);}
				else if(top && bottom && left && right && topleft && topright && !bottomleft && !bottomright){ds_grid_set(indexgrid,c,r,12);}
				else if(top && bottom && left && right && !topleft && topright && !bottomleft && !bottomright){ds_grid_set(indexgrid,c,r,13);}
				else if(top && bottom && left && right && topleft && !topright && !bottomleft && !bottomright){ds_grid_set(indexgrid,c,r,14);}
				else if(top && bottom && left && right && !topleft && !topright && !bottomleft && !bottomright){ds_grid_set(indexgrid,c,r,15);}
				else if(top && bottom && !left && right /*&& !topleft*/ && !topright /*&& !bottomleft*/ && bottomright){ds_grid_set(indexgrid,c,r,17);}
				else if(top && bottom && !left && right /*&& !topleft*/ && topright /*&& !bottomleft*/ && !bottomright){ds_grid_set(indexgrid,c,r,18);}
				else if(top && bottom && !left && right /*&& !topleft*/ && !topright /*&& !bottomleft*/ && !bottomright){ds_grid_set(indexgrid,c,r,19);}
				else if(!top && bottom && left && right /*&& !topleft && !topright*/ && bottomleft && !bottomright){ds_grid_set(indexgrid,c,r,21);}
				else if(!top && bottom && left && right /*&& !topleft && !topright*/ && !bottomleft && bottomright){ds_grid_set(indexgrid,c,r,22);}
				else if(!top && bottom && left && right /*&& !topleft && !topright*/ && !bottomleft && !bottomright){ds_grid_set(indexgrid,c,r,23);}
				else if(top && bottom && left && !right && topleft /*&& !topright*/ && !bottomleft /*&& !bottomright*/){ds_grid_set(indexgrid,c,r,25);}
				else if(top && bottom && left && !right && !topleft /*&& !topright*/ && bottomleft /*&& !bottomright*/){ds_grid_set(indexgrid,c,r,26);}
				else if(top && bottom && left && !right && !topleft /*&& !topright*/ && !bottomleft /*&& !bottomright*/){ds_grid_set(indexgrid,c,r,27);}
				else if(top && !bottom && left && right && !topleft && topright /*&& !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,29);}
				else if(top && !bottom && left && right && topleft && !topright /*&& !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,30);}
				else if(top && !bottom && left && right && !topleft && !topright /*&& !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,31);}
				else if(top && bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,32);}
				else if(!top && !bottom && left && right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,33);}
				else if(!top && bottom && !left && right /*&& !topleft && topright && bottomleft*/ && !bottomright){ds_grid_set(indexgrid,c,r,35);}
				else if(!top && bottom && left && !right /*&& topleft && !topright*/ && !bottomleft /*&& bottomright*/){ds_grid_set(indexgrid,c,r,37);}
				else if(top && !bottom && left && !right && !topleft /*&& topright && bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,39);}
				else if(top && !bottom && !left && right /*&& topleft*/ && !topright /*&& !bottomleft && bottomright*/){ds_grid_set(indexgrid,c,r,41);}
				else if(!top && bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,42);}
				else if(!top && !bottom && !left && right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,43);}
				else if(top && !bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,44);}
				else if(!top && !bottom && left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,45);}
				else if(!top && !bottom && !left && !right /*&& !topleft && !topright && !bottomleft && !bottomright*/){ds_grid_set(indexgrid,c,r,46);}
			
				// special cases for tilesets with diagonal tiles and variation on all tiles
				if (type = GroundType.Cliff)
				{
					// bottom right
					var storey = ds_grid_get(storeygrid,c,r);
					if (top && !bottom && left && !right && topleft && topright && bottomleft && !bottomright)
					{
						ds_grid_set(indexgrid,c,r,49);
						var w = scr_createStoreyWall(c,r,storey,3);
					}
					// bottom left
					else if (top && !bottom && !left && right && topleft && topright && !bottomleft && bottomright)
					{
						ds_grid_set(indexgrid,c,r,50);
						var w = scr_createStoreyWall(c,r,storey,2);
					}
					// top left
					else if (!top && bottom && !left && right && !topleft && topright && bottomleft && bottomright)
					{
						ds_grid_set(indexgrid,c,r,53);
						var w = scr_createStoreyWall(c,r,storey,0);
					}
					// top right
					else if (!top && bottom && left && !right && topleft && !topright && bottomleft && bottomright)
					{
						ds_grid_set(indexgrid,c,r,54);
						var w = scr_createStoreyWall(c,r,storey,1);
					}
				
					if (random(1) < 0.5)
					{
						ds_grid_set(spritegrid,c,r,spr_ground_cliff_variation);
					}
				}
			
				// tile detail variations
				if (ds_grid_get(indexgrid,c,r) == 47)
				{
					if (rand < 1)
					{
						switch (type)
						{
							case GroundType.Grass:
								ds_grid_set(spritegrid,c,r,spr_ground_grass_variation);
								ds_grid_set(indexgrid,c,r,scr_repeatValue(rand*123,100));
								break;
							case GroundType.TallGrass:
								ds_grid_set(spritegrid,c,r,spr_ground_tallgrass_variation);
								ds_grid_set(indexgrid,c,r,scr_repeatValue(rand*123,100));
								break;
						}
					}
					else
					{
						switch (type)
						{
							case GroundType.Grass:
								ds_grid_set(spritegrid,c,r,spr_ground_grass_variation_47);
								ds_grid_set(indexgrid,c,r,scr_repeatValue(rand,100));
								break;
							case GroundType.TallGrass:
								ds_grid_set(spritegrid,c,r,spr_ground_tallgrass_variation_47);
								ds_grid_set(indexgrid,c,r,scr_repeatValue(rand,100));
								break;
						}
					}
				}
				r++;
			
			}	
			c++;
		}
	
		// additional pass for tilesets that have diagonal tiles (this will add the small inner tiles for diagonals)
		c = 0;
		repeat(cols)
		{
			r = 0;
			repeat(rows)
			{
				type = ds_grid_get(grid, c, r);
				index = ds_grid_get(indexgrid, c, r);
			
				if (type == GroundType.Cliff && (index == 1 || index ==  2 || index == 4 || index == 8))
				{
				
					// get surrounding tile objects (no diagonals); values are clamped so checking outside the grid will return an identical neighbor
					i_top = ds_grid_get(indexgrid, clamp(c, 0, cols-1), clamp(r-1, 0, rows-1));
					i_bottom = ds_grid_get(indexgrid, clamp(c, 0, cols-1), clamp(r+1, 0, rows-1));
					i_left = ds_grid_get(indexgrid, clamp(c-1, 0, cols-1), clamp(r, 0, rows-1));
					i_right = ds_grid_get(indexgrid, clamp(c+1, 0, cols-1), clamp(r, 0, rows-1));
				
					switch (index)
					{
						case 1: if (i_top == 53 && i_left == 53) {ds_grid_set(indexgrid,c,r,55);} break;
						case 2: if (i_top == 54 && i_right == 54) {ds_grid_set(indexgrid,c,r,56);} break;
						case 4: if (i_bottom == 49 && i_right == 49) {ds_grid_set(indexgrid,c,r,51);} break;
						case 8: if (i_bottom == 50 && i_left == 50) {ds_grid_set(indexgrid,c,r,52);} break;
					}
				}	
				r++;
			
			}
			c++;
		}
	}	
	#endregion
}