///@function gmcallback_challengeCheck()
///@param str
if(room == roomMain)
{
	if(m_debugChallenge)
	{
		global.newUserForChallenge = false;
	}
	if(global.newUserForChallenge)
	{
		global.newUserForChallenge = false;
		gmcallback_gotUserHistory("false:false:false:0:false");
	}
	else
	{
		if(m_debugChallenge)
		{
			gmcallback_gotUserHistory("10:1:1:1");
		}
		else
		{
			getUserHistory(global.userigid);
		}
	}
}
else
{
	
	logga("User " + string(global.userigid) + " has started to play challenge num " + string(global.lastChallengeDone) + " with tot time " + string(60-global.challengeMalus));
	insertRecord(global.userigid,global.challengeType,60-global.challengeMalus,global.challengeMalusSecondsRemoved,string(0.10+(global.challengeMalusSecondsRemoved*global.challengeGalasilverPrice)),global.ismobile);
	
	
	global.lastChallengeDone ++;
	if(global.lastChallengeDone > 6)
	{
		global.lastChallengeDone = 0;
	}
	scr_storeUserdata();
	
}