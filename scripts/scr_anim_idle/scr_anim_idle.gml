///@function scr_anim_idle(object,direction)
///@param object the object
///@param direction direction of the player
switch(argument1)
{
	case Direction.Right:
	image_angle = 0;
	x = argument0.x - 3;
	y = argument0.y + 5;
	break;
	
	case Direction.Left:
	image_angle = 0;
	x = argument0.x + 3;
	y = argument0.y + 5;
	break;
	
	case Direction.Up:
	image_angle = 90;
	x = argument0.x + 5;
	y = argument0.y + 2;
	break;
	
	case Direction.Down:
	image_angle = 270;
	x = argument0.x - 5;
	y = argument0.y + 6;
	break;
}
y -= 16;