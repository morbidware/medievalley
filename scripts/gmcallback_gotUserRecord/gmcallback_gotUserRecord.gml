if(global.isSurvival)
{
	var str = argument0; 
	var del = "$";
	var res = ds_list_create();
	var pos = string_pos(del, str);
	var dellen = string_length(del);
	while (pos) 
	{
	    pos -= 1;
	    ds_list_add(res, string_copy(str, 1, pos));
	    str = string_delete(str, 1, pos + dellen);
	    pos = string_pos(del, str);
	}
	ds_list_add(res, str);
	
	var found = real(ds_list_find_value(res,0));
	global.landid = real(ds_list_find_value(res,1));
	
	if(found)
	{
		with (obj_home)
		{
			logga("> Received record. User already exists.");
			newuser = false;
			userrecordreceived = true;
		}
	}
	else
	{
		with (obj_home)
		{
			logga("> Received record. User is new.");
			newuser = true;
			userrecordreceived = true;
		}
	}
}
else
{
	if (round(real(argument0)) == 1)
	{
		with (obj_home)
		{
			logga("> Received record. User already exists.");
			newuser = false;
			userrecordreceived = true;
		}
	}
	else
	{
		with (obj_home)
		{
			logga("> Received record. User is new.");
			newuser = true;
			userrecordreceived = true;
		}
	}
}
