///@function scr_string_height_ext(text, sep, w);
///@description This function returns the height of a text block
///@param text
///@param sep
///@param w
var text = argument0;
var sep = argument1;
var w = argument2;

var subtext = "";
var startindex = 0;
var lines = 0;
var breakindex = 0;
var breaklength = 0;
for (var i = 1; i < string_length(text); i++) {
	
	var substring = string_copy(text, startindex, i-startindex);
	if (string_char_at(text, i) == " ")
	{
		breakindex = i+1;
		breaklength = i-startindex;
	}
	
	if (string_width(substring) >= w)
	{
		startindex = breakindex;
		i = startindex;
		lines++;
	}
}
return lines * sep;