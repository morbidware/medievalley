///@function easeInBack(t, b, c, d)

var t = argument0;
var b = argument1;
var c = argument2;
var d = argument3;
var s = 1.70158;

var t2 = t/d;

return (c*(t2)*t*((s+1)*t - s) + b);