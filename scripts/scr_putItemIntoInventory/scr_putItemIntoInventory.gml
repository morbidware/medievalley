///@function scr_putItemIntoInventory(object,inventoryType,slot)
///@param object
///@param inventoryType
///@param slot

// Inserts the specified object into the specified inventry type, at specified slot. If slot is -1,
// puts item in the first available slot, first trying to merge with already existent stacks.
// Returns true if succeded, false otherwise.
// IMPORTANT: object can either be a obj_pickable or an obj_inventory.
logga("---------------------------");
logga("scr_putItemIntoInventory()");
var obj = argument0;
var invType = argument1;
if(global.isSurvival && (invType == InventoryType.All || invType == InventoryType.Carry || invType == InventoryType.Backpack))
{
	invType = InventoryType.Inventory;
}
var slot = argument2;
var inv = instance_find(obj_playerInventory,0);
global.saveitemstimer = 0;
// get inventory ranges
var start;
var finish;
switch (invType)
{
	case InventoryType.All:
		start = 0;
		finish = global.inv_stashlimit;
		break;
		
	case InventoryType.Inventory:
		start = 0;
		finish = global.inv_inventorylimit;
		break;
		
	case InventoryType.Backpack:
		start = global.inv_inventorylimit;
		finish = global.inv_robelimit
		break;
		
	case InventoryType.Carry:
		start = 0;
		finish = global.inv_carrylimit;
		break;
		
	case InventoryType.Stash:
		start = global.inv_robelimit;
		finish = global.inv_stashlimit
		break;
	
	case InventoryType.Safe:
		start = global.inv_stashlimit;
		finish = global.inv_safelimit
		break;
}
// if any slot is good...
if (slot == -1)
{
	// first try to merge into a slot with the same item
	for(var i = start; i < finish; i++)
	{
		var slotValue = ds_grid_get(inv.inventory,i,0);
		// ignore reserved slots
		if (i == global.inv_head || i == global.inv_torso || i == global.inv_pants || i == global.inv_carry)
		{
			if(slotValue == -1)
			{
				
			}
			continue;
		}
		
		if (slotValue > 0)
		{
			if(slotValue.itemid == obj.itemid && slotValue.quantity + obj.quantity <= slotValue.maxquantity)
			{
				slotValue.quantity += obj.quantity;
				
				// refresh
				with(obj_playerInventory)
				{
					event_perform(ev_other,ev_user0);
				}
				with(obj_mission)
				{
					event_perform(ev_other,ev_user1);
				}
				scr_updateRecipes();
				if(global.isSurvival)
				{
					scr_storeItemsSurvival();
				}
				logga("---------------------------1");
				return true;
			}
		}
	}
	// otherwise put item into an emtpy slot
	for(var i = start; i < finish; i++)
	{
		
		// ignore reserved slots
		if (i == global.inv_head || i == global.inv_torso || i == global.inv_pants || i == global.inv_carry)
		{
			continue;
			
		}
		
		var slotValue = ds_grid_get(inv.inventory,i,0);
		if (slotValue == -1)
		{
			slot = i;
			break;
		}
	}
}
// if still not possible to add item...
if (slot == -1)
{
	if(!global.isSurvival)
	{
		// First try to put it into backpack
		if (invType == InventoryType.Inventory)
		{
			scr_putItemIntoInventory(obj,InventoryType.Backpack,-1);
			exit;
		}
		
	}
	
	// Otherwise just drop it on the ground
	var player = instance_find(obj_char,0);
	var dropped = scr_gameItemCreate(player.x, player.y+8, obj_pickable, obj.itemid, obj.quantity);
	dropped.durability = obj.durability;
	dropped.dropped = true;
	instance_destroy(obj);
	scr_updateRecipes();
	logga("---------------------------2");
	return false;
}

// add item to desired slot
if (obj.object_index == obj_inventory)
{
	if (slot == global.inv_head || slot == global.inv_torso || slot == global.inv_pants || slot == global.inv_carry)
	{
		
		if(obj.wearable == 0 ||(obj.wearable == 1 && slot != global.inv_head) || (obj.wearable == 2 && slot != global.inv_torso) || (obj.wearable == 3 && slot != global.inv_pants))
		{
			scr_putItemIntoInventory(obj,InventoryType.Inventory,-1);
			exit;
		}
		
		var inst = scr_gameItemCreate(0,0,obj_wearable,obj.itemid);
		with(inst)
		{
			var player = instance_find(obj_char,0);
			wear = instance_find(obj,0);
			player.defence += wear.defence;
			event_perform(ev_other,ev_user0);	
		}
	}	
	obj.slot = slot;
	obj.state = 0;
	ds_grid_set(inv.inventory,slot,0,obj);
	logga("add to slot "+string(slot));
	
}
else
{
	var inst = scr_gameItemCreate(0,0,obj_inventory,obj.itemid,obj.quantity);
	inst.x = -64;
	inst.y = -64;
	inst.slot = slot;
	inst.durability = obj.durability;
	ds_grid_set(inv.inventory,slot,0,inst);
	logga("add to new slot "+string(slot));
}

// refresh

with (obj_playerInventory)
{
	event_perform(ev_other,ev_user0);
}

with (obj_mission)
{
	event_perform(ev_other,ev_user1);
}
scr_updateRecipes();
if(global.isSurvival){
	scr_storeItemsSurvival();
}
return true;
