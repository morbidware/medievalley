crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	
	// execute action if conditions are met
	if (event_perform_ret(targetobject, ev_other, ev_user1) == true)
	{
		if (global.online && targetobject.canSendNetEvents)
		{
			eventPerformInteractable(targetobject.x,targetobject.y,targetobject,targetobject.object_index,ev_other,ev_user2);
			
		}
		if(global.isSurvival && global.online)scr_storeOneInterctableSurvival(targetobject.id);
		with(targetobject)
		{
			event_perform(ev_other, ev_user0);
		}
		
	}
	global.saveitemstimer = 0;
	if(!global.isSurvival)scr_storeAllInsocket();
}