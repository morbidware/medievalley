///@function scr_assignWardrobePartDirect(id,type)
///@param id
///@param type

// Directly assigns the selected wardrobe part without making any check.
// Used when loading a wardrobe setup that was already saved before.

var index = argument0;
var type = argument1;
var part = ds_list_create();

var player = instance_find(obj_char,0);

switch (type)
{
	case "body":
		ds_list_read(part, ds_list_find_value(global.wardrobe_body, index));
		player.custom_body = ds_list_find_value(part,0);
		player.custom_body_object = part;
		player.custom_body_index = index;
		break;
		
	case "head":
		ds_list_read(part, ds_list_find_value(global.wardrobe_head, index));
		player.custom_head= ds_list_find_value(part,0);
		player.custom_head_object = part;
		player.custom_head_index = index;
		break;
		
	case "upper":
		ds_list_read(part, ds_list_find_value(global.wardrobe_upper, index));
		player.custom_upper = ds_list_find_value(part,0);
		player.custom_upper_object = part;
		player.custom_upper_index = index;
		var mask = ds_list_find_value(player.custom_upper_object,4);
		if (string_char_at(mask,0) == "1") {
			renderhead = false;
		}
		else {
			renderhead = true;
		}
		if (string_char_at(mask,1) == "1")
		{
			renderlower = false;
		}
		else
		{
			renderlower = true;
		}
		break;
		
	case "lower":
		ds_list_read(part, ds_list_find_value(global.wardrobe_lower, index));
		player.custom_lower = ds_list_find_value(part,0);
		player.custom_lower_object = part;
		player.custom_lower_index = index;
		break;
}
