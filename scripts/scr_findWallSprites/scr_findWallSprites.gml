with(obj_wall)
{
	if(!hasBottom)
	{
		tind = choose(0,1);
		bind = -1;
	}
	
	if(!hasLeft && hasRight && hasTop && hasBottom)
	{
		tind = 4;
		bind = 2;
	}
	
	if(hasLeft && !hasRight && hasTop && hasBottom)
	{
		tind = 5;
		bind = 3;
	}
	
	if(hasTop && hasLeft && hasBottom && hasRight)
	{
		sprbot = spr_column;
		tind = -1;
		bind = choose(0,1);
		sprite_index = spr_column
		image_index = bind;
	}
}