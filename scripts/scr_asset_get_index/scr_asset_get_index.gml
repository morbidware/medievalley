///@function scr_asset_get_index(name);
///@param name
var res = ds_map_find_value(global.assets,argument0);

//show_debug_message("MAP SIZE: " + string(ds_map_size(global.assets)));
if(is_undefined(res))
{
	//show_debug_message(string(res));
	//show_debug_message("MISSING ASSET IN MAP: " + argument0);
	return spr_missing;
}
else
{
	
	//show_debug_message("FOUND ASSET IN MAP: " + argument0);
	return res;
}
