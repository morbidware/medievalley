///@function scr_draw_text_ext(x, y, text, sep, w);
///@description This function works like the standard one, but works in HTML too.
///@param x
///@param y
///@param text
///@param sep
///@param w
var px = argument0;
var py = argument1;
var text = argument2;
var sep = argument3;
var w = argument4;

var startindex = 0;
var lines = 0;
var breakindex = 0;
var breaklength = 0;
for (var i = 1; i < string_length(text); i++) {
	
	var substring = string_copy(text, startindex, i-startindex);
	if (string_char_at(text, i) == " ")
	{
		breakindex = i+1;
		breaklength = i-startindex;
	}
	
	if (string_width(substring) >= w)
	{
		draw_text(px, py + (lines * sep), string_copy(text, startindex, breaklength));
		startindex = breakindex;
		i = startindex;
		lines++;
	}
	if (i >= string_length(text)-1)
	{
		draw_text(px, py + (lines * sep), string_copy(text, startindex, i-startindex+2));
	}
}