///@description gmcallback_removeInteractable
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while(pos)
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = real(ds_list_find_value(res,0));
var yy = real(ds_list_find_value(res,1));
var pid = string(ds_list_find_value(res,2));

ds_list_destroy(res);
logga("______________^^^^^^^^^^^^^^^^^^^^^^^______________");
var plist = ds_priority_create();
logga("instance number obj_interactable " + string(instance_number(obj_interactable)));
for(var i = 0; i < instance_number(obj_interactable); i++)
{
	var pick = instance_find(obj_interactable,i);
	logga("pick is " + pick.itemid );
	if(pick.itemid == pid)
	{
		logga("ADDING " + pick.itemid);
		ds_priority_add(plist,pick,point_distance(pick.x,pick.y,xx,yy));
	}
}
logga("finished doing my priority list");
var nearestPickable = ds_priority_delete_min(plist);
logga("NOW GETTING NEAREST PICKABLE " + string(nearestPickable));
ds_priority_destroy(plist);
if(nearestPickable != noone)
{
	logga("NEAREST PICKABLE IS != NOONE " + nearestPickable.itemid);
	instance_destroy(nearestPickable);
}
logga("______________^^^^^^^^^^^^^^^^^^^^^^^______________");
/*
logga("?????????????????????????????? gmcallback_removeInteractable");
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var xx = real(ds_list_find_value(res,0));
var yy = real(ds_list_find_value(res,1));
var oid = real(ds_list_find_value(res,2));

ds_list_destroy(res);

with(instance_nearest(xx,yy,oid))
{
	logga("destroy INST");
	instance_destroy();
}

logga("?????????????????????????????? gmcallback_removeInteractable END");
*/