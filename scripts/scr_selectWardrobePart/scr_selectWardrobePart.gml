///@function scr_selectWardrobePart(id,next,type)
///@param id
///@param next (or previous)
///@param type

// If id is -1, choose the previous or next wardrobe part for specified type.
// If id is 0 or higher, choose the specified wardrobe part for the specified type

var index = argument0;
var next = argument1;
var type = argument2;

var toplimit = 0;
var player = instance_find(obj_char,0);
var list = ds_list_create();

switch (type)
{
	case "body": ds_list_copy(list,global.wardrobe_body); break;
	case "head": ds_list_copy(list,global.wardrobe_head); break;
	case "upper": ds_list_copy(list,global.wardrobe_upper); break;
	case "lower": ds_list_copy(list,global.wardrobe_lower); break;
}
toplimit = ds_list_size(list)-1;

// Id is -1 so choose previous or next wardrobe part
if (index < 0)
{
	switch (type)
	{
		case "body": index = player.custom_body_index; break;
		case "head": index = player.custom_head_index; break;
		case "upper": index = player.custom_upper_index; break;
		case "lower": index = player.custom_lower_index; break;
	}
	
	// Cycle selected index
	if (next == true)
	{
		index++;
	}
	else
	{
		index--;
	}
	if (index > toplimit)
	{
		index = 0;
	}
	if (index < 0)
	{
		index = toplimit;
	}
	
	switch (type)
	{
		case "body": player.custom_body_index = index; break;
		case "head": player.custom_head_index = index; break;
		case "upper": player.custom_upper_index = index; break;
		case "lower": player.custom_lower_index = index; break;
	}
}

// Read part

var part = ds_list_create();
ds_list_read(part,ds_list_find_value(list,index));
var partgender = ds_list_find_value(part,3);

//show_debug_message("--- Trying set part "+ds_list_find_value(part,0));

// If gender mismatch, continue reading parts until a suitable one is found
if (partgender != "mf" && player.gender != partgender)
{
	scr_selectWardrobePart(-1, next, type);
	return;
}

// Load the requested part and occlusion masks
switch (type)
{
	case "body":
		player.custom_body = ds_list_find_value(part,0);
		player.custom_body_object = part;
		break;
	case "head":
		player.custom_head = ds_list_find_value(part,0);
		player.custom_head_object = part;
		break;
	case "upper":
		player.custom_upper = ds_list_find_value(part,0);
		player.custom_upper_object = part;
		var mask = ds_list_find_value(player.custom_upper_object,4);
		if (string_char_at(mask,0) == "1") {
			renderhead = false;
		}
		else {
			renderhead = true;
		}
		if (string_char_at(mask,1) == "1")
		{
			renderlower = false;
		}
		else
		{
			renderlower = true;
		}
		break;
	case "lower":
		player.custom_lower = ds_list_find_value(part,0);
		player.custom_lower_object = part;
		break;
}

ds_list_clear(list);

//show_debug_message("--- SET PART "+ds_list_find_value(part,0));