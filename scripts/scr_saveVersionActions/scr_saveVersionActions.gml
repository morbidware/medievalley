///@function scr_saveVersionActions()
///@descriptions Execute actions to align save data to latest version
var thisVersion = 1;
while (global.saveversion < thisVersion)
{
	switch (global.saveversion)
	{
		case 0:  // v0.4.0.0 - Players receive a farmboard when they finish all the missions
		if (!instance_exists(obj_interactable_farmboard) && !instance_exists(obj_mission) && ds_list_find_index(global.mailIndices, 3) >= 0)
		{
			instance_create_depth(0,0,0,obj_reward_0008);
		}
	}
	global.saveversion++;
}