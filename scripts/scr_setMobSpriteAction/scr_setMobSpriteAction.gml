///@function scr_setMobSpriteAction(mobtype,action,lastdirection,animtime)
///@param mobtype
///@param action
///@param lastdirection
///@param animtime

//sprite_index = scr_asset_get_index("spr_"+string(argument0)+"_"+string(argument1));

switch(argument2)
{
	case Direction.Up:
	image_xscale = 1;
	sprite_index = scr_asset_get_index("spr_"+string(argument0)+"_"+string(argument1)+"_up");
	break;
	
	case Direction.Down:
	image_xscale = 1;
	sprite_index = scr_asset_get_index("spr_"+string(argument0)+"_"+string(argument1)+"_down");
	break;
	
	case Direction.Left:
	image_xscale = -1;
	sprite_index = scr_asset_get_index("spr_"+string(argument0)+"_"+string(argument1)+"_right");
	break;
	
	case Direction.Right:
	image_xscale = 1;
	sprite_index = scr_asset_get_index("spr_"+string(argument0)+"_"+string(argument1)+"_right");
	break;
}

if (argument3 <= 0)
{
	image_speed = animSpeed * global.dt;
}
else
{
	image_speed = image_number / argument3 * global.dt;
}