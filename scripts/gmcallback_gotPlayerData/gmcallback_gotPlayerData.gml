///@description gmcallback_gotPlayerData
if(global.isSurvival)
{
	gmcallback_gotPlayerDataSurvival(argument0);
	exit;
}
logga("gotPlayerData");
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);
var usrname = string(ds_list_find_value(res,0));
var usrid = real(ds_list_find_value(res,1));
var socketid = string(ds_list_find_value(res,2));
var userdata = string(ds_list_find_value(res,3));
var userstats = string(ds_list_find_value(res,4));
var deathdatastring = string(ds_list_find_value(res,5));
deathdatastring = string_replace_all(deathdatastring,"_","00000000");
deathdatastring = string_replace_all(deathdatastring,"%","303030303030");

var farmstring = string(ds_list_find_value(res,6));
farmstring = scr_uncompressFarmString(farmstring);

var pickablesstring = string(ds_list_find_value(res,7));

pickablesstring = string_replace_all(pickablesstring,"_","000000");
pickablesstring = string_replace_all(pickablesstring,"%","A666C696E743A313A3001");

var interactablesstring = string(ds_list_find_value(res,8));

interactablesstring = string_replace_all(interactablesstring,"_","000000");
interactablesstring = string_replace_all(interactablesstring,"%","303A313A313A303A3");
interactablesstring = string_replace_all(interactablesstring,"!","70696E653A3");
interactablesstring = string_replace_all(interactablesstring,"&","77696C6467726173733A");
interactablesstring = string_replace_all(interactablesstring,",","6F626A5F696E74657261637461626C655F");

var itemsstring = string(ds_list_find_value(res,9));

var missionsstring = string(ds_list_find_value(res,10));

var unlockablesstring = string(ds_list_find_value(res,11));

var housepickablesstring = string(ds_list_find_value(res,12));

housepickablesstring = string_replace_all(housepickablesstring,"_","000000");
housepickablesstring = string_replace_all(housepickablesstring,"%","A666C696E743A313A3001");

var houseinteractablesstring = string(ds_list_find_value(res,13));

houseinteractablesstring = string_replace_all(houseinteractablesstring,"_","000000");
houseinteractablesstring = string_replace_all(houseinteractablesstring,"%","303A313A313A303A3");
houseinteractablesstring = string_replace_all(houseinteractablesstring,"!","70696E653A3");
houseinteractablesstring = string_replace_all(houseinteractablesstring,"&","77696C6467726173733A");
houseinteractablesstring = string_replace_all(houseinteractablesstring,",","6F626A5F696E74657261637461626C655F");

ds_list_destroy(res);

global.username = usrname;
global.userid = usrid;
global.farmid = usrid; 
global.socketid = socketid;
global.gridstring = farmstring;//this is uncompressed so it's a ds_grid_write (a string to read later)
global.pickables = pickablesstring;
global.interactables = interactablesstring;
global.items = itemsstring;
global.missions = missionsstring;
global.userdata = userdata;
global.housepickables = housepickablesstring;
global.houseinteractables = houseinteractablesstring;
global.lastjoinedroom = global.socketid;
var tempUnlockables = ds_map_create();
ds_map_read(tempUnlockables, unlockablesstring);
if(ds_map_size(tempUnlockables) > 0)
{
	logga("ds_map_size(tempUnlockables) > 0");
	ds_map_read(global.unlockables, unlockablesstring);
}

var data = ds_list_create();
ds_list_read(data, deathdatastring);
if(ds_list_size(data) >= 1)
{
	global.lastdeathcell = ds_list_find_value(data,0);
	logga("global.lastdeathcell = " + string(global.lastdeathcell));
}
if(ds_list_size(data) >= 2)
{
	global.lastdeathx = ds_list_find_value(data,1);
	logga("global.lastdeathx = " + string(global.lastdeathx));
}
if(ds_list_size(data) >= 3)
{
	global.lastdeathy = ds_list_find_value(data,2);
	logga("global.lastdeathy = " + string(global.lastdeathy));
}
if(ds_list_size(data) >= 4)
{
	global.lastdeathinventory = ds_grid_create(10,3);
	ds_grid_read(global.lastdeathinventory, ds_list_find_value(data,3));
	logga("global.lastdeathinventory = " + string(global.lastdeathinventory));
}

scr_parseUserstats(userstats);
/*
for (var i = 0; i < ds_grid_width(global.lastdeathinventory); i++)
{
	var a = string(ds_grid_get(global.lastdeathinventory,i,0));
	var b = string(ds_grid_get(global.lastdeathinventory,i,1));
	var c = string(ds_grid_get(global.lastdeathinventory,i,2));
	logga("LIST ENTRY "+a+", "+b+", "+c);
}
*/

//logga("HO RICEVUTO LE CREDENZIALI --------------------");
//logga("global.username = " + global.username);
//logga("global.userid = " + global.userid);
//logga("global.socketid = " + global.socketid);

//logga("global.missions = " + global.missions);

//logga("global.gridstring = " + global.gridstring);
//logga("global.pickables = " + global.pickables);
//logga("global.interactables = " + string_copy(global.interactables,1,10)+"...");
//logga("global.userdata = " + global.userdata);
//logga("global.items = " + global.items);
//logga("unlockables = " + unlockablesstring);
//logga("global.unlockables = " + global.unlockables);

var k = ds_map_find_first(global.unlockables);
var v = ds_map_find_value(global.unlockables,k);
var c = 1;
var l = ds_map_size(global.unlockables);
while(c < l)
{
	logga("key: "+string(k)+ " value: " + string(v));
	k = ds_map_find_next(global.unlockables,k);
	var v = ds_map_find_value(global.unlockables,k);
	c ++;
}

//logga("ui_inventory --> " + string(ds_map_find_value(global.unlockables,"ui_inventory")));
if(!global.isChallenge)
{
	with(obj_loading_panel)
	{
		state = 1;
	}
	room_goto(roomFarm);
}
else
{
	if(global.newUserForChallenge)
	{
		checkIfPlayerCanPlayChallenge(global.userigid,"check");
	}
	else
	{
		checkIfPlayerCanPlayChallenge(global.userigid,"check");
	}
}
logga("gotPlayerData");