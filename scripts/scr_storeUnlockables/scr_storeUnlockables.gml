if(global.isSurvival)
{
	exit;
}
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
if (global.online)
{
	//sendUnlockablesString(global.userid, str, global.username);
	//logga("SCR_STORE_UNLOCKABLES: " + ds_map_write(global.unlockables));
	logga("SCR_STORE_UNLOCKABLES");
	var mapstring = ds_map_write(global.unlockables);
	if(mapstring != global.lastSavedUnlockables)
	{
		global.lastSavedUnlockables = mapstring;
		sendUnlockablesString(global.userid, global.lastSavedUnlockables, global.username);
	}	
	else
	{
		logga("skipping store unlockables");
	}
	gmcallback_updateLastSave();
}
global.uisavestatus.saving = false;