///@description gmcallback_gotUserData
with(obj_debugger)
{
	str += "scr_updateUserSurvival\n";
}
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var farmstring = string(ds_list_find_value(res,0));
farmstring = scr_uncompressFarmString(farmstring);

var pickablesstring = string(ds_list_find_value(res,1));
pickablesstring = string_replace_all(pickablesstring,"_","000000");
pickablesstring = string_replace_all(pickablesstring,"%","A666C696E743A313A3001");

var interactablesstring = string(ds_list_find_value(res,2));
interactablesstring = string_replace_all(interactablesstring,"_","000000");
interactablesstring = string_replace_all(interactablesstring,"%","303A313A313A303A3");
interactablesstring = string_replace_all(interactablesstring,"!","70696E653A3");
interactablesstring = string_replace_all(interactablesstring,"&","77696C6467726173733A");
interactablesstring = string_replace_all(interactablesstring,",","6F626A5F696E74657261637461626C655F");

ds_list_destroy(res);

global.gridstring = farmstring;
global.pickables = pickablesstring;
global.interactables = interactablesstring;