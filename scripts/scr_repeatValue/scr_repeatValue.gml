///@function scr_repeatValue(value,max)
///@param value
///@param max
var value = abs(argument0);

while (value > argument1)
{
	value -= argument1;
}

return value;
