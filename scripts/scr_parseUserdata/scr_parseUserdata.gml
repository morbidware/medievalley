///@function scr_parseUserdata()
///@argument char_id
logga("scr_parseUserdata()");
var char = argument0;
var str = global.userdata; 
logga(str);
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
	pos -= 1;
	ds_list_add(res, string_copy(str, 1, pos));
	str = string_delete(str, 1, pos + dellen);
	pos = string_pos(del, str);
}
ds_list_add(res, str);



if(global.isChallenge)
{
	//get life from challenge data
	//set stamina at 100
	global.charLife = ds_map_find_value(global.challengeData,"player_life");
	global.charStamina = 100;
	char.life = global.charLife;
	char.stamina = global.charStamina;
}
else
{
	//get life 
	global.charLife = real(ds_list_find_value(res,0));
	global.charStamina = real(ds_list_find_value(res,1));
	char.life = global.charLife;
	char.stamina = global.charStamina;
}
char.gold = real(ds_list_find_value(res,2));
char.galasilver = real(ds_list_find_value(res,3));
if (ds_list_size(res) > 4)
{
	global.musicvolume = real(ds_list_find_value(res,4));
	audio_group_set_gain(audiogroup_music, global.musicvolume, 0);
}
if (ds_list_size(res) > 5)
{
	global.sfxvolume = real(ds_list_find_value(res,5));
	audio_group_set_gain(audiogroup_sfx, global.sfxvolume, 0);
}
if (ds_list_size(res) > 6)
{
	var mailmap = ds_map_create();
	ds_map_read(mailmap, string(ds_list_find_value(res,6)));
	var key = ds_map_find_first(mailmap);
	repeat (ds_map_size(mailmap))
	{
		scr_addMail(real(key), real(ds_map_find_value(mailmap, key)), false, 0);
		key = ds_map_find_next(mailmap, key);
	}
}
if (ds_list_size(res) > 7)
{
	 ds_map_read(global.craftedobjects, string(ds_list_find_value(res,7)));
}
if (ds_list_size(res) > 8)
{
	var str = string(ds_list_find_value(res,8));
	str = string_replace_all(str,"_","000000");;
	var tempward = ds_list_create();
	ds_list_read(tempward,str);
	if(ds_list_size(tempward) > 0)
	{
		logga("tempward size" + string(ds_list_size(tempward)));
		ds_list_read(global.currentwardrobe,str)
	}
	ds_list_destroy(tempward);
}
if (ds_list_size(res) > 9)
{
	global.saveversion = round(real(ds_list_find_value(res,9)));
}
if (ds_list_size(res) > 10)
{
	global.allowvisitors = round(real(ds_list_find_value(res,10)));
}
if (ds_list_size(res) > 11)
{
	global.lastChallengeDone = round(real(ds_list_find_value(res,11)));
}
if (ds_list_size(res) > 12)
{
	global.graphicsquality = round(real(ds_list_find_value(res,12)));
}
if (ds_list_size(res) > 13)
{
	global.showhints = round(real(ds_list_find_value(res,13)));
}
if (ds_list_size(res) > 14)
{
	global.totslots = round(real(ds_list_find_value(res,14)));
	with(obj_playerInventory)
	{
		slots = global.totslots;
		ds_grid_resize(inventory,global.totslots,1);
	}
}
ds_list_destroy(res);
logga("-------------------------");