///@function scr_anim_watering(object,direction,crono,maxcrono,steps,sidesprite,frontsprite)
///@param object the object
///@param direction direction of the player
///@param crono current crono timer for animation
///@param maxcrono total crono of this animation
///@param steps frames for the animation
///@param side side graphics of the tool
///@param front side graphics of the tool

var t = 1-((argument2-1)/argument3);
var a;
var t2 = (ceil(t * argument4) / argument4) * 100;
var t3 = (ceil(t * argument4) / argument4);

switch(argument1)
{
	case Direction.Right:
	a = 90 - t2;
	x = argument0.x + cos(degtorad(a)) * 6;
	y = argument0.y + 3 - sin(degtorad(a)) * 6;
	image_angle = -(t2/3);
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Left:
	a = 270 + t2;
	x = argument0.x - cos(degtorad(a)) * 6;
	y = argument0.y + 3 + sin(degtorad(a)) * 6;
	image_angle = t2/3;
	if (argument5 != noone)
		sprite_index = argument5;
	if (global.game.player.gender == "f")
		y += 2;
	break;
	
	case Direction.Up:
	x = argument0.x;
	y = argument0.y - 3 + (t3 * 10);
	image_angle = 180;
	if (argument6 != noone)
		sprite_index = argument6;
	break;
	
	case Direction.Down:
	x = argument0.x;
	y = argument0.y - 3 + (t3 * 10);
	image_angle = 0;
	if (argument6 != noone)
		sprite_index = argument6;
	break;
}
y -= 16;
if(global.online)
{
	if(argument0.visible)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,image_xscale,image_yscale);
	}
	if(t >= 1.0)
	{
		drawEquipped(argument0.socketid,round(x),round(y),round(image_angle),sprite_index,image_index,0.0,0.0);
	}
}