///@function scr_heroGetDamage(heroid,quantity)
///@param heroid
///@param quantity
var hero = argument0;
var quantity = argument1;

quantity -= hero.defence;
if(quantity <= 0) quantity = 1;

hero.life -= quantity;
if(hero.life > 0)
{
	scr_setState(hero, ActorState.Hit);
	audio_play_sound(choose(snd_get_hit_male_1,snd_get_hit_male_2,snd_get_hit_male_3),0,0);
	
	with(obj_wearable)
	{
		
		durability -= lossperusage;

		if(durability <= 0)
		{
			audio_play_sound(snd_breakTool,0,false);
			var inventory = instance_find(obj_playerInventory,0);
			ds_grid_set(inventory.inventory,wear.slot,0,-1);
			instance_destroy(id);
		}
	}
}
else
{
	scr_setState(hero, ActorState.Die);
}
global.saveuserdatatimer = 0;
/*
if(room == !roomChallenge)
{
	var dmg_fx = instance_create_depth(hero.x,hero.y-(sprite_get_height(hero.sprite_index)/2),hero.depth-1,obj_fx_damage);
	dmg_fx.col = c_white;
	dmg_fx.str = "-"+string(quantity);
}
*/



