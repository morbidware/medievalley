///@function scr_loseEquippedQuantity(amount)
///@param quantity to lose
with(obj_char)
{
	var equipped = instance_nearest(x,y,obj_equipped);
	
	for (var i = 0; i < instance_number(obj_inventory); i++)
	{
		var inv = instance_find(obj_inventory,i);
		if(inv.slot == equipped.myslot)
		{
			inv.quantity -= argument0;
			if(inv.quantity <= 0)
			{
				instance_destroy(inv);
				ds_grid_set(inventory.inventory,equipped.myslot,0,-1);
				with(instance_nearest(other.x,other.y,obj_equipped))
				{
					instance_destroy();
				}
				handState = HandState.Empty;
			}
			break;
		}
	}
}