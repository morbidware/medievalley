///@description this cb is called when the player has successfully loaded the farm data from another user to visit the farm
if (global.timeout <= 0)
{
	exit;
}
var str = argument0; 
logga(str);
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var usrname = string(ds_list_find_value(res,0));
var usrid = real(ds_list_find_value(res,1));
var socketid = string(ds_list_find_value(res,2));
var farmstring = string(ds_list_find_value(res,3));

farmstring = scr_uncompressFarmString(farmstring);
//farmstring = string_replace_all(farmstring,"_","00000000000000000000000");
//farmstring = string_replace_all(farmstring,"%","0000000000");

var pickablesstring = string(ds_list_find_value(res,4));

pickablesstring = string_replace_all(pickablesstring,"_","000000");
pickablesstring = string_replace_all(pickablesstring,"%","A666C696E743A313A3001");

var interactablesstring = string(ds_list_find_value(res,5));

interactablesstring = string_replace_all(interactablesstring,"_","000000");
interactablesstring = string_replace_all(interactablesstring,"%","303A313A313A303A3");
interactablesstring = string_replace_all(interactablesstring,"!","70696E653A3");
interactablesstring = string_replace_all(interactablesstring,"&","77696C6467726173733A");
interactablesstring = string_replace_all(interactablesstring,",","6F626A5F696E74657261637461626C655F");

ds_list_destroy(res);

global.otherusername = usrname;
global.otheruserid = usrid;
global.otherfarmid = usrid; 
global.othersocketid = socketid;
global.othergridstring = farmstring;
global.otherpickables = pickablesstring;
global.otherinteractables = interactablesstring;

/*
logga("HO RICEVUTO LE CREDENZIALI --------------------");
logga("global.username = " + global.username);
logga("global.otherusername = " + global.otherusername);
logga("global.userid = " + global.userid);
logga("global.otheruserid = " + global.otheruserid);
logga("global.socketid = " + global.socketid);
logga("global.othersocketid = " + global.othersocketid);
logga("global.gridstring = " + string(string_length(global.gridstring)));
logga("global.othergridstring = " + string(string_length(global.othergridstring)));
logga("global.pickables = " + string(string_length(global.pickables)));
logga("global.otherpickables = " + string(string_length(global.otherpickables)));
logga("global.interactables = " + string(string_length(global.interactables)));
logga("global.otherinteractables = " + string(string_length(global.otherinteractables)));
logga("-----------------------------------------------");
*/

with(instance_create_depth(0,0,0,obj_fadeout))
{
	gotoroom = roomFarm;
}