///@function scr_compressFarmString
var grid = argument0;
var str = "";
for(var r = 0; r < ds_grid_height(grid); r++)
{
	for(var c = 0; c < ds_grid_width(grid); c++)
	{
		str += string(ds_grid_get(grid,c,r));
		str += ":";
	}
}
return string_copy(str,1,string_length(str)-1);