///@function gmcallback_gotChallengeData(string)
///@param date
// format is YYYY-MM-DD
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

ds_map_add(global.challengeData,"good_number",real(ds_list_find_value(res,0)));
ds_map_add(global.challengeData,"action_frames_normal",real(ds_list_find_value(res,1)));
ds_map_add(global.challengeData,"action_frames_wrong",real(ds_list_find_value(res,2)));
ds_map_add(global.challengeData,"action_frames_pain",real(ds_list_find_value(res,3)));
ds_map_add(global.challengeData,"player_damage",real(ds_list_find_value(res,4)));
ds_map_add(global.challengeData,"player_life",real(ds_list_find_value(res,5)));
ds_map_add(global.challengeData,"enemy_damage",real(ds_list_find_value(res,6)));
ds_map_add(global.challengeData,"enemy_life",real(ds_list_find_value(res,7)));	
ds_map_add(global.challengeData,"grow_crono",real(ds_list_find_value(res,8)));

global.challengeGoodNumber = real(ds_list_find_value(res,0));
global.charLife = real(ds_list_find_value(res,5));
logga("LIFE: global.charLife = "+string(global.charLife)+" gmcallback_gotChallengeData 31");
global.charStamina = 100;

ds_list_destroy(res);

logga("-- global.challengeData --------------------");
var k = ds_map_find_first(global.challengeData);
repeat(ds_map_size(global.challengeData))
{
	var v = ds_map_find_value(global.challengeData,k);
	logga(k+" "+string(v));
	k = ds_map_find_next(global.challengeData,k);
}
logga("-------------------------------------------");
