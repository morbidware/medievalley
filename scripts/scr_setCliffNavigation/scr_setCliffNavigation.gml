///@function scr_setCliffNavigation()

// detect all cliffs
c = 0;
r = 0;
repeat(cols)
{
	r = 0;
	repeat(rows)
	{
		if (ds_grid_get(spritegrid,c,r) == spr_ground_cliff_tiles || ds_grid_get(spritegrid,c,r) == spr_ground_cliff_variation){
			
			// fill pathfinding grid depending by sprite index
			switch (ds_grid_get(indexgrid,c,r))
			{
				// inward angles
				case 1: mp_grid_add_cell(pathgrid, c, r); break;
				case 2: mp_grid_add_cell(pathgrid, c, r); break;
				case 4: mp_grid_add_cell(pathgrid, c, r); break;
				case 8: mp_grid_add_cell(pathgrid, c, r); break;
				
				// outward angles
				case 34: mp_grid_add_cell(pathgrid, c, r); break;
				case 36: mp_grid_add_cell(pathgrid, c, r); break;
				case 38: mp_grid_add_cell(pathgrid, c, r);
						 mp_grid_add_cell(pathgrid, c, r+1);
						 mp_grid_add_cell(pathgrid, c, r+2);
						 mp_grid_add_cell(pathgrid, c, r+3); break;
				case 40: mp_grid_add_cell(pathgrid, c, r);
						 mp_grid_add_cell(pathgrid, c, r+1);
						 mp_grid_add_cell(pathgrid, c, r+2);
						 mp_grid_add_cell(pathgrid, c, r+3); break;
				
				// diagonals
				case 49: mp_grid_add_cell(pathgrid, c, r);
						 mp_grid_add_cell(pathgrid, c, r+1);
						 mp_grid_add_cell(pathgrid, c, r+2);
						 mp_grid_add_cell(pathgrid, c, r+3); break;
				case 50: mp_grid_add_cell(pathgrid, c, r);
						 mp_grid_add_cell(pathgrid, c, r+1);
						 mp_grid_add_cell(pathgrid, c, r+2);
						 mp_grid_add_cell(pathgrid, c, r+3); break;
				case 53: mp_grid_add_cell(pathgrid, c, r); break;
				case 54: mp_grid_add_cell(pathgrid, c, r); break;
				
				// sides
				case 16: mp_grid_add_cell(pathgrid, c, r); break;
				case 20: mp_grid_add_cell(pathgrid, c, r); break;
				case 24: mp_grid_add_cell(pathgrid, c, r); break;
				case 28: mp_grid_add_cell(pathgrid, c, r);
						 mp_grid_add_cell(pathgrid, c, r+1);
						 mp_grid_add_cell(pathgrid, c, r+2);
						 mp_grid_add_cell(pathgrid, c, r+3); break;
			}
		}
		r++;
	}
	c++;
}