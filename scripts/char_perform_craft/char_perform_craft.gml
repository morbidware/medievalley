if (lastdirection == Direction.Up || lastdirection = Direction.Left)
{
	scr_setSpriteAction("crafting",Direction.Left,-1);
}
else
{
	scr_setSpriteAction("crafting",Direction.Right,-1);
}

crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	scr_setState(id,ActorState.Idle);
	
	var inst = scr_gameItemCreate(x, y, obj_pickable, craftingid);
	scr_putItemIntoInventory(inst,InventoryType.Inventory,-1);
	instance_destroy(inst);
	scr_storeItems();
	craftingid = "";
}
