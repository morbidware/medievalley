///@function scr_getItemValueForDurability(itemid,basevalue,durability)
///@param itemid
///@param basevalue
///@param durability
var itemid = argument0;
var basevalue = argument1;
var durability = argument2;
if (itemid != "axe" &&
	itemid != "pickaxe" &&
	itemid != "shovel" &&
	itemid != "hoe" &&
	itemid != "hammer" &&
	itemid != "wateringcan" &&
	itemid != "spear" &&
	itemid != "sword" &&
	itemid != "sickle")
{
	return basevalue;
}

return floor (basevalue * (durability / 100.0));
