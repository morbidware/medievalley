///@function scr_storeItems()
if(global.isSurvival)
{
	scr_storeItemsSurvival();
	exit;
}
logga("scr_storeItems()");
if (!scr_allowedToSave()) { return; }

if(room == roomChallenge)
{
	exit;
}

//ds_list_clear(global.interactables);
global.uisavestatus.saving = true;
with(obj_debugger)
{
	str += "scr_storeItems\n";
}
var itemslist = ds_list_create();
//logga("There are " + string(instance_number(obj_inventory)) + " items");
for(var i = 0; i < instance_number(obj_inventory); i++)
{
	var p = instance_find(obj_inventory,i);
	//logga("found an item " + string(i) + " " + p.itemid);
	var storestr = p.itemid+":"+string(p.quantity)+":"+string(p.slot)+":"+string(p.durability)+":"+string(0);
	ds_list_add(itemslist,storestr);
}

if (global.online)
{
	var itemsstring = ds_list_write(itemslist);
	if(itemsstring != global.lastSavedItems)
	{
		global.lastSavedItems = itemsstring;
		global.items = global.lastSavedItems;
		sendItemsString(global.userid, ds_list_write(itemslist), global.username);
	}
	else
	{
		logga("skipping store items");
	}
	gmcallback_updateLastSave();
}
else
{
	global.items = ds_list_write(itemslist);
	//sendItemsString(global.userid, ds_list_write(itemslist), global.username);
	gmcallback_updateLastSave();
}
ds_list_destroy(itemslist);
global.uisavestatus.saving = false;
logga("scr_storeItems() end");