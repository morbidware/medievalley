///@function scr_updateState
///@description update Hero
///@param hero_id
with(argument0)
{
	switch(state)
	{
		case ActorState.Idle:
		char_perform_idle();
		break;
		case ActorState.Eat:
		char_perform_eat();
		break;
		case ActorState.Attack:
		char_perform_attack();
		break;
		case ActorState.Hit:
		char_perform_hit();
		break;
		case ActorState.Die:
		char_perform_die();
		break;
		case ActorState.Pick:
		char_perform_pick();
		break;
		case ActorState.PickUp:
		char_perform_pickup();
		break;
		case ActorState.Chop:
		char_perform_chop();
		break;
		case ActorState.Craft:
		char_perform_craft();
		break;
		case ActorState.Hoe:
		char_perform_hoe();
		break;
		case ActorState.Sow:
		char_perform_sow();
		break;
		case ActorState.Watering:
		char_perform_watering();
		break;
		case ActorState.Shovel:
		char_perform_shovel();
		break;
		case ActorState.Charge:
		char_perform_charge();
		break;
		case ActorState.Sickle:
		char_perform_sickle();
		break;
		case ActorState.Mine:
		char_perform_mine();
		break;
		case ActorState.Build:
		char_perform_build();
		break;
		case ActorState.Rest:
		char_perform_rest();
		break;
		case ActorState.Rebuild:
		char_perform_rebuild();
		break;
		case ActorState.Cook:
		char_perform_cook();
		break;
		case ActorState.Hammer:
		char_perform_hammer();
		break;
		case ActorState.Fish:
		char_perform_fish();
		break;
	}
}