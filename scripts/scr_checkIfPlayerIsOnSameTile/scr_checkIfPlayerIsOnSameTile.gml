var player = instance_find(obj_char,0);
if(floor(player.x/16) != player.highlightCol || floor(player.y/16) != player.highlightRow)
{
	return false;
}
else
{
	return true;
}