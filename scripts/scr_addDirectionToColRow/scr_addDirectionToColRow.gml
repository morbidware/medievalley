///@function scr_addDirectionToColRow(col,row,dir)
///@description Returns an array with Col and Row updated depending by the Direction provided
///@param col
///@param row
///@param dir
var col = argument0;
var row = argument1;
var dir = argument2;

var arr = array_create(2,noone);
arr[0] = col;
arr[1] = row;
if (dir == Direction.Right)
{
	arr[0] = arr[0]+1;
}
else if (dir == Direction.UpRight)
{
	arr[0] = arr[0]+1;
	arr[1] = arr[1]-1;
}
else if (dir == Direction.Up)
{
	arr[1] = arr[1]-1;
}
else if (dir == Direction.UpLeft)
{
	arr[0] = arr[0]-1;
	arr[1] = arr[1]-1;
}
else if (dir == Direction.Left)
{
	arr[0] = arr[0]-1;
}
else if (dir == Direction.DownLeft)
{
	arr[0] = arr[0]-1;
	arr[1] = arr[1]+1;
}
else if (dir == Direction.Down)
{
	arr[1] = arr[1]+1;
}
else if (dir == Direction.DownRight)
{
	arr[0] = arr[0]+1;
	arr[1] = arr[1]+1;
}	
return arr;