global.visitorscount = argument0;
if(global.visitorscount == 1)
{
	if(global.mobowner != 1)
	{
		global.mobowner = 1;
		var gls = instance_find(obj_gameLevelSurvival,0);
		with(gls)
		{
			event_perform(ev_other,ev_user4);
		}
		if(string_length(global.pickables) <= 0)
		{
			scr_populate_with(obj_pickable,"flint",16,GroundType.Soil,false,true);
			scr_populate_with(obj_pickable,"redbeansseeds",5,GroundType.Grass,false,true);
			scr_storePickablesSurvival();
		}
		
	}

}
/*if(room == roomSurvival)
{
	exit;
}

if (global.lastjoinedroom == global.socketid && global.othersocketid == "")
{
	global.visitorscount--;
}
if (global.allowvisitors == 0 || global.othersocketid != "" || room == roomChallenge || room == roomMain || room == roomInit)
{
	global.visitorscount = -1;
}
sendNumVisitorsString(global.socketid, string(global.visitorscount), global.username);*/