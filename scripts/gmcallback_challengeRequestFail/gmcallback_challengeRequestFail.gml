///@function gmcallback_challengeRequestFail(str)
///@param str
var str = argument0; 
var del = ";";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var txt = ds_list_find_value(res,0);
var url = ds_list_find_value(res,1);

var letter = instance_create_depth(0,0,0,obj_letter);
letter.title = "Error notice from the King";
letter.body = txt;
letter.signature = "King Thistle";
letter.willRedirect = true;
letter.redirectURL = url;
letter.ctaText = "Ok";
letter.ctaURL = letter.redirectURL;

with (obj_loading_panel)
{
	state = 1;
}