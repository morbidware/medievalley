///@function scr_fadeNearPlayer();

if(growstage > 1)
{
	var pla = instance_find(obj_char,0);
	var inplace = false;
	if( (pla.y < y && pla.y > y-(sprite_get_height(sprite_index) * 0.8) && pla.x > x-sprite_get_width(sprite_index)/2 && pla.x < x+sprite_get_width(sprite_index)/2) /* || (mouse_y < y && mouse_y > y-sprite_get_height(sprite_index) && mouse_x > x-sprite_get_width(sprite_index)/2 && mouse_x < x+sprite_get_width(sprite_index)/2)*/)
	{
		inplace = true;
	}
	else
	{
		inplace = false;
	}
	
	if (inplace && highlight && event_perform_ret(id, ev_other, ev_user1))
	{
		image_alpha += 0.1 * global.dt;
	}
	else if (!inplace)
	{
		image_alpha += 0.1 * global.dt;	
	}
	else
	{
		image_alpha -= 0.1 * global.dt;	
	}
}
else
{
	image_alpha += 0.1 * global.dt;
}
image_alpha = clamp(image_alpha,0.3,1.0);