///@description gmcallback_clientDisconnected
if(global.isSurvival)
{
	if(room == roomSurvival)
	{
		// Destroy dummy for the disconnected players
		with(obj_dummy)
		{
			if(socketid == string(argument0))
			{
				logga("destroy disconnected dummy " + string(argument0));
				instance_destroy();
			}
		}
		//scr_storeData();
	}
	
	exit;
}
if (room == roomMain || room == roomInit || room == roomChallenge || argument0 == noone || argument0 == "" || argument0 == 0)
{
	// Destroy dummy for the disconnected players
	with(obj_dummy)
	{
		if(socketid == string(argument0))
		{
			logga("destroy disconnected dummy " + string(argument0));
			instance_destroy();
		}
	}
	
	// Save and update clients if in our farm
	if (global.othersocketid == "")
	{
		getClientsInRoom(global.socketid);

		scr_storeData();
	}
	exit;
}

//logga("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::CLIENT DISCONNECTED CALLBACK "+string(argument0));
// If Owner has disconnected...
if(string(argument0) == global.othersocketid)
{	
	logga("will go back to my farm");
	global.otherusername = "";
	global.otheruserid = 0;
	global.othersocketid = "";
	global.othergridstring = "";
	global.otherpickables = "";
	global.otherinteractables = "";
	joinRoom(global.socketid);
	global.lastjoinedroom = global.socketid;
	getClientsInRoom(global.socketid);
	global.popupTitle = "SORRY";
	global.popupBody = "The player you visited has disconnected.";
	with(instance_create_depth(0,0,0,obj_fadeout))
	{
		gotoroom = noone;
	}
	if (global.visitorscount > 0)
	{
		updateFarm();
	}
	else
	{
		getPlayerData(global.username,global.userigid);
	}
}
else
{
	// Destroy dummy for the disconnected players
	with(obj_dummy)
	{
		if(socketid == string(argument0))
		{
			logga("destroy disconnected dummy " + string(argument0));
			instance_destroy();
		}
	}
	
	// Save and update clients if in our farm
	if (global.othersocketid == "")
	{
		getClientsInRoom(global.socketid);
		scr_storeData();
	}
}