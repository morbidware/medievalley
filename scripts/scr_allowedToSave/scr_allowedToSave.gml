///@function scr_allowedToSave()

// Returns a bool defining if it's possible to save the game in this moment.
// Mostly used to avoid issues with save overlapping/overwriting when online.
if(global.isSurvival && global.mobowner)
{
	return true;
}
// if visiting another farm, stop
if (global.othersocketid != "" || global.otheruserid > 0)
{
	return false;
}

// socket and user may be empty even when still in another farm; cover that case here
if (global.othersocketid == "" || global.otheruserid <= 0)
{
	if (instance_exists(obj_dummy))
	{
		return false;
	}
}

// saves mostly happen during fadeout; stop it if there's still a dummy around at that moment
if (instance_exists(obj_fadeout) && instance_exists(obj_dummy))
{
	return false;
}

return true;