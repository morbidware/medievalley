///@function scr_createWildPoll(wildtype)
///@description Returns a list with available wildland maps for the specified wildland type
///@param wildtype

var list = ds_list_create();
switch (argument0)
{	
	case WildType.Rural:
		
		if(!global.isSurvival)
		{
			ds_list_add(list,spr_rural_ruins_a);
			ds_list_add(list,spr_rural_ruins_b);
			ds_list_add(list,spr_rural_ruins_c);
		}
		ds_list_add(list,spr_rural_plains_a);
		ds_list_add(list,spr_rural_plains_b);
		ds_list_add(list,spr_rural_lake);
		ds_list_add(list,spr_rural_wood);
		ds_list_add(list,spr_rural_river);
		ds_list_add(list,spr_rural_cliff_a);
		ds_list_add(list,spr_rural_cliff_b);
		ds_list_add(list,spr_rural_cliff_c);	
		break;
	case WildType.Mountain:
		break;
	case WildType.Coast:
		break;
}

return list;