// tool animation
with(instance_nearest(x,y,obj_equipped))
{
	visible = true;
	var t = other.wateringTimer;
	if (other.stamina <= 0)
		t *= 2;
	scr_anim_watering(other,other.lastdirection,other.crono,t,sprite_get_number(other.custom_body_sprite),scr_asset_get_index("spr_equipped_"+itemid+"_side"),scr_asset_get_index("spr_equipped_"+itemid+"_front"));
}
if (crono >= wateringTimer)
{
	if (stamina <= 0)
	{
		if (random(1) > 0.5)
		{
			if(!global.ismobile)
			{
				instance_create_depth(interactionCol*16+8,interactionRow*16+8,depth,obj_fx_particle_water);
			}
		}
	}
	else
	{
		if(!global.ismobile)
		{
			instance_create_depth(interactionCol*16+8,interactionRow*16+8,depth,obj_fx_particle_water);
		}
	}
}

// action
crono -= global.dt;
if(crono <= 0)
{
	//decrease stamina
	scr_decreaseStamina(id, 1);
	// stop animation
	scr_setState(id,ActorState.Idle);
	with(instance_nearest(x,y,obj_equipped))
	{
		visible = false;
	}
	
	if(global.isChallenge)
	{
		if (groundtype == GroundType.Plowed && targetobject > -1 && accessible)
		{
			
		}
		else 
		{
			global.missedInteractions ++;
			var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
			p.val = global.missedInteractionsValue;
		}
	}
	
	// execute action if conditions are met
	if (groundtype == GroundType.Plowed && targetobject > -1 && accessible)
	{
		
		with(targetobject)
		{
			
			var player = instance_find(obj_char,0);
				player.interactionCol = player.highlightCol;
				player.interactionRow = player.highlightRow;
			if((x/16)-frac(x/16) != player.interactionCol || (y/16)-frac(y/16) != player.interactionRow)
			{
				exit;
			}
			if(growcrono == 0 && growstage < growstages && needswatering == 1)
			{
				
				if (global.online)
				{
					waterTile(player.interactionCol,player.interactionRow);
					scr_getGoldOnline(ActorState.Watering);
				}
				//show_debug_message("Posizione Tile: "+ string((y/16)-frac(y/16))+"y, "+string(x/16-frac(x/16))+"x    Posizione Evidenziata: "+string(player.highlightRow)+"y, "+string(player.highlightCol)+"x");
				growcrono = growtimer;
				var mission = instance_find(obj_mission_0004,0);
				if(mission != noone && mission.completion != 1)
				{
					growcrono *= 0.5;
				}
				if(room == roomChallenge)
				{
					global.interactions ++;
					with(obj_char)
					{
						var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
						p.val = global.interactionsValue;
					}
					growcrono = ds_map_find_value(global.challengeData,"grow_crono");
					shakeCrono = min(growcrono,40);
				}
			}
			else
			{
				if(room == roomChallenge)
				{
					global.missedInteractions ++;
					with(obj_char)
					{
						var p = instance_create_depth(x,y-(sprite_get_height(sprite_index)/2),depth-1,obj_fx_challenge_points);
						p.val = global.missedInteractionsValue;
					}
				}
			}
		}
		if(global.othersocketid == "")
		{
			scr_loseDurability(id,true);
		}
	}
	global.saveitemstimer = 0;
	if(global.isSurvival && global.online)
	{
		scr_storeGroundSurvival()
	}
	else
	{
		scr_storeAllInsocket();
	}
}