///@function scr_anim_charge(object,direction)
///@param object the object
///@param direction direction of the player

switch(argument1)
{
	case Direction.Right:
	image_angle = 90;
	x = argument0.x + cos(degtorad(image_angle)) * 6;
	y = argument0.y - sin(degtorad(image_angle)) * 6;
	break;
	
	case Direction.Left:
	image_angle = 270;
	x = argument0.x - cos(degtorad(image_angle)) * 6;
	y = argument0.y + sin(degtorad(image_angle)) * 6;
	break;
	
	case Direction.Up:
	image_angle = 180;
	x = argument0.x + cos(degtorad(image_angle)) * 6;
	y = argument0.y - sin(degtorad(image_angle)) * 6;
	break;
	
	case Direction.Down:
	image_angle = 0;
	x = argument0.x + cos(degtorad(image_angle)) * 6;
	y = argument0.y - sin(degtorad(image_angle)) * 6;
	break;
}
y -= 16;