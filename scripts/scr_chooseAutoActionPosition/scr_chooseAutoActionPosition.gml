///@function scr_chooseAutoActionPosition(posx,posy)
///@param posx
///@param posy
var posx = argument0;
var posy = argument1;

var inv = instance_find(obj_playerInventory,0);
var selectedobject = ds_grid_get(inv.inventory, inv.currentSelectedItemIndex, 0);
lastdirection = scr_directionFromPointAngle(x,y,posx,posy);
switch(lastdirection)
{
	case Direction.UpLeft: lastdirection = Direction.Left; break;
	case Direction.UpRight: lastdirection = Direction.Right; break;
	case Direction.DownLeft: lastdirection = Direction.Left; break;
	case Direction.DownRight: lastdirection = Direction.Right; break;
}
interactionCol = floor(posx/16);
interactionRow = floor(posy/16);
autotarget_x = -1;
autotarget_y = -1;

if (selectedobject.itemid == "hoe")
{
	scr_setState(id,ActorState.Hoe);
}
else if (selectedobject.itemid == "shovel")
{
	scr_setState(id,ActorState.Shovel);
}
else if (string_pos("seeds",selectedobject.itemid) >= string_length(selectedobject.itemid)-5)
{
	scr_setState(id,ActorState.Sow);
}
if (selectedobject.itemid == "basefencepiece")
{
	scr_setState(id,ActorState.Build);
}