///@function scr_storeItems()
logga("scr_storeItemsSurvival()");
if (!scr_allowedToSave()) { return; }

//ds_list_clear(global.interactables);
global.uisavestatus.saving = true;
with(obj_debugger)
{
	str += "scr_storeItems\n";
}
var itemslist = ds_list_create();

for(var i = 0; i < instance_number(obj_inventory); i++)
{
	var p = instance_find(obj_inventory,i);

	var storestr = p.itemid+":"+string(p.quantity)+":"+string(p.slot)+":"+string(p.durability)+":"+string(0);
	ds_list_add(itemslist,storestr);
}

if (global.online)
{
	var itemsstring = ds_list_write(itemslist);
	if(itemsstring != global.lastSavedItems)
	{
		global.lastSavedItems = itemsstring;
		global.items = global.lastSavedItems;
		sendItemsString(global.userid, ds_list_write(itemslist), global.username);
	}
	else
	{
		logga("skipping store items");
	}
	gmcallback_updateLastSave();
}
else
{
	global.items = ds_list_write(itemslist);
	//sendItemsString(global.userid, ds_list_write(itemslist), global.username);
	gmcallback_updateLastSave();
}
ds_list_destroy(itemslist);
global.uisavestatus.saving = false;
logga("scr_storeItemsSurvival() end");