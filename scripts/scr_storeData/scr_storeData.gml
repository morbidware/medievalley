///@function scr_storeData()
if (!scr_allowedToSave()) { return; }

logga("Saving game...");
if(global.isSurvival)
{
	instance_create_depth(0,0,0,obj_storeDataSurvival);
}
else
{
	instance_create_depth(0,0,0,obj_storeData);
}
