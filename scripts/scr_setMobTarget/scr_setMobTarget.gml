///@function scr_setMobTarget(radius, avoidplayer);
///@param radius
///@param avoidplayer

// Sets mob variables targetx and targety to the first useful free spot found.
// CAUTION: this script can be called ONLY inside an obj_enemy.

// If target is already set, quit
if (targetx >= 0 || targety >= 0)
{
	exit;
}

var radius = argument0;
var avoidplayer = argument1;
var angle = choose(0,90,180,270);
var px = x;
var py = y;

ds_list_clear(targetpoints_x);
ds_list_clear(targetpoints_y);
ds_list_clear(invalidpoints_x);
ds_list_clear(invalidpoints_y);

while (radius > 0)
{
	// Wrap angle; every time one entire round is made, reduce radius
	if (angle >= 360)
	{
		angle -= 360;
		radius -= 8;
	}
	
	// Calculate possible free position
	px = x + cos(degtorad(angle)) * radius;
	py = y - sin(degtorad(angle)) * radius;
	
	if (avoidplayer == true)
	{
		// If this angle is too close towards player, skip this step
		var playerangle = point_direction(x,y,hero.x,hero.y);
		if (angle+360 > playerangle+360-90 && angle+360 < playerangle+360+90)
		{
			ds_list_add(targetpoints_x, px);
			ds_list_add(targetpoints_y, py);
			angle += 15;
			continue;
		}
	}
	
	// If position is valid...
	var ok = true;
	if (px > 0 && px < room_width && py > 0 && py < room_height)
	{
		// Check if the point doesn't go through walls, but only with walls on the same storey
		var c = instance_number(obj_wall);
		var i = 0;
		repeat (c) {
			
			var wall = instance_find(obj_wall,i);
			i++;
			
			// skip walls on other storeys
			var skip = true;
			switch (storey)
			{
				case 0: if (wall.storey_0) {skip = false;} break;
				case 1: if (wall.storey_1) {skip = false;} break;
				case 2: if (wall.storey_2) {skip = false;} break;
				case 3: if (wall.storey_3) {skip = false;} break;
			}
			if (skip)
			{
				continue;
			}
			
			// final check for collision
			if (collision_line(x,y,px,py,wall,false,false) != noone)
			{
				ok = false;
				break;
			}
			if (point_distance(px,py,wall.x,wall.y) < 20)
			{
				ok = false;
				break;
			}
		}
	}
	else
	{
		ok = false;
	}
	
	// If no problematic walls are found, stop here
	if (ok)
	{
		break;
	}
	else
	{
		ds_list_add(invalidpoints_x, px);
		ds_list_add(invalidpoints_y, py);
		angle += 30;
	}
}

targetx = px;
targety = py;