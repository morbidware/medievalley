logga("scr_storeStoreySurvival()");
if (!scr_allowedToSave()) { return; }
global.uisavestatus.saving = true;

var fground = instance_find(obj_survivalGround,0);
var uncompressed_string = ds_grid_write(fground.storeygrid);
var compressed_string = scr_compressFarmString(fground.storeygrid);


logga("STORE GROUND SURVIVAL");
//logga(compressed_string);
with(obj_baseGround)
{
	if (global.online)
	{
		// If visiting a farm and owner is not here, save it for him
		if(global.otheruserid != 0 && !instance_exists(obj_dummy))
		{
			if(global.otherstoreystring != uncompressed_string)
			{
				logga("WILL UPDATE OTHER STOREY ON SERVER");
				global.otherstoreystring = ds_grid_write(grid);
				sendStoreyString(global.landid, compressed_string, global.otherusername);
			}
		}
		else
		{
			if(global.storeystring != uncompressed_string)
			{
				logga("WILL UPDATE STOREY ON SERVER");
				global.storeystring = ds_grid_write(grid);
				sendStoreyString(global.landid, compressed_string, global.username);
			}
			gmcallback_updateLastSave();
		}
	}
	else
	{
		global.storeystring = ds_grid_write(grid);
		gmcallback_updateLastSave();
	}
}

global.uisavestatus.saving = false;