///@function scr_dropItems(fromobject,itemid)
///@description this function creates "dropped" objects and positions them in a wat they don't overlap
///@param fromobject
///@param itemid (string)
if(room == roomChallenge)
{
	exit;
}
var drops = argument_count-1;
if (argument_count < 1)
{
	show_debug_message("ERROR: trying to drop objects with insufficient arguments");
	exit;
}

var increment = 360 / drops;
for (var i = 0; i < drops; i++) 
{	
	var itemid = argument[i+1];
	var angle = increment * i;
	var dx = argument[0].x + (sin(degtorad(angle)) * 8);
	var dy = argument[0].y + (cos(degtorad(angle)) * 5);
	if (global.online)
	{
		dropPickable(dx,dy,itemid,1);
	}
	scr_gameItemCreate(dx, dy, obj_pickable, itemid);
}

if(global.isSurvival  && global.online)
{
	scr_storePickablesSurvival();
}
else
{
	scr_storeAllInsocket();
}