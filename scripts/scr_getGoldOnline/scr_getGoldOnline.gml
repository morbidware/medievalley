///@function scr_getGoldOnline(ActorState)
///@param amount
///@description Gold is only given when working on other's Farms
if(global.othersocketid != "")
{
	var state = argument0;
	var earnedgold = 0;
	switch (state)
	{
		case ActorState.Chop: earnedgold = 3; break;
		case ActorState.Mine: earnedgold = 3; break;
		case ActorState.Sickle: earnedgold = 2; break;
		case ActorState.Pick: earnedgold = 1; break;
		case ActorState.Watering: earnedgold = 2; break;	
	}
	
	var pla = instance_find(obj_char,0);
	pla.gold += earnedgold;
	var fxcoin = instance_create_depth(x,y-16,depth-1,obj_fx_coin);
	fxcoin.str = "+"+string(earnedgold);
}