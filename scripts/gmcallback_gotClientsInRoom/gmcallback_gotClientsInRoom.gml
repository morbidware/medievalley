global.visitorscount = argument0;
if(room == roomSurvival)
{
	exit;
}

if (global.lastjoinedroom == global.socketid && global.othersocketid == "")
{
	global.visitorscount--;
}
if (global.allowvisitors == 0 || global.othersocketid != "" || room == roomChallenge || room == roomMain || room == roomInit)
{
	global.visitorscount = -1;
}
sendNumVisitorsString(global.socketid, string(global.visitorscount), global.username);