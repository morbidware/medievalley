//ds_list_clear(global.interactables);
if (!scr_allowedToSave()) { return; }

global.uisavestatus.saving = true;
var wallslist = ds_list_create();
instance_activate_object(obj_wall);
for(var i = 0; i < instance_number(obj_wall); i++)
{
	var p = instance_find(obj_wall,i);
	/*
	diagonalTopLeft = false;
	diagonalTopRight = false;
	diagonalBottomLeft = false;
	diagonalBottomRight = false;

	storey_0 = false;
	storey_1 = false;
	storey_2 = false;
	storey_3 = false;
	storey_stairs_in = false;
	storey_stairs_out = false;
	blocking = true;
	*/
	var storestr = object_get_name(p.object_index)+":"+string(p.x)+":"+string(p.y)+":"+string(p.diagonalTopLeft)+":"+string(p.diagonalTopRight)+":"+string(p.diagonalBottomLeft)+":"+string(p.diagonalBottomRight)+":"+string(p.storey_0)+":"+string(p.storey_1)+":"+string(p.storey_2)+":"+string(p.storey_3)+":"+string(p.storey_stairs_in)+":"+string(p.storey_stairs_out)+":"+string(p.blocking);
	ds_list_add(wallslist,storestr);
}
instance_activate_object(obj_stair);
for(var i = 0; i < instance_number(obj_stair); i++)
{
	var p = instance_find(obj_stair,i);
	/*
	type = 0;
	*/
	var storestr = "obj_stair:"+string(p.x)+":"+string(p.y)+":"+string(p.type);
	ds_list_add(wallslist,storestr);
}
var uncompressed_string = ds_list_write(wallslist);

if (global.online)
{
	if(global.wallsstring != uncompressed_string)
	{
		logga("WILL UPDATE WALLS ON SERVER");
		global.wallsstring = uncompressed_string;
		sendWallsStringSurvival(global.landid, uncompressed_string, global.username);
	}
	gmcallback_updateLastSave();
}
else
{
	global.interactables = uncompressed_string;
	gmcallback_updateLastSave();
}
ds_list_destroy(wallslist);

global.uisavestatus.saving = false;