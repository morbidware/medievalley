///@description gmcallback_gotCanvasResize
logga("gmcallback_gotCanvasResize");
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

global.cw = max(real(ds_list_find_value(res,0)),real(ds_list_find_value(res,1)));
global.ch = min(real(ds_list_find_value(res,0)),real(ds_list_find_value(res,1)));
logga("cw " + string(global.cw) + " ch " + string(global.ch));
ds_list_destroy(res);

var asp = global.ch/global.cw;
var hh = 540;
if(global.ch < hh)
{
	hh = global.ch;
}
window_set_size(hh/asp,hh);
window_set_position(global.cw/2.0,0);
global.lastcw = global.cw;
global.lastch = global.ch;

display_set_gui_size(global.cw,global.ch);
camera_set_view_size(view_camera[0],global.cw*0.5,global.ch*0.5);
view_set_wport(0,global.cw);
view_set_hport(0,global.ch);
alerta("end of gmcallback_gotCanvasResize");
//window_center();
