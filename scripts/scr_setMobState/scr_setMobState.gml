///@function scr_setMobState(mob,state);
///@description update Mob
///@param mob
///@param state

with(argument0)
{
	var oldState = state;
	var newState = argument1;
	var mobType = string_replace(object_get_name(object_index),"obj_","");
	if(oldState != newState)
	{
		state = newState;
		switch(state)
		{
			case MobState.Idle:
			crono = idleTimer;
			targetx = -1;
			targety = -1;
			scr_setMobSpriteAction(mobType,"idle",lastdirection,-1);
			image_index = 0;
			break;
			
			case MobState.Roam:
			crono = roamTimer;
			scr_setMobSpriteAction(mobType,"walk",lastdirection,-1);
			image_index = 0;
			break;
			
			case MobState.Charge:
			crono = chargeTimer;
			scr_setMobSpriteAction(mobType,"attack",lastdirection,chargeTimer);
			image_index = 0;
			break;
			
			case MobState.Attack:
			crono = attackTimer;
			scr_setMobSpriteAction(mobType,"attack",lastdirection,attackTimer);
			image_index = 0;
			break;
			
			case MobState.Chase:
			chasetimer = 0;
			scr_setMobSpriteAction(mobType,"run",lastdirection,chaseAnimSpeed);
			image_index = 0;
			//logga("new state = MobState.Chase");
			break;
			
			case MobState.AfterAttack:
			crono = afterattackTimer;
			scr_setMobSpriteAction(mobType,"idle",lastdirection,-1);
			image_index = 0;
			break;
			
			case MobState.Flee:
			crono = fleeTimer;
			targetx = -1;
			targety = -1;
			scr_setMobSpriteAction(mobType,"run",lastdirection,-1);
			image_index = 0;
			break;
			
			case MobState.Suffer:
			savedState = oldState;
			savedCrono = crono;
			if(path_index > -1)
			{
				path_speed = 0;
			}
			crono = sufferTimer;
			flashCrono = crono;
			scr_setMobSpriteAction(mobType,"suffer",lastdirection,sufferTimer);
			image_index = 0;
			break;
			
			case MobState.Die:
			event_perform(ev_other, ev_user1);
			crono = dieTimer;
			flashCrono = crono;
			scr_setMobSpriteAction(mobType,"die",lastdirection,dieTimer);
			image_index = 0;
			break;
			
			case MobState.RunAway:
			
			scr_setMobSpriteAction(mobType,"run",lastdirection,-1);
			image_index = 0;
			break;
			
		}
	}
}