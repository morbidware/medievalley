///@description gmcallback_drawEquipped
var str = argument0; 
var del = ":";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var sid = string(ds_list_find_value(res,0));
var xx = real(ds_list_find_value(res,1));
var yy = real(ds_list_find_value(res,2));
var ia = real(ds_list_find_value(res,3));
var si = real(ds_list_find_value(res,4));
var ii = real(ds_list_find_value(res,5));
var xs = real(ds_list_find_value(res,6));
var ys = real(ds_list_find_value(res,7));

ds_list_destroy(res);

for(var i = 0; i < instance_number(obj_dummy); i++)
{
	var dummy = instance_find(obj_dummy,i);
	if(dummy.socketid == sid)
	{
		dummy.eq_x = xx;
		dummy.eq_y = yy;
		dummy.eq_image_angle = ia;
		dummy.eq_sprite_index = si;
		dummy.eq_image_index = ii;
		dummy.eq_image_xscale = xs;
		dummy.eq_image_yscale = ys;
		dummy.eq_timer = dummy.eq_maxtimer;
	}
}