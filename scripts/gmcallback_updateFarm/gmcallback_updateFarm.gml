///@description with this cb we have successfully got back our data and are ready to leave another players farm 
logga("GMCALLBACK_UPDATEFARM");
var str = argument0; 
var del = "$";
var res = ds_list_create();
var pos = string_pos(del, str);
var dellen = string_length(del);
while (pos) 
{
    pos -= 1;
    ds_list_add(res, string_copy(str, 1, pos));
    str = string_delete(str, 1, pos + dellen);
    pos = string_pos(del, str);
}
ds_list_add(res, str);

var usrname = string(ds_list_find_value(res,0));
var usrid = real(ds_list_find_value(res,1));
var socketid = string(ds_list_find_value(res,2));
var farmstring = string(ds_list_find_value(res,3));

farmstring = scr_uncompressFarmString(farmstring);
//farmstring = string_replace_all(farmstring,"_","00000000000000000000000");
//farmstring = string_replace_all(farmstring,"%","0000000000");

var pickablesstring = string(ds_list_find_value(res,4));

pickablesstring = string_replace_all(pickablesstring,"_","000000");
pickablesstring = string_replace_all(pickablesstring,"%","A666C696E743A313A3001");

var interactablesstring = string(ds_list_find_value(res,5));

interactablesstring = string_replace_all(interactablesstring,"_","000000");
interactablesstring = string_replace_all(interactablesstring,"%","303A313A313A303A3");
interactablesstring = string_replace_all(interactablesstring,"!","70696E653A3");
interactablesstring = string_replace_all(interactablesstring,"&","77696C6467726173733A");
interactablesstring = string_replace_all(interactablesstring,",","6F626A5F696E74657261637461626C655F");

ds_list_destroy(res);

global.username = usrname;
global.userid = usrid;
global.farmid = usrid; 
global.socketid = socketid;
global.gridstring = farmstring;
global.pickables = pickablesstring;
global.interactables = interactablesstring;

if (instance_exists(obj_fadeout))
{
	with(obj_fadeout)
	{
		gotoroom = roomFarm;
	}
}
else with(instance_create_depth(0,0,0,obj_fadeout))
{
	gotoroom = roomFarm;
}