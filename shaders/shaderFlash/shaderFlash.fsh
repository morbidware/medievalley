//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform float u_intensity;

void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 color = mix(gl_FragColor,vec4(1.0,1.0,1.0,gl_FragColor.a),u_intensity);
	gl_FragColor = color;
	
	/*
	float left = UV.r;
	float top = UV.g;
	float right = UV.b;
	float bottom = UV.a;
	float wid = right-left;
	float hei = bottom-top;
	float centerx = left + (right-left)/2.0;
	float centery = top + (bottom-top)/2.0;
	float px = v_vTexcoord.x;
	float py = v_vTexcoord.y;
	float d = 1.0-(distance(vec2(px,py),vec2(centerx,centery))/(min(wid,hei)));
	float dx = 1.0-(abs(px-centerx)/wid)*2.0;
	float dy = 1.0-(abs(py-centery)/hei)*2.0;
	vec4 final = mix(vec4(dx),vec4(dy),0.5);
	gl_FragColor = vec4(d);
	*/
	
}
