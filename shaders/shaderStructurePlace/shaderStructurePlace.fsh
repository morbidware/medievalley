//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float u_canBePlaced;

void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 mixColor = vec4(1.0,0.0,0.0,1.0);
	
	if(u_canBePlaced == 1.0)
	{
		mixColor = vec4(0.0,1.0,0.0,1.0);
	}
	
	vec4 finalColor = gl_FragColor;
	
	if(finalColor.a > 0.0)
	{
		finalColor = mix(gl_FragColor, mixColor, 0.5);
	}
	
	finalColor.a *= 0.5;
	
	gl_FragColor = finalColor;
	
}
