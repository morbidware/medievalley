//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float u_r;
uniform float u_g;
uniform float u_b;

uniform float u_r2;
uniform float u_g2;
uniform float u_b2;

uniform float u_bf;

void main()
{
    //gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 colorhere = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 colorleft = v_vColour * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x-0.0005,v_vTexcoord.y) );
	vec4 colorright = v_vColour * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x+0.0005,v_vTexcoord.y) );
	vec4 colortop = v_vColour * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x,v_vTexcoord.y-0.0005) );
	vec4 colorbottom = v_vColour * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x,v_vTexcoord.y+0.0005) );
	
	float sthere = step(0.9, colorhere.a);
	
	float stleft = step(0.9, colorleft.a);
	float stright = step(0.9, colorright.a);
	float sttop = step(0.9, colortop.a);
	float stbottom = step(0.9, colorbottom.a);
	
	if(sthere == 0.0 && (stleft == 1.0||stright == 1.0||sttop == 1.0||stbottom == 1.0))
	{
		vec4 c1 = vec4(u_r,u_g,u_b,1.0);
		vec4 c2 = vec4(u_r2,u_g2,u_b2,1.0);
		gl_FragColor = mix(c1,c2,u_bf);
	}
	else
	{
		gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	}
	
}
