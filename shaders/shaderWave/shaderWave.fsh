//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float u_top;
uniform float u_bottom;
uniform float u_left;
uniform float u_right;

uniform float u_time;
uniform float u_intensity;
uniform float u_highlight;

uniform float u_resx;
uniform float u_resy;

void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	vec2 res = vec2(u_resx,u_resy);
	
	float w = u_right - u_left;
	float h = u_top - u_bottom;
	
	float uvxn = (v_vTexcoord.x - u_left)/w;
	float uvyn = (v_vTexcoord.y - u_bottom)/h;
	
	float stx = w/res.x;
	float sty = h/res.y;
	
	vec2 pa = vec2(uvxn,uvyn);
	vec2 pb = vec2(uvxn,0.0);
	float dist = pow(distance(pa,pb),2.0);
	
	float ox = cos(u_time) * (((stx*5.0)*dist)*u_intensity);
	
	//vec2 newtexcoord = vec2(v_vTexcoord.x+ox,v_vTexcoord.y);
	vec2 newtexcoord = vec2(v_vTexcoord.x+ox,v_vTexcoord.y);
	if(newtexcoord.x < u_left || newtexcoord.x > u_right)
	{
		gl_FragColor = vec4(0.0);
	}
	else
	{
		gl_FragColor = v_vColour * texture2D( gm_BaseTexture, newtexcoord );
	}
	
	if(u_highlight == 1.0)
	{
		vec4 color = mix(gl_FragColor,vec4(1.0,1.0,1.0,gl_FragColor.a),0.25);
		gl_FragColor = color;
	}
	//gl_FragColor = vec4(vec3(dist),1.0);
}
