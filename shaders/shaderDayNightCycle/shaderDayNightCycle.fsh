//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float u_r;
uniform float u_g;
uniform float u_b;

uniform sampler2D u_normalColorTex;
uniform sampler2D u_lightColorTex;

void main()
{
	//originalColor of the application surface
	vec4 originalColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	//finalColor of the application surface
	vec4 finalColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord ) *  vec4(u_r, u_g, u_b, 1.0);
	//normalColor is a b/w texture
	vec4 normalColor = v_vColour * texture2D( u_normalColorTex, v_vTexcoord*vec2(1.0,1.0));
	vec4 lightColor = v_vColour * texture2D( u_lightColorTex, v_vTexcoord*vec2(1.0,1.0));
	vec4 warmColor =  v_vColour * texture2D( gm_BaseTexture, v_vTexcoord ) *  vec4(255.0/255.0, 130.0/255.0, 0.0, 1.0);
	
	vec4 warmfinal = mix(finalColor,warmColor,lightColor.x);
	vec4 normalvsfinal = mix(warmfinal,originalColor,normalColor.x);
	
	gl_FragColor = normalvsfinal;
}
