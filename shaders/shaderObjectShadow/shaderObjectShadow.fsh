varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float u_top;
uniform float u_bottom;
uniform float u_left;
uniform float u_right;

uniform float u_time;
uniform float u_intensity;
uniform float u_highlight;

uniform float u_resx;
uniform float u_resy;

void main()
{	
	
	//gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	vec2 res = vec2(u_resx,u_resy);
	
	float w = u_right - u_left;
	float h = u_top - u_bottom;
	
	float uvxn = (v_vTexcoord.x - u_left)/w;
	float uvyn = (v_vTexcoord.y - u_bottom)/h;
	
	float stx = w/res.x;
	float sty = h/res.y;
	
	vec2 newtexcoord = vec2(v_vTexcoord.x,v_vTexcoord.y);
	if(newtexcoord.x < u_left || newtexcoord.x > u_right)
	{
		gl_FragColor = vec4(0.0);
	}
	else
	{
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, newtexcoord );
		
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x - stx, newtexcoord.y));
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x + stx,newtexcoord.y));
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x, newtexcoord.y - sty));
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x, newtexcoord.y + sty));
		
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x - (stx * 2.0), newtexcoord.y));
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x + (stx * 2.0), newtexcoord.y));
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x, newtexcoord.y - (sty * 2.0)));
		gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(newtexcoord.x, newtexcoord.y + (sty * 2.0)));
	}
}