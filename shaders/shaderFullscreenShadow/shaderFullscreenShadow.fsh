varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float screenx;
uniform float screeny;

void main()
{	
	//gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	float stx = 1.0/screenx;
	float sty = 1.0/screeny;
	
	gl_FragColor = (v_vColour * 0.1) * texture2D( gm_BaseTexture, v_vTexcoord );
	
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x - stx, v_vTexcoord.y));
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x + stx,v_vTexcoord.y));
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x, v_vTexcoord.y - sty));
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x, v_vTexcoord.y + sty));
		
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x - (stx * 2.0), v_vTexcoord.y));
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x + (stx * 2.0), v_vTexcoord.y));
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x, v_vTexcoord.y - (sty * 2.0)));
	gl_FragColor += (v_vColour * 0.1) * texture2D( gm_BaseTexture, vec2(v_vTexcoord.x, v_vTexcoord.y + (sty * 2.0)));
}