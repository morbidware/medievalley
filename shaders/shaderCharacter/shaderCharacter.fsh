// this shader will replace specific RGB values with given colors

varying vec2 v_vTexcoord;
varying vec4 v_vColour;

// skin
uniform float skin1_r;
uniform float skin1_g;
uniform float skin1_b;

uniform float skin2_r;
uniform float skin2_g;
uniform float skin2_b;

uniform float skin3_r;
uniform float skin3_g;
uniform float skin3_b;

// eye
uniform float eye_r;
uniform float eye_g;
uniform float eye_b;

// hair
uniform float hair1_r;
uniform float hair1_g;
uniform float hair1_b;

uniform float hair2_r;
uniform float hair2_g;
uniform float hair2_b;

uniform float hair3_r;
uniform float hair3_g;
uniform float hair3_b;

// sleeve
uniform float sleeve1_r;
uniform float sleeve1_g;
uniform float sleeve1_b;

uniform float sleeve2_r;
uniform float sleeve2_g;
uniform float sleeve2_b;

uniform float sleeve3_r;
uniform float sleeve3_g;
uniform float sleeve3_b;

// pants
uniform float pants1_r;
uniform float pants1_g;
uniform float pants1_b;

uniform float pants2_r;
uniform float pants2_g;
uniform float pants2_b;

uniform float pants3_r;
uniform float pants3_g;
uniform float pants3_b;

// shoes
uniform float shoes1_r;
uniform float shoes1_g;
uniform float shoes1_b;

uniform float shoes2_r;
uniform float shoes2_g;
uniform float shoes2_b;

uniform float shoes3_r;
uniform float shoes3_g;
uniform float shoes3_b;

void main()
{
    gl_FragColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	vec4 skin1 = vec4(skin1_r, skin1_g, skin1_b, 255.0);
	vec4 skin2 = vec4(skin2_r, skin2_g, skin2_b, 255.0);
	vec4 skin3 = vec4(skin3_r, skin3_g, skin3_b, 255.0);
	
	vec4 eye = vec4(eye_r, eye_g, eye_b, 255.0);
	
	vec4 hair1 = vec4(hair1_r, hair1_g, hair1_b, 255.0);
	vec4 hair2 = vec4(hair2_r, hair2_g, hair2_b, 255.0);
	vec4 hair3 = vec4(hair3_r, hair3_g, hair3_b, 255.0);
	
	vec4 sleeve1 = vec4(sleeve1_r, sleeve1_g, sleeve1_b, 255.0);
	vec4 sleeve2 = vec4(sleeve2_r, sleeve2_g, sleeve2_b, 255.0);
	vec4 sleeve3 = vec4(sleeve3_r, sleeve3_g, sleeve3_b, 255.0);
	
	vec4 pants1 = vec4(pants1_r, pants1_g, pants1_b, 255.0);
	vec4 pants2 = vec4(pants2_r, pants2_g, pants2_b, 255.0);
	vec4 pants3 = vec4(pants3_r, pants3_g, pants3_b, 255.0);
	
	vec4 shoes1 = vec4(shoes1_r, shoes1_g, shoes1_b, 255.0);
	vec4 shoes2 = vec4(shoes2_r, shoes2_g, shoes2_b, 255.0);
	vec4 shoes3 = vec4(shoes3_r, shoes3_g, shoes3_b, 255.0);
	
	if (gl_FragColor.a > 0.0)
	{
		// skin
		if (int(gl_FragColor.r * 255.0) == 173) { gl_FragColor = skin1; return; }
		else if (int(gl_FragColor.r * 255.0) == 221) { gl_FragColor = skin2; return; }
		else if (int(gl_FragColor.r * 255.0) == 244) { gl_FragColor = skin3; return; }

		// eye
		else if (int(gl_FragColor.r * 255.0) == 0) { gl_FragColor = eye; return; }
	
		// hair
		else if (int(gl_FragColor.r * 255.0) == 168) { gl_FragColor = hair1; return; }
		else if (int(gl_FragColor.r * 255.0) == 207) { gl_FragColor = hair2; return; }
		else if (int(gl_FragColor.r * 255.0) == 251) { gl_FragColor = hair3; return; }
	
		// sleeve
		else if (int(gl_FragColor.r * 255.0) == 10) { gl_FragColor = sleeve1; return; }
		else if (int(gl_FragColor.r * 255.0) == 29) { gl_FragColor = sleeve2; return; }
		else if (int(gl_FragColor.r * 255.0) == 48) { gl_FragColor = sleeve3; return; }
	
		// pants
		else if (int(gl_FragColor.r * 255.0) == 6) { gl_FragColor = pants1; return; }
		else if (int(gl_FragColor.r * 255.0) == 17) { gl_FragColor = pants2; return; }
		else if (int(gl_FragColor.r * 255.0) == 35) { gl_FragColor = pants3; return; }
	
		// shoes
		else if (int(gl_FragColor.r * 255.0) == 65) { gl_FragColor = shoes1; return; }
		else if (int(gl_FragColor.r * 255.0) == 96) { gl_FragColor = shoes2; return; }
		else if (int(gl_FragColor.r * 255.0) == 112) { gl_FragColor = shoes3; return; }
	}
}
