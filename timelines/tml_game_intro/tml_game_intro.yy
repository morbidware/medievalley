{
    "id": "3c2378a4-0542-49d2-b532-8637bbe3c9ee",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "tml_game_intro",
    "momentList": [
        {
            "id": "7f3c25d9-953f-4b9b-8d97-dd906005da52",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "1a5e5c7a-536b-43b7-98f8-25ff81e3b6fa",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "3c2378a4-0542-49d2-b532-8637bbe3c9ee"
            },
            "moment": 0
        },
        {
            "id": "070b503e-04f2-4524-b2f6-2a20482ecf34",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "f8cc41d8-3b9a-4d32-b074-6d52d115ff76",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 100,
                "eventtype": 0,
                "m_owner": "3c2378a4-0542-49d2-b532-8637bbe3c9ee"
            },
            "moment": 100
        },
        {
            "id": "aed587fe-2fcf-4c0b-aec9-88000e99e535",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "2eea6b24-90e3-44d6-9fd1-c3c2e379b2e8",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 220,
                "eventtype": 0,
                "m_owner": "3c2378a4-0542-49d2-b532-8637bbe3c9ee"
            },
            "moment": 220
        },
        {
            "id": "5f2cf7ee-8f7c-4b20-bd54-0d404affd500",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "ea68a9e2-13a8-4804-a496-e94886830f6c",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 280,
                "eventtype": 0,
                "m_owner": "3c2378a4-0542-49d2-b532-8637bbe3c9ee"
            },
            "moment": 280
        },
        {
            "id": "42a2f416-9c79-4bed-9b72-6281881d8550",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "7bc16bea-52af-4c3e-9cef-fa20b3b1879d",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 700,
                "eventtype": 0,
                "m_owner": "3c2378a4-0542-49d2-b532-8637bbe3c9ee"
            },
            "moment": 700
        }
    ]
}