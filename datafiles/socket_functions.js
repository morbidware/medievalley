'use strict';

//this is the server
var destinationids = [];
var destinationnames = [];
var destinantionnumtrees = [];
var destinantionnumrocks = [];
var destinantionnumplants = [];
var destinantionnumvisitors = [];
var connectedUsers = 0;
var debug = 0;
var zlib = require('zlib');
var slug = require('slug');

module.exports = function(io) 
{

    ////////////////////////////////////////////////////////////////

    var DBConnectionManager = require('./dbconnection');
    var rt_functions = require('./rt_functions.js') 


    function executeMysqlStatement( queryString )
    {
        console.log("executeMysqlStatement with query = ("+queryString.substring(0,100)+")");
        DBConnectionManager.query( queryString, null, function(result, err)
        {
            if(err)
            {
                console.log("error in executeMysqlStatement " + err + "statement : " + queryString );
                activityLogErr(err);
                throw err;
            }
            var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
            console.log("res:"+res);

        });

    }


    io.on('connection', function(socket)
    {
        var skip_farm_processing = 1; //initialize with url..
        //local variable to the socket
        var mysocket = socket.id;
        //local destination is initially same as socket
        var destinationid = socket.id;
        //global destinations holds all socket ids
        //destinationids.push(socket.id);
        //if asked for the socket list return a string with all sockets connected
        
        console.log("A user connected with socket " + mysocket);

        //perche usiamo NGINX come proxy...
        var client_ip_address = socket.handshake.headers["x-real-ip"];
        console.log("client_ip_address" + client_ip_address);
        //var test_address = socket.request.connection.remoteAddress;
        
        console.log('indexjs:server: user connected ' + socket.id + ' ' + io.sockets.connected[socket.id] + ' ' + Object.keys(io.sockets.connected).length);
        
        //io.emit('update users connected', connectedUsers);
        socket.emit('client connected', socket.id);
        //io.emit emette dal server a tutti i client
        //socket.broadcast.emit emette dal server a tutti i client tranne il chiamante
        //socket.emit emette dal client al server
        
        var s_username = "";
        var s_userid = 0;
        var s_userigid = 0;
        var s_socketid = "";
        var s_farmstring = "";
        var s_pickables = "";
        var s_house_pickables = "";
        var s_interactables = "";
        var s_house_interactables = "";
        var s_items = "";
        var s_missions = "";
        var s_userdata = "";
        var s_userstats = "";
        var s_deathdata = "";
        //CONFIG
        var s_csd = 1;
        var s_tct = 5;
        var s_weg = 0;
        var s_gth = 75;
        var s_challenge_id = 0;

        var s_address = client_ip_address.replace("::ffff:","");	
        var s_email = "";

        socket.on("fix name", function(str)
        {
            var dec = decodeURIComponent(str);
            var res = slug(dec);
            activityLog("FIX NAME: "+str+" -> "+res);
            socket.emit("fixed name", res);
        });

        socket.on("logga", function(str)
        {
            activityLog(str);
        });

        socket.on("bunyan log", function(str)
        {
            activityLog(str);
        });

        function activityLog(str)
        {
            console.log("<"+s_userigid+"> "+str);
        }

        function activityLogErr(str)
        {
            /*
            FgBlack = "\x1b[30m"
            FgRed = "\x1b[31m"
            FgGreen = "\x1b[32m"
            FgYellow = "\x1b[33m"
            FgBlue = "\x1b[34m"
            FgMagenta = "\x1b[35m"
            FgCyan = "\x1b[36m"
            FgWhite = "\x1b[37m"
            Blink = "\x1b[5m"
            */
            console.log("\x1b[31m%s\x1b[0m","<"+s_userigid+"> "+str);
        }
        
        function activityLogBlink(str)
        {
            console.log("\x1b[5m%s\x1b[0m","<"+s_userigid+"> "+str);
        }

        function activityLogBlue(str)
        {
            console.log("\x1b[36m%s\x1b[0m","<"+s_userigid+"> "+str);
        }

        function activityLogGreen(str)
        {
            console.log("\x1b[32m%s\x1b[0m","<"+s_userigid+"> "+str);
        }

        function logDestinations()
        {
        	var i = 0;
        	activityLogBlue("---------------- DESTINATIONIDS ----------------");
        	destinationids.forEach(function(v)
	        {
	        	var len = 23 - destinationnames[i].length;
	        	var stri = "";
	        	if(len > 0)
	        	{
	        		for(var l = 0; l < len; l++)
	        		{
	        			stri += " ";
	        		}
	        		stri += "|  ";
	        	}
	        	else 
	        	{
	        		stri += "|";
	        	}
	            activityLogBlue("|" + destinationnames[i] + stri + "" + v + "|");
	            i++;
	        });
	        activityLogBlue("------------------------------------------------");
        }

        socket.on("get clients in room", function(roomname)
        {
            if ( io.sockets.adapter.rooms[roomname] == undefined)
            {
                //activityLog("ROOM UNDEFINED");
                socket.emit("got clients in room", 0);
                return;
            }
            var arr = io.sockets.adapter.rooms[roomname].sockets;
            if (arr == undefined)
            {
                //activityLog("SOCKETS UNDEFINED");
                socket.emit("got clients in room", 0);
                return;
            }
            var c = 0;
            for (var key in arr)
            {
                c++;
            }
            //activityLog("CLIENTS: "+c);
            socket.emit("got clients in room", c);
        });

        socket.on('get user creation date', function(usrigid)
        {
            
                DBConnectionManager.query("SELECT * FROM users WHERE igid = '"+usrigid+"'", function(result, err)
                {
                    if(err)
                    {
                        socket.emit("cannot get user creation date");
                        activityLogErr(err);
                        throw err;
                    }
                    else
                    {
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        socket.emit('got user creation date', res[0].date);
                    }

                });
        });

        socket.on('get socket list', function()
        {
            sendSocketsList();
        });

        function sendSocketsList()
        {
            //activityLog("-------------------------");
            var str = "";
            var str2 = "";
            var str3 = "";
            var str4 = "";
            var str5 = "";
            var str6 = "";
            var i = 0;
            destinationids.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinationids.length-1)
                {
                    str += v+":";
                }
                else
                {
                    str += v;
                }
                i++;
            });
            i = 0;
            destinationnames.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinationnames.length-1)
                {
                    str2 += v+":";
                }
                else
                {
                    str2 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumtrees.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumtrees.length-1)
                {
                    str3 += v+":";
                }
                else
                {
                    str3 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumrocks.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumrocks.length-1)
                {
                    str4 += v+":";
                }
                else
                {
                    str4 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumplants.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumplants.length-1)
                {
                    str5 += v+":";
                }
                else
                {
                    str5 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumvisitors.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumvisitors.length-1)
                {
                    str6 += v+":";
                }
                else
                {
                    str6 += v;
                }
                i++;
            });
            socket.emit('send sockets list',str,str2,str3,str4,str5,str6);
            //activityLog("-------------------------");
        }
        
        //GOING TO ANOTHER FARM 2: with this function we will ask the destination socket to give back its values
        socket.on("reach user for data",function(s)
        {
            var des = s;
            var found = false;
            for(var i = 0; i < destinationids.length; i++)
            {
                if(destinationids[i] == s)
                {
                    des = s;
                    found = true;
                    break;
                }
            }

            //origin = s_username;?????
            if(found)
            {
                activityLog(mysocket+": asking data to " + io.sockets.connected[des]);
                io.sockets.connected[des].emit("give me data",mysocket);
            }
            else
            {
                socket.emit("get user data failed");
            }
        });

        //GOING TO ANOTHER FARM 4: with this function the destination sends out its data
        socket.on("give me data",function(origin)
        {
            activityLog(mysocket+": sending data to " + origin);
            io.to(origin).emit("got user data", s_username, s_userid, s_socketid, s_farmstring, s_pickables, s_interactables);
        });

        socket.on("retrieve user data",function(usrname,userid,s)
        {
            //activityLog("indexjs:server: get user data " + username + " " + userid + " " + s);
        });

        //CALLED ON STARTUP
        socket.on("get player record",function(usrigid)
        {
            activityLog("Will check if there's already some saved data for user " + usrigid);
            activityLog("Connecting to table USERS to find user " + usrigid);
            DBConnectionManager.query("SELECT * FROM users WHERE igid = '"+usrigid+"'", null, function(result,err)
            {
                //activityLog("inside select");
                if(err)
                {
                    activityLog("There was an error for user " + usrigid + ". " + err);
                    socket.emit("get player data failed");
                    activityLogErr(err);
                    throw err;
                    return;
                }

                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }

                //if not found add it to the db
                if(Object.keys(res).length == 0)
                {
                    activityLog("No data found for user " + usrigid);
                    socket.emit('got player record',"0");
                    socket.emit('got player gold',"100:100:0:0:1:1:9201000000000000:9201000000000000:2E01000000000000:0:0:0:1:1");
                }
                else//just read it and go on
                {
                    activityLog("Found data for user " + usrigid);
                    socket.emit('got player record',"1");
                    socket.emit('got player gold',res[0].userdata);
                }

            });
        
        });

        socket.on("resync user",function(usrigid,usremail, usrname, ischallenge)
        {
            activityLog("resync user ");
            s_userigid = usrigid;
            s_email = usremail;
            activityLogGreen("SOCKET.JOIN -> "+usrname);
            socket.join(usrname);
            s_username = usrname;//decodeURIComponent(usrname);
            
            /*
            activityLog("resync user --- id:"+s_userigid);
            activityLog("resync user --- email:"+s_email);
            activityLog("resync user --- ip:"+s_address);
            activityLog("resync user --- username:"+s_username);
			*/
            if(ischallenge == 1)
            {
                skip_farm_processing = 1;
                activityLog("skip_farm_processing " + skip_farm_processing);
            }
            else
            {
                skip_farm_processing = 0;
                activityLog("skip_farm_processing " + skip_farm_processing);
            }
            if(!skip_farm_processing)
            {
            	activityLogBlink("RESYNC: Looking for user "+s_username+":"+socket.id+" in destinationids ");
	            var i = 0;
		        destinationids.forEach(function(v)
		        {
		            if(socket.id == v)
		            {
		            	activityLogBlink("RESYNC: Found user "+s_username+":"+socket.id+" in destinationids -> removing");
		                destinationids.splice(i, 1);  
		                destinationnames.splice(i, 1); 
		                destinantionnumtrees.splice(i, 1);
		                destinantionnumrocks.splice(i, 1);
		                destinantionnumplants.splice(i, 1);  
		                connectedUsers--;
		            }
		            else
		            {
		                i++;
		            }
		        });
                activityLogBlink("RESYNC: Now adding user "+s_username+":"+socket.id+" in destinationids");
                destinationid = socket.id;
                destinationids.push(socket.id);
                destinationnames.push(s_username);
                destinantionnumtrees.push("0");
                destinantionnumrocks.push("0");
                destinantionnumplants.push("0");  
                connectedUsers++;
                sendSocketsList();
                logDestinations();
                /*
		        var check_name = s_username;
		    	var check_encoded = encodeURIComponent(check_name);
		    	var check_clean = check_encoded.replace(/%20/g," ");
		    	var check_match = (check_name === check_clean);
		    	if(check_match)
		    	{
			        activityLogBlink("RESYNC: Now adding user "+s_username+":"+socket.id+" in destinationids");
			        destinationid = socket.id;
			        destinationids.push(socket.id);
			        destinationnames.push(s_username);
			        destinantionnumtrees.push("0");
			        destinantionnumrocks.push("0");
			        destinantionnumplants.push("0");  
			        connectedUsers++;
			        sendSocketsList();
			        logDestinations();
		        }
		        else
		        {
		        	activityLogBlink("RESYNC: SKIPPING user "+s_username+":"+socket.id+" for its name");
		        	logDestinations();
		        }
                */
		    }
        });

        socket.on("register user",function(usrigid,usremail,ischallenge)
        {
            activityLog("register user "+usrigid,+" email "+usremail+" ischallenge "+ischallenge);
            s_userigid = usrigid;
            s_email = usremail;

            if(ischallenge == 1)
            {
                skip_farm_processing = 1;
                activityLog("skip_farm_processing " + skip_farm_processing);
            }
            else
            {
                skip_farm_processing = 0;
                activityLog("skip_farm_processing " + skip_farm_processing);
                return;
            }
            
            activityLog("register user --- id:"+s_userigid);
            activityLog("register user --- email:"+s_email);
            activityLog("register user --- ip:"+s_address);
            activityLog("Let's look for same ip address "+s_address+" and different email from "+s_email);

            /////////////////////////////////////////////////////////////////////////////////////////////////////
            DBConnectionManager.query("SELECT * FROM challenges WHERE date >= curdate() and ipaddr = '"+s_address+"' and email != '"+s_email+"' LIMIT 1", null, function(result,err)
            {
                if(err)
                {
                    activityLog("THERE WAS AN ERROR LOOKING FOR USER WITH SAME ADDRES AND DIFF EMAIL IN challenges");
                    activityLogErr(err);
                    return;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                if(Object.keys(res).length != 0)
                {
                    activityLog("Found cheater, check if present in Blacklist table");
                    DBConnectionManager.query("SELECT * FROM blacklist WHERE (date >= curdate() OR permanent = 1) AND email = '"+ s_email +"' LIMIT 1", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLog("THERE WAS AN ERROR LOOKING FOR IPADDRESS IN blacklist");
                            activityLogErr(err);
                            throw err;
                        }
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        if(Object.keys(res).length == 0)
                        {
                            activityLog("User was not found in Blacklist table, adding...");
                            DBConnectionManager.query("INSERT INTO blacklist (igid,ipaddr,email,banreason) VALUES ('"+s_userigid+"','"+s_address+"','"+s_email+"',3)", null, function(res,err)
                            {
                                if(err)
                                {
                                    activityLog("THERE WAS AN ERROR ADDING THIS USER TO BLACKLIST");
                                    socket.emit("get player data failed");
                                    activityLogErr(err);
                                    throw err;
                                }
                                activityLog("SUCCESSFULLY added user with email:"+ s_email +" to blacklist");
                                return;

                            });

                        }
                        else
                        {
                            activityLog("User already present in Blacklist table, skipping.");
                        }
                    });	

                }
                else
                {
                    activityLog("this user seems to be playing fair");
                    DBConnectionManager.query("SELECT * FROM ban_rules WHERE LOCATE(keyword,'"+s_email+"') > 0 OR LOCATE(network,'"+s_address+"') = 1", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLog("THERE WAS AN ERROR LOCATING THE KEYS IN BAN RULES");
                            activityLogErr(err);
                            throw err;
                        }
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        if(Object.keys(res).length > 0)
                        {
                            activityLog("Found cheater from ban_rules. Check if present in blacklist already.");
                            DBConnectionManager.query("SELECT * FROM blacklist WHERE (date >= curdate() OR permanent = 1) AND igid = '"+s_userigid+"' AND email = '"+s_email+"' LIMIT 1", null, function(result,err)
                            {
                                if(err)
                                {
                                    activityLog("THERE WAS AN ERROR LOOKING FOR THIS USER IN BLACKLIST");
                                    socket.emit("get player data failed");
                                    activityLogErr(err);
                                    throw err;
                                }
                                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                                if(Object.keys(res).length > 0)
                                {
                                    activityLog("USER WAS ALREADY PRESENT IN BLACKLIST");
                                }
                                else
                                {
                                    activityLog("USER WAS NOT FOUND IN BLACKLIST. ADDING...");
                                    DBConnectionManager.query("INSERT INTO blacklist (igid,ipaddr,email,banreason,permanent) VALUES ('"+s_userigid+"','"+s_address+"','"+s_email+"',1,1)", null, function(res,err)
                                    {
                                        if(err)
                                        {
                                            activityLog("THERE WAS AN ERROR ADDING THIS USER TO BLACKLIST");
                                            socket.emit("get player data failed");
                                            activityLogErr(err);
                                            throw err;
                                        }
                                        activityLog("SUCCESSFULLY added user to blacklist");

                                    });
                                }

                                
                            });

                            
                        }
                        else
                        {
                            activityLog("USER NOT A CHEATER CAN PLAY");
                        }
                    });
                }
            });
        
            ///////////////////////////////////////////////////////////////////////
        
            
        });
        //CALLED WHEN CLICKING NEW GAME OR CONTINUE OR PLAY CHALLENGE
        socket.on("get player data",function(usrname,usrigid)
        {
            //UTF here usrname is received encoded and stored decoded 
            s_username = usrname;//decodeURIComponent(usrname);
            activityLog("Will get player data for user " + usrigid + "and username (decoded)" + s_username );
            
            var _userigid = usrigid;
            var _socketid = socket.id;
            var _userid = 0;


            var _username = s_username;

            s_socketid = socket.id;

            if(!skip_farm_processing)
            {
	            var found = false;

	            for(var i = 0; i < destinationids.length; i++)
	            {
	                var v = destinationids[i];
	                if(v.indexOf(s_socketid) > -1)
	                {
	                    found = true;
	                    break;
	                }
	            }
	            if(!found)
	            {
                    activityLogBlink("CONNECTION: Adding user "+s_username+":"+socket.id+" to destinationids");
                    destinationids.push(socket.id);
                    destinationnames.push(s_username);
                    destinantionnumtrees.push("0");
                    destinantionnumrocks.push("0");
                    destinantionnumplants.push("0");
                    destinantionnumvisitors.push("0");
                    //activityLogGreen("SOCKET.JOIN -> "+encodeURIComponent(s_username));
                    //socket.join(encodeURIComponent(s_username));
                    activityLogGreen("SOCKET.JOIN -> "+s_username);
                    socket.join(s_username);
                    connectedUsers ++;
                    /*
	            	var check_name = s_username;
			    	var check_encoded = encodeURIComponent(check_name);
			    	var check_clean = check_encoded.replace(/%20/g," ");
			    	var check_match = (check_name === check_clean);
			    	if(check_match)
			    	{
		            	activityLogBlink("CONNECTION: Adding user "+s_username+":"+socket.id+" to destinationids");
		                destinationids.push(socket.id);
		                destinationnames.push(s_username);
		                destinantionnumtrees.push("0");
		                destinantionnumrocks.push("0");
		                destinantionnumplants.push("0");
		                destinantionnumvisitors.push("0");
		                activityLogGreen("SOCKET.JOIN -> "+encodeURIComponent(s_username));
		                socket.join(encodeURIComponent(s_username));
		                connectedUsers ++;
		            }
		            else
		            {
		            	activityLogBlink("CONNECTION: SKIPPING user "+s_username+":"+socket.id+" for its name");
		            }
                    */
	            }
	            logDestinations();
	        }
            
            //ask for username data
            activityLog("CONNECTING to table USERS to FIND user " + _userigid);
            DBConnectionManager.query("SELECT * FROM users WHERE igid = '"+_userigid+"'", null, function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while LOOKING FOR user " + usrigid + ". " + err);
                    socket.emit("get player data failed");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                //if not found add it to the db
                if(Object.keys(res).length == 0)
                {
                    activityLog("NO DATA FOUND for user " + _userigid);
                    activityLog("Adding user " + _userigid + " to table USERS");
                    //UTF here _username is sent already decoded at the beginning and then it's always sent back encoded to index.html
                    var escapedusername = _username.replace(/'/g, "\\'");//this escapes single quotes -> \'
                    DBConnectionManager.query("INSERT INTO users (username,igid,userdata,stats,deathdata) VALUES ('"+escapedusername+"','"+_userigid+"','100:100:0:0:1:1:9201000000000000:9201000000000000:2E01000000000000:0:0:0:1:1','','')", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLog("There was an ERROR while ADDING user " + usrigid + " to USERS. " + err);
                            socket.emit("get player data failed");
                            activityLogErr(err);
                            throw err;
                        }
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        _userid = res.insertId;
                        s_userid = res.insertId;
                        s_userdata = "100:100:0:0:1:1:9201000000000000:9201000000000000:2E01000000000000:0:0:0:1:1";
                        s_userstats = "";
                        s_deathdata = "";

                        activityLog("ADDING user " + _userigid + " to table FARMS");

                        DBConnectionManager.query("INSERT INTO farms (userid,size,unlockablesstring) VALUES ('"+_userid+"','45:35','9201000000000000')", null, function(res,err)
                        {
                            if(err)
                            {
                                activityLog("There was an ERROR while ADDING user " + usrigid + " to FARMS. " + err);
                                socket.emit("get player data failed");
                                activityLogErr(err);
                                throw err;
                                return;
                            }
                            activityLog("ADDED user " + _userigid + " to table FARMS");
                            s_farmstring = "";
                            s_pickables = "";
                            s_interactables = "";
                            s_items = "";
                            s_missions = "";
                            s_house_pickables = "";
                            s_house_interactables = "";
                            //return the values
                            //socket.emit('got player data',encodeURIComponent(_username),_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                            socket.emit('got player data',_username,_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                        });
                    });

                }
                else//just read it and go on
                {
                    activityLog("FOUND DATA for user " + _userigid);
                    _userid = res[0].userid;
                    s_userid = res[0].userid;
                    s_userdata = res[0].userdata;
                    s_userstats = res[0].stats;
                    s_deathdata = res[0].deathdata;

                    /* Skip farm reading because it takes 20 second and is no good for challenges... */
                    if( skip_farm_processing )
                    {
                        activityLog("skip_farm_processing = 1, There was NO SAVED FARM for user " + _userigid);
                        s_farmstring = "";
                        s_pickables = "";
                        s_interactables = "";
                        s_items = "";
                        s_missions = "";
                        s_house_pickables = "";
                        s_house_interactables = "";

                        //socket.emit('got player data',encodeURIComponent(_username),_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                        socket.emit('got player data',_username,_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                    }
                    else
                    {
                        activityLog("CONNECTING to table FARMS to FIND A RECORD " + _userigid + " with userid " + _userid);
                        DBConnectionManager.query("SELECT userid FROM farms WHERE userid = '"+ _userid +"'", null, function(result,err)
                        {
                            if(err)
                            {
                                activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into farms. " + err);
                                socket.emit("get player data failed");
                                activityLogErr(err);
                                throw err;
                            }
                            var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                            /*activityLog("found " + Object.keys(res).length + " db objects...")*/
                            if(Object.keys(res).length == 0)
                            {
                                activityLog("There was NO SAVED FARM for user " + _userigid);
                                s_farmstring = "";
                                s_pickables = "";
                                s_interactables = "";
                                s_items = "";
                                s_missions = "";
                                s_house_pickables = "";
                                s_house_interactables = "";

                                if(!skip_farm_processing)
                                {
                                    DBConnectionManager.query("INSERT INTO farms (userid,size,unlockablesstring) VALUES ('"+_userid+"','45:35','9201000000000000')", null, function(res,err)
                                    {
                                        if(err)
                                        {
                                            activityLog("There was an ERROR while ADDING user " + usrigid + " to FARMS. " + err);
                                            socket.emit("get player data failed");
                                            activityLogErr(err);
                                            throw err;
                                        }
                                        activityLog("ADDED user " + _userigid + " to table FARMS");
                                        s_farmstring = "";
                                        s_pickables = "";
                                        s_interactables = "";
                                        s_items = "";
                                        s_missions = "";
                                        s_house_pickables = "";
                                        s_house_interactables = "";
                                        //return the values
                                        //socket.emit('got player data',encodeURIComponent(_username),_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                                        socket.emit('got player data',_username,_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                                    });
                                }
                                else
                                {
                                    //socket.emit('got player data',encodeURIComponent(_username),_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                                    socket.emit('got player data',_username,_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                                }
                            }
                            else//if found return all values
                            {
                                activityLog("NOW CONNECTING AGAIN to table FARMS to FIND ALL DATA FOR USER " + _userigid + " with userid " + _userid);
                                DBConnectionManager.query("SELECT * FROM farms WHERE userid = '"+ _userid +"'", null, function(result,err)
                                {
                                    if(err)
                                    {
                                        activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into farms. " + err);
                                        socket.emit("get player data failed");
                                        activityLogErr(err);
                                        throw err;
                                        return;
                                    }
                                    var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                                    activityLog("SENDING BACK ALL FARM DATA FOR USER " + _userigid);
                                    /*INFLATE*/
                                    s_farmstring = res[0].farmstring;
                                    if( s_farmstring != "" && s_farmstring != null)
                                    {
                                        s_farmstring = zlib.inflateSync(new Buffer(res[0].farmstring, 'base64')).toString()
                                    }
                                    if(s_farmstring == null)
                                    {
                                        s_farmstring = "";
                                    }
                                    //activityLog("inflated farm string:"+s_farmstring.substr(0,40)+"...");
                                    s_pickables = res[0].pickablesstring;
                                    s_interactables = res[0].interactablesstring;
                                    if(s_interactables != "" && s_interactables != null)
                                    {
                                        s_interactables = zlib.inflateSync(new Buffer(res[0].interactablesstring, 'base64')).toString();
                                    }
                                    if(s_interactables == null)
                                    {
                                        s_interactables = "";
                                    }
                                    s_items = res[0].itemsstring;
                                    s_missions = res[0].missionsstring;
                                    s_house_pickables = res[0].housepickablesstring;
                                    s_house_interactables = res[0].houseinteractablesstring;
                                    if(s_farmstring == "" || s_pickables == "" || s_interactables == "")
                                    {
                                        activityLog("WARNING! Corrupted data. Starting new game...");
                                        s_userdata = "100:100:0:0:1:1:9201000000000000:9201000000000000:2E01000000000000:0:0:0:1:1";
                                        s_userstats = "";
                                        s_deathdata = "";
                                        //socket.emit('got player data',encodeURIComponent(_username),_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                                        socket.emit('got player data',_username,_userid,_socketid,s_userdata,s_userstats,s_deathdata,"","","","","","9201000000000000","","");
                                    }
                                    else
                                    {
                                        /*HERE BECAUSE WE MUST PASS THE INFLATED FARMSTRING*/
                                        //socket.emit('got player data',encodeURIComponent(_username),_userid,_socketid,s_userdata,s_userstats,s_deathdata,s_farmstring, res[0].pickablesstring, s_interactables, res[0].itemsstring, res[0].missionsstring, res[0].unlockablesstring,res[0].housepickablesstring,res[0].houseinteractablesstring);
                                        socket.emit('got player data',_username,_userid,_socketid,s_userdata,s_userstats,s_deathdata,s_farmstring, res[0].pickablesstring, s_interactables, res[0].itemsstring, res[0].missionsstring, res[0].unlockablesstring,res[0].housepickablesstring,res[0].houseinteractablesstring);
                                    }
                                });
                            }
                        });
                    }
                }

            });

            

        });


        socket.on("clear player data",function(usrigid)
        {
            //activityLog("server: clear player data");
            var _userigid = usrigid;
            var found = false;

            //ask for username data
            DBConnectionManager.query("SELECT * FROM users WHERE igid = '"+_userigid+"'", null, function(result,err)
            {
                if(err)
                {
                    //activityLog("server: error finding user " + err);
                    socket.emit("delete player data failed");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                //if found , we delete
                if(Object.keys(res).length > 0)
                {
                    var _userid = res[0].userid;
                    s_userid = res[0].userid;
                    if(!skip_farm_processing) {
                        //activityLog("server: delete farm");
                        DBConnectionManager.query("DELETE FROM farms WHERE userid = '"+_userid+"'", null, function(res,err)
                        {
                            if(err)
                            {
                                activityLog("server: error deleting farm " + err);
                                socket.emit("delete player data failed");
                                activityLogErr(err);
                                throw err;
                            }
                            else
                            {
                                activityLog("server: deleted farm");
                            }
                        });

                    }
                    
                    DBConnectionManager.query("DELETE FROM users WHERE userid = '"+_userid+"'", null, function(res,err)
                    {
                        if(err)
                        {
                            activityLog("server: error deleting user " + err);
                            socket.emit("delete player data failed");
                            activityLogErr(err);
                            throw err;
                        }
                        else
                        {
                            activityLog("server: deleted user");
                        }
                        
                    });
                }
            });
            
            

        });

        socket.on('send userdata string', function(usrid,fstr,usrname)
        {
            DBConnectionManager.query("UPDATE users SET userdata = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            });	
        });

        socket.on('send userstats string', function(usrid,fstr,usrname)
        {	
            DBConnectionManager.query("UPDATE users SET stats = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            });
        });

        socket.on('send deathdata string', function(usrid,fstr,usrname)
        {
            DBConnectionManager.query("UPDATE users SET deathdata = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            });
        
        });

        socket.on('send farm string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_farmstring = fstr;	
            }
            if( !skip_farm_processing )
            {
                activityLog("indexjs:server: send farm string " + usrname + " " + usrid);
                /*DEFLATED*/
                var deflated = zlib.deflateSync(fstr).toString('base64');
                /*var inflated = zlib.inflateSync(new Buffer(deflated, 'base64')).toString();
                activityLog("deflated:"+deflated);
                activityLog("inflated:"+inflated);
                */
                /*
                var buf2 = new Buffer(buf, 'base64');
                zlib.unzip(buf2, function(err, buffer_res) {
                if (!err) {
                    activityLog(buffer_res.toString());
                }
                });
                */
                DBConnectionManager.query("UPDATE farms SET farmstring = '"+deflated+"' WHERE userid = "+usrid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    activityLog("updated farmstring " + usrid);
                    
                    socket.broadcast.to(usrname).emit('farm string to store',fstr);
                });
            }	
                
        });

        socket.on('send pickables string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_pickables = fstr;	
            }
            if( !skip_farm_processing )
            {
                //activityLog("indexjs:server: send pickables string " + usrname);
                var queryStr = "UPDATE farms SET pickablesstring = '"+fstr+"' WHERE userid = "+usrid;
                console.log("launching query:", queryStr);
                DBConnectionManager.query(queryStr, null, function(res,err)
                {
                    if(err)
                    {
                        console.log( err );
                        return;
                    }
                    activityLog("updated pickables string " + usrid);
                    activityLog("emit updated pickables string to" + usrname);
                    socket.broadcast.to(usrname).emit('pickables string to store',fstr);
                    
                });	
            
            }	
            
        });

        socket.on('send house pickables string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_house_pickables = fstr;	
            }

            if( !skip_farm_processing )
            {
                //activityLog("indexjs:server: send house pickables string " + usrname);
                DBConnectionManager.query("UPDATE farms SET housepickablesstring = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    //activityLog("updated house pickables string " + usrid);
                });	
                socket.broadcast.to(usrname).emit('house pickables string to store',fstr);
                    
            }
            
        });

        socket.on('send interactables string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_interactables = fstr;	
            }

            if( !skip_farm_processing )
            {
                //activityLog("indexjs:server: send house pickables string " + usrname);
                /*DEFLATE*/
                var deflated = zlib.deflateSync(fstr).toString('base64');
                DBConnectionManager.query("UPDATE farms SET interactablesstring = '"+deflated+"' WHERE userid = "+usrid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    //activityLog("updated house pickables string " + usrid);
                });	
                socket.broadcast.to(usrname).emit('interactables string to store',fstr);
            }
            
        });

        socket.on('send num trees string', function(usrid,npstr,usrname)
        {
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
            var i = 0;
            destinationids.forEach(function(v)
            {
                //activityLog(usrid + " " + v);
                if(usrid == v)
                {
                    //activityLog("destinantionnumtrees["+i+"] = " + npstr);
                    destinantionnumtrees[i] = npstr;   
                }
                else
                {
                    i++;
                }
            });
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
        });

        socket.on('send num rocks string', function(usrid,npstr,usrname)
        {
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
            var i = 0;
            destinationids.forEach(function(v)
            {
                //activityLog(usrid + " " + v);
                if(usrid == v)
                {
                    //activityLog("destinantionnumrocks["+i+"] = " + npstr);
                    destinantionnumrocks[i] = npstr;   
                }
                else
                {
                    i++;
                }
            });
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
        });

        socket.on('send num plants string', function(usrid,npstr,usrname)
        {
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
            var i = 0;
            destinationids.forEach(function(v)
            {
                //activityLog(usrid + " " + v);
                if(usrid == v)
                {
                    //activityLog("destinantionnumplants["+i+"] = " + npstr);
                    destinantionnumplants[i] = npstr;   
                }
                else
                {
                    i++;
                }
            });
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
        });

        socket.on('send num visitors string', function(usrid,npstr,usrname)
        {
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
            var i = 0;
            destinationids.forEach(function(v)
            {
                //activityLog(usrid + " " + v);
                if(usrid == v)
                {
                    //activityLog("destinantionnumvisitors["+i+"] = " + npstr);
                    destinantionnumvisitors[i] = npstr;   
                }
                else
                {
                    i++;
                }
            });
            //activityLog("..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*..*");
        });

        socket.on('send house interactables string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_house_interactables = fstr;	
            }
            if( !skip_farm_processing )
            {
                executeMysqlStatement(( "UPDATE farms SET houseinteractablesstring = '"+fstr+"' WHERE userid = "+usrid ));

                socket.broadcast.to(usrname).emit('house interactables string to store',fstr);
            }
        });

        
        socket.on('send items string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_items = fstr;	
            }
            if( !skip_farm_processing )
                executeMysqlStatement(( "UPDATE farms SET itemsstring = '"+fstr+"' WHERE userid = "+usrid ));
        });

        socket.on('send missions string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_missions = fstr;	
            }

            if( !skip_farm_processing )
                executeMysqlStatement(( "UPDATE farms SET missionsstring = '"+fstr+"' WHERE userid = "+usrid ));			
        });

        socket.on('send unlockables string', function(usrid,fstr,usrname)
        {
            if( !skip_farm_processing )
            {	
                var querystr = ("UPDATE farms SET unlockablesstring = '"+fstr+"' WHERE userid = "+usrid );
                activityLog("sending query : " + querystr)
                executeMysqlStatement( querystr );
            }
        });

        socket.on("store all in socket",function(fstr,pstr,istr,usrname)
        {
            if(usrname == s_username)
            {
                //activityLog("indexjs:server: store all strings in same client");
                s_farmstring = fstr;
                s_pickables = pstr;
                s_interactables = istr;
            }
            else
            {
                //activityLog("indexjs:server: store all strings in another client " + usrname);
                socket.broadcast.to(usrname).emit('all strings to store',fstr,pstr,istr);
            }
            
        });  	
        /*THIS FUNCTION STORES ALL ITEMS IN THE SAME CLIENT SOCKET*/
        socket.on("store items in socket",function(fstr)
        {
            //activityLog("indexjs:server: store all items in same client");
            s_items = fstr;
        });  	

        socket.on("store all strings",function(fstr,pstr,istr,usrname)
        {
            //activityLog("indexjs:server: store all strings");
            s_farmstring = fstr;
            s_pickables = pstr;
            s_interactables = istr;
        });

        socket.on("store farm string",function(str)
        {
            //activityLog("indexjs:server: store farm string");
            s_farmstring = str;
        });
        /*
        socket.on("reach socket for username",function(str)
        {
            activityLog("indexjs:server: reach socket for username");
            io.to(str).emit("get socket username", s_username);
        });
        */
        socket.on("store pickables string",function(str)
        {
            //activityLog("indexjs:server: store pickables string");
            s_pickables = str;
        });

        socket.on("store house pickables string",function(str)
        {
            //activityLog("indexjs:server: store house pickables string");
            s_house_pickables = str;
        });

        socket.on("store interactables string",function(str)
        {
            //activityLog("indexjs:server: store interactables string");
            s_interactables = str;
        });

        socket.on("store house interactables string",function(str)
        {
            //activityLog("indexjs:server: store house interactables string");
            s_house_interactables = str;
        });

        socket.on("send item to user stash", function(usrid,iid,q,d)
        {
            //activityLog("indexjs:server: send item to user stash");
            io.to(usrid).emit("add item to stash", iid,q,d);
        });

        socket.on('get user farm', function(usrid,sock)
        {
            if( !skip_farm_processing )
            {
                DBConnectionManager.query("SELECT * FROM farms WHERE userid = '"+usrid+"'", null, function(result,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }	
                    var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                    var farmlen = Object.keys(res).length;
                    //activityLog("farmlen " + farmlen);
                    //activityLog(res);
                    if(farmlen == 1)
                    {
                        destinationid = sock;
                        //activityLog("join room " + destinationid);
                        activityLogGreen("SOCKET.JOIN -> "+destinationid);
                        socket.join(destinationid);
                        /*INFLATE*/
                        var fstr = res[0].farmstring;
                        if(fstr != "" && fstr != null)
                        {
                            fstr = zlib.inflateSync(new Buffer(res[0].farmstring, 'base64')).toString();
                        }
                        if(fstr == null)
                        {
                            fstr = "";
                        }
                        var istr = res[0].interactablesstring;
                        if(istr != "" && istr != null)
                        {
                            istr = zlib.inflateSync(new Buffer(res[0].interactablesstring, 'base64')).toString();
                        }
                        if(istr == null)
                        {
                            istr = "";
                        }
                        activityLog("inflated farm string:"+fstr);
                        socket.emit("got user farm",fstr,res[0].pickablesstring,istr);
                    }	
                    
                });
                    
            }
            
        });

        socket.on('leave room', function(sock)
        {
        	activityLogGreen("SOCKET.LEAVE -> "+sock);
            socket.leave(sock);
        });

        socket.on('join room', function(sock)
        {
            //activityLog("---------------------------------------------------------");
            //activityLog('indexjs:server: join room');
            //activityLog("leave room " + destinationid);
            //activityLog("join room " + sock);
            //activityLog("---------------------------------------------------------");
            activityLogGreen("SOCKET.LEAVE -> "+destinationid);
            socket.leave(destinationid);
            socket.to(destinationid).emit("leave farm", s_socketid);
            destinationid = sock;
            activityLogGreen("SOCKET.JOIN -> "+destinationid);
            socket.join(destinationid);
        });
        //GOING BACK TO MY FARM 2
        socket.on('update farm', function()
        {
            //activityLog('indexjs:server: update farm');
            socket.emit("update farm",s_username,s_userid,s_socketid,s_farmstring,s_pickables,s_interactables);
        });


    socket.on('reconnect', function()
    {
    	activityLogBlink("RECONNECTION EVENT");
    	/*
        //activityLog('indexjs:server: client reconnected ' + socketid + " " + socket.id);
        var i = 0;
        destinationids.forEach(function(v)
        {
            if(socket.id == v)
            {

                destinationids.splice(i, 1);  
                destinationnames.splice(i, 1); 
                destinantionnumtrees.splice(i, 1);
                destinantionnumrocks.splice(i, 1);
                destinantionnumplants.splice(i, 1);  
            }
            else
            {
                i++;
            }
        });
        destinationid = socket.id;
        destinationids.push(socket.id);
        destinationnames.push(s_username);
        destinantionnumtrees.push("0");
        destinantionnumrocks.push("0");
        destinantionnumplants.push("0");  
        sendSocketsList();
        socket.emit('client reconnected', socket.id);
        
        activityLog('client reconnected , socket: ' + socket.id);
        */
    });

    socket.on('disconnect', function()
    {
        
        var sid = socket.id;

        activityLog("DISCONNECTED user " + s_userid);
        
        if(!skip_farm_processing && s_userid != 0) // Se e' 0 questa non DEVE essere chiamata!
        {
            if(s_farmstring != null && s_pickables != null && s_interactables != null)
            {
                var fstr = s_farmstring;
                fstr = fstr.replace(/00000000000000000000000/g,"_");
                fstr = fstr.replace(/0000000000/g,"%");

                //activityLog(fstr);

                var pstr = s_pickables;
                pstr = pstr.replace(/000000/g,"_");
                pstr = pstr.replace(/A666C696E743A313A3001/g,"%");

                //activityLog(pstr);

                var istr = s_interactables;
                istr = istr.replace(/000000/g,"_");
                istr = istr.replace(/303A313A313A303A3/g,"%");
                istr = istr.replace(/70696E653A3/g,"!");
                istr = istr.replace(/77696C6467726173733A/g,"&");
                istr = istr.replace(/6F626A5F696E74657261637461626C655F/g,",");

                activityLog("DISCONNECTED user "+s_userid+" has data to save.");
                activityLog("UPDATING table farms for user " + s_userid);
                /*DEFLATE*/
                var deflated = zlib.deflateSync(fstr).toString('base64');
                var deflated2 = zlib.deflateSync(istr).toString('base64');
                executeMysqlStatement( ( "UPDATE farms SET farmstring = '"+deflated+"', pickablesstring = '"+pstr+"', interactablesstring = '"+deflated2+"'  WHERE userid = "+s_userid ) );

            }
        }

        activityLogBlink("DISCONNECTION: Looking for user "+s_username+":"+sid+" in destinations");
        var i = 0;
        destinationids.forEach(function(v)
        {
            if(sid == v)
            {
            	activityLogBlink("DISCONNECTION: found user "+s_username+":"+sid+" in destinations -> removing...");
                destinationids.splice(i, 1);
                destinationnames.splice(i, 1);   
                destinantionnumtrees.splice(i, 1);
                destinantionnumrocks.splice(i, 1);
                destinantionnumplants.splice(i, 1);
                connectedUsers --;
            }
            else
            {
                i++;
            }
        });
        logDestinations();
        sendSocketsList();
        socket.removeAllListeners();
        socket.disconnect(true);
        io.emit('client disconnected', sid);
        
        activityLog('Got disconnect!');
        
        activityLog('client disconnected, socket: ' + sid);
    });
    
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
        //activityLog('indexjs:message: ' + msg);
    });

    socket.on('get connected users', function(){
        //activityLog("indexjs:server:get connected users");
        io.emit('update users connected', connectedUsers);
    });
    ///upon receiving this message, emit the socket id to the calling client
    socket.on('get client id', function(){
        //activityLog("indexjs:server:socket:emit client id");
        socket.emit('get client id', socket.id);
    });

    socket.on('send char resting', function(id,val)
    {
        //activityLog("indexjs:server:sendCharResting " + id + " " + val);
        socket.to(destinationid).emit('send char resting',id,val);
    });

    socket.on('send char data', function(xx,yy,ss1,ss2,ss3,ss4,ii,sx,sy,id,nam,sta){
        //activityLog("indexjs:server:"+xx+":"+yy+":"+ss1+":"+ss2+":"+ss3+":"+ss4+":"+ii+":"+sx+":"+sy+":"+id+":"+nam+":"+sta);
        //activityLog("indexjs:server:sending char data");
        socket.to(destinationid).emit('send char data',xx,yy,ss1,ss2,ss3,ss4,ii,sx,sy,id,nam,sta);
    });

    socket.on('remove pickable', function(xx,yy,oid){
        //activityLog("indexjs:server:remove pickable"+xx+":"+yy+":"+oid+":"+destinationid);
        socket.to(destinationid).emit('remove pickable',xx,yy,oid);
    });

    socket.on('remove interactable', function(xx,yy,oid){
        //activityLog("indexjs:server:remove interactable:"+xx+":"+yy+":"+oid+":"+destinationid);
        socket.to(destinationid).emit('remove interactable',xx,yy,oid);
    });

    socket.on('event perform interactable', function(xx,yy,iid,oid,evt,evn){
        //activityLog("indexjs:server:event perform interactable "+xx+":"+yy+":"+iid+":"+oid+":"+evt+":"+evn+":"+destinationid);
        socket.to(destinationid).emit('event perform interactable',xx,yy,iid,oid,evt,evn);
    });

    socket.on('drop pickable', function(xx,yy,pid,quant){
        //activityLog("indexjs:server:drop pickable"+xx+":"+yy+":"+pid+":"+quant+":"+destinationid);
        socket.to(destinationid).emit('drop pickable',xx,yy,pid,quant);
    });

    socket.on('magnet pickable', function(xx,yy,pid,sid){
        //activityLog("indexjs:server:magnet pickable"+xx+":"+yy+":"+pid+":"+sid+":"+destinationid;
        //socket.broadcast.to(destinationid).emit('magnet pickable',xx,yy,pid,sid);
        socket.to(destinationid).emit('magnet pickable',xx,yy,pid,sid);
    });

    socket.on('change ground tile', function(col,row,type){
        activityLog("indexjs:server:change ground tile"+col+":"+row+":"+type+":"+destinationid);
        socket.to(destinationid).emit('change ground tile',col,row,type);
    });

    socket.on('sow plant', function(col,row,obj,str){
        activityLog("indexjs:server:sow plant"+col+":"+row+":"+obj+":"+str+":"+destinationid);
        socket.to(destinationid).emit('sow plant',col,row,obj,str);
    });

    socket.on('water tile', function(col,row){
        activityLog("indexjs:server:water tile"+col+":"+row+":"+destinationid);
        socket.to(destinationid).emit('water tile',col,row);
    });

    socket.on('send emote', function(usr,type){
        //activityLog("indexjs:server:send emote"+usr+":"+type+":"+destinationid);
        socket.to(destinationid).emit('send emote',usr,type);
    });

    socket.on('place structure', function(xx,yy,obj,id){
        activityLog("indexjs:server:place structure"+xx+":"+yy+":"+obj+":"+id+":"+destinationid);
        socket.to(destinationid).emit('place structure',xx,yy,obj,id);
    });

    socket.on('start structure job', function(xx,yy,obj,id){
        activityLog("indexjs:server:start structure job"+xx+":"+yy+":"+obj+":"+id+":"+destinationid);
        socket.to(destinationid).emit('start structure job',xx,yy,obj,id);
    });

    socket.on('draw equipped', function(sid,xx,yy,ia,si,ii,xs,ys){
        socket.to(destinationid).emit('draw equipped',sid,xx,yy,ia,si,ii,xs,ys);
    });

        socket.on('kick all players', function(){
            //activityLog("kick all players");
            socket.to(destinationid).emit('kick all players');
        });

        socket.on('get user history', function(igid)
        {
            if(debug == 1)
            {
                activityLog("got user history 0-1-1-0-10 DEBUG OK");
                socket.emit("got user history",0,1,1,0,10);
                return;
            }
            activityLog("sending json for GET USER HISTORY { user_id:" + igid + " }");

            const request = require('request');
            var postigid = igid;
            request.post('https://www.indiegala.com/feudalife_get_user_history', {
                json: {
                    user_id: postigid
                }
            }, (error, res, body) => {
                if (error) {
                    activityLog("--ERROR---------get user history-------------------------");
                    activityLog(error);
                    activityLog("--ERROR-----------------------------------------------");
                    return;
                }
                activityLog("--NOERROR------------get user history------------------");
                activityLog("RESPONSE Status code:" + res.statusCode );
                if(res.statusCode != 200)
                {
                    activityLog("WILL EMIT get user history error");
                }
                else
                {
                    activityLog("status " + body.status);
                    activityLog("text " + body.text);
                    activityLog("new user " + body.new_user);
                    activityLog("store purchase " + body.store_purchase);
                    activityLog("bundle purchase " + body.bundle_purchase);
                    activityLog("giveaways created " + body.giveaways_created);
                    activityLog("steam group member " + body.steamgroup_member);
                    
                    var status = body.status;
                    var text = body.text;
                    var new_user = body.new_user;
                    var store_purchase = body.store_purchase;
                    var bundle_purchase = body.bundle_purchase;
                    var keys_won = 0;
                    var giveaway_created = body.giveaways_created;
                    var steamgroup_member = body.steamgroup_member;

                    DBConnectionManager.query("SELECT COUNT(id) FROM challenges WHERE igid = '"+igid+"' AND won = '1' AND cheat IS NULL AND DATE(date) >= NOW() - INTERVAL 1 DAY;", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLogErr(err);
                            activityLog("WILL EMIT get user history mysql error");
                            return;
                        }
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        activityLog("MYSQL RESPONSE:");
                        for (var key in res[0])
                        {
                            activityLog(key+":"+ res[0][key]);
                            keys_won = res[0][key];
                        }
                        //debug
                        //keys_won = 2;
                        activityLog("WILL EMIT get user ("+igid+") history callback status: " + status + " text: " + text + " new_user: " + new_user + " store_purchase: " + store_purchase + " bundle_purchase: " + bundle_purchase + " keys_won: " + keys_won + " giveaway_created: " + giveaway_created);
                        socket.emit("got user history",new_user,store_purchase,bundle_purchase,keys_won,giveaway_created,steamgroup_member);
                    });
                
                }
            });
        });

        socket.on('insert record', function(igid,challengetype,d,e,g,p)
        {
            var ct = challengetype;
            activityLog("WILL Insert record into challenges for user " + igid + " and challengetype " + ct + " on platform " + p);
            //executeMysqlStatement( ( "INSERT INTO challenges (igid,challengetype,tottime,csd,malussecondsremoved,galacredit,ipaddr,email) VALUES ('"+igid+"','"+ct+"','"+(d*s_csd)+"','"+s_csd+"','"+e+"','0','"+s_address+"','"+s_email+"')" ) );
            DBConnectionManager.query("INSERT INTO challenges (igid,challengetype,tottime,csd,malussecondsremoved,galacredit,ipaddr,email,platform) VALUES ('"+igid+"','"+ct+"','"+(d*s_csd)+"','"+s_csd+"','"+e+"','0','"+s_address+"','"+s_email+"','"+p+"')", null, 
            function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while ADDING user " + usrigid + " to CHALLENGES. " + err);
                    socket.emit("get player data failed");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                s_challenge_id = res.insertId;
                activityLog("challenge ID = "+ s_challenge_id);
            });
        });

        function createDateString(d)
        {
            var year = d.getFullYear().toString();
            var month = (d.getMonth()+1).toString();
            if(month.length == 1){month = "0"+month;}
            var day = d.getDate().toString();
            if(day.length == 1){day = "0"+day;}
            var hours = d.getHours().toString();
            if(hours.length == 1){hours = "0"+hours;}
            var minutes = d.getMinutes().toString();
            if(minutes.length == 1){minutes = "0"+minutes;}
            var seconds = d.getSeconds().toString();
            if(seconds.length == 1){seconds = "0"+seconds;}
            var str = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
            return str;
        }


        function sendBadge(igid,challengetype,won,time,gold,a,b,c,d,e,points)
        {
            activityLog("WILL SELECT record from challenges for user " + igid);
            DBConnectionManager.query("SELECT * FROM challenges WHERE igid = '"+igid+"' order BY date DESC LIMIT 1", null, function(result,err) {
                if(err) {
                    activityLog("THERE WAS AN ERROR");
                    socket.emit("challenge callback error");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                if(Object.keys(res).length == 0) {
                    activityLog("USER RECORD NOT FOUND! IT'S A CHEATER!");
                    //ban callback
                    socket.emit("challenge callback", "fail", "inconsistent data", "", "", "");
                    return;
                }
                else {
                    var startdate = new Date(res[0].startdate);
                    var tottime = res[0].tottime;
                    var finishdate = new Date();
                    var elapsedtime = (finishdate.getTime() - startdate.getTime())/1000;
                    var rowid = res[0].id;
                    var diff = elapsedtime - tottime;

                    activityLogBlink("::::::::::::::::::::::::::::::::::::::::diff:"+diff+" s_tct:"+s_tct);
                    if(diff < -s_tct && challengetype == 4)
                    {
                    	activityLogBlink("::::::::::::::::::::::::::::::::::::::::player died with archers");
                    }
                    if( diff > s_tct || (diff < -s_tct && challengetype != 4)) {//elapsedtime < tottime - 1.9 || elapsedtime > tottime + 1.9)
                        //ban callback
                        activityLog("WARNING! elapsedtime:" + elapsedtime + " tottime:" + tottime + " tottime*csd:" + tottime*s_csd);
                        socket.emit("challenge request fail", "There was some trouble reaching the server.\nPlease ensure you have a stable connection during the challenge.\nIf you experience this issue too frequently please contact support.", "https://www.indiegala.com/profile");
                        var final_e_string = createDateString(finishdate);
                        DBConnectionManager.query("UPDATE challenges SET won = '"+won+"', mywon='0', cheat='2', time = '"+(time*s_csd)+"', gold = '"+gold+"', earnedgalacreds = '0.00', interactions = '"+a+"', finishedinteractions = '"+b+"', walkframes = '"+c+"', elapsedservertime = '"+elapsedtime+"', finishdate = '"+final_e_string+"', points = '"+points+"' WHERE (id = "+rowid+")", null, function(res,err) {
                            if(err) {
                                activityLogErr(err);
                                throw err;
                            }
                        });
                        return;
                    }
                    else {
                        activityLog("--------------------------------------------------------");
                        activityLog("sending json{ user_id:" + igid + " ip:" + s_address + " challenge:" + challengetype + " won:" + won + " win_time:" + time + "gold:" + gold + " v_a: " + a + " v_b:" + b + " v_c:" + c + " v_d:" + d +" malus_seconds_removed:" + e +" check:}");
                        activityLog("--------------------------------------------------------");
                        const request = require('request');
                        var challenge_time = 0;
                        challenge_time = d;
                        var victory_time = 0.0;
                        victory_time = time;
                        var postigid = igid;
                        var ct = challengetype;
                        request.post('https://www.indiegala.com/feudalife_challenge_req', 
                        {
                            json: {
                                user_id: postigid,
                                ip_address: s_address,
                                challenge_type: ct,
                                has_won: won,
                                win_time: victory_time,
                                earned_gold: gold,
                                tot_time: challenge_time,
                                v_a:a,
                                v_b:b,
                                v_c:c,
                                malus_seconds_removed:e,
                                check:"",
                                challenge_id:s_challenge_id
                            }
                        }, (error, res, body) => {
                            if (error) {
                                activityLog("--ERROR---------send badge----------------------------");
                                activityLog(error);
                                activityLog("--ERROR-----------------------------------------------");
                                socket.emit("challenge callback error");
                                return;
                            }

                            activityLog("--NOERROR--------------send badge------------------");
                            activityLog("RESPONSE Status code:" + res.statusCode );
                            if(res.statusCode != 200) {
                                    activityLog("WILL EMIT challenge error");
                                    socket.emit("challenge callback error");
                                    return;
                            }
                            else {
                                var status = body.status;//string
                                var text = body.text;//string
                                var is_galacredit_winner = "";//bool
                                var is_new_user = "";//bool
                                var earned_amount = "";//float
                                var galac = "0.00";
                                var username = body.username;
                                var userimage = body.userimage;

                                if(body.galacredit_challenge_response != undefined)
                                {
                                    is_galacredit_winner = body.galacredit_challenge_response.is_galacredit_winner;
                                    is_new_user = body.galacredit_challenge_response.is_new_user;
                                    earned_amount = body.galacredit_challenge_response.earned_amount;
                                    if(is_galacredit_winner)
                                    {
                                        galac = earned_amount;
                                    }
                                }
                                activityLog("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.");
                                activityLog("body:"+body);
                                activityLog("status:"+status);
                                activityLog("text:"+text);
                                activityLog("is_galacredit_winner:"+is_galacredit_winner);
                                activityLog("is_new_user:"+is_new_user);
                                activityLog("earned_amount:"+earned_amount);
                                activityLog("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.");
                                activityLog("WILL EMIT challenge callback status: " + status + " text: " + text);
                                socket.emit("challenge callback", status, text, is_galacredit_winner, is_new_user, earned_amount);
                                var w = won;
                                if(status == "fail")
                                {
                                    w = 0;
                                    var error_type = body.error_type;
                                    var error_code = body.error_code;
                                    activityLog("error_type:"+error_type);
                                    activityLog("error_code:"+error_code);
                                }
                                var final_d_string = createDateString(finishdate);
                                DBConnectionManager.query("UPDATE challenges SET won = '"+w+"', time = '"+(time*s_csd)+"', gold = '"+gold+"', earnedgalacreds = '"+galac+"', interactions = '"+a+"', finishedinteractions = '"+b+"', walkframes = '"+c+"', elapsedservertime = '"+elapsedtime+"', finishdate = '"+final_d_string+"', points = '"+points+"' WHERE (id = "+rowid+")", null, function(res,err) {
                                    if(err)
                                    {
                                        activityLogErr(err);
                                        return;
                                    }
                                    else
                                    {
                                        //get challenge again and update on rt db
                                        
                                        DBConnectionManager.query("SELECT * FROM challenges WHERE (id = "+rowid+")", null, function(result,err)
                                        {
                                            if(err)
                                            {
                                                activityLog("THERE WAS AN ERROR LOOKING FOR challenge WHERE (id = "+rowid+")");
                                                activityLogErr(err);
                                                return;
                                            }
                                            var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                                            if(Object.keys(res).length != 0)
                                            {				
                                                activityLog("res[0] : " + res[0] )
                                                activityLog("Updating rethink db with challenge");
                                                
                                                try
                                                {
                                                    var rounded_time = res[0].elapsedservertime;
                                                    if( rounded_time > 60 )
                                                        rounded_time = 60.0;

                                                    rt_functions.saveChallengeinRethinkDB(res[0].id, username, userimage, res[0].igid, res[0].challengetype, 
                                                                                            res[0].tottime, res[0].gold, res[0].galacredit, 
                                                                                            res[0].ipaddr, res[0].email, res[0].won, 
                                                                                            rounded_time, res[0].finishdate, res[0].points );
                                                                                            
                                                    
                                                }
                                                catch(e) {
                                                    activityLog(e);
                                                    // [Error: Uh oh!]
                                                }
                                            }
                                        });
                                        
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

        socket.on('send badge', function(igid,challengetype,won,time,gold,a,b,c,d,e,points)
        {
            if(debug == 1)
            {
                activityLog("sendBade DEBUG OK");
                sendBadge(igid,challengetype,won,time,gold,a,b,c,d,e,points);
                return;
            }
            activityLog("I will check for a cheater before sending the badge");
            activityLog("looking for cheater in blacklist");
            
            var queryToSend = "SELECT * FROM blacklist WHERE (date >= curdate() OR permanent = 1) AND ((igid = '"+igid+"' AND igid != '0') OR email='"+s_email+"') LIMIT 1";
            if( s_email == '' )
            queryToSend = "SELECT * FROM blacklist WHERE (date >= curdate() OR permanent = 1) AND (igid = '"+igid+"' AND igid != '0') LIMIT 1";

            DBConnectionManager.query( queryToSend, null, function(result,err)
            {
                if(err)
                {
                    activityLog("error in query SELECT * FROM blacklist");
                    activityLogErr(err);
                    socket.emit("challenge callback", "fail", "inconsistent data", "", "", "");
                    return;

                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                if(Object.keys(res).length == 0)
                {
                    activityLog("not found in blacklist. Looking for clues in ban_rules");
                    DBConnectionManager.query("SELECT * FROM ban_rules WHERE LOCATE(keyword,'"+s_email+"') > 0 OR LOCATE(network,'"+s_address+"') = 1", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLog("error in SELECT * FROM ban_rules");
                            socket.emit("challenge callback", "fail", "inconsistent data", "", "", "");
                            activityLogErr(err);
                            return;
                        }
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        if(Object.keys(res).length == 0)
                        {
                            activityLog("no clues in ban_rules. Send this badge request!");
                            sendBadge(igid,challengetype,won,time,gold,a,b,c,d,e,points);
                        }
                        else
                        {
                            activityLog("found some clues in ban_rules. Add this user to the permanent blacklist");
                            DBConnectionManager.query("INSERT INTO blacklist (igid,ipaddr,email,banreason,permanent) VALUES ('"+s_userigid+"','"+s_address+"','"+s_email+"',1,1)", null, function(res,err)
                            {
                                if(err)
                                {
                                    activityLog("THERE WAS AN ERROR ADDING THIS USER TO THE PERMANENT BLACKLIST");
                                    socket.emit("challenge callback", "fail", "inconsistent data", "", "", "");
                                    activityLogErr(err);
                                    throw err;
                                    return;
                                }
                                activityLog("SUCCESSFULLY added user to permanent blacklist");
                                socket.emit("challenge callback", "fail", "inconsistent data", "", "", "");
                                //socket.emit("challenge request fail", "The activity of your account is not compliant with our rules and you have been permanently banned.", "https://www.indiegala.com/profile");
                                
                            });
                        }
                        

                    });
                }
                else
                {
                    activityLog("cheater found in blacklist. igid : [" + igid + "] email : [" + s_email + " Sending inconsistent data. ");
                    socket.emit("challenge callback", "fail", "inconsistent data", "", "", "");
                    /*
                    if(res[0].permanent == 1)
                    {
                        socket.emit("challenge request fail", "The activity of your account is not compliant with our rules and you have been permanently banned.", "https://www.indiegala.com/profile");
                    }
                    else
                    {
                        socket.emit("challenge request fail", "The activity of your account is not compliant with our rules and you have been temporary blacklisted.", "https://www.indiegala.com/profile");
                    }
                    */
                }
            });
            
        });

        socket.on('check if player can play challenge', function(igid,act)
        {
            if(debug == 1)
            {
                activityLog("can play challenge callback DEBUG OK");
                socket.emit("can play challenge callback");
                return;
            }
            
            DBConnectionManager.query("SELECT * FROM blacklist WHERE (date >= curdate() OR permanent = 1) AND ((igid = '"+igid+"' AND igid != '0') OR email='"+s_email+"') LIMIT 1", null, function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while LOOKING FOR user " + igid + ": " + err);
                    socket.emit("challenge request fail", "There was an unknown error! Please try again in a few minutes.", "https://www.indiegala.com/profile");
                    activityLogErr(err);
                    return;

                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                activityLog(" number of objects found: " + Object.keys(res).length)
                if(Object.keys(res).length == 0)
                {
                    activityLog("This user (" + igid + ") is not a cheater and can progress.");
                    const request = require('request');
                    activityLog("check if player can play challenge with " + igid + " " + act);
                    var postigid = igid;

                    
                    request.post('https://www.indiegala.com/feudalife_challenge_check_silver', 
                    {
                        json: {
                            user_id: postigid,
                            action: act
                        }
                    }, (error, res, body) => {
                        
                    if (error) {
                        activityLog("--ERROR---------check if player can play challenge--------");
                        activityLog(error);
                        console.error("--ERROR-----------------------------------------------");
                        return;
                    }
                    activityLog("--NOERROR---------check if player can play challenge--------");
                    activityLog("RESPONSE Status code:" + res.statusCode );

                    var failurl = "https://www.indiegala.com/profile";
                    var failurltext = "You don't have enough Galacredits to play. Buy some more from your profile.";

                    if(res.statusCode != 200)
                    {
                        activityLog("there was an error with the request itself");
                        if(act == "check")
                        {
                            activityLog("error on request during CHECK");
                        }
                        else if(act == "play")
                        {
                            activityLog("error on request during PLAY");
                        }
                        socket.emit("challenge request fail", failurltext, failurl);
                    }
                    else
                    {
                        var status = body.status;
                        activityLog("status " + status);

                        if(status == "ok")
                        {
                            var paid = body.match_paid;
                            var canplay = body.can_play;
                            activityLog("paid " + paid);
                            activityLog("canplay " + canplay);

                            if(act == "check" && paid == false && canplay == true)
                            {
                                activityLog("this user can play and has not paid");
                                socket.emit("can play challenge callback");
                            }
                            else if(act == "play" && paid == true && canplay == true)
                            {
                                activityLog("this user can play the challenge and has paid");
                                socket.emit("can play challenge callback");
                            }
                            else
                            {
                                activityLog("status was OK but there was some problem");
                                if(act == "check")
                                {
                                    activityLog("the problem was during CHECK");
                                    //socket.emit("send player to", failurl);
                                }
                                else if(act == "play")
                                {
                                    activityLog("the problem was during PLAY");
                                    //socket.emit("send player to", failurl);
                                }

                                socket.emit("challenge request fail", failurltext, failurl);
                            }
                        }
                        else
                        {
                            activityLog("status was not OK");
                            var giveawaystatus = body.text;
                            //socket.emit("send player to", failurl);
                            if(giveawaystatus == "giveaway closed")
                            {
                                socket.emit("challenge request fail", "The Giveaway is closed.", "https://www.indiegala.com");
                            }
                            else if(giveawaystatus == "invalid email")
                            {
                                socket.emit("challenge request fail", "Accounts made with disposable email providers cannot access the giveaway.\nSwitch to another account or contact support if you want to change the email associated to your account. ", "https://www.indiegala.com");
                            }
                            else
                            {
                                socket.emit("challenge request fail", failurltext, failurl);
                            }
                            
                        }

                    }
                    });
                    //activityLog("ok. Can play");
                    //socket.emit("can play challenge callback");
                
                }	
                else
                {
                    activityLog("This user (" + igid + ") is a cheater and cannot progress.");
                    socket.emit("challenge request fail", "Giveaway locked due to suspicious activity.", "https://www.indiegala.com/profile");
                    /*
                    if(res[0].permanent == 1)
                    {
                        socket.emit("challenge request fail", "The activity of your account is not compliant with our rules and you have been permanently banned.", "https://www.indiegala.com/profile");
                    }
                    else
                    {
                        socket.emit("challenge request fail", "The activity of your account is not compliant with our rules and you have been temporary blacklisted.", "https://www.indiegala.com/profile");
                    }
                    */
                    return;
                }
                
            });
            
        });


        socket.on('remove malus', function(igid,seconds)
        {
            const request = require('request');
            activityLog("check if player can remove malus " + igid + " " + seconds);
            var postigid = igid;
            if(debug == 1)
            {
                activityLog("removed malus DEBUG OK");
                socket.emit("removed malus", "1");
                return;
            }
            request.post('https://www.indiegala.com/feudalife_challenge_remove_malus', {
            json: {
                user_id: postigid,
                second_to_remove : seconds
            }
            }, (error, res, body) => {
                
            if (error) {
                activityLog("--ERROR--------------remove malus------------------");
                activityLog(error);
                activityLog("--ERROR-----------------------------------------------");
                socket.emit("removed malus", "0");
                return;
            }
            activityLog("--NOERROR--------------remove malus------------------");
            activityLog("RESPONSE Status code:" + res.statusCode );
            if(res.statusCode != 200)
            {
                    socket.emit("removed malus", "0");
            }
            else
            {
                var status = body.status;
                var text = body.text;
                activityLog("status " + status + " text " + text);
                if(status == "ok" && text == "success")
                {
                    socket.emit("removed malus", "1");
                }
                else if(status == "fail" && text == "not enough credits")
                {
                    socket.emit("removed malus", "2");
                }
                else
                {
                    socket.emit("removed malus", "0");
                }
            }
            });
        });


        socket.on('visit url', function(url)
        {
            socket.emit("send player to", url);
        });

        socket.on('getCSD', function()
        {
            DBConnectionManager.query("SELECT * FROM challenge_config WHERE id = '1'", null, function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while Looking into challenge_config " + err);
                    s_csd = 1.0;
                    s_tct = 5.0;
                    s_weg = 0;
                    s_gth = 75;
                    socket.emit("gotCSD","1.0");
                    socket.emit("gotWEG","0");

                    socket.emit("gotMSB","4");
                    socket.emit("gotMSH","6");
                    socket.emit("gotBSM","1.0");
                    activityLogErr(err);
                    throw err;
                    return;

                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                if(Object.keys(res).length == 0)
                {
                    s_csd = 1.0;
                    s_tct = 5.0;
                    s_weg = 0;
                    s_gth = 75;
                    socket.emit("gotCSD","1.0");
                    socket.emit("gotWEG","0");

                    socket.emit("gotMSB","4");
                    socket.emit("gotMSH","6");
                    socket.emit("gotBSM","1.0");
                }
                else
                {
                    s_csd = res[0].second_duration;
                    s_tct = res[0].time_cheat_threshold;
                    s_weg = res[0].will_earn_galacredits;
                    s_gth = res[0].gold_threshold;
                    socket.emit("gotCSD",res[0].second_duration);
                    socket.emit("gotWEG",res[0].will_earn_galacredits);

                    socket.emit("gotMSB",res[0].malus_seconds_base);
                    socket.emit("gotMSH",res[0].malus_seconds_high);
                    socket.emit("gotBSM",res[0].bonus_seconds_multi);
                }

            });
            
        });

        socket.on('get challenge data', function(ch)
        {
            activityLog('socket.on(get challenge data)');		

            activityLog("get challenge data " + ch);
            DBConnectionManager.query("SELECT * FROM challenge_data WHERE read_only_challenge_type = '"+ch+"'", null, function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while LOOKING FOR chalenge data. " + err);
                    socket.emit("got challenge data error");
                    activityLogErr(err);
                    throw err;
                    return;

                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                if(Object.keys(res).length == 0)
                {
                    activityLog("no results. " + err);
                    socket.emit("got challenge data error");
                }
                else
                {
                    activityLog("got some challenge data ");
                    socket.emit("got challenge data",res[0].good_number,res[0].action_frames_normal,res[0].action_frames_wrong,res[0].action_frames_pain,res[0].player_damage,res[0].player_life,res[0].enemy_damage,res[0].enemy_life,res[0].grow_crono);
                }

            });
        
        });
    });


}
