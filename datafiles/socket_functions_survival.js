'use strict';

//this is the server
var destinationids = [];
var destinationnames = [];
var destinationlandids = [];
var destinantionnumtrees = [];
var destinantionnumrocks = [];
var destinantionnumplants = [];
var destinantionnumvisitors = [];

var destinationgroundstring = [];
var destinationstoreystring = [];
var destinationwallsstring = [];
var destinationmobsstring = [];
var destinationpickablesstring = [];
var destinationinteractablesstring = [];

var connectedUsers = 0;
var debug = 0;
var zlib = require('zlib');
var slug = require('slug');

module.exports = function(io) 
{

    ////////////////////////////////////////////////////////////////

    var DBConnectionManager = require('./dbconnection');
    var rt_functions = require('./rt_functions.js') 


    function executeMysqlStatement( queryString )
    {
        console.log("executeMysqlStatement with query = ("+queryString.substring(0,100)+")");
        DBConnectionManager.query( queryString, null, function(result, err)
        {
            if(err)
            {
                console.log("error in executeMysqlStatement " + err + "statement : " + queryString );
                activityLogErr(err);
                throw err;
            }
            var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
            console.log("res:"+res);

        });

    }


    io.on('connection', function(socket)
    {
        var skip_farm_processing = 1; //initialize with url..
        //local variable to the socket
        var mysocket = socket.id;
        //local destination is initially same as socket
        var destinationid = socket.id;
        //global destinations holds all socket ids
        //destinationids.push(socket.id);
        //if asked for the socket list return a string with all sockets connected
        
        console.log("A user connected with socket " + mysocket);

        //perche usiamo NGINX come proxy...
        var client_ip_address = socket.handshake.headers["x-real-ip"];
        console.log("client_ip_address" + client_ip_address);
        //var test_address = socket.request.connection.remoteAddress;
        
        console.log('indexjs:server: user connected ' + socket.id + ' ' + io.sockets.connected[socket.id] + ' ' + Object.keys(io.sockets.connected).length);
        
        //io.emit('update users connected', connectedUsers);
        socket.emit('client connected', socket.id);
        //io.emit emette dal server a tutti i client
        //socket.broadcast.emit emette dal server a tutti i client tranne il chiamante
        //socket.emit emette dal client al server
        
        var s_username = "";
        var s_userid = 0;
        var s_userigid = 0;
        var s_socketid = "";
        var s_farmstring = "";
        var s_storeystring = "";
        var s_wallsstring = "";
        var s_mobsstring = "";
        var s_pickables = "";
        var s_interactables = "";
        var s_items = "";
        var s_userdata = "";
        var s_landid = 0;

        var s_address = client_ip_address.replace("::ffff:","");    
        var s_email = "";

        socket.on("fix name", function(str)
        {
            var dec = decodeURIComponent(str);
            var res = slug(dec);
            activityLog("FIX NAME: "+str+" -> "+res);
            socket.emit("fixed name", res);
        });

        socket.on("logga", function(str)
        {
            activityLog(str);
        });

        socket.on("bunyan log", function(str)
        {
            activityLog(str);
        });

        function activityLog(str)
        {
            console.log("<"+s_userigid+"> "+str);
        }

        function activityLogErr(str)
        {
            /*
            FgBlack = "\x1b[30m"
            FgRed = "\x1b[31m"
            FgGreen = "\x1b[32m"
            FgYellow = "\x1b[33m"
            FgBlue = "\x1b[34m"
            FgMagenta = "\x1b[35m"
            FgCyan = "\x1b[36m"
            FgWhite = "\x1b[37m"
            Blink = "\x1b[5m"
            */
            console.log("\x1b[31m%s\x1b[0m","<"+s_userigid+"> "+str);
        }
        
        function activityLogBlink(str)
        {
            console.log("\x1b[5m%s\x1b[0m","<"+s_userigid+"> "+str);
        }

        function activityLogBlue(str)
        {
            console.log("\x1b[36m%s\x1b[0m","<"+s_userigid+"> "+str);
        }

        function activityLogGreen(str)
        {
            console.log("\x1b[32m%s\x1b[0m","<"+s_userigid+"> "+str);
        }

        function logDestinations()
        {
            var i = 0;
            activityLogBlue("---------------- DESTINATIONIDS ----------------");
            destinationids.forEach(function(v)
            {
                var len = 23 - destinationnames[i].length;
                var len2 = 23 - destinationids[i].length;
                var len3 = 23 - destinationlandids[i].length;
                var stri = "|";
                stri += destinationnames[i];
                if(len > 0)
                {
                    for(var l = 0; l < len; l++)
                    {
                        stri += " ";
                    }
                    stri += "|";
                }
                else 
                {
                    stri += "|";
                }
                stri += destinationids[i];
                if(len2 > 0)
                {
                    for(var l = 0; l < len2; l++)
                    {
                        stri += " ";
                    }
                    stri += "|";
                }
                else 
                {
                    stri += "|";
                }
                stri += destinationlandids[i];
                if(len3 > 0)
                {
                    for(var l = 0; l < len3; l++)
                    {
                        stri += " ";
                    }
                    stri += "|";
                }
                else 
                {
                    stri += "|";
                }
                activityLogBlue(stri);
                i++;
            });
            activityLogBlue("------------------------------------------------");
        }

        socket.on("get clients in room", function(roomname)
        {
            if ( io.sockets.adapter.rooms[roomname] == undefined)
            {
                //activityLog("ROOM UNDEFINED");
                socket.emit("got clients in room", 0);
                return;
            }
            var arr = io.sockets.adapter.rooms[roomname].sockets;
            if (arr == undefined)
            {
                //activityLog("SOCKETS UNDEFINED");
                socket.emit("got clients in room", 0);
                return;
            }
            var c = 0;
            for (var key in arr)
            {
                c++;
            }
            //activityLog("CLIENTS: "+c);
            socket.emit("got clients in room", c);
        });

        socket.on("get clients in island", function(roomname)
        {
            if ( io.sockets.adapter.rooms[roomname] == undefined)
            {
                //activityLog("ROOM UNDEFINED");
                socket.emit("got clients in island", 0);
                return;
            }
            var arr = io.sockets.adapter.rooms[roomname].sockets;
            if (arr == undefined)
            {
                //activityLog("SOCKETS UNDEFINED");
                socket.emit("got clients in island", 0);
                return;
            }
            var c = 0;
            for (var key in arr)
            {
                c++;
            }
            //activityLog("CLIENTS: "+c);
            socket.emit("got clients in island", c);
        });

        socket.on('get user creation date', function(usrigid)
        {
            
                DBConnectionManager.query("SELECT * FROM users_survival WHERE igid = '"+usrigid+"'", function(result, err)
                {
                    if(err)
                    {
                        socket.emit("cannot get user creation date");
                        activityLogErr(err);
                        throw err;
                    }
                    else
                    {
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        socket.emit('got user creation date', res[0].date);
                    }

                });
        });

        socket.on('get socket list', function()
        {
            sendSocketsList();
        });

        function sendSocketsList()
        {
            //activityLog("-------------------------");
            var str = "";
            var str2 = "";
            var str3 = "";
            var str4 = "";
            var str5 = "";
            var str6 = "";
            var i = 0;
            destinationids.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinationids.length-1)
                {
                    str += v+":";
                }
                else
                {
                    str += v;
                }
                i++;
            });
            i = 0;
            destinationnames.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinationnames.length-1)
                {
                    str2 += v+":";
                }
                else
                {
                    str2 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumtrees.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumtrees.length-1)
                {
                    str3 += v+":";
                }
                else
                {
                    str3 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumrocks.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumrocks.length-1)
                {
                    str4 += v+":";
                }
                else
                {
                    str4 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumplants.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumplants.length-1)
                {
                    str5 += v+":";
                }
                else
                {
                    str5 += v;
                }
                i++;
            });
            i = 0;
            destinantionnumvisitors.forEach(function(v)
            {
                //activityLog(v);
                if(i < destinantionnumvisitors.length-1)
                {
                    str6 += v+":";
                }
                else
                {
                    str6 += v;
                }
                i++;
            });
            socket.emit('send sockets list',str,str2,str3,str4,str5,str6);
            //activityLog("-------------------------");
        }
        
        //GOING TO ANOTHER FARM 2: with this function we will ask the destination socket to give back its values
        socket.on("reach user for data",function(s)
        {
            var des = s;
            var found = false;
            for(var i = 0; i < destinationids.length; i++)
            {
                if(destinationids[i] == s)
                {
                    des = s;
                    found = true;
                    break;
                }
            }

            //origin = s_username;?????
            if(found)
            {
                activityLog(mysocket+": asking data to " + io.sockets.connected[des]);
                io.sockets.connected[des].emit("give me data",mysocket);
            }
            else
            {
                socket.emit("get user data failed");
            }
        });

        function reachUserForData(s)
        {
            activityLog("reachUserForData("+s+")");
            var des = s;
            var found = false;
            for(var i = 0; i < destinationids.length; i++)
            {
                if(destinationids[i] == s)
                {
                    des = s;
                    found = true;
                    break;
                }
            }
            if(found)
            {
                activityLog(mysocket+": asking data to " + io.sockets.connected[des]);
                /*activityLog(s_username);
                activityLog(s_userid);
                activityLog(s_socketid);
                activityLog(s_userdata);*/
                io.sockets.connected[des].emit("give me data",mysocket,s_username,s_userid,s_socketid,s_userdata);
            }
            else
            {
                socket.emit("get user data failed");
            }
        }

        //GOING TO ANOTHER FARM 4: with this function the destination sends out its data
        socket.on("give me data",function(origin,usrname,usrid,sock,ud)
        {
            activityLog(mysocket+": sending data to " + origin);
            /*activityLog(usrname);
            activityLog(usrid);
            activityLog(sock);
            activityLog(ud);*/
            activityLog("interactables: "+s_interactables);
            //io.to(origin).emit("got user data", s_username, s_userid, s_socketid, s_farmstring, s_pickables, s_interactables);
            if(s_farmstring != "" && s_storeystring != "" && s_wallsstring != "" && s_mobsstring != "" && s_interactables != "")
            {
                io.to(origin).emit('got player data survival',usrname,usrid,sock,ud,s_farmstring,s_storeystring,s_wallsstring,s_mobsstring,s_pickables, s_interactables, s_items);
            }
            
        });

        socket.on("retrieve user data",function(usrname,userid,s)
        {
            //activityLog("indexjs:server: get user data " + username + " " + userid + " " + s);
        });

        //CALLED ON STARTUP
        socket.on("get player record",function(usrigid)
        {
            activityLog("Will check if there's already some saved data for user " + usrigid);
            activityLog("Connecting to table USERS to find user " + usrigid);
            DBConnectionManager.query("SELECT * FROM users_survival WHERE igid = '"+usrigid+"'", null, function(result,err)
            {
                //activityLog("inside select");
                if(err)
                {
                    activityLog("There was an error for user " + usrigid + ". " + err);
                    socket.emit("get player data failed");
                    activityLogErr(err);
                    throw err;
                    return;
                }

                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }

                //if not found add it to the db
                if(Object.keys(res).length == 0)
                {
                    activityLog("No data found for user " + usrigid);
                    socket.emit('got player record survival',"0","0");
                }
                else//just read it and go on
                {
                    activityLog("landid " + res[0].landid);
                    if(res[0].landid != 0)
                    {
                        activityLog("Found data for user " + usrigid);
                        socket.emit('got player record survival',"1",res[0].landid);
                    }
                    else
                    {
                        activityLog("No data found for user " + usrigid);
                        socket.emit('got player record survival',"0","0");
                    }
                }

            });
        
        });

        socket.on("resync user",function(usrigid,usremail, usrname, ischallenge)
        {
            activityLog("resync user ");
            s_userigid = usrigid;
            s_email = usremail;
            activityLogGreen("SOCKET.JOIN -> "+usrname);
            socket.join(usrname);
            s_username = usrname;//decodeURIComponent(usrname);
            
            /*
            activityLog("resync user --- id:"+s_userigid);
            activityLog("resync user --- email:"+s_email);
            activityLog("resync user --- ip:"+s_address);
            activityLog("resync user --- username:"+s_username);
            */
            if(ischallenge == 1)
            {
                skip_farm_processing = 1;
                activityLog("skip_farm_processing " + skip_farm_processing);
            }
            else
            {
                skip_farm_processing = 0;
                activityLog("skip_farm_processing " + skip_farm_processing);
            }
            if(!skip_farm_processing)
            {
                activityLogBlink("RESYNC: Looking for user "+s_username+":"+socket.id+" in destinationids ");
                var i = 0;
                destinationids.forEach(function(v)
                {
                    if(socket.id == v)
                    {
                        activityLogBlink("RESYNC: Found user "+s_username+":"+socket.id+" in destinationids -> removing");
                        destinationids.splice(i, 1);  
                        destinationnames.splice(i, 1); 
                        destinationlandids.splice(i, 1); 
                        destinantionnumtrees.splice(i, 1);
                        destinantionnumrocks.splice(i, 1);
                        destinantionnumplants.splice(i, 1);  
                        connectedUsers--;
                    }
                    else
                    {
                        i++;
                    }
                });
                activityLogBlink("RESYNC: Now adding user "+s_username+":"+socket.id+" in destinationids");
                destinationid = socket.id;
                destinationids.push(socket.id);
                destinationnames.push(s_username);
                destinationlandids.push(s_landid); 
                destinantionnumtrees.push("0");
                destinantionnumrocks.push("0");
                destinantionnumplants.push("0");  
                connectedUsers++;
                sendSocketsList();
                logDestinations();
                /*
                var check_name = s_username;
                var check_encoded = encodeURIComponent(check_name);
                var check_clean = check_encoded.replace(/%20/g," ");
                var check_match = (check_name === check_clean);
                if(check_match)
                {
                    activityLogBlink("RESYNC: Now adding user "+s_username+":"+socket.id+" in destinationids");
                    destinationid = socket.id;
                    destinationids.push(socket.id);
                    destinationnames.push(s_username);
                    destinantionnumtrees.push("0");
                    destinantionnumrocks.push("0");
                    destinantionnumplants.push("0");  
                    connectedUsers++;
                    sendSocketsList();
                    logDestinations();
                }
                else
                {
                    activityLogBlink("RESYNC: SKIPPING user "+s_username+":"+socket.id+" for its name");
                    logDestinations();
                }
                */
            }
        });

        socket.on("register user",function(usrigid,usremail,ischallenge)
        {
            activityLog("register user "+usrigid,+" email "+usremail+" ischallenge "+ischallenge);
            s_userigid = usrigid;
            s_email = usremail;

            skip_farm_processing = 0;
            activityLog("skip_farm_processing " + skip_farm_processing);
        });

        socket.on("generate island",function(usrname,usrigid)
        {
            DBConnectionManager.query("SELECT * FROM users_survival WHERE igid = '"+usrigid+"'", null, function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while LOOKING FOR user " + usrigid + ". " + err);
                    socket.emit("get player data failed");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                
                DBConnectionManager.query("INSERT INTO survival (landid) SELECT COUNT(*) + 1 FROM survival", null, function(result,err)
                {
                    if(err)
                    {
                        activityLog("There was an ERROR while ADDING user " + usrigid + " to SURVIVAL. " + err);
                        socket.emit("get player data failed");
                        activityLogErr(err);
                        throw err;
                    }
                    DBConnectionManager.query("SELECT * FROM survival WHERE landid = (SELECT COUNT(*) FROM survival)", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLogErr(err);
                            throw err;
                        }
                        var res_surv = result; if(res_surv != null && res_surv != undefined){ res_surv = JSON.parse(JSON.stringify(result)); }
                        socket.emit('got player record survival',"1",res_surv[0].landid);
                        activityLog("ADDED user " + usrigid + " to table SURVIVAL");
                        s_farmstring = "";
                        s_storeystring = "";
                        s_mobsstring = "";
                        s_pickables = "";
                        s_interactables = "";
                        s_items = "";
                        s_userdata = "100:100:100:-1:-1:134:1:1";

                        DBConnectionManager.query("UPDATE users_survival SET landid= "+res_surv[0].landid+" WHERE userid= "+res[0].userid, null, function(result,err)
                        {
                            if(err)
                            {
                                activityLogErr(err);
                                throw err;
                            }
                            
                        });

                        //return the values
                        socket.emit('got player data survival',usrname,res[0].userid,socket.id,s_userdata,"","","","","","","");
                    });
                });
                
            });
        });

        //CALLED WHEN CLICKING NEW GAME OR CONTINUE OR PLAY CHALLENGE
        socket.on("get player data",function(usrname,usrigid)
        {
            //UTF here usrname is received encoded and stored decoded 
            s_username = usrname;//decodeURIComponent(usrname);
            activityLog("Will get player data for user " + usrigid + "and username (decoded)" + s_username );
            
            var _userigid = usrigid;
            var _socketid = socket.id;
            var _userid = 0;
            s_landid = 0;
            var _lastlandid = 0;
            var _username = s_username;

            s_socketid = socket.id;
            
            if(!skip_farm_processing)
            {
                var found = false;

                for(var i = 0; i < destinationids.length; i++)
                {
                    var v = destinationids[i];
                    if(v.indexOf(s_socketid) > -1)
                    {
                        found = true;
                        break;
                    }
                }
                if(!found)
                {
                    activityLogBlink("CONNECTION: Adding user "+s_username+":"+socket.id+" to destinationids");
                    destinationids.push(socket.id);
                    destinationnames.push(s_username);
                    destinationlandids.push("0");
                    destinantionnumtrees.push("0");
                    destinantionnumrocks.push("0");
                    destinantionnumplants.push("0");
                    destinantionnumvisitors.push("0");
                    activityLogGreen("SOCKET.JOIN -> "+s_username);
                    socket.join(s_username);
                    connectedUsers ++;
                }
                logDestinations();
            }
            
            //ask for username data
            activityLog("CONNECTING to table USERS to FIND user " + _userigid);
            DBConnectionManager.query("SELECT * FROM users_survival WHERE igid = '"+_userigid+"'", null, function(result,err)
            {
                if(err)
                {
                    activityLog("There was an ERROR while LOOKING FOR user " + usrigid + ". " + err);
                    socket.emit("get player data failed");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                //if not found add it to the db
                if(Object.keys(res).length == 0)
                {
                    activityLog("NO DATA FOUND for user " + _userigid);
                    activityLog("Adding user " + _userigid + " to table USERS");
                    //UTF here _username is sent already decoded at the beginning and then it's always sent back encoded to index.html
                    var escapedusername = _username.replace(/'/g, "\\'");//this escapes single quotes -> \'
                    DBConnectionManager.query("INSERT INTO users_survival (username,igid,userdata,itemsstring) VALUES ('"+escapedusername+"','"+_userigid+"','100:100:100:-1:-1:134:1:1','2E01000000000000')", null, function(result,err)
                    {
                        if(err)
                        {
                            activityLog("There was an ERROR while ADDING user " + usrigid + " to USERS. " + err);
                            socket.emit("get player data failed");
                            activityLogErr(err);
                            throw err;
                        }
                        var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                        _userid = res.insertId;
                        s_userid = res.insertId;
                        s_userdata = "100:100:100:-1:-1:134:1:1";
                        s_items = "";

                        activityLog("ADDING user " + _userigid + " to table SURVIVAL");

                        DBConnectionManager.query("SELECT * FROM survival WHERE users < 10 and landid != "+_lastlandid+" LIMIT 1", null, function(result,err)
                        {
                            if(err)
                            {
                                activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into SURVIVAL. " + err);
                                socket.emit("get player data failed");
                                activityLogErr(err);
                                throw err;
                                return;
                            }
                            var res_surv = result; if(res_surv != null && res_surv != undefined){ res_surv = JSON.parse(JSON.stringify(result)); }
                                
                            s_farmstring = res_surv[0].groundstring;
                            s_storeystring = res_surv[0].storeystring;
                            s_wallsstring = res_surv[0].wallsstring;
                            s_mobsstring = res_surv[0].mobsstring;
                            s_pickables = res_surv[0].pickablesstring;
                            s_interactables = res_surv[0].interactablesstring;
                            for(var i = 0;i<=destinationlandids.length;i++)
                            {
                                if(destinationlandids[i] == res_surv[0].landid)
                                {
                                    if(destinationids[i] != s_socketid)
                                    {
                                        io.sockets.connected[destinationids[i]].emit("give me data",mysocket,s_username,s_userid,s_socketid,s_userdata);
                                        break;
                                    }
                                    
                                }
                            }
                            
                            
                            if(s_farmstring == null)
                            {
                                s_farmstring = "";
                            }
                            if(s_storeystring == null)
                            {
                                s_storeystring = "";
                            }
                            if(s_interactables == null)
                            {
                                s_interactables = "";
                            }
                            DBConnectionManager.query("UPDATE survival SET users = "+(res_surv[0].users+1)+" WHERE landid= "+res_surv[0].landid, null, function(result,err)
                            {
                                if(err)
                                {
                                    activityLogErr(err);
                                    throw err;
                                }
                                
                            });
                            DBConnectionManager.query("UPDATE users_survival SET landid= "+res_surv[0].landid+" WHERE userid= "+_userid, null, function(result,err)
                            {
                                if(err)
                                {
                                    activityLogErr(err);
                                    throw err;
                                }
                                                
                            });
                            if(s_farmstring != ""/*|| s_pickables == "" || s_interactables == ""*/)
                            {
                                /*HERE BECAUSE WE MUST PASS THE INFLATED FARMSTRING*/
                                socket.emit('got player record survival',"1",res_surv[0].landid);
                                activityLog("socket emit 'got player data survival'");
                                socket.emit('got player data survival',_username,_userid,s_socketid,s_userdata,s_farmstring,s_storeystring,s_wallsstring,s_mobsstring,s_pickables, s_interactables, s_items);
                            }
                        });
                    });

                }
                else//just read it and go on
                {
                    activityLog("FOUND DATA for user " + _userigid);
                    _userid = res[0].userid;
                    s_userid = res[0].userid;
                    s_userdata = res[0].userdata;
                    s_items = res[0].itemsstring;
                    s_landid = res[0].landid;
                    _lastlandid = res[0].lastlandid;

                    /*for(var i = 0; i < destinationids.length; i++)
                    {
                        if(destinationids[i] == s_socketid)
                        {
                            destinationlandids[i] = s_landid;
                            break;
                        }
                    }
                    logDestinations();
                    
                    var found_index = -1;
                    for(var i = 0; i < destinationlandids.length; i++)
                    {
                        if(destinationlandids[i] == s_landid)
                        {
                            var candidate_socket = destinationids[i];
                            if(candidate_socket != s_socketid)
                            {
                                found_index = i;
                                break;
                            }
                        }
                    }
                    if(found_index >= 0)
                    {
                        reachUserForData(destinationids[found_index]);
                    }
                    else
                    {*/

                        activityLog("CONNECTING to table survival to FIND A RECORD " + _userigid + " with userid " + _userid);
                        DBConnectionManager.query("SELECT landid FROM survival WHERE landid = '"+ s_landid +"'", null, function(result,err)
                        {
                            if(err)
                            {
                                activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into SURVIVAL. " + err);
                                socket.emit("get player data failed");
                                activityLogErr(err);
                                throw err;
                            }
                            var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                            
                            if(Object.keys(res).length == 0)
                            {
                                
                                DBConnectionManager.query("SELECT * FROM survival WHERE users < 10 and landid != "+_lastlandid+" LIMIT 1", null, function(result,err)
                                {
                                    if(err)
                                    {
                                        activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into SURVIVAL. " + err);
                                        socket.emit("get player data failed");
                                        activityLogErr(err);
                                        throw err;
                                        return;
                                    }
                                    var res_surv = result; if(res_surv != null && res_surv != undefined){ res_surv = JSON.parse(JSON.stringify(result)); }

                                    s_farmstring = res_surv[0].groundstring;
                                    s_storeystring = res_surv[0].storeystring;
                                    s_wallsstring = res_surv[0].wallsstring;
                                    s_mobsstring = res_surv[0].mobsstring;
                                    s_pickables = res_surv[0].pickablesstring;
                                    s_interactables = res_surv[0].interactablesstring;
                                    s_userdata = "100:100:100:-1:-1:134:1:1";
                                    s_items = "";
                                    
                                    for(var i = 0;i<=destinationlandids.length;i++)
                                    {
                                        if(destinationlandids[i] == res_surv[0].landid)
                                        {
                                            if(destinationids[i] != s_socketid)
                                            {
                                                
                                                io.sockets.connected[destinationids[i]].emit("give me data",mysocket,s_username,s_userid,s_socketid,s_userdata);
                                                break;
                                            }
                                            
                                        }
                                    }
                                    
                                    if(s_farmstring == null)
                                    {
                                        s_farmstring = "";
                                    }
                                    if(s_storeystring == null)
                                    {
                                        s_storeystring = "";
                                    }
                                    if(s_interactables == null)
                                    {
                                        s_interactables = "";
                                    }

                                    DBConnectionManager.query("UPDATE survival SET users = "+(res_surv[0].users+1)+" WHERE landid= "+res_surv[0].landid, null, function(result,err)
                                    {
                                        if(err)
                                        {
                                            activityLogErr(err);
                                            throw err;
                                        }
                                        
                                    });
                                    DBConnectionManager.query("UPDATE users_survival SET landid= "+res_surv[0].landid+" WHERE userid= "+_userid, null, function(result,err)
                                    {
                                        if(err)
                                        {
                                            activityLogErr(err);
                                            throw err;
                                        }
                                                        
                                    });

                                    if(s_farmstring != ""/*|| s_pickables == "" || s_interactables == ""*/)
                                    {
                                        /*HERE BECAUSE WE MUST PASS THE INFLATED FARMSTRING*/
                                        socket.emit('got player record survival',"1",res_surv[0].landid);
                                        activityLog("socket emit 'got player data survival'");
                                        socket.emit('got player data survival',_username,_userid,s_socketid,s_userdata,s_farmstring,s_storeystring,s_wallsstring,s_mobsstring,s_pickables, s_interactables, s_items);                                    
                                    }
                                });
                            }
                            else//if found return all values
                            {
                                activityLog("NOW CONNECTING AGAIN to table survival to FIND ALL DATA FOR USER " + _userigid + " with userid " + _userid);
                                DBConnectionManager.query("SELECT * FROM survival WHERE landid = '"+ s_landid +"'", null, function(result,err)
                                {
                                    if(err)
                                    {
                                        activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into SURVIVAL. " + err);
                                        socket.emit("get player data failed");
                                        activityLogErr(err);
                                        throw err;
                                        return;
                                    }
                                    var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                                    
                                    if(Object.keys(res).length == 0)
                                    {                                
                                        DBConnectionManager.query("SELECT * FROM survival WHERE users < 10 and landid != "+_lastlandid+"  LIMIT 1", null, function(result,err)
                                        {
                                            if(err)
                                            {
                                                activityLog("There was an ERROR while LOOKING FOR user " + _userigid + " into SURVIVAL. " + err);
                                                socket.emit("get player data failed");
                                                activityLogErr(err);
                                                throw err;
                                                return;
                                            }
                                            var res_surv = result; if(res_surv != null && res_surv != undefined){ res_surv = JSON.parse(JSON.stringify(result)); }
                                            s_farmstring = res_surv[0].groundstring;
                                            s_storeystring = res_surv[0].storeystring;
                                            s_wallsstring = res_surv[0].wallsstring;
                                            s_mobsstring = res_surv[0].mobsstring;
                                            s_pickables = res_surv[0].pickablesstring;
                                            s_interactables = res_surv[0].interactablesstring;
                                            s_userdata = "100:100:100:-1:-1:134:1:1";
                                            s_items = "";

                                            for(var i = 0;i<=destinationlandids.length;i++)
                                            {
                                                if(destinationlandids[i] == res_surv[0].landid)
                                                {
                                                    if(destinationids[i] != s_socketid)
                                                    {
                                                        io.sockets.connected[destinationids[i]].emit("give me data",mysocket,s_username,s_userid,s_socketid,s_userdata);
                                                        break;
                                                    }
                                                    
                                                }
                                            }

                                            if(s_farmstring == null)
                                            {
                                                s_farmstring = "";
                                            }
                                            if(s_storeystring == null)
                                            {
                                                s_storeystring = "";
                                            }
                                            if(s_interactables == null)
                                            {
                                                s_interactables = "";
                                            }

                                            DBConnectionManager.query("UPDATE survival SET users = "+(res_surv[0].users+1)+" WHERE landid= "+res_surv[0].landid, null, function(result,err)
                                            {
                                                if(err)
                                                {
                                                    activityLogErr(err);
                                                    throw err;
                                                }
                                                
                                            });
                                            DBConnectionManager.query("UPDATE users_survival SET landid= "+res_surv[0].landid+" WHERE userid= "+_userid, null, function(result,err)
                                            {
                                                if(err)
                                                {
                                                    activityLogErr(err);
                                                    throw err;
                                                }
                                                
                                            });

                                            if(s_farmstring != "")
                                            {
                                                /*HERE BECAUSE WE MUST PASS THE INFLATED FARMSTRING*/
                                                socket.emit('got player record survival',"1",res_surv[0].landid);
                                                activityLog("socket emit 'got player data survival'");
                                                socket.emit('got player data survival',_username,_userid,s_socketid,s_userdata,s_farmstring,s_storeystring,s_wallsstring,s_mobsstring,s_pickables, s_interactables, s_items);                                            
                                            }
                                        });
                                    }
                                    else
                                    {
                                        s_farmstring = res[0].groundstring;
                                        s_storeystring = res[0].storeystring;
                                        s_wallsstring = res[0].wallsstring;
                                        s_mobsstring = res[0].mobsstring;
                                        s_pickables = res[0].pickablesstring;
                                        s_interactables = res[0].interactablesstring;
                                        for(var i = 0;i<=destinationlandids.length;i++)
                                        {
                                            if(destinationlandids[i] == res[0].landid)
                                            {
                                                if(destinationids[i] != s_socketid)
                                                {
                                                    reachUserForData(destinationids[i]);
                                                    //io.sockets.connected[destinationids[i]].emit("give me data",mysocket,s_username,s_userid,s_socketid,s_userdata);
                                                    break;
                                                }
                                                
                                            }
                                        }
                                            
                                        if(s_farmstring == null)
                                        {
                                            s_farmstring = "";
                                        }
                                        if(s_storeystring == null)
                                        {
                                            s_storeystring = "";
                                        }
                                        if(s_interactables == null)
                                        {
                                            s_interactables = "";
                                        }
                                        if(s_farmstring != "")
                                        {
                                            socket.emit('got player record survival',"1",res[0].landid);
                                            activityLog("socket emit 'got player data survival'");
                                            socket.emit('got player data survival',_username,_userid,s_socketid,s_userdata,s_farmstring,s_storeystring,s_wallsstring,s_mobsstring,s_pickables, s_interactables, s_items);                                            
                                        }
                                    }
                                });
                            }
                        });
                    //}
                }

            });

        });


        socket.on("clear player data",function(usrigid)
        {
            //activityLog("server: clear player data");
            var _userigid = usrigid;
            var found = false;

            //ask for username data
            DBConnectionManager.query("SELECT * FROM users_survival WHERE igid = '"+_userigid+"'", null, function(result,err)
            {
                if(err)
                {
                    //activityLog("server: error finding user " + err);
                    socket.emit("delete player data failed");
                    activityLogErr(err);
                    throw err;
                }
                var res = result; if(res != null && res != undefined){ res = JSON.parse(JSON.stringify(result)); }
                //if found , we delete
                if(Object.keys(res).length > 0)
                {
                    var _userid = res[0].userid;
                    s_userid = res[0].userid;
                    var lastlandid = res[0].landid;
                    /*if(!skip_farm_processing) {
                        //activityLog("server: delete farm");
                        DBConnectionManager.query("DELETE FROM survival WHERE userid = '"+_userid+"'", null, function(res,err)
                        {
                            if(err)
                            {
                                activityLog("server: error deleting farm " + err);
                                socket.emit("delete player data failed");
                                activityLogErr(err);
                                throw err;
                            }
                            else
                            {
                                activityLog("server: deleted farm");
                            }
                        });

                    }*/
                    
                    DBConnectionManager.query("UPDATE users_survival SET lastlandid = landid, landid = 0 WHERE userid = '"+_userid+"'", null, function(res,err)
                    {
                        if(err)
                        {
                            activityLog("server: error reset user " + err);
                            socket.emit("delete player data failed");
                            activityLogErr(err);
                            throw err;
                        }
                        else
                        {
                            DBConnectionManager.query("UPDATE survival SET users = users - 1 WHERE landid = '"+lastlandid+"'", null, function(res,err)
                            {
                                if(err)
                                {
                                activityLog("server: error reset user " + err);
                                socket.emit("delete player data failed");
                                activityLogErr(err);
                                throw err;
                                }
                            activityLog("server: reset user");
                            });
                        }
                        
                    });
                }
            });
            
            

        });
        socket.on('send userdatasurvival string', function(usrid,fstr)
        {
            console.log("UPDATE users_survival SET userdata = "+fstr+" WHERE userid = "+usrid);
            DBConnectionManager.query("UPDATE users_survival SET userdata = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            }); 
        });

        socket.on('send userstats string', function(usrid,fstr,usrname)
        {   
            DBConnectionManager.query("UPDATE users_survival SET stats = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            });
        });

        socket.on('send deathdata string', function(usrid,fstr,usrname)
        {
            DBConnectionManager.query("UPDATE users_survival SET deathdata = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            });
        
        });

        socket.on('send farm string', function(landid,fstr,usrname)
        {
            var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationgroundstring[landid] = LZString.decompressFromBase64(fstr);
            if( !skip_farm_processing )
            {
                activityLog("indexjs:server: send farm string " + usrname + " " + landid);
                /*DEFLATED*/
                //var deflated = zlib.deflateSync(fstr).toString('base64');
                DBConnectionManager.query("UPDATE survival SET groundstring = '"+fstr+"' WHERE landid = "+landid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    activityLog("updated farmstring " + landid);
                    
                    s_farmstring = fstr;
                    socket.broadcast.to(landid).emit('farm string to store',fstr);
                });
            }   
                
        });

        socket.on('send storey string', function(landid,fstr,usrname)
        {
            var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationstoreystring[landid] = LZString.decompressFromBase64(fstr);
            if( !skip_farm_processing )
            {
                activityLog("indexjs:server: send storey string " + usrname + " " + landid);
                /*DEFLATED*/
                //var deflated = zlib.deflateSync(fstr).toString('base64');
                DBConnectionManager.query("UPDATE survival SET storeystring = '"+fstr+"' WHERE landid = "+landid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    activityLog("updated storeystring " + landid);
                    s_storeystring = fstr;
                    socket.broadcast.to(landid).emit('storey string to store',fstr);
                });
            }   
                
        });

        socket.on('send walls string', function(landid,fstr,usrname)
        {
            
            var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationwallsstring[landid] = LZString.decompressFromBase64(fstr);

            if( !skip_farm_processing )
            {
                activityLog("indexjs:server: send walls string " + usrname + " " + landid);
                DBConnectionManager.query("UPDATE survival SET wallsstring = '"+fstr+"' WHERE landid = "+landid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    s_wallsstring = fstr;    
                    activityLog("updated wallsstring " + landid);
                    
                });
            }   
                
        });

        socket.on('send mobs string', function(landid,fstr,usrname)
        {
            var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationmobsstring[landid] = LZString.decompressFromBase64(fstr);
            if( !skip_farm_processing )
            {
                
                activityLog("indexjs:server: send mobs string " + usrname + " " + landid);
                DBConnectionManager.query("UPDATE survival SET mobsstring = '"+fstr+"' WHERE landid = "+landid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    s_mobsstring = fstr;    
                    activityLog("updated mobsstring " + landid);
                    
                });
            }   
                
        });

        socket.on('send pickables string', function(landid,fstr,usrname)
        {
            var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationpickablesstring[landid] = LZString.decompressFromBase64(fstr);

            if( !skip_farm_processing )
            {
                activityLog("indexjs:server: send pickables string " + usrname);
                var queryStr = "UPDATE survival SET pickablesstring = '"+fstr+"' WHERE landid = "+landid;
                DBConnectionManager.query(queryStr, null, function(res,err)
                {
                    if(err)
                    {
                        console.log( err );
                        return;
                    }
                    s_pickables = fstr;   
                    socket.broadcast.to(landid).emit('pickables string to store',fstr);
                    
                }); 
            
            }   
            
        });

        socket.on('send house pickables string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_house_pickables = fstr;   
            }

            if( !skip_farm_processing )
            {
                //activityLog("indexjs:server: send house pickables string " + usrname);
                DBConnectionManager.query("UPDATE survival SET housepickablesstring = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    //activityLog("updated house pickables string " + usrid);
                }); 
                socket.broadcast.to(usrname).emit('house pickables string to store',fstr);
                    
            }
            
        });

        socket.on('send interactables string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_interactables = fstr; 
            }

            if( !skip_farm_processing )
            {
                //activityLog("indexjs:server: send house pickables string " + usrname);
                /*DEFLATE*/
                //var deflated = zlib.deflateSync(fstr).toString('base64');
                DBConnectionManager.query("UPDATE survival SET interactablesstring = '"+fstr+"' WHERE userid = "+usrid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    //activityLog("updated house pickables string " + usrid);
                }); 
                socket.broadcast.to(landid).emit('interactables string to store',fstr);
            }
            
        });

        socket.on('send interactables string survival', function(landid,fstr,usrname)
        {
            /*var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationinteractablesstring[landid] = LZString.decompressFromBase64(fstr);*/
            
            if( !skip_farm_processing )
            {
                //activityLog("indexjs:server: send house pickables string " + usrname);
                /*DEFLATE*/
                //var deflated = zlib.deflateSync(fstr).toString('base64');
                DBConnectionManager.query("UPDATE survival SET interactablesstring = '"+fstr+"' WHERE landid = "+landid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    s_interactables = fstr;
                    
                    //activityLog("updated interactables string " + usrid);
                }); 
                socket.broadcast.to(landid).emit('interactables string to store',fstr);
            }
            
        });

        socket.on('send one interactable string', function(landid,fstr)
        {
            var LZString = require('/usr/lib/node_modules/lz-string/libs/lz-string.js');
            destinationinteractablesstring[landid] = s_interactables;
            destinationinteractablesstring[landid] = LZString.decompressFromBase64(destinationinteractablesstring[landid]);
            destinationinteractablesstring[landid].replace(fstr, "");
            activityLog(destinationinteractablesstring[landid]);
            activityLog(fstr);
            var compressed = LZString.compressToBase64(destinationinteractablesstring[landid]);
            if( !skip_farm_processing )
            {
                DBConnectionManager.query("UPDATE survival SET interactablesstring = '"+compressed+"' WHERE landid = "+landid, null, function(res,err)
                {
                    if(err)
                    {
                        activityLogErr(err);
                        throw err;
                    }
                    s_interactables = compressed;
                    
                    //activityLog("updated house pickables string " + usrid);
                }); 
                socket.broadcast.to(landid).emit('interactables string to store',compressed);
            }
            
        });
        
        socket.on('send items string', function(usrid,fstr,usrname)
        {
            if(usrid == s_userid)
            {
                s_items = fstr; 
            }
            if( !skip_farm_processing )
                executeMysqlStatement(( "UPDATE users_survival SET itemsstring = '"+fstr+"' WHERE userid = "+usrid ));
        });

        socket.on("store all in socket",function(fstr,pstr,istr,landid)
        {
            if(landid == s_landid)
            {
                //activityLog("indexjs:server: store all strings in same client");
                s_farmstring = fstr;
                destinationgroundstring[landid] = fstr;
                s_pickables = pstr;
                destinationpickablesstring[landid] = pstr;
                s_interactables = istr;
                destinationinteractablesstring[landid] = istr;

                activityLog("farmstring: "+s_farmstring);
                activityLog("pickables: "+s_pickables);
                activityLog("interactables: "+s_interactables);

                socket.broadcast.to(landid).emit('all strings to store',fstr,pstr,istr);
            }
            /*else
            {
                //activityLog("indexjs:server: store all strings in another client " + usrname);
                socket.broadcast.to(usrname).emit('all strings to store',fstr,pstr,istr);
            }*/
            
        });     
        /*THIS FUNCTION STORES ALL ITEMS IN THE SAME CLIENT SOCKET*/
        socket.on("store items in socket",function(fstr)
        {
            //activityLog("indexjs:server: store all items in same client");
            s_items = fstr;
        });     

        socket.on("store all strings",function(fstr,pstr,istr,landid)
        {
            //activityLog("indexjs:server: store all strings");
            s_farmstring = fstr;
            s_pickables = pstr;
            s_interactables = istr;
        });

        socket.on("store farm string",function(str)
        {
            //activityLog("indexjs:server: store farm string");
            s_farmstring = str;
        });
        /*
        socket.on("reach socket for username",function(str)
        {
            activityLog("indexjs:server: reach socket for username");
            io.to(str).emit("get socket username", s_username);
        });
        */
        socket.on("store pickables string",function(str)
        {
            //activityLog("indexjs:server: store pickables string");
            s_pickables = str;
        });

        socket.on("store interactables string",function(str)
        {
            //activityLog("indexjs:server: store interactables string");
            s_interactables = str;
        });

        
        socket.on('leave room', function(sock)
        {
            activityLogGreen("SOCKET.LEAVE -> "+sock);
            socket.leave(sock);
        });

        socket.on('join room', function(sock)
        {
            //activityLog("---------------------------------------------------------");
            //activityLog('indexjs:server: join room');
            //activityLog("leave room " + destinationid);
            //activityLog("join room " + sock);
            //activityLog("---------------------------------------------------------");
            activityLogGreen("SOCKET.LEAVE -> "+destinationid);
            socket.leave(destinationid);
            socket.to(destinationid).emit("leave farm", s_socketid);
            destinationid = sock;
            activityLogGreen("SOCKET.JOIN -> "+destinationid);
            socket.join(destinationid);
        });
        //GOING BACK TO MY FARM 2
        socket.on('update farm', function()
        {
            //activityLog('indexjs:server: update farm');
            socket.emit("update farm",s_username,s_userid,s_socketid,s_farmstring,s_pickables,s_interactables);
        });


    socket.on('reconnect', function()
    {
        activityLogBlink("RECONNECTION EVENT");
    });

    socket.on('disconnect', function()
    {
        
        var sid = socket.id;
        var _landid = 0;
        activityLog("DISCONNECTED user " + s_userid);
        
        if(!skip_farm_processing && s_userid != 0) // Se e' 0 questa non DEVE essere chiamata!
        {
            if(s_farmstring != null && s_pickables != null && s_interactables != null)
            {
                var fstr = s_farmstring;
                fstr = fstr.replace(/00000000000000000000000/g,"_");
                fstr = fstr.replace(/0000000000/g,"%");

                //activityLog(fstr);

                var pstr = s_pickables;
                pstr = pstr.replace(/000000/g,"_");
                pstr = pstr.replace(/A666C696E743A313A3001/g,"%");

                //activityLog(pstr);

                var istr = s_interactables;
                istr = istr.replace(/000000/g,"_");
                istr = istr.replace(/303A313A313A303A3/g,"%");
                istr = istr.replace(/70696E653A3/g,"!");
                istr = istr.replace(/77696C6467726173733A/g,"&");
                istr = istr.replace(/6F626A5F696E74657261637461626C655F/g,",");

                activityLog("DISCONNECTED user "+s_userid+" has data to save.");
                activityLog("UPDATING table survival for user " + s_userid);
                /*DEFLATE*/
                var deflated = zlib.deflateSync(fstr).toString('base64');
                var deflated2 = zlib.deflateSync(istr).toString('base64');
                //executeMysqlStatement( ( "UPDATE survival SET groundstring = '"+deflated+"', pickablesstring = '"+pstr+"', interactablesstring = '"+deflated2+"'  WHERE userid = "+s_userid ) );

            }
        }

        activityLogBlink("DISCONNECTION: Looking for user "+s_username+":"+sid+" in destinations");
        var i = 0;
        destinationids.forEach(function(v)
        {
            
            if(sid == v)
            {
                for(var j = 0;j<=i;j++)
                {
                    if(destinationlandids[i] == destinationlandids[j])
                    {
                        if(destinationids[i] == destinationids[j])
                        {
                            _landid = destinationlandids[i];
                        }
                        break;
                    }
                }
                activityLogBlink("DISCONNECTION: found user "+s_username+":"+sid+" in destinations -> removing...");
                destinationids.splice(i, 1);
                destinationnames.splice(i, 1);   
                destinationlandids.splice(i, 1);   
                destinantionnumtrees.splice(i, 1);
                destinantionnumrocks.splice(i, 1);
                destinantionnumplants.splice(i, 1);
                connectedUsers --;
                
                
            }
            else
            {
                i++;
            }
        });
        logDestinations();
        sendSocketsList();
        socket.removeAllListeners();
        socket.disconnect(true);
        io.emit('client disconnected', sid);
        
        activityLog('Got disconnect!');
        
        activityLog('client disconnected, socket: ' + sid);

        if(_landid != 0)
        {
            for(var i = 0;i<destinationlandids.length;i++)
            {
                if(_landid == destinationlandids[i])
                {
                    activityLog("found another mob owner");
                    io.sockets.connected[destinationids[i]].emit('found another mob owner');
                    break;
                }
            }
        }
    });
    
    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
        //activityLog('indexjs:message: ' + msg);
    });

    socket.on('get connected users', function(){
        //activityLog("indexjs:server:get connected users");
        io.emit('update users connected', connectedUsers);
    });
    ///upon receiving this message, emit the socket id to the calling client
    socket.on('get client id', function(){
        //activityLog("indexjs:server:socket:emit client id");
        socket.emit('get client id', socket.id);
    });

    socket.on('send char resting', function(id,val)
    {
        //activityLog("indexjs:server:sendCharResting " + id + " " + val);
        socket.to(destinationid).emit('send char resting',id,val);
    });

    socket.on('send dummy data to owner', function(dmg,sid){
        //activityLog("indexjs:server:sending dummy data");
        io.sockets.connected[sid].emit('send dummy data to owner',dmg,sid);
    });

    socket.on('send dummy data to mob owner', function(mid,dmg,sid){
        //activityLog("indexjs:server:sending dummy data");
        io.sockets.connected[sid].emit('send dummy data to mob owner',mid,dmg,sid);
    });

    socket.on('send char data', function(xx,yy,ss1,ss2,ss3,ss4,ii,sx,sy,id,nam,sta){
        //activityLog("indexjs:server:"+xx+":"+yy+":"+ss1+":"+ss2+":"+ss3+":"+ss4+":"+ii+":"+sx+":"+sy+":"+id+":"+nam+":"+sta);
        //activityLog("indexjs:server:sending char data");
        socket.to(destinationid).emit('send char data',xx,yy,ss1,ss2,ss3,ss4,ii,sx,sy,id,nam,sta);
    });

    socket.on('send mob data', function(xx,yy,si,ii,sx,mid,sid,lf,stt,trgtx,trgty){
        //activityLog("indexjs:server:"+xx+" "+yy+" "+si+" "+ii+" "+sx+" "+id);
        //activityLog("indexjs:server:sending mob data");
        socket.to(destinationid).emit('send mob data',xx,yy,si,ii,sx,mid,sid,lf,stt,trgtx,trgty);
    });

    socket.on('remove pickable', function(xx,yy,oid){
        //activityLog("indexjs:server:remove pickable"+xx+":"+yy+":"+oid+":"+destinationid);
        socket.to(destinationid).emit('remove pickable',xx,yy,oid);
    });

    socket.on('remove interactable', function(xx,yy,oid){
        //activityLog("indexjs:server:remove interactable:"+xx+":"+yy+":"+oid+":"+destinationid);
        socket.to(destinationid).emit('remove interactable',xx,yy,oid);
    });

    socket.on('event perform interactable', function(xx,yy,iid,oid,evt,evn){
        //activityLog("indexjs:server:event perform interactable "+xx+":"+yy+":"+iid+":"+oid+":"+evt+":"+evn+":"+destinationid);
        socket.to(destinationid).emit('event perform interactable',xx,yy,iid,oid,evt,evn);
    });

    socket.on('drop pickable', function(xx,yy,pid,quant){
        //activityLog("indexjs:server:drop pickable"+xx+":"+yy+":"+pid+":"+quant+":"+destinationid);
        socket.to(destinationid).emit('drop pickable',xx,yy,pid,quant);
    });

    socket.on('magnet pickable', function(xx,yy,pid,sid){
        //activityLog("indexjs:server:magnet pickable"+xx+":"+yy+":"+pid+":"+sid+":"+destinationid;
        //socket.broadcast.to(destinationid).emit('magnet pickable',xx,yy,pid,sid);
        socket.to(destinationid).emit('magnet pickable',xx,yy,pid,sid);
    });

    socket.on('change ground tile', function(col,row,type){
        activityLog("indexjs:server:change ground tile"+col+":"+row+":"+type+":"+destinationid);
        socket.to(destinationid).emit('change ground tile',col,row,type);
    });

    socket.on('sow plant', function(col,row,obj,str){
        activityLog("indexjs:server:sow plant"+col+":"+row+":"+obj+":"+str+":"+destinationid);
        socket.to(destinationid).emit('sow plant',col,row,obj,str);
    });

    socket.on('water tile', function(col,row){
        activityLog("indexjs:server:water tile"+col+":"+row+":"+destinationid);
        socket.to(destinationid).emit('water tile',col,row);
    });

    socket.on('send emote', function(usr,type){
        //activityLog("indexjs:server:send emote"+usr+":"+type+":"+destinationid);
        socket.to(destinationid).emit('send emote',usr,type);
    });

    socket.on('place structure', function(xx,yy,obj,id){
        activityLog("indexjs:server:place structure"+xx+":"+yy+":"+obj+":"+id+":"+destinationid);
        socket.to(destinationid).emit('place structure',xx,yy,obj,id);
    });

    socket.on('start structure job', function(xx,yy,obj,id){
        activityLog("indexjs:server:start structure job"+xx+":"+yy+":"+obj+":"+id+":"+destinationid);
        socket.to(destinationid).emit('start structure job',xx,yy,obj,id);
    });

    socket.on('draw equipped', function(sid,xx,yy,ia,si,ii,xs,ys){
        socket.to(destinationid).emit('draw equipped',sid,xx,yy,ia,si,ii,xs,ys);
    });

        socket.on('kick all players', function(){
            //activityLog("kick all players");
            socket.to(destinationid).emit('kick all players');
        });

        socket.on('visit url', function(url)
        {
            socket.emit("send player to", url);
        });

        ////////////////////////////////////////////SURV
         socket.on('send userdata string', function(usrid,str)
        {
            DBConnectionManager.query("UPDATE users_survival SET userdata = '"+str+"' WHERE userid = "+usrid, null, function(res,err)
            {
                if(err)
                {
                    activityLogErr(err);
                    throw err;
                }
                
            }); 
        });
        ////////////////////////////////////////////////


    });


}
