{
    "id": "7a7070ba-4c49-4ec2-81e4-be47b8a992ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_cliff_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3036fef7-31a1-4225-9615-2bcd058b60e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a7070ba-4c49-4ec2-81e4-be47b8a992ce",
            "compositeImage": {
                "id": "046ffeec-856e-4a98-ab4c-7a3613580435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3036fef7-31a1-4225-9615-2bcd058b60e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b8a553-93d1-40c0-b67f-fcee3ea04587",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3036fef7-31a1-4225-9615-2bcd058b60e5",
                    "LayerId": "d529bf2e-72e6-46a3-9689-7466e8d06d12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d529bf2e-72e6-46a3-9689-7466e8d06d12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a7070ba-4c49-4ec2-81e4-be47b8a992ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 18,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}