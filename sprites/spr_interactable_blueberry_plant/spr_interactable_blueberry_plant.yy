{
    "id": "58f26c55-1885-4013-8b21-e980eda92529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_blueberry_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2d23b05-2e12-49a9-84cb-471ed723fa5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58f26c55-1885-4013-8b21-e980eda92529",
            "compositeImage": {
                "id": "89a65c01-f270-42c1-a393-25b68da29e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d23b05-2e12-49a9-84cb-471ed723fa5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232570a6-b59c-47a0-9245-fe700bb105c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d23b05-2e12-49a9-84cb-471ed723fa5a",
                    "LayerId": "c69909a7-b251-4550-bd51-78a4ba5d68cf"
                }
            ]
        },
        {
            "id": "bda58b09-b34f-430f-ac77-6893dc909620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58f26c55-1885-4013-8b21-e980eda92529",
            "compositeImage": {
                "id": "46332e25-850a-4deb-b487-3ca1cb9ed424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda58b09-b34f-430f-ac77-6893dc909620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb32567-a848-4205-a0a8-d0e103a685ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda58b09-b34f-430f-ac77-6893dc909620",
                    "LayerId": "c69909a7-b251-4550-bd51-78a4ba5d68cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c69909a7-b251-4550-bd51-78a4ba5d68cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58f26c55-1885-4013-8b21-e980eda92529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 14,
    "yorig": 29
}