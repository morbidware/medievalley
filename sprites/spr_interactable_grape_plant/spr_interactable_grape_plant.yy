{
    "id": "dacd4985-9ea2-4418-9db7-a73372354135",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_grape_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2e2c8d0-65e6-4aca-81f0-a01b28f66d2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "compositeImage": {
                "id": "17f5534c-e1ce-4913-9035-339267cb5689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e2c8d0-65e6-4aca-81f0-a01b28f66d2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd66328-7762-4304-87ef-04166c5f4429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e2c8d0-65e6-4aca-81f0-a01b28f66d2b",
                    "LayerId": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6"
                }
            ]
        },
        {
            "id": "563aa92e-c941-4d8b-92d6-2c3a15805513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "compositeImage": {
                "id": "680b0d93-0c40-4118-96cb-982ca1458670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563aa92e-c941-4d8b-92d6-2c3a15805513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3cc1f10-abb3-4119-ad2c-0cfcd82e6b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563aa92e-c941-4d8b-92d6-2c3a15805513",
                    "LayerId": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6"
                }
            ]
        },
        {
            "id": "c2bee762-f944-4d7a-98e9-f49fc70e8b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "compositeImage": {
                "id": "cc5f37fb-eb83-4db0-95ca-40d994c63fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2bee762-f944-4d7a-98e9-f49fc70e8b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1496dc4f-651d-4e89-8486-eac731b635e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2bee762-f944-4d7a-98e9-f49fc70e8b08",
                    "LayerId": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6"
                }
            ]
        },
        {
            "id": "80ee4eca-6b0a-49c7-b54d-a021c2df0f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "compositeImage": {
                "id": "b2dfec9a-f825-4b35-aa83-edd72854c3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80ee4eca-6b0a-49c7-b54d-a021c2df0f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72a7781a-65b4-44fd-bc0e-28790cc26e81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80ee4eca-6b0a-49c7-b54d-a021c2df0f19",
                    "LayerId": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6"
                }
            ]
        },
        {
            "id": "c8fa1ff3-7323-4497-b2cf-e2514c089810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "compositeImage": {
                "id": "e2f9de44-b5d2-469e-99a7-c26c51a9f554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8fa1ff3-7323-4497-b2cf-e2514c089810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8999d874-a500-42cd-b201-a4e27a5c4239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8fa1ff3-7323-4497-b2cf-e2514c089810",
                    "LayerId": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6"
                }
            ]
        },
        {
            "id": "bba80c31-dd2e-42e2-8867-e63f1de72219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "compositeImage": {
                "id": "ac1f7e6c-77b7-47a5-9f6c-69e139247c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bba80c31-dd2e-42e2-8867-e63f1de72219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8978b67a-a42b-46a9-a60d-35b0ca05d663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bba80c31-dd2e-42e2-8867-e63f1de72219",
                    "LayerId": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "75a38a2d-ecad-439e-a5f3-6d9cae9186c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dacd4985-9ea2-4418-9db7-a73372354135",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 29
}