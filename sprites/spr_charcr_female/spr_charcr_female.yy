{
    "id": "431cc3e0-a3c6-45ae-9a44-5f21f6ccd660",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_female",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd74466c-4d3f-44ad-9478-fb4b03126533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "431cc3e0-a3c6-45ae-9a44-5f21f6ccd660",
            "compositeImage": {
                "id": "a1404455-8646-4b88-9760-313f4955737b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd74466c-4d3f-44ad-9478-fb4b03126533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53cff795-191a-4bb9-99c5-90cb2cc04675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd74466c-4d3f-44ad-9478-fb4b03126533",
                    "LayerId": "3750d379-f36e-4ac7-a35c-530f4c5a3578"
                }
            ]
        },
        {
            "id": "78bc628b-84a3-47f8-a6d4-d4cc1f13705b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "431cc3e0-a3c6-45ae-9a44-5f21f6ccd660",
            "compositeImage": {
                "id": "f6fb5b4f-6013-4c04-bf35-24b8c14f6c7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78bc628b-84a3-47f8-a6d4-d4cc1f13705b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caed4dec-8dac-43b3-87f5-1e36766c55a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78bc628b-84a3-47f8-a6d4-d4cc1f13705b",
                    "LayerId": "3750d379-f36e-4ac7-a35c-530f4c5a3578"
                }
            ]
        },
        {
            "id": "de873b18-c96a-4618-bf39-f9387ca378d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "431cc3e0-a3c6-45ae-9a44-5f21f6ccd660",
            "compositeImage": {
                "id": "b65b5f7c-3ca1-4c43-a573-c819c895cd37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de873b18-c96a-4618-bf39-f9387ca378d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2211dfd5-b6f1-4b4e-81bc-7bcc7f55e9e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de873b18-c96a-4618-bf39-f9387ca378d1",
                    "LayerId": "3750d379-f36e-4ac7-a35c-530f4c5a3578"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3750d379-f36e-4ac7-a35c-530f4c5a3578",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "431cc3e0-a3c6-45ae-9a44-5f21f6ccd660",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}