{
    "id": "2e73bad2-2808-4aff-ab46-3eeeca4ccd2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00791cb4-e291-4a44-832c-03befafd9370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e73bad2-2808-4aff-ab46-3eeeca4ccd2d",
            "compositeImage": {
                "id": "bf24eb1b-a3f2-4fa1-abb8-73b10dbb8b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00791cb4-e291-4a44-832c-03befafd9370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b784d950-bcde-48ef-aae5-02278c787f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00791cb4-e291-4a44-832c-03befafd9370",
                    "LayerId": "d088d3fd-9935-401f-a55b-be8f4fabe1c2"
                }
            ]
        },
        {
            "id": "01923e0b-0efd-4db5-a2ad-c2b3b25b28b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e73bad2-2808-4aff-ab46-3eeeca4ccd2d",
            "compositeImage": {
                "id": "49dca38a-41ab-4c15-bd4d-44de35eb3c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01923e0b-0efd-4db5-a2ad-c2b3b25b28b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a3f03ab-9f35-4f7a-b393-090da1ee31a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01923e0b-0efd-4db5-a2ad-c2b3b25b28b5",
                    "LayerId": "d088d3fd-9935-401f-a55b-be8f4fabe1c2"
                }
            ]
        },
        {
            "id": "a762fe9b-fd97-47b9-b7b6-4bca9a0290c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e73bad2-2808-4aff-ab46-3eeeca4ccd2d",
            "compositeImage": {
                "id": "296d33d8-0db1-4527-a6a4-8502a647fd25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a762fe9b-fd97-47b9-b7b6-4bca9a0290c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c4bb5d-ddd5-4393-acd9-e9ee0b7c90ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a762fe9b-fd97-47b9-b7b6-4bca9a0290c7",
                    "LayerId": "d088d3fd-9935-401f-a55b-be8f4fabe1c2"
                }
            ]
        },
        {
            "id": "ed43ef1b-b792-472d-a314-65681b9df8a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e73bad2-2808-4aff-ab46-3eeeca4ccd2d",
            "compositeImage": {
                "id": "94d7089a-8374-43e3-99c1-6a7ecc84cda6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed43ef1b-b792-472d-a314-65681b9df8a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545d5aa8-06e4-4acb-a22c-ac019150af5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed43ef1b-b792-472d-a314-65681b9df8a9",
                    "LayerId": "d088d3fd-9935-401f-a55b-be8f4fabe1c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d088d3fd-9935-401f-a55b-be8f4fabe1c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e73bad2-2808-4aff-ab46-3eeeca4ccd2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}