{
    "id": "be30e851-4cd7-4ff3-a45b-a743d7f00a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_selling_npc_blueprints_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01d533ac-47c1-47b9-ba90-29312bf555ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be30e851-4cd7-4ff3-a45b-a743d7f00a41",
            "compositeImage": {
                "id": "6541175d-7dec-4e33-9ea7-2f8f8068b893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d533ac-47c1-47b9-ba90-29312bf555ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6be6258b-210b-4161-b430-a3f2d2d6bb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d533ac-47c1-47b9-ba90-29312bf555ae",
                    "LayerId": "ada5a28b-3b16-4feb-8d30-c01e5e694ee9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ada5a28b-3b16-4feb-8d30-c01e5e694ee9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be30e851-4cd7-4ff3-a45b-a743d7f00a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}