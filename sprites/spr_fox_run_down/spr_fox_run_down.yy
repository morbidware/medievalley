{
    "id": "6a61917b-2423-4705-a3da-a7841982cc69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b216fbdc-101d-4336-9c8f-639361b118f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "compositeImage": {
                "id": "0ad6ff36-7db1-4620-ae7e-32cd4ae89181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b216fbdc-101d-4336-9c8f-639361b118f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae8f1a3-24a4-4690-bf14-21297e6799f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b216fbdc-101d-4336-9c8f-639361b118f1",
                    "LayerId": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0"
                }
            ]
        },
        {
            "id": "70ba621f-049e-4d4e-add9-2fcc624de279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "compositeImage": {
                "id": "1bdf4444-6cfb-4c5e-a9a2-1df75238d1dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ba621f-049e-4d4e-add9-2fcc624de279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecf07e97-f933-4328-8e04-a4ea94fec34a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ba621f-049e-4d4e-add9-2fcc624de279",
                    "LayerId": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0"
                }
            ]
        },
        {
            "id": "bde110a4-de09-47a3-94ee-6f55c9ba4407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "compositeImage": {
                "id": "9e9926e0-1b80-4a35-9579-21e8ea1d95ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde110a4-de09-47a3-94ee-6f55c9ba4407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c033d210-c82c-45fe-be9c-c78147ebf800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde110a4-de09-47a3-94ee-6f55c9ba4407",
                    "LayerId": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0"
                }
            ]
        },
        {
            "id": "1034f696-7d08-4470-b291-ceb03bd44132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "compositeImage": {
                "id": "ea0a8325-00f4-4d2d-a280-b86bea0ab847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1034f696-7d08-4470-b291-ceb03bd44132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b9f6278-e8b6-4d68-9cdc-a38ed3fd04fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1034f696-7d08-4470-b291-ceb03bd44132",
                    "LayerId": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0"
                }
            ]
        },
        {
            "id": "36dd4a86-1963-4e66-9208-2ee0c2cc4b01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "compositeImage": {
                "id": "40b9fc2f-001d-455b-827c-f3024555d84a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36dd4a86-1963-4e66-9208-2ee0c2cc4b01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66cfefc-2385-402e-8d1a-6db5433517b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36dd4a86-1963-4e66-9208-2ee0c2cc4b01",
                    "LayerId": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0"
                }
            ]
        },
        {
            "id": "a7853652-6584-4f45-86c0-bea87926a25a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "compositeImage": {
                "id": "65d91df1-5f8a-45a7-8734-cbdc4b74f40e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7853652-6584-4f45-86c0-bea87926a25a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efcb7b23-9155-495b-aec3-f2056d6e2c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7853652-6584-4f45-86c0-bea87926a25a",
                    "LayerId": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "2c83b7e5-d862-48b4-87d1-40a5befd7ee0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a61917b-2423-4705-a3da-a7841982cc69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}