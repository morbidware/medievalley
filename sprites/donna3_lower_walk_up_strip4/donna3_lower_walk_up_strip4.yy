{
    "id": "4169e3ba-c6f8-428e-ac14-08ed2c09d6ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6a81ebe-f792-4bc9-ba00-081ee79368de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4169e3ba-c6f8-428e-ac14-08ed2c09d6ec",
            "compositeImage": {
                "id": "05162935-e606-4c22-a9d9-49e8c168419d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a81ebe-f792-4bc9-ba00-081ee79368de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be5f566-9fa2-454a-952e-1b9b7d2a3d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a81ebe-f792-4bc9-ba00-081ee79368de",
                    "LayerId": "8990ba65-4cc8-488f-90d6-07fde9dce78d"
                }
            ]
        },
        {
            "id": "5f16cbcd-33fe-4cf5-b114-809fc43c3ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4169e3ba-c6f8-428e-ac14-08ed2c09d6ec",
            "compositeImage": {
                "id": "b6919ba9-bbb0-45e8-80ca-cc97a53633d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f16cbcd-33fe-4cf5-b114-809fc43c3ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "256ad40d-aec1-46d2-bab0-eb30ca6e569f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f16cbcd-33fe-4cf5-b114-809fc43c3ecc",
                    "LayerId": "8990ba65-4cc8-488f-90d6-07fde9dce78d"
                }
            ]
        },
        {
            "id": "9a44b8be-0843-424a-8663-c40a8c30230a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4169e3ba-c6f8-428e-ac14-08ed2c09d6ec",
            "compositeImage": {
                "id": "f091f24c-4109-421a-a8d6-95aee80d9606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a44b8be-0843-424a-8663-c40a8c30230a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da5791d-25ca-4944-8f87-6cd9ffd81f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a44b8be-0843-424a-8663-c40a8c30230a",
                    "LayerId": "8990ba65-4cc8-488f-90d6-07fde9dce78d"
                }
            ]
        },
        {
            "id": "1a8f4031-ca1d-4f0a-9f00-6d64b7fb226c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4169e3ba-c6f8-428e-ac14-08ed2c09d6ec",
            "compositeImage": {
                "id": "60a63d7c-3e78-46cf-8a36-6783a8b938d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a8f4031-ca1d-4f0a-9f00-6d64b7fb226c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a9e148-865f-412f-8188-826d853c0e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a8f4031-ca1d-4f0a-9f00-6d64b7fb226c",
                    "LayerId": "8990ba65-4cc8-488f-90d6-07fde9dce78d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8990ba65-4cc8-488f-90d6-07fde9dce78d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4169e3ba-c6f8-428e-ac14-08ed2c09d6ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}