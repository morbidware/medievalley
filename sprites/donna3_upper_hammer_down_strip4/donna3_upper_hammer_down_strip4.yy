{
    "id": "2479cbfd-9d37-4dde-b798-52120fe80402",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c24dfbf-96a3-4ba7-b60c-279a76df0de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2479cbfd-9d37-4dde-b798-52120fe80402",
            "compositeImage": {
                "id": "3f6f7e0b-6b4c-43c9-b5bc-583edd648d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c24dfbf-96a3-4ba7-b60c-279a76df0de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c712ea2-b6d6-4285-a708-5de3e49ac3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c24dfbf-96a3-4ba7-b60c-279a76df0de7",
                    "LayerId": "a215cbb3-4f0e-4818-a0dd-45deeec3a740"
                }
            ]
        },
        {
            "id": "635512c5-b886-4533-89f9-c9c4cf10281d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2479cbfd-9d37-4dde-b798-52120fe80402",
            "compositeImage": {
                "id": "b5ad5b4a-076b-404b-a185-e94f5dffc07c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635512c5-b886-4533-89f9-c9c4cf10281d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2331b86-8f66-4621-b9c0-33c5ce88480d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635512c5-b886-4533-89f9-c9c4cf10281d",
                    "LayerId": "a215cbb3-4f0e-4818-a0dd-45deeec3a740"
                }
            ]
        },
        {
            "id": "653f00e6-b4ad-49cd-93bf-14d84ef6900c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2479cbfd-9d37-4dde-b798-52120fe80402",
            "compositeImage": {
                "id": "e439fbd6-f5f6-4366-9dc4-89a9a1bdd7db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "653f00e6-b4ad-49cd-93bf-14d84ef6900c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae3feef4-20ed-4f04-8ca6-13d216060dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "653f00e6-b4ad-49cd-93bf-14d84ef6900c",
                    "LayerId": "a215cbb3-4f0e-4818-a0dd-45deeec3a740"
                }
            ]
        },
        {
            "id": "d18df084-46a6-4f87-b70f-9bbf41bfecdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2479cbfd-9d37-4dde-b798-52120fe80402",
            "compositeImage": {
                "id": "11c09389-4231-4af2-8051-364fd0cc6a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18df084-46a6-4f87-b70f-9bbf41bfecdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880f50ea-9a90-4574-ae74-ea4a96561b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18df084-46a6-4f87-b70f-9bbf41bfecdb",
                    "LayerId": "a215cbb3-4f0e-4818-a0dd-45deeec3a740"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a215cbb3-4f0e-4818-a0dd-45deeec3a740",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2479cbfd-9d37-4dde-b798-52120fe80402",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}