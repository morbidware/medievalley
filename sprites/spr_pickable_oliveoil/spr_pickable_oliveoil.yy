{
    "id": "01b7cf13-3a00-44c2-880b-1f96f2659276",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_oliveoil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a9440d3-d2e9-4299-9f51-c4dddcd55dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01b7cf13-3a00-44c2-880b-1f96f2659276",
            "compositeImage": {
                "id": "edba406e-a20e-44b5-b73e-8133329bd053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a9440d3-d2e9-4299-9f51-c4dddcd55dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f56c2f-261e-4c72-a08c-cba7aada9210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a9440d3-d2e9-4299-9f51-c4dddcd55dfe",
                    "LayerId": "4a2b7616-d505-4c03-8243-67de3e452aea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4a2b7616-d505-4c03-8243-67de3e452aea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01b7cf13-3a00-44c2-880b-1f96f2659276",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}