{
    "id": "dee97aff-2dda-4611-a20a-0939d81379d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_tomatoes_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6826fc97-27f8-4773-87ff-9b7196ee3db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dee97aff-2dda-4611-a20a-0939d81379d6",
            "compositeImage": {
                "id": "63e245b1-e78e-487c-bc79-5dcb93d6e936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6826fc97-27f8-4773-87ff-9b7196ee3db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce038b91-45cb-4bbf-80ec-ea4bc17c59b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6826fc97-27f8-4773-87ff-9b7196ee3db9",
                    "LayerId": "d751cf5c-1239-43ef-9ab2-fbb55a3c29da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d751cf5c-1239-43ef-9ab2-fbb55a3c29da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dee97aff-2dda-4611-a20a-0939d81379d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}