{
    "id": "f97ccd5e-eb82-4aa6-a098-ee63aab5a75d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a31a8026-0512-42ac-b3f0-a9e535654ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f97ccd5e-eb82-4aa6-a098-ee63aab5a75d",
            "compositeImage": {
                "id": "b8f1cb7c-72da-427a-b9c9-08a3203c8da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31a8026-0512-42ac-b3f0-a9e535654ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a6547c-512d-40b8-9ebc-7e85584db781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31a8026-0512-42ac-b3f0-a9e535654ddf",
                    "LayerId": "7f87cc0b-1aec-4f17-9584-6b7820d2eb3b"
                }
            ]
        },
        {
            "id": "d1c17c2f-2e6f-411c-9af2-d9398088da82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f97ccd5e-eb82-4aa6-a098-ee63aab5a75d",
            "compositeImage": {
                "id": "fcc08ab7-8afd-4b4f-82d9-20a1f3aca98f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c17c2f-2e6f-411c-9af2-d9398088da82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c3db3a5-3a5e-4d54-a5e7-3f5278548f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c17c2f-2e6f-411c-9af2-d9398088da82",
                    "LayerId": "7f87cc0b-1aec-4f17-9584-6b7820d2eb3b"
                }
            ]
        },
        {
            "id": "230717c2-2db5-45b0-9ba2-41299a929c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f97ccd5e-eb82-4aa6-a098-ee63aab5a75d",
            "compositeImage": {
                "id": "945e557b-1481-4ab9-a1f5-57cab9881e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "230717c2-2db5-45b0-9ba2-41299a929c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "174c38e6-3a88-4503-9b6a-f1be2f5e89b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "230717c2-2db5-45b0-9ba2-41299a929c63",
                    "LayerId": "7f87cc0b-1aec-4f17-9584-6b7820d2eb3b"
                }
            ]
        },
        {
            "id": "72560fa9-9c60-446d-81f5-08a412dbb753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f97ccd5e-eb82-4aa6-a098-ee63aab5a75d",
            "compositeImage": {
                "id": "e069dc76-3bc5-4b3f-a59f-8ce3a3b3d846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72560fa9-9c60-446d-81f5-08a412dbb753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27ab27dc-699e-4c8d-aba2-cf8130631e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72560fa9-9c60-446d-81f5-08a412dbb753",
                    "LayerId": "7f87cc0b-1aec-4f17-9584-6b7820d2eb3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f87cc0b-1aec-4f17-9584-6b7820d2eb3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f97ccd5e-eb82-4aa6-a098-ee63aab5a75d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}