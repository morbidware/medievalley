{
    "id": "c8fa806b-7aae-484b-a821-8a5918e125b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_body_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfab1866-7392-477d-9fae-1b57c0422ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8fa806b-7aae-484b-a821-8a5918e125b3",
            "compositeImage": {
                "id": "200cf756-4ec0-4e6e-88f6-e8e63d2b622e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfab1866-7392-477d-9fae-1b57c0422ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4a1e510-dd8f-4e14-bf9a-fdbeeeb8a8b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfab1866-7392-477d-9fae-1b57c0422ce3",
                    "LayerId": "69902b53-1465-4343-ae09-0d099ddf79d8"
                }
            ]
        },
        {
            "id": "6252199e-3070-4b53-8747-13216be31433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8fa806b-7aae-484b-a821-8a5918e125b3",
            "compositeImage": {
                "id": "0ad5b260-93e9-480b-9e7f-4ecfff779741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6252199e-3070-4b53-8747-13216be31433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef6306b5-acf2-48f6-85cd-82609a35face",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6252199e-3070-4b53-8747-13216be31433",
                    "LayerId": "69902b53-1465-4343-ae09-0d099ddf79d8"
                }
            ]
        },
        {
            "id": "e1b6bfca-7c6c-492b-9f09-424abbb42211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8fa806b-7aae-484b-a821-8a5918e125b3",
            "compositeImage": {
                "id": "4bdaf73c-2e0c-4a56-8d2a-85950bd8e26e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1b6bfca-7c6c-492b-9f09-424abbb42211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18e0e61-db83-4624-99ce-d1150914aa7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1b6bfca-7c6c-492b-9f09-424abbb42211",
                    "LayerId": "69902b53-1465-4343-ae09-0d099ddf79d8"
                }
            ]
        },
        {
            "id": "e3bd4b57-2b49-475b-a4d0-3d19eb566708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8fa806b-7aae-484b-a821-8a5918e125b3",
            "compositeImage": {
                "id": "03a48756-7758-4526-b564-fdf408524854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3bd4b57-2b49-475b-a4d0-3d19eb566708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1a5b23-5f6b-43e6-b8a4-bd936747e5b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3bd4b57-2b49-475b-a4d0-3d19eb566708",
                    "LayerId": "69902b53-1465-4343-ae09-0d099ddf79d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "69902b53-1465-4343-ae09-0d099ddf79d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8fa806b-7aae-484b-a821-8a5918e125b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}