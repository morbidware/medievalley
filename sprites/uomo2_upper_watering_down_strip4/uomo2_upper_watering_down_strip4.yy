{
    "id": "630c3ea7-a962-42b4-b11d-4f41ef4049e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1ac5a8c-3c8f-4f7a-bfb6-34c8ddf70a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630c3ea7-a962-42b4-b11d-4f41ef4049e8",
            "compositeImage": {
                "id": "f28146f3-0234-46e0-b89b-d2016f483ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1ac5a8c-3c8f-4f7a-bfb6-34c8ddf70a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97777837-58ef-4d48-82af-a7f2de86ee65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1ac5a8c-3c8f-4f7a-bfb6-34c8ddf70a2d",
                    "LayerId": "9fca7812-637b-4c05-9a8c-8bc01f500fd6"
                }
            ]
        },
        {
            "id": "6692a059-777a-42f7-99ca-813a4760a84a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630c3ea7-a962-42b4-b11d-4f41ef4049e8",
            "compositeImage": {
                "id": "4a38007e-a944-466e-b9f2-8a3e6c9113d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6692a059-777a-42f7-99ca-813a4760a84a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8f127c7-6433-4234-b552-f64a5259888e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6692a059-777a-42f7-99ca-813a4760a84a",
                    "LayerId": "9fca7812-637b-4c05-9a8c-8bc01f500fd6"
                }
            ]
        },
        {
            "id": "d6d0cf75-59ca-4629-9e67-2c3c71efdf1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630c3ea7-a962-42b4-b11d-4f41ef4049e8",
            "compositeImage": {
                "id": "8a13cff8-ccd2-46f4-9352-2d02978ba7cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d0cf75-59ca-4629-9e67-2c3c71efdf1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f17360f-8e6b-4a5b-a40c-e139611b8c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d0cf75-59ca-4629-9e67-2c3c71efdf1c",
                    "LayerId": "9fca7812-637b-4c05-9a8c-8bc01f500fd6"
                }
            ]
        },
        {
            "id": "7aa45526-829c-4388-8a02-e707893f4cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "630c3ea7-a962-42b4-b11d-4f41ef4049e8",
            "compositeImage": {
                "id": "7397c182-3b81-4bb1-aa83-7847de5062d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa45526-829c-4388-8a02-e707893f4cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2ea441-f1a8-44ae-a61d-cda00f1b9b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa45526-829c-4388-8a02-e707893f4cc1",
                    "LayerId": "9fca7812-637b-4c05-9a8c-8bc01f500fd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fca7812-637b-4c05-9a8c-8bc01f500fd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "630c3ea7-a962-42b4-b11d-4f41ef4049e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}