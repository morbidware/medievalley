{
    "id": "68adeba0-bd68-4b26-8942-7391cc992e33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_watered_stain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5702b4b7-ff9a-4316-8ee5-528ef2136f05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68adeba0-bd68-4b26-8942-7391cc992e33",
            "compositeImage": {
                "id": "1372f290-5a44-4051-8060-35651a63f2e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5702b4b7-ff9a-4316-8ee5-528ef2136f05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c2fe6b2-801a-49e1-a5d5-6be34e5ae7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5702b4b7-ff9a-4316-8ee5-528ef2136f05",
                    "LayerId": "3034bae8-2677-42f3-8186-6fa400c7145e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3034bae8-2677-42f3-8186-6fa400c7145e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68adeba0-bd68-4b26-8942-7391cc992e33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}