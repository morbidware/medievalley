{
    "id": "5f9626e9-8e1c-434a-b9ba-4333eab10772",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08aa4d7c-20c2-4854-89c1-3a4a99533846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f9626e9-8e1c-434a-b9ba-4333eab10772",
            "compositeImage": {
                "id": "aabcb523-225d-4bb9-a1b0-cc8f74facc5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08aa4d7c-20c2-4854-89c1-3a4a99533846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d674eec5-57b2-4c39-a8dc-1c24fdc1b979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08aa4d7c-20c2-4854-89c1-3a4a99533846",
                    "LayerId": "1856325c-f845-458b-ac75-3f9bb70c99a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1856325c-f845-458b-ac75-3f9bb70c99a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f9626e9-8e1c-434a-b9ba-4333eab10772",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}