{
    "id": "5ecfe58a-fb91-4029-967c-6e4814db809b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "261122ba-0081-4705-a327-aa56c4ea6c8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ecfe58a-fb91-4029-967c-6e4814db809b",
            "compositeImage": {
                "id": "2918cd4a-083f-41e8-b000-f0126f4eabac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "261122ba-0081-4705-a327-aa56c4ea6c8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0fa8265-749f-4389-b825-d1304c6c83ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "261122ba-0081-4705-a327-aa56c4ea6c8f",
                    "LayerId": "f1ef011b-b15a-4b7d-89d7-8b28530fc098"
                }
            ]
        },
        {
            "id": "0dee547e-7ba6-4e1c-967e-66246d8994b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ecfe58a-fb91-4029-967c-6e4814db809b",
            "compositeImage": {
                "id": "f21b0916-008c-4ace-bcbd-41e9eb237927",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dee547e-7ba6-4e1c-967e-66246d8994b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14535c86-da88-4864-892d-e148e6cd4f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dee547e-7ba6-4e1c-967e-66246d8994b6",
                    "LayerId": "f1ef011b-b15a-4b7d-89d7-8b28530fc098"
                }
            ]
        },
        {
            "id": "7ac81c43-b504-41c3-80d3-ff893086409f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ecfe58a-fb91-4029-967c-6e4814db809b",
            "compositeImage": {
                "id": "0ea0094d-a732-4e2e-96dc-3d258bff3173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac81c43-b504-41c3-80d3-ff893086409f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d05413-1a13-47c3-ba05-6f741c5cef51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac81c43-b504-41c3-80d3-ff893086409f",
                    "LayerId": "f1ef011b-b15a-4b7d-89d7-8b28530fc098"
                }
            ]
        },
        {
            "id": "958606c9-96fa-449c-bab4-5e7410864219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ecfe58a-fb91-4029-967c-6e4814db809b",
            "compositeImage": {
                "id": "d42476a0-8442-4794-bf02-40e8ac2c010c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "958606c9-96fa-449c-bab4-5e7410864219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be8bc94-8e66-4d08-a4c0-eb1995a6b5a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "958606c9-96fa-449c-bab4-5e7410864219",
                    "LayerId": "f1ef011b-b15a-4b7d-89d7-8b28530fc098"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f1ef011b-b15a-4b7d-89d7-8b28530fc098",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ecfe58a-fb91-4029-967c-6e4814db809b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}