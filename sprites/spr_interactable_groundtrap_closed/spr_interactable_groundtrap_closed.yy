{
    "id": "3910d12f-b7c8-43af-a300-595571e99f19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_groundtrap_closed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6777d550-d9aa-4fd9-824f-387639bbb17e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3910d12f-b7c8-43af-a300-595571e99f19",
            "compositeImage": {
                "id": "57045bdc-ec3f-4eea-989e-f3c8ceaba3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6777d550-d9aa-4fd9-824f-387639bbb17e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d6c6544-422e-4bb3-8fe1-98d5739fab7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6777d550-d9aa-4fd9-824f-387639bbb17e",
                    "LayerId": "ed5c6d9c-9a55-457e-a1b5-b2c88d7e7dfd"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 16,
    "layers": [
        {
            "id": "ed5c6d9c-9a55-457e-a1b5-b2c88d7e7dfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3910d12f-b7c8-43af-a300-595571e99f19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}