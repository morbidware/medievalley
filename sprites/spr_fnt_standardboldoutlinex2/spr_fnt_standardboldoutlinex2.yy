{
    "id": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_standardboldoutlinex2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3116456-65de-4b89-a5a2-436a61ba2957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "7a07be27-8770-4164-a051-60a7d73d571f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3116456-65de-4b89-a5a2-436a61ba2957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db615887-1bd5-492f-a232-e91c33634c5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3116456-65de-4b89-a5a2-436a61ba2957",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "b083b283-3ee5-4121-88ea-b4256fbcbace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "1cd19db8-5e7a-46d6-ada2-a5a553950b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b083b283-3ee5-4121-88ea-b4256fbcbace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3795081-5be2-46be-9367-0c4b89107602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b083b283-3ee5-4121-88ea-b4256fbcbace",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "f9c4f26b-68ed-4981-8c13-8481ffc17885",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "407f47d8-8b80-4cf5-85b9-6cb130bfa02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c4f26b-68ed-4981-8c13-8481ffc17885",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3349c2-f39d-47d5-ab39-5f819df74c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c4f26b-68ed-4981-8c13-8481ffc17885",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "3bd98a26-0fe5-4331-adf1-6fdb45ad2e2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "3220cb48-1ffe-4c45-ba81-458ff96f0294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd98a26-0fe5-4331-adf1-6fdb45ad2e2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36da7fb2-423a-40b5-9a18-491c718e1ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd98a26-0fe5-4331-adf1-6fdb45ad2e2e",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "e2725c50-5c1b-4171-950c-344b445f901c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "bfc7113f-054e-4ed6-a36d-c1430d3248ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2725c50-5c1b-4171-950c-344b445f901c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da84cb46-81a0-4bb1-9218-25f4a5031429",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2725c50-5c1b-4171-950c-344b445f901c",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "85537da3-a391-4b68-a1a3-c92b2f907db0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "9e5a46c8-71aa-4223-9c51-0810fa330ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85537da3-a391-4b68-a1a3-c92b2f907db0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73cbb2d8-7d15-4dea-8659-caee364fd1cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85537da3-a391-4b68-a1a3-c92b2f907db0",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "87459918-52b8-4cd3-af62-2726e6445c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "ac93cbbc-2c29-44e1-b5db-6cc033811ec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87459918-52b8-4cd3-af62-2726e6445c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aa81cc6-4344-40fa-8974-6c2493550cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87459918-52b8-4cd3-af62-2726e6445c5a",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "46ede688-33ec-4d98-9c5c-c8b606128f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "e3f9ed07-dac3-46b9-9d89-dc5246cac327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ede688-33ec-4d98-9c5c-c8b606128f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e4fa43a-b926-4788-b594-f90b19dda09a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ede688-33ec-4d98-9c5c-c8b606128f34",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "662f052d-8222-4372-910a-14b272f64fa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "d8cf9d16-173e-402b-9e58-aad0c79162f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "662f052d-8222-4372-910a-14b272f64fa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf72f383-f9e9-43d2-ba08-10d362d48bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "662f052d-8222-4372-910a-14b272f64fa4",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "21e2e3df-0e35-4f67-a866-87db7b6d0020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "cc83348e-d50d-48e3-96c8-40d8960f5d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e2e3df-0e35-4f67-a866-87db7b6d0020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8fd6be2-d1b4-4041-9697-360b60f2140d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e2e3df-0e35-4f67-a866-87db7b6d0020",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "0fcdd0ce-5bac-4a6f-8b70-71ce51b5d225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "ae2333fb-4e41-4608-a346-735c3b9601fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fcdd0ce-5bac-4a6f-8b70-71ce51b5d225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef0dabd4-5221-42f3-a90b-9ea235df7326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fcdd0ce-5bac-4a6f-8b70-71ce51b5d225",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "a9c3a1a9-3d01-4cf5-ad06-dfeee1b422e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "af5aa744-cc24-48e4-80ba-79fe2c3197a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c3a1a9-3d01-4cf5-ad06-dfeee1b422e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ff8552-7cf2-46b2-8ea9-08dd46ed8c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c3a1a9-3d01-4cf5-ad06-dfeee1b422e1",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "2d8ef199-de5c-49df-829e-f89ccf988e38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "fedf90bc-6f2f-45cf-a04c-a2ae41e7cc2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d8ef199-de5c-49df-829e-f89ccf988e38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aecc99a8-7ca2-4a64-b7ef-ae3331680056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d8ef199-de5c-49df-829e-f89ccf988e38",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "989f6154-3923-46a6-8c08-7fd834527e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "f7865de9-9b56-4b4d-8e50-3aec70d55ada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989f6154-3923-46a6-8c08-7fd834527e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94158747-4908-4a8c-af70-f0e608ed1579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989f6154-3923-46a6-8c08-7fd834527e82",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "edfbfd29-58a4-4103-9303-c23a3d86c61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "3fdb7fad-fd5b-4fe5-a449-311e3c3c5cff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edfbfd29-58a4-4103-9303-c23a3d86c61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7579d3bb-f76b-49b3-98df-fb3ea26bd58a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edfbfd29-58a4-4103-9303-c23a3d86c61a",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "c995a3a9-027c-450f-a8c0-d49630a3a2d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "389affbc-a015-450e-894e-a67c719820c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c995a3a9-027c-450f-a8c0-d49630a3a2d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8298c172-f825-4f5a-940b-dfd42c70eb74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c995a3a9-027c-450f-a8c0-d49630a3a2d6",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "bf4e3375-7889-408a-8725-1173c4f5db40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "a015941f-306b-4c80-83d2-6d62d2ae43c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4e3375-7889-408a-8725-1173c4f5db40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6a3ffc8-548d-4f4c-8877-ac46be80b4a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4e3375-7889-408a-8725-1173c4f5db40",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "05c98a03-0148-4fc6-b627-42d2a139a298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "0e8e110f-a701-4eb1-b7eb-bcb09d611899",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c98a03-0148-4fc6-b627-42d2a139a298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "122eb4e7-f4a1-4490-a022-43cbe523ca58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c98a03-0148-4fc6-b627-42d2a139a298",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "3cd83208-222e-4c00-bb8d-f38c50eb68a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "e72bb82a-cbcf-4957-9d22-89e6bd5409ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd83208-222e-4c00-bb8d-f38c50eb68a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871f4613-d2bc-4043-969f-d9d75a19b0dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd83208-222e-4c00-bb8d-f38c50eb68a7",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "e7d6f664-c8b6-469c-90c2-9bb93b2474ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "5eb06f44-6176-4406-b2ef-de2e29308618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7d6f664-c8b6-469c-90c2-9bb93b2474ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aae9490-f07a-4285-b410-612c344d2eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7d6f664-c8b6-469c-90c2-9bb93b2474ed",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "5af55978-5f56-4f40-a6da-3d0cb289b6b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "cab295f0-9031-47bf-9e39-74bf4b9f22ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5af55978-5f56-4f40-a6da-3d0cb289b6b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8382c5ac-eaa8-4382-a61b-5ee1102604d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5af55978-5f56-4f40-a6da-3d0cb289b6b7",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "d077a8ee-e706-42c6-91d0-862f8110fa3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "047da741-7228-48f5-ac8c-10bbd8b9a919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d077a8ee-e706-42c6-91d0-862f8110fa3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "910e118c-2f49-4015-a4a9-19412dd39e42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d077a8ee-e706-42c6-91d0-862f8110fa3b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "f6eac25f-1750-4add-b335-d020be3a82e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "524dbfd2-a228-4404-8e1a-4f9e173e6b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6eac25f-1750-4add-b335-d020be3a82e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f81de93-a56b-460c-b99f-9cd9659fddf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6eac25f-1750-4add-b335-d020be3a82e6",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "977370db-cc60-405f-8afa-2904d7e4ab84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "be1ba988-3173-4d9d-bb4c-8b782266d972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "977370db-cc60-405f-8afa-2904d7e4ab84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8c63a7-ddd1-49f8-889b-4edd497e0120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "977370db-cc60-405f-8afa-2904d7e4ab84",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "ca9be105-ee5d-4542-b7fc-d8bf095529c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "84f30d12-9df2-4d89-b9e2-21635330d62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca9be105-ee5d-4542-b7fc-d8bf095529c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "773d9904-5baa-4540-a4b8-cc4716f269e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca9be105-ee5d-4542-b7fc-d8bf095529c2",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "56a89ab3-2f2e-4d15-a4cf-1bfaebc0742b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "8093f0c4-4c09-45da-9283-891c3064ed6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56a89ab3-2f2e-4d15-a4cf-1bfaebc0742b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89874c02-7655-40f7-9553-db3aa1e4ad04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56a89ab3-2f2e-4d15-a4cf-1bfaebc0742b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "57d503fd-fa77-4167-a7cb-86df2da40544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "48df0f27-56e0-4da0-bb66-b8139efe2a7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57d503fd-fa77-4167-a7cb-86df2da40544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15521917-e2e6-472f-865c-0893532e5f87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d503fd-fa77-4167-a7cb-86df2da40544",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "ff92400b-8326-42c5-b127-cc3c0ce720ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "6485ab7c-eb88-472b-a33a-a92b14510f7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff92400b-8326-42c5-b127-cc3c0ce720ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00afb436-92e2-4803-a1af-b20ef8e1c0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff92400b-8326-42c5-b127-cc3c0ce720ff",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "c2d38823-7e75-4c7b-bff7-9b478c92d4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "44a25008-7d51-4e9f-a50c-098c04db87d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d38823-7e75-4c7b-bff7-9b478c92d4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b465f0-eaa0-40d5-bc19-01d544a8fb78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d38823-7e75-4c7b-bff7-9b478c92d4bc",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "62dd6a2b-c134-4be9-8fe0-679b13599d7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "7303c1a2-6b13-4b16-a702-f9407d152e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62dd6a2b-c134-4be9-8fe0-679b13599d7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3214d901-f3be-4d54-b450-4e0a3c005650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62dd6a2b-c134-4be9-8fe0-679b13599d7b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "5707b8bf-91df-4ead-80d7-95f5fd99857a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "a9c52fdc-60d7-4d4d-8946-380e19bf612f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5707b8bf-91df-4ead-80d7-95f5fd99857a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69ba1cd8-7dcb-4e7f-95b0-17c77e415619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5707b8bf-91df-4ead-80d7-95f5fd99857a",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "c7e4bb28-936e-4a72-a8ff-c369be6c392d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "05205823-9747-4ac7-ad18-9b298c035e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e4bb28-936e-4a72-a8ff-c369be6c392d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e57170-0538-4aaf-9838-82f9345f00d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e4bb28-936e-4a72-a8ff-c369be6c392d",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "f9d664d8-cc9e-4e3b-9f09-ba3edb1c2345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "1b4879ec-172b-4c47-90f3-fa103cbb78b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d664d8-cc9e-4e3b-9f09-ba3edb1c2345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02e8f28-3958-4b99-9f15-f0505dc9b786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d664d8-cc9e-4e3b-9f09-ba3edb1c2345",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "bdfa8d4b-97c8-4f8d-b337-0be0aa7b6810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "5548b3e3-3c72-49c3-ba79-5c86f40f7857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdfa8d4b-97c8-4f8d-b337-0be0aa7b6810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7292449-8970-4cda-8e7f-6b498c529aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdfa8d4b-97c8-4f8d-b337-0be0aa7b6810",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "80e8376d-5f46-4183-b296-a51cd7b8f6a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "695a497d-67d3-4039-835d-bd205f6adeec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80e8376d-5f46-4183-b296-a51cd7b8f6a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "679b3471-9dd4-44f5-930a-ba160d84c1b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80e8376d-5f46-4183-b296-a51cd7b8f6a5",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "2c7397e4-5fcf-4eee-be6a-990b352285b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "e26798ac-8369-432a-a979-5f3248dad1fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c7397e4-5fcf-4eee-be6a-990b352285b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5ea89b-2d21-4036-8bae-aa048887859e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c7397e4-5fcf-4eee-be6a-990b352285b1",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "2dd828c8-d009-4010-9859-705617efb0c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "f254af45-5a05-4d1d-ba71-aafb610de8b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd828c8-d009-4010-9859-705617efb0c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4937176-0de8-48cf-a914-26466aeb85ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd828c8-d009-4010-9859-705617efb0c5",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "0566c1a0-8aa2-43cb-8c54-66918fae6785",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "9a787e90-3fa6-428f-93b2-f4801789c7b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0566c1a0-8aa2-43cb-8c54-66918fae6785",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a97154e-bc49-452d-8c83-83d91ef56803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0566c1a0-8aa2-43cb-8c54-66918fae6785",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "57b4c9d5-e741-4ff4-9b54-d4d5962559c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "f42aa9b1-8c86-4341-8e6f-ad6eb4408a8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b4c9d5-e741-4ff4-9b54-d4d5962559c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ace381b7-de5f-41f3-bf08-52f6f1ba6b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b4c9d5-e741-4ff4-9b54-d4d5962559c3",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "05964629-aa2b-4190-8d94-df8f2aa13421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "5d53fcf9-ea9c-437b-b146-1c90c061eaed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05964629-aa2b-4190-8d94-df8f2aa13421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee632197-b270-4b8a-a7bb-96a3ab216e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05964629-aa2b-4190-8d94-df8f2aa13421",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "beac0993-81c1-4166-aa87-11aeebc50515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "9a4bfd34-2721-46c3-95ad-9b7a0e3dbb7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beac0993-81c1-4166-aa87-11aeebc50515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae87003-9d25-4af7-8ef5-2ee546e0920d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beac0993-81c1-4166-aa87-11aeebc50515",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "4197257e-a8e2-46e8-a806-616e730f6625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "d917c6c9-77a3-41f0-8ed8-6e6547a551a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4197257e-a8e2-46e8-a806-616e730f6625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d48005-bf82-4875-be33-f5954c5c5aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4197257e-a8e2-46e8-a806-616e730f6625",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "5cae956d-8ba1-4787-bdae-89884682529d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "b2fa7b28-138c-4ba6-91fe-4e8aea33f58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cae956d-8ba1-4787-bdae-89884682529d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c5cff7-ccf6-4beb-ad45-0667609f145d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cae956d-8ba1-4787-bdae-89884682529d",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "f3c60152-e52e-4e61-a2ce-dad4e6e6dd5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "7133db3e-7607-4705-9693-35103d5d5dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3c60152-e52e-4e61-a2ce-dad4e6e6dd5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba440373-5989-4fc6-9343-dd46d749b8de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3c60152-e52e-4e61-a2ce-dad4e6e6dd5c",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "72cf32d5-bcf1-4695-9ede-45dc407f170b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "587c9353-e8e7-412c-9273-d3f3beca8610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72cf32d5-bcf1-4695-9ede-45dc407f170b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "763756e0-f483-45f2-9730-24a6b3fb952b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72cf32d5-bcf1-4695-9ede-45dc407f170b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "c0e45976-63fa-4b45-9fe2-35d4e700b450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "14677312-5f8b-4c29-9c16-58954c32d549",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0e45976-63fa-4b45-9fe2-35d4e700b450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde2c1e3-0857-4d9d-994b-70bc18460563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0e45976-63fa-4b45-9fe2-35d4e700b450",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "5f6378b7-42a9-4b45-b81c-242562288fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "2fce067d-cd20-442e-bfc6-75f9b6aaf967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6378b7-42a9-4b45-b81c-242562288fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7e0979a-b39e-423f-a864-d34e36f8eba6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6378b7-42a9-4b45-b81c-242562288fc8",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "9c99f84a-5419-442e-b888-fa1d015355bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "2204f16c-a033-4bbf-84ec-c02f7e294fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c99f84a-5419-442e-b888-fa1d015355bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed2a0f6-95ad-49ae-a7ed-603b4de4e901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c99f84a-5419-442e-b888-fa1d015355bf",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "56993d56-1b73-4eea-882a-dfa125d0e951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "b20cc960-65c7-401a-8e0b-863ca44b7b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56993d56-1b73-4eea-882a-dfa125d0e951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3bd4e5d-4cc7-4d05-9aa1-8de6f509f491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56993d56-1b73-4eea-882a-dfa125d0e951",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "6c9fb59b-3f0c-44ad-8096-3c620b091607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "712617dd-d9d8-42a5-8c6c-317358a4818e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9fb59b-3f0c-44ad-8096-3c620b091607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb852636-770f-44a6-90d7-d034a6a4786c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9fb59b-3f0c-44ad-8096-3c620b091607",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "ca48455a-eccc-4f32-a133-d7a42f3d6e23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "422c0a31-5ddf-473c-a48e-e4eaaa62a317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca48455a-eccc-4f32-a133-d7a42f3d6e23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a7db9b2-dff0-4c72-983b-e81104497c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca48455a-eccc-4f32-a133-d7a42f3d6e23",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "a86994e8-6efd-4425-854a-cc87efcc45b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "38900478-0fa9-4286-b23e-54631d6c9f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a86994e8-6efd-4425-854a-cc87efcc45b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b512d0f-d324-435e-ae94-c26c10bfa822",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a86994e8-6efd-4425-854a-cc87efcc45b6",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "1f2278ac-37c7-45ee-ae7b-f247dca2314a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "dec15ddb-e909-42ab-8210-3cee883b9bd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2278ac-37c7-45ee-ae7b-f247dca2314a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73ea5e2e-1d12-4281-9459-8d5e7ace2857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2278ac-37c7-45ee-ae7b-f247dca2314a",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "aa6cb608-ce08-4b88-af2f-4da080a406e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "6e64bb53-f54c-4cb5-abd6-92687fc0176a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa6cb608-ce08-4b88-af2f-4da080a406e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb76c05-6337-4fcf-8636-3fbf76255f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa6cb608-ce08-4b88-af2f-4da080a406e4",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "af1b7ae7-6aad-44a2-bffd-1452c97e12ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "5def6a10-712b-4d5e-84f6-f92a0b7ce930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af1b7ae7-6aad-44a2-bffd-1452c97e12ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "723be552-06bf-4be3-bb3e-7cedee82e389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af1b7ae7-6aad-44a2-bffd-1452c97e12ea",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "090923ed-01d0-452a-89ef-78cfee3776ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "b211b096-b9c8-44d0-aa97-08adf5a1041e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "090923ed-01d0-452a-89ef-78cfee3776ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c3214d-9f4c-4540-bfb2-1a3b3ba43009",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "090923ed-01d0-452a-89ef-78cfee3776ae",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "b86cffde-dbad-4641-be6c-53c8d178830b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "d433211e-a811-4347-b2da-97ce55ea7806",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b86cffde-dbad-4641-be6c-53c8d178830b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e42b51-6390-4912-a70e-efc1591c8534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b86cffde-dbad-4641-be6c-53c8d178830b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "536ffcc9-57cc-4c0b-bb07-4044e44e338f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "c372de43-51df-494c-9f2e-02082a90621d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536ffcc9-57cc-4c0b-bb07-4044e44e338f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f7de3b-e475-465d-9af7-560cf757f75b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536ffcc9-57cc-4c0b-bb07-4044e44e338f",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "359609ab-8281-4323-a6e4-d1399c6358c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "dbd5eabe-dac2-423f-88aa-e9f2606060ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "359609ab-8281-4323-a6e4-d1399c6358c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e6e7ff8-a492-4ab0-b5af-612b26d1e0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "359609ab-8281-4323-a6e4-d1399c6358c7",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "5801cd5b-87de-4dce-beed-b75bb09503a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "b35ac759-4e0b-4bd8-9eef-9d5775537fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5801cd5b-87de-4dce-beed-b75bb09503a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b63357-dd54-42e7-b395-fec11fc4b743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5801cd5b-87de-4dce-beed-b75bb09503a8",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "38d44b98-9da7-47a2-98b0-2f7be270df98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "043ef258-9b71-4de6-ac53-431768ea4511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d44b98-9da7-47a2-98b0-2f7be270df98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7881adb-e7fb-4ca2-98fa-36861a382caa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d44b98-9da7-47a2-98b0-2f7be270df98",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "3071e9dd-cf2b-4308-8564-eda2c97afed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "aa4862e4-2a60-4ed7-848f-63efca2b7664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3071e9dd-cf2b-4308-8564-eda2c97afed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b74db255-0177-4414-bced-4f03deb21274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3071e9dd-cf2b-4308-8564-eda2c97afed3",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "1742a285-a65c-4316-95f8-3de41098242c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "e0b20b96-c01f-475f-a761-2ab7925a5d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1742a285-a65c-4316-95f8-3de41098242c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33267223-b73e-4f6a-8198-bdede3b40ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1742a285-a65c-4316-95f8-3de41098242c",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "6cde6bbc-7254-4c4b-ab90-2bb40a360cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "e1033606-ace3-40db-b37a-3e8035c51114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cde6bbc-7254-4c4b-ab90-2bb40a360cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35889646-397c-4045-ab0f-aa1d6c99ffd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cde6bbc-7254-4c4b-ab90-2bb40a360cf4",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "1b350f0b-3a2a-4bee-9775-42afa079418b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "d117cf77-1bf7-4e68-af2c-af4ee43f6dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b350f0b-3a2a-4bee-9775-42afa079418b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328d72ae-2015-4a63-ad3c-7c6f3bf4c3e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b350f0b-3a2a-4bee-9775-42afa079418b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "512d48bc-cf85-4611-bd0c-837196338e21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "a38919ee-01d7-49f5-b69c-dc6df7dd07f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "512d48bc-cf85-4611-bd0c-837196338e21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db9e89b6-10d8-4007-b08f-6adc76389269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512d48bc-cf85-4611-bd0c-837196338e21",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "94cc1223-3d13-4752-b967-9f400d9a472d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "abc94821-1d70-4c70-961a-35078bc1d4b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94cc1223-3d13-4752-b967-9f400d9a472d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "397eaaad-c06e-4722-bd74-b8406e75f2d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94cc1223-3d13-4752-b967-9f400d9a472d",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "dfdbec5a-68c2-4dd4-841c-1ba2dc9dff2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "e1077d62-e903-40da-b344-48f983bc7de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfdbec5a-68c2-4dd4-841c-1ba2dc9dff2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e6a3af-3be0-4f31-94bc-cd2a3774b8a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfdbec5a-68c2-4dd4-841c-1ba2dc9dff2c",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "294b115a-8777-4aa9-adc1-4801a3fbc9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "0312e28f-a52e-4d90-a47d-a3926d8bf38a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "294b115a-8777-4aa9-adc1-4801a3fbc9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0197e9-bae8-4579-9b51-6dea2802f942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "294b115a-8777-4aa9-adc1-4801a3fbc9ea",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "2d67f6cb-a15d-4bd1-a3d7-97f512724aaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "d5d673da-ea65-4c99-a9ac-cc95b86ab103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d67f6cb-a15d-4bd1-a3d7-97f512724aaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c0374e-a361-419e-b3ec-3d8f1d2aa741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d67f6cb-a15d-4bd1-a3d7-97f512724aaf",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "b39eb7f5-701b-48e8-9308-caf73085bc7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "954926c5-9211-4abe-b24f-ee3f78c9972f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b39eb7f5-701b-48e8-9308-caf73085bc7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5318bff1-0236-4ce2-8748-7f8faefbd64f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b39eb7f5-701b-48e8-9308-caf73085bc7b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "f55d496d-cf0b-472b-8eff-c0dba15b9818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "97636ee4-b042-4087-815d-52b787e9f4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55d496d-cf0b-472b-8eff-c0dba15b9818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c559e74-bf94-4334-86f5-f22c820a82d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55d496d-cf0b-472b-8eff-c0dba15b9818",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "dec81880-a165-4fb2-95bb-7645006ad71b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "776742f7-ff79-4284-ba75-adc10b610cd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec81880-a165-4fb2-95bb-7645006ad71b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0815a07-ffa7-405b-9f79-14f61ab226d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec81880-a165-4fb2-95bb-7645006ad71b",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "e0ba5005-ff5d-4a60-b974-9a293d18fa77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "53af6672-8e78-4ebf-b0f7-d6f122a2981a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ba5005-ff5d-4a60-b974-9a293d18fa77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c03277be-e4ab-4902-a7fb-7cb0bc07a5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ba5005-ff5d-4a60-b974-9a293d18fa77",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "2dea1c54-8563-43e1-9c12-a716813a8b08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "6c00c4f0-8573-445c-b4c0-a773b6359244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dea1c54-8563-43e1-9c12-a716813a8b08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22845de-9e91-4f1c-94bb-181f45ade6a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dea1c54-8563-43e1-9c12-a716813a8b08",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "2ecb7ec4-d575-4b3a-92d6-7f15ee5825ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "77d3cc79-d638-4db4-93af-0b537180b31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ecb7ec4-d575-4b3a-92d6-7f15ee5825ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18628b32-a589-40e8-874f-95f182d7af68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ecb7ec4-d575-4b3a-92d6-7f15ee5825ac",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "b72ecbac-f236-4aa4-8fd9-80e21dcc3099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "08b18c8f-78bc-4571-9bf6-ca034903b34c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72ecbac-f236-4aa4-8fd9-80e21dcc3099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfd262c-161b-44a0-a4cf-75ca10c9d3b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72ecbac-f236-4aa4-8fd9-80e21dcc3099",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        },
        {
            "id": "bfb0f2a0-c427-4244-9c64-1ebc4482a82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "compositeImage": {
                "id": "bdfb9615-85aa-4ba1-83fa-6105c3687b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfb0f2a0-c427-4244-9c64-1ebc4482a82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e850295-33d4-4677-96d7-eb1b7cd47ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfb0f2a0-c427-4244-9c64-1ebc4482a82d",
                    "LayerId": "527f9430-5805-4946-969e-c66900e82243"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "527f9430-5805-4946-969e-c66900e82243",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8215f7fd-1517-4dd8-9ba5-742e99bc724f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}