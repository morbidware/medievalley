{
    "id": "e0c71049-45b5-495f-8b08-4e1dafdf5989",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b7787f5-7202-4343-8319-db8071090509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0c71049-45b5-495f-8b08-4e1dafdf5989",
            "compositeImage": {
                "id": "456f9dad-898b-4520-b792-7a936e0980cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7787f5-7202-4343-8319-db8071090509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb55705-3c71-4910-9877-cb686dea6cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7787f5-7202-4343-8319-db8071090509",
                    "LayerId": "8052542f-a400-48b7-9f79-e6b58724b956"
                }
            ]
        },
        {
            "id": "dc843143-f423-4c02-98f9-36849eab209b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0c71049-45b5-495f-8b08-4e1dafdf5989",
            "compositeImage": {
                "id": "bddc59f9-6003-4971-9da2-a22e6ad4c328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc843143-f423-4c02-98f9-36849eab209b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a644ca-d344-47d5-8fbd-3f2ccf04a4de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc843143-f423-4c02-98f9-36849eab209b",
                    "LayerId": "8052542f-a400-48b7-9f79-e6b58724b956"
                }
            ]
        },
        {
            "id": "517eebd7-e0b3-449b-aae3-7fc8a1b40514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0c71049-45b5-495f-8b08-4e1dafdf5989",
            "compositeImage": {
                "id": "e3b8a438-b89c-4857-abc1-fb3230427d18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517eebd7-e0b3-449b-aae3-7fc8a1b40514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f94c9c-01f8-47ee-975e-8a926adef3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517eebd7-e0b3-449b-aae3-7fc8a1b40514",
                    "LayerId": "8052542f-a400-48b7-9f79-e6b58724b956"
                }
            ]
        },
        {
            "id": "54c90759-bd5b-4b20-aade-fc28b52644af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0c71049-45b5-495f-8b08-4e1dafdf5989",
            "compositeImage": {
                "id": "f887cf61-7fcb-45dd-89eb-f32bb2acae76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54c90759-bd5b-4b20-aade-fc28b52644af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef18013-752a-46e4-949f-ee18de2072db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54c90759-bd5b-4b20-aade-fc28b52644af",
                    "LayerId": "8052542f-a400-48b7-9f79-e6b58724b956"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8052542f-a400-48b7-9f79-e6b58724b956",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0c71049-45b5-495f-8b08-4e1dafdf5989",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}