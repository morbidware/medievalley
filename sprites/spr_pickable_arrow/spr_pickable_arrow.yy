{
    "id": "a4254a5b-deba-49db-95e1-77bcb722dcac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99dab13a-878a-4e3f-8997-176454d3c438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4254a5b-deba-49db-95e1-77bcb722dcac",
            "compositeImage": {
                "id": "d9506390-17b8-445b-b3e8-fa83940a43b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99dab13a-878a-4e3f-8997-176454d3c438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45c01bc2-3fbc-4e57-be1d-784041aea3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99dab13a-878a-4e3f-8997-176454d3c438",
                    "LayerId": "ae8830d1-e7eb-4613-8551-c9ac05617651"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ae8830d1-e7eb-4613-8551-c9ac05617651",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4254a5b-deba-49db-95e1-77bcb722dcac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}