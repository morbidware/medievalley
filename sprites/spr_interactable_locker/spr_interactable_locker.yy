{
    "id": "fce279aa-51cb-4b8d-8ee3-61fd625a601d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_locker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c2ab17b-97a5-42bb-bd91-30ddd79f74f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fce279aa-51cb-4b8d-8ee3-61fd625a601d",
            "compositeImage": {
                "id": "26825342-0a99-4176-a78e-f37bc40bf0cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2ab17b-97a5-42bb-bd91-30ddd79f74f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63961c13-ccc4-491d-86b7-ef4442d9228f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2ab17b-97a5-42bb-bd91-30ddd79f74f4",
                    "LayerId": "6877d87f-af15-45a8-8fe3-0b1a28170db4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6877d87f-af15-45a8-8fe3-0b1a28170db4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fce279aa-51cb-4b8d-8ee3-61fd625a601d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}