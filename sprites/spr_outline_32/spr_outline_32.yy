{
    "id": "d48fec4d-3783-4c62-aa7c-14cf28776321",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_outline_32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a4c8bcb-2a2d-4d6a-9d42-676b7b8b9639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d48fec4d-3783-4c62-aa7c-14cf28776321",
            "compositeImage": {
                "id": "f5c46cf8-c6e3-474c-b430-eaaa795d8bc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4c8bcb-2a2d-4d6a-9d42-676b7b8b9639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf64d68a-5204-4094-bd48-55c842505d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4c8bcb-2a2d-4d6a-9d42-676b7b8b9639",
                    "LayerId": "8c0a6038-f5c7-49bb-87e9-31686ba42583"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8c0a6038-f5c7-49bb-87e9-31686ba42583",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d48fec4d-3783-4c62-aa7c-14cf28776321",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}