{
    "id": "d078ff67-bb50-492d-af9e-6bff8887fce0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_upgradeinfo_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 0,
    "bbox_right": 173,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f747a5bc-ed24-4552-8dd1-2ea1859a9c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d078ff67-bb50-492d-af9e-6bff8887fce0",
            "compositeImage": {
                "id": "a31a460c-4b8b-47d3-90fd-8e1b13306168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f747a5bc-ed24-4552-8dd1-2ea1859a9c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79473191-31ae-42ad-8497-f5dd5b6e81da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f747a5bc-ed24-4552-8dd1-2ea1859a9c83",
                    "LayerId": "7573e077-19d7-4aca-a3d1-9acc18d77c01"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 103,
    "height": 103,
    "layers": [
        {
            "id": "7573e077-19d7-4aca-a3d1-9acc18d77c01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d078ff67-bb50-492d-af9e-6bff8887fce0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 174,
    "xorig": 0,
    "yorig": 0
}