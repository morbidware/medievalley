{
    "id": "3ffdc39b-e343-480a-97ed-47c50541eb50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca87c271-30b9-40c7-95b6-c37f564f01b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ffdc39b-e343-480a-97ed-47c50541eb50",
            "compositeImage": {
                "id": "e02b652a-dd49-43d8-9330-b0247b25731c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca87c271-30b9-40c7-95b6-c37f564f01b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20826c3-070b-4c8d-bce3-b56e4a5caeb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca87c271-30b9-40c7-95b6-c37f564f01b8",
                    "LayerId": "64825f3b-5fb0-4dc0-a2db-4ee85cb2ff9b"
                }
            ]
        },
        {
            "id": "b772ca5d-b6e4-467e-b176-6b39a4d6bb54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ffdc39b-e343-480a-97ed-47c50541eb50",
            "compositeImage": {
                "id": "0f9130e4-ca28-42dd-860c-6392aa4a8b5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b772ca5d-b6e4-467e-b176-6b39a4d6bb54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cad13b5b-d524-49d2-8657-e495e67956d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b772ca5d-b6e4-467e-b176-6b39a4d6bb54",
                    "LayerId": "64825f3b-5fb0-4dc0-a2db-4ee85cb2ff9b"
                }
            ]
        },
        {
            "id": "148036d5-a893-4924-bd8d-b15b43084c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ffdc39b-e343-480a-97ed-47c50541eb50",
            "compositeImage": {
                "id": "3158cbe1-88d8-45ea-a0f5-db1cb1ca5628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148036d5-a893-4924-bd8d-b15b43084c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "670caee7-88c1-4571-b564-23e6965a23da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148036d5-a893-4924-bd8d-b15b43084c04",
                    "LayerId": "64825f3b-5fb0-4dc0-a2db-4ee85cb2ff9b"
                }
            ]
        },
        {
            "id": "b5ee4f59-a5df-4833-b417-18bd91dbe3b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ffdc39b-e343-480a-97ed-47c50541eb50",
            "compositeImage": {
                "id": "1d12c134-1e3f-4ece-b30c-1bef2acd54cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5ee4f59-a5df-4833-b417-18bd91dbe3b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b30123-581f-4f79-80de-0c136a2c474e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5ee4f59-a5df-4833-b417-18bd91dbe3b0",
                    "LayerId": "64825f3b-5fb0-4dc0-a2db-4ee85cb2ff9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "64825f3b-5fb0-4dc0-a2db-4ee85cb2ff9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ffdc39b-e343-480a-97ed-47c50541eb50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}