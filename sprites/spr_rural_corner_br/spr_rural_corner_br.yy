{
    "id": "0d4a6fc9-5717-4567-9796-8497165fab77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_corner_br",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e3cc529-a4ff-48b7-9c8e-a1106477b637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4a6fc9-5717-4567-9796-8497165fab77",
            "compositeImage": {
                "id": "5a3b071c-a4c7-4eda-aeda-a23e4df76f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e3cc529-a4ff-48b7-9c8e-a1106477b637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51ab1dd5-60b3-4907-b827-740bd26875b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e3cc529-a4ff-48b7-9c8e-a1106477b637",
                    "LayerId": "428d8bb4-369e-4562-b843-7727fdd1729b"
                }
            ]
        },
        {
            "id": "23debafc-0baf-417b-bf6a-5f027820b080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4a6fc9-5717-4567-9796-8497165fab77",
            "compositeImage": {
                "id": "8caf1f73-037d-499e-909d-1ab7d35a0f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23debafc-0baf-417b-bf6a-5f027820b080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7902958b-0611-4e84-aba4-0e4671569c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23debafc-0baf-417b-bf6a-5f027820b080",
                    "LayerId": "428d8bb4-369e-4562-b843-7727fdd1729b"
                }
            ]
        },
        {
            "id": "e02ed973-bac3-40b8-9757-c491718b922b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4a6fc9-5717-4567-9796-8497165fab77",
            "compositeImage": {
                "id": "d3be38e7-ffad-4e27-b954-aef9390c409d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e02ed973-bac3-40b8-9757-c491718b922b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6facb5a2-b4c3-4f96-ad83-0832cfb652a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e02ed973-bac3-40b8-9757-c491718b922b",
                    "LayerId": "428d8bb4-369e-4562-b843-7727fdd1729b"
                }
            ]
        },
        {
            "id": "427ebe1d-b92c-4178-ab14-f3d417ef298b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4a6fc9-5717-4567-9796-8497165fab77",
            "compositeImage": {
                "id": "865817b6-fe8b-466b-a45c-1c4a7631bed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427ebe1d-b92c-4178-ab14-f3d417ef298b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6132c995-588c-4fca-839a-3c9cd939ec57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427ebe1d-b92c-4178-ab14-f3d417ef298b",
                    "LayerId": "428d8bb4-369e-4562-b843-7727fdd1729b"
                }
            ]
        },
        {
            "id": "3118728f-9202-45e8-aeb6-68392b99bf59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d4a6fc9-5717-4567-9796-8497165fab77",
            "compositeImage": {
                "id": "4b608d73-03ce-4e9f-9770-8c957957d0e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3118728f-9202-45e8-aeb6-68392b99bf59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53eaac5a-fd83-4ab9-bcf1-be73556ffc52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3118728f-9202-45e8-aeb6-68392b99bf59",
                    "LayerId": "428d8bb4-369e-4562-b843-7727fdd1729b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "428d8bb4-369e-4562-b843-7727fdd1729b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d4a6fc9-5717-4567-9796-8497165fab77",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}