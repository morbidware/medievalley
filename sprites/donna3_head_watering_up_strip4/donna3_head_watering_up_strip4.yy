{
    "id": "d49d5b2b-09af-43e2-adac-41d4c5a433bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c856afbd-bfe4-48b3-b171-d3cc88247f73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49d5b2b-09af-43e2-adac-41d4c5a433bc",
            "compositeImage": {
                "id": "39e7a70c-52ac-4a61-b440-1555ec56a24b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c856afbd-bfe4-48b3-b171-d3cc88247f73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ec1e37-210f-44fa-aeff-e088f1398b98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c856afbd-bfe4-48b3-b171-d3cc88247f73",
                    "LayerId": "de9e7d7a-f305-43db-be70-60380d0c8975"
                }
            ]
        },
        {
            "id": "93172438-28d7-40f1-9937-06659ad6d64a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49d5b2b-09af-43e2-adac-41d4c5a433bc",
            "compositeImage": {
                "id": "da6a1e14-bed4-4c82-8d2f-7dba0f9570a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93172438-28d7-40f1-9937-06659ad6d64a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e9b39c-9456-4e5c-b557-7fd15c100f97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93172438-28d7-40f1-9937-06659ad6d64a",
                    "LayerId": "de9e7d7a-f305-43db-be70-60380d0c8975"
                }
            ]
        },
        {
            "id": "00fefaf6-54cb-410d-88f4-2936081b3818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49d5b2b-09af-43e2-adac-41d4c5a433bc",
            "compositeImage": {
                "id": "6a196965-d989-4c79-b8ab-dfcbaebc1e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00fefaf6-54cb-410d-88f4-2936081b3818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e458c7ef-c8c7-40fd-9fee-58b9dc76954a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00fefaf6-54cb-410d-88f4-2936081b3818",
                    "LayerId": "de9e7d7a-f305-43db-be70-60380d0c8975"
                }
            ]
        },
        {
            "id": "12c1bccc-f29b-4df4-9ae4-c928eb95dba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d49d5b2b-09af-43e2-adac-41d4c5a433bc",
            "compositeImage": {
                "id": "bfdedd59-0024-4a75-ba37-e8698704ee58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c1bccc-f29b-4df4-9ae4-c928eb95dba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dccf3a9-0aad-4687-8790-a7a46bdce16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c1bccc-f29b-4df4-9ae4-c928eb95dba9",
                    "LayerId": "de9e7d7a-f305-43db-be70-60380d0c8975"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "de9e7d7a-f305-43db-be70-60380d0c8975",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d49d5b2b-09af-43e2-adac-41d4c5a433bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}