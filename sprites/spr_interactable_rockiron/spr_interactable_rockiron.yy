{
    "id": "26288e2c-607e-461d-ba58-206b72881e2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_rockiron",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b136903-c52c-4dce-b273-bd31da65a15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26288e2c-607e-461d-ba58-206b72881e2f",
            "compositeImage": {
                "id": "3c76fd47-18ac-481c-aaa8-9fbd5dc0af27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b136903-c52c-4dce-b273-bd31da65a15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a11c42-8494-44ad-be9d-a0a0638de4b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b136903-c52c-4dce-b273-bd31da65a15b",
                    "LayerId": "2b2725c7-90ed-429a-a481-715a3e3d9b46"
                }
            ]
        },
        {
            "id": "14fd2736-27dd-4418-a9dc-af49c99c0bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26288e2c-607e-461d-ba58-206b72881e2f",
            "compositeImage": {
                "id": "a9c5bf66-3808-4de5-bacd-0f8d90f337a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fd2736-27dd-4418-a9dc-af49c99c0bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4684fd-4342-490c-8aa1-0cebe3ab6030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fd2736-27dd-4418-a9dc-af49c99c0bdb",
                    "LayerId": "2b2725c7-90ed-429a-a481-715a3e3d9b46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2b2725c7-90ed-429a-a481-715a3e3d9b46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26288e2c-607e-461d-ba58-206b72881e2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 26
}