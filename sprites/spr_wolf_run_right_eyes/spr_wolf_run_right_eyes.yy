{
    "id": "58631d33-3998-443a-8f5f-2b2691a24e19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_run_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 40,
    "bbox_right": 41,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea208e68-e677-44f8-a6e6-78fa4b26e6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "compositeImage": {
                "id": "efe410ce-4b47-43b5-b78a-8fad22a3eb74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea208e68-e677-44f8-a6e6-78fa4b26e6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62744255-e7a6-4724-bfc8-f52feacb1d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea208e68-e677-44f8-a6e6-78fa4b26e6a1",
                    "LayerId": "9325c732-bfca-434c-9ef1-e13a633f05ce"
                }
            ]
        },
        {
            "id": "c37e9841-dbf7-41cd-be9b-417eba7268a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "compositeImage": {
                "id": "5125c7e5-c8b4-4fa5-b4ca-1978a6e0005c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37e9841-dbf7-41cd-be9b-417eba7268a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507e9521-89e1-4ada-86a6-886eb8cc518b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37e9841-dbf7-41cd-be9b-417eba7268a9",
                    "LayerId": "9325c732-bfca-434c-9ef1-e13a633f05ce"
                }
            ]
        },
        {
            "id": "c6399c27-e7ca-4a20-8486-3f7651e5e776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "compositeImage": {
                "id": "2614a49a-bf82-40ce-bf86-32afa3e1ac6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6399c27-e7ca-4a20-8486-3f7651e5e776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454226cc-4f3b-4243-ad35-36bc3a3e87e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6399c27-e7ca-4a20-8486-3f7651e5e776",
                    "LayerId": "9325c732-bfca-434c-9ef1-e13a633f05ce"
                }
            ]
        },
        {
            "id": "0396cf50-d5a7-4c3c-9bff-c73c4f1f1fda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "compositeImage": {
                "id": "4c675f21-47cf-41ab-a430-951733d23fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0396cf50-d5a7-4c3c-9bff-c73c4f1f1fda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c1e7368-c0a8-4ca8-88ee-d79fe03ea6a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0396cf50-d5a7-4c3c-9bff-c73c4f1f1fda",
                    "LayerId": "9325c732-bfca-434c-9ef1-e13a633f05ce"
                }
            ]
        },
        {
            "id": "689cbd29-0132-4471-903c-59f6d7c63f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "compositeImage": {
                "id": "4213ebd6-49e1-4d92-b037-da3fa5d66350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "689cbd29-0132-4471-903c-59f6d7c63f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83f17262-cb34-4e28-9758-1ac22f8b8ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "689cbd29-0132-4471-903c-59f6d7c63f57",
                    "LayerId": "9325c732-bfca-434c-9ef1-e13a633f05ce"
                }
            ]
        },
        {
            "id": "3a2612f0-375c-453d-9a22-0669c9b6de97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "compositeImage": {
                "id": "126688d5-4800-4b4a-a4de-010d857deade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2612f0-375c-453d-9a22-0669c9b6de97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36634da7-50c1-4da7-963b-b234b2352e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2612f0-375c-453d-9a22-0669c9b6de97",
                    "LayerId": "9325c732-bfca-434c-9ef1-e13a633f05ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "9325c732-bfca-434c-9ef1-e13a633f05ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58631d33-3998-443a-8f5f-2b2691a24e19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}