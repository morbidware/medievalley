{
    "id": "e7bff30e-d0bb-4bd6-8a82-0ccc3dc8b238",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipeinfo_pointer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "386fcbf9-d25b-4140-b3cf-604d76abc303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7bff30e-d0bb-4bd6-8a82-0ccc3dc8b238",
            "compositeImage": {
                "id": "fca468ab-9225-49c0-9dfb-9c24fee1bda2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386fcbf9-d25b-4140-b3cf-604d76abc303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b604bc-964c-43a7-b85d-9540c283f212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386fcbf9-d25b-4140-b3cf-604d76abc303",
                    "LayerId": "c821021b-6fde-4264-813e-56503ce4f6cf"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 8,
    "height": 16,
    "layers": [
        {
            "id": "c821021b-6fde-4264-813e-56503ce4f6cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7bff30e-d0bb-4bd6-8a82-0ccc3dc8b238",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 5,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 15,
    "yorig": 8
}