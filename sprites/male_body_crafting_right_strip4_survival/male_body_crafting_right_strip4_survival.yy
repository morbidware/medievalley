{
    "id": "3bfac6db-cc6b-45c0-989b-2533112c030c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_crafting_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bcbe67b-8559-4259-9a3d-49985ebb6d00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "compositeImage": {
                "id": "667017b8-2137-4065-92e7-a34020c9fbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bcbe67b-8559-4259-9a3d-49985ebb6d00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9fe2ad6-3146-49fc-aec8-86e6ae1a38d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bcbe67b-8559-4259-9a3d-49985ebb6d00",
                    "LayerId": "302b68b5-0ec1-4dc5-92b0-e3860900be8e"
                }
            ]
        },
        {
            "id": "d65c6bc4-6cfe-4571-acfc-66fe70547ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "compositeImage": {
                "id": "f44cd2af-51d7-4248-8b20-498b447f7679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d65c6bc4-6cfe-4571-acfc-66fe70547ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7120e071-ded4-4d5a-a507-8fad7e7aebd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d65c6bc4-6cfe-4571-acfc-66fe70547ee2",
                    "LayerId": "302b68b5-0ec1-4dc5-92b0-e3860900be8e"
                }
            ]
        },
        {
            "id": "587f563e-e6d9-4423-a06b-a01454b2e521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "compositeImage": {
                "id": "bc012d2e-60c7-40d7-9422-4fb251a1a8df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "587f563e-e6d9-4423-a06b-a01454b2e521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4a6a0e-db84-43ba-af2d-eaf37d458c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "587f563e-e6d9-4423-a06b-a01454b2e521",
                    "LayerId": "302b68b5-0ec1-4dc5-92b0-e3860900be8e"
                }
            ]
        },
        {
            "id": "15c0f829-29c5-45af-b72b-d186ae10d862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "compositeImage": {
                "id": "b0bf7d46-a2c0-4cef-903c-10a5e257de36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c0f829-29c5-45af-b72b-d186ae10d862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10159bf5-a322-47e2-8c4a-f43f3ce87281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c0f829-29c5-45af-b72b-d186ae10d862",
                    "LayerId": "302b68b5-0ec1-4dc5-92b0-e3860900be8e"
                }
            ]
        },
        {
            "id": "554c1186-e89c-46e8-bb93-dcaeab086250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "compositeImage": {
                "id": "d3b18815-1027-4bc6-a5c6-a140565ec205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554c1186-e89c-46e8-bb93-dcaeab086250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9365f425-d698-4556-b09b-981b56d53f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554c1186-e89c-46e8-bb93-dcaeab086250",
                    "LayerId": "302b68b5-0ec1-4dc5-92b0-e3860900be8e"
                }
            ]
        },
        {
            "id": "bf36cc84-d4ae-46f9-9d02-89970421543c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "compositeImage": {
                "id": "3508d598-a9be-4550-a0ab-cce9f5acf520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf36cc84-d4ae-46f9-9d02-89970421543c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8238cac-cfb1-45d4-9956-4297bf39ae8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf36cc84-d4ae-46f9-9d02-89970421543c",
                    "LayerId": "302b68b5-0ec1-4dc5-92b0-e3860900be8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "302b68b5-0ec1-4dc5-92b0-e3860900be8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bfac6db-cc6b-45c0-989b-2533112c030c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}