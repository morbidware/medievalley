{
    "id": "030dd901-d47f-4b34-ad90-263acbba7451",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potion_defence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3ec0228-01b3-4c9b-af5e-8c6557a839a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "030dd901-d47f-4b34-ad90-263acbba7451",
            "compositeImage": {
                "id": "fa1579dd-bc6c-4f8b-84b3-7c4ff2d511d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ec0228-01b3-4c9b-af5e-8c6557a839a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54f82da-3687-48b4-94d3-28d5c5867eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ec0228-01b3-4c9b-af5e-8c6557a839a5",
                    "LayerId": "cb010bd2-5d61-40d7-acdd-4f6d7e6619e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cb010bd2-5d61-40d7-acdd-4f6d7e6619e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "030dd901-d47f-4b34-ad90-263acbba7451",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}