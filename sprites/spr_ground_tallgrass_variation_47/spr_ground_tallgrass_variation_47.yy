{
    "id": "5a20437d-de3c-4ef9-b5c8-646c7ef68432",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_tallgrass_variation_47",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ff27a74-9c67-407b-b4fd-4820f59119d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a20437d-de3c-4ef9-b5c8-646c7ef68432",
            "compositeImage": {
                "id": "79e57126-2e86-4d2c-b1f8-134043ebf872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff27a74-9c67-407b-b4fd-4820f59119d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78fc242f-9879-4641-b426-810e90b1fd1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff27a74-9c67-407b-b4fd-4820f59119d6",
                    "LayerId": "59c7facc-0fa4-4f93-b7f2-73ccba301ff4"
                }
            ]
        },
        {
            "id": "1c128382-f13e-472d-8f4a-6c395a939d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a20437d-de3c-4ef9-b5c8-646c7ef68432",
            "compositeImage": {
                "id": "2454e83a-c083-47f0-a975-b444087ebc8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c128382-f13e-472d-8f4a-6c395a939d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98629ab6-9a8f-4580-9180-c9146c74b139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c128382-f13e-472d-8f4a-6c395a939d28",
                    "LayerId": "59c7facc-0fa4-4f93-b7f2-73ccba301ff4"
                }
            ]
        },
        {
            "id": "fa63c09b-b3e7-47a4-8609-aeb3e50fc6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a20437d-de3c-4ef9-b5c8-646c7ef68432",
            "compositeImage": {
                "id": "2e2b0a80-aef5-4a8a-84e6-0bce6673d7f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa63c09b-b3e7-47a4-8609-aeb3e50fc6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c30eb4-35c7-43f1-a8d0-7887cb43d404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa63c09b-b3e7-47a4-8609-aeb3e50fc6ed",
                    "LayerId": "59c7facc-0fa4-4f93-b7f2-73ccba301ff4"
                }
            ]
        },
        {
            "id": "ed418ef4-6ed9-4762-8b87-775226fbbb63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a20437d-de3c-4ef9-b5c8-646c7ef68432",
            "compositeImage": {
                "id": "ed719942-e1af-48ad-8914-695110b83dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed418ef4-6ed9-4762-8b87-775226fbbb63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eae40a4-60e8-44d3-805f-6a3d465c2c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed418ef4-6ed9-4762-8b87-775226fbbb63",
                    "LayerId": "59c7facc-0fa4-4f93-b7f2-73ccba301ff4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "59c7facc-0fa4-4f93-b7f2-73ccba301ff4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a20437d-de3c-4ef9-b5c8-646c7ef68432",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}