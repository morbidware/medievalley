{
    "id": "1b01b4f6-4c39-449f-90ad-50760150568e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipe_alchemytable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06329419-5710-47b8-aef4-aa925439af05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b01b4f6-4c39-449f-90ad-50760150568e",
            "compositeImage": {
                "id": "36d1cca8-f6dc-4dae-9c8a-24262d2ff03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06329419-5710-47b8-aef4-aa925439af05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f611c1c2-b90b-4775-bfd9-b832e11eb055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06329419-5710-47b8-aef4-aa925439af05",
                    "LayerId": "9a62fd9f-e804-4a64-b97b-650fb42d0ab7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "9a62fd9f-e804-4a64-b97b-650fb42d0ab7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b01b4f6-4c39-449f-90ad-50760150568e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}