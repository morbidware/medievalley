{
    "id": "735c67d4-92cd-431a-8423-5e66b28c8b21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_idle_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c01f0a56-9d8b-47e2-a6bd-774194c59e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "735c67d4-92cd-431a-8423-5e66b28c8b21",
            "compositeImage": {
                "id": "5e156230-1eb1-4e6a-82ab-3c98bbb883d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01f0a56-9d8b-47e2-a6bd-774194c59e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf670d47-713d-40e8-b42b-edc491e71dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01f0a56-9d8b-47e2-a6bd-774194c59e0f",
                    "LayerId": "e6f8f9b0-0788-499d-b4d8-371e874a420b"
                }
            ]
        },
        {
            "id": "5a1e097d-8b48-4d0d-868c-e66dd094b075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "735c67d4-92cd-431a-8423-5e66b28c8b21",
            "compositeImage": {
                "id": "f044be1f-27ee-4c81-882c-bcea46af479b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a1e097d-8b48-4d0d-868c-e66dd094b075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd175a1-9076-4e83-9f0f-5be532ced850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a1e097d-8b48-4d0d-868c-e66dd094b075",
                    "LayerId": "e6f8f9b0-0788-499d-b4d8-371e874a420b"
                }
            ]
        },
        {
            "id": "cb7dbc0a-47ce-48b5-b20d-93ea26ee18a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "735c67d4-92cd-431a-8423-5e66b28c8b21",
            "compositeImage": {
                "id": "efae6817-910f-4a5a-ba95-b8157d350be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7dbc0a-47ce-48b5-b20d-93ea26ee18a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b67514-fe3b-409a-8093-515a3faa9637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7dbc0a-47ce-48b5-b20d-93ea26ee18a4",
                    "LayerId": "e6f8f9b0-0788-499d-b4d8-371e874a420b"
                }
            ]
        },
        {
            "id": "5d02f81c-fa92-47af-91c9-fd4a6f232ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "735c67d4-92cd-431a-8423-5e66b28c8b21",
            "compositeImage": {
                "id": "59a71fa9-3286-4224-91ba-e3037cd43f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d02f81c-fa92-47af-91c9-fd4a6f232ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e1c7b6-ac4b-4e4c-8483-173537489b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d02f81c-fa92-47af-91c9-fd4a6f232ed0",
                    "LayerId": "e6f8f9b0-0788-499d-b4d8-371e874a420b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e6f8f9b0-0788-499d-b4d8-371e874a420b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "735c67d4-92cd-431a-8423-5e66b28c8b21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}