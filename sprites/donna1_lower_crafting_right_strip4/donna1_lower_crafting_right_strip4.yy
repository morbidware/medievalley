{
    "id": "6d8b73e9-4c69-4dbe-81b0-21c349b1e5aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33a63d30-f7fa-4f70-9920-d004c372ecd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8b73e9-4c69-4dbe-81b0-21c349b1e5aa",
            "compositeImage": {
                "id": "f3490e5c-a170-4cb6-97ea-1df14ba4dfa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a63d30-f7fa-4f70-9920-d004c372ecd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7fc9219-0681-46c2-a5c6-b3f82fd9cd47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a63d30-f7fa-4f70-9920-d004c372ecd7",
                    "LayerId": "8ce6126d-da0c-427c-a00f-97febe0dc72b"
                }
            ]
        },
        {
            "id": "f9483f7e-d1cd-4648-85c7-e4ed44eab635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8b73e9-4c69-4dbe-81b0-21c349b1e5aa",
            "compositeImage": {
                "id": "8fb2d639-8acf-442a-b1b6-aac95b7703f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9483f7e-d1cd-4648-85c7-e4ed44eab635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e3e5b8d-b6ab-4dc2-ae4a-3b1197a263a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9483f7e-d1cd-4648-85c7-e4ed44eab635",
                    "LayerId": "8ce6126d-da0c-427c-a00f-97febe0dc72b"
                }
            ]
        },
        {
            "id": "7da888c3-2c17-4966-92b0-e016e772c62f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8b73e9-4c69-4dbe-81b0-21c349b1e5aa",
            "compositeImage": {
                "id": "b5abedad-ee8f-4cc3-b25e-e68633f77c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da888c3-2c17-4966-92b0-e016e772c62f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db96518d-fef3-47b5-9ea6-5e00da4c2bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da888c3-2c17-4966-92b0-e016e772c62f",
                    "LayerId": "8ce6126d-da0c-427c-a00f-97febe0dc72b"
                }
            ]
        },
        {
            "id": "83ecd164-5fcc-4779-9fbd-f4d24d30ff49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8b73e9-4c69-4dbe-81b0-21c349b1e5aa",
            "compositeImage": {
                "id": "2010df18-1394-42b0-b959-ea43c78bb653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ecd164-5fcc-4779-9fbd-f4d24d30ff49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "440ad443-5e07-4936-88d4-96f9c3607b4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ecd164-5fcc-4779-9fbd-f4d24d30ff49",
                    "LayerId": "8ce6126d-da0c-427c-a00f-97febe0dc72b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8ce6126d-da0c-427c-a00f-97febe0dc72b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d8b73e9-4c69-4dbe-81b0-21c349b1e5aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}