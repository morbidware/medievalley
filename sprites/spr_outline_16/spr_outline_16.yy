{
    "id": "1ec3a684-2455-4871-afe3-5c5264878ca6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_outline_16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3052a8f9-60e0-4694-883d-588665a2ddbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ec3a684-2455-4871-afe3-5c5264878ca6",
            "compositeImage": {
                "id": "62a18444-1afb-433e-90b6-13d8a0ec7090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3052a8f9-60e0-4694-883d-588665a2ddbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b4929b-153b-414d-898b-6c5c69e6a9cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3052a8f9-60e0-4694-883d-588665a2ddbe",
                    "LayerId": "18d126a4-09db-45a9-a7de-fd20b5f6695f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "18d126a4-09db-45a9-a7de-fd20b5f6695f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ec3a684-2455-4871-afe3-5c5264878ca6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}