{
    "id": "94febfe2-943d-4ae1-9ef5-484c42431f91",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf233309-82ee-439f-ba9d-4c116e294c75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94febfe2-943d-4ae1-9ef5-484c42431f91",
            "compositeImage": {
                "id": "de4e876a-90d9-4b66-9b27-4d9759dd2572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf233309-82ee-439f-ba9d-4c116e294c75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cd0eb9c-ed20-4fdb-9693-18a09c6e0045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf233309-82ee-439f-ba9d-4c116e294c75",
                    "LayerId": "fbc2607a-b222-46a0-aa95-67e48add7381"
                }
            ]
        },
        {
            "id": "a940310c-bf62-4eb2-ab46-0e0d60cddc68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94febfe2-943d-4ae1-9ef5-484c42431f91",
            "compositeImage": {
                "id": "5b194bbe-d47b-4ec7-8d39-797a32208894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a940310c-bf62-4eb2-ab46-0e0d60cddc68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2985f3d0-cb4b-49a2-91d9-b8927a6c2118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a940310c-bf62-4eb2-ab46-0e0d60cddc68",
                    "LayerId": "fbc2607a-b222-46a0-aa95-67e48add7381"
                }
            ]
        },
        {
            "id": "610c75a7-f77a-4234-8dc1-ad369df74e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94febfe2-943d-4ae1-9ef5-484c42431f91",
            "compositeImage": {
                "id": "fe3b7314-88bf-407e-b070-37ec3677fcd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "610c75a7-f77a-4234-8dc1-ad369df74e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cba8bde-74a3-4c44-983f-b899e92dfd34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "610c75a7-f77a-4234-8dc1-ad369df74e7d",
                    "LayerId": "fbc2607a-b222-46a0-aa95-67e48add7381"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fbc2607a-b222-46a0-aa95-67e48add7381",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94febfe2-943d-4ae1-9ef5-484c42431f91",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}