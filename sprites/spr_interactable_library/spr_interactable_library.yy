{
    "id": "9685b744-5ccd-46fb-9b1b-7d1219131e06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_library",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e180b184-f2a4-4319-acbb-92e740769c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9685b744-5ccd-46fb-9b1b-7d1219131e06",
            "compositeImage": {
                "id": "616c7d57-eff5-49cd-a401-ab35b4bc1213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e180b184-f2a4-4319-acbb-92e740769c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e417f73c-52b8-4470-8656-d70120990e90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e180b184-f2a4-4319-acbb-92e740769c82",
                    "LayerId": "38d52ed3-8bb5-4d44-a912-f079d9ec6b87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "38d52ed3-8bb5-4d44-a912-f079d9ec6b87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9685b744-5ccd-46fb-9b1b-7d1219131e06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 40
}