{
    "id": "3239e5a6-dd0a-4b38-b3d9-f3c983a050a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7244bdfc-515a-4a52-9533-5143e168d60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3239e5a6-dd0a-4b38-b3d9-f3c983a050a4",
            "compositeImage": {
                "id": "b7bebadd-a86f-438d-8f74-0be334c20844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7244bdfc-515a-4a52-9533-5143e168d60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68863cab-1333-460e-9b95-70f721c12237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7244bdfc-515a-4a52-9533-5143e168d60f",
                    "LayerId": "e51b1e67-ae8f-4e71-b4b9-12334ad91456"
                }
            ]
        },
        {
            "id": "178ba99d-9352-46d0-aea9-1f4344321dd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3239e5a6-dd0a-4b38-b3d9-f3c983a050a4",
            "compositeImage": {
                "id": "071c6914-d193-45a9-b8c7-7f97087bb5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178ba99d-9352-46d0-aea9-1f4344321dd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e13e932-abf4-4b9a-b921-43a029aa8098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178ba99d-9352-46d0-aea9-1f4344321dd7",
                    "LayerId": "e51b1e67-ae8f-4e71-b4b9-12334ad91456"
                }
            ]
        },
        {
            "id": "7f083cb3-0d12-464a-b88e-f0aba9d1dcf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3239e5a6-dd0a-4b38-b3d9-f3c983a050a4",
            "compositeImage": {
                "id": "e0a545fb-b1db-432f-8eb4-cd30d07ce238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f083cb3-0d12-464a-b88e-f0aba9d1dcf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95e70476-95f4-4c15-9c45-7e807f145e28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f083cb3-0d12-464a-b88e-f0aba9d1dcf7",
                    "LayerId": "e51b1e67-ae8f-4e71-b4b9-12334ad91456"
                }
            ]
        },
        {
            "id": "89a75598-9a61-44e9-a1e4-30e7e594f762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3239e5a6-dd0a-4b38-b3d9-f3c983a050a4",
            "compositeImage": {
                "id": "866856e9-71f5-4e14-8576-a398b6a11372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a75598-9a61-44e9-a1e4-30e7e594f762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28f157f5-4627-418f-8462-d51902274dcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a75598-9a61-44e9-a1e4-30e7e594f762",
                    "LayerId": "e51b1e67-ae8f-4e71-b4b9-12334ad91456"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e51b1e67-ae8f-4e71-b4b9-12334ad91456",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3239e5a6-dd0a-4b38-b3d9-f3c983a050a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}