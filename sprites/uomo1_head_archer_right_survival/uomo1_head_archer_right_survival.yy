{
    "id": "83b44233-0b4c-414d-983d-41ac33e0ea27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_archer_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8066caf2-01ab-4e68-972b-c3a832ca05d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83b44233-0b4c-414d-983d-41ac33e0ea27",
            "compositeImage": {
                "id": "646eceeb-d6ad-466b-bd96-9bdacea1e3bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8066caf2-01ab-4e68-972b-c3a832ca05d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b5cae47-c335-4875-94d8-6d0e295538b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8066caf2-01ab-4e68-972b-c3a832ca05d2",
                    "LayerId": "0faec6ea-d13a-447a-8341-a29f407e4d71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0faec6ea-d13a-447a-8341-a29f407e4d71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83b44233-0b4c-414d-983d-41ac33e0ea27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}