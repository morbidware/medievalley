{
    "id": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_melee_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c708486-8784-4f59-a154-f4c9261ddc0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "compositeImage": {
                "id": "07b58907-a352-438c-aa5b-45529ed2913f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c708486-8784-4f59-a154-f4c9261ddc0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332b044d-0c57-4a54-aecd-ce2c9e1b1a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c708486-8784-4f59-a154-f4c9261ddc0e",
                    "LayerId": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d"
                }
            ]
        },
        {
            "id": "f3921c2c-39a3-4468-b92c-5b52caef120c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "compositeImage": {
                "id": "16d04c6b-ab8d-4a0c-8bfd-c32929101e25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3921c2c-39a3-4468-b92c-5b52caef120c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0647b97b-d876-49cb-ac84-afb6014f4be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3921c2c-39a3-4468-b92c-5b52caef120c",
                    "LayerId": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d"
                }
            ]
        },
        {
            "id": "ef19d1dc-0c11-46d6-bde0-54e1eed2bbf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "compositeImage": {
                "id": "f3dd3a5e-9b93-4f60-8a4f-5e7c99f552b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef19d1dc-0c11-46d6-bde0-54e1eed2bbf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37dc3e7a-09a7-472c-b47d-10d8db3d8d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef19d1dc-0c11-46d6-bde0-54e1eed2bbf2",
                    "LayerId": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d"
                }
            ]
        },
        {
            "id": "08003a92-d348-41da-9a9b-1009854ab836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "compositeImage": {
                "id": "b40f5beb-2b82-4521-a613-5a1b33b18727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08003a92-d348-41da-9a9b-1009854ab836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6c98bc-5d05-43cb-a4c8-1e3121c33863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08003a92-d348-41da-9a9b-1009854ab836",
                    "LayerId": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d"
                }
            ]
        },
        {
            "id": "c9c8f52e-a233-46e3-8c35-baf16108302a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "compositeImage": {
                "id": "7873e5d0-c310-4a83-bc66-a9e21da54ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c8f52e-a233-46e3-8c35-baf16108302a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c373b2-9010-468c-9e61-14a5cb8461f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c8f52e-a233-46e3-8c35-baf16108302a",
                    "LayerId": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d"
                }
            ]
        },
        {
            "id": "fcf8ca37-410a-40d5-9015-b4e42ed81899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "compositeImage": {
                "id": "79974e2e-1505-4075-adaf-981ef85f0667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf8ca37-410a-40d5-9015-b4e42ed81899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a982847-5fce-4363-aa19-4e07e0a727de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf8ca37-410a-40d5-9015-b4e42ed81899",
                    "LayerId": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "40dc083d-ef2a-4a1e-aaa9-0f629640b38d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f6b85e0-0aff-4068-acd5-c3cc3688bce2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}