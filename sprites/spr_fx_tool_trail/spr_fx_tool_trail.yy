{
    "id": "3af956ea-94ea-4101-ba51-3464113e98e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_tool_trail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 7,
    "bbox_right": 27,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0865ddc0-c3f4-499b-808a-2c699cecb899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af956ea-94ea-4101-ba51-3464113e98e0",
            "compositeImage": {
                "id": "f78b5465-a1ed-4fd1-8378-a2de8e62307e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0865ddc0-c3f4-499b-808a-2c699cecb899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d750aa1-6716-4b24-a61d-f20041fd4aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0865ddc0-c3f4-499b-808a-2c699cecb899",
                    "LayerId": "1a3b94ed-7a1a-4628-999d-2ffa5718040b"
                }
            ]
        },
        {
            "id": "20550414-100a-4068-acb2-5572690db277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af956ea-94ea-4101-ba51-3464113e98e0",
            "compositeImage": {
                "id": "bda71058-1d75-45fc-872c-adc4ff0e4e48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20550414-100a-4068-acb2-5572690db277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ae1da82-cdcc-41ee-9c25-d6a428a17361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20550414-100a-4068-acb2-5572690db277",
                    "LayerId": "1a3b94ed-7a1a-4628-999d-2ffa5718040b"
                }
            ]
        },
        {
            "id": "512933f2-afd2-49ee-9ab2-dfef1ed70346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af956ea-94ea-4101-ba51-3464113e98e0",
            "compositeImage": {
                "id": "2c1753c4-ba59-4df4-bfe5-843971f3ca7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "512933f2-afd2-49ee-9ab2-dfef1ed70346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0f0b36-fec2-4764-99e2-ed12d163146b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512933f2-afd2-49ee-9ab2-dfef1ed70346",
                    "LayerId": "1a3b94ed-7a1a-4628-999d-2ffa5718040b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "1a3b94ed-7a1a-4628-999d-2ffa5718040b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3af956ea-94ea-4101-ba51-3464113e98e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}