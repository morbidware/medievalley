{
    "id": "5c0d38f4-30c3-467f-b81a-30075b6789c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipe_kitchen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc6a597b-8ca6-4417-a5d3-691057678fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c0d38f4-30c3-467f-b81a-30075b6789c8",
            "compositeImage": {
                "id": "f1c5dcb7-ce13-4659-8c37-95d913c0b79d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc6a597b-8ca6-4417-a5d3-691057678fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd1cdc11-8db2-4b34-9620-242c52887690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc6a597b-8ca6-4417-a5d3-691057678fc9",
                    "LayerId": "37c60209-3793-4d3a-a067-0cefea093085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "37c60209-3793-4d3a-a067-0cefea093085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c0d38f4-30c3-467f-b81a-30075b6789c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}