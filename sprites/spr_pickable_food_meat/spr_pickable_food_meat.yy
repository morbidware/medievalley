{
    "id": "410439e4-9679-4dec-a905-15c63c0c50e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_food_meat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "061122e0-2f99-4bac-9cf8-27e504a7c2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "410439e4-9679-4dec-a905-15c63c0c50e1",
            "compositeImage": {
                "id": "780f1cb3-9455-4e16-aa9e-7360a21742c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061122e0-2f99-4bac-9cf8-27e504a7c2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60040818-0b53-437f-99ef-f3adcd77d710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061122e0-2f99-4bac-9cf8-27e504a7c2d9",
                    "LayerId": "8f9c43c0-4083-4808-b384-a46e2756ce6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8f9c43c0-4083-4808-b384-a46e2756ce6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "410439e4-9679-4dec-a905-15c63c0c50e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}