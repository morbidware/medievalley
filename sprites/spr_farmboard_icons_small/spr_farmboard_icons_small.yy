{
    "id": "dc58d161-7482-4c54-bdae-ca5e75361a8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_icons_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e7dc2fb-9674-4ce3-9ca0-f394377e2763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc58d161-7482-4c54-bdae-ca5e75361a8c",
            "compositeImage": {
                "id": "bf4451b2-c5af-4a1f-8af3-60e70a94306a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7dc2fb-9674-4ce3-9ca0-f394377e2763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01d79ed-d9d9-4f15-85a5-823b19609701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7dc2fb-9674-4ce3-9ca0-f394377e2763",
                    "LayerId": "32066b78-5471-4334-8df7-d2d635615d68"
                }
            ]
        },
        {
            "id": "6fecbe00-2062-440e-bf37-21b440c2cb18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc58d161-7482-4c54-bdae-ca5e75361a8c",
            "compositeImage": {
                "id": "462e88dd-7f7d-4946-b2d6-18e4137bc0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fecbe00-2062-440e-bf37-21b440c2cb18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bf06f8a-ba58-4bb3-b76e-0a9dcd2d47a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fecbe00-2062-440e-bf37-21b440c2cb18",
                    "LayerId": "32066b78-5471-4334-8df7-d2d635615d68"
                }
            ]
        },
        {
            "id": "81626b98-163d-4447-b013-b43b39132c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc58d161-7482-4c54-bdae-ca5e75361a8c",
            "compositeImage": {
                "id": "eee2c968-cce6-4938-947c-b85e092b50e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81626b98-163d-4447-b013-b43b39132c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c14846e-211b-44a5-8b1a-ae74949eac54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81626b98-163d-4447-b013-b43b39132c84",
                    "LayerId": "32066b78-5471-4334-8df7-d2d635615d68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "32066b78-5471-4334-8df7-d2d635615d68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc58d161-7482-4c54-bdae-ca5e75361a8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 4
}