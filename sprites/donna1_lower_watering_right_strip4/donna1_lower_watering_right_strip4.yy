{
    "id": "f00ac0c9-8cc0-4073-968b-41ab35b4586e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb0b5fa8-4736-4692-90d7-1ede34ff4196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f00ac0c9-8cc0-4073-968b-41ab35b4586e",
            "compositeImage": {
                "id": "7e5ffe6e-f95f-46be-b8e7-05869c0f4e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0b5fa8-4736-4692-90d7-1ede34ff4196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017686cd-327c-4b67-aa9d-50aaf8412004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0b5fa8-4736-4692-90d7-1ede34ff4196",
                    "LayerId": "dedf4ccd-600d-4494-b0c0-34a7a031e636"
                }
            ]
        },
        {
            "id": "fb7823de-7102-4270-adc6-e72c6f6906ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f00ac0c9-8cc0-4073-968b-41ab35b4586e",
            "compositeImage": {
                "id": "1fc12832-7250-4d59-a739-ab5e59ffbf95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb7823de-7102-4270-adc6-e72c6f6906ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "642b9dd6-2bdf-42ea-9aab-78de80ad7fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb7823de-7102-4270-adc6-e72c6f6906ed",
                    "LayerId": "dedf4ccd-600d-4494-b0c0-34a7a031e636"
                }
            ]
        },
        {
            "id": "87c482f1-40ae-4925-a90b-f6347d5f49f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f00ac0c9-8cc0-4073-968b-41ab35b4586e",
            "compositeImage": {
                "id": "aeb7a5a8-1215-4eee-aa2f-b308967ebda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c482f1-40ae-4925-a90b-f6347d5f49f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "077fbec6-f765-4518-81c9-0192068ccc69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c482f1-40ae-4925-a90b-f6347d5f49f0",
                    "LayerId": "dedf4ccd-600d-4494-b0c0-34a7a031e636"
                }
            ]
        },
        {
            "id": "40511d7c-6a53-4538-b902-c42369bccb9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f00ac0c9-8cc0-4073-968b-41ab35b4586e",
            "compositeImage": {
                "id": "bfbb999b-237c-4248-bd28-3264a7908537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40511d7c-6a53-4538-b902-c42369bccb9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ae200b3-c2c6-4327-a99f-389af888dd00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40511d7c-6a53-4538-b902-c42369bccb9d",
                    "LayerId": "dedf4ccd-600d-4494-b0c0-34a7a031e636"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dedf4ccd-600d-4494-b0c0-34a7a031e636",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f00ac0c9-8cc0-4073-968b-41ab35b4586e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}