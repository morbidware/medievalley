{
    "id": "acd57c57-3706-4dcd-8867-c2231651b00f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_hammer_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "852dabf1-9753-4ac2-84ac-a494e8aaf1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "compositeImage": {
                "id": "54478bfd-99d3-414b-bade-db58a9b874f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "852dabf1-9753-4ac2-84ac-a494e8aaf1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ae9f02e-12e7-4478-b7f7-63905cd03174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "852dabf1-9753-4ac2-84ac-a494e8aaf1eb",
                    "LayerId": "79414d79-6530-4bdd-ab9a-8011f2cbb652"
                }
            ]
        },
        {
            "id": "8ca932f7-a323-460c-8f10-b0539f95913c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "compositeImage": {
                "id": "7dc15976-b92a-4a50-bbda-2784efa97340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca932f7-a323-460c-8f10-b0539f95913c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe170c8-0a9a-4233-bc7e-79a6bc156940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca932f7-a323-460c-8f10-b0539f95913c",
                    "LayerId": "79414d79-6530-4bdd-ab9a-8011f2cbb652"
                }
            ]
        },
        {
            "id": "dab6fc42-cb4f-4ff4-b181-fe823a1f3e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "compositeImage": {
                "id": "e25fc37a-38c1-4a0a-b563-82fec5b3e0be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dab6fc42-cb4f-4ff4-b181-fe823a1f3e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5048e52-1f9c-41e6-b075-3e4b7c60e869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dab6fc42-cb4f-4ff4-b181-fe823a1f3e50",
                    "LayerId": "79414d79-6530-4bdd-ab9a-8011f2cbb652"
                }
            ]
        },
        {
            "id": "4a4ae42d-a83f-46e5-a9bb-e1f37fd3ed83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "compositeImage": {
                "id": "266ef63e-6d16-448e-985b-edc164d7f75f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4ae42d-a83f-46e5-a9bb-e1f37fd3ed83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f933d23-50e8-445b-a7f5-a45ef7bbca13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4ae42d-a83f-46e5-a9bb-e1f37fd3ed83",
                    "LayerId": "79414d79-6530-4bdd-ab9a-8011f2cbb652"
                }
            ]
        },
        {
            "id": "efd43f8b-f3d0-4a1b-8a3a-5c7570392f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "compositeImage": {
                "id": "5b625e8e-dc82-4b89-b4f9-04f9e2ebd29a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efd43f8b-f3d0-4a1b-8a3a-5c7570392f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11aa51dc-2751-4f19-83f8-6de0e90d9446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efd43f8b-f3d0-4a1b-8a3a-5c7570392f72",
                    "LayerId": "79414d79-6530-4bdd-ab9a-8011f2cbb652"
                }
            ]
        },
        {
            "id": "7c2b2f2f-e677-4699-a697-5e561e71f18e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "compositeImage": {
                "id": "444fceb7-2198-4799-92c2-cf1b645048ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c2b2f2f-e677-4699-a697-5e561e71f18e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47185465-387f-4f2d-99af-c3338947cefa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c2b2f2f-e677-4699-a697-5e561e71f18e",
                    "LayerId": "79414d79-6530-4bdd-ab9a-8011f2cbb652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "79414d79-6530-4bdd-ab9a-8011f2cbb652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acd57c57-3706-4dcd-8867-c2231651b00f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}