{
    "id": "84c27701-6c5c-4a0c-ae10-d708e3b16d20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45a6a122-6657-4c93-abd9-d51193d6f392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84c27701-6c5c-4a0c-ae10-d708e3b16d20",
            "compositeImage": {
                "id": "2e0e4761-8251-4dad-ac81-b42032d15f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45a6a122-6657-4c93-abd9-d51193d6f392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c2fdf7-d6d2-4cda-9775-05fbde4ed834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45a6a122-6657-4c93-abd9-d51193d6f392",
                    "LayerId": "9c9cc56e-fd5f-4d3c-ac40-8d44b10a75e0"
                }
            ]
        },
        {
            "id": "db130541-d81c-4e6e-8872-93e54baee2b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84c27701-6c5c-4a0c-ae10-d708e3b16d20",
            "compositeImage": {
                "id": "a3c4b6fb-c5aa-4cc5-b6d8-611f2d492134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db130541-d81c-4e6e-8872-93e54baee2b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23e5571-edfd-426d-87f5-380dc3110300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db130541-d81c-4e6e-8872-93e54baee2b6",
                    "LayerId": "9c9cc56e-fd5f-4d3c-ac40-8d44b10a75e0"
                }
            ]
        },
        {
            "id": "cd7ee88b-3542-4cc4-ac51-ebbb3e8ac917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84c27701-6c5c-4a0c-ae10-d708e3b16d20",
            "compositeImage": {
                "id": "ea0dbf9c-7b3c-4688-802f-54f816515565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd7ee88b-3542-4cc4-ac51-ebbb3e8ac917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "534dce1a-bdc4-46c5-a430-b858b0d52460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd7ee88b-3542-4cc4-ac51-ebbb3e8ac917",
                    "LayerId": "9c9cc56e-fd5f-4d3c-ac40-8d44b10a75e0"
                }
            ]
        },
        {
            "id": "cbd640b0-b9f9-4244-98d5-952d92f93eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84c27701-6c5c-4a0c-ae10-d708e3b16d20",
            "compositeImage": {
                "id": "5209e969-773f-49bd-ad48-a70ddb19b3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd640b0-b9f9-4244-98d5-952d92f93eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "828dca70-60e0-4d69-846b-5727a8e9aeea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd640b0-b9f9-4244-98d5-952d92f93eb6",
                    "LayerId": "9c9cc56e-fd5f-4d3c-ac40-8d44b10a75e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9c9cc56e-fd5f-4d3c-ac40-8d44b10a75e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84c27701-6c5c-4a0c-ae10-d708e3b16d20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}