{
    "id": "751dd63f-6558-4fc8-b2e6-83ce254a9b5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_gethit_up_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "488dc663-1afa-4fea-93da-92c7d72d4ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751dd63f-6558-4fc8-b2e6-83ce254a9b5b",
            "compositeImage": {
                "id": "b3cae2a7-d8bf-4110-9ecb-99cb413a118f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488dc663-1afa-4fea-93da-92c7d72d4ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abfd4db2-9568-4627-81ce-2f7f55442207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488dc663-1afa-4fea-93da-92c7d72d4ccc",
                    "LayerId": "dd161a50-c212-4659-a5a6-4c76eb3c5b3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd161a50-c212-4659-a5a6-4c76eb3c5b3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "751dd63f-6558-4fc8-b2e6-83ce254a9b5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}