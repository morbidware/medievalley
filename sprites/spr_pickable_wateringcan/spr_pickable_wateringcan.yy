{
    "id": "2d0bd09c-cadc-4965-87b6-70a2c4c4b72b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_wateringcan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "535d24e3-8f73-4bb9-8ae9-f6d742071544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d0bd09c-cadc-4965-87b6-70a2c4c4b72b",
            "compositeImage": {
                "id": "d69596bf-9f0c-41e2-bfda-2a3d2f86db1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535d24e3-8f73-4bb9-8ae9-f6d742071544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4e372a1-1ed7-4448-b58e-ee6ed6cdc160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535d24e3-8f73-4bb9-8ae9-f6d742071544",
                    "LayerId": "5d6a6dd9-5004-45bc-a99c-1471ba077e3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5d6a6dd9-5004-45bc-a99c-1471ba077e3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d0bd09c-cadc-4965-87b6-70a2c4c4b72b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}