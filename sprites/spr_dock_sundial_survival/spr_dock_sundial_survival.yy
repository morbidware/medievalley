{
    "id": "31839411-56e3-4bf4-9c00-60ef414c539b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_sundial_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f68c954b-ff71-410d-b0c3-10c33e756629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31839411-56e3-4bf4-9c00-60ef414c539b",
            "compositeImage": {
                "id": "5b6f1942-626b-4854-901c-fd401a7ebabf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f68c954b-ff71-410d-b0c3-10c33e756629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4772da41-de7a-48f7-807e-037d13c98520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f68c954b-ff71-410d-b0c3-10c33e756629",
                    "LayerId": "63118e72-1d98-45b7-aa5f-e87f1898d7de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "63118e72-1d98-45b7-aa5f-e87f1898d7de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31839411-56e3-4bf4-9c00-60ef414c539b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}