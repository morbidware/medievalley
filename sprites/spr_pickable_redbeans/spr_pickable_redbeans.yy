{
    "id": "507c2712-ee43-41d6-8917-0ab3047ae8c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_redbeans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb042708-8fd6-43a3-918f-12c986c96acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "507c2712-ee43-41d6-8917-0ab3047ae8c4",
            "compositeImage": {
                "id": "d2af2534-80b2-4b80-9e06-3b031c644816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb042708-8fd6-43a3-918f-12c986c96acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3615aa21-d5e0-4646-b26f-fc06e3b17b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb042708-8fd6-43a3-918f-12c986c96acf",
                    "LayerId": "15c1281c-9609-444b-acf2-939acdda86b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "15c1281c-9609-444b-acf2-939acdda86b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "507c2712-ee43-41d6-8917-0ab3047ae8c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}