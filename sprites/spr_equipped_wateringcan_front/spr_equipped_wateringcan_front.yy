{
    "id": "fe0a2d13-e350-417f-bcf4-7b0bdec6fb8f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_wateringcan_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f4a0992-9664-4957-9436-4ddfd046bd9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe0a2d13-e350-417f-bcf4-7b0bdec6fb8f",
            "compositeImage": {
                "id": "de53eceb-7226-4018-b469-f850702ed79e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f4a0992-9664-4957-9436-4ddfd046bd9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493d11fd-063a-47ef-8c1c-39164af3170d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f4a0992-9664-4957-9436-4ddfd046bd9c",
                    "LayerId": "55c62322-0333-48d7-962e-28fdd734ecd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "55c62322-0333-48d7-962e-28fdd734ecd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe0a2d13-e350-417f-bcf4-7b0bdec6fb8f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 1
}