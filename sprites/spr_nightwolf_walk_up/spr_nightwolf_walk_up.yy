{
    "id": "9a17a28d-c2b6-475b-9184-5927cbaacd74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 33,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b281d9f6-47bc-46c9-9ff1-1221bb70d5ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a17a28d-c2b6-475b-9184-5927cbaacd74",
            "compositeImage": {
                "id": "0cf5528b-165e-4a10-a1a1-d92c5d7e2fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b281d9f6-47bc-46c9-9ff1-1221bb70d5ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c5b99b-e2bd-4c49-aa1d-c1ef9375829d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b281d9f6-47bc-46c9-9ff1-1221bb70d5ae",
                    "LayerId": "e94891af-c10c-4afb-961e-3b53cce9854c"
                }
            ]
        },
        {
            "id": "6b96d233-133f-4b45-9b5d-d4ef201a95c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a17a28d-c2b6-475b-9184-5927cbaacd74",
            "compositeImage": {
                "id": "8831d3ac-f118-4676-934c-67bbfdcab94c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b96d233-133f-4b45-9b5d-d4ef201a95c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8d466ca-f564-4aaf-9927-adf39a10955d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b96d233-133f-4b45-9b5d-d4ef201a95c1",
                    "LayerId": "e94891af-c10c-4afb-961e-3b53cce9854c"
                }
            ]
        },
        {
            "id": "313a6739-29f9-46fa-a0c4-4dd2a694de51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a17a28d-c2b6-475b-9184-5927cbaacd74",
            "compositeImage": {
                "id": "1012352e-9b85-4bb4-8faf-39ccd70c6256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313a6739-29f9-46fa-a0c4-4dd2a694de51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "573ecc3a-caef-42b7-94d1-fdb58a995b5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313a6739-29f9-46fa-a0c4-4dd2a694de51",
                    "LayerId": "e94891af-c10c-4afb-961e-3b53cce9854c"
                }
            ]
        },
        {
            "id": "b4d0810a-f36d-49c9-884f-2f1a90009a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a17a28d-c2b6-475b-9184-5927cbaacd74",
            "compositeImage": {
                "id": "900e7894-9854-4196-a8e1-e26971929c31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d0810a-f36d-49c9-884f-2f1a90009a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8de9be17-2212-4cea-90bd-fa0a27171857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d0810a-f36d-49c9-884f-2f1a90009a48",
                    "LayerId": "e94891af-c10c-4afb-961e-3b53cce9854c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "e94891af-c10c-4afb-961e-3b53cce9854c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a17a28d-c2b6-475b-9184-5927cbaacd74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}