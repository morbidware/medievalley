{
    "id": "ff1af29d-4a99-420f-a6e2-ae2305d1a5ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_sapling_hard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10b53b1a-a9a8-4de9-bad1-4a6e817d5d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff1af29d-4a99-420f-a6e2-ae2305d1a5ce",
            "compositeImage": {
                "id": "ca97caaa-7b13-4b88-ada5-76eb8378dbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b53b1a-a9a8-4de9-bad1-4a6e817d5d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f1bb43-df65-4ba6-912c-d0600d06c3a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b53b1a-a9a8-4de9-bad1-4a6e817d5d50",
                    "LayerId": "f5fd5d35-ddb3-423e-a5dd-e73c036ed892"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "f5fd5d35-ddb3-423e-a5dd-e73c036ed892",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff1af29d-4a99-420f-a6e2-ae2305d1a5ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 15
}