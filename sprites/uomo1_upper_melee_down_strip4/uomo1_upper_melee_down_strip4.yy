{
    "id": "472e0566-90ba-4574-b980-605c6a1ede34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b59a902e-40cf-41b2-ae73-d83df5b9b39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472e0566-90ba-4574-b980-605c6a1ede34",
            "compositeImage": {
                "id": "a0547a15-8fc6-4239-a11e-259ed33e68cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b59a902e-40cf-41b2-ae73-d83df5b9b39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2040a175-0593-49b3-957e-06bd4b4c2fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b59a902e-40cf-41b2-ae73-d83df5b9b39a",
                    "LayerId": "bde7d3b2-9093-402f-8a5c-7396e5b3fa35"
                }
            ]
        },
        {
            "id": "269bc4fe-5157-4990-b3ab-067bad6fc120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472e0566-90ba-4574-b980-605c6a1ede34",
            "compositeImage": {
                "id": "45b511a7-9660-4855-92cb-c5dec7461a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269bc4fe-5157-4990-b3ab-067bad6fc120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71feaa30-43cf-42b8-a893-e14da29596a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269bc4fe-5157-4990-b3ab-067bad6fc120",
                    "LayerId": "bde7d3b2-9093-402f-8a5c-7396e5b3fa35"
                }
            ]
        },
        {
            "id": "c9440d57-cbb6-4c8d-ba1e-0359d94b3aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472e0566-90ba-4574-b980-605c6a1ede34",
            "compositeImage": {
                "id": "0442c69d-3713-48bd-bc3a-55227bb4a3b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9440d57-cbb6-4c8d-ba1e-0359d94b3aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b39b33-2037-4980-8833-c2c90d2b0aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9440d57-cbb6-4c8d-ba1e-0359d94b3aac",
                    "LayerId": "bde7d3b2-9093-402f-8a5c-7396e5b3fa35"
                }
            ]
        },
        {
            "id": "f3bc9667-16f0-4c10-81fc-419776ea2be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472e0566-90ba-4574-b980-605c6a1ede34",
            "compositeImage": {
                "id": "1481c840-28cd-4d7b-b26a-258e8af9346f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3bc9667-16f0-4c10-81fc-419776ea2be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef312a19-5b8c-42ad-9f88-da2979aecfe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3bc9667-16f0-4c10-81fc-419776ea2be6",
                    "LayerId": "bde7d3b2-9093-402f-8a5c-7396e5b3fa35"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bde7d3b2-9093-402f-8a5c-7396e5b3fa35",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "472e0566-90ba-4574-b980-605c6a1ede34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}