{
    "id": "d3b23398-1e5c-48ac-b61e-f8b1b8418c42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ea0cb4c-1e5d-4735-acd0-00f943b4410b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3b23398-1e5c-48ac-b61e-f8b1b8418c42",
            "compositeImage": {
                "id": "787714e1-a29e-4983-8962-4c9ef2319d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ea0cb4c-1e5d-4735-acd0-00f943b4410b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496dab9f-fa86-4328-b689-af3d497bea58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea0cb4c-1e5d-4735-acd0-00f943b4410b",
                    "LayerId": "6f5f7857-69c2-4c4f-b83c-fe17990c7873"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f5f7857-69c2-4c4f-b83c-fe17990c7873",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3b23398-1e5c-48ac-b61e-f8b1b8418c42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}