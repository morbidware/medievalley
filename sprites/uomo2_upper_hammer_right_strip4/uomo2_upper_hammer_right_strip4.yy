{
    "id": "bc33b0ff-b2ee-491c-b1ac-e4f71fce0fd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc6b8100-86f5-4019-b30a-7b7d12b49ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc33b0ff-b2ee-491c-b1ac-e4f71fce0fd1",
            "compositeImage": {
                "id": "fee78661-bc5f-43f0-88db-ea39c7f46c31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc6b8100-86f5-4019-b30a-7b7d12b49ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee137bc-480f-43ee-8db6-f7d6883c3794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc6b8100-86f5-4019-b30a-7b7d12b49ca6",
                    "LayerId": "a54fa78f-d251-4e7e-a8ab-a31bbcd093f1"
                }
            ]
        },
        {
            "id": "18f53a36-eb01-464f-bf7e-43f90a55e770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc33b0ff-b2ee-491c-b1ac-e4f71fce0fd1",
            "compositeImage": {
                "id": "15e198d3-6310-4135-b37a-7b04dad87531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f53a36-eb01-464f-bf7e-43f90a55e770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0f6ddec-1d67-4f1b-b4bb-015dfaa56743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f53a36-eb01-464f-bf7e-43f90a55e770",
                    "LayerId": "a54fa78f-d251-4e7e-a8ab-a31bbcd093f1"
                }
            ]
        },
        {
            "id": "2316757b-5258-47c8-afdc-ec79843f0e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc33b0ff-b2ee-491c-b1ac-e4f71fce0fd1",
            "compositeImage": {
                "id": "4ac8d338-5234-4154-ac3b-d3d52c22e251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2316757b-5258-47c8-afdc-ec79843f0e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d927142f-c986-431f-a6f3-2171d046726b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2316757b-5258-47c8-afdc-ec79843f0e1f",
                    "LayerId": "a54fa78f-d251-4e7e-a8ab-a31bbcd093f1"
                }
            ]
        },
        {
            "id": "f3df51ec-e91e-49b5-bb48-f1bc09733b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc33b0ff-b2ee-491c-b1ac-e4f71fce0fd1",
            "compositeImage": {
                "id": "80e5fa7b-6942-4f4d-8fc3-788bb895c2bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3df51ec-e91e-49b5-bb48-f1bc09733b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e251c48b-8177-4450-9789-1da1c8ab919f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3df51ec-e91e-49b5-bb48-f1bc09733b0a",
                    "LayerId": "a54fa78f-d251-4e7e-a8ab-a31bbcd093f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a54fa78f-d251-4e7e-a8ab-a31bbcd093f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc33b0ff-b2ee-491c-b1ac-e4f71fce0fd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}