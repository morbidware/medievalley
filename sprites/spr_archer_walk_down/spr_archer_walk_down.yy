{
    "id": "59e30864-6094-42c5-902b-803c139ea3e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fd854ff-13b0-47e5-b712-8ecaa0712024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59e30864-6094-42c5-902b-803c139ea3e6",
            "compositeImage": {
                "id": "a20a4fd1-c02d-4f96-811c-59f96ef64ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fd854ff-13b0-47e5-b712-8ecaa0712024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1916128c-c31c-42ee-be2b-e7e6684df23e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fd854ff-13b0-47e5-b712-8ecaa0712024",
                    "LayerId": "80532b51-aea5-405a-bb26-bfa3b6bc9667"
                }
            ]
        },
        {
            "id": "9edc4e75-6ccf-495f-89b5-a246db5ee03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59e30864-6094-42c5-902b-803c139ea3e6",
            "compositeImage": {
                "id": "4dee6e77-5b81-4563-b2c5-5a897246a981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9edc4e75-6ccf-495f-89b5-a246db5ee03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55d7f97-5a2e-48f9-959a-60b534b905bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9edc4e75-6ccf-495f-89b5-a246db5ee03b",
                    "LayerId": "80532b51-aea5-405a-bb26-bfa3b6bc9667"
                }
            ]
        },
        {
            "id": "e9e37820-c063-41c4-96c1-f277cc30c53c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59e30864-6094-42c5-902b-803c139ea3e6",
            "compositeImage": {
                "id": "d4552f85-2a82-47d1-bb72-e8a3be33b4ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9e37820-c063-41c4-96c1-f277cc30c53c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846e00fd-7fd7-44a6-aa41-fc6986dfecde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9e37820-c063-41c4-96c1-f277cc30c53c",
                    "LayerId": "80532b51-aea5-405a-bb26-bfa3b6bc9667"
                }
            ]
        },
        {
            "id": "88fed858-8b4d-49db-8515-d265579f2468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59e30864-6094-42c5-902b-803c139ea3e6",
            "compositeImage": {
                "id": "93426d63-d780-48e5-967e-039f31c05bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88fed858-8b4d-49db-8515-d265579f2468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88f050c3-61bf-4bbc-af86-5ee29d3a6ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88fed858-8b4d-49db-8515-d265579f2468",
                    "LayerId": "80532b51-aea5-405a-bb26-bfa3b6bc9667"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "80532b51-aea5-405a-bb26-bfa3b6bc9667",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59e30864-6094-42c5-902b-803c139ea3e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}