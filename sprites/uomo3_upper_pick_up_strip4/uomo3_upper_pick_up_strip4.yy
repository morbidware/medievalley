{
    "id": "f1285daa-2097-4285-92b8-f134cfc734f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6109da2-1fe4-4b37-a0a9-69255b519099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1285daa-2097-4285-92b8-f134cfc734f1",
            "compositeImage": {
                "id": "520f31d8-762b-42de-aa48-053fcbc76994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6109da2-1fe4-4b37-a0a9-69255b519099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e979bd97-86b9-4889-a23a-61c4132889cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6109da2-1fe4-4b37-a0a9-69255b519099",
                    "LayerId": "2282732e-a6ad-4930-8be6-0e126a8e3c80"
                }
            ]
        },
        {
            "id": "5bddacad-a5ab-426c-b8ae-d029329aa772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1285daa-2097-4285-92b8-f134cfc734f1",
            "compositeImage": {
                "id": "040183ed-3252-405d-8f78-cb772d4e1f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bddacad-a5ab-426c-b8ae-d029329aa772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde3f356-278a-4fa0-866c-3c74fc574df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bddacad-a5ab-426c-b8ae-d029329aa772",
                    "LayerId": "2282732e-a6ad-4930-8be6-0e126a8e3c80"
                }
            ]
        },
        {
            "id": "2a5ffd2d-3373-4485-8c14-a04ecf07921b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1285daa-2097-4285-92b8-f134cfc734f1",
            "compositeImage": {
                "id": "d0d9da2e-bfe6-4153-9525-64268b6458f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a5ffd2d-3373-4485-8c14-a04ecf07921b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904f4710-54b8-4306-931c-822f311a3676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a5ffd2d-3373-4485-8c14-a04ecf07921b",
                    "LayerId": "2282732e-a6ad-4930-8be6-0e126a8e3c80"
                }
            ]
        },
        {
            "id": "b3d6d64c-bfe4-4ea9-8a5c-de63d095a579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1285daa-2097-4285-92b8-f134cfc734f1",
            "compositeImage": {
                "id": "abcf4f11-32d0-4cc8-bcf3-eb823b1ea28c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d6d64c-bfe4-4ea9-8a5c-de63d095a579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0c6cc22-4658-4d15-9444-5c4a6d160c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d6d64c-bfe4-4ea9-8a5c-de63d095a579",
                    "LayerId": "2282732e-a6ad-4930-8be6-0e126a8e3c80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2282732e-a6ad-4930-8be6-0e126a8e3c80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1285daa-2097-4285-92b8-f134cfc734f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}