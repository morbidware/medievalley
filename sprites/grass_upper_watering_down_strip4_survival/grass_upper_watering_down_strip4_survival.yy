{
    "id": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_watering_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39a54bd0-6a9b-4eb3-a404-d1aa617c9cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "compositeImage": {
                "id": "f54225e4-b8e8-4303-9414-1785da056b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39a54bd0-6a9b-4eb3-a404-d1aa617c9cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbf289b7-ba3e-4e57-8410-8eb6f62649da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39a54bd0-6a9b-4eb3-a404-d1aa617c9cca",
                    "LayerId": "2e785c5b-b0bd-4c95-995c-1c9a94330869"
                }
            ]
        },
        {
            "id": "b84d7c90-922c-4a7f-90a1-e87992d1b568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "compositeImage": {
                "id": "dac6ca15-e1a4-4852-a3db-3aadf43573c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84d7c90-922c-4a7f-90a1-e87992d1b568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "395dce84-62ce-40e1-97e7-02257ffb6dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84d7c90-922c-4a7f-90a1-e87992d1b568",
                    "LayerId": "2e785c5b-b0bd-4c95-995c-1c9a94330869"
                }
            ]
        },
        {
            "id": "67e52d8a-1ca9-48af-9f54-5a1509809750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "compositeImage": {
                "id": "699edb86-bd53-4243-9b31-3d4a61e6298a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67e52d8a-1ca9-48af-9f54-5a1509809750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e996fc7-2383-43ed-b206-15d88752afb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67e52d8a-1ca9-48af-9f54-5a1509809750",
                    "LayerId": "2e785c5b-b0bd-4c95-995c-1c9a94330869"
                }
            ]
        },
        {
            "id": "ec1a7dac-592b-4ae5-a50e-cc242841df2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "compositeImage": {
                "id": "da820892-e1d0-480d-a4f7-071936f609a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec1a7dac-592b-4ae5-a50e-cc242841df2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed04926a-6e3e-4937-bb61-9d05a03e5b33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec1a7dac-592b-4ae5-a50e-cc242841df2c",
                    "LayerId": "2e785c5b-b0bd-4c95-995c-1c9a94330869"
                }
            ]
        },
        {
            "id": "d5a5c628-fa9e-4170-8a09-034fe52f653a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "compositeImage": {
                "id": "44d0e934-3643-421b-83e4-d411a69e4ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a5c628-fa9e-4170-8a09-034fe52f653a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda7e552-253a-4c9a-b53a-db3ab4f1a89c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a5c628-fa9e-4170-8a09-034fe52f653a",
                    "LayerId": "2e785c5b-b0bd-4c95-995c-1c9a94330869"
                }
            ]
        },
        {
            "id": "d2bc1fd3-d3a2-482d-b74c-cfd03cca2b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "compositeImage": {
                "id": "50597457-96e9-44ba-b941-ec0b03e2ec43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2bc1fd3-d3a2-482d-b74c-cfd03cca2b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "361522a2-66db-4fcf-a3c5-9d8920459866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2bc1fd3-d3a2-482d-b74c-cfd03cca2b86",
                    "LayerId": "2e785c5b-b0bd-4c95-995c-1c9a94330869"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2e785c5b-b0bd-4c95-995c-1c9a94330869",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d9c6d7b-288b-458d-9716-0b78fdfce3a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}