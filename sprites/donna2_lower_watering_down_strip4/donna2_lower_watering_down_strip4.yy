{
    "id": "578f1c44-a15d-4430-9519-8bbfe9b9e4ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32f2bbc6-f3f8-473f-a18c-2916d74dcbe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "578f1c44-a15d-4430-9519-8bbfe9b9e4ee",
            "compositeImage": {
                "id": "8818e026-3e8e-4823-9689-0497b4e16244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32f2bbc6-f3f8-473f-a18c-2916d74dcbe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b401bbd0-ebc2-4f1b-910d-9447ddbe65e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32f2bbc6-f3f8-473f-a18c-2916d74dcbe7",
                    "LayerId": "408d30c9-f84b-48e9-b3ab-9d64b423e29c"
                }
            ]
        },
        {
            "id": "612115d1-75aa-4b7d-a577-30f00dbcd628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "578f1c44-a15d-4430-9519-8bbfe9b9e4ee",
            "compositeImage": {
                "id": "4dbc0bf2-81dc-4b1f-9bed-6a8665a3865f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612115d1-75aa-4b7d-a577-30f00dbcd628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2214cf1-b5a2-41e2-8588-3d1fceae1a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612115d1-75aa-4b7d-a577-30f00dbcd628",
                    "LayerId": "408d30c9-f84b-48e9-b3ab-9d64b423e29c"
                }
            ]
        },
        {
            "id": "0e17f6f1-4939-4b32-b63a-cb72a7360cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "578f1c44-a15d-4430-9519-8bbfe9b9e4ee",
            "compositeImage": {
                "id": "cf2d8565-f608-45ce-9e1f-fbee8aa01738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e17f6f1-4939-4b32-b63a-cb72a7360cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4f52e61-93e0-43a5-a0b0-570c91f08104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e17f6f1-4939-4b32-b63a-cb72a7360cc3",
                    "LayerId": "408d30c9-f84b-48e9-b3ab-9d64b423e29c"
                }
            ]
        },
        {
            "id": "0d9e6299-e07b-46ca-8da4-ab4541eaffef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "578f1c44-a15d-4430-9519-8bbfe9b9e4ee",
            "compositeImage": {
                "id": "724e01f9-024a-4bda-b80e-aef581c3b4d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d9e6299-e07b-46ca-8da4-ab4541eaffef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0f6b1e3-a81f-45dc-9e3f-cbb71b117288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d9e6299-e07b-46ca-8da4-ab4541eaffef",
                    "LayerId": "408d30c9-f84b-48e9-b3ab-9d64b423e29c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "408d30c9-f84b-48e9-b3ab-9d64b423e29c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "578f1c44-a15d-4430-9519-8bbfe9b9e4ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}