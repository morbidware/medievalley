{
    "id": "246cc246-f546-4196-b99d-46c32dd9fc34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_close_silver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b19ff8c-ee02-49e5-93e5-33ea7deb921e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "246cc246-f546-4196-b99d-46c32dd9fc34",
            "compositeImage": {
                "id": "c355d4dd-cc48-4576-8de2-a7bae5d3a77b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b19ff8c-ee02-49e5-93e5-33ea7deb921e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9aefef-422d-4814-9074-b19d6c15b777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b19ff8c-ee02-49e5-93e5-33ea7deb921e",
                    "LayerId": "f5fafcf7-be97-4520-a994-37c244a27012"
                }
            ]
        },
        {
            "id": "97b8d31c-09f6-4914-a42c-854784ced4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "246cc246-f546-4196-b99d-46c32dd9fc34",
            "compositeImage": {
                "id": "0d0cce69-dbac-4c6d-9690-f05432cfa5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b8d31c-09f6-4914-a42c-854784ced4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f295c47-dc8c-42a0-a860-109b03c242c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b8d31c-09f6-4914-a42c-854784ced4c9",
                    "LayerId": "f5fafcf7-be97-4520-a994-37c244a27012"
                }
            ]
        },
        {
            "id": "f09c6623-6cca-478f-847b-7659d56b4ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "246cc246-f546-4196-b99d-46c32dd9fc34",
            "compositeImage": {
                "id": "abcf4c92-0efa-4188-92ec-e61afae3c3d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f09c6623-6cca-478f-847b-7659d56b4ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b184b91e-fe06-4075-b086-ac1ca3e7ed89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f09c6623-6cca-478f-847b-7659d56b4ad4",
                    "LayerId": "f5fafcf7-be97-4520-a994-37c244a27012"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f5fafcf7-be97-4520-a994-37c244a27012",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "246cc246-f546-4196-b99d-46c32dd9fc34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}