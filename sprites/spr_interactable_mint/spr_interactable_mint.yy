{
    "id": "06a4cddd-4ac3-4e05-be4f-667326bc6e71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_mint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46806239-7a0c-4614-b622-cc250ee7b08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a4cddd-4ac3-4e05-be4f-667326bc6e71",
            "compositeImage": {
                "id": "16cca750-208c-4c68-9f4d-635eb015cc7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46806239-7a0c-4614-b622-cc250ee7b08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a212be66-1a7f-46f9-81e9-edde21a3a361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46806239-7a0c-4614-b622-cc250ee7b08a",
                    "LayerId": "0e3c1937-ec15-4668-af54-11deb7e414c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0e3c1937-ec15-4668-af54-11deb7e414c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06a4cddd-4ac3-4e05-be4f-667326bc6e71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 13
}