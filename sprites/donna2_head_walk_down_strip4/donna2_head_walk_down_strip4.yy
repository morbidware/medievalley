{
    "id": "661d270e-efa1-4025-bf71-9e10767fa07f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2617f789-903c-486b-bed6-f4e4fdbddf1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661d270e-efa1-4025-bf71-9e10767fa07f",
            "compositeImage": {
                "id": "51c7a753-c791-4ec7-9bb2-c185f07f410c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2617f789-903c-486b-bed6-f4e4fdbddf1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69402f49-909c-4992-9057-a20772f2de60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2617f789-903c-486b-bed6-f4e4fdbddf1b",
                    "LayerId": "2e9c4742-d3b7-43fc-bdbc-567a25ecd9e6"
                }
            ]
        },
        {
            "id": "97a70640-7b0e-49b6-bca5-edef6a21dc6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661d270e-efa1-4025-bf71-9e10767fa07f",
            "compositeImage": {
                "id": "dede8cb2-c30e-4151-8189-a1d210952d3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a70640-7b0e-49b6-bca5-edef6a21dc6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b462ee35-5365-468a-82fd-cc64e5322986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a70640-7b0e-49b6-bca5-edef6a21dc6f",
                    "LayerId": "2e9c4742-d3b7-43fc-bdbc-567a25ecd9e6"
                }
            ]
        },
        {
            "id": "9c6771eb-1bf0-478b-b461-be3f2f1b00e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661d270e-efa1-4025-bf71-9e10767fa07f",
            "compositeImage": {
                "id": "b6902528-0b9f-4649-8472-0ee25eca54f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c6771eb-1bf0-478b-b461-be3f2f1b00e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34af20dc-c5e8-4441-8a7e-bb20cacdbe63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c6771eb-1bf0-478b-b461-be3f2f1b00e9",
                    "LayerId": "2e9c4742-d3b7-43fc-bdbc-567a25ecd9e6"
                }
            ]
        },
        {
            "id": "3e377bda-9029-4377-898a-f33863138802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661d270e-efa1-4025-bf71-9e10767fa07f",
            "compositeImage": {
                "id": "bcb91823-2b5a-49f0-9ba4-f113ab35da7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e377bda-9029-4377-898a-f33863138802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d56e61-b12a-4d59-8068-7e5a1fa352ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e377bda-9029-4377-898a-f33863138802",
                    "LayerId": "2e9c4742-d3b7-43fc-bdbc-567a25ecd9e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2e9c4742-d3b7-43fc-bdbc-567a25ecd9e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "661d270e-efa1-4025-bf71-9e10767fa07f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}