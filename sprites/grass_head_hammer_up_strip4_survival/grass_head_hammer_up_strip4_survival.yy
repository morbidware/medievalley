{
    "id": "127047d8-9bce-4fd0-a298-e039f2055756",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_hammer_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7483489-09ee-46c3-bcdd-f0b8e5a6742a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "compositeImage": {
                "id": "07b006de-e944-4299-b8c9-7d505db2de28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7483489-09ee-46c3-bcdd-f0b8e5a6742a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feb1425d-2e90-49bd-a915-a61c09e684e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7483489-09ee-46c3-bcdd-f0b8e5a6742a",
                    "LayerId": "a99bc318-470f-4278-84f5-d75f2a036bc2"
                }
            ]
        },
        {
            "id": "ed2df6f8-bb49-45fe-ba60-2843c42c0f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "compositeImage": {
                "id": "f54ffd35-0ebb-49b6-87bb-da700d9a4b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2df6f8-bb49-45fe-ba60-2843c42c0f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "128bb058-aeb5-417a-8ed6-e066b0025946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2df6f8-bb49-45fe-ba60-2843c42c0f9f",
                    "LayerId": "a99bc318-470f-4278-84f5-d75f2a036bc2"
                }
            ]
        },
        {
            "id": "f12d6eab-509e-4894-95d5-fc9d423547d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "compositeImage": {
                "id": "28b62071-5e8d-4f91-8d4d-0494389c4724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12d6eab-509e-4894-95d5-fc9d423547d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b073eda-c355-44ff-9a97-fc4e1c8e9354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12d6eab-509e-4894-95d5-fc9d423547d8",
                    "LayerId": "a99bc318-470f-4278-84f5-d75f2a036bc2"
                }
            ]
        },
        {
            "id": "3f2feb05-f3e3-4c54-b85c-96d642073313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "compositeImage": {
                "id": "16452c69-073c-4422-a3a2-3df95a31a10f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2feb05-f3e3-4c54-b85c-96d642073313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d2ff54-2350-43e4-96c9-17f7acb927b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2feb05-f3e3-4c54-b85c-96d642073313",
                    "LayerId": "a99bc318-470f-4278-84f5-d75f2a036bc2"
                }
            ]
        },
        {
            "id": "3dcfaee1-d4e0-46db-96c1-a10afea2b0e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "compositeImage": {
                "id": "ad9c9ab1-54cd-4528-8a6f-2a55c1a66ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dcfaee1-d4e0-46db-96c1-a10afea2b0e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9270fbe-8916-4f02-979e-d1ae5d0a9862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dcfaee1-d4e0-46db-96c1-a10afea2b0e9",
                    "LayerId": "a99bc318-470f-4278-84f5-d75f2a036bc2"
                }
            ]
        },
        {
            "id": "ee8a6f9c-4910-475d-8b06-093f6b2277d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "compositeImage": {
                "id": "d1433cbf-5d27-485b-ad1e-cfae90a5ff71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee8a6f9c-4910-475d-8b06-093f6b2277d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa10636-2ea1-4048-8080-f983120e31a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee8a6f9c-4910-475d-8b06-093f6b2277d3",
                    "LayerId": "a99bc318-470f-4278-84f5-d75f2a036bc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a99bc318-470f-4278-84f5-d75f2a036bc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "127047d8-9bce-4fd0-a298-e039f2055756",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}