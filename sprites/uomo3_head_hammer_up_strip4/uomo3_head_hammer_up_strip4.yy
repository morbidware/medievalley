{
    "id": "1e6db5e4-4f50-49de-88b5-1c4efba9d136",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f72e573-ab82-48c7-ac95-8a395dfa8014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6db5e4-4f50-49de-88b5-1c4efba9d136",
            "compositeImage": {
                "id": "ae53c63f-3446-487c-a874-cc7984897b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f72e573-ab82-48c7-ac95-8a395dfa8014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c18414d-c1c7-469c-8e13-5c763a84a7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f72e573-ab82-48c7-ac95-8a395dfa8014",
                    "LayerId": "f63adfac-941d-4def-a910-bcb728b4feea"
                }
            ]
        },
        {
            "id": "d06253d1-de01-43e0-aaab-b50b1a9ca5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6db5e4-4f50-49de-88b5-1c4efba9d136",
            "compositeImage": {
                "id": "7812c96d-5b94-43d5-903a-be606793d8a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06253d1-de01-43e0-aaab-b50b1a9ca5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "318f47f3-6ff4-4349-9820-d77317aa7500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06253d1-de01-43e0-aaab-b50b1a9ca5d9",
                    "LayerId": "f63adfac-941d-4def-a910-bcb728b4feea"
                }
            ]
        },
        {
            "id": "b1e3592f-7b81-4150-806e-ea22c19290fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6db5e4-4f50-49de-88b5-1c4efba9d136",
            "compositeImage": {
                "id": "50b27c0a-befd-46f3-aa8b-a58b1c3ade56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e3592f-7b81-4150-806e-ea22c19290fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19114d6c-ba35-48b6-bbe6-317945db2c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e3592f-7b81-4150-806e-ea22c19290fe",
                    "LayerId": "f63adfac-941d-4def-a910-bcb728b4feea"
                }
            ]
        },
        {
            "id": "3569a092-1003-40c4-985d-452810e95960",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6db5e4-4f50-49de-88b5-1c4efba9d136",
            "compositeImage": {
                "id": "49129f48-c009-47dd-9336-32581043b81a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3569a092-1003-40c4-985d-452810e95960",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7148522-5863-49b7-a222-654c1fba4798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3569a092-1003-40c4-985d-452810e95960",
                    "LayerId": "f63adfac-941d-4def-a910-bcb728b4feea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f63adfac-941d-4def-a910-bcb728b4feea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e6db5e4-4f50-49de-88b5-1c4efba9d136",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}