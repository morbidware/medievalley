{
    "id": "f19f370e-dfbb-42d6-902c-7b7d2370f752",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3252d402-5136-46e0-9a06-3a479ec84c84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f19f370e-dfbb-42d6-902c-7b7d2370f752",
            "compositeImage": {
                "id": "34ef38e4-3afa-446d-ad47-dd01de200548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3252d402-5136-46e0-9a06-3a479ec84c84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74b8c1d3-24f7-4be9-9114-691a5e4626a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3252d402-5136-46e0-9a06-3a479ec84c84",
                    "LayerId": "cff7ee76-bb14-4e80-8dbc-9ee723bf237a"
                }
            ]
        },
        {
            "id": "566c835f-c3c4-4414-b8a7-81362e658ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f19f370e-dfbb-42d6-902c-7b7d2370f752",
            "compositeImage": {
                "id": "8f1cb670-5f5f-4dbc-85b5-3b05a9988c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "566c835f-c3c4-4414-b8a7-81362e658ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "418e8130-41e4-475d-9cbb-d5ef2db2fedb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "566c835f-c3c4-4414-b8a7-81362e658ca8",
                    "LayerId": "cff7ee76-bb14-4e80-8dbc-9ee723bf237a"
                }
            ]
        },
        {
            "id": "8837f42c-d2ac-4861-bb70-7c77c34e3c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f19f370e-dfbb-42d6-902c-7b7d2370f752",
            "compositeImage": {
                "id": "a52b6772-5e56-458f-a0c9-e629cc67befc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8837f42c-d2ac-4861-bb70-7c77c34e3c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4135041e-cf30-4122-96ef-945db63b47c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8837f42c-d2ac-4861-bb70-7c77c34e3c83",
                    "LayerId": "cff7ee76-bb14-4e80-8dbc-9ee723bf237a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "cff7ee76-bb14-4e80-8dbc-9ee723bf237a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f19f370e-dfbb-42d6-902c-7b7d2370f752",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 6
}