{
    "id": "b7e33d98-0364-4dcb-a2e9-1b750f9679de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_gethit_up_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffdfcffb-e8d4-4f9d-827c-a439ec04142b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7e33d98-0364-4dcb-a2e9-1b750f9679de",
            "compositeImage": {
                "id": "7da37a84-8e41-40fe-b35b-646697c29b7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffdfcffb-e8d4-4f9d-827c-a439ec04142b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd8e8e67-444e-4e11-bda6-88fae296f1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffdfcffb-e8d4-4f9d-827c-a439ec04142b",
                    "LayerId": "cae3c08a-88ed-4d03-bf4e-25e656655265"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cae3c08a-88ed-4d03-bf4e-25e656655265",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7e33d98-0364-4dcb-a2e9-1b750f9679de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}