{
    "id": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_sugarbeet_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5738d35-2eee-4be6-b5b7-eb417aa07441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "compositeImage": {
                "id": "d5a5bd22-1a2b-4540-b6a6-eab5a4019bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5738d35-2eee-4be6-b5b7-eb417aa07441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00905b24-de09-4dfe-8f07-f2cec72417b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5738d35-2eee-4be6-b5b7-eb417aa07441",
                    "LayerId": "48a8d94f-2235-406c-b679-13c3e53e51c8"
                }
            ]
        },
        {
            "id": "1e2e97ce-2752-497e-99b9-55a94f129ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "compositeImage": {
                "id": "8715840c-5637-420f-93d4-4b3c67f7c551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2e97ce-2752-497e-99b9-55a94f129ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7262f4a3-b63a-4803-9782-e3f1dab06e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2e97ce-2752-497e-99b9-55a94f129ff4",
                    "LayerId": "48a8d94f-2235-406c-b679-13c3e53e51c8"
                }
            ]
        },
        {
            "id": "ed2e74cc-1656-48fb-a94e-845d3fd07374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "compositeImage": {
                "id": "99706292-4b46-459d-be87-c5acaae22246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2e74cc-1656-48fb-a94e-845d3fd07374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29df1f95-ba5e-435d-b368-52a207b03acc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2e74cc-1656-48fb-a94e-845d3fd07374",
                    "LayerId": "48a8d94f-2235-406c-b679-13c3e53e51c8"
                }
            ]
        },
        {
            "id": "4a18183c-1d27-4d32-90da-98b5603a50d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "compositeImage": {
                "id": "473c36d6-179b-443e-a507-8d276a7385a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a18183c-1d27-4d32-90da-98b5603a50d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63810cf-114a-4faa-a173-5e5e5589ed5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a18183c-1d27-4d32-90da-98b5603a50d2",
                    "LayerId": "48a8d94f-2235-406c-b679-13c3e53e51c8"
                }
            ]
        },
        {
            "id": "542d9f5b-8633-48e7-bb20-fb38afc7a527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "compositeImage": {
                "id": "b68f1141-b3f7-48c8-9fa7-4fa09b8ef7d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "542d9f5b-8633-48e7-bb20-fb38afc7a527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbcb7432-0bda-4ff3-943e-5d477b09ae32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "542d9f5b-8633-48e7-bb20-fb38afc7a527",
                    "LayerId": "48a8d94f-2235-406c-b679-13c3e53e51c8"
                }
            ]
        },
        {
            "id": "a1616936-906a-422a-8cbf-56b8ff6b843e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "compositeImage": {
                "id": "9330124c-1921-431d-ba04-cf566ca054dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1616936-906a-422a-8cbf-56b8ff6b843e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c29bcb-e372-4d7f-83dc-6c2d63f193cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1616936-906a-422a-8cbf-56b8ff6b843e",
                    "LayerId": "48a8d94f-2235-406c-b679-13c3e53e51c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48a8d94f-2235-406c-b679-13c3e53e51c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5884add2-f0d7-4ace-8798-aff0f2569bf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 6,
    "yorig": 28
}