{
    "id": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mask_32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82f34e08-bf90-4470-bbba-72045ad171ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
            "compositeImage": {
                "id": "160f82d6-b9d1-47da-b935-2ed4c7087c54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f34e08-bf90-4470-bbba-72045ad171ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fc56d7a-fd0e-4030-ab31-5076d1f2fbf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f34e08-bf90-4470-bbba-72045ad171ba",
                    "LayerId": "3fd0eb53-bdb9-468b-8cb5-1720a9ad4633"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3fd0eb53-bdb9-468b-8cb5-1720a9ad4633",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3783f57b-fc86-495e-bbcd-d177c7ac78b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}