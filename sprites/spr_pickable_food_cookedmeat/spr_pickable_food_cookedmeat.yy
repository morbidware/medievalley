{
    "id": "5af22722-ced4-43ad-88d3-cd9f7848ce2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_food_cookedmeat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c86945d-c469-4f1b-93c7-c34f9d41df44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af22722-ced4-43ad-88d3-cd9f7848ce2d",
            "compositeImage": {
                "id": "5dd81248-4981-47e0-9b10-64a56ba93613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c86945d-c469-4f1b-93c7-c34f9d41df44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c00ed3c-d38f-4a47-a8ec-400b195f8102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c86945d-c469-4f1b-93c7-c34f9d41df44",
                    "LayerId": "d8b2a791-9fdd-4ba2-a6d3-7ac8de1741a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d8b2a791-9fdd-4ba2-a6d3-7ac8de1741a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5af22722-ced4-43ad-88d3-cd9f7848ce2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}