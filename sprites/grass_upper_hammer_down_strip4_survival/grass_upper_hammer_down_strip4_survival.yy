{
    "id": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_hammer_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9738f3b1-e4b5-4ab1-b9f4-283cb911247b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "compositeImage": {
                "id": "11d6f83f-68d3-4c82-860b-ef450dba345c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9738f3b1-e4b5-4ab1-b9f4-283cb911247b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29af7efe-da67-4414-8c2d-c440da8c8e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9738f3b1-e4b5-4ab1-b9f4-283cb911247b",
                    "LayerId": "f69f96bb-4c41-4380-a220-d24ffcd38792"
                }
            ]
        },
        {
            "id": "f32f4603-9fc1-4710-84dc-044438a61145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "compositeImage": {
                "id": "46d593c4-7975-49e1-9299-ebdb2edb4a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f32f4603-9fc1-4710-84dc-044438a61145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64cd876b-d3dc-40e9-aaec-6bcaa3ec7ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f32f4603-9fc1-4710-84dc-044438a61145",
                    "LayerId": "f69f96bb-4c41-4380-a220-d24ffcd38792"
                }
            ]
        },
        {
            "id": "041a5fb1-d2fe-48ad-9dac-859520614715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "compositeImage": {
                "id": "033ad991-2f5a-41a0-96df-b55f1c31243d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "041a5fb1-d2fe-48ad-9dac-859520614715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8813901d-8243-478d-a066-98673549cb2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "041a5fb1-d2fe-48ad-9dac-859520614715",
                    "LayerId": "f69f96bb-4c41-4380-a220-d24ffcd38792"
                }
            ]
        },
        {
            "id": "e1aa561b-3e6c-469b-a2f6-9f058948ccd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "compositeImage": {
                "id": "2419eda6-9fa1-4213-8721-49b64ed57f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1aa561b-3e6c-469b-a2f6-9f058948ccd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3395fe8-7038-4a26-829a-97a4b5e41a9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1aa561b-3e6c-469b-a2f6-9f058948ccd5",
                    "LayerId": "f69f96bb-4c41-4380-a220-d24ffcd38792"
                }
            ]
        },
        {
            "id": "1eea1ec5-45aa-4ec3-931d-38d8bec583de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "compositeImage": {
                "id": "2da7f1aa-9b7b-44d7-b83e-a10d3e78a0e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eea1ec5-45aa-4ec3-931d-38d8bec583de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691f4407-c01f-43e3-b41e-d46e643bb4bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eea1ec5-45aa-4ec3-931d-38d8bec583de",
                    "LayerId": "f69f96bb-4c41-4380-a220-d24ffcd38792"
                }
            ]
        },
        {
            "id": "b7f016f1-1cb0-4d03-b3f2-6b8a17583362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "compositeImage": {
                "id": "1fa88c87-23c3-4342-9706-12fc48afd53f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f016f1-1cb0-4d03-b3f2-6b8a17583362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1696c262-5e4f-49aa-b4c1-ced086b980ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f016f1-1cb0-4d03-b3f2-6b8a17583362",
                    "LayerId": "f69f96bb-4c41-4380-a220-d24ffcd38792"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f69f96bb-4c41-4380-a220-d24ffcd38792",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4eb38431-a0da-4c90-8a76-44ac91973ed3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}