{
    "id": "7063bfd7-d4ee-4545-bf21-cab4afbf566e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf21db16-ff5b-49e3-a01e-6c34963b5d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7063bfd7-d4ee-4545-bf21-cab4afbf566e",
            "compositeImage": {
                "id": "676548cc-4269-46ff-8d31-1ea0ad4b8f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf21db16-ff5b-49e3-a01e-6c34963b5d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a28cf6ed-ff9f-433e-b2a4-098aee407f94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf21db16-ff5b-49e3-a01e-6c34963b5d19",
                    "LayerId": "b2634488-6bb1-4556-ae07-e88bfaa27423"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2634488-6bb1-4556-ae07-e88bfaa27423",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7063bfd7-d4ee-4545-bf21-cab4afbf566e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}