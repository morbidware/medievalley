{
    "id": "1c793df9-2ec8-402b-b5a4-669c42a4a185",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_exit_fullscreen_old",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a33c5397-4da9-4f25-a12f-ba2d64cd798b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c793df9-2ec8-402b-b5a4-669c42a4a185",
            "compositeImage": {
                "id": "d865fb89-dd96-40ba-bf9d-40a9aa786d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a33c5397-4da9-4f25-a12f-ba2d64cd798b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bd58d9d-0137-4dfd-8e09-f816a9ecf623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33c5397-4da9-4f25-a12f-ba2d64cd798b",
                    "LayerId": "59b351ba-9ef7-42fd-9a52-cd2dbd69878d"
                },
                {
                    "id": "753d88df-7aa9-40e3-964b-895a8c1bbe3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a33c5397-4da9-4f25-a12f-ba2d64cd798b",
                    "LayerId": "b279e4d7-36cb-4534-af59-f2268fbfa858"
                }
            ]
        },
        {
            "id": "8882d5c3-b036-41ec-a58b-7f623bf7aed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c793df9-2ec8-402b-b5a4-669c42a4a185",
            "compositeImage": {
                "id": "0a6e0c2f-08fd-4922-8a93-580a90369aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8882d5c3-b036-41ec-a58b-7f623bf7aed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56f4e2f1-1f41-4e7a-a226-e1845e8c39a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8882d5c3-b036-41ec-a58b-7f623bf7aed4",
                    "LayerId": "59b351ba-9ef7-42fd-9a52-cd2dbd69878d"
                },
                {
                    "id": "200e4ef2-3064-4406-adc7-bbdc1fea73ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8882d5c3-b036-41ec-a58b-7f623bf7aed4",
                    "LayerId": "b279e4d7-36cb-4534-af59-f2268fbfa858"
                }
            ]
        },
        {
            "id": "d38ea32a-cd4b-4938-be24-59537cd527e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c793df9-2ec8-402b-b5a4-669c42a4a185",
            "compositeImage": {
                "id": "bae3f8e2-c692-49b3-8f0d-2a8dac05ea51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d38ea32a-cd4b-4938-be24-59537cd527e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5f70ef-7d11-48d4-b171-b428a2715089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d38ea32a-cd4b-4938-be24-59537cd527e4",
                    "LayerId": "59b351ba-9ef7-42fd-9a52-cd2dbd69878d"
                },
                {
                    "id": "be31348b-66d1-4f15-8db2-ce729431e11c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d38ea32a-cd4b-4938-be24-59537cd527e4",
                    "LayerId": "b279e4d7-36cb-4534-af59-f2268fbfa858"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "59b351ba-9ef7-42fd-9a52-cd2dbd69878d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c793df9-2ec8-402b-b5a4-669c42a4a185",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b279e4d7-36cb-4534-af59-f2268fbfa858",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c793df9-2ec8-402b-b5a4-669c42a4a185",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 37,
    "yorig": 4
}