{
    "id": "e25bfb36-0ede-404c-864e-bb21e34f55b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a92883c8-0966-44b6-a315-e49464d8f9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e25bfb36-0ede-404c-864e-bb21e34f55b9",
            "compositeImage": {
                "id": "878ec629-d837-476f-88aa-c14dbae74c70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a92883c8-0966-44b6-a315-e49464d8f9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd5c73e-e1e1-4c8a-9258-16670fb3096f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a92883c8-0966-44b6-a315-e49464d8f9c8",
                    "LayerId": "1ff76df0-6389-4c01-a499-8d8c50743a9c"
                }
            ]
        },
        {
            "id": "dfe113a7-5290-4944-ba92-bca169215f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e25bfb36-0ede-404c-864e-bb21e34f55b9",
            "compositeImage": {
                "id": "76001186-7bab-43a2-aea8-d7045adbfad6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfe113a7-5290-4944-ba92-bca169215f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f931969-3d92-4418-9036-0aa42d51eb46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfe113a7-5290-4944-ba92-bca169215f6f",
                    "LayerId": "1ff76df0-6389-4c01-a499-8d8c50743a9c"
                }
            ]
        },
        {
            "id": "cf746cfb-3e32-4730-a32d-7f578a640251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e25bfb36-0ede-404c-864e-bb21e34f55b9",
            "compositeImage": {
                "id": "86917123-0de9-4c4a-aaf7-19ab99abda04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf746cfb-3e32-4730-a32d-7f578a640251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aba9f2a-1814-4386-bed7-03304254ee52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf746cfb-3e32-4730-a32d-7f578a640251",
                    "LayerId": "1ff76df0-6389-4c01-a499-8d8c50743a9c"
                }
            ]
        },
        {
            "id": "1b254750-430a-4545-a427-0375990d4661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e25bfb36-0ede-404c-864e-bb21e34f55b9",
            "compositeImage": {
                "id": "ba0e0e25-aa27-4e9f-9956-73f23cec4393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b254750-430a-4545-a427-0375990d4661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129cab66-32a5-4442-86f4-c08bd9070d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b254750-430a-4545-a427-0375990d4661",
                    "LayerId": "1ff76df0-6389-4c01-a499-8d8c50743a9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ff76df0-6389-4c01-a499-8d8c50743a9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e25bfb36-0ede-404c-864e-bb21e34f55b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}