{
    "id": "61804e57-55f4-4880-a549-2d570983bf71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_status_strength_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a135bb7d-3d58-4c21-85fd-8c0de8f9faf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61804e57-55f4-4880-a549-2d570983bf71",
            "compositeImage": {
                "id": "97aaea62-0212-4dd2-9d26-2b3cccab967b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a135bb7d-3d58-4c21-85fd-8c0de8f9faf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1fad384-c2a0-4717-a1cc-430fb2e680d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a135bb7d-3d58-4c21-85fd-8c0de8f9faf3",
                    "LayerId": "6d9c5f71-d84a-4255-ad4e-c2c7ead4cf05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "6d9c5f71-d84a-4255-ad4e-c2c7ead4cf05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61804e57-55f4-4880-a549-2d570983bf71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}