{
    "id": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_walk_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5cfbdc1-ab90-4eba-9547-3a280d2d9296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "compositeImage": {
                "id": "c537daa0-b296-4e7f-ab5b-356851c2acb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cfbdc1-ab90-4eba-9547-3a280d2d9296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e495b49c-c8a3-45bf-a26e-7b143a9cc008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cfbdc1-ab90-4eba-9547-3a280d2d9296",
                    "LayerId": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d"
                }
            ]
        },
        {
            "id": "da042521-fa1b-4e9b-a6f3-05ac324eecfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "compositeImage": {
                "id": "e380c579-8892-4822-a7c7-29e7cae3807d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da042521-fa1b-4e9b-a6f3-05ac324eecfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "536ac70c-df77-441f-b3fc-92e139052752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da042521-fa1b-4e9b-a6f3-05ac324eecfb",
                    "LayerId": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d"
                }
            ]
        },
        {
            "id": "36fe032b-acb3-4235-913c-e95c54b16853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "compositeImage": {
                "id": "fdc9f58f-85e0-4138-8370-2f02f5fe5c61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36fe032b-acb3-4235-913c-e95c54b16853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bce3745e-66df-484b-8097-2a7f28cacfce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36fe032b-acb3-4235-913c-e95c54b16853",
                    "LayerId": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d"
                }
            ]
        },
        {
            "id": "811b9920-221e-4c00-9b00-e865df1a7305",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "compositeImage": {
                "id": "d28fbc23-d995-44ed-a9c1-69f779559622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811b9920-221e-4c00-9b00-e865df1a7305",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87609ab6-8e04-479b-a0b4-9a4e3b79338c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811b9920-221e-4c00-9b00-e865df1a7305",
                    "LayerId": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d"
                }
            ]
        },
        {
            "id": "b2bd01af-7570-4ed8-a2bd-b5c6a1f4db86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "compositeImage": {
                "id": "692b55f3-ddb2-4d8d-b67a-10efb12a46b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2bd01af-7570-4ed8-a2bd-b5c6a1f4db86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d244f6d0-6bcb-4f2d-a41f-d47a26978a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2bd01af-7570-4ed8-a2bd-b5c6a1f4db86",
                    "LayerId": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d"
                }
            ]
        },
        {
            "id": "bf9a627f-ea9f-42b0-9ce8-0218e6106e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "compositeImage": {
                "id": "0292d3db-8784-400f-9918-94614fe9c7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf9a627f-ea9f-42b0-9ce8-0218e6106e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e02a81-3afe-405b-bba5-a96d7cb4a65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf9a627f-ea9f-42b0-9ce8-0218e6106e7a",
                    "LayerId": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "845f1998-b51f-44d2-b7ad-d9d6c9f3088d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57b90665-7523-4f46-8c7f-3fc7a13afbd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}