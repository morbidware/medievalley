{
    "id": "c7531d98-255f-4dc0-805b-24a2758f3b41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_die_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b876ff61-e2f2-43f7-8411-208567c35f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7531d98-255f-4dc0-805b-24a2758f3b41",
            "compositeImage": {
                "id": "fd1f9018-2115-44ba-a883-98992fd007b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b876ff61-e2f2-43f7-8411-208567c35f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "044a44b2-14c2-4628-a466-3a626d9c109c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b876ff61-e2f2-43f7-8411-208567c35f0f",
                    "LayerId": "8345a2ad-59d9-4759-9211-6557f3e92de4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "8345a2ad-59d9-4759-9211-6557f3e92de4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7531d98-255f-4dc0-805b-24a2758f3b41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}