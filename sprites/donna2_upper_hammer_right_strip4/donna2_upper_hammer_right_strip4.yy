{
    "id": "215c9363-9ee2-4f7f-bfe9-e373a0765095",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5de0c4f-78f4-4daa-ba31-245fe217a207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "215c9363-9ee2-4f7f-bfe9-e373a0765095",
            "compositeImage": {
                "id": "a976587b-1422-4436-9e57-2d27b0265235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5de0c4f-78f4-4daa-ba31-245fe217a207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9be257-2d6a-464b-83dc-c3df689dc5f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5de0c4f-78f4-4daa-ba31-245fe217a207",
                    "LayerId": "c4564d4a-6e35-452c-93af-abfbc0183dba"
                }
            ]
        },
        {
            "id": "168c2415-fbad-4a14-b414-2d2960408007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "215c9363-9ee2-4f7f-bfe9-e373a0765095",
            "compositeImage": {
                "id": "b09000a6-e8d5-4285-bfb4-5ea77c07fd8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "168c2415-fbad-4a14-b414-2d2960408007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb36221-f92e-472c-9a43-c677bff76c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "168c2415-fbad-4a14-b414-2d2960408007",
                    "LayerId": "c4564d4a-6e35-452c-93af-abfbc0183dba"
                }
            ]
        },
        {
            "id": "a22ee894-54b3-4512-aafc-53574061e9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "215c9363-9ee2-4f7f-bfe9-e373a0765095",
            "compositeImage": {
                "id": "1bbc4c21-0598-4d98-a8e3-3cb8eb6649b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22ee894-54b3-4512-aafc-53574061e9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e71eec0a-ef7d-4ffa-9c0c-7467cf93aa31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22ee894-54b3-4512-aafc-53574061e9d0",
                    "LayerId": "c4564d4a-6e35-452c-93af-abfbc0183dba"
                }
            ]
        },
        {
            "id": "d8bd8670-0d29-4949-9e56-e8174bf742d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "215c9363-9ee2-4f7f-bfe9-e373a0765095",
            "compositeImage": {
                "id": "3de80fae-2348-4eb6-93b1-9ac1070ad8bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8bd8670-0d29-4949-9e56-e8174bf742d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "785f8d54-7fc2-4950-93f7-76a3b993e7cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8bd8670-0d29-4949-9e56-e8174bf742d8",
                    "LayerId": "c4564d4a-6e35-452c-93af-abfbc0183dba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c4564d4a-6e35-452c-93af-abfbc0183dba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "215c9363-9ee2-4f7f-bfe9-e373a0765095",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}