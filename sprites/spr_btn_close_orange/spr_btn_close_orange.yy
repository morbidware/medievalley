{
    "id": "348fc906-1b4f-4117-819d-d2ff3801aff8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_close_orange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f18a0b60-f644-448d-8d08-e7c6085e816f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "348fc906-1b4f-4117-819d-d2ff3801aff8",
            "compositeImage": {
                "id": "6f1e67be-d8ac-44ea-9343-894a228dce1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18a0b60-f644-448d-8d08-e7c6085e816f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5271bb84-bd06-4454-aa36-89b6836ff888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18a0b60-f644-448d-8d08-e7c6085e816f",
                    "LayerId": "ca1524a7-d0b0-4e69-a75c-122f1d1c3503"
                }
            ]
        },
        {
            "id": "0a3eef17-0a78-42f1-99e4-060731a37981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "348fc906-1b4f-4117-819d-d2ff3801aff8",
            "compositeImage": {
                "id": "e9f9be0f-dbcc-4117-a5ca-a2c5b6300efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a3eef17-0a78-42f1-99e4-060731a37981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d17741-3ff8-4004-a334-88d2cac5a07e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a3eef17-0a78-42f1-99e4-060731a37981",
                    "LayerId": "ca1524a7-d0b0-4e69-a75c-122f1d1c3503"
                }
            ]
        },
        {
            "id": "9c75c91b-4c65-4f4e-9045-64fa5a6123c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "348fc906-1b4f-4117-819d-d2ff3801aff8",
            "compositeImage": {
                "id": "eab7faef-c229-4104-bd23-12f4eff7bcb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c75c91b-4c65-4f4e-9045-64fa5a6123c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e82726-d642-4bd7-acdd-1ab3aa64a560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c75c91b-4c65-4f4e-9045-64fa5a6123c1",
                    "LayerId": "ca1524a7-d0b0-4e69-a75c-122f1d1c3503"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ca1524a7-d0b0-4e69-a75c-122f1d1c3503",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "348fc906-1b4f-4117-819d-d2ff3801aff8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}