{
    "id": "5a5d9b6f-dbb1-413a-a5b0-5bc0510dc7ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3078a714-8475-417e-84be-5832e9609161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5d9b6f-dbb1-413a-a5b0-5bc0510dc7ca",
            "compositeImage": {
                "id": "ac93b6b3-61e9-44c2-a429-77897fae3017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3078a714-8475-417e-84be-5832e9609161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac9eb8d1-18ad-4d9c-a787-6380fdf95364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3078a714-8475-417e-84be-5832e9609161",
                    "LayerId": "7b0e207a-d756-4e72-a124-f6c47a735e0e"
                }
            ]
        },
        {
            "id": "b4a43ee2-a42b-411b-9320-2eaee0f8e76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5d9b6f-dbb1-413a-a5b0-5bc0510dc7ca",
            "compositeImage": {
                "id": "cbdd0777-5b1d-4b84-ae9e-ba8256f32bff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a43ee2-a42b-411b-9320-2eaee0f8e76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c361aab9-3672-428f-b022-210820e0e0c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a43ee2-a42b-411b-9320-2eaee0f8e76f",
                    "LayerId": "7b0e207a-d756-4e72-a124-f6c47a735e0e"
                }
            ]
        },
        {
            "id": "8f60f805-6308-47bf-bd53-314068c702d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5d9b6f-dbb1-413a-a5b0-5bc0510dc7ca",
            "compositeImage": {
                "id": "7b3a3837-8e5a-434c-a8d9-4e5d6c9d0cc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f60f805-6308-47bf-bd53-314068c702d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaab7331-6391-4d93-a154-3bf09e4fab19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f60f805-6308-47bf-bd53-314068c702d6",
                    "LayerId": "7b0e207a-d756-4e72-a124-f6c47a735e0e"
                }
            ]
        },
        {
            "id": "70262c86-d29d-4b67-a239-0e3cf40e9bc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a5d9b6f-dbb1-413a-a5b0-5bc0510dc7ca",
            "compositeImage": {
                "id": "e8cf5b7a-febc-4cf4-8df0-0f7f9110506f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70262c86-d29d-4b67-a239-0e3cf40e9bc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1c259d8-bd67-46de-a03a-90d363fb0206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70262c86-d29d-4b67-a239-0e3cf40e9bc0",
                    "LayerId": "7b0e207a-d756-4e72-a124-f6c47a735e0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7b0e207a-d756-4e72-a124-f6c47a735e0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a5d9b6f-dbb1-413a-a5b0-5bc0510dc7ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}