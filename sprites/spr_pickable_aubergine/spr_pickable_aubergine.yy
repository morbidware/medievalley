{
    "id": "19b48f74-58e7-4714-aea2-0f0a04365000",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_aubergine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60aa8769-edb9-4344-a4f6-b4acdeadc522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19b48f74-58e7-4714-aea2-0f0a04365000",
            "compositeImage": {
                "id": "11af281f-1d2b-42ea-86d3-65f4214b2ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60aa8769-edb9-4344-a4f6-b4acdeadc522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e066b1-31eb-4178-851e-a5caba8065b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60aa8769-edb9-4344-a4f6-b4acdeadc522",
                    "LayerId": "f417dd79-9bb2-4427-936f-4163e886aee3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f417dd79-9bb2-4427-936f-4163e886aee3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19b48f74-58e7-4714-aea2-0f0a04365000",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}