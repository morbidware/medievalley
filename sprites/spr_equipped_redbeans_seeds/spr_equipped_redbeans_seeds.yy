{
    "id": "83450a45-79ae-437c-8d77-fe1760ada718",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_redbeans_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8f2f016-aa02-4ab5-9791-46926dfa9370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83450a45-79ae-437c-8d77-fe1760ada718",
            "compositeImage": {
                "id": "d76ebada-42d1-4218-befd-a11a843dd9b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f2f016-aa02-4ab5-9791-46926dfa9370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb16c35c-54df-449d-8f63-f260981d7710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f2f016-aa02-4ab5-9791-46926dfa9370",
                    "LayerId": "915ec12b-e6bf-40b9-91c3-79549d4638f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "915ec12b-e6bf-40b9-91c3-79549d4638f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83450a45-79ae-437c-8d77-fe1760ada718",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}