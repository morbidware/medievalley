{
    "id": "fdd1e03a-a618-43a9-9362-512bd2e19c8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_sword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0a1c40f-1a83-4fe3-84a7-e2d0de1f03ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdd1e03a-a618-43a9-9362-512bd2e19c8e",
            "compositeImage": {
                "id": "44d607fb-75e2-4cef-b1ad-575d16489f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0a1c40f-1a83-4fe3-84a7-e2d0de1f03ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c06dd33-43ca-47e6-a640-5484b6858ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0a1c40f-1a83-4fe3-84a7-e2d0de1f03ad",
                    "LayerId": "f4d8cac7-856d-4bdc-8fe2-acdac3b23c72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f4d8cac7-856d-4bdc-8fe2-acdac3b23c72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdd1e03a-a618-43a9-9362-512bd2e19c8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}