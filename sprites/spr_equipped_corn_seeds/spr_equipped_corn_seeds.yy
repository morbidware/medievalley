{
    "id": "4346195b-69c0-4267-857f-2768a64e16f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_corn_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c747443d-9245-4adc-97d3-bae8620332a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4346195b-69c0-4267-857f-2768a64e16f1",
            "compositeImage": {
                "id": "7f255748-3c97-425d-9a3d-730c295f4330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c747443d-9245-4adc-97d3-bae8620332a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34dfa151-792c-41aa-8a39-44eeb51945c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c747443d-9245-4adc-97d3-bae8620332a5",
                    "LayerId": "9a853807-7642-4515-9e9d-e78d53ece19c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9a853807-7642-4515-9e9d-e78d53ece19c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4346195b-69c0-4267-857f-2768a64e16f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}