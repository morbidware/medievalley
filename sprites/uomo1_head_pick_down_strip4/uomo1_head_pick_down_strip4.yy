{
    "id": "eb1e8271-dd01-4a13-aa4d-51b00024bc69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1066ff07-b316-48df-82ac-88924eb8aecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb1e8271-dd01-4a13-aa4d-51b00024bc69",
            "compositeImage": {
                "id": "e5328ba7-1672-4b2c-827a-cef98cf4f76c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1066ff07-b316-48df-82ac-88924eb8aecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "193241b9-bc11-4fef-90bf-71873fd5d7b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1066ff07-b316-48df-82ac-88924eb8aecc",
                    "LayerId": "4579ba8e-949d-4c9d-a179-6fee7f52d6ea"
                }
            ]
        },
        {
            "id": "7162a6c5-4e90-4d44-98d3-ec7a3a9b77b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb1e8271-dd01-4a13-aa4d-51b00024bc69",
            "compositeImage": {
                "id": "49946951-9790-474c-88c5-b774533dab3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7162a6c5-4e90-4d44-98d3-ec7a3a9b77b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aeabeba-eda8-43d6-bab0-da334bbf658b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7162a6c5-4e90-4d44-98d3-ec7a3a9b77b0",
                    "LayerId": "4579ba8e-949d-4c9d-a179-6fee7f52d6ea"
                }
            ]
        },
        {
            "id": "2b02f999-afdb-49e5-a5ac-dc76b517ccfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb1e8271-dd01-4a13-aa4d-51b00024bc69",
            "compositeImage": {
                "id": "eb6f9e8c-a41b-4f80-937c-0b912406a04c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b02f999-afdb-49e5-a5ac-dc76b517ccfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a075f4d-a424-4d61-8b20-f718bf35f0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b02f999-afdb-49e5-a5ac-dc76b517ccfb",
                    "LayerId": "4579ba8e-949d-4c9d-a179-6fee7f52d6ea"
                }
            ]
        },
        {
            "id": "418fbd8b-4f58-4cc2-bdfc-3016b0df3810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb1e8271-dd01-4a13-aa4d-51b00024bc69",
            "compositeImage": {
                "id": "af413710-1212-40db-b8a2-473d34dce3f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "418fbd8b-4f58-4cc2-bdfc-3016b0df3810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f0ddbd-3ce4-47b3-b992-2ef8f750b1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "418fbd8b-4f58-4cc2-bdfc-3016b0df3810",
                    "LayerId": "4579ba8e-949d-4c9d-a179-6fee7f52d6ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4579ba8e-949d-4c9d-a179-6fee7f52d6ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb1e8271-dd01-4a13-aa4d-51b00024bc69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}