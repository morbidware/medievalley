{
    "id": "1e3c5db7-82b6-4c70-9941-1c7667394a6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_carrot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d18acf7e-d4ad-46ac-b810-b4abfb956015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e3c5db7-82b6-4c70-9941-1c7667394a6f",
            "compositeImage": {
                "id": "38cfdeb7-adc7-408e-9564-3413027013a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18acf7e-d4ad-46ac-b810-b4abfb956015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8723419-89ac-4ea4-8545-d625b40c4950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18acf7e-d4ad-46ac-b810-b4abfb956015",
                    "LayerId": "200ffa8e-081f-47f4-8c5e-5277ab4dddf0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "200ffa8e-081f-47f4-8c5e-5277ab4dddf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e3c5db7-82b6-4c70-9941-1c7667394a6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}