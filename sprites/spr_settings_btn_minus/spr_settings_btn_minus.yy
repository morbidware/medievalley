{
    "id": "b12de68b-60b4-4c58-ab5c-8485a11326ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_btn_minus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69148cb3-dc61-44f4-9ec1-f4b51d6381dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b12de68b-60b4-4c58-ab5c-8485a11326ed",
            "compositeImage": {
                "id": "f96e52fa-c970-4d9f-bea1-d684287080b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69148cb3-dc61-44f4-9ec1-f4b51d6381dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1c868d-dfb5-42a3-a043-61efbbb1837c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69148cb3-dc61-44f4-9ec1-f4b51d6381dd",
                    "LayerId": "44954d89-0e6f-4baf-a37e-8e493d2f66cd"
                }
            ]
        },
        {
            "id": "bce368f2-018e-46f7-9c31-564522d66a03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b12de68b-60b4-4c58-ab5c-8485a11326ed",
            "compositeImage": {
                "id": "c772c044-11e1-4cd4-a6aa-2845714dc184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bce368f2-018e-46f7-9c31-564522d66a03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "945a6fba-1918-45af-a257-62dc714945c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bce368f2-018e-46f7-9c31-564522d66a03",
                    "LayerId": "44954d89-0e6f-4baf-a37e-8e493d2f66cd"
                }
            ]
        },
        {
            "id": "20b15ae4-ebc3-4c11-8b1d-d39b2707f307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b12de68b-60b4-4c58-ab5c-8485a11326ed",
            "compositeImage": {
                "id": "3e12bfe3-2dcf-4746-9b35-df35c7ecd144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b15ae4-ebc3-4c11-8b1d-d39b2707f307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c56d1e3f-74cf-48cb-bbb2-577377102197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b15ae4-ebc3-4c11-8b1d-d39b2707f307",
                    "LayerId": "44954d89-0e6f-4baf-a37e-8e493d2f66cd"
                }
            ]
        },
        {
            "id": "fcd9c931-153e-4503-b219-4379d7a5b8fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b12de68b-60b4-4c58-ab5c-8485a11326ed",
            "compositeImage": {
                "id": "eddb4732-c7cb-4865-b24d-f2302b7371ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd9c931-153e-4503-b219-4379d7a5b8fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea323ae-dd81-4940-8905-1ab3b883c609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd9c931-153e-4503-b219-4379d7a5b8fa",
                    "LayerId": "44954d89-0e6f-4baf-a37e-8e493d2f66cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "44954d89-0e6f-4baf-a37e-8e493d2f66cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b12de68b-60b4-4c58-ab5c-8485a11326ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}