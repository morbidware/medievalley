{
    "id": "4318f9ff-e9b3-40ed-824f-84d9f553dabe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c66bc375-a3ea-4bc7-b406-1d67981f6984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4318f9ff-e9b3-40ed-824f-84d9f553dabe",
            "compositeImage": {
                "id": "4ff9bc22-a2af-4209-9494-e525a1196a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c66bc375-a3ea-4bc7-b406-1d67981f6984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e40e9807-b0b8-4d8c-825b-020b376d5399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c66bc375-a3ea-4bc7-b406-1d67981f6984",
                    "LayerId": "ddebd1f0-a0dd-4383-8ad0-c399b0694c79"
                }
            ]
        },
        {
            "id": "481fa510-d15f-4098-b5eb-d1d1ac71c85c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4318f9ff-e9b3-40ed-824f-84d9f553dabe",
            "compositeImage": {
                "id": "f6576b35-d243-4057-a968-908bb90c20ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "481fa510-d15f-4098-b5eb-d1d1ac71c85c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee38975-773a-4200-842b-3ab2152cb32e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481fa510-d15f-4098-b5eb-d1d1ac71c85c",
                    "LayerId": "ddebd1f0-a0dd-4383-8ad0-c399b0694c79"
                }
            ]
        },
        {
            "id": "7e137359-e3bf-497e-9863-cfcca5055d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4318f9ff-e9b3-40ed-824f-84d9f553dabe",
            "compositeImage": {
                "id": "a931b0c7-3f67-4cea-984d-6f5de6a9419d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e137359-e3bf-497e-9863-cfcca5055d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a763411e-73cf-4c79-ba06-9a27b65be250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e137359-e3bf-497e-9863-cfcca5055d50",
                    "LayerId": "ddebd1f0-a0dd-4383-8ad0-c399b0694c79"
                }
            ]
        },
        {
            "id": "5272332d-f609-4622-8891-9c6eb4e04b8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4318f9ff-e9b3-40ed-824f-84d9f553dabe",
            "compositeImage": {
                "id": "3d058092-1a7e-4e69-81af-daab1a15e0f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5272332d-f609-4622-8891-9c6eb4e04b8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d898fb6b-bfe1-45a7-a2be-7429e5254d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5272332d-f609-4622-8891-9c6eb4e04b8a",
                    "LayerId": "ddebd1f0-a0dd-4383-8ad0-c399b0694c79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ddebd1f0-a0dd-4383-8ad0-c399b0694c79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4318f9ff-e9b3-40ed-824f-84d9f553dabe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}