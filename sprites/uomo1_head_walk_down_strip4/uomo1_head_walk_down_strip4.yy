{
    "id": "e7eb5c0a-8a79-4b08-b2aa-d9b4d45d489d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a3e30d5-03f8-4f81-be02-67ed1f4078d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7eb5c0a-8a79-4b08-b2aa-d9b4d45d489d",
            "compositeImage": {
                "id": "981a70c6-7d8d-4f4d-a00c-cd15eb2bc6c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a3e30d5-03f8-4f81-be02-67ed1f4078d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b22221-d0ee-4b0c-b11a-bacf38027a59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a3e30d5-03f8-4f81-be02-67ed1f4078d1",
                    "LayerId": "690d12f7-6d24-44ce-abc8-38649c8eca87"
                }
            ]
        },
        {
            "id": "4c8dc3b1-6cf7-470f-b742-f1436c8147dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7eb5c0a-8a79-4b08-b2aa-d9b4d45d489d",
            "compositeImage": {
                "id": "d3bbc7b3-150d-4648-9e89-c3dce38ffe6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c8dc3b1-6cf7-470f-b742-f1436c8147dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c37d67-fc9f-48ee-80a1-68c349105c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c8dc3b1-6cf7-470f-b742-f1436c8147dd",
                    "LayerId": "690d12f7-6d24-44ce-abc8-38649c8eca87"
                }
            ]
        },
        {
            "id": "c59834c7-c60e-4350-ad92-50f4cfec2d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7eb5c0a-8a79-4b08-b2aa-d9b4d45d489d",
            "compositeImage": {
                "id": "34f6208e-9b9d-4f95-82a5-db30850f9f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c59834c7-c60e-4350-ad92-50f4cfec2d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25133cb-6da5-4a55-878d-79420f0e2f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c59834c7-c60e-4350-ad92-50f4cfec2d65",
                    "LayerId": "690d12f7-6d24-44ce-abc8-38649c8eca87"
                }
            ]
        },
        {
            "id": "e57064b7-4ec0-42f8-a65b-0b1afd7023f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7eb5c0a-8a79-4b08-b2aa-d9b4d45d489d",
            "compositeImage": {
                "id": "0c727b3a-92ea-422c-b62c-2dc4fb1ce9fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e57064b7-4ec0-42f8-a65b-0b1afd7023f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af30e7fc-9ef5-4d7a-9a9a-d2d82b1952a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e57064b7-4ec0-42f8-a65b-0b1afd7023f6",
                    "LayerId": "690d12f7-6d24-44ce-abc8-38649c8eca87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "690d12f7-6d24-44ce-abc8-38649c8eca87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7eb5c0a-8a79-4b08-b2aa-d9b4d45d489d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}