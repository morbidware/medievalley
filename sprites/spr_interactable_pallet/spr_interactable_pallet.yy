{
    "id": "bf5f8e8e-3a9a-453c-bfa0-efce6b91e31f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_pallet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 2,
    "bbox_right": 45,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdf43aa9-d8ca-42e0-9375-a0ff51b7275d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5f8e8e-3a9a-453c-bfa0-efce6b91e31f",
            "compositeImage": {
                "id": "829d6475-f1ac-43f8-9ae3-16a6643cd5a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdf43aa9-d8ca-42e0-9375-a0ff51b7275d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6035f25f-4166-4163-94ec-d6e4c56f4bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdf43aa9-d8ca-42e0-9375-a0ff51b7275d",
                    "LayerId": "0caa0678-e91d-4cea-9b0a-0046051024a9"
                }
            ]
        },
        {
            "id": "a7f88ac4-2ac0-4833-be5a-3a9001f80fd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf5f8e8e-3a9a-453c-bfa0-efce6b91e31f",
            "compositeImage": {
                "id": "05d8348d-7e05-4a2c-aea5-d4edeedf6a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f88ac4-2ac0-4833-be5a-3a9001f80fd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b70cb5b7-b91b-4356-9355-d6a33efb4e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f88ac4-2ac0-4833-be5a-3a9001f80fd0",
                    "LayerId": "0caa0678-e91d-4cea-9b0a-0046051024a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0caa0678-e91d-4cea-9b0a-0046051024a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf5f8e8e-3a9a-453c-bfa0-efce6b91e31f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}