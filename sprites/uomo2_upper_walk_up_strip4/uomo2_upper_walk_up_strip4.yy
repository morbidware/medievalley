{
    "id": "5025f84b-a29e-4930-925b-1c0b99a3660c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98fe1f7c-a0c2-4ed7-9728-5bebdfdf0a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5025f84b-a29e-4930-925b-1c0b99a3660c",
            "compositeImage": {
                "id": "0cc5c2c5-6e31-45d9-8ea2-ac47fd8f781f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98fe1f7c-a0c2-4ed7-9728-5bebdfdf0a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6904510-7e2d-4f88-b03d-72dba232efca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98fe1f7c-a0c2-4ed7-9728-5bebdfdf0a23",
                    "LayerId": "56a340a9-89d9-4b7c-a468-99ee150c36ea"
                }
            ]
        },
        {
            "id": "065318e4-4039-4a4a-8daa-1a7ce7e309d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5025f84b-a29e-4930-925b-1c0b99a3660c",
            "compositeImage": {
                "id": "7f178a4e-35e1-4c5f-ac6a-c0d2bbeaa90d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065318e4-4039-4a4a-8daa-1a7ce7e309d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11edfc7-d720-41c7-88e1-963cf3c357e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065318e4-4039-4a4a-8daa-1a7ce7e309d6",
                    "LayerId": "56a340a9-89d9-4b7c-a468-99ee150c36ea"
                }
            ]
        },
        {
            "id": "d61636b1-bb6c-4dce-8db1-6d7954045c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5025f84b-a29e-4930-925b-1c0b99a3660c",
            "compositeImage": {
                "id": "9c7eb522-56d7-4d1b-991d-1115746b386a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61636b1-bb6c-4dce-8db1-6d7954045c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb094767-2f63-45a7-b8c6-77e41220ee80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61636b1-bb6c-4dce-8db1-6d7954045c23",
                    "LayerId": "56a340a9-89d9-4b7c-a468-99ee150c36ea"
                }
            ]
        },
        {
            "id": "f415a292-46e0-412f-9a31-b9cc54efb7b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5025f84b-a29e-4930-925b-1c0b99a3660c",
            "compositeImage": {
                "id": "a6d41229-db17-4fbc-9802-6c1aa4b521ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f415a292-46e0-412f-9a31-b9cc54efb7b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c7c3009-6e30-4d9c-8b22-17a4af2a717c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f415a292-46e0-412f-9a31-b9cc54efb7b2",
                    "LayerId": "56a340a9-89d9-4b7c-a468-99ee150c36ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56a340a9-89d9-4b7c-a468-99ee150c36ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5025f84b-a29e-4930-925b-1c0b99a3660c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}