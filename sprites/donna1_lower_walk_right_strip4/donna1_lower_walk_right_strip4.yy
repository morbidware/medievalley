{
    "id": "6a4615be-c061-4ea9-b850-408dccdf49ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "811ec38b-b142-44df-85b1-49c05e12e316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a4615be-c061-4ea9-b850-408dccdf49ab",
            "compositeImage": {
                "id": "42d62a65-5229-4686-b620-c3b9781a817c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "811ec38b-b142-44df-85b1-49c05e12e316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7291f6d6-2ef7-460c-b351-154df38479bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "811ec38b-b142-44df-85b1-49c05e12e316",
                    "LayerId": "59bf35e3-0b27-493b-812b-cc944bc3cd8a"
                }
            ]
        },
        {
            "id": "233d99af-ddd6-45ea-973c-e1856e03cdc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a4615be-c061-4ea9-b850-408dccdf49ab",
            "compositeImage": {
                "id": "8761b1eb-822f-405a-9edf-3bb05e59297d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233d99af-ddd6-45ea-973c-e1856e03cdc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d72be745-8a09-40a6-84a3-8e13c0f8b3d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233d99af-ddd6-45ea-973c-e1856e03cdc8",
                    "LayerId": "59bf35e3-0b27-493b-812b-cc944bc3cd8a"
                }
            ]
        },
        {
            "id": "7c647d16-aec8-4dba-8dd6-df860bc879ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a4615be-c061-4ea9-b850-408dccdf49ab",
            "compositeImage": {
                "id": "e85d7e80-6fbd-44d3-a190-a12e8a302bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c647d16-aec8-4dba-8dd6-df860bc879ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6945ac2a-53ad-4229-8a21-ee7fac96ad23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c647d16-aec8-4dba-8dd6-df860bc879ca",
                    "LayerId": "59bf35e3-0b27-493b-812b-cc944bc3cd8a"
                }
            ]
        },
        {
            "id": "27b03c76-8d31-4de9-b933-33c980e17abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a4615be-c061-4ea9-b850-408dccdf49ab",
            "compositeImage": {
                "id": "b21a5b41-1047-43e3-ab4d-e8e206d2e089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27b03c76-8d31-4de9-b933-33c980e17abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cff41762-82a7-4387-997a-73707b985ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27b03c76-8d31-4de9-b933-33c980e17abe",
                    "LayerId": "59bf35e3-0b27-493b-812b-cc944bc3cd8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "59bf35e3-0b27-493b-812b-cc944bc3cd8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a4615be-c061-4ea9-b850-408dccdf49ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}