{
    "id": "27e51390-8917-4689-9e85-1a651ea773d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_forge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c347a15-4d48-4a4d-b160-aa3f7252a96f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27e51390-8917-4689-9e85-1a651ea773d4",
            "compositeImage": {
                "id": "92ba34c9-1ff3-4cc9-9c6a-b58f9b13eb03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c347a15-4d48-4a4d-b160-aa3f7252a96f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956be215-05c1-4786-85c7-77170b655091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c347a15-4d48-4a4d-b160-aa3f7252a96f",
                    "LayerId": "7d048ebf-9eca-49f7-b45d-eea086994278"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7d048ebf-9eca-49f7-b45d-eea086994278",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27e51390-8917-4689-9e85-1a651ea773d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 9,
    "yorig": 12
}