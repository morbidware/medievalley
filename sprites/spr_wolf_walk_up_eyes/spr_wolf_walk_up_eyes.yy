{
    "id": "48b8c687-ddc2-4cee-9c23-ee1d2fc765d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_walk_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cc4b4d0-a482-4d5f-96a9-e7c21a995ff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48b8c687-ddc2-4cee-9c23-ee1d2fc765d0",
            "compositeImage": {
                "id": "694304c4-37d2-4187-a06b-4e1a2a8de148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cc4b4d0-a482-4d5f-96a9-e7c21a995ff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc6dec6-e7b6-4d17-830a-31be8cf476b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cc4b4d0-a482-4d5f-96a9-e7c21a995ff6",
                    "LayerId": "1dc466a7-8a1c-4f06-a491-131d3e74f4b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "1dc466a7-8a1c-4f06-a491-131d3e74f4b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48b8c687-ddc2-4cee-9c23-ee1d2fc765d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}