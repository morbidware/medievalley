{
    "id": "5b5c5ff3-f7ce-483b-8625-243387f2ca68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_gethit_down_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "015620b3-d95f-4e04-8e85-6e637d5d4df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b5c5ff3-f7ce-483b-8625-243387f2ca68",
            "compositeImage": {
                "id": "5f231182-ebac-43d2-9e80-9461a297bd91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015620b3-d95f-4e04-8e85-6e637d5d4df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5741bb16-cfe4-44d2-b4a6-6652012fe83b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015620b3-d95f-4e04-8e85-6e637d5d4df1",
                    "LayerId": "c1a0f61a-83ee-4076-854f-7a82079fff62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1a0f61a-83ee-4076-854f-7a82079fff62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b5c5ff3-f7ce-483b-8625-243387f2ca68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}