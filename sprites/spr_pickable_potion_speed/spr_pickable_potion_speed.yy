{
    "id": "50076ae1-99b8-4b31-b048-36946ba03d20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potion_speed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4851766e-7a66-43f4-8dd7-260ce9315159",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50076ae1-99b8-4b31-b048-36946ba03d20",
            "compositeImage": {
                "id": "024756ec-dcb2-45d6-b91d-d759eef68c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4851766e-7a66-43f4-8dd7-260ce9315159",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312f6b74-e968-448a-9cec-4bd2d58ae16e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4851766e-7a66-43f4-8dd7-260ce9315159",
                    "LayerId": "635c9410-2e8a-4df3-b2e8-4745013cbfb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "635c9410-2e8a-4df3-b2e8-4745013cbfb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50076ae1-99b8-4b31-b048-36946ba03d20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}