{
    "id": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_hammer_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2ba32ee-f4ca-4ce0-865e-20dda9d30fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "compositeImage": {
                "id": "2e440375-32bc-471a-8302-5c0495e27efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ba32ee-f4ca-4ce0-865e-20dda9d30fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aa73c33-c204-45d2-b5de-629bad14c154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ba32ee-f4ca-4ce0-865e-20dda9d30fc3",
                    "LayerId": "6d93d29c-e342-41ff-a137-158c3c5e67a3"
                }
            ]
        },
        {
            "id": "488c5d94-7642-4a19-81b4-cf456eb20e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "compositeImage": {
                "id": "2469806e-48aa-4de6-804a-4d90ea5df15a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488c5d94-7642-4a19-81b4-cf456eb20e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab57054b-cbc4-40ac-9698-3ed7156778c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488c5d94-7642-4a19-81b4-cf456eb20e40",
                    "LayerId": "6d93d29c-e342-41ff-a137-158c3c5e67a3"
                }
            ]
        },
        {
            "id": "797c422f-5d79-48a0-ba1c-f580605535f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "compositeImage": {
                "id": "9767e37d-7e8d-4527-bec4-a825607e5aef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "797c422f-5d79-48a0-ba1c-f580605535f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9010278a-40c4-46ee-a59b-5618ff5ae7a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "797c422f-5d79-48a0-ba1c-f580605535f9",
                    "LayerId": "6d93d29c-e342-41ff-a137-158c3c5e67a3"
                }
            ]
        },
        {
            "id": "047ff4ad-159d-4b38-9472-13b3b026ccbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "compositeImage": {
                "id": "8e4a1b06-4392-4346-9437-fceee02b2e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "047ff4ad-159d-4b38-9472-13b3b026ccbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0125fc-06dc-4a8f-90c3-b531c8ec07f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "047ff4ad-159d-4b38-9472-13b3b026ccbf",
                    "LayerId": "6d93d29c-e342-41ff-a137-158c3c5e67a3"
                }
            ]
        },
        {
            "id": "c5415804-298c-4833-ad02-7f12bdbcec10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "compositeImage": {
                "id": "6f81271f-1250-494e-86d4-731145589fa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5415804-298c-4833-ad02-7f12bdbcec10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83f95e7a-0b35-4001-a3d3-9b34c4ee3c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5415804-298c-4833-ad02-7f12bdbcec10",
                    "LayerId": "6d93d29c-e342-41ff-a137-158c3c5e67a3"
                }
            ]
        },
        {
            "id": "adfbc2dd-69e9-4350-a895-85194c9ce300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "compositeImage": {
                "id": "4573eb5e-0d92-41f5-b813-e502d917b863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adfbc2dd-69e9-4350-a895-85194c9ce300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5177eb96-0edd-46a9-b5bc-62a5adb0050b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adfbc2dd-69e9-4350-a895-85194c9ce300",
                    "LayerId": "6d93d29c-e342-41ff-a137-158c3c5e67a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6d93d29c-e342-41ff-a137-158c3c5e67a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cfb5bb9-f0f2-4c72-912a-f9b9bec17202",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}