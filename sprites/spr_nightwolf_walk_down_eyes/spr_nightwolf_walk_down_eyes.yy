{
    "id": "4cbe189a-4c27-4a15-ad30-3c6aee920256",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_walk_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 23,
    "bbox_right": 28,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee513516-fcb7-4570-8bbe-4ab26a50216c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbe189a-4c27-4a15-ad30-3c6aee920256",
            "compositeImage": {
                "id": "a61ebdb5-9dc0-40de-b9c1-89fba861b515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee513516-fcb7-4570-8bbe-4ab26a50216c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d98f7cf-7e6c-45ff-9282-ec6aa8db93b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee513516-fcb7-4570-8bbe-4ab26a50216c",
                    "LayerId": "c07795e0-fbc8-47fa-9108-4426dcfdc961"
                }
            ]
        },
        {
            "id": "dce086d9-9c81-412d-aa28-f1ab4daa1604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbe189a-4c27-4a15-ad30-3c6aee920256",
            "compositeImage": {
                "id": "39cbdc50-4cc6-4b88-9aba-cd480a9b3c4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce086d9-9c81-412d-aa28-f1ab4daa1604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "252fb4c6-68a2-4c8c-be90-bc7926e78034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce086d9-9c81-412d-aa28-f1ab4daa1604",
                    "LayerId": "c07795e0-fbc8-47fa-9108-4426dcfdc961"
                }
            ]
        },
        {
            "id": "98f45409-41b1-4883-8bf8-af6682d9be28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbe189a-4c27-4a15-ad30-3c6aee920256",
            "compositeImage": {
                "id": "0909b943-13ac-4a17-94d4-90709b41dd16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f45409-41b1-4883-8bf8-af6682d9be28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01c8b5fd-e974-442b-acc7-af1696da2d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f45409-41b1-4883-8bf8-af6682d9be28",
                    "LayerId": "c07795e0-fbc8-47fa-9108-4426dcfdc961"
                }
            ]
        },
        {
            "id": "6cea48a3-452c-46ed-aad6-80c071881db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbe189a-4c27-4a15-ad30-3c6aee920256",
            "compositeImage": {
                "id": "02b4026d-4466-43ea-a28c-17bd8ddb8010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cea48a3-452c-46ed-aad6-80c071881db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db3bbf32-4620-4424-9bb0-eb587cb55d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cea48a3-452c-46ed-aad6-80c071881db7",
                    "LayerId": "c07795e0-fbc8-47fa-9108-4426dcfdc961"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "c07795e0-fbc8-47fa-9108-4426dcfdc961",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cbe189a-4c27-4a15-ad30-3c6aee920256",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}