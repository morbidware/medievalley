{
    "id": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_melee_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd49dccc-70bd-477e-b83f-389ba11b74a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "compositeImage": {
                "id": "e9d5411f-d0df-4b2b-b427-82d68bff0563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd49dccc-70bd-477e-b83f-389ba11b74a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8cf2e5-fe71-43f0-9fab-3f012c716ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd49dccc-70bd-477e-b83f-389ba11b74a6",
                    "LayerId": "864d5c5b-e489-47b8-a84d-cde2e274a360"
                }
            ]
        },
        {
            "id": "4d53725b-d68f-49e6-ac3b-e6ab34e60b9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "compositeImage": {
                "id": "c5502102-efd9-419f-b94a-4feebc80f93d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d53725b-d68f-49e6-ac3b-e6ab34e60b9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc6d7a5-cfe7-49f1-9ac2-6e7e397b0e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d53725b-d68f-49e6-ac3b-e6ab34e60b9e",
                    "LayerId": "864d5c5b-e489-47b8-a84d-cde2e274a360"
                }
            ]
        },
        {
            "id": "dfe3f11d-2288-4157-a57f-1750b8f03884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "compositeImage": {
                "id": "10642c1f-1f92-476c-8659-78bb958a4fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfe3f11d-2288-4157-a57f-1750b8f03884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2c6ac12-94e2-45d7-9b12-f16c3b6e30b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfe3f11d-2288-4157-a57f-1750b8f03884",
                    "LayerId": "864d5c5b-e489-47b8-a84d-cde2e274a360"
                }
            ]
        },
        {
            "id": "d7bae041-58a4-425b-a823-5e35a18bd51f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "compositeImage": {
                "id": "33015f10-778b-4701-a250-f2ab9fadb596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7bae041-58a4-425b-a823-5e35a18bd51f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a49affc-55f5-4a10-a75f-4b5f910dc6c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7bae041-58a4-425b-a823-5e35a18bd51f",
                    "LayerId": "864d5c5b-e489-47b8-a84d-cde2e274a360"
                }
            ]
        },
        {
            "id": "2fffdb0e-6247-4a34-91e0-9d8dfb6d5691",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "compositeImage": {
                "id": "1a029884-cb42-43f9-864e-ce3998fe88ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fffdb0e-6247-4a34-91e0-9d8dfb6d5691",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119d981d-d2c4-4cd0-b259-8a44dd31c748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fffdb0e-6247-4a34-91e0-9d8dfb6d5691",
                    "LayerId": "864d5c5b-e489-47b8-a84d-cde2e274a360"
                }
            ]
        },
        {
            "id": "f97c765d-d3cc-440e-829b-0465ac0891b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "compositeImage": {
                "id": "5ea90993-f24f-4362-9638-d6114c5233b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f97c765d-d3cc-440e-829b-0465ac0891b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b4b1cb-c984-4c4e-907f-3ed36e8c3d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f97c765d-d3cc-440e-829b-0465ac0891b2",
                    "LayerId": "864d5c5b-e489-47b8-a84d-cde2e274a360"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "864d5c5b-e489-47b8-a84d-cde2e274a360",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43efa7e9-a0a6-4e90-9853-d1615b678f1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}