{
    "id": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42b0665d-2ef2-4c52-a7a9-b997b8f0b835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "compositeImage": {
                "id": "a3a0e995-76d2-4e64-b503-fe92dd2b1313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b0665d-2ef2-4c52-a7a9-b997b8f0b835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a56d72-ce0c-4bf0-bb0b-47aeae1e257f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b0665d-2ef2-4c52-a7a9-b997b8f0b835",
                    "LayerId": "eb5fe141-9c63-4b06-8893-8455ac77cfc2"
                }
            ]
        },
        {
            "id": "7ee90cda-ef1a-4e66-9249-084b9e366fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "compositeImage": {
                "id": "866ba936-a048-4546-ac24-9adfd5c5b5ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ee90cda-ef1a-4e66-9249-084b9e366fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0878612a-18da-4d7c-a19b-16c1dca8a475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ee90cda-ef1a-4e66-9249-084b9e366fc9",
                    "LayerId": "eb5fe141-9c63-4b06-8893-8455ac77cfc2"
                }
            ]
        },
        {
            "id": "b37cc4d5-197b-4fa9-98b7-844aa8329eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "compositeImage": {
                "id": "ae74e5f4-3087-4d0e-885b-7baa7af0deb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b37cc4d5-197b-4fa9-98b7-844aa8329eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f4ebc98-75d1-451f-88de-a4392b0137bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b37cc4d5-197b-4fa9-98b7-844aa8329eed",
                    "LayerId": "eb5fe141-9c63-4b06-8893-8455ac77cfc2"
                }
            ]
        },
        {
            "id": "88f71c10-96fb-4f33-9b11-96c026b01674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "compositeImage": {
                "id": "c21145b6-d6b7-44e2-b69f-6a34f28139a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f71c10-96fb-4f33-9b11-96c026b01674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8a9356-a925-436e-95ea-9e390e7fe6f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f71c10-96fb-4f33-9b11-96c026b01674",
                    "LayerId": "eb5fe141-9c63-4b06-8893-8455ac77cfc2"
                }
            ]
        },
        {
            "id": "51844a4a-93ce-49c9-a673-e5e9f6406dca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "compositeImage": {
                "id": "a4046dcf-a4e9-49cb-8f1c-12e51b9f95b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51844a4a-93ce-49c9-a673-e5e9f6406dca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2f7456-fbb0-4b79-ada5-ede2eb3b01eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51844a4a-93ce-49c9-a673-e5e9f6406dca",
                    "LayerId": "eb5fe141-9c63-4b06-8893-8455ac77cfc2"
                }
            ]
        },
        {
            "id": "2b33e53f-f4d2-424b-9030-b3398e29343a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "compositeImage": {
                "id": "dcf12fb5-f127-4a3c-8536-21c71117c0d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b33e53f-f4d2-424b-9030-b3398e29343a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21880d7d-4de4-4d1a-a010-89e1270857f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b33e53f-f4d2-424b-9030-b3398e29343a",
                    "LayerId": "eb5fe141-9c63-4b06-8893-8455ac77cfc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "eb5fe141-9c63-4b06-8893-8455ac77cfc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6b0cba7-c304-418d-b259-3fb6e057ce18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}