{
    "id": "47aa8fc1-347c-44f6-a1df-22e43c56d986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_popup_btn_yes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2dbbda1-8883-4f45-b5ca-4849660b8179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47aa8fc1-347c-44f6-a1df-22e43c56d986",
            "compositeImage": {
                "id": "cb79cbcf-7191-4ad8-a7da-918cd01b7397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2dbbda1-8883-4f45-b5ca-4849660b8179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a42cad71-6422-4bf4-9489-92d1ca823e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2dbbda1-8883-4f45-b5ca-4849660b8179",
                    "LayerId": "9142993a-5275-4acf-8e47-4f7881f3f526"
                }
            ]
        }
    ],
    "gridX": 160,
    "gridY": 90,
    "height": 12,
    "layers": [
        {
            "id": "9142993a-5275-4acf-8e47-4f7881f3f526",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47aa8fc1-347c-44f6-a1df-22e43c56d986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 6
}