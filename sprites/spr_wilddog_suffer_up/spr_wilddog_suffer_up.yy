{
    "id": "35af0063-07e8-4e2f-b930-582f55470c76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_suffer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 27,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69954415-093d-436c-a3b0-a5ab38f85593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35af0063-07e8-4e2f-b930-582f55470c76",
            "compositeImage": {
                "id": "3dda06e3-a82e-4267-9bc3-667378c9b310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69954415-093d-436c-a3b0-a5ab38f85593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bc9231-9e31-4a28-8196-e484b3de1d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69954415-093d-436c-a3b0-a5ab38f85593",
                    "LayerId": "03bba86c-d287-4ba0-80a2-2ff1b5edbff9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "03bba86c-d287-4ba0-80a2-2ff1b5edbff9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35af0063-07e8-4e2f-b930-582f55470c76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}