{
    "id": "b13cc40b-dbcc-4275-9f63-b42764cb250d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b1b88db-c776-47a6-a00d-cb4d3edc80ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b13cc40b-dbcc-4275-9f63-b42764cb250d",
            "compositeImage": {
                "id": "423b94fd-5057-40d0-a2dd-98af5dabc666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b1b88db-c776-47a6-a00d-cb4d3edc80ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "198aceae-1ce1-48fb-9f9d-24795addfc3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b1b88db-c776-47a6-a00d-cb4d3edc80ba",
                    "LayerId": "0f35e092-0982-4f98-8782-d654f2342325"
                }
            ]
        },
        {
            "id": "2993972a-b72e-4d3f-a1b4-44054f38f1ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b13cc40b-dbcc-4275-9f63-b42764cb250d",
            "compositeImage": {
                "id": "680b45ba-6083-4720-85ef-6ce2f26af028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2993972a-b72e-4d3f-a1b4-44054f38f1ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154f45dc-5a64-489a-b4c6-2081036c173c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2993972a-b72e-4d3f-a1b4-44054f38f1ee",
                    "LayerId": "0f35e092-0982-4f98-8782-d654f2342325"
                }
            ]
        },
        {
            "id": "80adcb67-c7f4-4bb1-b0bd-fda32555b654",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b13cc40b-dbcc-4275-9f63-b42764cb250d",
            "compositeImage": {
                "id": "bdf4c34d-751e-47bd-b7a1-d1ccd90082fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80adcb67-c7f4-4bb1-b0bd-fda32555b654",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66ba2789-a760-4217-8081-aa74beadefdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80adcb67-c7f4-4bb1-b0bd-fda32555b654",
                    "LayerId": "0f35e092-0982-4f98-8782-d654f2342325"
                }
            ]
        },
        {
            "id": "c2c8ad80-6ddc-4524-a964-a1871f49f896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b13cc40b-dbcc-4275-9f63-b42764cb250d",
            "compositeImage": {
                "id": "b604789a-b94f-42c1-8f91-743581d8fcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c8ad80-6ddc-4524-a964-a1871f49f896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "614899c9-67ce-40f8-8297-56d95a94aa8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c8ad80-6ddc-4524-a964-a1871f49f896",
                    "LayerId": "0f35e092-0982-4f98-8782-d654f2342325"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0f35e092-0982-4f98-8782-d654f2342325",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b13cc40b-dbcc-4275-9f63-b42764cb250d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}