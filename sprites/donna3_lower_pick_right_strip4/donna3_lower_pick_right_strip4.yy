{
    "id": "1282b319-f12b-484c-b3fa-39a62e77827f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02a3e910-7946-4c40-8301-5de82a4cfbf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1282b319-f12b-484c-b3fa-39a62e77827f",
            "compositeImage": {
                "id": "618db396-344e-42c8-8565-fcdb52526d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02a3e910-7946-4c40-8301-5de82a4cfbf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e9a8a8f-e673-43e5-b384-88731745394d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02a3e910-7946-4c40-8301-5de82a4cfbf3",
                    "LayerId": "fed2c761-953d-477b-9f50-3bfc1a8b7999"
                }
            ]
        },
        {
            "id": "80426513-5068-43ee-b044-4262fae900bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1282b319-f12b-484c-b3fa-39a62e77827f",
            "compositeImage": {
                "id": "c2d2e91f-7ec9-4998-9688-aa2e5f668028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80426513-5068-43ee-b044-4262fae900bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e87e20-bd09-4fd3-8a8a-2cdfd81000f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80426513-5068-43ee-b044-4262fae900bd",
                    "LayerId": "fed2c761-953d-477b-9f50-3bfc1a8b7999"
                }
            ]
        },
        {
            "id": "dc2b5809-8472-4086-9ee1-38dd2a466f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1282b319-f12b-484c-b3fa-39a62e77827f",
            "compositeImage": {
                "id": "0a9d3360-2ecc-4e05-84dd-05c52965ab03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc2b5809-8472-4086-9ee1-38dd2a466f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba266e1-3604-4f59-9346-cac0a6365944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2b5809-8472-4086-9ee1-38dd2a466f6f",
                    "LayerId": "fed2c761-953d-477b-9f50-3bfc1a8b7999"
                }
            ]
        },
        {
            "id": "ddc4f44a-2f1d-4ce3-93c3-c3f7728cd40c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1282b319-f12b-484c-b3fa-39a62e77827f",
            "compositeImage": {
                "id": "73fc58f0-0bd2-4020-83b2-d7697a8db21a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddc4f44a-2f1d-4ce3-93c3-c3f7728cd40c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44005c38-698f-451b-bdb2-c0a0f060bb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddc4f44a-2f1d-4ce3-93c3-c3f7728cd40c",
                    "LayerId": "fed2c761-953d-477b-9f50-3bfc1a8b7999"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fed2c761-953d-477b-9f50-3bfc1a8b7999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1282b319-f12b-484c-b3fa-39a62e77827f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}