{
    "id": "c15f1bcf-4451-4296-823a-aa9b6a04c56a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_hoe_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b1c1979-7d97-4b01-ba8b-933f84faa3c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c15f1bcf-4451-4296-823a-aa9b6a04c56a",
            "compositeImage": {
                "id": "4d35c5ca-c831-4916-97eb-56f718a9d34e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1c1979-7d97-4b01-ba8b-933f84faa3c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe6df026-0493-4ee4-be7c-2fd61e12cdc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1c1979-7d97-4b01-ba8b-933f84faa3c8",
                    "LayerId": "bf4a22b5-3a88-43a5-b257-24b807b51561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bf4a22b5-3a88-43a5-b257-24b807b51561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c15f1bcf-4451-4296-823a-aa9b6a04c56a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 13
}