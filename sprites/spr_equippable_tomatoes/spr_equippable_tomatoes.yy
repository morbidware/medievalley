{
    "id": "f7fff0b5-a627-495f-b686-8269798051f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equippable_tomatoes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ddada12-3737-41c8-ba71-e93245fd046e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7fff0b5-a627-495f-b686-8269798051f2",
            "compositeImage": {
                "id": "677b8f65-7d69-4e83-b51c-63e21c090abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ddada12-3737-41c8-ba71-e93245fd046e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be7cb45d-61d4-420d-b991-556744ca9c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ddada12-3737-41c8-ba71-e93245fd046e",
                    "LayerId": "16402ce7-ce23-4887-b3e9-84bc34499a7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "16402ce7-ce23-4887-b3e9-84bc34499a7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7fff0b5-a627-495f-b686-8269798051f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}