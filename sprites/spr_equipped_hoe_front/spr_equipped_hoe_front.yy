{
    "id": "803e301d-557f-42b7-8e0b-b7ef7f8a423b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_hoe_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69aa1e1e-729d-4176-bde0-795cc01984a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803e301d-557f-42b7-8e0b-b7ef7f8a423b",
            "compositeImage": {
                "id": "c3aae4bb-aa1a-4dda-a0f5-65716d02e328",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69aa1e1e-729d-4176-bde0-795cc01984a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1807740-e662-477f-aa6f-4026a5ec01c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69aa1e1e-729d-4176-bde0-795cc01984a7",
                    "LayerId": "9974672b-6f05-4de5-aad0-9f02be84a829"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9974672b-6f05-4de5-aad0-9f02be84a829",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "803e301d-557f-42b7-8e0b-b7ef7f8a423b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 2,
    "yorig": 18
}