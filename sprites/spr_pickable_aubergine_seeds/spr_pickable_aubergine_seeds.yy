{
    "id": "ea344f6a-ff69-4d96-9c60-a23cba0ff99e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_aubergine_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a4b5044-6d28-4059-a976-04bf8056cf3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea344f6a-ff69-4d96-9c60-a23cba0ff99e",
            "compositeImage": {
                "id": "6d49b77a-a26f-4ba8-acd5-39f690ee53c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a4b5044-6d28-4059-a976-04bf8056cf3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6fc4e2-ccec-4dff-b8c0-a961b81f466d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a4b5044-6d28-4059-a976-04bf8056cf3e",
                    "LayerId": "b6e794c6-10a4-4558-97bc-af5d5fa92656"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b6e794c6-10a4-4558-97bc-af5d5fa92656",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea344f6a-ff69-4d96-9c60-a23cba0ff99e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}