{
    "id": "a99b8ccc-c128-4293-b80c-16722559ecee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_xpdr_x2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "355bcda9-80e7-4fff-b845-94f901de7ff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "fb16bfa7-64cb-4969-a374-701caf20725e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "355bcda9-80e7-4fff-b845-94f901de7ff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5838e6fc-c706-402c-90f4-88fcf07499dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "355bcda9-80e7-4fff-b845-94f901de7ff4",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "137b74a5-b818-496b-a5fc-432fa702e5c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "401bcd76-78c1-4820-9725-826f12227b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "137b74a5-b818-496b-a5fc-432fa702e5c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd36130-688f-4bdc-9327-0318bda7c3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "137b74a5-b818-496b-a5fc-432fa702e5c2",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "5d189082-5964-49a0-941e-c2eb90544e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "1c69574c-dd84-4e2d-ae2a-f0b0b220e101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d189082-5964-49a0-941e-c2eb90544e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfb3456-a7ee-4c09-9f71-374edb09d137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d189082-5964-49a0-941e-c2eb90544e06",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "562200be-02b6-4e5d-a10c-cb6e06857110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "57eb1aad-5022-42fa-ba24-5d8421e3d477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "562200be-02b6-4e5d-a10c-cb6e06857110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96d9fb5-f821-44dd-82d5-9596a54545dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "562200be-02b6-4e5d-a10c-cb6e06857110",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "9a297d1b-afa3-4b9a-bf3f-a042cd97cd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "fefc7eb5-01d5-46f5-b6bc-67ae900e8ecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a297d1b-afa3-4b9a-bf3f-a042cd97cd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce12da3-c553-4927-9466-a19d2ba577dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a297d1b-afa3-4b9a-bf3f-a042cd97cd5a",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "bc5375c3-9240-41a6-a4e2-3df38bea7715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "e84cad84-be40-465d-8def-523b2393e110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5375c3-9240-41a6-a4e2-3df38bea7715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79a47e4-c260-464f-9b12-4054b103fac5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5375c3-9240-41a6-a4e2-3df38bea7715",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "2652ebcc-c4b3-4f67-bd03-1d675695ce54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "392e090a-9315-4839-a0d8-ca66779bf4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2652ebcc-c4b3-4f67-bd03-1d675695ce54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "862f8ad1-8252-481b-99e7-8ffca06cc9b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2652ebcc-c4b3-4f67-bd03-1d675695ce54",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "072a1c5e-d54c-40b0-bb55-b4b0966ad7c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "6b456b4b-e90c-4855-bd86-a0f2b406e348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "072a1c5e-d54c-40b0-bb55-b4b0966ad7c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4e0a4b-6be8-4d10-9629-f98477e25b79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "072a1c5e-d54c-40b0-bb55-b4b0966ad7c5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "9765c47e-cb31-4577-b9fe-ccfcdaaace78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "4efbb92d-f411-4abd-92de-62d85d686fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9765c47e-cb31-4577-b9fe-ccfcdaaace78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5751ba-d118-40ca-b180-fb73468d7fd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9765c47e-cb31-4577-b9fe-ccfcdaaace78",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "9bcb3829-0676-402f-bf62-22d95a1a3b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "1bcad9bd-27fd-41ff-b11d-988107b560c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bcb3829-0676-402f-bf62-22d95a1a3b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1417d794-37ea-439d-b7e3-802244c68d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bcb3829-0676-402f-bf62-22d95a1a3b5c",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "33297e8e-b7c4-4320-be71-99572090b021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "705abb6c-6a87-4490-bc31-89249fe73652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33297e8e-b7c4-4320-be71-99572090b021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ce97972-69e7-4f3d-b2f0-82b8499c51bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33297e8e-b7c4-4320-be71-99572090b021",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "78566e11-bf94-45cd-abc6-316ae0992c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d930f4f3-3a0f-4a97-ae22-80a93d4dd8fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78566e11-bf94-45cd-abc6-316ae0992c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c05fdd5-4ba9-42d4-810c-882c428a53f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78566e11-bf94-45cd-abc6-316ae0992c5b",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "d29898df-f37c-4883-9f40-41ed47867285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d5c08732-47d7-4765-a92b-073722638e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d29898df-f37c-4883-9f40-41ed47867285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f1a15e-51a4-403c-b848-ac4d9f7f2911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d29898df-f37c-4883-9f40-41ed47867285",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "aff3c3cb-0a17-4f80-9521-acf82b127ff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "0bd7754d-251e-4d72-a43e-fdde33f2d14e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aff3c3cb-0a17-4f80-9521-acf82b127ff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "256d2b79-50d1-469f-a6c4-2512d494d6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aff3c3cb-0a17-4f80-9521-acf82b127ff8",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "e4969e7b-e953-4fd1-b0db-b1b42df60969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "abe0b329-9ba0-4cda-a452-fa6871f9745f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4969e7b-e953-4fd1-b0db-b1b42df60969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b3b4a6-f240-4b8e-9a95-84103b6febe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4969e7b-e953-4fd1-b0db-b1b42df60969",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "1e1d001d-95cd-4dc2-b32b-c875e44ada77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "098b2420-69f0-44ce-8a8d-e23c97747fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e1d001d-95cd-4dc2-b32b-c875e44ada77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73e27040-7fa9-46d1-bc86-d178b357c669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e1d001d-95cd-4dc2-b32b-c875e44ada77",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "29a77134-6a8f-4170-a4b7-d1d389d746d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "e7838448-70f0-4f45-95a1-557323d4d5ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a77134-6a8f-4170-a4b7-d1d389d746d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47c88ee-13f4-4e0e-a21d-920fbf7bb0b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a77134-6a8f-4170-a4b7-d1d389d746d3",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "ff2ef72e-e0a3-47da-a71f-ac037cf91b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "28055471-ade1-4247-a234-038afca0574c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff2ef72e-e0a3-47da-a71f-ac037cf91b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d073810-b238-4bdf-ad4c-ac929d0c93d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff2ef72e-e0a3-47da-a71f-ac037cf91b0f",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "82aea0e7-654a-4922-99e0-ea3f98becb5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "6ff9df9c-75fe-4983-bdfe-2448b38b3249",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82aea0e7-654a-4922-99e0-ea3f98becb5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6b238f9-1f83-4083-88e0-dc2d5b147e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82aea0e7-654a-4922-99e0-ea3f98becb5c",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "cdc5251d-1d66-4698-b05c-8a3f627d5aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "ff6e5c83-3323-4eff-9765-7c4d0af52fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc5251d-1d66-4698-b05c-8a3f627d5aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "319d7adf-070e-447a-9a25-9f383a989051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc5251d-1d66-4698-b05c-8a3f627d5aa4",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "fb5d5e76-07b1-4fd6-b600-1624138c7581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d9c99151-264e-4b39-9476-a26df1e95a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb5d5e76-07b1-4fd6-b600-1624138c7581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fcbc54c-3614-445f-b942-acdd9355e3ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb5d5e76-07b1-4fd6-b600-1624138c7581",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "8b45af73-9071-4815-a550-f3a56ce22364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "54b0da6f-96fd-495e-a9d2-551d50084774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b45af73-9071-4815-a550-f3a56ce22364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33f39b2-936d-49fe-9708-6085450626f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b45af73-9071-4815-a550-f3a56ce22364",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "4fe94334-7e8a-4b56-909f-76a0852e5b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "a38ea97c-f9a8-4e6f-9b7b-4ab2f9703135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe94334-7e8a-4b56-909f-76a0852e5b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9024b39-cc5c-4839-b6e7-255dcc90094e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe94334-7e8a-4b56-909f-76a0852e5b0f",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "6bd5718d-1afd-4c2d-846d-3c7eacfd99c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "03e19f3a-f02d-4cba-b633-c983a3889869",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd5718d-1afd-4c2d-846d-3c7eacfd99c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db66ca42-b15c-41ee-b1d4-903df042e3ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd5718d-1afd-4c2d-846d-3c7eacfd99c5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "4f314911-84ce-4d5d-93d5-62008c3165fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "6c1eff80-07dd-4bf1-9946-abc7394189fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f314911-84ce-4d5d-93d5-62008c3165fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c88f614-bd1a-4af5-aa3b-0f5f17cdea36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f314911-84ce-4d5d-93d5-62008c3165fb",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "93c27f78-1fb5-4571-9aea-e8984f55122f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d1cb02f7-c037-4f8b-b302-3d4631657d6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93c27f78-1fb5-4571-9aea-e8984f55122f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c35c2b-549b-41ea-a62f-6d1d68a953b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93c27f78-1fb5-4571-9aea-e8984f55122f",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "b0ea4399-87a9-4120-a68d-ec4283f27283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "c51258de-38c2-4727-84a0-aa6a1317fa2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ea4399-87a9-4120-a68d-ec4283f27283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c9bc7e-1659-481a-9673-10e767b5f85a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ea4399-87a9-4120-a68d-ec4283f27283",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "b2a3b4a8-c2d0-4dad-af4f-04990e87a66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "01580d07-49db-44ed-8b33-f39beac9129d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a3b4a8-c2d0-4dad-af4f-04990e87a66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863eb48d-7a32-4772-8d0f-b7f76cc99568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a3b4a8-c2d0-4dad-af4f-04990e87a66f",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "ca149e5e-8a0e-43e6-9de5-83e68bf1496e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "e58dfe25-ab67-42ab-a6b5-d90ae09764cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca149e5e-8a0e-43e6-9de5-83e68bf1496e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b2e1d7a-1c8f-4013-b575-96190bc3f72f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca149e5e-8a0e-43e6-9de5-83e68bf1496e",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "3f44f23a-0e28-41f4-b34b-b3040d94ab6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "bb23aa24-c347-468f-9c68-1724a8b9f8da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f44f23a-0e28-41f4-b34b-b3040d94ab6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed2dfa6-e5e8-47c7-beaa-bfe1407e5b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f44f23a-0e28-41f4-b34b-b3040d94ab6d",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "cf06a4f8-1a0b-4daa-a872-fc5b301e7093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "63d6c6a2-bf8b-479f-a7da-227ee7547c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf06a4f8-1a0b-4daa-a872-fc5b301e7093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdf5b740-f5fc-4cf1-8e5f-40bcadaa9c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf06a4f8-1a0b-4daa-a872-fc5b301e7093",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "bf84b306-851e-4298-a248-321245a145b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "1b8abc01-56ca-43fc-81d3-db306c037256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf84b306-851e-4298-a248-321245a145b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ea27e9-4d73-4d3b-8a18-557cf40dd623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf84b306-851e-4298-a248-321245a145b5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "ce6f27f8-c188-4fda-a9d4-bdd550e985b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d4698a67-3b7f-4b3e-9ac4-f9fcfe1b9c91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6f27f8-c188-4fda-a9d4-bdd550e985b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8ff6016-edc1-462d-85d9-f0754a6df850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6f27f8-c188-4fda-a9d4-bdd550e985b5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "51ade55b-b7a2-400e-af35-533277fdf8bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "712c22ee-2a54-45f9-9609-fe194a88e82b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51ade55b-b7a2-400e-af35-533277fdf8bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61badc1c-f888-453d-9fed-8c353c68ec63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51ade55b-b7a2-400e-af35-533277fdf8bf",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "88245494-1412-435f-91a6-04ff607f8fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "51d53bf4-d562-4165-b4ae-7cf19a8e95a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88245494-1412-435f-91a6-04ff607f8fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66b49c1b-9b1d-4020-b522-d70904906832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88245494-1412-435f-91a6-04ff607f8fc7",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "9bad4565-0cd9-4839-9b35-7bad94f82079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "fc898fe6-57d1-4c1a-aae6-091d4e46b5d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bad4565-0cd9-4839-9b35-7bad94f82079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117377e6-49bc-4e3f-8cae-0da835248dde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bad4565-0cd9-4839-9b35-7bad94f82079",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "3626fccb-432b-4d3f-95ce-5bdc3feafaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "fae8f697-1c7b-47d8-8c43-a0c3821e7e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3626fccb-432b-4d3f-95ce-5bdc3feafaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1efd1a8a-dde3-4cc8-9697-556e244e0224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3626fccb-432b-4d3f-95ce-5bdc3feafaf2",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "bbc0bc97-df05-4c0c-9c6d-64f4873791ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d80fa2c9-c164-4c3b-b1e8-c4acdb18c109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc0bc97-df05-4c0c-9c6d-64f4873791ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad720b3-9f11-4823-8ec8-b7492cec4393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc0bc97-df05-4c0c-9c6d-64f4873791ec",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "d01fd6d9-c86e-478a-9538-c945199ddbba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "a5c1cbe9-7024-4cb1-8dc8-df6e53d228f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01fd6d9-c86e-478a-9538-c945199ddbba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f31c051-df4e-40ac-8b75-7095a4364fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01fd6d9-c86e-478a-9538-c945199ddbba",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "0f413dc8-e95a-48f9-975b-823002c16e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5c45940a-96cf-4f09-840f-c99d10373901",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f413dc8-e95a-48f9-975b-823002c16e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4de81d0-041d-4c44-9da2-c2ad80762782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f413dc8-e95a-48f9-975b-823002c16e1f",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "df0d7bb2-6475-414a-90cd-87449ad340d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "84c40db7-cc4e-491e-acd4-6525565ed59f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df0d7bb2-6475-414a-90cd-87449ad340d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4eb61a-0c65-4065-b71c-506c981bb8ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df0d7bb2-6475-414a-90cd-87449ad340d0",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "ff5557a8-e69c-4df6-975a-72dd16d43d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "e3a38c74-2741-468c-aac9-0bd74ced31e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff5557a8-e69c-4df6-975a-72dd16d43d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07647e81-38b2-479d-8aae-472c79685362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff5557a8-e69c-4df6-975a-72dd16d43d7d",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "4357f28c-8f70-4ed2-8d32-8d35d7492e94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "289ba694-b73c-4cbc-ae86-79c5dde49da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4357f28c-8f70-4ed2-8d32-8d35d7492e94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e71b5130-6fc4-4da0-8089-fdd80d5f693f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4357f28c-8f70-4ed2-8d32-8d35d7492e94",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "7d49a1b9-cf1c-49ac-8e72-c5bb7d26d121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d9e26b08-f937-4bdb-ba7d-1f9c5fd7e506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d49a1b9-cf1c-49ac-8e72-c5bb7d26d121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7962bca9-0293-42b8-a3e3-15ca86bc7743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d49a1b9-cf1c-49ac-8e72-c5bb7d26d121",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "a16141d5-fc07-4792-9811-2fb910d34450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5ce9402c-7e20-43ec-913b-715395541dde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16141d5-fc07-4792-9811-2fb910d34450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ef236e-3c65-4650-af1d-894953f85437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16141d5-fc07-4792-9811-2fb910d34450",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "376f6dc8-2b63-49a4-81cd-9e253d2835a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5d4bb6d5-4216-40b7-99fa-d25b5cb0c2a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "376f6dc8-2b63-49a4-81cd-9e253d2835a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e027cbef-0663-4042-aa40-8c256eb66a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "376f6dc8-2b63-49a4-81cd-9e253d2835a5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "aa4760f7-319b-4a26-b987-3107bd0d2bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "fb40e729-dfd4-4089-8b5d-c277e425bbfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa4760f7-319b-4a26-b987-3107bd0d2bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c36f071-88ae-44ef-98de-69c5b5db1e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa4760f7-319b-4a26-b987-3107bd0d2bda",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "29b580ba-aed4-4bc8-9b72-81dc263fb9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5470f808-02d6-4efe-8a7b-573bb1d37d7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b580ba-aed4-4bc8-9b72-81dc263fb9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6da4b3a-42d4-4239-b415-ecc828461867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b580ba-aed4-4bc8-9b72-81dc263fb9b0",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "fd47ed22-326f-4c51-99cd-d26426c828c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "520d0dde-bd1e-49de-ae65-53ca2a379e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd47ed22-326f-4c51-99cd-d26426c828c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c42bee-d80a-4867-badc-abea410932c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd47ed22-326f-4c51-99cd-d26426c828c5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "db6693f4-356d-4bb6-9a50-53b0bdabbe14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "95a2f446-fd44-4f1d-89c1-254767ec41c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6693f4-356d-4bb6-9a50-53b0bdabbe14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6042c7b9-0844-440b-a050-f2cf9d7a6806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6693f4-356d-4bb6-9a50-53b0bdabbe14",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "29e35c8d-d4a5-4f73-bf51-b517c2c86f6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "82780c36-3d6c-4445-9a5a-40ead86812ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e35c8d-d4a5-4f73-bf51-b517c2c86f6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73490da1-e994-48e1-b55a-225a12056f7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e35c8d-d4a5-4f73-bf51-b517c2c86f6c",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "2fd50125-5bae-41e9-b752-09c704b3fb59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5f21d3f5-60e6-47a6-9eb1-91dbcc951492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd50125-5bae-41e9-b752-09c704b3fb59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c989283d-0735-4fc5-9766-fe4668ec2054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd50125-5bae-41e9-b752-09c704b3fb59",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "dcae56b7-23d1-4efd-b721-9f6700596b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "d03a48f0-595e-4165-9ab2-9dc2851122a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcae56b7-23d1-4efd-b721-9f6700596b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11a780ea-273e-4507-b10e-247281445001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcae56b7-23d1-4efd-b721-9f6700596b64",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "e9c53745-5a4c-424a-9c6a-efa59f3bf093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "9cd05193-6769-426a-ad76-4f9341c26920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c53745-5a4c-424a-9c6a-efa59f3bf093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a04e83-926e-4392-aced-51696aa56a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c53745-5a4c-424a-9c6a-efa59f3bf093",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "523dd653-e068-41b3-987e-6c2c16370fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "a72edb39-dc56-41b1-9cf6-0015947fae18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "523dd653-e068-41b3-987e-6c2c16370fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623691a1-620b-40dd-b2c8-ee51581f9546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "523dd653-e068-41b3-987e-6c2c16370fbf",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "af7bebb7-a2fb-43af-a86b-207c1ed4fb42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "ce479491-aa70-4f0c-9690-f71d6017a719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af7bebb7-a2fb-43af-a86b-207c1ed4fb42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb9b73c-56e5-4eda-b240-88addddb94f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af7bebb7-a2fb-43af-a86b-207c1ed4fb42",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "b4e61cac-f0f0-4482-96f4-39a0258cb5d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "f4ed923f-2dec-4cc2-8254-af514daa987d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e61cac-f0f0-4482-96f4-39a0258cb5d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dc64da4-ca39-4474-9aa0-4bbba8065ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e61cac-f0f0-4482-96f4-39a0258cb5d7",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "ad66fa1c-3873-4781-8b89-035108aee8e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "8ec05b4c-e175-4ca9-815b-ea82d847cf94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad66fa1c-3873-4781-8b89-035108aee8e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11bc98e-c849-4e6c-9225-5bf626099983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad66fa1c-3873-4781-8b89-035108aee8e2",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "6642eedf-2c88-45f4-8bf7-c22d8ca806c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "2b5ad080-ff94-4c47-8493-a0136b008cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6642eedf-2c88-45f4-8bf7-c22d8ca806c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79ca04d-b289-433d-bf4c-9881155c5260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6642eedf-2c88-45f4-8bf7-c22d8ca806c1",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "c0d7270a-0d62-40eb-964a-a0e262e7062f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "03288453-5e3f-4283-a479-a843cc99b2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0d7270a-0d62-40eb-964a-a0e262e7062f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "defe0dea-783b-406d-bd84-8012215bfda6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0d7270a-0d62-40eb-964a-a0e262e7062f",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "a7471d05-5e64-45bc-8438-112ebb6c37a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5073781e-6cd0-4ba9-9295-6e9c4437d154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7471d05-5e64-45bc-8438-112ebb6c37a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf98d43-c39a-4b4d-9fbe-8e44b32e42c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7471d05-5e64-45bc-8438-112ebb6c37a5",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "14c0399e-cc80-4fe1-80ff-06081c20dbf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "437c81c9-deb0-4174-95ad-7679db249ce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c0399e-cc80-4fe1-80ff-06081c20dbf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b58c965f-cf7a-43e7-b10d-502babeceded",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c0399e-cc80-4fe1-80ff-06081c20dbf4",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "622eb000-6364-4bda-a376-f23321bb762d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "5ff5263e-785b-4f76-a0d0-c9e44e02d5fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622eb000-6364-4bda-a376-f23321bb762d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ff95f9-8445-4c90-8ef2-03c7817008d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622eb000-6364-4bda-a376-f23321bb762d",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "66c218ac-3d64-41db-8a29-18b8a5b43615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "cb0b5b4a-941d-40f6-baaa-7a87ca7d0c26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c218ac-3d64-41db-8a29-18b8a5b43615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1ccb70-91db-4828-bc70-8853ee363714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c218ac-3d64-41db-8a29-18b8a5b43615",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "5803dd7a-f515-446e-84bf-f3fc7b8afd81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "78e9c9d4-5b4f-4aa6-9b94-2f4538646238",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5803dd7a-f515-446e-84bf-f3fc7b8afd81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ceefbd5-43c4-4afb-80e8-f4fb72e0ffb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5803dd7a-f515-446e-84bf-f3fc7b8afd81",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "d4e56347-81b7-44e4-95da-70bc96be23b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "12b68bbe-06c0-4e10-9c01-97472d35301a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e56347-81b7-44e4-95da-70bc96be23b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa8c5cd-a1d8-431b-b90e-c71f0bb315ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e56347-81b7-44e4-95da-70bc96be23b9",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "ee47a8a0-2324-47b2-8500-e5986fc7e990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "e8822137-f32e-4c35-a965-079d8d38816c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee47a8a0-2324-47b2-8500-e5986fc7e990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3acb39b1-1ac2-463d-9677-3533054ff01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee47a8a0-2324-47b2-8500-e5986fc7e990",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "8f76b058-4188-4805-a38c-ef329780212c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "aa44c928-2653-4db1-9b98-45a035ed955e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f76b058-4188-4805-a38c-ef329780212c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fafc5522-69a4-451b-bdc5-2c8ab06219f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f76b058-4188-4805-a38c-ef329780212c",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "bef3c7fe-0c68-4d64-acc1-f9b99cf5a461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "66b5e7bd-963b-4117-801d-d4102c25bb9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bef3c7fe-0c68-4d64-acc1-f9b99cf5a461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a080fb54-4eae-4b38-a302-aea0beeb5fa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bef3c7fe-0c68-4d64-acc1-f9b99cf5a461",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "8a841fd5-7260-49b1-aaff-848989233977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "38e907fe-6d01-4005-986b-a79c67184b8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a841fd5-7260-49b1-aaff-848989233977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab552cd-5e65-441f-9829-4b6e861dc844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a841fd5-7260-49b1-aaff-848989233977",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "dd226dcb-434d-4954-ad18-793170238b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "69d75495-2043-4dd8-89bc-f6094a21d7ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd226dcb-434d-4954-ad18-793170238b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21eda65-75f5-4d06-b964-c53fca254949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd226dcb-434d-4954-ad18-793170238b15",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "cb505923-4db0-4d42-89d3-075b607f7503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "85562438-ab27-4485-80e4-3a60ed97f63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb505923-4db0-4d42-89d3-075b607f7503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beffbda8-9dc8-40f0-9013-6aa73ba798ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb505923-4db0-4d42-89d3-075b607f7503",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "1e29096a-311c-4d85-898e-5e644e7b7a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "c43b77ec-bc1b-4f88-a1aa-481fc5786421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e29096a-311c-4d85-898e-5e644e7b7a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706ae292-fcab-4f4a-81ba-756ed592254b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e29096a-311c-4d85-898e-5e644e7b7a6c",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "3699c78d-ab38-4adc-9371-c14219a5bb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "3f55dc74-292e-4338-83fa-aa3efc4873f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3699c78d-ab38-4adc-9371-c14219a5bb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b278a2b4-7761-47bf-892e-5c7a579fe8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3699c78d-ab38-4adc-9371-c14219a5bb89",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        },
        {
            "id": "5a4368a8-5ed8-4867-b6bb-a4f82391fa5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "compositeImage": {
                "id": "efc38f8e-e3f3-48c0-8f6d-03fdbe395a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4368a8-5ed8-4867-b6bb-a4f82391fa5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef64c1e8-1119-425a-8283-f7ec97a1045c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4368a8-5ed8-4867-b6bb-a4f82391fa5a",
                    "LayerId": "3b7389bc-bfd7-4825-b481-ca9db38aba75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "3b7389bc-bfd7-4825-b481-ca9db38aba75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a99b8ccc-c128-4293-b80c-16722559ecee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}