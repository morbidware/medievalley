{
    "id": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_watering_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81e1f89e-39d5-48d8-8ee4-adac263c207c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "compositeImage": {
                "id": "c7d9ba48-c6a4-41a6-844e-23d2fc0738ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e1f89e-39d5-48d8-8ee4-adac263c207c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a674bce8-93e8-4918-9155-c158fe1ed951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e1f89e-39d5-48d8-8ee4-adac263c207c",
                    "LayerId": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86"
                }
            ]
        },
        {
            "id": "325891a6-cc58-4a93-9c90-cad047da617a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "compositeImage": {
                "id": "5cf2aa89-113d-4111-bea6-f622121a5f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "325891a6-cc58-4a93-9c90-cad047da617a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07295bfc-5703-40a7-95a7-3e0a91ec5aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "325891a6-cc58-4a93-9c90-cad047da617a",
                    "LayerId": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86"
                }
            ]
        },
        {
            "id": "7f8f19db-7593-4028-864a-644953ff22bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "compositeImage": {
                "id": "82be055a-b270-4949-8377-6b5008f008bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8f19db-7593-4028-864a-644953ff22bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b39f1e72-da7b-4b39-9b92-b8db12fa658c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8f19db-7593-4028-864a-644953ff22bc",
                    "LayerId": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86"
                }
            ]
        },
        {
            "id": "2ea68161-c3e3-4a07-befa-002a53c09b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "compositeImage": {
                "id": "6621a5a0-c8ea-4aed-80b3-36b67bace583",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ea68161-c3e3-4a07-befa-002a53c09b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8d7ed4-b4f8-469b-aa39-79d679e0a065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ea68161-c3e3-4a07-befa-002a53c09b80",
                    "LayerId": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86"
                }
            ]
        },
        {
            "id": "3c2e872e-c64e-43a8-9ee1-ae1ba614cd3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "compositeImage": {
                "id": "321cbc32-443f-4f58-b3fb-2039415ffeac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c2e872e-c64e-43a8-9ee1-ae1ba614cd3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d2ddd8-735c-437d-90b8-a17ea023e9fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c2e872e-c64e-43a8-9ee1-ae1ba614cd3a",
                    "LayerId": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86"
                }
            ]
        },
        {
            "id": "99f9bf7b-96e3-428c-9e7d-f29017b07585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "compositeImage": {
                "id": "e2bc4587-31cf-4d2d-8b67-72129b7a41d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f9bf7b-96e3-428c-9e7d-f29017b07585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7e1a449-54ff-4abc-8f90-635a6f712a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f9bf7b-96e3-428c-9e7d-f29017b07585",
                    "LayerId": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "38a3ba9b-9a93-44fb-80d0-6401c1a2be86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7a00d54-c457-4b63-9d12-b63f3f271f28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}