{
    "id": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_hammer_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bacc0217-d5ed-4b63-b650-4f7fe1cf9a2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "compositeImage": {
                "id": "e83f9dce-641a-4d3e-aaa0-b36b5e6d695b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacc0217-d5ed-4b63-b650-4f7fe1cf9a2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83318e10-25fb-406f-9043-01f9d913164b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacc0217-d5ed-4b63-b650-4f7fe1cf9a2d",
                    "LayerId": "54d2dea5-6719-49dd-ac74-40257efe4e2b"
                }
            ]
        },
        {
            "id": "7c19af38-960e-4409-ae9d-3b95ccce04c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "compositeImage": {
                "id": "d746df9b-c6d1-4137-8b85-90566ac62151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c19af38-960e-4409-ae9d-3b95ccce04c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b81b12-ba59-4ad2-a946-61e6d428dc04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c19af38-960e-4409-ae9d-3b95ccce04c0",
                    "LayerId": "54d2dea5-6719-49dd-ac74-40257efe4e2b"
                }
            ]
        },
        {
            "id": "324f22a3-f061-4cbf-8a19-df86a9b4844c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "compositeImage": {
                "id": "1a13da59-f523-4803-9293-b9c875c57c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "324f22a3-f061-4cbf-8a19-df86a9b4844c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e5b618-1319-4359-adab-162b5da5687d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "324f22a3-f061-4cbf-8a19-df86a9b4844c",
                    "LayerId": "54d2dea5-6719-49dd-ac74-40257efe4e2b"
                }
            ]
        },
        {
            "id": "b099856b-0d14-4578-a698-f414c4baacfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "compositeImage": {
                "id": "55d565bc-19be-4e6b-adca-260eb75e5084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b099856b-0d14-4578-a698-f414c4baacfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e980569e-0529-432e-b6c4-354a193a38c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b099856b-0d14-4578-a698-f414c4baacfd",
                    "LayerId": "54d2dea5-6719-49dd-ac74-40257efe4e2b"
                }
            ]
        },
        {
            "id": "39d06a47-36f6-49d1-89b3-85659e499bf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "compositeImage": {
                "id": "1ee26d14-2741-45a3-99bf-97e3735bd612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39d06a47-36f6-49d1-89b3-85659e499bf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482ded71-2270-429b-939f-241c1404e8b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39d06a47-36f6-49d1-89b3-85659e499bf9",
                    "LayerId": "54d2dea5-6719-49dd-ac74-40257efe4e2b"
                }
            ]
        },
        {
            "id": "796e39d8-eca1-4604-930a-2bb72982739a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "compositeImage": {
                "id": "07852021-3e16-4075-a26e-815f60e88c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "796e39d8-eca1-4604-930a-2bb72982739a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e393a82-9078-4d45-bfd5-6e8d64e3cc73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "796e39d8-eca1-4604-930a-2bb72982739a",
                    "LayerId": "54d2dea5-6719-49dd-ac74-40257efe4e2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54d2dea5-6719-49dd-ac74-40257efe4e2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06a6fc5c-e976-4063-8d11-e00a5d903f28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}