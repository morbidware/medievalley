{
    "id": "f596a619-dfe3-42aa-a7b3-551b45f1fcd7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c142ec18-7285-4795-9633-5b60c8492116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f596a619-dfe3-42aa-a7b3-551b45f1fcd7",
            "compositeImage": {
                "id": "03b40004-8d3d-4074-a663-e6cf1b8dc208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c142ec18-7285-4795-9633-5b60c8492116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c352fa-43b9-46d4-9655-86431efa9708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c142ec18-7285-4795-9633-5b60c8492116",
                    "LayerId": "b3916c05-f059-43b8-a3b5-cdab150a686d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b3916c05-f059-43b8-a3b5-cdab150a686d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f596a619-dfe3-42aa-a7b3-551b45f1fcd7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}