{
    "id": "e7beb1bf-7821-4c38-af10-033cc3902955",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4794feeb-d8fa-4d24-b3ce-b2a5d55392cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7beb1bf-7821-4c38-af10-033cc3902955",
            "compositeImage": {
                "id": "b31b71ff-0711-4644-ab45-733574a7e651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4794feeb-d8fa-4d24-b3ce-b2a5d55392cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c188c977-b5fa-4c0b-a27d-215fa7351fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4794feeb-d8fa-4d24-b3ce-b2a5d55392cd",
                    "LayerId": "eae44009-e0e9-4316-b435-ac6544751153"
                }
            ]
        },
        {
            "id": "6f38dd4c-7d0d-4ebb-aff4-6984a9dc0eb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7beb1bf-7821-4c38-af10-033cc3902955",
            "compositeImage": {
                "id": "7c1a714c-e629-4c1c-ba12-632299f05eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f38dd4c-7d0d-4ebb-aff4-6984a9dc0eb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed270cd-3cfb-47ab-8ed4-14485c50111f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f38dd4c-7d0d-4ebb-aff4-6984a9dc0eb9",
                    "LayerId": "eae44009-e0e9-4316-b435-ac6544751153"
                }
            ]
        },
        {
            "id": "8b8a6a33-3472-4c73-b78c-6f82ee357584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7beb1bf-7821-4c38-af10-033cc3902955",
            "compositeImage": {
                "id": "a409a0a9-54ec-4c6c-a35a-327549af2b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b8a6a33-3472-4c73-b78c-6f82ee357584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a23e81d7-f255-4a37-8154-81b3e27e959d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b8a6a33-3472-4c73-b78c-6f82ee357584",
                    "LayerId": "eae44009-e0e9-4316-b435-ac6544751153"
                }
            ]
        },
        {
            "id": "c5005b2d-c1fc-4bec-a212-2a9633f748e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7beb1bf-7821-4c38-af10-033cc3902955",
            "compositeImage": {
                "id": "b7f7cb98-121f-4b73-a9be-e75772f875cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5005b2d-c1fc-4bec-a212-2a9633f748e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5bede6f-83c7-4886-8219-b18cbb7ae56f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5005b2d-c1fc-4bec-a212-2a9633f748e8",
                    "LayerId": "eae44009-e0e9-4316-b435-ac6544751153"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eae44009-e0e9-4316-b435-ac6544751153",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7beb1bf-7821-4c38-af10-033cc3902955",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}