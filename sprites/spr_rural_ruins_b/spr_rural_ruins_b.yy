{
    "id": "6fb94f29-c5c5-4e90-ad9a-ab2a3efe57e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_ruins_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17aebe38-3d52-4d19-8dd7-a240a04a7fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fb94f29-c5c5-4e90-ad9a-ab2a3efe57e3",
            "compositeImage": {
                "id": "91b2162b-42ce-436d-9dcc-1db969fd9b99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17aebe38-3d52-4d19-8dd7-a240a04a7fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e81d8b-8f3c-41c7-94a3-a2267e02bb1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17aebe38-3d52-4d19-8dd7-a240a04a7fd8",
                    "LayerId": "a5796627-55fa-4c39-9ab2-dff1dd7db3cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a5796627-55fa-4c39-9ab2-dff1dd7db3cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fb94f29-c5c5-4e90-ad9a-ab2a3efe57e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}