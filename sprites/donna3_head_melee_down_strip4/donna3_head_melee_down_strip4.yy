{
    "id": "1a5a70c0-e3ce-4489-b94d-7c3c61ac97a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "916d5ff1-0b85-4f2b-ae9a-751cf5f84407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a5a70c0-e3ce-4489-b94d-7c3c61ac97a0",
            "compositeImage": {
                "id": "d6bac7dd-cd0f-4ed5-a884-6e58a07a2267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916d5ff1-0b85-4f2b-ae9a-751cf5f84407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ab243ce-93a0-4372-a8fe-c51b31f099a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916d5ff1-0b85-4f2b-ae9a-751cf5f84407",
                    "LayerId": "90aed082-a98d-418d-945c-9af6f4dabac9"
                }
            ]
        },
        {
            "id": "9d4b2738-729a-41f2-816d-340acecd7e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a5a70c0-e3ce-4489-b94d-7c3c61ac97a0",
            "compositeImage": {
                "id": "462712de-c211-4cce-8ff3-9c7c3bed86ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d4b2738-729a-41f2-816d-340acecd7e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83822ea4-3a65-4622-a6ee-0baf4377e6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d4b2738-729a-41f2-816d-340acecd7e26",
                    "LayerId": "90aed082-a98d-418d-945c-9af6f4dabac9"
                }
            ]
        },
        {
            "id": "1f5c1394-0e0e-49ed-a3e8-05056cc3319e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a5a70c0-e3ce-4489-b94d-7c3c61ac97a0",
            "compositeImage": {
                "id": "beed2315-1d50-4f2a-9480-fadc2b497cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5c1394-0e0e-49ed-a3e8-05056cc3319e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7111b7d4-7776-4244-bcbe-6011760ab7c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5c1394-0e0e-49ed-a3e8-05056cc3319e",
                    "LayerId": "90aed082-a98d-418d-945c-9af6f4dabac9"
                }
            ]
        },
        {
            "id": "25a2b9ea-a0eb-4e02-90d6-531f95f3734c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a5a70c0-e3ce-4489-b94d-7c3c61ac97a0",
            "compositeImage": {
                "id": "8f12dd83-41dc-49db-a6c8-b938ac219956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a2b9ea-a0eb-4e02-90d6-531f95f3734c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ceb46e7-6725-4f8d-93f4-51ce801d1e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a2b9ea-a0eb-4e02-90d6-531f95f3734c",
                    "LayerId": "90aed082-a98d-418d-945c-9af6f4dabac9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "90aed082-a98d-418d-945c-9af6f4dabac9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a5a70c0-e3ce-4489-b94d-7c3c61ac97a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}