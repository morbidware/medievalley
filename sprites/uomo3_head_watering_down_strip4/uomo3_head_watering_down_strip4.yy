{
    "id": "36d8b6c1-cd3c-435d-97f9-dd52403f1e62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7009e492-ccb1-4c24-8c29-3b3a4d0c324b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d8b6c1-cd3c-435d-97f9-dd52403f1e62",
            "compositeImage": {
                "id": "77b304e0-8c1a-4af1-83e9-36d2d5fa04db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7009e492-ccb1-4c24-8c29-3b3a4d0c324b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55e07c5-b45a-447b-8826-e91278200136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7009e492-ccb1-4c24-8c29-3b3a4d0c324b",
                    "LayerId": "b3e7cd38-97b6-4b0e-8dd9-476c87e4f2f1"
                }
            ]
        },
        {
            "id": "46c6201c-3eac-4bc2-9a0e-b90516acfaaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d8b6c1-cd3c-435d-97f9-dd52403f1e62",
            "compositeImage": {
                "id": "540a6f8c-7652-4ea0-be5e-8cec0b5fd66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c6201c-3eac-4bc2-9a0e-b90516acfaaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a46611-98de-44b2-aadb-a09c03496f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c6201c-3eac-4bc2-9a0e-b90516acfaaf",
                    "LayerId": "b3e7cd38-97b6-4b0e-8dd9-476c87e4f2f1"
                }
            ]
        },
        {
            "id": "2f6853a5-7631-4b48-9cbf-288ad521321c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d8b6c1-cd3c-435d-97f9-dd52403f1e62",
            "compositeImage": {
                "id": "476737b0-47c8-417e-b294-eb5ec3131386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6853a5-7631-4b48-9cbf-288ad521321c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9837cb08-8853-4d61-ab3f-84235ce867ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6853a5-7631-4b48-9cbf-288ad521321c",
                    "LayerId": "b3e7cd38-97b6-4b0e-8dd9-476c87e4f2f1"
                }
            ]
        },
        {
            "id": "ee58bc80-aff8-4962-8ac1-f0c9131874cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d8b6c1-cd3c-435d-97f9-dd52403f1e62",
            "compositeImage": {
                "id": "0020d44b-d939-4874-bcf2-d19c68347f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee58bc80-aff8-4962-8ac1-f0c9131874cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c9fd5ff-8ddb-403c-8df3-181018b676de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee58bc80-aff8-4962-8ac1-f0c9131874cc",
                    "LayerId": "b3e7cd38-97b6-4b0e-8dd9-476c87e4f2f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b3e7cd38-97b6-4b0e-8dd9-476c87e4f2f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36d8b6c1-cd3c-435d-97f9-dd52403f1e62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}