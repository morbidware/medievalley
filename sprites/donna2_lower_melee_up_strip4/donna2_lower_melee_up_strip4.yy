{
    "id": "4cede4f9-a092-4efc-9ba6-4e508ee6060b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd2b4c8a-39a9-45d7-bb91-e1260586039d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cede4f9-a092-4efc-9ba6-4e508ee6060b",
            "compositeImage": {
                "id": "f17ace9b-1a41-4c89-8096-112276ba9a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2b4c8a-39a9-45d7-bb91-e1260586039d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf1fdf7f-89ce-4f4b-badf-ca81fa9d4ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2b4c8a-39a9-45d7-bb91-e1260586039d",
                    "LayerId": "d1347ff1-a39c-45ad-a68d-70f53852b306"
                }
            ]
        },
        {
            "id": "7ec8bd5a-8806-4f73-b853-edbf22e6082c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cede4f9-a092-4efc-9ba6-4e508ee6060b",
            "compositeImage": {
                "id": "0d98ff24-75ff-41ad-bffd-32e036e709e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ec8bd5a-8806-4f73-b853-edbf22e6082c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fb08c50-59f2-4b19-9371-f58a8ff01465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ec8bd5a-8806-4f73-b853-edbf22e6082c",
                    "LayerId": "d1347ff1-a39c-45ad-a68d-70f53852b306"
                }
            ]
        },
        {
            "id": "553c1386-7506-4dc6-bd9e-f636f01fed58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cede4f9-a092-4efc-9ba6-4e508ee6060b",
            "compositeImage": {
                "id": "671170b0-d5c5-4aa5-ae46-d93833508d40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553c1386-7506-4dc6-bd9e-f636f01fed58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "884f1b92-410a-4a54-98ab-de6220496655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553c1386-7506-4dc6-bd9e-f636f01fed58",
                    "LayerId": "d1347ff1-a39c-45ad-a68d-70f53852b306"
                }
            ]
        },
        {
            "id": "ee6f404e-4c71-4646-9b6e-139124b8f0e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cede4f9-a092-4efc-9ba6-4e508ee6060b",
            "compositeImage": {
                "id": "435d0c73-d282-4efc-8431-5352701c6dba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee6f404e-4c71-4646-9b6e-139124b8f0e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b51123b9-e1ce-4f18-bbf0-29e962d3a44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee6f404e-4c71-4646-9b6e-139124b8f0e2",
                    "LayerId": "d1347ff1-a39c-45ad-a68d-70f53852b306"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1347ff1-a39c-45ad-a68d-70f53852b306",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cede4f9-a092-4efc-9ba6-4e508ee6060b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}