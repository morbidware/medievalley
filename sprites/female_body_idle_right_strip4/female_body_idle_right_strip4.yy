{
    "id": "9adfdc40-f642-4c24-896f-80d2b8c383c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7160a1a4-2949-42b1-94fb-a2a6fa806c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9adfdc40-f642-4c24-896f-80d2b8c383c0",
            "compositeImage": {
                "id": "e98455a3-39ff-4a53-b35c-74f8f68da95b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7160a1a4-2949-42b1-94fb-a2a6fa806c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "becdeb43-009a-4069-8954-2a348526d3ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7160a1a4-2949-42b1-94fb-a2a6fa806c9e",
                    "LayerId": "7d98c2e3-c27d-4837-81c2-af1edf228907"
                }
            ]
        },
        {
            "id": "7fe8d385-c747-41c5-865d-902567c647e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9adfdc40-f642-4c24-896f-80d2b8c383c0",
            "compositeImage": {
                "id": "cd5d9a69-e7ba-493b-99cb-189d0497d44e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe8d385-c747-41c5-865d-902567c647e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf55b45-4e22-4590-ab09-433c6f5a598e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe8d385-c747-41c5-865d-902567c647e2",
                    "LayerId": "7d98c2e3-c27d-4837-81c2-af1edf228907"
                }
            ]
        },
        {
            "id": "44cf10ce-933c-46a4-a087-10edff12438d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9adfdc40-f642-4c24-896f-80d2b8c383c0",
            "compositeImage": {
                "id": "19079717-c492-4f65-83f5-41d033ee8589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44cf10ce-933c-46a4-a087-10edff12438d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d791ba-e067-4846-a9f8-ec42eccbbede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44cf10ce-933c-46a4-a087-10edff12438d",
                    "LayerId": "7d98c2e3-c27d-4837-81c2-af1edf228907"
                }
            ]
        },
        {
            "id": "f4b21b56-6604-4a54-9be5-67950fae80fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9adfdc40-f642-4c24-896f-80d2b8c383c0",
            "compositeImage": {
                "id": "32c601bd-5bb2-4354-9244-a254e118ed23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b21b56-6604-4a54-9be5-67950fae80fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29f0be2-6ff0-4867-8085-22c09a875f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b21b56-6604-4a54-9be5-67950fae80fa",
                    "LayerId": "7d98c2e3-c27d-4837-81c2-af1edf228907"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7d98c2e3-c27d-4837-81c2-af1edf228907",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9adfdc40-f642-4c24-896f-80d2b8c383c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}