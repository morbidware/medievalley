{
    "id": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_melee_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b06b14f6-9680-492d-8a7b-e51cac173483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "compositeImage": {
                "id": "4a6aa451-09c9-4c0a-9888-198d04cfa6de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b06b14f6-9680-492d-8a7b-e51cac173483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4aead2-1d82-4085-89ea-2f80df98a3d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b06b14f6-9680-492d-8a7b-e51cac173483",
                    "LayerId": "c02a2efe-6074-4f31-8cd2-e5e8832f365a"
                }
            ]
        },
        {
            "id": "812ca05e-22b5-4843-b743-ef7a7232df90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "compositeImage": {
                "id": "d97c7ef4-2f12-49f9-b92f-131216826ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "812ca05e-22b5-4843-b743-ef7a7232df90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132d9037-66f4-4758-aad2-702e08f428a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "812ca05e-22b5-4843-b743-ef7a7232df90",
                    "LayerId": "c02a2efe-6074-4f31-8cd2-e5e8832f365a"
                }
            ]
        },
        {
            "id": "bac4f3f3-0c20-4bc1-bbf1-754ed888d916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "compositeImage": {
                "id": "1bdad32d-2a0f-4fbb-97fb-59004786e247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bac4f3f3-0c20-4bc1-bbf1-754ed888d916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20aaca34-00c0-4076-8e6a-af8392235060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bac4f3f3-0c20-4bc1-bbf1-754ed888d916",
                    "LayerId": "c02a2efe-6074-4f31-8cd2-e5e8832f365a"
                }
            ]
        },
        {
            "id": "f0e802dd-e227-4df3-a748-b4e201e1aa81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "compositeImage": {
                "id": "ff1fbcd8-5da0-4991-b4a1-741d5aec20f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0e802dd-e227-4df3-a748-b4e201e1aa81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b60f19d0-c9fd-4fc2-bfea-92499d470562",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0e802dd-e227-4df3-a748-b4e201e1aa81",
                    "LayerId": "c02a2efe-6074-4f31-8cd2-e5e8832f365a"
                }
            ]
        },
        {
            "id": "9290aea1-f522-4e26-9d40-1d352885f0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "compositeImage": {
                "id": "f132ee1b-e701-46be-94e9-fb3749e6d9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9290aea1-f522-4e26-9d40-1d352885f0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ae4650-1c54-452d-8dbb-c5cec2d35938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9290aea1-f522-4e26-9d40-1d352885f0f1",
                    "LayerId": "c02a2efe-6074-4f31-8cd2-e5e8832f365a"
                }
            ]
        },
        {
            "id": "1e3749ce-8898-4224-b6a2-9252e7692aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "compositeImage": {
                "id": "d5f130d6-b57e-4605-ac9f-445ce4218e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e3749ce-8898-4224-b6a2-9252e7692aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42267d48-1ff6-4f72-af25-b78ee93264fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e3749ce-8898-4224-b6a2-9252e7692aac",
                    "LayerId": "c02a2efe-6074-4f31-8cd2-e5e8832f365a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c02a2efe-6074-4f31-8cd2-e5e8832f365a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad37fbc3-6fa5-4049-a3af-3447959f90c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}