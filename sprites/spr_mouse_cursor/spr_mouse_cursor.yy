{
    "id": "0682a057-7e4b-4499-9ee3-139304a1d58b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "138046d5-d7f2-4f08-b681-9fd126b96589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0682a057-7e4b-4499-9ee3-139304a1d58b",
            "compositeImage": {
                "id": "9e7407ef-85cd-432c-9a48-f5a7f20e8275",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "138046d5-d7f2-4f08-b681-9fd126b96589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afbf53d6-ffcb-4098-80c3-cd3f76bc2b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "138046d5-d7f2-4f08-b681-9fd126b96589",
                    "LayerId": "ea54f8ce-5862-4372-bc69-ba48f48216e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "ea54f8ce-5862-4372-bc69-ba48f48216e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0682a057-7e4b-4499-9ee3-139304a1d58b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}