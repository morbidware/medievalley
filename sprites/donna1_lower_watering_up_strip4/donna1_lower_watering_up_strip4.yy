{
    "id": "1431ddd7-0a56-4ed2-9a8d-4f943e990578",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64a3066d-242e-4c41-ab7f-cbeba4328d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1431ddd7-0a56-4ed2-9a8d-4f943e990578",
            "compositeImage": {
                "id": "5d7b0888-d629-4208-bbfb-bd97184915de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a3066d-242e-4c41-ab7f-cbeba4328d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a631a1-37d6-48f3-aecf-6d934698ad59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a3066d-242e-4c41-ab7f-cbeba4328d84",
                    "LayerId": "0da5dd07-a9ac-461e-901d-d5eb7ff1e2c1"
                }
            ]
        },
        {
            "id": "08cee88e-331e-4d28-9d78-5f4f88d2c784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1431ddd7-0a56-4ed2-9a8d-4f943e990578",
            "compositeImage": {
                "id": "02dc0524-6524-42e1-a088-418441969558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08cee88e-331e-4d28-9d78-5f4f88d2c784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c69b6e-9aa3-4d90-be80-c952c6c19962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08cee88e-331e-4d28-9d78-5f4f88d2c784",
                    "LayerId": "0da5dd07-a9ac-461e-901d-d5eb7ff1e2c1"
                }
            ]
        },
        {
            "id": "3b38bc04-340d-4050-ac6c-0d36348262ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1431ddd7-0a56-4ed2-9a8d-4f943e990578",
            "compositeImage": {
                "id": "ec09d9d3-77cb-4cb2-b28f-960fca090e47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b38bc04-340d-4050-ac6c-0d36348262ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2b6087-e794-4b08-bc8d-73aafbed85db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b38bc04-340d-4050-ac6c-0d36348262ed",
                    "LayerId": "0da5dd07-a9ac-461e-901d-d5eb7ff1e2c1"
                }
            ]
        },
        {
            "id": "699904e4-24ed-44fd-a290-d9753e7e7f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1431ddd7-0a56-4ed2-9a8d-4f943e990578",
            "compositeImage": {
                "id": "7fe708e6-2bdd-4aaa-96a9-9be7fb0a6a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699904e4-24ed-44fd-a290-d9753e7e7f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199967a0-1aba-4d37-9c30-47ac106061d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699904e4-24ed-44fd-a290-d9753e7e7f85",
                    "LayerId": "0da5dd07-a9ac-461e-901d-d5eb7ff1e2c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0da5dd07-a9ac-461e-901d-d5eb7ff1e2c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1431ddd7-0a56-4ed2-9a8d-4f943e990578",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}