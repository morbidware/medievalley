{
    "id": "9b7b248f-4a32-4938-a02f-71bb1282ad5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb64a2e5-0bce-404c-92f0-b147aa1bdd9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b7b248f-4a32-4938-a02f-71bb1282ad5d",
            "compositeImage": {
                "id": "0079aa36-f3e6-4125-bf1c-dcb52447629d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb64a2e5-0bce-404c-92f0-b147aa1bdd9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c4c745-4574-4d00-a959-bf3a324deb6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb64a2e5-0bce-404c-92f0-b147aa1bdd9b",
                    "LayerId": "5fd80e3e-53b6-48c3-9e6f-ad295d12c766"
                }
            ]
        },
        {
            "id": "5fbc8345-52e2-4f97-bf94-deca856940a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b7b248f-4a32-4938-a02f-71bb1282ad5d",
            "compositeImage": {
                "id": "ba24e698-f684-4c0e-bc29-938647607c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fbc8345-52e2-4f97-bf94-deca856940a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e37cd4a-5023-49ca-bcc9-0f82a8f44715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fbc8345-52e2-4f97-bf94-deca856940a8",
                    "LayerId": "5fd80e3e-53b6-48c3-9e6f-ad295d12c766"
                }
            ]
        },
        {
            "id": "20d4a42b-5e25-4e8f-a731-a9cc5299971a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b7b248f-4a32-4938-a02f-71bb1282ad5d",
            "compositeImage": {
                "id": "103a595c-90e8-474f-b6f9-6d07da4fce4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d4a42b-5e25-4e8f-a731-a9cc5299971a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb8115d2-6252-4228-bfd3-8f4059913e82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d4a42b-5e25-4e8f-a731-a9cc5299971a",
                    "LayerId": "5fd80e3e-53b6-48c3-9e6f-ad295d12c766"
                }
            ]
        },
        {
            "id": "52159af0-81fc-42dc-b6f2-1bb757226796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b7b248f-4a32-4938-a02f-71bb1282ad5d",
            "compositeImage": {
                "id": "343ed8d1-13cc-401a-9253-1864f6cc516b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52159af0-81fc-42dc-b6f2-1bb757226796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4c78b00-4502-4c24-9db2-cb90a878c110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52159af0-81fc-42dc-b6f2-1bb757226796",
                    "LayerId": "5fd80e3e-53b6-48c3-9e6f-ad295d12c766"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5fd80e3e-53b6-48c3-9e6f-ad295d12c766",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b7b248f-4a32-4938-a02f-71bb1282ad5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}