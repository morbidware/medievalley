{
    "id": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_watering_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d95d5b2-9bbc-42a2-9ddb-e8a5a5878b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "compositeImage": {
                "id": "f049e3c5-7cd6-426b-a890-c01af2d4e82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d95d5b2-9bbc-42a2-9ddb-e8a5a5878b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bdb8952-9b12-4e28-b5c4-27ff3bd0e1e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d95d5b2-9bbc-42a2-9ddb-e8a5a5878b00",
                    "LayerId": "024f9bc3-6183-47d8-b1d1-601196f2b221"
                }
            ]
        },
        {
            "id": "074259e6-2258-4c30-9434-82560d90c58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "compositeImage": {
                "id": "dd02f1da-a316-4726-afe9-8686fd1c9389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074259e6-2258-4c30-9434-82560d90c58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925a0754-87ff-4ac3-9e20-744fbbf82f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074259e6-2258-4c30-9434-82560d90c58c",
                    "LayerId": "024f9bc3-6183-47d8-b1d1-601196f2b221"
                }
            ]
        },
        {
            "id": "60f9e74a-ab34-49c1-84c2-1d5887f34e6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "compositeImage": {
                "id": "37bbebc5-6eff-4cfa-ba1c-b54e8abd6cc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60f9e74a-ab34-49c1-84c2-1d5887f34e6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3738f414-7213-4283-8cb9-c39f19f6d973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60f9e74a-ab34-49c1-84c2-1d5887f34e6d",
                    "LayerId": "024f9bc3-6183-47d8-b1d1-601196f2b221"
                }
            ]
        },
        {
            "id": "cbe8b194-cf0d-458a-98c4-d398d650b528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "compositeImage": {
                "id": "e0354087-8bd1-49af-a3c2-ba4bb626294a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbe8b194-cf0d-458a-98c4-d398d650b528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b833c05c-0815-4499-bf92-744ed57ea575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbe8b194-cf0d-458a-98c4-d398d650b528",
                    "LayerId": "024f9bc3-6183-47d8-b1d1-601196f2b221"
                }
            ]
        },
        {
            "id": "ab4bf862-940d-42e4-b690-c96e303ce115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "compositeImage": {
                "id": "e70e4fd1-727b-4bd4-8ad0-e179934fa353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab4bf862-940d-42e4-b690-c96e303ce115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbe9f7a6-ce6a-4d12-aa72-43007538eeab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab4bf862-940d-42e4-b690-c96e303ce115",
                    "LayerId": "024f9bc3-6183-47d8-b1d1-601196f2b221"
                }
            ]
        },
        {
            "id": "a8fe5c92-b1b6-43d4-9c4b-691092e04096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "compositeImage": {
                "id": "bb4cb5b1-4360-451c-a027-6ecbdc905b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8fe5c92-b1b6-43d4-9c4b-691092e04096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec3a4c73-5425-4643-bf7f-7ef006e6d006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8fe5c92-b1b6-43d4-9c4b-691092e04096",
                    "LayerId": "024f9bc3-6183-47d8-b1d1-601196f2b221"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "024f9bc3-6183-47d8-b1d1-601196f2b221",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55f0ea2a-b84a-4c61-90e4-3790dd596195",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}