{
    "id": "07a11fa7-0ca5-478c-afa2-0c5fd694c2ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vpad_button_area",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dd2ecfa-66b3-49e8-8484-b70535072d62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a11fa7-0ca5-478c-afa2-0c5fd694c2ba",
            "compositeImage": {
                "id": "d7a3607f-da89-4ad3-a8f5-5a3760ddca57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd2ecfa-66b3-49e8-8484-b70535072d62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d633275a-5ee8-4b63-9d7f-5fdef906cefd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd2ecfa-66b3-49e8-8484-b70535072d62",
                    "LayerId": "d399f4dd-764c-4597-ae8f-6ed34cbe60d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d399f4dd-764c-4597-ae8f-6ed34cbe60d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a11fa7-0ca5-478c-afa2-0c5fd694c2ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}