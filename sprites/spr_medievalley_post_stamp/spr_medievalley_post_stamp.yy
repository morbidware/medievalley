{
    "id": "ec6c7984-cbe2-4fc0-9160-9d371e08cab0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_medievalley_post_stamp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14a88161-ede6-4c0f-89fb-9bf3fc495b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec6c7984-cbe2-4fc0-9160-9d371e08cab0",
            "compositeImage": {
                "id": "937072d2-cc7f-4dfd-8b89-aadcb312f22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14a88161-ede6-4c0f-89fb-9bf3fc495b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ace187-8329-4065-a19a-38dca6af8c6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14a88161-ede6-4c0f-89fb-9bf3fc495b5d",
                    "LayerId": "bc25a26f-9744-4da2-a5a5-521903a08fc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "bc25a26f-9744-4da2-a5a5-521903a08fc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec6c7984-cbe2-4fc0-9160-9d371e08cab0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 63,
    "xorig": 31,
    "yorig": 21
}