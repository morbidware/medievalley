{
    "id": "469aea75-7dee-439f-857a-8fe667c99f31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00705ddc-eba2-4b0b-9e55-ffdc22e97230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "469aea75-7dee-439f-857a-8fe667c99f31",
            "compositeImage": {
                "id": "7c3c9858-3c1f-49a6-a8ae-67aa88234652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00705ddc-eba2-4b0b-9e55-ffdc22e97230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14edbc67-3bb4-417e-8d79-9d26a78a1d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00705ddc-eba2-4b0b-9e55-ffdc22e97230",
                    "LayerId": "7c7979ef-bf4e-4aa6-ae51-a26446d6aae4"
                }
            ]
        },
        {
            "id": "7e3c0d3f-92b9-437b-aaa4-bcee9f6ad14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "469aea75-7dee-439f-857a-8fe667c99f31",
            "compositeImage": {
                "id": "1b77eb79-378d-4630-9741-36d5212f5fe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e3c0d3f-92b9-437b-aaa4-bcee9f6ad14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d977383c-fbf9-494a-9964-26ff71101341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e3c0d3f-92b9-437b-aaa4-bcee9f6ad14e",
                    "LayerId": "7c7979ef-bf4e-4aa6-ae51-a26446d6aae4"
                }
            ]
        },
        {
            "id": "9e213b0c-7902-4a58-95b2-ce6818421565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "469aea75-7dee-439f-857a-8fe667c99f31",
            "compositeImage": {
                "id": "49a7700a-ad44-4045-b812-5edb60a3485e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e213b0c-7902-4a58-95b2-ce6818421565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca454807-85a8-4c15-8d16-91ef65d9d9b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e213b0c-7902-4a58-95b2-ce6818421565",
                    "LayerId": "7c7979ef-bf4e-4aa6-ae51-a26446d6aae4"
                }
            ]
        },
        {
            "id": "21d531d3-8e7f-44ad-978a-561f9948ecf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "469aea75-7dee-439f-857a-8fe667c99f31",
            "compositeImage": {
                "id": "c9b63d33-b446-4497-953b-884e0a4ce4fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d531d3-8e7f-44ad-978a-561f9948ecf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "198c9bd2-d808-4181-92f4-02efdbd63f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d531d3-8e7f-44ad-978a-561f9948ecf1",
                    "LayerId": "7c7979ef-bf4e-4aa6-ae51-a26446d6aae4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7c7979ef-bf4e-4aa6-ae51-a26446d6aae4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "469aea75-7dee-439f-857a-8fe667c99f31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}