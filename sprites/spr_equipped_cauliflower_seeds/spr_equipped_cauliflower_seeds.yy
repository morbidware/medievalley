{
    "id": "52d208a0-2f1a-487a-9215-4d3d0a185b63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_cauliflower_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5e4ffa3-76cb-40f2-962a-5102912854d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52d208a0-2f1a-487a-9215-4d3d0a185b63",
            "compositeImage": {
                "id": "9cb3c1e9-82ac-4ec0-ae1c-d6eae97647f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e4ffa3-76cb-40f2-962a-5102912854d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e23db632-bcc6-4ed6-a1e7-bc9e83ea9888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e4ffa3-76cb-40f2-962a-5102912854d0",
                    "LayerId": "021ddc38-0322-484b-a214-20864630b21e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "021ddc38-0322-484b-a214-20864630b21e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52d208a0-2f1a-487a-9215-4d3d0a185b63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}