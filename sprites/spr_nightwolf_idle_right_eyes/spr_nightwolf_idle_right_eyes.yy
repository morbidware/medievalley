{
    "id": "10066a55-6251-48d8-b8ab-abc960de8043",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_idle_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 41,
    "bbox_right": 41,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d462e20-3d78-437c-9c5e-e96052f923ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10066a55-6251-48d8-b8ab-abc960de8043",
            "compositeImage": {
                "id": "70ff8e54-26d7-4b63-9c4d-12ae3dd03063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d462e20-3d78-437c-9c5e-e96052f923ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788d67a3-c2d3-4728-a8b0-48ae68a2b5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d462e20-3d78-437c-9c5e-e96052f923ca",
                    "LayerId": "460981d8-b27e-4112-ab0d-5f1d813970a2"
                }
            ]
        },
        {
            "id": "eec60b3c-0bf4-4ecb-af06-53033f1cca78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10066a55-6251-48d8-b8ab-abc960de8043",
            "compositeImage": {
                "id": "bfee9189-6a6c-4516-aad3-847aaf5eb3e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eec60b3c-0bf4-4ecb-af06-53033f1cca78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcdc8acb-1afc-42d3-a66a-6b03351ef9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eec60b3c-0bf4-4ecb-af06-53033f1cca78",
                    "LayerId": "460981d8-b27e-4112-ab0d-5f1d813970a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "460981d8-b27e-4112-ab0d-5f1d813970a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10066a55-6251-48d8-b8ab-abc960de8043",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}