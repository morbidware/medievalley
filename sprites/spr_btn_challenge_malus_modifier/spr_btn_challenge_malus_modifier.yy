{
    "id": "f76d45fd-f6a7-4ced-88f7-91521ead374b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_challenge_malus_modifier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "048859a6-d0d7-47dc-bd3a-b429d00b0870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f76d45fd-f6a7-4ced-88f7-91521ead374b",
            "compositeImage": {
                "id": "61ec7eb2-eaf8-4000-87a2-cf0623af10e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048859a6-d0d7-47dc-bd3a-b429d00b0870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a2e165-2e8a-42fe-a963-808d7bb93ecd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048859a6-d0d7-47dc-bd3a-b429d00b0870",
                    "LayerId": "2d8db7df-d64c-4e4f-89a0-cb8e58c1e928"
                }
            ]
        },
        {
            "id": "6ccfd05a-83fe-4f47-8d27-3ab86bf606e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f76d45fd-f6a7-4ced-88f7-91521ead374b",
            "compositeImage": {
                "id": "50ac74b8-2fa8-46c4-a3e2-5e1fcf8718e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ccfd05a-83fe-4f47-8d27-3ab86bf606e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a090d57a-dd21-40e5-94f0-a538bb4fdfcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ccfd05a-83fe-4f47-8d27-3ab86bf606e8",
                    "LayerId": "2d8db7df-d64c-4e4f-89a0-cb8e58c1e928"
                }
            ]
        },
        {
            "id": "869502c1-02c5-4d8d-8b1c-7900703837a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f76d45fd-f6a7-4ced-88f7-91521ead374b",
            "compositeImage": {
                "id": "c99914c5-5f52-4b27-b938-25c209d3fcbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869502c1-02c5-4d8d-8b1c-7900703837a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9ee1b0-9012-45a8-ab4c-00da6b4ba81c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869502c1-02c5-4d8d-8b1c-7900703837a4",
                    "LayerId": "2d8db7df-d64c-4e4f-89a0-cb8e58c1e928"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "2d8db7df-d64c-4e4f-89a0-cb8e58c1e928",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f76d45fd-f6a7-4ced-88f7-91521ead374b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}