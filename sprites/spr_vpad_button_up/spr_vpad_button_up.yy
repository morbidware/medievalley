{
    "id": "4f2009ec-cccb-4920-a88a-68be6e483533",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vpad_button_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e4e4c79-57c4-4a6b-a563-a6d5aa06a747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f2009ec-cccb-4920-a88a-68be6e483533",
            "compositeImage": {
                "id": "58699834-f2f9-4e42-a25f-8306b841ddf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4e4c79-57c4-4a6b-a563-a6d5aa06a747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d51a4d9f-0320-485f-bd50-529b4121b482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4e4c79-57c4-4a6b-a563-a6d5aa06a747",
                    "LayerId": "e22be5e9-c02a-433e-8cb1-69baf76766c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e22be5e9-c02a-433e-8cb1-69baf76766c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f2009ec-cccb-4920-a88a-68be6e483533",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}