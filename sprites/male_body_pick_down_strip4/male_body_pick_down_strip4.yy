{
    "id": "75637386-7019-4dc4-b33c-3a2e2eaa2e2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b4a9577-e761-4f75-882a-871b649743d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75637386-7019-4dc4-b33c-3a2e2eaa2e2e",
            "compositeImage": {
                "id": "3d58a65f-c41c-4921-ad5d-ee03d30efbe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b4a9577-e761-4f75-882a-871b649743d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e104f35f-ec84-4423-9770-70ba4d187d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b4a9577-e761-4f75-882a-871b649743d4",
                    "LayerId": "4301c79f-0c7c-4ac3-a71e-b35870bbbadb"
                }
            ]
        },
        {
            "id": "8cf9e223-0a4f-4366-a95a-62379d6bca15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75637386-7019-4dc4-b33c-3a2e2eaa2e2e",
            "compositeImage": {
                "id": "ec2de07e-a81d-4fb3-8308-99c7054c1641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cf9e223-0a4f-4366-a95a-62379d6bca15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70f3060-7b7e-4f94-82d3-648697ce97c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cf9e223-0a4f-4366-a95a-62379d6bca15",
                    "LayerId": "4301c79f-0c7c-4ac3-a71e-b35870bbbadb"
                }
            ]
        },
        {
            "id": "d1279879-b6fc-4104-b517-4745de6739a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75637386-7019-4dc4-b33c-3a2e2eaa2e2e",
            "compositeImage": {
                "id": "f863b2a9-162e-406e-83cc-3e128f67483c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1279879-b6fc-4104-b517-4745de6739a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5d7c5b-138d-419f-b742-e1d27e514a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1279879-b6fc-4104-b517-4745de6739a9",
                    "LayerId": "4301c79f-0c7c-4ac3-a71e-b35870bbbadb"
                }
            ]
        },
        {
            "id": "7fbc7e4a-6eb3-4d6c-becf-e5c40449475f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75637386-7019-4dc4-b33c-3a2e2eaa2e2e",
            "compositeImage": {
                "id": "5f1175da-469f-426c-9e67-cd7efa89bcae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbc7e4a-6eb3-4d6c-becf-e5c40449475f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44b22d39-8e20-4234-b4d8-c07942f25f94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbc7e4a-6eb3-4d6c-becf-e5c40449475f",
                    "LayerId": "4301c79f-0c7c-4ac3-a71e-b35870bbbadb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4301c79f-0c7c-4ac3-a71e-b35870bbbadb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75637386-7019-4dc4-b33c-3a2e2eaa2e2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}