{
    "id": "fdfd3bcc-ef3f-43fa-abac-5106ea2a9583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0920e11-8401-4902-821d-af731ca24511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdfd3bcc-ef3f-43fa-abac-5106ea2a9583",
            "compositeImage": {
                "id": "1e76b694-787d-4e44-9806-7ef25a31adc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0920e11-8401-4902-821d-af731ca24511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "185a9817-7154-4ad8-bae9-28e410403926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0920e11-8401-4902-821d-af731ca24511",
                    "LayerId": "1db9f2ad-62c5-4cbf-88d5-7bd723f3f557"
                }
            ]
        },
        {
            "id": "240e5e1c-275c-4bf4-8c56-260c79d3bb9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdfd3bcc-ef3f-43fa-abac-5106ea2a9583",
            "compositeImage": {
                "id": "eb4d762a-6a62-4d48-80f9-20530f3c5642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240e5e1c-275c-4bf4-8c56-260c79d3bb9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd656c9-039c-46e5-bbba-7dbaa92669a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240e5e1c-275c-4bf4-8c56-260c79d3bb9f",
                    "LayerId": "1db9f2ad-62c5-4cbf-88d5-7bd723f3f557"
                }
            ]
        },
        {
            "id": "92e35c41-8ed9-402c-908a-486c3b85c0a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdfd3bcc-ef3f-43fa-abac-5106ea2a9583",
            "compositeImage": {
                "id": "e62ba6ec-0d71-4906-a755-325c93a18c93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e35c41-8ed9-402c-908a-486c3b85c0a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5920b3b-696c-4451-832a-8eed21b8fe50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e35c41-8ed9-402c-908a-486c3b85c0a8",
                    "LayerId": "1db9f2ad-62c5-4cbf-88d5-7bd723f3f557"
                }
            ]
        },
        {
            "id": "25415c58-701a-42ba-9e84-cf60f595044b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdfd3bcc-ef3f-43fa-abac-5106ea2a9583",
            "compositeImage": {
                "id": "5d978b95-d258-4a50-a271-9b7fcb49e22a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25415c58-701a-42ba-9e84-cf60f595044b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79e5b42-3cd6-47a6-98a8-9b2943edb8af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25415c58-701a-42ba-9e84-cf60f595044b",
                    "LayerId": "1db9f2ad-62c5-4cbf-88d5-7bd723f3f557"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1db9f2ad-62c5-4cbf-88d5-7bd723f3f557",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdfd3bcc-ef3f-43fa-abac-5106ea2a9583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}