{
    "id": "06ad7c79-910c-4caf-9d62-562a7771f21b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_selling_npc_potions_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22ad9903-7d96-4236-9644-d4c42801ebcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06ad7c79-910c-4caf-9d62-562a7771f21b",
            "compositeImage": {
                "id": "cd239e30-b7ad-47a5-ab60-a482c677fea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ad9903-7d96-4236-9644-d4c42801ebcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf44ddc1-1332-45ea-9fbc-1fdb49936ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ad9903-7d96-4236-9644-d4c42801ebcf",
                    "LayerId": "9f8109a3-f216-4fbd-b1a7-9713c71b19a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f8109a3-f216-4fbd-b1a7-9713c71b19a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06ad7c79-910c-4caf-9d62-562a7771f21b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}