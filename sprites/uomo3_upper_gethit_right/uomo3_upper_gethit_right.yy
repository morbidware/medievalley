{
    "id": "1d6d9697-73e8-430d-afcb-a8e8c53548c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "161d7adb-fa9f-4579-aabc-6d15c36c7d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d6d9697-73e8-430d-afcb-a8e8c53548c4",
            "compositeImage": {
                "id": "03855009-a60e-431d-9e1d-c64013a4c976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "161d7adb-fa9f-4579-aabc-6d15c36c7d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4f38c8-feac-4768-81dc-254d746baa3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "161d7adb-fa9f-4579-aabc-6d15c36c7d90",
                    "LayerId": "ba7dced3-b909-48b4-b48b-f9319c9558ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba7dced3-b909-48b4-b48b-f9319c9558ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d6d9697-73e8-430d-afcb-a8e8c53548c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}