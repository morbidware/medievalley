{
    "id": "98c5b504-0b13-470a-8885-06db38aec419",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5041fa4f-68cd-4ff3-b5dc-37626a775737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98c5b504-0b13-470a-8885-06db38aec419",
            "compositeImage": {
                "id": "6a19ddfd-edfe-48c0-a769-a549f0606a53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5041fa4f-68cd-4ff3-b5dc-37626a775737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca4b146-a2c5-4b95-b74d-f670e07b873a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5041fa4f-68cd-4ff3-b5dc-37626a775737",
                    "LayerId": "1ccb9a8b-75e6-4e01-bf25-8c7933ea7295"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ccb9a8b-75e6-4e01-bf25-8c7933ea7295",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98c5b504-0b13-470a-8885-06db38aec419",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}