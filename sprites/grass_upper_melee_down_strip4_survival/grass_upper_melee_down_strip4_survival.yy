{
    "id": "10c95fe2-e161-4de3-854a-cce5b4253863",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_melee_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "285099fe-5ad0-4d65-b479-6bd5bd6874c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "compositeImage": {
                "id": "39aa99f5-1fc0-427a-9c80-ddf505c8e02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "285099fe-5ad0-4d65-b479-6bd5bd6874c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71069dbe-2a55-48f7-900f-04bd81dab612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "285099fe-5ad0-4d65-b479-6bd5bd6874c5",
                    "LayerId": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28"
                }
            ]
        },
        {
            "id": "7f7963d6-6b4c-4fe9-a86a-aa835b5acc93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "compositeImage": {
                "id": "ab59ad1e-d07c-4f07-98bf-d9d312743cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7963d6-6b4c-4fe9-a86a-aa835b5acc93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe75716-7315-4c22-9b5b-56a39b371051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7963d6-6b4c-4fe9-a86a-aa835b5acc93",
                    "LayerId": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28"
                }
            ]
        },
        {
            "id": "1648ffca-c866-4857-a6ea-39d6d8b478e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "compositeImage": {
                "id": "67f47674-9efc-49f8-af9c-46b50a3dd29c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1648ffca-c866-4857-a6ea-39d6d8b478e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5b562b-2de8-41d1-bf67-2e4f695b3c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1648ffca-c866-4857-a6ea-39d6d8b478e9",
                    "LayerId": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28"
                }
            ]
        },
        {
            "id": "44b9140e-c0c1-42b8-afee-f8cde039b94c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "compositeImage": {
                "id": "dbaf418c-eef6-48f4-baf2-51e16b591dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b9140e-c0c1-42b8-afee-f8cde039b94c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220e2618-610e-4f9a-a2e7-1257431c04c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b9140e-c0c1-42b8-afee-f8cde039b94c",
                    "LayerId": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28"
                }
            ]
        },
        {
            "id": "5ccafcf4-63b4-40bc-b03f-6c803185aaba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "compositeImage": {
                "id": "845e63e0-a593-411d-87ec-4952484868f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ccafcf4-63b4-40bc-b03f-6c803185aaba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "046d8f16-7feb-4362-ba64-ed649af52b02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ccafcf4-63b4-40bc-b03f-6c803185aaba",
                    "LayerId": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28"
                }
            ]
        },
        {
            "id": "cf57ae42-8a8f-478a-92c4-2723dba29a3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "compositeImage": {
                "id": "1ed79ecb-1f2c-4cb4-bbd6-21c88521b7bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf57ae42-8a8f-478a-92c4-2723dba29a3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1800fb7b-4fd4-4679-8e63-216ed76db604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf57ae42-8a8f-478a-92c4-2723dba29a3c",
                    "LayerId": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8deaa08b-d776-43c8-9c0e-cc2ef4bdfa28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10c95fe2-e161-4de3-854a-cce5b4253863",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}