{
    "id": "4cd5245e-387a-4954-b30b-a611a49f6758",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_standardboldoutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93e06e15-475b-489f-abdf-42a6e8c42281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "23ad61d4-7b06-4215-8bb6-3fa914014995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e06e15-475b-489f-abdf-42a6e8c42281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a156e2cd-df1f-4232-bb0d-020313e030fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e06e15-475b-489f-abdf-42a6e8c42281",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "7938d4a7-1463-4742-9b5b-324067534244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "ddb2e574-17a9-4d20-9f1e-a69985666153",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7938d4a7-1463-4742-9b5b-324067534244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482a66ab-c965-467c-a054-58c8ea78663e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7938d4a7-1463-4742-9b5b-324067534244",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "fbdfe951-446c-4db8-93d4-0d5a2d75f800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "680dcc00-82d4-490c-a243-1a17f7c4e046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbdfe951-446c-4db8-93d4-0d5a2d75f800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09bbbf9b-2c86-43a9-ac86-6875bc73619e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbdfe951-446c-4db8-93d4-0d5a2d75f800",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "9e9c655c-b16e-41f1-9684-6f8ae7b7b9bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "964d86ab-c07b-4199-ac24-45e010f74276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e9c655c-b16e-41f1-9684-6f8ae7b7b9bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e660ad7f-2615-4121-9a80-72ecc8a00522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e9c655c-b16e-41f1-9684-6f8ae7b7b9bb",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "16d10832-5dfd-481e-8888-c122887e4091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "cd367c7c-ad96-479f-aaf2-adf604ade8fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16d10832-5dfd-481e-8888-c122887e4091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b8893a-71c0-4e7b-aa6a-044be62e9132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16d10832-5dfd-481e-8888-c122887e4091",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "afa38580-c23b-40da-a410-c36006a7f540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "774d74e6-d418-43c0-9f50-42273a1296f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa38580-c23b-40da-a410-c36006a7f540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42566ba-3330-4146-93b7-5a001b322cb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa38580-c23b-40da-a410-c36006a7f540",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "340372e6-dec3-474f-a3e4-697ebdcda969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "7b5fbd9b-8a87-447e-89f1-ffa3c75a2763",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "340372e6-dec3-474f-a3e4-697ebdcda969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb2f210b-aa78-4a13-89ef-0b84c04d50a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "340372e6-dec3-474f-a3e4-697ebdcda969",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "6c2dab2d-8060-4652-acfc-5974ebb6230a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "451cbd39-3316-45bc-a99b-9d1944a2f455",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c2dab2d-8060-4652-acfc-5974ebb6230a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "465d2613-2929-4ae7-aeb5-c2d7801a7f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c2dab2d-8060-4652-acfc-5974ebb6230a",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "4ec3e34c-959b-4136-aac0-aa6795e8de03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "41f7e050-4eda-4cf8-85d0-904b4bc5c89b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ec3e34c-959b-4136-aac0-aa6795e8de03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d222c8-04a4-4eee-87a3-9c0b6d6e82ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ec3e34c-959b-4136-aac0-aa6795e8de03",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "933589c0-f82f-41b1-94e5-02dce9812b70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "5063c97f-7462-4b37-8554-eb2cde0f8cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933589c0-f82f-41b1-94e5-02dce9812b70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33930c54-2f49-44f8-8b13-e9480a3a4e6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933589c0-f82f-41b1-94e5-02dce9812b70",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "72fc73b2-14a7-4e19-9b11-148b601a9847",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "7e948128-0e68-4a40-9cdd-0baba42ef1f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72fc73b2-14a7-4e19-9b11-148b601a9847",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbf1e6c-d8ab-4430-b021-a23d260a429a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72fc73b2-14a7-4e19-9b11-148b601a9847",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "db6d2c7e-e4df-4914-bcce-80f3ba712366",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "50db5ae7-aa21-426f-9d45-d89335730dac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6d2c7e-e4df-4914-bcce-80f3ba712366",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4075ab48-431c-4667-ae5c-51814b5a045f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6d2c7e-e4df-4914-bcce-80f3ba712366",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "48b93dc6-62d4-4567-bdba-6006ee2c6ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "808d9187-edee-44b4-ab5d-75f7af007d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48b93dc6-62d4-4567-bdba-6006ee2c6ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b729cbf9-4740-440c-8a9d-197b3e8a9097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48b93dc6-62d4-4567-bdba-6006ee2c6ed4",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "ad311cc5-6641-48d1-a8b1-c0267294bb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "43503246-9950-457e-90ec-899d5f3d1c48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad311cc5-6641-48d1-a8b1-c0267294bb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520640f9-7089-4fd6-8d7c-952c440687b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad311cc5-6641-48d1-a8b1-c0267294bb89",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "0c9255a8-1e54-4131-98bf-5152de57185f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "f86b0bcc-cd27-4f8e-9236-db36fc0493b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c9255a8-1e54-4131-98bf-5152de57185f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d18cf59-c5c4-47f1-813e-c740c2e88aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c9255a8-1e54-4131-98bf-5152de57185f",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "5ee81a2e-1ad2-44b0-b4ab-9bd244c545e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "1a2c9095-9a3a-415d-816a-dbcf57c224ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee81a2e-1ad2-44b0-b4ab-9bd244c545e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e86c3e3-ffcb-443a-b5e1-fb47ebe4c34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee81a2e-1ad2-44b0-b4ab-9bd244c545e8",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "84d73f04-5aef-4797-81f0-38b2198ead30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "d6dbbebc-5b85-4e4a-8cfc-e6acb35e61f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d73f04-5aef-4797-81f0-38b2198ead30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebd4838a-e081-47b3-90f4-fba1666c3162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d73f04-5aef-4797-81f0-38b2198ead30",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "6bde95dc-5c68-4c5a-a8a9-dc86aee3f905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "4486ee72-7234-445f-9fbe-0cb9279c4013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bde95dc-5c68-4c5a-a8a9-dc86aee3f905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d32d71a-893c-4878-9600-3ec5d4ab3a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bde95dc-5c68-4c5a-a8a9-dc86aee3f905",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "4f28aeee-6e62-4605-864d-bd44cfadbbbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "0910c248-fcec-4414-a6a3-50d7093ac824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f28aeee-6e62-4605-864d-bd44cfadbbbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53008a9e-2c1a-48a3-a20f-9e8f0620770b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f28aeee-6e62-4605-864d-bd44cfadbbbc",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "3ddc3629-78d1-48e6-94ca-299d615f3ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "fa3b2ed2-2e39-4865-9955-bce6e9230b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ddc3629-78d1-48e6-94ca-299d615f3ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d7ddd1-6db4-4012-976b-b3b3cf29a914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ddc3629-78d1-48e6-94ca-299d615f3ccf",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "869fff09-d858-4a54-a270-20868b20ca93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "85808a86-fa98-4123-ab6b-15e568bc7aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869fff09-d858-4a54-a270-20868b20ca93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2593ee-ee58-4f7b-8262-ac0e09480988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869fff09-d858-4a54-a270-20868b20ca93",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "bbdfe580-9443-42a3-8213-418d88c9112d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "7e675ba2-7ddb-4a9a-b513-3c9e082d62d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbdfe580-9443-42a3-8213-418d88c9112d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3411bdf-ab6d-4164-be0e-8b8109105460",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbdfe580-9443-42a3-8213-418d88c9112d",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "f1fbdffd-94c5-4939-b2bc-bb11545acc26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "10ff6284-23a6-4fbb-a6ae-ebf5d873046d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fbdffd-94c5-4939-b2bc-bb11545acc26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d66cc7-329f-4625-b9f7-2f620bc21ed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fbdffd-94c5-4939-b2bc-bb11545acc26",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "0d9da5f9-44f9-40f8-9ff4-057631aa1c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "99c48e21-360f-4bc3-906f-037cac84c37d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d9da5f9-44f9-40f8-9ff4-057631aa1c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec42ffb-6899-48bb-9746-92f7b5ea4cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d9da5f9-44f9-40f8-9ff4-057631aa1c08",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "37e34b32-31ff-4fad-8bd2-ee4cdf46f582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "df815a00-6f38-4da5-bd17-49f9e027268e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37e34b32-31ff-4fad-8bd2-ee4cdf46f582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3657d953-d137-4252-87a1-ec6e955e196b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37e34b32-31ff-4fad-8bd2-ee4cdf46f582",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "2bda56cc-ca9c-40b3-bb7d-dd03dc91a8c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "371117f6-bd15-424a-9484-f79bff1a3bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bda56cc-ca9c-40b3-bb7d-dd03dc91a8c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e23c8b9-b7bc-4e4d-93f6-c03f09ba22f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bda56cc-ca9c-40b3-bb7d-dd03dc91a8c8",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "4f9b8810-a221-44dc-919f-3911fdadfd35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "d877d9ac-8fc2-4a42-8d85-762e727332b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f9b8810-a221-44dc-919f-3911fdadfd35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af13211-f81c-49b0-896f-fe97369c397b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f9b8810-a221-44dc-919f-3911fdadfd35",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "c6b4cf73-d33a-4d5c-b4c5-8cdbbdd03132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "0af1c143-6b57-47e0-98fa-b92efc62a8b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6b4cf73-d33a-4d5c-b4c5-8cdbbdd03132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbfe4120-8c34-4d13-87a5-b11886e8e707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6b4cf73-d33a-4d5c-b4c5-8cdbbdd03132",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "e2edec85-f622-4490-9531-844c26ad9dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "83fdbbf2-62a9-402c-b5f1-988fc7fba47e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2edec85-f622-4490-9531-844c26ad9dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5009df4-d580-4ffc-85a9-62b3a8eea798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2edec85-f622-4490-9531-844c26ad9dfb",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "caf115bb-33d6-4fd7-bf7f-f3915a66df5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "808f4fc1-d43e-40ba-b51e-3fb55d2939d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf115bb-33d6-4fd7-bf7f-f3915a66df5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0360ae8-37c9-4334-a4ad-d93cc592f982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf115bb-33d6-4fd7-bf7f-f3915a66df5f",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "303d863f-9409-48f7-b3c5-fbc384322051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "5fcf8b67-df1a-463a-9a98-1120cf3098b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "303d863f-9409-48f7-b3c5-fbc384322051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c483be40-d557-4233-aa8b-3d6c7bce8d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "303d863f-9409-48f7-b3c5-fbc384322051",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "d503621f-70f2-486f-a1c2-6e8900a60a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "512433ab-c9db-43dd-845e-4090b7eefa71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d503621f-70f2-486f-a1c2-6e8900a60a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "190e4ac1-00c9-40c8-80e9-0cc430d7878f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d503621f-70f2-486f-a1c2-6e8900a60a4f",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "0291de70-0b49-4bd7-bd99-f6f8823835b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "94400133-a43e-4d1c-9303-13c181881c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0291de70-0b49-4bd7-bd99-f6f8823835b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22e9564-b8b5-4be4-892d-d5d9e3b15ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0291de70-0b49-4bd7-bd99-f6f8823835b5",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "991a1d5a-1af6-4541-a110-b0f6d71d168e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "d15be8fc-06f3-4dc4-86d6-aca12d1798dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "991a1d5a-1af6-4541-a110-b0f6d71d168e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5dd7fb-13ac-4c1e-b11d-9db1a6220693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "991a1d5a-1af6-4541-a110-b0f6d71d168e",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "f6239645-d293-410d-ac64-a28d6b61787f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "b77c43e8-f7eb-44b1-bf77-af80c228330a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6239645-d293-410d-ac64-a28d6b61787f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf9cb7a-cb2e-4d72-b2a5-53d1387fbb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6239645-d293-410d-ac64-a28d6b61787f",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "1c0deb1a-3a8f-4e75-9167-b63cd38e6fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "d08eebd3-eaff-4d83-9fba-d27ffffed0b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c0deb1a-3a8f-4e75-9167-b63cd38e6fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb68e27-65c0-4c7d-8cb2-d5c977e6cdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0deb1a-3a8f-4e75-9167-b63cd38e6fb1",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "741b3782-3ef9-4fb8-89d5-07ccf6650f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "41a5c35a-e827-4f8e-819c-5eb137f3a79f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "741b3782-3ef9-4fb8-89d5-07ccf6650f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5288c17a-2b7b-4e27-aea6-dfb817c99270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "741b3782-3ef9-4fb8-89d5-07ccf6650f37",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "8d23cb6c-f60c-4211-8a55-cf55861278b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "87764a44-5c7b-4ca6-b24a-431c16fe193e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d23cb6c-f60c-4211-8a55-cf55861278b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552a4452-9c31-43d7-9e9e-7028504f573e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d23cb6c-f60c-4211-8a55-cf55861278b5",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "6cad8023-68ab-41ed-a0e8-1c1b1ef2d5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "15daf73e-abe4-430b-8539-1ea5577a540a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cad8023-68ab-41ed-a0e8-1c1b1ef2d5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0b0ecd0-1937-4924-a1a3-8e27b64147bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cad8023-68ab-41ed-a0e8-1c1b1ef2d5bd",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "0bcd9505-8354-4299-94e6-0f7d4513ec81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "7ad053fd-1601-4889-93dc-f848751d0aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bcd9505-8354-4299-94e6-0f7d4513ec81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aca886a-a77c-4f3e-8b78-9cb98ed5d593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bcd9505-8354-4299-94e6-0f7d4513ec81",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "e22d8699-e0b4-4fb8-afe6-2f3855b4fdb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "812a1a36-3bed-4d23-aabd-fcad3e97bbee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22d8699-e0b4-4fb8-afe6-2f3855b4fdb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f519e46-d1c6-4ba4-8846-ebdaf2840f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22d8699-e0b4-4fb8-afe6-2f3855b4fdb1",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "62445772-21c9-44a4-98f8-d36827241a46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "5069093a-0f18-4d04-aa3f-7bb80e790aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62445772-21c9-44a4-98f8-d36827241a46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6705601b-136b-4739-aa22-e2aaa052df1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62445772-21c9-44a4-98f8-d36827241a46",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "65b44780-be63-4602-9317-dc977e12aa30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "91af4734-6c95-45bc-996e-f2473cbde815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b44780-be63-4602-9317-dc977e12aa30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6144d1-526d-4011-88b0-e4ab500eab1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b44780-be63-4602-9317-dc977e12aa30",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "01d8fcae-896a-4d47-a924-0dd96df5ef60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "e75f71b9-f46c-4852-8e3a-e64df0a7c3c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d8fcae-896a-4d47-a924-0dd96df5ef60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94ef91e-f84b-40fa-b577-35f07107174d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d8fcae-896a-4d47-a924-0dd96df5ef60",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "257a3516-4e0f-4c02-a847-c44d7a87c31d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "68b8fe31-efb2-47a6-b4af-1ce84e9ac3b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "257a3516-4e0f-4c02-a847-c44d7a87c31d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17463fa4-6caf-4be9-aa9e-d59bd33f73de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "257a3516-4e0f-4c02-a847-c44d7a87c31d",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "3f2de163-49ce-4c3c-bc58-0ef6c211fa68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "c04b052a-0f9c-473b-a7b0-b7c0202c1c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2de163-49ce-4c3c-bc58-0ef6c211fa68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee01f210-7759-4de6-b871-76ebafbbe1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2de163-49ce-4c3c-bc58-0ef6c211fa68",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "a3530a2d-23c3-4aa8-a061-409c888d50ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "0019aa76-fbd8-407f-8d67-d070956551de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3530a2d-23c3-4aa8-a061-409c888d50ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c72dd74-7b91-4676-8efe-e7461d04dfd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3530a2d-23c3-4aa8-a061-409c888d50ad",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "f046b5c7-a76d-4492-a9ef-a0434189661e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "cf4091da-5193-49f1-9d96-778f27ecfda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f046b5c7-a76d-4492-a9ef-a0434189661e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef28324f-f175-4e9b-bb9f-a5a13534ea76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f046b5c7-a76d-4492-a9ef-a0434189661e",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "b84a985f-1202-4f4c-90a6-6f1871edd1e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "a75da4bd-25e2-4e62-b011-a8e23688babb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84a985f-1202-4f4c-90a6-6f1871edd1e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dff4c9f-ae31-403f-a4a2-732d5cbe36ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84a985f-1202-4f4c-90a6-6f1871edd1e2",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "ce541758-13af-4d62-8d2d-6378550e0ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "30619266-0046-495b-ad18-f39cbe4bf157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce541758-13af-4d62-8d2d-6378550e0ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1961d6d3-8841-48dc-9b5f-ee3b9e4d78f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce541758-13af-4d62-8d2d-6378550e0ce3",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "052f5131-dbaa-4260-99ba-8d143ff2e3eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "e37f4315-b4cb-4bb0-ba5f-1e1dcf55cf94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "052f5131-dbaa-4260-99ba-8d143ff2e3eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9921b789-9091-4d46-8a66-aabf99a73dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052f5131-dbaa-4260-99ba-8d143ff2e3eb",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "b168f917-6379-46ae-a9eb-c5590f918e12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "beb42aeb-bc59-4860-af9e-1d61fa603637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b168f917-6379-46ae-a9eb-c5590f918e12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e53c0e4-3ee4-40d3-93e8-7e46bb1b5d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b168f917-6379-46ae-a9eb-c5590f918e12",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "9bde8def-10e5-460e-a6ae-c06cee657488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "1349eb77-db69-420e-8ce4-1dcaedfd519a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bde8def-10e5-460e-a6ae-c06cee657488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67c176a-0c32-4a59-a66c-7186824c8e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bde8def-10e5-460e-a6ae-c06cee657488",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "91bffa3b-30e3-4769-80ba-85a5dd5be1a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "c850d29c-99e2-4b6f-be51-6567c18b1787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91bffa3b-30e3-4769-80ba-85a5dd5be1a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0507d1f5-839d-426d-8ae1-87ffbee58450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91bffa3b-30e3-4769-80ba-85a5dd5be1a5",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "22449310-f41f-426a-b932-4745b72842c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "fa0b5534-ebd6-4762-8a3c-5f1539a0f4a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22449310-f41f-426a-b932-4745b72842c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd1b476d-100e-4c19-8d2c-5cb9f0ef8138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22449310-f41f-426a-b932-4745b72842c6",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "bbcfced6-a640-419c-99f8-ad31903dc44e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "ef10089f-814b-4dac-9e02-8d8a9971f741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbcfced6-a640-419c-99f8-ad31903dc44e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa5cdba4-05c0-4ebf-a1b7-b1bcc23b0b1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbcfced6-a640-419c-99f8-ad31903dc44e",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "3b76cedb-8061-42b3-9e78-3b158ea4ff5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "b5f32283-3513-4bbf-b8ee-e8349e61de73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b76cedb-8061-42b3-9e78-3b158ea4ff5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc5782a-ad8d-4b8a-9aea-78ef4be404e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b76cedb-8061-42b3-9e78-3b158ea4ff5d",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "4fb92c85-0d6a-4353-ad22-0dc5c08ef2f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "0a73b2bb-18ca-4a1a-8211-9f7c28603c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb92c85-0d6a-4353-ad22-0dc5c08ef2f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac8ae45-2ec3-454a-887e-804528eef993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb92c85-0d6a-4353-ad22-0dc5c08ef2f8",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "cc68d47d-a4da-4010-ae27-1b83d13b2dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "307c7832-33ad-48a1-8757-ed2c45d88df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc68d47d-a4da-4010-ae27-1b83d13b2dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444ec94e-80b9-4027-b264-6d734a1ef54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc68d47d-a4da-4010-ae27-1b83d13b2dc3",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "cda22d54-1173-4c61-bc4c-8caef47167d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "632f6841-a0bc-49c7-be99-b4f721e0399a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda22d54-1173-4c61-bc4c-8caef47167d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c311f38-649e-4644-bc1e-7900ed27c8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda22d54-1173-4c61-bc4c-8caef47167d7",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "a719f851-2452-4b06-af65-9e27471a00f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "31a5beae-39a5-4fe6-9ae6-71b950041a88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a719f851-2452-4b06-af65-9e27471a00f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3c34413-76c3-4eed-882e-0af699ffff02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a719f851-2452-4b06-af65-9e27471a00f2",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "17bb2167-4665-4fd1-9c8a-921e22d12972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "d739fede-8bda-4480-bbe0-101b43a65120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17bb2167-4665-4fd1-9c8a-921e22d12972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84b744f-3cac-4b3a-9c75-ba889a593dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17bb2167-4665-4fd1-9c8a-921e22d12972",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "ce564827-2d8f-4ca1-b986-6706c057668a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "9cf68121-aa58-46e1-b20b-c28c9dfdc71b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce564827-2d8f-4ca1-b986-6706c057668a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c91cbc4b-397d-4dcc-aae1-1903c755b1ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce564827-2d8f-4ca1-b986-6706c057668a",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "da562897-3e7f-4c5f-8306-518ab9e9cae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "29fa6787-e16b-4ea0-b924-6cf14d1d5e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da562897-3e7f-4c5f-8306-518ab9e9cae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f53e186-e0de-45aa-a189-eb514c3eae41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da562897-3e7f-4c5f-8306-518ab9e9cae2",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "4d77e2a4-190a-4a9f-8a72-14e7947df824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "5db46e0a-71c0-40a4-87db-b3e0a6c84ad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d77e2a4-190a-4a9f-8a72-14e7947df824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a356eeca-a02c-48bb-af4f-f93c54213a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d77e2a4-190a-4a9f-8a72-14e7947df824",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "7425cbb2-704c-436b-bb97-a6f8eeaf7a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "79588303-29d0-4bf4-aca3-318fa2385b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7425cbb2-704c-436b-bb97-a6f8eeaf7a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e87f368-7067-4e48-bef7-55378fd256e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7425cbb2-704c-436b-bb97-a6f8eeaf7a92",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "9f34ee51-84c1-4f61-8d72-430b05f7ac0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "6e962e8b-4cd7-4134-a10b-8038a1358ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f34ee51-84c1-4f61-8d72-430b05f7ac0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8169f7d-3d9e-479f-b3dd-6a5337c47c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f34ee51-84c1-4f61-8d72-430b05f7ac0e",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "1f158a12-a744-4286-b3f6-790a946fe986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "5caf6d1d-9ce4-42e2-b7c0-1e6246350bf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f158a12-a744-4286-b3f6-790a946fe986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08927e45-0d9a-4373-a936-5c972725aa3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f158a12-a744-4286-b3f6-790a946fe986",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "2aadbd2f-af10-42b6-90f6-8901b6863c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "13ec51c5-9220-456e-a184-6aafa6bd61d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aadbd2f-af10-42b6-90f6-8901b6863c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23abdfe2-67f4-4d33-a936-423b24d6dfb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aadbd2f-af10-42b6-90f6-8901b6863c4f",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "ddde649c-87e3-46e7-a64c-a4f61e688ba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "b06a8cc1-0144-4bcc-a2a5-fc2033a4b5ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddde649c-87e3-46e7-a64c-a4f61e688ba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5350d9e-d898-445c-9419-4b6335834e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddde649c-87e3-46e7-a64c-a4f61e688ba5",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "5fb5223d-c5f1-43ea-954a-a63efa369511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "7d88016e-1cc8-4f3f-9479-ba3036c59b1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fb5223d-c5f1-43ea-954a-a63efa369511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83f9c41b-0d77-44cc-820d-4082c473ac38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fb5223d-c5f1-43ea-954a-a63efa369511",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "416c2f42-2817-480f-a177-a5f5afb2a2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "3c9b3e8a-d666-4235-ad06-d277bf35f23e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416c2f42-2817-480f-a177-a5f5afb2a2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed437337-749d-4fdc-b590-0b3098904c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416c2f42-2817-480f-a177-a5f5afb2a2d3",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "20ef9ac1-5b04-43ad-8cc6-0ac8dba4d21c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "ae782bab-a255-4b1f-899d-880012fb31cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20ef9ac1-5b04-43ad-8cc6-0ac8dba4d21c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d8e1178-ff66-4705-8930-100021546162",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20ef9ac1-5b04-43ad-8cc6-0ac8dba4d21c",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "a0f4eb2d-a2a8-4387-a2b8-34bd03a7db22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "a01322aa-29ff-4749-9d5e-70a9068fde9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0f4eb2d-a2a8-4387-a2b8-34bd03a7db22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afe305ff-93ac-47a6-8041-c54b50316953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0f4eb2d-a2a8-4387-a2b8-34bd03a7db22",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "1c199154-836d-4740-91a9-9aca8b9c4f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "7c284d36-fd09-4c34-a72a-4cf35f9f8b8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c199154-836d-4740-91a9-9aca8b9c4f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8cda33-08a4-4c97-9b22-081e51aa6a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c199154-836d-4740-91a9-9aca8b9c4f48",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "065b1e9d-7dd7-4fe2-ade3-e8341d8ee102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "99cc2c82-f4e2-452d-bffd-903e191342e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065b1e9d-7dd7-4fe2-ade3-e8341d8ee102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a4edb5a-0e78-42bb-ae63-65551044ff50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065b1e9d-7dd7-4fe2-ade3-e8341d8ee102",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "eab94f52-04b0-47e8-a3e0-8549ac17d3ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "42f4b1d2-2f7e-48a2-9e89-1f6820c2d9b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab94f52-04b0-47e8-a3e0-8549ac17d3ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd3bf414-1fbf-4b7f-96a7-098a4f1f607f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab94f52-04b0-47e8-a3e0-8549ac17d3ab",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        },
        {
            "id": "0bc9815f-b41d-4eac-94d5-dd6c013be7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "compositeImage": {
                "id": "a055121e-3d81-49b5-a24d-faed7816eccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc9815f-b41d-4eac-94d5-dd6c013be7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a684eb0f-8c87-4eca-a65d-4061a72e997e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc9815f-b41d-4eac-94d5-dd6c013be7e6",
                    "LayerId": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "553e9ec7-68c6-4b48-ad2e-33e0a4b4b2d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cd5245e-387a-4954-b30b-a611a49f6758",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}