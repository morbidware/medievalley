{
    "id": "b4a39ad1-f36c-4d5e-8003-e0ffb4665e5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c68203a4-5fff-471d-9cc8-68f1106cc8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4a39ad1-f36c-4d5e-8003-e0ffb4665e5c",
            "compositeImage": {
                "id": "9cddd17f-c3b8-4b84-8a5a-9458fe40aa29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68203a4-5fff-471d-9cc8-68f1106cc8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48b53a83-574f-4715-b662-cccce9976a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68203a4-5fff-471d-9cc8-68f1106cc8b7",
                    "LayerId": "26e4d766-fd5a-4aff-9ecd-31089df85d91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "26e4d766-fd5a-4aff-9ecd-31089df85d91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4a39ad1-f36c-4d5e-8003-e0ffb4665e5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}