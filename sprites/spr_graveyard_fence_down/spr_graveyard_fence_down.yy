{
    "id": "85b6680f-6763-47ce-a896-f70ceaceaf9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_fence_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 103,
    "bbox_left": 1,
    "bbox_right": 175,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76a6aacd-65cc-4a9b-959f-6ebe4a047f61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85b6680f-6763-47ce-a896-f70ceaceaf9d",
            "compositeImage": {
                "id": "6d334d6a-714a-40bc-8465-977a730d581e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a6aacd-65cc-4a9b-959f-6ebe4a047f61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25da25d2-919e-4473-ba5a-0857f7c48909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a6aacd-65cc-4a9b-959f-6ebe4a047f61",
                    "LayerId": "fa24c4f8-9077-4749-a853-8e039a920b5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 104,
    "layers": [
        {
            "id": "fa24c4f8-9077-4749-a853-8e039a920b5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85b6680f-6763-47ce-a896-f70ceaceaf9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 176,
    "xorig": 88,
    "yorig": 98
}