{
    "id": "da9b3318-9d5d-44be-a143-11a2c3c3cfd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_garlic_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97ca11c5-90b2-4d23-a46b-3403132b7b81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da9b3318-9d5d-44be-a143-11a2c3c3cfd1",
            "compositeImage": {
                "id": "6a6f0e75-8605-4888-9721-eb3736c33054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97ca11c5-90b2-4d23-a46b-3403132b7b81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5654a601-e0ee-44fc-9147-1c20f51daf44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ca11c5-90b2-4d23-a46b-3403132b7b81",
                    "LayerId": "f4c431a0-ddf9-4610-a0f5-3475a9c75d26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f4c431a0-ddf9-4610-a0f5-3475a9c75d26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da9b3318-9d5d-44be-a143-11a2c3c3cfd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}