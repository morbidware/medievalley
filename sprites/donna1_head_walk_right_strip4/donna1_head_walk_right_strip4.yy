{
    "id": "9e122364-9443-44a8-9eea-09725b93ce78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b4a63f9-a0d3-4dc7-9533-a171a6e01851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e122364-9443-44a8-9eea-09725b93ce78",
            "compositeImage": {
                "id": "72411c2b-89c0-43e5-b57f-ae49ab1c9355",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4a63f9-a0d3-4dc7-9533-a171a6e01851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de7c52f7-0d45-474c-8265-55a77561c437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4a63f9-a0d3-4dc7-9533-a171a6e01851",
                    "LayerId": "4aaddc72-7e2c-405b-be81-71e1eccc8ac9"
                }
            ]
        },
        {
            "id": "7099c036-9e14-4205-82a6-9482882fab9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e122364-9443-44a8-9eea-09725b93ce78",
            "compositeImage": {
                "id": "9ecb8e22-2f5d-4747-86b4-d4c9e1afe2ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7099c036-9e14-4205-82a6-9482882fab9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f01fad-f319-47e3-8d1f-7320ca8ce921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7099c036-9e14-4205-82a6-9482882fab9e",
                    "LayerId": "4aaddc72-7e2c-405b-be81-71e1eccc8ac9"
                }
            ]
        },
        {
            "id": "2625a2ff-4dca-4898-8a0a-f750582abc60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e122364-9443-44a8-9eea-09725b93ce78",
            "compositeImage": {
                "id": "90941018-785c-4994-9246-1445a5904fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2625a2ff-4dca-4898-8a0a-f750582abc60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a4b456-d14e-4cde-8bc5-86e9b35a4f62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2625a2ff-4dca-4898-8a0a-f750582abc60",
                    "LayerId": "4aaddc72-7e2c-405b-be81-71e1eccc8ac9"
                }
            ]
        },
        {
            "id": "135a11cb-05fa-4af1-9a37-7d4a222258e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e122364-9443-44a8-9eea-09725b93ce78",
            "compositeImage": {
                "id": "8c38ce4f-084f-4bff-a4ba-a8e7f06a3186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "135a11cb-05fa-4af1-9a37-7d4a222258e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4c740ee-e4f2-40a5-93b1-732a61aa93c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "135a11cb-05fa-4af1-9a37-7d4a222258e3",
                    "LayerId": "4aaddc72-7e2c-405b-be81-71e1eccc8ac9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4aaddc72-7e2c-405b-be81-71e1eccc8ac9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e122364-9443-44a8-9eea-09725b93ce78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}