{
    "id": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_walk_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "386a229f-0a62-432a-af7f-8d9eb82a031f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "compositeImage": {
                "id": "ef53217e-7c38-4772-b334-09250b7029e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386a229f-0a62-432a-af7f-8d9eb82a031f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2935745e-d521-4eea-85bd-2c9c0d4105ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386a229f-0a62-432a-af7f-8d9eb82a031f",
                    "LayerId": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f"
                }
            ]
        },
        {
            "id": "a48559bf-424c-480b-b19f-c18fae8d13c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "compositeImage": {
                "id": "8b84bbcd-467a-4f4e-a106-42ea9136eb0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48559bf-424c-480b-b19f-c18fae8d13c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ea46aad-9c7b-4b39-a8a9-ffbf7bc54585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48559bf-424c-480b-b19f-c18fae8d13c3",
                    "LayerId": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f"
                }
            ]
        },
        {
            "id": "61e835f6-6477-4d8a-8365-2485f2429708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "compositeImage": {
                "id": "935a671d-86fa-4761-b0d3-fc8ca7ae59a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61e835f6-6477-4d8a-8365-2485f2429708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f63fe6-ee79-44f1-a5cf-6849b21903a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61e835f6-6477-4d8a-8365-2485f2429708",
                    "LayerId": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f"
                }
            ]
        },
        {
            "id": "5ae8cc9b-b266-499b-95e5-d259941ea066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "compositeImage": {
                "id": "fcf4245a-0064-4266-ba2c-964d725254b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ae8cc9b-b266-499b-95e5-d259941ea066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ec43a6c-7fec-4301-85ff-62046ee7277c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ae8cc9b-b266-499b-95e5-d259941ea066",
                    "LayerId": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f"
                }
            ]
        },
        {
            "id": "3306dfb4-b2f8-4079-b092-d9fc3da4e22b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "compositeImage": {
                "id": "86e2c643-ce80-4c01-b0c6-a3d0016ae28d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3306dfb4-b2f8-4079-b092-d9fc3da4e22b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d61684d-0dfb-492a-8833-fe82b1f2743c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3306dfb4-b2f8-4079-b092-d9fc3da4e22b",
                    "LayerId": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f"
                }
            ]
        },
        {
            "id": "c64a1a06-7373-4a85-adac-b479e4de8bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "compositeImage": {
                "id": "b730449b-b295-4c0b-8cda-c7066fede393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64a1a06-7373-4a85-adac-b479e4de8bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74c76a26-69bc-4075-a8d2-9c3ea44251de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64a1a06-7373-4a85-adac-b479e4de8bb2",
                    "LayerId": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b1f8ea2e-224d-4cfd-b18b-798cee031c9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5d2b402-2679-4c4c-8d18-bacc0234e05a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}