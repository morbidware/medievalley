{
    "id": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_melee_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0c55590-41ce-467b-afc7-8a6af8433695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "compositeImage": {
                "id": "5e85bfc4-bf62-40b6-89d3-6779cd66a176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0c55590-41ce-467b-afc7-8a6af8433695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55860d3-5e29-460f-adfd-5973be037be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0c55590-41ce-467b-afc7-8a6af8433695",
                    "LayerId": "8002a1e5-3896-492a-a947-06b61303fa40"
                }
            ]
        },
        {
            "id": "104ee68a-b0e8-4255-bf0e-9397849673fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "compositeImage": {
                "id": "4f638fe7-426c-4e15-86c7-a61e14ac2210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "104ee68a-b0e8-4255-bf0e-9397849673fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939d1ad8-558b-4ffd-86ef-905fa7725557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "104ee68a-b0e8-4255-bf0e-9397849673fd",
                    "LayerId": "8002a1e5-3896-492a-a947-06b61303fa40"
                }
            ]
        },
        {
            "id": "dbf1da61-e944-4e20-ab19-2c1586229e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "compositeImage": {
                "id": "f56b16ed-ed11-4d55-9936-e09ea06aeb3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf1da61-e944-4e20-ab19-2c1586229e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dca8eb3-5bd7-4dee-9ef9-01d422ff5efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf1da61-e944-4e20-ab19-2c1586229e1a",
                    "LayerId": "8002a1e5-3896-492a-a947-06b61303fa40"
                }
            ]
        },
        {
            "id": "d317db7f-9517-4eba-ad77-bccdac534c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "compositeImage": {
                "id": "9c5c8b1e-d876-466a-bbcc-68bef5c3c804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d317db7f-9517-4eba-ad77-bccdac534c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d41bc00d-dcbb-4376-94bf-dcb0cbd9c1a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d317db7f-9517-4eba-ad77-bccdac534c3a",
                    "LayerId": "8002a1e5-3896-492a-a947-06b61303fa40"
                }
            ]
        },
        {
            "id": "b40db588-d223-4a9f-8e22-e106c7134cb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "compositeImage": {
                "id": "caae1a13-da3d-4250-9796-deeca79ea244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b40db588-d223-4a9f-8e22-e106c7134cb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dda1c5b5-db5e-4b17-a707-ab01ef33c8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b40db588-d223-4a9f-8e22-e106c7134cb5",
                    "LayerId": "8002a1e5-3896-492a-a947-06b61303fa40"
                }
            ]
        },
        {
            "id": "d3577d84-6b65-4f79-acbb-468eb25efb41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "compositeImage": {
                "id": "b92a5b46-2fd1-4d2c-80a4-844888e9cfa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3577d84-6b65-4f79-acbb-468eb25efb41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be926a5-ab02-4ead-aeb4-864d33248503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3577d84-6b65-4f79-acbb-468eb25efb41",
                    "LayerId": "8002a1e5-3896-492a-a947-06b61303fa40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8002a1e5-3896-492a-a947-06b61303fa40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d4752d6-9847-4dcd-b34e-8043a223d3e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}