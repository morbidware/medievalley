{
    "id": "5d95677d-6d26-41ec-880e-513d52fdcc59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_ironnugget",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 4,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "206f3eb4-85af-44e8-b49b-e6908edaca45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d95677d-6d26-41ec-880e-513d52fdcc59",
            "compositeImage": {
                "id": "a8a9a6a9-5e07-43a1-b743-b0424ecd0ae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206f3eb4-85af-44e8-b49b-e6908edaca45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b085261-9efa-40bb-8fe0-e58e4122cfc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206f3eb4-85af-44e8-b49b-e6908edaca45",
                    "LayerId": "57ff7c33-0952-476e-a963-d2cd61887ce9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "57ff7c33-0952-476e-a963-d2cd61887ce9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d95677d-6d26-41ec-880e-513d52fdcc59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}