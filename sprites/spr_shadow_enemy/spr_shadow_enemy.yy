{
    "id": "1f6fb074-efef-4631-a3f2-cb2abe5fe9e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b8b054d-20fc-4469-a519-c49ecb3dbaec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f6fb074-efef-4631-a3f2-cb2abe5fe9e8",
            "compositeImage": {
                "id": "6cb175b3-3c80-45ec-904f-1e1bdc020383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b8b054d-20fc-4469-a519-c49ecb3dbaec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a04e757-e4fc-48ec-ad39-21355a510132",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b8b054d-20fc-4469-a519-c49ecb3dbaec",
                    "LayerId": "c74497ae-33b6-4c46-9ab1-6325e76c3ebc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c74497ae-33b6-4c46-9ab1-6325e76c3ebc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f6fb074-efef-4631-a3f2-cb2abe5fe9e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 30
}