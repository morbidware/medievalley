{
    "id": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_melee_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b5f1c28-880d-4a19-886c-8b3a49aedccb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "compositeImage": {
                "id": "ff7c3108-1dd3-482e-b3d0-78060f6088f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5f1c28-880d-4a19-886c-8b3a49aedccb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afbde0e9-1a13-4e94-bb9b-23a3acf5add7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5f1c28-880d-4a19-886c-8b3a49aedccb",
                    "LayerId": "7dafc669-acc3-406c-8ce5-8165a31f33c9"
                }
            ]
        },
        {
            "id": "4973fc63-d00f-4e53-8965-f54deacfdefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "compositeImage": {
                "id": "edf74c56-4481-49e0-86d3-b40efb62040c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4973fc63-d00f-4e53-8965-f54deacfdefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9363ceeb-f58f-4ede-be17-f7e689f5875c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4973fc63-d00f-4e53-8965-f54deacfdefc",
                    "LayerId": "7dafc669-acc3-406c-8ce5-8165a31f33c9"
                }
            ]
        },
        {
            "id": "b55e0d60-097c-4313-a3af-9075e5223e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "compositeImage": {
                "id": "71b9a165-401b-4360-8f0a-e7166178b461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b55e0d60-097c-4313-a3af-9075e5223e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6209c6e4-0fde-4fda-af8b-0d839c19f840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b55e0d60-097c-4313-a3af-9075e5223e50",
                    "LayerId": "7dafc669-acc3-406c-8ce5-8165a31f33c9"
                }
            ]
        },
        {
            "id": "c080a5a2-bd0e-4c91-9f51-1214344f1055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "compositeImage": {
                "id": "b0f8f508-87f4-4ea4-8b09-a9476213c12c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c080a5a2-bd0e-4c91-9f51-1214344f1055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44537509-5401-4a52-a2e9-52d34d05b993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c080a5a2-bd0e-4c91-9f51-1214344f1055",
                    "LayerId": "7dafc669-acc3-406c-8ce5-8165a31f33c9"
                }
            ]
        },
        {
            "id": "2e6db9a2-11f0-4dcc-94b0-a4b50740d53b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "compositeImage": {
                "id": "a5597490-1471-4bc8-9800-fa7159491e02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e6db9a2-11f0-4dcc-94b0-a4b50740d53b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83610c29-2ff3-433d-9243-3b49866a1a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e6db9a2-11f0-4dcc-94b0-a4b50740d53b",
                    "LayerId": "7dafc669-acc3-406c-8ce5-8165a31f33c9"
                }
            ]
        },
        {
            "id": "8a42a883-8456-4b15-8458-0c78d858c9b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "compositeImage": {
                "id": "31eec266-cfc9-40f9-a35a-53bdd5f244ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a42a883-8456-4b15-8458-0c78d858c9b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f238671a-4658-4891-8b64-462375ab06cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a42a883-8456-4b15-8458-0c78d858c9b1",
                    "LayerId": "7dafc669-acc3-406c-8ce5-8165a31f33c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7dafc669-acc3-406c-8ce5-8165a31f33c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c9d8296-a115-4bf8-a1a5-4c927687b0f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}