{
    "id": "f9d3140a-0a29-4e48-80f9-123a5ad0c452",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_icons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "226e09eb-42b0-4c95-b08d-a5d68f1416f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9d3140a-0a29-4e48-80f9-123a5ad0c452",
            "compositeImage": {
                "id": "c17af7e8-ac97-40c3-b453-c11fe323d54c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "226e09eb-42b0-4c95-b08d-a5d68f1416f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ecdbd1d-be96-4349-a69a-82a16a536026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "226e09eb-42b0-4c95-b08d-a5d68f1416f7",
                    "LayerId": "a2ee128a-9694-43d2-8fbb-89b85a5e9d83"
                }
            ]
        },
        {
            "id": "02f31c52-3db3-4da9-bcdf-650758a133be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9d3140a-0a29-4e48-80f9-123a5ad0c452",
            "compositeImage": {
                "id": "8aa9996a-7b99-45f4-b8a8-bc97a28adba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f31c52-3db3-4da9-bcdf-650758a133be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23d39ce1-4d4d-4724-be57-5959b39dbfda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f31c52-3db3-4da9-bcdf-650758a133be",
                    "LayerId": "a2ee128a-9694-43d2-8fbb-89b85a5e9d83"
                }
            ]
        },
        {
            "id": "fafd97c4-bc4d-4210-9062-1ac6dd56ed9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9d3140a-0a29-4e48-80f9-123a5ad0c452",
            "compositeImage": {
                "id": "aadb9fcd-9f47-43e5-8bce-e66cac8708e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fafd97c4-bc4d-4210-9062-1ac6dd56ed9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74dcab6b-16a6-4e5a-9dfb-c3a5ec7b2c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fafd97c4-bc4d-4210-9062-1ac6dd56ed9f",
                    "LayerId": "a2ee128a-9694-43d2-8fbb-89b85a5e9d83"
                }
            ]
        },
        {
            "id": "c8a7270c-9b18-4d38-b63b-802c0e670cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9d3140a-0a29-4e48-80f9-123a5ad0c452",
            "compositeImage": {
                "id": "f319a942-b636-4abf-b1e5-c7a3beb41930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8a7270c-9b18-4d38-b63b-802c0e670cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cc7fde8-5553-4339-8aef-ade055ef1ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8a7270c-9b18-4d38-b63b-802c0e670cf6",
                    "LayerId": "a2ee128a-9694-43d2-8fbb-89b85a5e9d83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "a2ee128a-9694-43d2-8fbb-89b85a5e9d83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9d3140a-0a29-4e48-80f9-123a5ad0c452",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 6
}