{
    "id": "e38b3392-8b35-4074-8032-a24dc86ff692",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5dd8b77-3930-4a30-86a8-c05b49502450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e38b3392-8b35-4074-8032-a24dc86ff692",
            "compositeImage": {
                "id": "d8cfbdc9-20dc-44e1-a2f6-c4580094491d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5dd8b77-3930-4a30-86a8-c05b49502450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "270e9d57-8d54-4439-a7d2-4c40582da148",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5dd8b77-3930-4a30-86a8-c05b49502450",
                    "LayerId": "4f6ee555-e835-40da-bd77-d1bf296cfee7"
                }
            ]
        },
        {
            "id": "3b53282a-c6cd-434a-9c01-04849a3534ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e38b3392-8b35-4074-8032-a24dc86ff692",
            "compositeImage": {
                "id": "713ada5a-3258-4c1b-bb42-c2d70dda177c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b53282a-c6cd-434a-9c01-04849a3534ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59b93251-a4ab-417c-bc3e-e1d695e75212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b53282a-c6cd-434a-9c01-04849a3534ee",
                    "LayerId": "4f6ee555-e835-40da-bd77-d1bf296cfee7"
                }
            ]
        },
        {
            "id": "d84a1613-8201-4de4-aa26-736b30e455e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e38b3392-8b35-4074-8032-a24dc86ff692",
            "compositeImage": {
                "id": "d11e75a7-8235-4858-a5a0-6ee24620afed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84a1613-8201-4de4-aa26-736b30e455e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "901ef8da-bee9-4f4b-b576-83752c39fe6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84a1613-8201-4de4-aa26-736b30e455e2",
                    "LayerId": "4f6ee555-e835-40da-bd77-d1bf296cfee7"
                }
            ]
        },
        {
            "id": "c9584ed8-aaae-494b-9cb9-f82ab056f466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e38b3392-8b35-4074-8032-a24dc86ff692",
            "compositeImage": {
                "id": "2e90a57f-40a3-4c52-a447-2eebdb50b6d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9584ed8-aaae-494b-9cb9-f82ab056f466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8437f93d-706c-40ca-8d70-7b5162627dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9584ed8-aaae-494b-9cb9-f82ab056f466",
                    "LayerId": "4f6ee555-e835-40da-bd77-d1bf296cfee7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4f6ee555-e835-40da-bd77-d1bf296cfee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e38b3392-8b35-4074-8032-a24dc86ff692",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}