{
    "id": "23629f0f-754c-48df-bd9f-b09c2fac1d1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_life_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef46c284-c1e4-4fa6-adbf-815b3d744101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23629f0f-754c-48df-bd9f-b09c2fac1d1e",
            "compositeImage": {
                "id": "3913bcf8-f7f2-4eff-9457-b2e609ba6943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef46c284-c1e4-4fa6-adbf-815b3d744101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97896795-2f48-4cac-8be5-e80e8d77eea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef46c284-c1e4-4fa6-adbf-815b3d744101",
                    "LayerId": "38aa1d6f-2575-44db-81ff-23f424859c33"
                }
            ]
        },
        {
            "id": "955348cb-4b85-4082-82e2-302aee23aa46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23629f0f-754c-48df-bd9f-b09c2fac1d1e",
            "compositeImage": {
                "id": "7cdd4ade-0c26-4379-8e23-8b2a35e18271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955348cb-4b85-4082-82e2-302aee23aa46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc25299-61a9-44a8-bcdc-1fef6dfef7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955348cb-4b85-4082-82e2-302aee23aa46",
                    "LayerId": "38aa1d6f-2575-44db-81ff-23f424859c33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "38aa1d6f-2575-44db-81ff-23f424859c33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23629f0f-754c-48df-bd9f-b09c2fac1d1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 68
}