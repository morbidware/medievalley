{
    "id": "2da6ad53-c518-4bf2-bee7-34a4c4964c7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_die_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7add7baf-96d6-456a-a9b6-d546b6547a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2da6ad53-c518-4bf2-bee7-34a4c4964c7b",
            "compositeImage": {
                "id": "45a83a7c-1a3d-4ca6-874e-f8f2c3b88fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7add7baf-96d6-456a-a9b6-d546b6547a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd634639-f45d-4b6a-aae8-30d2c6b59109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7add7baf-96d6-456a-a9b6-d546b6547a35",
                    "LayerId": "d0b28535-e6b4-4e38-823f-15f09c8c7265"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0b28535-e6b4-4e38-823f-15f09c8c7265",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2da6ad53-c518-4bf2-bee7-34a4c4964c7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}