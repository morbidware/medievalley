{
    "id": "7458c714-da06-4013-9d4f-3a23733beeb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d30e86d-6813-4e34-b563-fd5536a96524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7458c714-da06-4013-9d4f-3a23733beeb2",
            "compositeImage": {
                "id": "1a9b17ce-0272-41da-bfa3-b3e74cbd66a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d30e86d-6813-4e34-b563-fd5536a96524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fece3c00-702c-4550-8610-cb4c7dba7fe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d30e86d-6813-4e34-b563-fd5536a96524",
                    "LayerId": "a812b24c-1659-48f5-a4ff-1b83c01119b9"
                }
            ]
        },
        {
            "id": "bcb2c108-06d1-49d8-a8e2-cfb05578e2c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7458c714-da06-4013-9d4f-3a23733beeb2",
            "compositeImage": {
                "id": "db66786d-d9b5-4960-ae03-7ce0ab3c57f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcb2c108-06d1-49d8-a8e2-cfb05578e2c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4ea590-68be-4d49-afde-582f49302f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcb2c108-06d1-49d8-a8e2-cfb05578e2c7",
                    "LayerId": "a812b24c-1659-48f5-a4ff-1b83c01119b9"
                }
            ]
        },
        {
            "id": "f49164d0-e873-4e03-b0de-75d03762c05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7458c714-da06-4013-9d4f-3a23733beeb2",
            "compositeImage": {
                "id": "2507bbf4-8119-41f0-ada1-7e8035a8c908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f49164d0-e873-4e03-b0de-75d03762c05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1afef1cf-89eb-4893-b2dd-e0e433540650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49164d0-e873-4e03-b0de-75d03762c05e",
                    "LayerId": "a812b24c-1659-48f5-a4ff-1b83c01119b9"
                }
            ]
        },
        {
            "id": "228c8965-ab29-4d36-867f-4b7e7771ca76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7458c714-da06-4013-9d4f-3a23733beeb2",
            "compositeImage": {
                "id": "81188a07-dc26-4e50-a661-421f34a24124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228c8965-ab29-4d36-867f-4b7e7771ca76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ab98e27-b2cc-4556-ad41-ca756a35cd52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228c8965-ab29-4d36-867f-4b7e7771ca76",
                    "LayerId": "a812b24c-1659-48f5-a4ff-1b83c01119b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a812b24c-1659-48f5-a4ff-1b83c01119b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7458c714-da06-4013-9d4f-3a23733beeb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}