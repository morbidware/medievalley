{
    "id": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_pick_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f48e138-7ca8-40b4-ab89-f0e53ad3fe54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "compositeImage": {
                "id": "edee4236-a53c-45e5-b68d-922c2a7fe607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f48e138-7ca8-40b4-ab89-f0e53ad3fe54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e24bff-a7db-469d-aabe-de5bccfa4a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f48e138-7ca8-40b4-ab89-f0e53ad3fe54",
                    "LayerId": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4"
                }
            ]
        },
        {
            "id": "58684248-1110-4c55-9d8f-5e2c7400498b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "compositeImage": {
                "id": "42edb0ad-937a-4e1b-859a-365ebba689c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58684248-1110-4c55-9d8f-5e2c7400498b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39372712-5921-4783-981c-d0fbd2ccff01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58684248-1110-4c55-9d8f-5e2c7400498b",
                    "LayerId": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4"
                }
            ]
        },
        {
            "id": "25e47a2c-82c0-4201-a1bc-12509915d106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "compositeImage": {
                "id": "5ccb73c2-ee46-4a42-bd3b-b777e78f91b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25e47a2c-82c0-4201-a1bc-12509915d106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f1a310-37d0-48b5-ac79-0ef0bc976e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25e47a2c-82c0-4201-a1bc-12509915d106",
                    "LayerId": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4"
                }
            ]
        },
        {
            "id": "3b1366e3-4b16-49c6-acc2-f2efb6ccc8c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "compositeImage": {
                "id": "31970dba-4946-4428-8c8b-df1948bdfd1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b1366e3-4b16-49c6-acc2-f2efb6ccc8c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0905a10c-39ca-40be-91e9-30c5664b68c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b1366e3-4b16-49c6-acc2-f2efb6ccc8c1",
                    "LayerId": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4"
                }
            ]
        },
        {
            "id": "de229fd7-edbe-41e4-acd6-6167a7a5c046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "compositeImage": {
                "id": "bcc7587f-9f48-4770-84cc-ee9670e2b00c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de229fd7-edbe-41e4-acd6-6167a7a5c046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5bd7fed-5cb9-422f-971d-97600aab170d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de229fd7-edbe-41e4-acd6-6167a7a5c046",
                    "LayerId": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4"
                }
            ]
        },
        {
            "id": "3f1a084f-f4eb-4e87-96c6-ad7a08e7a1fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "compositeImage": {
                "id": "f5365e52-8cf9-4110-8dd8-5cc9b2d3e217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1a084f-f4eb-4e87-96c6-ad7a08e7a1fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba32c02-58fc-4f0b-b9e5-5ec17fb66edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1a084f-f4eb-4e87-96c6-ad7a08e7a1fb",
                    "LayerId": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c9688e12-76f1-4d04-8803-e9e4edf3c9b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbc2d719-dc1c-4502-bb6d-c4d3ed2c23c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}