{
    "id": "4580ad23-21da-4967-8512-0c18ed2b6c01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe613cf6-f769-4af1-9c46-6c359ed30739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4580ad23-21da-4967-8512-0c18ed2b6c01",
            "compositeImage": {
                "id": "3957e4e5-b059-4da6-9cc8-b0e976c9ec28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe613cf6-f769-4af1-9c46-6c359ed30739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e636742-d8d8-403b-89cb-7a539f40c170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe613cf6-f769-4af1-9c46-6c359ed30739",
                    "LayerId": "c7bde644-9e92-4fc9-9522-3bea0f00dc6e"
                }
            ]
        },
        {
            "id": "60a2a1d6-568a-4d46-91d8-e16780575b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4580ad23-21da-4967-8512-0c18ed2b6c01",
            "compositeImage": {
                "id": "38cc5320-0d34-4cad-bf01-2dda5141a856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a2a1d6-568a-4d46-91d8-e16780575b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b69a5fba-9fca-430b-bb34-b99295adf0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a2a1d6-568a-4d46-91d8-e16780575b68",
                    "LayerId": "c7bde644-9e92-4fc9-9522-3bea0f00dc6e"
                }
            ]
        },
        {
            "id": "3117e421-3113-4451-93ff-f903ec8b656c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4580ad23-21da-4967-8512-0c18ed2b6c01",
            "compositeImage": {
                "id": "5a710f37-18c4-47f6-bf04-80b436214e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3117e421-3113-4451-93ff-f903ec8b656c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a48f9e6-ccf1-4d0c-9e71-e6e2019050dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3117e421-3113-4451-93ff-f903ec8b656c",
                    "LayerId": "c7bde644-9e92-4fc9-9522-3bea0f00dc6e"
                }
            ]
        },
        {
            "id": "33ffcc3e-5ab8-4f29-80ba-7ed292ad5beb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4580ad23-21da-4967-8512-0c18ed2b6c01",
            "compositeImage": {
                "id": "49119e72-5075-45c5-9d72-591b0d62c26a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33ffcc3e-5ab8-4f29-80ba-7ed292ad5beb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e52709-a4d4-450a-882c-8207d9e9605d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33ffcc3e-5ab8-4f29-80ba-7ed292ad5beb",
                    "LayerId": "c7bde644-9e92-4fc9-9522-3bea0f00dc6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7bde644-9e92-4fc9-9522-3bea0f00dc6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4580ad23-21da-4967-8512-0c18ed2b6c01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}