{
    "id": "e67670b1-7b61-4f79-ab14-84955f8acc00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57a55ec4-67f5-41f1-aefc-a530ed6ce247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67670b1-7b61-4f79-ab14-84955f8acc00",
            "compositeImage": {
                "id": "98ad29c7-f22d-4530-896a-aed386127236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a55ec4-67f5-41f1-aefc-a530ed6ce247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d610fb-8b79-4ff2-b659-4b263a0e8678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a55ec4-67f5-41f1-aefc-a530ed6ce247",
                    "LayerId": "cad32cd2-da00-46a6-9f92-31ea4dfcd98c"
                }
            ]
        },
        {
            "id": "a52de0f6-21ee-4006-99ae-21a2bf40ffcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67670b1-7b61-4f79-ab14-84955f8acc00",
            "compositeImage": {
                "id": "bd1ed3d7-7eab-4966-9617-e63cf5967d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a52de0f6-21ee-4006-99ae-21a2bf40ffcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec4291a-15db-420c-a828-6713f90f76a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a52de0f6-21ee-4006-99ae-21a2bf40ffcc",
                    "LayerId": "cad32cd2-da00-46a6-9f92-31ea4dfcd98c"
                }
            ]
        },
        {
            "id": "1cd31fc5-e538-4314-aef6-ff6d02fec32b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67670b1-7b61-4f79-ab14-84955f8acc00",
            "compositeImage": {
                "id": "0f91bcda-55d0-4439-985a-47d1ec7b88de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd31fc5-e538-4314-aef6-ff6d02fec32b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb10b206-b079-47fc-8b14-ef1f6caed169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd31fc5-e538-4314-aef6-ff6d02fec32b",
                    "LayerId": "cad32cd2-da00-46a6-9f92-31ea4dfcd98c"
                }
            ]
        },
        {
            "id": "208f3a06-529c-4440-907d-6b59e8d74ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e67670b1-7b61-4f79-ab14-84955f8acc00",
            "compositeImage": {
                "id": "0072b5a8-e59c-4202-9eb9-5f5a45fffaf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208f3a06-529c-4440-907d-6b59e8d74ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5acbd256-1cd2-43e9-9aae-008fcada63a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208f3a06-529c-4440-907d-6b59e8d74ea9",
                    "LayerId": "cad32cd2-da00-46a6-9f92-31ea4dfcd98c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cad32cd2-da00-46a6-9f92-31ea4dfcd98c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e67670b1-7b61-4f79-ab14-84955f8acc00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}