{
    "id": "609873b0-c465-4b3f-a865-67325d2199c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_walk_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5e341d0-08b5-411f-9642-494234a181d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "compositeImage": {
                "id": "c59eb3ad-881b-4035-96a3-82e0fb5166d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e341d0-08b5-411f-9642-494234a181d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a331a852-e459-42fe-905b-36618fb08704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e341d0-08b5-411f-9642-494234a181d9",
                    "LayerId": "071bda4a-8fdd-44e1-9ed5-d17002a339d6"
                }
            ]
        },
        {
            "id": "d4577a4a-252b-4ef1-ac2e-552d92179399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "compositeImage": {
                "id": "f48ad27f-b518-4315-b2be-935bf5f0335f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4577a4a-252b-4ef1-ac2e-552d92179399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cafc6a41-1450-4fbf-be1d-1002ce41428c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4577a4a-252b-4ef1-ac2e-552d92179399",
                    "LayerId": "071bda4a-8fdd-44e1-9ed5-d17002a339d6"
                }
            ]
        },
        {
            "id": "e121348a-6a03-4910-b453-fe7c3bfc8efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "compositeImage": {
                "id": "7d7ac6ce-339b-4599-ab42-270b9b6746c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e121348a-6a03-4910-b453-fe7c3bfc8efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee4631fc-0c80-4556-ade3-bf73a7c3fbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e121348a-6a03-4910-b453-fe7c3bfc8efd",
                    "LayerId": "071bda4a-8fdd-44e1-9ed5-d17002a339d6"
                }
            ]
        },
        {
            "id": "5ee7395f-915d-41e0-a86b-6f09f0412054",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "compositeImage": {
                "id": "8347f64d-35b4-4528-a9de-de59c0578b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee7395f-915d-41e0-a86b-6f09f0412054",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4411e6f-86da-4c82-a872-c70864fb60b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee7395f-915d-41e0-a86b-6f09f0412054",
                    "LayerId": "071bda4a-8fdd-44e1-9ed5-d17002a339d6"
                }
            ]
        },
        {
            "id": "2e283430-fd8b-487a-95b9-8142940fa9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "compositeImage": {
                "id": "e045ede7-83ed-4700-98fe-980245389f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e283430-fd8b-487a-95b9-8142940fa9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051151e1-a96f-41b1-a148-b99fe4367f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e283430-fd8b-487a-95b9-8142940fa9cc",
                    "LayerId": "071bda4a-8fdd-44e1-9ed5-d17002a339d6"
                }
            ]
        },
        {
            "id": "e56dbf40-a448-4c03-afa5-9ae7723de0e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "compositeImage": {
                "id": "59372305-bb6c-4909-a30b-259b22cd7851",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e56dbf40-a448-4c03-afa5-9ae7723de0e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f9ff47-8d23-4341-8857-5a6a53370dc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e56dbf40-a448-4c03-afa5-9ae7723de0e5",
                    "LayerId": "071bda4a-8fdd-44e1-9ed5-d17002a339d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "071bda4a-8fdd-44e1-9ed5-d17002a339d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "609873b0-c465-4b3f-a865-67325d2199c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}