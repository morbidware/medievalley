{
    "id": "f8e2ab78-5229-48fe-8684-20c89900a392",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfa61512-0ecf-409c-8241-29373bc43f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e2ab78-5229-48fe-8684-20c89900a392",
            "compositeImage": {
                "id": "b76466ea-1a91-42a1-8537-ffac6ba5a5d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfa61512-0ecf-409c-8241-29373bc43f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b54dc7-4cde-4e62-be61-cf4f5a85460f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfa61512-0ecf-409c-8241-29373bc43f40",
                    "LayerId": "0841d4db-8445-43cb-a023-69e9de213142"
                }
            ]
        },
        {
            "id": "7b90e176-8545-43d1-8195-6b71b170c76b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e2ab78-5229-48fe-8684-20c89900a392",
            "compositeImage": {
                "id": "b598f5aa-180c-4032-9589-17eacf5b1348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b90e176-8545-43d1-8195-6b71b170c76b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ed7cc1-1976-4de4-b0a2-5bd8e86d7371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b90e176-8545-43d1-8195-6b71b170c76b",
                    "LayerId": "0841d4db-8445-43cb-a023-69e9de213142"
                }
            ]
        },
        {
            "id": "94b4dd37-6b41-42b8-8f27-81d26c4a88ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e2ab78-5229-48fe-8684-20c89900a392",
            "compositeImage": {
                "id": "f03d99a4-7905-43a5-83c0-edc845318527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b4dd37-6b41-42b8-8f27-81d26c4a88ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ba8cc33-25bd-4ca9-afd8-d8db504cecb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b4dd37-6b41-42b8-8f27-81d26c4a88ee",
                    "LayerId": "0841d4db-8445-43cb-a023-69e9de213142"
                }
            ]
        },
        {
            "id": "d95e602e-1ff1-4ba0-b06d-962d63672e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e2ab78-5229-48fe-8684-20c89900a392",
            "compositeImage": {
                "id": "0b273e80-0326-40a8-b943-11530a0463c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95e602e-1ff1-4ba0-b06d-962d63672e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f7d3e1-d8d9-48fd-b8bc-acc323f77875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95e602e-1ff1-4ba0-b06d-962d63672e53",
                    "LayerId": "0841d4db-8445-43cb-a023-69e9de213142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0841d4db-8445-43cb-a023-69e9de213142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8e2ab78-5229-48fe-8684-20c89900a392",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}