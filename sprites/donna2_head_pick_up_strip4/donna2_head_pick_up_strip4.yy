{
    "id": "423bd93a-8f37-4e66-925f-da26eacdc47d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8efba046-4af5-48cc-9707-44c79e2bdb19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423bd93a-8f37-4e66-925f-da26eacdc47d",
            "compositeImage": {
                "id": "6f84f610-85ed-43c1-8960-0f012fe2c45c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8efba046-4af5-48cc-9707-44c79e2bdb19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ced7dfa-dc34-4164-95bd-1ce2dcd3edf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8efba046-4af5-48cc-9707-44c79e2bdb19",
                    "LayerId": "2d08e411-2f40-4cc9-94b6-e06804d94828"
                }
            ]
        },
        {
            "id": "c4921242-71da-424f-8c92-e8430f760928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423bd93a-8f37-4e66-925f-da26eacdc47d",
            "compositeImage": {
                "id": "2b83756b-d6d7-489d-8754-e7a916f2924a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4921242-71da-424f-8c92-e8430f760928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b129d30-4a34-4cca-bf65-a1805472155d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4921242-71da-424f-8c92-e8430f760928",
                    "LayerId": "2d08e411-2f40-4cc9-94b6-e06804d94828"
                }
            ]
        },
        {
            "id": "ceef062a-9323-4240-af33-c5f465cb6932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423bd93a-8f37-4e66-925f-da26eacdc47d",
            "compositeImage": {
                "id": "48ad5a64-7a3f-47c5-a33e-9be6be078ed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceef062a-9323-4240-af33-c5f465cb6932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f8072a6-99b2-4573-81c7-a8cdd328e34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceef062a-9323-4240-af33-c5f465cb6932",
                    "LayerId": "2d08e411-2f40-4cc9-94b6-e06804d94828"
                }
            ]
        },
        {
            "id": "4666eb8a-c9a9-421f-af71-e3d93733deeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423bd93a-8f37-4e66-925f-da26eacdc47d",
            "compositeImage": {
                "id": "ad624733-2663-41d2-b7a4-ae311d002d60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4666eb8a-c9a9-421f-af71-e3d93733deeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7affeba6-26c5-4764-aa0c-23edc4daef35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4666eb8a-c9a9-421f-af71-e3d93733deeb",
                    "LayerId": "2d08e411-2f40-4cc9-94b6-e06804d94828"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2d08e411-2f40-4cc9-94b6-e06804d94828",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "423bd93a-8f37-4e66-925f-da26eacdc47d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}