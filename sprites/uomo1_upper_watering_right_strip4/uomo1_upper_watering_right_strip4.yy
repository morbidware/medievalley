{
    "id": "d6539e22-8ef2-4af7-8972-72c60b8afbe5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3095c047-f81b-4423-bb7c-05d304c23d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6539e22-8ef2-4af7-8972-72c60b8afbe5",
            "compositeImage": {
                "id": "a32dbaf2-2d8b-4324-a404-99f73e957949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3095c047-f81b-4423-bb7c-05d304c23d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "931e4b6b-d00f-41cb-98e9-cfafe628b2e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3095c047-f81b-4423-bb7c-05d304c23d22",
                    "LayerId": "b24c4410-9823-4e6a-b889-48d5558bc244"
                }
            ]
        },
        {
            "id": "a7f9d0b3-753a-41b5-a625-bb46ef3de759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6539e22-8ef2-4af7-8972-72c60b8afbe5",
            "compositeImage": {
                "id": "db0dab56-1c88-4f04-bfc0-4f447b6b2e2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f9d0b3-753a-41b5-a625-bb46ef3de759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f4ae6b-bd9a-49e1-8ad5-dd02b6342076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f9d0b3-753a-41b5-a625-bb46ef3de759",
                    "LayerId": "b24c4410-9823-4e6a-b889-48d5558bc244"
                }
            ]
        },
        {
            "id": "7d14ddb1-201d-4192-9556-9f09e8552a81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6539e22-8ef2-4af7-8972-72c60b8afbe5",
            "compositeImage": {
                "id": "66047b50-32b8-4a62-a982-e7397fef60a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d14ddb1-201d-4192-9556-9f09e8552a81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdc0a9be-ab93-470f-81d2-40ff7e2c7d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d14ddb1-201d-4192-9556-9f09e8552a81",
                    "LayerId": "b24c4410-9823-4e6a-b889-48d5558bc244"
                }
            ]
        },
        {
            "id": "8ede471b-3991-448e-b026-0ad7ba77ac81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6539e22-8ef2-4af7-8972-72c60b8afbe5",
            "compositeImage": {
                "id": "2bc0cf7f-2f80-42b2-a212-c984389b5555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ede471b-3991-448e-b026-0ad7ba77ac81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f79077-7de2-4561-8fca-490d901de631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ede471b-3991-448e-b026-0ad7ba77ac81",
                    "LayerId": "b24c4410-9823-4e6a-b889-48d5558bc244"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b24c4410-9823-4e6a-b889-48d5558bc244",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6539e22-8ef2-4af7-8972-72c60b8afbe5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}