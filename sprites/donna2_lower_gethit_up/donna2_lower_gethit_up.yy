{
    "id": "670b02ad-da7b-4464-920f-217a2a7bac2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "723cdfbf-e465-457c-b50a-742bd875fffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "670b02ad-da7b-4464-920f-217a2a7bac2b",
            "compositeImage": {
                "id": "b3e59c98-79b8-4739-bfd9-e91038e926b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "723cdfbf-e465-457c-b50a-742bd875fffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda21d1b-da7c-42ec-b6d8-531090c58d7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "723cdfbf-e465-457c-b50a-742bd875fffa",
                    "LayerId": "b5d69a1b-271d-4146-9b91-1a6e5aaaf76e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5d69a1b-271d-4146-9b91-1a6e5aaaf76e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "670b02ad-da7b-4464-920f-217a2a7bac2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}