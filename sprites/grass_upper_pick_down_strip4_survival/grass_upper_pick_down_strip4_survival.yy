{
    "id": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_pick_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4dbfe52d-a582-4fc6-9754-b0118929bf78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "compositeImage": {
                "id": "49cabed8-e047-4788-901c-e8725f44e5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dbfe52d-a582-4fc6-9754-b0118929bf78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2add1b97-95f0-4ffc-a939-fd0720356604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dbfe52d-a582-4fc6-9754-b0118929bf78",
                    "LayerId": "9a5de1d6-9d62-4765-87a2-90e40106b0f0"
                }
            ]
        },
        {
            "id": "df2fff0f-4081-48ca-b621-6e6453d90133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "compositeImage": {
                "id": "0ff10288-ec8c-4ea4-894a-bbfbd6b2ff12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df2fff0f-4081-48ca-b621-6e6453d90133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a0afc9-0745-484c-8dd4-b891e3ed6d15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df2fff0f-4081-48ca-b621-6e6453d90133",
                    "LayerId": "9a5de1d6-9d62-4765-87a2-90e40106b0f0"
                }
            ]
        },
        {
            "id": "47b478f8-7a87-41ab-a59e-5fa6acd22893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "compositeImage": {
                "id": "d341e82d-a242-4a99-aef6-c47b77bcb990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b478f8-7a87-41ab-a59e-5fa6acd22893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c67ab3b-efbc-476e-9acb-969257083699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b478f8-7a87-41ab-a59e-5fa6acd22893",
                    "LayerId": "9a5de1d6-9d62-4765-87a2-90e40106b0f0"
                }
            ]
        },
        {
            "id": "700dfe4f-aa82-4e4a-905b-c6a630dcfb3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "compositeImage": {
                "id": "0066f810-42ff-4472-b2ca-916ca5a1bf99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "700dfe4f-aa82-4e4a-905b-c6a630dcfb3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1224f87e-9cf9-4963-95cb-30dae8ea96f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "700dfe4f-aa82-4e4a-905b-c6a630dcfb3b",
                    "LayerId": "9a5de1d6-9d62-4765-87a2-90e40106b0f0"
                }
            ]
        },
        {
            "id": "274089ec-b34f-46b7-b229-d58958102dd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "compositeImage": {
                "id": "6ca36726-390c-4401-be3d-f22b3f2f41e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274089ec-b34f-46b7-b229-d58958102dd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d9d73ef-b4cb-46c6-bb91-f58b082d9a31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274089ec-b34f-46b7-b229-d58958102dd1",
                    "LayerId": "9a5de1d6-9d62-4765-87a2-90e40106b0f0"
                }
            ]
        },
        {
            "id": "b88cc464-3dd8-4c29-95b0-e82c1eff804e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "compositeImage": {
                "id": "f7e7698d-c9a9-48e2-9e72-a09e3acdbb70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b88cc464-3dd8-4c29-95b0-e82c1eff804e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e04faae8-f33d-48d8-a2a9-902b5e509e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88cc464-3dd8-4c29-95b0-e82c1eff804e",
                    "LayerId": "9a5de1d6-9d62-4765-87a2-90e40106b0f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9a5de1d6-9d62-4765-87a2-90e40106b0f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44bd87d1-87ee-453a-85b8-8adfe74ca10e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}