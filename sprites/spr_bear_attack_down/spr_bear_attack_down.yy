{
    "id": "a0bf5a32-3c49-41eb-9cca-d1680a00ff48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 39,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5674fcd3-d89b-41a4-9f1b-f1e425bc3587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf5a32-3c49-41eb-9cca-d1680a00ff48",
            "compositeImage": {
                "id": "2bf42a29-b772-4f01-9f1b-5a5710a7ce31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5674fcd3-d89b-41a4-9f1b-f1e425bc3587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eec42c4c-8c7c-4e88-9e83-f28e156c8c81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5674fcd3-d89b-41a4-9f1b-f1e425bc3587",
                    "LayerId": "671a476d-4f85-4e3f-9dd9-0fb1b8e2ed8b"
                }
            ]
        },
        {
            "id": "79966bbb-3440-47fd-a04b-153934dab456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf5a32-3c49-41eb-9cca-d1680a00ff48",
            "compositeImage": {
                "id": "9901bf2a-5cab-4e1c-820e-bdd191265511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79966bbb-3440-47fd-a04b-153934dab456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496af82c-79e7-4d85-af57-624137b65cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79966bbb-3440-47fd-a04b-153934dab456",
                    "LayerId": "671a476d-4f85-4e3f-9dd9-0fb1b8e2ed8b"
                }
            ]
        },
        {
            "id": "29352370-5157-48f8-ab12-014a6f10fd06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0bf5a32-3c49-41eb-9cca-d1680a00ff48",
            "compositeImage": {
                "id": "71507838-e6dd-49f0-89d9-0afd8f4b2e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29352370-5157-48f8-ab12-014a6f10fd06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b881b45-76d3-4d78-86e5-3610971501e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29352370-5157-48f8-ab12-014a6f10fd06",
                    "LayerId": "671a476d-4f85-4e3f-9dd9-0fb1b8e2ed8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "671a476d-4f85-4e3f-9dd9-0fb1b8e2ed8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0bf5a32-3c49-41eb-9cca-d1680a00ff48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 46
}