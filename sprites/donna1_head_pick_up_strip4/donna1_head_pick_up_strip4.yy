{
    "id": "4053bebb-3487-4cb4-9409-16ee79e2e679",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae9ebc53-b242-4b6e-82c5-707acafeb456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4053bebb-3487-4cb4-9409-16ee79e2e679",
            "compositeImage": {
                "id": "e536a3dc-e31b-4fe3-b8b4-c3240dd1a10b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9ebc53-b242-4b6e-82c5-707acafeb456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84512c47-f631-4afb-a1ed-129d0c7bbe00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9ebc53-b242-4b6e-82c5-707acafeb456",
                    "LayerId": "5c6e846c-f3cc-4551-b070-7c85429430e5"
                }
            ]
        },
        {
            "id": "d5749cbf-9fae-4978-b5ad-6aa17222de1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4053bebb-3487-4cb4-9409-16ee79e2e679",
            "compositeImage": {
                "id": "150c3134-e369-4f56-8d22-6c6ba6877dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5749cbf-9fae-4978-b5ad-6aa17222de1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edfeba0-65fd-471c-8cda-a40f77f3592b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5749cbf-9fae-4978-b5ad-6aa17222de1e",
                    "LayerId": "5c6e846c-f3cc-4551-b070-7c85429430e5"
                }
            ]
        },
        {
            "id": "bc6aeca5-b9f0-4183-8ccc-6d44ca3bc2cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4053bebb-3487-4cb4-9409-16ee79e2e679",
            "compositeImage": {
                "id": "c4fd35db-0fd3-4450-a02f-d2220b4c4348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc6aeca5-b9f0-4183-8ccc-6d44ca3bc2cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd60f072-9a74-46b3-af67-68a96daf02f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc6aeca5-b9f0-4183-8ccc-6d44ca3bc2cf",
                    "LayerId": "5c6e846c-f3cc-4551-b070-7c85429430e5"
                }
            ]
        },
        {
            "id": "8d7ebd98-8bea-49a4-a7af-86bb3778978d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4053bebb-3487-4cb4-9409-16ee79e2e679",
            "compositeImage": {
                "id": "36b65f14-8d1f-449a-b15f-12ac4bf0b4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d7ebd98-8bea-49a4-a7af-86bb3778978d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f547ff-ed3a-49af-a850-fe78f6fc281b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d7ebd98-8bea-49a4-a7af-86bb3778978d",
                    "LayerId": "5c6e846c-f3cc-4551-b070-7c85429430e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5c6e846c-f3cc-4551-b070-7c85429430e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4053bebb-3487-4cb4-9409-16ee79e2e679",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}