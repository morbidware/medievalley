{
    "id": "eff6279f-930f-4c45-94b3-f53daa56b80d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_mail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "488e8453-d787-4f47-9bae-ec0ed36c3d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff6279f-930f-4c45-94b3-f53daa56b80d",
            "compositeImage": {
                "id": "2f75568f-2c22-401a-9a17-b191ffe348aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "488e8453-d787-4f47-9bae-ec0ed36c3d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64db5473-7cd5-4d5c-8188-b07eda1101b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "488e8453-d787-4f47-9bae-ec0ed36c3d35",
                    "LayerId": "9e13e94b-e088-46ff-83dc-cf776fe740c8"
                }
            ]
        },
        {
            "id": "05864c6d-f6e2-4397-8fc9-299770db1168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff6279f-930f-4c45-94b3-f53daa56b80d",
            "compositeImage": {
                "id": "3ae221f8-19a5-4415-acab-20831f452a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05864c6d-f6e2-4397-8fc9-299770db1168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7ba50b3-bea5-4838-9dd2-ae16b2b5e5d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05864c6d-f6e2-4397-8fc9-299770db1168",
                    "LayerId": "9e13e94b-e088-46ff-83dc-cf776fe740c8"
                }
            ]
        },
        {
            "id": "4b094221-8875-4ca2-b465-95ddeb498374",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff6279f-930f-4c45-94b3-f53daa56b80d",
            "compositeImage": {
                "id": "db8e8627-be74-4470-87c6-23912143c204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b094221-8875-4ca2-b465-95ddeb498374",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9dc86d8-3d20-43ce-8029-57d08f92be5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b094221-8875-4ca2-b465-95ddeb498374",
                    "LayerId": "9e13e94b-e088-46ff-83dc-cf776fe740c8"
                }
            ]
        },
        {
            "id": "ae169ebd-73d2-4448-9e04-bb4ba151ddf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eff6279f-930f-4c45-94b3-f53daa56b80d",
            "compositeImage": {
                "id": "94274e37-e2de-4bc2-83b1-50fbe074e205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae169ebd-73d2-4448-9e04-bb4ba151ddf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5142506b-b3ef-45be-84f2-247e7285129f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae169ebd-73d2-4448-9e04-bb4ba151ddf3",
                    "LayerId": "9e13e94b-e088-46ff-83dc-cf776fe740c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "9e13e94b-e088-46ff-83dc-cf776fe740c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eff6279f-930f-4c45-94b3-f53daa56b80d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}