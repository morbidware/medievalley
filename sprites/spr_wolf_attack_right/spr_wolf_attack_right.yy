{
    "id": "e3e93575-0840-422a-af8d-35ed40892657",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 7,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e66f0f8-0f02-4fe1-952d-dbe373e77061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3e93575-0840-422a-af8d-35ed40892657",
            "compositeImage": {
                "id": "cd9cac7c-c7cf-4697-abce-965357b270f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e66f0f8-0f02-4fe1-952d-dbe373e77061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80fb2968-a4a7-4d3d-9a05-6489fbfbb84c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e66f0f8-0f02-4fe1-952d-dbe373e77061",
                    "LayerId": "2164cd03-ef6e-4cd0-b29a-456f08fe8b68"
                }
            ]
        },
        {
            "id": "29239cd3-efec-4a51-bc48-681319301596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3e93575-0840-422a-af8d-35ed40892657",
            "compositeImage": {
                "id": "4cdcbc6e-f785-4f0c-9c91-ff6268a51882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29239cd3-efec-4a51-bc48-681319301596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61214e3d-e553-4058-a727-98a56731a122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29239cd3-efec-4a51-bc48-681319301596",
                    "LayerId": "2164cd03-ef6e-4cd0-b29a-456f08fe8b68"
                }
            ]
        },
        {
            "id": "e67ebbba-8ce0-4e4e-bfce-007b9be34383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3e93575-0840-422a-af8d-35ed40892657",
            "compositeImage": {
                "id": "30d41a9e-eca6-4ce6-8b8b-f0857aa957d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67ebbba-8ce0-4e4e-bfce-007b9be34383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b864f36a-ba17-45a7-912d-5955fc435728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67ebbba-8ce0-4e4e-bfce-007b9be34383",
                    "LayerId": "2164cd03-ef6e-4cd0-b29a-456f08fe8b68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "2164cd03-ef6e-4cd0-b29a-456f08fe8b68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3e93575-0840-422a-af8d-35ed40892657",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}