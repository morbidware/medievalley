{
    "id": "f410c6f2-5a60-49e5-b60e-e1106c83605c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipe_grindstone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c4d0085-4f2b-4c03-ae2e-69678051aec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f410c6f2-5a60-49e5-b60e-e1106c83605c",
            "compositeImage": {
                "id": "324e4261-0410-4de0-a88c-317a069121f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4d0085-4f2b-4c03-ae2e-69678051aec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333921c7-039d-4a4b-84ea-a8627ab678d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4d0085-4f2b-4c03-ae2e-69678051aec5",
                    "LayerId": "5293516f-91cb-4c15-9630-42847ec241b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "5293516f-91cb-4c15-9630-42847ec241b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f410c6f2-5a60-49e5-b60e-e1106c83605c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 21,
    "xorig": 10,
    "yorig": 15
}