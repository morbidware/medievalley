{
    "id": "8091d055-a4d0-4df8-bf04-99d283926284",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_walk_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c1e6d81-9415-47fd-bb5e-ac33ad05a1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8091d055-a4d0-4df8-bf04-99d283926284",
            "compositeImage": {
                "id": "94665ccf-0e36-45f1-9943-0384aeabab34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c1e6d81-9415-47fd-bb5e-ac33ad05a1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45159976-daad-4784-b8a9-f02b273a5f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c1e6d81-9415-47fd-bb5e-ac33ad05a1c5",
                    "LayerId": "96928ea9-5837-430a-b9f9-06ee52a22cdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "96928ea9-5837-430a-b9f9-06ee52a22cdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8091d055-a4d0-4df8-bf04-99d283926284",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}