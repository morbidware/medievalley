{
    "id": "2e60fed1-f1af-4842-94b3-50b3bd6d8e6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoklet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e97a602-fa22-41a2-9784-bd5e6afe83b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e60fed1-f1af-4842-94b3-50b3bd6d8e6e",
            "compositeImage": {
                "id": "c848e62c-0229-420b-97de-f27da3d95f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e97a602-fa22-41a2-9784-bd5e6afe83b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c3c788-3190-48f8-b090-6e34f69f65a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e97a602-fa22-41a2-9784-bd5e6afe83b4",
                    "LayerId": "10ac0c25-c70c-4416-8faf-18f23bb5741f"
                }
            ]
        },
        {
            "id": "573d3c4c-2ce8-482c-a5e3-f7a2124dab43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e60fed1-f1af-4842-94b3-50b3bd6d8e6e",
            "compositeImage": {
                "id": "61ac888d-864a-4761-b0c5-010e338e65f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "573d3c4c-2ce8-482c-a5e3-f7a2124dab43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5591c4a8-9dbb-4ddb-ab32-5e8d56759fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "573d3c4c-2ce8-482c-a5e3-f7a2124dab43",
                    "LayerId": "10ac0c25-c70c-4416-8faf-18f23bb5741f"
                }
            ]
        },
        {
            "id": "c0da5e9b-2525-4143-bd6a-2b195ba10a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e60fed1-f1af-4842-94b3-50b3bd6d8e6e",
            "compositeImage": {
                "id": "63936259-85ef-4676-90f9-9d81372ebdf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0da5e9b-2525-4143-bd6a-2b195ba10a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04fc651f-ef4f-4d84-9cde-caa4256890aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0da5e9b-2525-4143-bd6a-2b195ba10a24",
                    "LayerId": "10ac0c25-c70c-4416-8faf-18f23bb5741f"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "10ac0c25-c70c-4416-8faf-18f23bb5741f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e60fed1-f1af-4842-94b3-50b3bd6d8e6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}