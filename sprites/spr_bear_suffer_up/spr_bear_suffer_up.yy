{
    "id": "b3881f2b-3334-4e68-8e4c-00344768d95f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_suffer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 14,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6fa42ea-7c88-452d-9960-d7815753fcc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3881f2b-3334-4e68-8e4c-00344768d95f",
            "compositeImage": {
                "id": "3fe5f1ce-e81a-41aa-99a4-a96eebc87ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6fa42ea-7c88-452d-9960-d7815753fcc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095d282d-aa2b-4af7-b1ad-f836cc82b52c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6fa42ea-7c88-452d-9960-d7815753fcc4",
                    "LayerId": "761d0916-736b-4732-89a6-a3c5b14fd405"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "761d0916-736b-4732-89a6-a3c5b14fd405",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3881f2b-3334-4e68-8e4c-00344768d95f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}