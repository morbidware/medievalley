{
    "id": "3eb81bd4-3b4d-498f-b46e-9d03602810d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_joystickBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 0,
    "bbox_right": 109,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ab0c0a1-1c57-40b1-95c5-18cf74bb1cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eb81bd4-3b4d-498f-b46e-9d03602810d0",
            "compositeImage": {
                "id": "cc991388-731a-4cba-aaf9-87243486c202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab0c0a1-1c57-40b1-95c5-18cf74bb1cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6caf786-b71c-46d6-add8-5fdd0ffd6073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab0c0a1-1c57-40b1-95c5-18cf74bb1cdd",
                    "LayerId": "84655b20-122a-4f5b-939c-dc6d178c48a2"
                },
                {
                    "id": "2ed64ed8-1eb1-4a8b-b4c8-bf786499ac20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab0c0a1-1c57-40b1-95c5-18cf74bb1cdd",
                    "LayerId": "92eb8331-09ab-47ae-9e24-971f2db7a587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "92eb8331-09ab-47ae-9e24-971f2db7a587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eb81bd4-3b4d-498f-b46e-9d03602810d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "84655b20-122a-4f5b-939c-dc6d178c48a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eb81bd4-3b4d-498f-b46e-9d03602810d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 110,
    "xorig": 55,
    "yorig": 55
}