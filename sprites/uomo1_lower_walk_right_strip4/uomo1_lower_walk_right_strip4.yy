{
    "id": "653f4e4b-565e-49d8-b11b-778c418a5ce0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98905840-b9f6-4426-a238-6fc1f77b2c05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "653f4e4b-565e-49d8-b11b-778c418a5ce0",
            "compositeImage": {
                "id": "6ce28ed9-2da7-4afc-82ec-12da4a2c8120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98905840-b9f6-4426-a238-6fc1f77b2c05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08037c8f-2bf0-4520-b084-386fcfff20ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98905840-b9f6-4426-a238-6fc1f77b2c05",
                    "LayerId": "19acf312-3778-41f7-9d1d-fc5e1664884e"
                }
            ]
        },
        {
            "id": "5d7ac69b-534a-4d5b-b482-3201fb7880b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "653f4e4b-565e-49d8-b11b-778c418a5ce0",
            "compositeImage": {
                "id": "4a5c89c2-0b83-4a1e-905b-f0c63e2d7ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7ac69b-534a-4d5b-b482-3201fb7880b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0f770ec-4997-4181-b1e4-0663a19378a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7ac69b-534a-4d5b-b482-3201fb7880b0",
                    "LayerId": "19acf312-3778-41f7-9d1d-fc5e1664884e"
                }
            ]
        },
        {
            "id": "c71d8dcd-534a-40d9-9214-1b01caec2208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "653f4e4b-565e-49d8-b11b-778c418a5ce0",
            "compositeImage": {
                "id": "31001662-48f7-454c-983c-b17853fc3922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71d8dcd-534a-40d9-9214-1b01caec2208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4734f32d-2991-4e93-9a34-d9d53bc594d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71d8dcd-534a-40d9-9214-1b01caec2208",
                    "LayerId": "19acf312-3778-41f7-9d1d-fc5e1664884e"
                }
            ]
        },
        {
            "id": "ef02bc56-b6a8-44cd-8378-1432b6773c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "653f4e4b-565e-49d8-b11b-778c418a5ce0",
            "compositeImage": {
                "id": "915bac54-bc11-4ddf-ac13-086710bd250f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef02bc56-b6a8-44cd-8378-1432b6773c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0284d382-d0a4-44c5-afd4-9c33726dd20c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef02bc56-b6a8-44cd-8378-1432b6773c96",
                    "LayerId": "19acf312-3778-41f7-9d1d-fc5e1664884e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "19acf312-3778-41f7-9d1d-fc5e1664884e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "653f4e4b-565e-49d8-b11b-778c418a5ce0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}