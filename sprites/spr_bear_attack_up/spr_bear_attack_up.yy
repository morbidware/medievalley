{
    "id": "de677e5a-230f-4ffe-a03e-86d86820cd62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 8,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4699edfe-f3f2-410c-bc57-2601bc62dc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de677e5a-230f-4ffe-a03e-86d86820cd62",
            "compositeImage": {
                "id": "b49a9a09-3ca2-4990-8b41-0f4600a01cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4699edfe-f3f2-410c-bc57-2601bc62dc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3884f2ea-f16e-4685-98e8-75890119beec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4699edfe-f3f2-410c-bc57-2601bc62dc08",
                    "LayerId": "59def439-a7ba-4f57-84b9-6d65869e7556"
                }
            ]
        },
        {
            "id": "f4a693c0-edd7-406b-946d-04bbd129aa5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de677e5a-230f-4ffe-a03e-86d86820cd62",
            "compositeImage": {
                "id": "b6f0262c-24ab-42ca-9595-2bf445e196a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a693c0-edd7-406b-946d-04bbd129aa5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31a518f8-17fd-4da7-ac32-ec91a3a765bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a693c0-edd7-406b-946d-04bbd129aa5e",
                    "LayerId": "59def439-a7ba-4f57-84b9-6d65869e7556"
                }
            ]
        },
        {
            "id": "74e40702-a8f3-411a-8325-36e9e2f4fcf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de677e5a-230f-4ffe-a03e-86d86820cd62",
            "compositeImage": {
                "id": "acce8699-cec6-47d7-bc9b-a81f678ef906",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74e40702-a8f3-411a-8325-36e9e2f4fcf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ae54e7-cdbf-4a25-b19e-2b2ee6badf24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74e40702-a8f3-411a-8325-36e9e2f4fcf4",
                    "LayerId": "59def439-a7ba-4f57-84b9-6d65869e7556"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "59def439-a7ba-4f57-84b9-6d65869e7556",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de677e5a-230f-4ffe-a03e-86d86820cd62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 44
}