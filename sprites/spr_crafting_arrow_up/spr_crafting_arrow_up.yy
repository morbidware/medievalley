{
    "id": "87b95281-c1cf-488c-a7fa-94432e3ec08f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_arrow_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a053d19f-91d7-4dec-a052-1e125b7c0ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87b95281-c1cf-488c-a7fa-94432e3ec08f",
            "compositeImage": {
                "id": "430545ba-61cc-4ae6-99d2-64596c010567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a053d19f-91d7-4dec-a052-1e125b7c0ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62dbff87-8df8-4116-ba45-22aa1aefb878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a053d19f-91d7-4dec-a052-1e125b7c0ce9",
                    "LayerId": "8cb7fe2f-cdbc-4ffc-b42f-dd928e771f47"
                },
                {
                    "id": "307e7be0-4ac0-482e-8bad-ac2d51d9df20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a053d19f-91d7-4dec-a052-1e125b7c0ce9",
                    "LayerId": "c42eee96-d4b9-4b72-87dc-899438c1b30a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c42eee96-d4b9-4b72-87dc-899438c1b30a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87b95281-c1cf-488c-a7fa-94432e3ec08f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8cb7fe2f-cdbc-4ffc-b42f-dd928e771f47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87b95281-c1cf-488c-a7fa-94432e3ec08f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}