{
    "id": "c9a6eeb0-f2c5-47e2-b5d8-7ce700e1093e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aaad7f4d-a539-46ab-86a2-cb739f6a1e94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9a6eeb0-f2c5-47e2-b5d8-7ce700e1093e",
            "compositeImage": {
                "id": "3a872c48-be33-45d0-8797-b8e36aee6bdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaad7f4d-a539-46ab-86a2-cb739f6a1e94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311a3858-62a2-4c07-9d2f-8f890eea69da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaad7f4d-a539-46ab-86a2-cb739f6a1e94",
                    "LayerId": "3b76b0f6-da21-4111-b544-e95499cb69d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "3b76b0f6-da21-4111-b544-e95499cb69d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9a6eeb0-f2c5-47e2-b5d8-7ce700e1093e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 19
}