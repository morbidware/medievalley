{
    "id": "2271149d-250c-4fc3-871b-a551db6ab30b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "212985a3-0a62-4778-9ecf-0272bd4db7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2271149d-250c-4fc3-871b-a551db6ab30b",
            "compositeImage": {
                "id": "7ec18f1c-e2a1-41d2-9b3c-6176f6d86dee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212985a3-0a62-4778-9ecf-0272bd4db7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b7728bc-bd5a-4181-9271-1c6107359aa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212985a3-0a62-4778-9ecf-0272bd4db7ca",
                    "LayerId": "cd7cf65d-0662-4fee-bc5e-a1ecbd8a8510"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "cd7cf65d-0662-4fee-bc5e-a1ecbd8a8510",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2271149d-250c-4fc3-871b-a551db6ab30b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 13,
    "yorig": 32
}