{
    "id": "9add66e3-0863-4331-81c9-587e4188fe94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f09c8904-76bd-45c0-a7fe-9376c897924a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9add66e3-0863-4331-81c9-587e4188fe94",
            "compositeImage": {
                "id": "badb7a9a-107f-4518-82d8-22c660480481",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f09c8904-76bd-45c0-a7fe-9376c897924a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d192f1e4-8ad1-4016-b367-4ba41d26e1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f09c8904-76bd-45c0-a7fe-9376c897924a",
                    "LayerId": "48d8efa1-a2fe-4c57-8d4e-441fac2cca4e"
                }
            ]
        },
        {
            "id": "d4fd91dd-965c-427e-8d72-9b349068889a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9add66e3-0863-4331-81c9-587e4188fe94",
            "compositeImage": {
                "id": "235ef2e5-95a6-44c0-ab4a-8fedaca6a61d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4fd91dd-965c-427e-8d72-9b349068889a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b916c3-058d-41c6-b74e-501f30ac254b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4fd91dd-965c-427e-8d72-9b349068889a",
                    "LayerId": "48d8efa1-a2fe-4c57-8d4e-441fac2cca4e"
                }
            ]
        },
        {
            "id": "9293ebf8-7cc2-401c-8e10-f61d545eff69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9add66e3-0863-4331-81c9-587e4188fe94",
            "compositeImage": {
                "id": "7cae6f1e-0ce1-4dac-afb6-917803b131ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9293ebf8-7cc2-401c-8e10-f61d545eff69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b33ae4-ee6e-4783-9b8e-e67d3c642ed5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9293ebf8-7cc2-401c-8e10-f61d545eff69",
                    "LayerId": "48d8efa1-a2fe-4c57-8d4e-441fac2cca4e"
                }
            ]
        },
        {
            "id": "99d118c8-b7ff-446a-ab03-a066a49b53e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9add66e3-0863-4331-81c9-587e4188fe94",
            "compositeImage": {
                "id": "2a2b87da-93f4-4096-ad90-5d2c8edf7a1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99d118c8-b7ff-446a-ab03-a066a49b53e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0d1b27f-4891-4a47-83f4-e45721664392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99d118c8-b7ff-446a-ab03-a066a49b53e0",
                    "LayerId": "48d8efa1-a2fe-4c57-8d4e-441fac2cca4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48d8efa1-a2fe-4c57-8d4e-441fac2cca4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9add66e3-0863-4331-81c9-587e4188fe94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}