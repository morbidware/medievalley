{
    "id": "61d73fa6-b785-4d60-9fa4-bd5a61bbb22d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_suffer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 11,
    "bbox_right": 30,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e23c53b2-39bf-40f6-a43f-222c0aa8608e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61d73fa6-b785-4d60-9fa4-bd5a61bbb22d",
            "compositeImage": {
                "id": "bde0f1d4-889e-4534-82a0-38fa48e9a6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e23c53b2-39bf-40f6-a43f-222c0aa8608e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "473ea936-613a-4c02-be49-a0172011c695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e23c53b2-39bf-40f6-a43f-222c0aa8608e",
                    "LayerId": "68bf5123-1f90-4e32-bad7-9e25929a6ee2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68bf5123-1f90-4e32-bad7-9e25929a6ee2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61d73fa6-b785-4d60-9fa4-bd5a61bbb22d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}