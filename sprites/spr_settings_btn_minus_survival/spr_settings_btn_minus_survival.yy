{
    "id": "03a62227-ac3a-421f-a39f-41021d33ecd8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_btn_minus_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1d69334-6c2c-4eb2-9248-901d1dc8c2b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a62227-ac3a-421f-a39f-41021d33ecd8",
            "compositeImage": {
                "id": "c70168b2-0e6c-4aca-b591-c86e9f6f960b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d69334-6c2c-4eb2-9248-901d1dc8c2b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22227b86-d141-44fc-9349-f368a7eb5a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d69334-6c2c-4eb2-9248-901d1dc8c2b0",
                    "LayerId": "eb69bc7a-5ccf-4ef2-9ec3-974e73afedf1"
                }
            ]
        },
        {
            "id": "4a902550-8190-4546-8365-a60ff93b4faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a62227-ac3a-421f-a39f-41021d33ecd8",
            "compositeImage": {
                "id": "526bd1e2-19fc-4dfd-aea5-b78347ee8f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a902550-8190-4546-8365-a60ff93b4faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61125bac-c7c2-4f6e-888a-7c6efee26cc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a902550-8190-4546-8365-a60ff93b4faf",
                    "LayerId": "eb69bc7a-5ccf-4ef2-9ec3-974e73afedf1"
                }
            ]
        },
        {
            "id": "e5c8fdbf-4660-4582-80f0-637ddaf69ee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03a62227-ac3a-421f-a39f-41021d33ecd8",
            "compositeImage": {
                "id": "1094f862-d2cf-40b9-aaa2-3b1b6f9aba1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c8fdbf-4660-4582-80f0-637ddaf69ee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b3588cf-c4ac-400c-8a2f-40380bcdbc31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c8fdbf-4660-4582-80f0-637ddaf69ee3",
                    "LayerId": "eb69bc7a-5ccf-4ef2-9ec3-974e73afedf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "eb69bc7a-5ccf-4ef2-9ec3-974e73afedf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03a62227-ac3a-421f-a39f-41021d33ecd8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}