{
    "id": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_melee_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d571060-58a1-4944-8d12-9c1949025a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "compositeImage": {
                "id": "dc8b0617-bdda-42a4-b718-d677b8a4b392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d571060-58a1-4944-8d12-9c1949025a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c006d635-6108-471e-9375-4d64066e70b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d571060-58a1-4944-8d12-9c1949025a27",
                    "LayerId": "ba5c3643-ffd1-4e7d-9617-73391075839f"
                }
            ]
        },
        {
            "id": "d96ee85c-b566-4ce7-8274-b98d5ecbf314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "compositeImage": {
                "id": "47251c83-c9fb-42b2-98f0-cf79bfb09cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96ee85c-b566-4ce7-8274-b98d5ecbf314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0678e7a-5648-4be7-91bb-b983eb94ee47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96ee85c-b566-4ce7-8274-b98d5ecbf314",
                    "LayerId": "ba5c3643-ffd1-4e7d-9617-73391075839f"
                }
            ]
        },
        {
            "id": "9a3cbe95-7f3f-4bdb-bda7-9a63ceb2ed3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "compositeImage": {
                "id": "b29ba85e-c5c8-449f-9d0b-65dcbf14006f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a3cbe95-7f3f-4bdb-bda7-9a63ceb2ed3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4487de-c09a-4e34-9e84-41580e29bb76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a3cbe95-7f3f-4bdb-bda7-9a63ceb2ed3e",
                    "LayerId": "ba5c3643-ffd1-4e7d-9617-73391075839f"
                }
            ]
        },
        {
            "id": "01110b5e-112d-4ddc-b335-fcec327852e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "compositeImage": {
                "id": "0fe156b6-75e1-4079-a67d-a5bbb405b2ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01110b5e-112d-4ddc-b335-fcec327852e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0998a4b7-bb14-4e33-8aab-e4dbf4c80918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01110b5e-112d-4ddc-b335-fcec327852e1",
                    "LayerId": "ba5c3643-ffd1-4e7d-9617-73391075839f"
                }
            ]
        },
        {
            "id": "10b6f551-89a8-46d9-9594-206a68374509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "compositeImage": {
                "id": "d8f6f0f5-d936-489b-b296-23873f66e831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b6f551-89a8-46d9-9594-206a68374509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf02af47-e5d5-4ebc-9c68-2e65dd3efcf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b6f551-89a8-46d9-9594-206a68374509",
                    "LayerId": "ba5c3643-ffd1-4e7d-9617-73391075839f"
                }
            ]
        },
        {
            "id": "039232bd-87ca-4820-84f1-b971bd85bd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "compositeImage": {
                "id": "fd751fd5-9448-4e2d-8661-2243136b51dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "039232bd-87ca-4820-84f1-b971bd85bd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4068b6b-cb3b-4bf1-b153-fcb60773d71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "039232bd-87ca-4820-84f1-b971bd85bd2c",
                    "LayerId": "ba5c3643-ffd1-4e7d-9617-73391075839f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba5c3643-ffd1-4e7d-9617-73391075839f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0212eabd-d9a5-4e39-84f5-ec8cbef21431",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}