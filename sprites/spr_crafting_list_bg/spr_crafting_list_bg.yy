{
    "id": "05f89c86-5da4-4ee9-a33d-59933e612198",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_list_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 321,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88cf19e5-d691-44c2-a029-9966b517dedc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f89c86-5da4-4ee9-a33d-59933e612198",
            "compositeImage": {
                "id": "df4429b6-b0b7-49ce-a07d-665b635a5a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cf19e5-d691-44c2-a029-9966b517dedc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da9fc7b5-5182-49fd-bf95-6405f3526da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cf19e5-d691-44c2-a029-9966b517dedc",
                    "LayerId": "1ba0d1be-6a41-48a0-b2fd-87730f39bd85"
                }
            ]
        },
        {
            "id": "4da7d888-8605-4d2b-89bd-9d85798ed3d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f89c86-5da4-4ee9-a33d-59933e612198",
            "compositeImage": {
                "id": "dc5674fc-b194-4be2-a508-fff90cfcbb31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4da7d888-8605-4d2b-89bd-9d85798ed3d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d309828e-a01a-4b01-b341-0e881f32f2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4da7d888-8605-4d2b-89bd-9d85798ed3d2",
                    "LayerId": "1ba0d1be-6a41-48a0-b2fd-87730f39bd85"
                }
            ]
        },
        {
            "id": "5954593b-d00e-4f78-b23b-bd7627f6d899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f89c86-5da4-4ee9-a33d-59933e612198",
            "compositeImage": {
                "id": "071abbd0-fa88-462d-97b5-4bbca67a43f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5954593b-d00e-4f78-b23b-bd7627f6d899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0bef05-5624-4702-90f2-7cebba089bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5954593b-d00e-4f78-b23b-bd7627f6d899",
                    "LayerId": "1ba0d1be-6a41-48a0-b2fd-87730f39bd85"
                }
            ]
        },
        {
            "id": "ece40949-d8c2-45a3-98b3-e7d5e2c2521c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f89c86-5da4-4ee9-a33d-59933e612198",
            "compositeImage": {
                "id": "333e9688-984b-4417-8167-f7529d267f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece40949-d8c2-45a3-98b3-e7d5e2c2521c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442bde29-9d2f-4f91-87f6-80b43948e2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece40949-d8c2-45a3-98b3-e7d5e2c2521c",
                    "LayerId": "1ba0d1be-6a41-48a0-b2fd-87730f39bd85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 322,
    "layers": [
        {
            "id": "1ba0d1be-6a41-48a0-b2fd-87730f39bd85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05f89c86-5da4-4ee9-a33d-59933e612198",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}