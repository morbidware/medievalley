{
    "id": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_hammer_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "905fe4cd-236a-4456-8a28-024641038dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "compositeImage": {
                "id": "943e3751-c49c-45b8-97ba-042b4885979f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "905fe4cd-236a-4456-8a28-024641038dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28747ba1-2f5b-429d-babb-6056ab91f2c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905fe4cd-236a-4456-8a28-024641038dfc",
                    "LayerId": "224f196a-3857-47c3-88e2-6463952adeb4"
                }
            ]
        },
        {
            "id": "5d0a75d5-7b12-4591-91d1-0cad9e5b6f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "compositeImage": {
                "id": "90744b12-b2c0-40bb-8037-46f1547625b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0a75d5-7b12-4591-91d1-0cad9e5b6f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8950be84-ab0a-4eef-9a27-ccc1f03a2126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0a75d5-7b12-4591-91d1-0cad9e5b6f31",
                    "LayerId": "224f196a-3857-47c3-88e2-6463952adeb4"
                }
            ]
        },
        {
            "id": "4eec3e1f-bbea-4948-9d94-f675247349e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "compositeImage": {
                "id": "deaf5e72-ad60-4434-9737-92452d1bd062",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eec3e1f-bbea-4948-9d94-f675247349e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3fae635-82f8-4dea-b177-86fc3b3a27e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eec3e1f-bbea-4948-9d94-f675247349e6",
                    "LayerId": "224f196a-3857-47c3-88e2-6463952adeb4"
                }
            ]
        },
        {
            "id": "d93c6575-1465-4068-9226-fe865c6491f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "compositeImage": {
                "id": "bddc90a2-151d-4f02-af2e-215496ba19f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93c6575-1465-4068-9226-fe865c6491f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c343ec6-d1e7-4545-bfc7-a2b51dca06b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93c6575-1465-4068-9226-fe865c6491f8",
                    "LayerId": "224f196a-3857-47c3-88e2-6463952adeb4"
                }
            ]
        },
        {
            "id": "43d25dff-b22d-4c31-ade0-0039cf302b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "compositeImage": {
                "id": "f93e3bed-e1dd-4bd6-89a9-f2798c2fd17e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d25dff-b22d-4c31-ade0-0039cf302b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "607bb28c-4b34-4068-adc2-d8a58ea17c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d25dff-b22d-4c31-ade0-0039cf302b96",
                    "LayerId": "224f196a-3857-47c3-88e2-6463952adeb4"
                }
            ]
        },
        {
            "id": "cb8ff174-7953-46b2-8453-fb09da635b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "compositeImage": {
                "id": "73a268ea-46ad-4873-b363-b8273b3971dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb8ff174-7953-46b2-8453-fb09da635b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82912fd1-7512-48f8-86fd-f20560d86075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb8ff174-7953-46b2-8453-fb09da635b92",
                    "LayerId": "224f196a-3857-47c3-88e2-6463952adeb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "224f196a-3857-47c3-88e2-6463952adeb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81c9118f-d44c-4c24-a3d6-34a60d884f44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}