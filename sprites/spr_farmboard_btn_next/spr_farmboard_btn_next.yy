{
    "id": "31389c4b-a533-4863-847e-b07e37e4a597",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_btn_next",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb83366-c39a-45c2-85ef-5099112c91d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31389c4b-a533-4863-847e-b07e37e4a597",
            "compositeImage": {
                "id": "d7aa42d0-d6cb-4a7c-8b54-e9ed49b24c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb83366-c39a-45c2-85ef-5099112c91d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d975245d-d0c2-461a-ac09-c51df6a1950d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb83366-c39a-45c2-85ef-5099112c91d5",
                    "LayerId": "c85e9362-9a42-4469-a842-8f802181c65a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c85e9362-9a42-4469-a842-8f802181c65a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31389c4b-a533-4863-847e-b07e37e4a597",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}