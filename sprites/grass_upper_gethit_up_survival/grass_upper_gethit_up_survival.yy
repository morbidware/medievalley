{
    "id": "95d720ab-7b8a-42a1-881c-cb5b6497e160",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_gethit_up_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6243ba0-da6d-4722-8ede-8da1b54d27b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95d720ab-7b8a-42a1-881c-cb5b6497e160",
            "compositeImage": {
                "id": "8edb6f97-42ed-4235-a9f7-b7ef8f1e258d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6243ba0-da6d-4722-8ede-8da1b54d27b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5438730-0044-46ff-962b-e6b97e6ea1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6243ba0-da6d-4722-8ede-8da1b54d27b6",
                    "LayerId": "00e52f48-6d03-47c1-9dba-7037845f4e1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00e52f48-6d03-47c1-9dba-7037845f4e1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95d720ab-7b8a-42a1-881c-cb5b6497e160",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}