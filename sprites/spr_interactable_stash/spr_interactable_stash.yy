{
    "id": "a6975173-dd8d-487d-a8df-5ba7d599281d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_stash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d47b513-eb3c-47a1-bf94-4ad9acaf277e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6975173-dd8d-487d-a8df-5ba7d599281d",
            "compositeImage": {
                "id": "f4ba865e-79b9-4192-b451-01381724eaf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d47b513-eb3c-47a1-bf94-4ad9acaf277e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3f841a-e373-4c39-86a6-66b935d2e191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d47b513-eb3c-47a1-bf94-4ad9acaf277e",
                    "LayerId": "82bdc3bb-4e9e-44d0-8e9d-63c5fd4db555"
                }
            ]
        },
        {
            "id": "ea99e224-403d-4cb9-ada0-ccdf2e692f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6975173-dd8d-487d-a8df-5ba7d599281d",
            "compositeImage": {
                "id": "9a6c823f-13a4-4b53-8511-8c50959b7a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea99e224-403d-4cb9-ada0-ccdf2e692f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "620705fb-a7cc-4487-ae85-0c7a25e98477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea99e224-403d-4cb9-ada0-ccdf2e692f6e",
                    "LayerId": "82bdc3bb-4e9e-44d0-8e9d-63c5fd4db555"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "82bdc3bb-4e9e-44d0-8e9d-63c5fd4db555",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6975173-dd8d-487d-a8df-5ba7d599281d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 44
}