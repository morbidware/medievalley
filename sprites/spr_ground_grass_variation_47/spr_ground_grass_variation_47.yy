{
    "id": "ee07b257-378c-4e2f-ba87-9cd4dc007da1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_grass_variation_47",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "24344ab6-5144-454f-ba1c-f4efde02623e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee07b257-378c-4e2f-ba87-9cd4dc007da1",
            "compositeImage": {
                "id": "b2b70b80-bdfb-45ea-9b11-9d99dd2be304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24344ab6-5144-454f-ba1c-f4efde02623e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe51e74-28f8-4174-9349-989ab44d9463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24344ab6-5144-454f-ba1c-f4efde02623e",
                    "LayerId": "b7eea20a-7b11-496e-aebf-abfe9ab3a6ba"
                }
            ]
        },
        {
            "id": "36fe200f-7596-4fbc-bc28-ece5bb05c2b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee07b257-378c-4e2f-ba87-9cd4dc007da1",
            "compositeImage": {
                "id": "47957aaf-3574-437f-8dff-8406316425b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36fe200f-7596-4fbc-bc28-ece5bb05c2b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a7a083-c385-4fd0-8be2-856f75d8ccd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36fe200f-7596-4fbc-bc28-ece5bb05c2b0",
                    "LayerId": "b7eea20a-7b11-496e-aebf-abfe9ab3a6ba"
                }
            ]
        },
        {
            "id": "9dcec5b1-ff2d-49d1-b0aa-81140f2cd411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee07b257-378c-4e2f-ba87-9cd4dc007da1",
            "compositeImage": {
                "id": "db9c4cfa-1587-403a-986e-b9a2d3297075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dcec5b1-ff2d-49d1-b0aa-81140f2cd411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65db1825-1ebb-4d73-b3d5-71b5f7bb6a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dcec5b1-ff2d-49d1-b0aa-81140f2cd411",
                    "LayerId": "b7eea20a-7b11-496e-aebf-abfe9ab3a6ba"
                }
            ]
        },
        {
            "id": "8a2d7343-e883-4145-a198-44b773d3fc23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee07b257-378c-4e2f-ba87-9cd4dc007da1",
            "compositeImage": {
                "id": "ad602953-2397-4765-a8d0-6f0287794500",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a2d7343-e883-4145-a198-44b773d3fc23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc839cbb-ce5a-4374-bc63-2f503806bbfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a2d7343-e883-4145-a198-44b773d3fc23",
                    "LayerId": "b7eea20a-7b11-496e-aebf-abfe9ab3a6ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b7eea20a-7b11-496e-aebf-abfe9ab3a6ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee07b257-378c-4e2f-ba87-9cd4dc007da1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}