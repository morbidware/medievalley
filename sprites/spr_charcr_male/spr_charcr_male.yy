{
    "id": "14920d05-473f-478e-b32e-e491e0450b65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_male",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a10213bb-8029-4094-8585-c3e48dccb2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14920d05-473f-478e-b32e-e491e0450b65",
            "compositeImage": {
                "id": "fd7b983c-1b5d-4417-adc5-c7aa2108b0c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a10213bb-8029-4094-8585-c3e48dccb2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05eb5453-2980-45d8-bbf2-2da01bd8475f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a10213bb-8029-4094-8585-c3e48dccb2ac",
                    "LayerId": "79af0e3d-1529-4b51-b8c0-80eebf900f17"
                }
            ]
        },
        {
            "id": "f821f3b1-403e-4e38-82aa-fababe548e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14920d05-473f-478e-b32e-e491e0450b65",
            "compositeImage": {
                "id": "0d7e3246-d300-4850-9e5e-57a7aa637742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f821f3b1-403e-4e38-82aa-fababe548e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a8e85b-7e83-48b6-af4b-bc406d0fa4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f821f3b1-403e-4e38-82aa-fababe548e7c",
                    "LayerId": "79af0e3d-1529-4b51-b8c0-80eebf900f17"
                }
            ]
        },
        {
            "id": "20ce3fef-8c75-46fa-a326-bfabc042ec2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14920d05-473f-478e-b32e-e491e0450b65",
            "compositeImage": {
                "id": "6b2b8d99-5994-4a3e-8d29-3d1997d71c43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20ce3fef-8c75-46fa-a326-bfabc042ec2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea22a58-fb67-46ab-b7cb-096c09363de3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20ce3fef-8c75-46fa-a326-bfabc042ec2d",
                    "LayerId": "79af0e3d-1529-4b51-b8c0-80eebf900f17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79af0e3d-1529-4b51-b8c0-80eebf900f17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14920d05-473f-478e-b32e-e491e0450b65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}