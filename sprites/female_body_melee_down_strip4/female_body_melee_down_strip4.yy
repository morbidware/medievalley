{
    "id": "dfe6d56a-6d2d-49fd-8e61-5e9f01a02780",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2524b78b-07be-4519-801c-66afdcff17a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe6d56a-6d2d-49fd-8e61-5e9f01a02780",
            "compositeImage": {
                "id": "87c9a7b8-2973-414a-a85a-333269f9b2ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2524b78b-07be-4519-801c-66afdcff17a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf5fa17-25c1-4a4f-bd06-e675ab857561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2524b78b-07be-4519-801c-66afdcff17a4",
                    "LayerId": "aadfa70c-3da2-4e29-b4d6-c992a5b1edfc"
                }
            ]
        },
        {
            "id": "d8aaebc7-7c38-43c4-bb2b-f1ffce25572e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe6d56a-6d2d-49fd-8e61-5e9f01a02780",
            "compositeImage": {
                "id": "2f7c8319-ed4c-4cdb-9098-6fad4f346964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8aaebc7-7c38-43c4-bb2b-f1ffce25572e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e7c7dab-a99d-44d5-bb1c-86a321f3f7c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8aaebc7-7c38-43c4-bb2b-f1ffce25572e",
                    "LayerId": "aadfa70c-3da2-4e29-b4d6-c992a5b1edfc"
                }
            ]
        },
        {
            "id": "d87913a0-d164-437a-8fd1-64e8f119636e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe6d56a-6d2d-49fd-8e61-5e9f01a02780",
            "compositeImage": {
                "id": "eb442df7-7444-40e6-873e-1d0bc3b6152f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87913a0-d164-437a-8fd1-64e8f119636e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89779d2-5efd-4095-a8ec-0997cc502697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87913a0-d164-437a-8fd1-64e8f119636e",
                    "LayerId": "aadfa70c-3da2-4e29-b4d6-c992a5b1edfc"
                }
            ]
        },
        {
            "id": "f48484b9-bed1-453e-9aff-0ae6f4a56542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfe6d56a-6d2d-49fd-8e61-5e9f01a02780",
            "compositeImage": {
                "id": "63ecfd7f-a3a9-4cfe-b560-d3faa7552567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f48484b9-bed1-453e-9aff-0ae6f4a56542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7861072-ada8-4971-8755-a58fa4e1ac67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f48484b9-bed1-453e-9aff-0ae6f4a56542",
                    "LayerId": "aadfa70c-3da2-4e29-b4d6-c992a5b1edfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aadfa70c-3da2-4e29-b4d6-c992a5b1edfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfe6d56a-6d2d-49fd-8e61-5e9f01a02780",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}