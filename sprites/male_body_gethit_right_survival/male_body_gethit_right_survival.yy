{
    "id": "3e83dc1b-6fc3-4f99-990b-586e0dff5cf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_gethit_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfb97ed0-ffbc-4fa7-8a55-83b98dfb37ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e83dc1b-6fc3-4f99-990b-586e0dff5cf7",
            "compositeImage": {
                "id": "5946e7b1-1d34-4090-a00a-c57cd21ac11c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb97ed0-ffbc-4fa7-8a55-83b98dfb37ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2ff901-3c05-40ff-bdb4-91367ad02f8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb97ed0-ffbc-4fa7-8a55-83b98dfb37ac",
                    "LayerId": "7f55b0ee-b366-4ef3-a2ff-d968e6e97f4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f55b0ee-b366-4ef3-a2ff-d968e6e97f4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e83dc1b-6fc3-4f99-990b-586e0dff5cf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}