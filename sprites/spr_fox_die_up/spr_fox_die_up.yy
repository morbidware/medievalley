{
    "id": "0bc1ff4f-8a1e-42a1-b338-7f09d94f1f68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_die_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ac82e8c-0b38-422f-8899-76e0bee0d80d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bc1ff4f-8a1e-42a1-b338-7f09d94f1f68",
            "compositeImage": {
                "id": "2b1aec3a-3e09-467a-93f7-618f0d2e7946",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ac82e8c-0b38-422f-8899-76e0bee0d80d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4662cc-3051-40fb-bc98-05594800d501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ac82e8c-0b38-422f-8899-76e0bee0d80d",
                    "LayerId": "c8069815-7a6c-4a80-bad0-0b1f4a820544"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c8069815-7a6c-4a80-bad0-0b1f4a820544",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bc1ff4f-8a1e-42a1-b338-7f09d94f1f68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}