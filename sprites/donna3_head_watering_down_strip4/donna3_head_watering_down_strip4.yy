{
    "id": "0ceab87a-449b-4013-953c-a14df73b5f3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "831010a5-ffa2-409b-86ad-e27bf76e671e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ceab87a-449b-4013-953c-a14df73b5f3e",
            "compositeImage": {
                "id": "7ac30b02-137d-4780-9be2-72fd53211501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "831010a5-ffa2-409b-86ad-e27bf76e671e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05860947-d947-420e-8f41-6f8c3166892e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "831010a5-ffa2-409b-86ad-e27bf76e671e",
                    "LayerId": "aa35d695-d0a1-4c47-bb95-7a43905257d1"
                }
            ]
        },
        {
            "id": "b4443565-5e1e-4657-bd91-ee5ba5f21341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ceab87a-449b-4013-953c-a14df73b5f3e",
            "compositeImage": {
                "id": "af06b2a0-d9ca-422a-a882-be6c7827c0a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4443565-5e1e-4657-bd91-ee5ba5f21341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84c23445-11c5-4ac9-83ea-499517680580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4443565-5e1e-4657-bd91-ee5ba5f21341",
                    "LayerId": "aa35d695-d0a1-4c47-bb95-7a43905257d1"
                }
            ]
        },
        {
            "id": "6d8c2dc9-0e2d-45e6-b466-4b8b367af869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ceab87a-449b-4013-953c-a14df73b5f3e",
            "compositeImage": {
                "id": "71590049-e22b-41d0-9df1-106c4662a046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8c2dc9-0e2d-45e6-b466-4b8b367af869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30fdbfd2-e9a1-473c-843b-5e13559c6aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8c2dc9-0e2d-45e6-b466-4b8b367af869",
                    "LayerId": "aa35d695-d0a1-4c47-bb95-7a43905257d1"
                }
            ]
        },
        {
            "id": "f5edd00d-9b50-4430-bf06-abf2ddb73ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ceab87a-449b-4013-953c-a14df73b5f3e",
            "compositeImage": {
                "id": "47aca8c0-0834-4194-a2f8-7eec072b0479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5edd00d-9b50-4430-bf06-abf2ddb73ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ceaf99-42ad-46d4-ba30-a6a02de699ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5edd00d-9b50-4430-bf06-abf2ddb73ed9",
                    "LayerId": "aa35d695-d0a1-4c47-bb95-7a43905257d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aa35d695-d0a1-4c47-bb95-7a43905257d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ceab87a-449b-4013-953c-a14df73b5f3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}