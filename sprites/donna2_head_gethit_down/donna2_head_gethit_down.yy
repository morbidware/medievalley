{
    "id": "849c1a4c-6e82-4032-afcc-ae45c538bd70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8fe50a2-7073-4f5b-b729-3e6d00ac4ffa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849c1a4c-6e82-4032-afcc-ae45c538bd70",
            "compositeImage": {
                "id": "5a11b4a6-ba02-4d81-ba08-54c14d3ba844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8fe50a2-7073-4f5b-b729-3e6d00ac4ffa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b9ce31-3295-421e-bc96-cdbbfff3b721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8fe50a2-7073-4f5b-b729-3e6d00ac4ffa",
                    "LayerId": "c20d5894-174e-43e7-870c-a4df2be596ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c20d5894-174e-43e7-870c-a4df2be596ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "849c1a4c-6e82-4032-afcc-ae45c538bd70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}