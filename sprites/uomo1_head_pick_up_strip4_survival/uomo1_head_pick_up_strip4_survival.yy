{
    "id": "84d5de14-b492-46ed-865d-be894b0213ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_pick_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "549e245b-0d7e-437f-91bd-8f243e573b3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "compositeImage": {
                "id": "faade67f-79ff-4c1f-9bb7-07e97015bcd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "549e245b-0d7e-437f-91bd-8f243e573b3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "490a196b-bcba-43a9-98ce-8f89080deafd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "549e245b-0d7e-437f-91bd-8f243e573b3d",
                    "LayerId": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5"
                }
            ]
        },
        {
            "id": "4f5ee1f6-5ab4-4c18-b3ef-166c629df90d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "compositeImage": {
                "id": "b733ca9e-6f90-4b2a-a0fd-8c9818a787c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f5ee1f6-5ab4-4c18-b3ef-166c629df90d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "003f5a73-90b5-484d-a4a3-85eaf28e29f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f5ee1f6-5ab4-4c18-b3ef-166c629df90d",
                    "LayerId": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5"
                }
            ]
        },
        {
            "id": "89ffc6e9-0b4c-4556-aeeb-4e20fe0f29fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "compositeImage": {
                "id": "0029bfb2-8ce8-4d5c-8339-cfeaa59f9854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ffc6e9-0b4c-4556-aeeb-4e20fe0f29fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1f7291-b777-4050-8270-2e9f44ff13f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ffc6e9-0b4c-4556-aeeb-4e20fe0f29fd",
                    "LayerId": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5"
                }
            ]
        },
        {
            "id": "baf95c96-b6a9-4c77-842a-819a1551b181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "compositeImage": {
                "id": "6b6382ad-3139-4147-9255-af5cfaf32537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baf95c96-b6a9-4c77-842a-819a1551b181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee50990-1be2-4e55-9f52-a1a419315585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baf95c96-b6a9-4c77-842a-819a1551b181",
                    "LayerId": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5"
                }
            ]
        },
        {
            "id": "94f746fd-9804-4fc2-b562-ea07f0d60dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "compositeImage": {
                "id": "7bcbdea7-2fc3-4082-a4a9-a56e1e1f7a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94f746fd-9804-4fc2-b562-ea07f0d60dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bce87c14-7ffb-487f-a3b4-b78ef0b454c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94f746fd-9804-4fc2-b562-ea07f0d60dbe",
                    "LayerId": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5"
                }
            ]
        },
        {
            "id": "f56f9b58-5e2f-4402-8857-017082ad2701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "compositeImage": {
                "id": "a1a1c42d-1a28-4c3c-8d4a-8e19ecba316a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56f9b58-5e2f-4402-8857-017082ad2701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c719629-ab95-42c4-b7ec-552bf3fca013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56f9b58-5e2f-4402-8857-017082ad2701",
                    "LayerId": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aa8e047f-e37e-4414-bb0c-d32e0c1f3fe5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84d5de14-b492-46ed-865d-be894b0213ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}