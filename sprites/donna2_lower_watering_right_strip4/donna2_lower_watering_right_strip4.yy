{
    "id": "0f2c3027-68d7-4d40-8e1f-553bdfb7756d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e530ff59-d521-43ba-ae27-ca3e95e50a49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f2c3027-68d7-4d40-8e1f-553bdfb7756d",
            "compositeImage": {
                "id": "31bc8ef4-c159-45e6-b0b7-54bd20529974",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e530ff59-d521-43ba-ae27-ca3e95e50a49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1b72b4-fb4f-4ed0-be98-1a9dd43a8f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e530ff59-d521-43ba-ae27-ca3e95e50a49",
                    "LayerId": "fc7b0a04-0348-4df9-b902-5711d9a29f51"
                }
            ]
        },
        {
            "id": "b15f77f8-2f7a-47be-a71f-796ad9b532d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f2c3027-68d7-4d40-8e1f-553bdfb7756d",
            "compositeImage": {
                "id": "6b7934ec-7abf-43ca-bd46-2825eb526d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b15f77f8-2f7a-47be-a71f-796ad9b532d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8a2f04-0ca9-4543-8daa-e9f251f2be3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b15f77f8-2f7a-47be-a71f-796ad9b532d6",
                    "LayerId": "fc7b0a04-0348-4df9-b902-5711d9a29f51"
                }
            ]
        },
        {
            "id": "f1c922b0-f7c0-49f7-a485-4d8031312534",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f2c3027-68d7-4d40-8e1f-553bdfb7756d",
            "compositeImage": {
                "id": "64efe185-3f8a-44fb-a60d-a07e4977da8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c922b0-f7c0-49f7-a485-4d8031312534",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c6bb121-3fe5-44e8-92aa-9c844e6c7f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c922b0-f7c0-49f7-a485-4d8031312534",
                    "LayerId": "fc7b0a04-0348-4df9-b902-5711d9a29f51"
                }
            ]
        },
        {
            "id": "b563aeb1-4283-44ea-8e12-66210d0b471a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f2c3027-68d7-4d40-8e1f-553bdfb7756d",
            "compositeImage": {
                "id": "607632d2-df5f-4e5f-aee3-91069ea45d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b563aeb1-4283-44ea-8e12-66210d0b471a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80415fea-0990-4fd6-a542-e9ee3cccac4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b563aeb1-4283-44ea-8e12-66210d0b471a",
                    "LayerId": "fc7b0a04-0348-4df9-b902-5711d9a29f51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc7b0a04-0348-4df9-b902-5711d9a29f51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f2c3027-68d7-4d40-8e1f-553bdfb7756d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}