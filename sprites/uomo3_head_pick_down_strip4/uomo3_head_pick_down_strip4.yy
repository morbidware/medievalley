{
    "id": "d272f2b8-bc3e-4fc3-a5c8-0600cdebd60c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e761394-82e0-489c-8781-107b8a30037e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d272f2b8-bc3e-4fc3-a5c8-0600cdebd60c",
            "compositeImage": {
                "id": "7199f8a1-84cd-443d-b6ad-f58022ab9810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e761394-82e0-489c-8781-107b8a30037e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0df613-ab39-49de-8ecb-cb544129f8d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e761394-82e0-489c-8781-107b8a30037e",
                    "LayerId": "ee2f3bc0-1a96-4226-9e1c-187c397cfbcc"
                }
            ]
        },
        {
            "id": "eda673de-d484-416b-93af-0a18f15a3f49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d272f2b8-bc3e-4fc3-a5c8-0600cdebd60c",
            "compositeImage": {
                "id": "657235ee-3474-45d9-a2fe-79c36c49ab79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda673de-d484-416b-93af-0a18f15a3f49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb34f15a-e105-4fc9-bbff-f86221062d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda673de-d484-416b-93af-0a18f15a3f49",
                    "LayerId": "ee2f3bc0-1a96-4226-9e1c-187c397cfbcc"
                }
            ]
        },
        {
            "id": "f7f39180-c5d6-46d2-924c-d012b34b096b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d272f2b8-bc3e-4fc3-a5c8-0600cdebd60c",
            "compositeImage": {
                "id": "dc4c7047-fc47-4096-8ffe-03f72fa8f962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f39180-c5d6-46d2-924c-d012b34b096b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be148290-b994-4ae1-acb9-b5e45f3b0aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f39180-c5d6-46d2-924c-d012b34b096b",
                    "LayerId": "ee2f3bc0-1a96-4226-9e1c-187c397cfbcc"
                }
            ]
        },
        {
            "id": "6f73b8e9-73c3-4517-afdc-7fbef8815754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d272f2b8-bc3e-4fc3-a5c8-0600cdebd60c",
            "compositeImage": {
                "id": "c777627c-653d-4fcf-9b48-b3e96fe29cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f73b8e9-73c3-4517-afdc-7fbef8815754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "817e45d4-9c3a-4a90-8bd6-bb582a30dff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f73b8e9-73c3-4517-afdc-7fbef8815754",
                    "LayerId": "ee2f3bc0-1a96-4226-9e1c-187c397cfbcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ee2f3bc0-1a96-4226-9e1c-187c397cfbcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d272f2b8-bc3e-4fc3-a5c8-0600cdebd60c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}