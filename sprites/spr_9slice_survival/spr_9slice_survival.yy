{
    "id": "5287973a-f906-485d-88a3-aae2d486607a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9slice_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62824220-3460-46df-91dc-242beb63a148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "99452e54-29d2-4aaa-bd3b-5fc2af3854b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62824220-3460-46df-91dc-242beb63a148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c47f81-56c9-4016-826a-ba865ac57df4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62824220-3460-46df-91dc-242beb63a148",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "3fbbe556-918b-4458-8ba8-43949bbcef64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62824220-3460-46df-91dc-242beb63a148",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "7261386d-f996-4943-8047-ea4f4b3e80c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "32675af6-a07a-4ec4-bbef-187987bc5796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7261386d-f996-4943-8047-ea4f4b3e80c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873293a2-0057-4dfe-a562-89d70dc3972c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7261386d-f996-4943-8047-ea4f4b3e80c4",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "99f1438b-e158-4dc8-83bd-7234d8985d16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7261386d-f996-4943-8047-ea4f4b3e80c4",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "f49f48bc-b34d-42a2-a0a5-97693217cd01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "57852af4-f87c-4c2c-93ad-0b59e9dab090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f49f48bc-b34d-42a2-a0a5-97693217cd01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c704531-b586-45e3-a370-75eeb280c0ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49f48bc-b34d-42a2-a0a5-97693217cd01",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "40bd02cf-e082-42df-bff3-d7f00f5bf3df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49f48bc-b34d-42a2-a0a5-97693217cd01",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "b5863bbd-54ff-4268-8caa-8992cd557f34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "23b9fd57-1be7-452e-9525-b8a765b5251b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5863bbd-54ff-4268-8caa-8992cd557f34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33baeab0-34d6-43ff-8c9c-0573fdf7d951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5863bbd-54ff-4268-8caa-8992cd557f34",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "fe8e859b-6321-4519-9a18-a6380c47da5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5863bbd-54ff-4268-8caa-8992cd557f34",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "e2270ef8-407b-440c-bfbb-2d0752cac5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "f28ec360-45d4-4893-8222-c2ddbbc1201d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2270ef8-407b-440c-bfbb-2d0752cac5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "640cda62-2c97-4d22-88f3-acc59cea9ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2270ef8-407b-440c-bfbb-2d0752cac5a4",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "0886fd7a-0998-4a05-a61e-073ae769e08e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2270ef8-407b-440c-bfbb-2d0752cac5a4",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "772a1b2d-75ab-45af-bb13-d73cc71904e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "784a976d-3dcf-4354-ad51-4a6869a082d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772a1b2d-75ab-45af-bb13-d73cc71904e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5534abf9-9535-424d-8300-7ab48cc175bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772a1b2d-75ab-45af-bb13-d73cc71904e6",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "6ed0d302-af35-4d8d-a64b-581609453d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772a1b2d-75ab-45af-bb13-d73cc71904e6",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "a40ecefd-41c5-43e5-a973-be69b264dbae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "de46f423-c501-4719-a57b-a040fc97729f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40ecefd-41c5-43e5-a973-be69b264dbae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "430ecf83-6b2a-46e7-ac38-610b2cc5535c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40ecefd-41c5-43e5-a973-be69b264dbae",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "abe6a3fc-34c0-468a-b50e-d57c5d25603f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40ecefd-41c5-43e5-a973-be69b264dbae",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "ad1d7472-c5c6-4b2c-a1bd-e8a60faa48e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "71ed229c-0ca4-47f2-868e-a6b938322a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad1d7472-c5c6-4b2c-a1bd-e8a60faa48e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de7115d2-ead3-422b-a4df-4ef751b13eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1d7472-c5c6-4b2c-a1bd-e8a60faa48e3",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "8d826846-d318-40e1-803c-ed720e976290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1d7472-c5c6-4b2c-a1bd-e8a60faa48e3",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        },
        {
            "id": "35ce08cd-11b1-4c12-b22c-1f0009b7233d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "compositeImage": {
                "id": "167d43d3-f495-423b-bfec-fcfdebff2c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ce08cd-11b1-4c12-b22c-1f0009b7233d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2531dc3b-8e26-482c-97c7-fe12fbd8ac82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ce08cd-11b1-4c12-b22c-1f0009b7233d",
                    "LayerId": "df0cde3f-71d2-4425-9356-3f7931a94cbe"
                },
                {
                    "id": "20895d0a-46ac-40b2-b5e6-69384bf7558c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ce08cd-11b1-4c12-b22c-1f0009b7233d",
                    "LayerId": "856df0b9-4169-4f81-b426-4fa11a57d6f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "df0cde3f-71d2-4425-9356-3f7931a94cbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "856df0b9-4169-4f81-b426-4fa11a57d6f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5287973a-f906-485d-88a3-aae2d486607a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}