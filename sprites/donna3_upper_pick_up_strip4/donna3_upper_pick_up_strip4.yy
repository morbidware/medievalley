{
    "id": "09cdb32c-35e0-4110-98d2-3ccb6b567ebd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc31b88a-8f5d-47de-8cdb-aec35cefc504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09cdb32c-35e0-4110-98d2-3ccb6b567ebd",
            "compositeImage": {
                "id": "d4dac6e5-ab69-44e8-86df-61143bc24f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc31b88a-8f5d-47de-8cdb-aec35cefc504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e1c8e1-5c17-4e99-9a7e-633a7ea6a273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc31b88a-8f5d-47de-8cdb-aec35cefc504",
                    "LayerId": "424d2c02-e656-4287-bda1-12f52c772fd3"
                }
            ]
        },
        {
            "id": "48a2aa8c-6b62-4513-9df9-6890ffae3029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09cdb32c-35e0-4110-98d2-3ccb6b567ebd",
            "compositeImage": {
                "id": "fa699799-eab3-45c2-ace7-78ea706a6183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48a2aa8c-6b62-4513-9df9-6890ffae3029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b608a0ad-905a-49ca-8859-494a7854467a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48a2aa8c-6b62-4513-9df9-6890ffae3029",
                    "LayerId": "424d2c02-e656-4287-bda1-12f52c772fd3"
                }
            ]
        },
        {
            "id": "c8b65708-f80f-4ebd-8862-bf6c0d5409c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09cdb32c-35e0-4110-98d2-3ccb6b567ebd",
            "compositeImage": {
                "id": "40cc0f89-846b-4474-9d81-f690e963ade8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b65708-f80f-4ebd-8862-bf6c0d5409c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18beca0a-2a75-4135-b8a9-12a1fe4dd7f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b65708-f80f-4ebd-8862-bf6c0d5409c9",
                    "LayerId": "424d2c02-e656-4287-bda1-12f52c772fd3"
                }
            ]
        },
        {
            "id": "838c1896-fcce-4f69-b350-09218f12f384",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09cdb32c-35e0-4110-98d2-3ccb6b567ebd",
            "compositeImage": {
                "id": "edcbbd88-4f45-4bdd-8973-171ab74d8a43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "838c1896-fcce-4f69-b350-09218f12f384",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13948182-8643-4be7-a64b-dd1c2fea34e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "838c1896-fcce-4f69-b350-09218f12f384",
                    "LayerId": "424d2c02-e656-4287-bda1-12f52c772fd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "424d2c02-e656-4287-bda1-12f52c772fd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09cdb32c-35e0-4110-98d2-3ccb6b567ebd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}