{
    "id": "ecfbb4ef-de79-4faf-a511-9aa995bb3c62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_bread",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea245582-c47a-47cd-b2d2-dba01f8b40d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecfbb4ef-de79-4faf-a511-9aa995bb3c62",
            "compositeImage": {
                "id": "eada5e0d-8487-4f99-aa87-5fe01f6b56bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea245582-c47a-47cd-b2d2-dba01f8b40d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b02378c-feb2-4ac6-8fa4-dd818fa13024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea245582-c47a-47cd-b2d2-dba01f8b40d0",
                    "LayerId": "83c9f6b5-a9f4-4311-83bb-64c67c190cb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "83c9f6b5-a9f4-4311-83bb-64c67c190cb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecfbb4ef-de79-4faf-a511-9aa995bb3c62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 13
}