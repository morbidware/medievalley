{
    "id": "84a497fb-1f3c-4e0d-b325-91e6ba50677b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_basementfloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bd17b17-7dd1-450d-8522-2abd3f31dced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84a497fb-1f3c-4e0d-b325-91e6ba50677b",
            "compositeImage": {
                "id": "e41af607-ba6b-41a3-b2c4-7bc50e4bf8ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd17b17-7dd1-450d-8522-2abd3f31dced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae68597-460e-4836-8d83-2763f795c040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd17b17-7dd1-450d-8522-2abd3f31dced",
                    "LayerId": "dddc3aaa-f001-4e57-b6cf-aaa9fc8e5e23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dddc3aaa-f001-4e57-b6cf-aaa9fc8e5e23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84a497fb-1f3c-4e0d-b325-91e6ba50677b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}