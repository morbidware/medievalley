{
    "id": "22c1b3e5-5548-41a3-bd1f-d163dc4a90b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_wateringcan_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b20a5a4d-5d66-4d3a-bf37-b450a2a5add2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22c1b3e5-5548-41a3-bd1f-d163dc4a90b3",
            "compositeImage": {
                "id": "a2367659-c0b4-4206-8ccf-d58c3d48d3b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20a5a4d-5d66-4d3a-bf37-b450a2a5add2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c721cbc-4cfc-4032-8240-06751fd3338f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20a5a4d-5d66-4d3a-bf37-b450a2a5add2",
                    "LayerId": "0c95050d-a0e3-42be-b74c-3d4a7ae44c4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0c95050d-a0e3-42be-b74c-3d4a7ae44c4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22c1b3e5-5548-41a3-bd1f-d163dc4a90b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 7
}