{
    "id": "d8c7cf0c-bbd7-41a7-bc9e-e834dbcb6ae7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_fence_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c72e087-6cd5-47d1-bfc1-28dcf4a69212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8c7cf0c-bbd7-41a7-bc9e-e834dbcb6ae7",
            "compositeImage": {
                "id": "b2113d7d-45d9-467e-8c9f-0f5c838d5729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c72e087-6cd5-47d1-bfc1-28dcf4a69212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf27c562-5e68-45ff-b26f-2f5e265c5bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c72e087-6cd5-47d1-bfc1-28dcf4a69212",
                    "LayerId": "0b701bd6-3c54-4bb2-932c-59c12b91332f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 240,
    "layers": [
        {
            "id": "0b701bd6-3c54-4bb2-932c-59c12b91332f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8c7cf0c-bbd7-41a7-bc9e-e834dbcb6ae7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 120
}