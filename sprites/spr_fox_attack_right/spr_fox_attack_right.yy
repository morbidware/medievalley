{
    "id": "0e062616-8ce8-4dda-b42a-1acdfaafd132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f76ede6-a1c2-462e-987f-a5057a037042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e062616-8ce8-4dda-b42a-1acdfaafd132",
            "compositeImage": {
                "id": "f6cf3334-0357-4e61-93cc-02c55780bed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f76ede6-a1c2-462e-987f-a5057a037042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e18e7c88-4d0d-41f8-a52f-88c98edbc215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f76ede6-a1c2-462e-987f-a5057a037042",
                    "LayerId": "d5ba216e-ae53-4805-afd1-92739cc9f861"
                }
            ]
        },
        {
            "id": "414219fb-19dc-4e69-9edf-4f81c10d183b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e062616-8ce8-4dda-b42a-1acdfaafd132",
            "compositeImage": {
                "id": "ba304faa-0333-4e33-944b-4d3dc4a8f53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414219fb-19dc-4e69-9edf-4f81c10d183b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef42038-8cc8-4c38-bd22-a83bbf1c6ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414219fb-19dc-4e69-9edf-4f81c10d183b",
                    "LayerId": "d5ba216e-ae53-4805-afd1-92739cc9f861"
                }
            ]
        },
        {
            "id": "96fe7c92-3b60-4c42-bb7c-60c4fd2f64fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e062616-8ce8-4dda-b42a-1acdfaafd132",
            "compositeImage": {
                "id": "9f265eae-2d77-437d-a921-e5bc220eb024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96fe7c92-3b60-4c42-bb7c-60c4fd2f64fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ba97e21-80f6-4920-9902-8d72d87d22e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96fe7c92-3b60-4c42-bb7c-60c4fd2f64fd",
                    "LayerId": "d5ba216e-ae53-4805-afd1-92739cc9f861"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5ba216e-ae53-4805-afd1-92739cc9f861",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e062616-8ce8-4dda-b42a-1acdfaafd132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}