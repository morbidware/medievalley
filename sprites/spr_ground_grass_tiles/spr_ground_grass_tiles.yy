{
    "id": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_grass_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af67c9de-51d6-43e4-91c3-639620e7c0fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "2d8f146e-b587-4a9f-bfd7-88d8c966a435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af67c9de-51d6-43e4-91c3-639620e7c0fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71d5034b-ea8e-4893-96ca-74030bc9062f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af67c9de-51d6-43e4-91c3-639620e7c0fc",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "4e0e99ce-57d7-4dff-80bc-03d565b9a886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "252d17fd-1e4e-4b01-addd-85870460b2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0e99ce-57d7-4dff-80bc-03d565b9a886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb42b2f0-0488-47c2-adcf-779670893fc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0e99ce-57d7-4dff-80bc-03d565b9a886",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "9ddc94d4-d8c7-40c4-bd10-9737958e5705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "d571de50-7dae-406d-acbd-120231624efc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ddc94d4-d8c7-40c4-bd10-9737958e5705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e14226cc-6336-47ba-9418-2957a54a0ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ddc94d4-d8c7-40c4-bd10-9737958e5705",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "0bf314c6-9ed6-45b9-975d-be7d0c413122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "6ce0cf01-e037-499b-8ed1-d03e3ac10997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bf314c6-9ed6-45b9-975d-be7d0c413122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f2ac20-95c6-40a1-8f4d-7934ca0f9003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bf314c6-9ed6-45b9-975d-be7d0c413122",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "c9842f68-57fe-4b8c-aeb4-820599c5dff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "0e4eff37-f3c6-4faf-be23-6b44dcd7a52b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9842f68-57fe-4b8c-aeb4-820599c5dff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db2eb8e-9a84-449b-af28-b64aef3ec537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9842f68-57fe-4b8c-aeb4-820599c5dff7",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "86834136-5aff-4ebf-911e-ba00c21e08a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "c8ab2794-4752-41ae-bf49-af286f3f0c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86834136-5aff-4ebf-911e-ba00c21e08a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28a196c8-19bb-4ef5-a8f5-d7fbdee27abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86834136-5aff-4ebf-911e-ba00c21e08a2",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "568ea32e-f4b3-4a46-aa63-7bc495283649",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "d5acda02-a3fc-48c4-896f-d39e0b07e660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "568ea32e-f4b3-4a46-aa63-7bc495283649",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8c0fc0-a18a-4149-b284-f86f4ceeaa8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "568ea32e-f4b3-4a46-aa63-7bc495283649",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "d2ee8e93-cb82-4191-ae21-212971bf6007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "718dc1fe-02bf-4e31-a0c1-12af5a03087c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ee8e93-cb82-4191-ae21-212971bf6007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f44529ba-e4f7-41e2-b553-d14d53cedbf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ee8e93-cb82-4191-ae21-212971bf6007",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "28518cf3-eb7c-460c-be7e-9228bbe84878",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "087635fc-251d-4189-a5ed-239dadaa826e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28518cf3-eb7c-460c-be7e-9228bbe84878",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2a52233-646a-447e-8009-326fd6d0868b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28518cf3-eb7c-460c-be7e-9228bbe84878",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "f83c15a7-fbf5-499b-a78b-f68affeda74e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "359132b5-752c-436a-a49d-a44db652bc46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83c15a7-fbf5-499b-a78b-f68affeda74e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebe4de0d-303a-420e-88ef-97a22af5609e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83c15a7-fbf5-499b-a78b-f68affeda74e",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "c906271c-4ce4-4b70-9b94-fabac6a6039e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "4e6b22b4-7dfd-4c8e-99ae-7bc4cc71422c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c906271c-4ce4-4b70-9b94-fabac6a6039e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f93a9ec-d314-49bb-ba24-8f8a4a0bc6f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c906271c-4ce4-4b70-9b94-fabac6a6039e",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "6bcc405f-8283-4bbe-81c9-cd01055e0af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "bc175f2b-3878-4c17-b9ad-efe5818246c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bcc405f-8283-4bbe-81c9-cd01055e0af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df60cc94-fb4b-4d83-a439-d6c41dcf5f98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bcc405f-8283-4bbe-81c9-cd01055e0af8",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "ffb701aa-cfc2-453f-b58d-c2a4d7e8fb95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "b7a8cc98-15e2-41d2-8e22-85b02339cc93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffb701aa-cfc2-453f-b58d-c2a4d7e8fb95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0f0804-3dbd-42c5-8d4d-adca98f362c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffb701aa-cfc2-453f-b58d-c2a4d7e8fb95",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "66acd353-405f-4635-bbf6-3ab58e27dedc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "11bc65f8-b684-494d-ad95-d29b73d0384b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66acd353-405f-4635-bbf6-3ab58e27dedc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f3c70f4-acb7-42c8-a644-ee789385888f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66acd353-405f-4635-bbf6-3ab58e27dedc",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "10554370-3fd8-4032-8059-8228208470a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "006b4e87-1084-46bc-8eac-3baca6adb707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10554370-3fd8-4032-8059-8228208470a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6978191f-9585-4903-a13e-e04d0a701a03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10554370-3fd8-4032-8059-8228208470a2",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "24c98b0b-b320-4ba5-9e75-533b0183a7c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "f174ecb0-ec2e-4783-8c34-b62ac298dbbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c98b0b-b320-4ba5-9e75-533b0183a7c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b37eba09-24eb-4d2f-80f1-1708203c4dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c98b0b-b320-4ba5-9e75-533b0183a7c5",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "7cf72e29-87be-4472-a614-85d0833ed286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "2c59a173-eb4f-4313-9667-5ff1266d404c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf72e29-87be-4472-a614-85d0833ed286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82da5ef7-28d0-4ff4-aeb6-0b3aea99b889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf72e29-87be-4472-a614-85d0833ed286",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "739a5c20-2f78-46a9-8211-9795f4c884da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "a7bd90a7-ff9b-42f0-a3ef-15a31653ff45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "739a5c20-2f78-46a9-8211-9795f4c884da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58981473-3c92-4406-829c-b543854cdd7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "739a5c20-2f78-46a9-8211-9795f4c884da",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "141d928b-5328-4b89-ba09-b615846b67e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "f9f1c5ed-cf50-4cd7-b6c2-002d3f554548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "141d928b-5328-4b89-ba09-b615846b67e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3208a0eb-7960-4c68-83db-e9413278c62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141d928b-5328-4b89-ba09-b615846b67e1",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "f3d2a96f-a4ff-4e7e-8b99-1ebefb4c0443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "8d071570-06e6-4dff-83b1-222fef50a9e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d2a96f-a4ff-4e7e-8b99-1ebefb4c0443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a89a7b-181b-4275-85b8-0f6a00257568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d2a96f-a4ff-4e7e-8b99-1ebefb4c0443",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "5b2ebd56-fb00-4d28-af59-9b1da812fc25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "1e0ce774-2e5b-4719-8753-f30b9455f3ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b2ebd56-fb00-4d28-af59-9b1da812fc25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d16328a-7f90-4c8b-ace2-1d8ee3d26aab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b2ebd56-fb00-4d28-af59-9b1da812fc25",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "485394b0-f3c4-4e60-a03b-03851d4e6a74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "69b58035-b2c2-49a5-80ff-5b2109773de1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485394b0-f3c4-4e60-a03b-03851d4e6a74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af603eee-75df-4873-965f-206d3e27d30a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485394b0-f3c4-4e60-a03b-03851d4e6a74",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "7bade98b-f54c-4719-88d8-c63cc2b386ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "8a5780be-9fe5-4b30-b50b-74919ac5dd92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bade98b-f54c-4719-88d8-c63cc2b386ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f49c5174-e8a8-4785-beb9-2693b8c3cf93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bade98b-f54c-4719-88d8-c63cc2b386ee",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "41574dc1-6a85-4512-8cd3-8243e36a6a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "804fbcef-3070-4852-bd90-13806b4cff17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41574dc1-6a85-4512-8cd3-8243e36a6a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c64858c-7bc5-4059-b284-fafd39dba01a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41574dc1-6a85-4512-8cd3-8243e36a6a1c",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "d87bd5c6-1dea-4c9a-892a-aad701652253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "6465aac4-ae44-4696-82a2-150002e52bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87bd5c6-1dea-4c9a-892a-aad701652253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb3c2310-1cef-4c92-88b0-4f7d5b0cf781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87bd5c6-1dea-4c9a-892a-aad701652253",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "fd75831c-39f0-4213-9297-a80ba1cf1915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "03391830-fae2-4737-b915-fad7335e9742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd75831c-39f0-4213-9297-a80ba1cf1915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "651ddc89-0c8c-40ec-8a2b-32d7a0d6e2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd75831c-39f0-4213-9297-a80ba1cf1915",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "f74036f6-8e83-4f36-95e6-ba07424a963b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "32d3be10-80a8-4170-a694-85b8b891413b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f74036f6-8e83-4f36-95e6-ba07424a963b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47b3846-11c9-422f-aa9a-a9e4b78dc704",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f74036f6-8e83-4f36-95e6-ba07424a963b",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "f5ed56a3-cab0-4c17-adb0-950606e69ab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "cb6639d2-b7db-45ff-b153-3f750da77feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ed56a3-cab0-4c17-adb0-950606e69ab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e57cf5b-5fad-493d-a1b6-3bc81bbd7df6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ed56a3-cab0-4c17-adb0-950606e69ab9",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "449019f7-7136-4c60-902c-f9958c6c31e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "ebf7b23e-4f85-4a8d-9117-244b79446e7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449019f7-7136-4c60-902c-f9958c6c31e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f30ea9-7b30-4419-9aa9-e91bb14ee555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449019f7-7136-4c60-902c-f9958c6c31e7",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "3a8ff1ab-195f-4de5-ab23-c9fbef8ba10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "8219fcc9-cc0a-4b85-90bc-ee7a1875f74d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8ff1ab-195f-4de5-ab23-c9fbef8ba10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ee5ee3-c891-4916-bc4b-0d39857d2462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8ff1ab-195f-4de5-ab23-c9fbef8ba10c",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "53718f99-8bfb-4f47-9a1c-e6bd3d9ae00f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "9c0a94ed-a6e6-4024-be11-3b740f7f1ae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53718f99-8bfb-4f47-9a1c-e6bd3d9ae00f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef0a07f-f223-4c63-b2e7-ca5987bf31c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53718f99-8bfb-4f47-9a1c-e6bd3d9ae00f",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "e2c6d622-91d7-4b84-9fb4-a5cbf0d0b523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "cf6251ab-2857-47f8-b8a2-8387f144cf6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2c6d622-91d7-4b84-9fb4-a5cbf0d0b523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f99d49-ad59-42f0-87b3-e7bec272705e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2c6d622-91d7-4b84-9fb4-a5cbf0d0b523",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "584f2dcc-3288-4acf-b91c-fef4a5463b0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "ecf404af-b429-47c4-b821-50b4f637f4b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "584f2dcc-3288-4acf-b91c-fef4a5463b0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "956f4731-76c4-4a36-8767-3c02c8475ccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "584f2dcc-3288-4acf-b91c-fef4a5463b0c",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "7f7a1f11-25ec-40ed-ab58-8f310a4296ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "2046a0fa-521c-43e5-85f9-c3dab9c45b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7a1f11-25ec-40ed-ab58-8f310a4296ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14ecfab4-6746-4070-b437-eb83a36824d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7a1f11-25ec-40ed-ab58-8f310a4296ef",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "571fd6d4-7fad-4c34-9961-a3c5f0044ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "c4bbdf96-1577-4c3b-960a-b5a71a4f91f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "571fd6d4-7fad-4c34-9961-a3c5f0044ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2aebb3-0a13-485c-9c2d-2782e7d00b8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "571fd6d4-7fad-4c34-9961-a3c5f0044ce7",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "61637d91-cad4-4ca9-abd2-47bfcd6fa437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "9d18223a-ce44-4a5a-834f-79aa9186e6c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61637d91-cad4-4ca9-abd2-47bfcd6fa437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eac246c-aaf6-4a78-b704-595017914fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61637d91-cad4-4ca9-abd2-47bfcd6fa437",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "82e7bb05-e144-40eb-b5b9-a90925cfdfe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "a0cb6902-ad8a-4a1c-b84c-81f365293c1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82e7bb05-e144-40eb-b5b9-a90925cfdfe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "825a79ff-3166-4453-9e83-445151a9c0fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82e7bb05-e144-40eb-b5b9-a90925cfdfe2",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "e302b35f-0759-4e5b-91da-de5001199666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "1aa3c283-977f-4b6d-8894-b95767059c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e302b35f-0759-4e5b-91da-de5001199666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa63c98b-6887-47ab-88cc-a5150d90dc9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e302b35f-0759-4e5b-91da-de5001199666",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "012478fa-2724-4a9b-b9ea-faa1e36c0712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "cd8b4f0d-d039-475c-a6e1-114a59c1e333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "012478fa-2724-4a9b-b9ea-faa1e36c0712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f8a79cf-01f3-40e8-8be2-9dfb41aee0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "012478fa-2724-4a9b-b9ea-faa1e36c0712",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "34159902-d1ac-448f-8c08-7c1338e64602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "0397e7e2-090d-4e0c-b3dc-bb3a12147299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34159902-d1ac-448f-8c08-7c1338e64602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3309cf5d-ffee-4649-a8c1-875d650b1366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34159902-d1ac-448f-8c08-7c1338e64602",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "7eba1353-e070-46e6-b3fa-c4f583acc8fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "d5fb07a2-bfaf-4171-aa10-f6f1ce3ef7bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eba1353-e070-46e6-b3fa-c4f583acc8fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fa04a4d-4591-4f37-8f83-35781b6f799e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eba1353-e070-46e6-b3fa-c4f583acc8fd",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "90e73706-f5b5-4ab3-9067-b2cce60207a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "773cd64d-fee0-42f1-a032-e58b0572d725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e73706-f5b5-4ab3-9067-b2cce60207a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4fc5e4-de0e-47d3-9fbd-f39edfbc6863",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e73706-f5b5-4ab3-9067-b2cce60207a7",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "021a3101-b24f-43e8-94da-ad31d3002c13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "20616528-f12d-4e1c-b041-23fef7fdac1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "021a3101-b24f-43e8-94da-ad31d3002c13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205dd980-ad20-49f4-9953-6c44e3405e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "021a3101-b24f-43e8-94da-ad31d3002c13",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "f34ffadd-9445-427a-b6e9-b0cc88bbfcf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "f11ec0d7-b702-434f-b853-43fe68d9c6fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f34ffadd-9445-427a-b6e9-b0cc88bbfcf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78e404ec-5605-4246-b25a-d77538ec0f22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f34ffadd-9445-427a-b6e9-b0cc88bbfcf8",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "33e9756c-f503-4bd7-a321-05f5ff725ecb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "f6d8123c-7674-4b17-9afb-f38371077c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e9756c-f503-4bd7-a321-05f5ff725ecb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08ef5f1-b71b-416f-8c80-8f23ecaa51de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e9756c-f503-4bd7-a321-05f5ff725ecb",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "67d9924a-bfa3-4441-a0df-139fc64bcd8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "2dc4e29a-234e-4ed5-a914-d6b8120d6c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d9924a-bfa3-4441-a0df-139fc64bcd8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f69aa89a-55af-493c-9902-ace6f942d504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d9924a-bfa3-4441-a0df-139fc64bcd8b",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "b63c878f-955b-497e-896e-54ff260b5ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "6b176efd-51e3-49f2-b3c4-0d85b7fb13c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b63c878f-955b-497e-896e-54ff260b5ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce49582-d245-492e-91e5-2cf7c3e98b75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b63c878f-955b-497e-896e-54ff260b5ce4",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "2a7635e0-4e75-436f-9717-f30619ac5664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "8ca781ec-fa19-47ae-bd82-6a23695092de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a7635e0-4e75-436f-9717-f30619ac5664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "251c3667-0ce6-4610-832c-58fbe065fa0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a7635e0-4e75-436f-9717-f30619ac5664",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        },
        {
            "id": "cd35335b-09b1-4715-aef8-92266e08eca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "compositeImage": {
                "id": "466f1da7-2384-4987-8462-d86bc97354b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd35335b-09b1-4715-aef8-92266e08eca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce9f3ec9-31b4-487a-8012-74f6cc6019bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd35335b-09b1-4715-aef8-92266e08eca6",
                    "LayerId": "528e2272-b89d-4e97-85d7-4d0361179590"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "528e2272-b89d-4e97-85d7-4d0361179590",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5a1f1e2-2008-4551-aa48-34a7867a6223",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}