{
    "id": "e9f82ff5-81db-4153-be40-113c360709ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_watering_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83fbc5fe-8c66-4a92-b956-89f42b5c6859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "compositeImage": {
                "id": "61694c84-98ae-46ac-9571-48f293be76c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83fbc5fe-8c66-4a92-b956-89f42b5c6859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "866405e6-ad3a-4a1f-b54d-fecdf51ba08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83fbc5fe-8c66-4a92-b956-89f42b5c6859",
                    "LayerId": "77d69bd0-e6d9-4af9-8a69-107bd512f896"
                }
            ]
        },
        {
            "id": "611268c0-2d83-401c-a273-ee4de259c75c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "compositeImage": {
                "id": "ed58e06c-e3fb-4319-8c93-9c111c5d8118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "611268c0-2d83-401c-a273-ee4de259c75c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "116e6008-615f-4482-930f-d35ab3aa9b65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611268c0-2d83-401c-a273-ee4de259c75c",
                    "LayerId": "77d69bd0-e6d9-4af9-8a69-107bd512f896"
                }
            ]
        },
        {
            "id": "dfa16f08-242e-4b6d-b904-4388d9b9940b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "compositeImage": {
                "id": "3941940c-b0cd-4a68-bbcf-3c82bad6b912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa16f08-242e-4b6d-b904-4388d9b9940b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84068e9-7395-4e0e-b2a4-07b589201ee8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa16f08-242e-4b6d-b904-4388d9b9940b",
                    "LayerId": "77d69bd0-e6d9-4af9-8a69-107bd512f896"
                }
            ]
        },
        {
            "id": "39ebb288-c64d-4e64-9b5d-be236b0e801b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "compositeImage": {
                "id": "7f894824-0f18-4107-83c0-c392d7f5a471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ebb288-c64d-4e64-9b5d-be236b0e801b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c8ba3e3-8e8d-4296-b51b-d6437e642089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ebb288-c64d-4e64-9b5d-be236b0e801b",
                    "LayerId": "77d69bd0-e6d9-4af9-8a69-107bd512f896"
                }
            ]
        },
        {
            "id": "1f35d4ca-2bc3-4bfd-b24d-a1662f773644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "compositeImage": {
                "id": "a1d6bd99-6959-411b-898a-e3108231bcef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f35d4ca-2bc3-4bfd-b24d-a1662f773644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3361092a-e9c7-49d3-8a30-17fd7992a4a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f35d4ca-2bc3-4bfd-b24d-a1662f773644",
                    "LayerId": "77d69bd0-e6d9-4af9-8a69-107bd512f896"
                }
            ]
        },
        {
            "id": "44e9b8e5-167b-4397-9671-f9d41192051f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "compositeImage": {
                "id": "4dca4d4d-401c-4649-a845-ec4a0f4aa1f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44e9b8e5-167b-4397-9671-f9d41192051f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967a0a73-bb9e-4faa-a4b4-41a16028b352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44e9b8e5-167b-4397-9671-f9d41192051f",
                    "LayerId": "77d69bd0-e6d9-4af9-8a69-107bd512f896"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "77d69bd0-e6d9-4af9-8a69-107bd512f896",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9f82ff5-81db-4153-be40-113c360709ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}