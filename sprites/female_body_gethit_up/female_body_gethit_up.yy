{
    "id": "77017c6d-f1f1-4d54-9e4d-5f3f6e32ff1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eac744b8-4aaa-4662-8963-bab1f5560d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77017c6d-f1f1-4d54-9e4d-5f3f6e32ff1f",
            "compositeImage": {
                "id": "5f942c82-6e08-474d-8ccb-33214ffb9909",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eac744b8-4aaa-4662-8963-bab1f5560d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bce913b5-b537-49ce-86b0-3884cce8b85d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eac744b8-4aaa-4662-8963-bab1f5560d23",
                    "LayerId": "fdfbe0a8-fa43-4af8-acc2-1f944702a0fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fdfbe0a8-fa43-4af8-acc2-1f944702a0fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77017c6d-f1f1-4d54-9e4d-5f3f6e32ff1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}