{
    "id": "4f893b18-a7c7-490e-82b6-4b1d2199a186",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_emotes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ee75dde-a1d1-445c-80dd-77b3305e6d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f893b18-a7c7-490e-82b6-4b1d2199a186",
            "compositeImage": {
                "id": "c80eabdf-3e20-4d9e-8048-4b47d356a145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee75dde-a1d1-445c-80dd-77b3305e6d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5cba51f-db96-45a1-888f-7d5ec71997c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee75dde-a1d1-445c-80dd-77b3305e6d78",
                    "LayerId": "c901b89c-7baa-4e4f-829a-0c95d82671d5"
                }
            ]
        },
        {
            "id": "9a0c83d3-290c-45d8-87b5-1c0953ee4d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f893b18-a7c7-490e-82b6-4b1d2199a186",
            "compositeImage": {
                "id": "85300a77-08e0-4cc6-bd89-04ec3cccacb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0c83d3-290c-45d8-87b5-1c0953ee4d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58aa555f-eefd-401b-b4c0-0a2493c05626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0c83d3-290c-45d8-87b5-1c0953ee4d22",
                    "LayerId": "c901b89c-7baa-4e4f-829a-0c95d82671d5"
                }
            ]
        },
        {
            "id": "b3f13350-e187-47e0-aec9-d9c686e81d7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f893b18-a7c7-490e-82b6-4b1d2199a186",
            "compositeImage": {
                "id": "e11031a8-ca39-4707-8926-cc0712a094b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f13350-e187-47e0-aec9-d9c686e81d7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9af4049-824a-4bbd-824f-0304fde6e3ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f13350-e187-47e0-aec9-d9c686e81d7b",
                    "LayerId": "c901b89c-7baa-4e4f-829a-0c95d82671d5"
                }
            ]
        },
        {
            "id": "faa29b6b-63d2-4b99-8c7d-c8ac57020599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f893b18-a7c7-490e-82b6-4b1d2199a186",
            "compositeImage": {
                "id": "41d1a423-5869-4148-afc1-a6b19e296c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa29b6b-63d2-4b99-8c7d-c8ac57020599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0c6ad80-d458-4f1e-b3fa-d431a722e084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa29b6b-63d2-4b99-8c7d-c8ac57020599",
                    "LayerId": "c901b89c-7baa-4e4f-829a-0c95d82671d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "c901b89c-7baa-4e4f-829a-0c95d82671d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f893b18-a7c7-490e-82b6-4b1d2199a186",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}