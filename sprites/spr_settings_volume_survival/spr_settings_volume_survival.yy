{
    "id": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_volume_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80bee8f0-b106-40eb-b7cf-95706331df62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "21fd5794-5e9f-481a-986f-15e131c01135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80bee8f0-b106-40eb-b7cf-95706331df62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec2ba854-42a8-4c81-a37e-4de025b382c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80bee8f0-b106-40eb-b7cf-95706331df62",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "6abb1eef-090a-43f7-8130-eef748ed9111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "b3a48a4a-5ef4-4100-9586-ebafaa00f73b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abb1eef-090a-43f7-8130-eef748ed9111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a43b28-4231-4f81-a17c-ca9b44e85775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abb1eef-090a-43f7-8130-eef748ed9111",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "dd0631a4-6944-4f8b-839a-69b68607ced2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "e3af5b87-75b0-463c-a3aa-eb28c9fb2dd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd0631a4-6944-4f8b-839a-69b68607ced2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b8836a1-1b7d-4710-ba65-da71f88010eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd0631a4-6944-4f8b-839a-69b68607ced2",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "e38e969b-602b-43e1-9461-59e19a3fd40c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "b1479c63-a760-46ad-8591-930ef7e3df12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e38e969b-602b-43e1-9461-59e19a3fd40c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e98b37fa-d6bb-46f4-b65b-99688776115f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e38e969b-602b-43e1-9461-59e19a3fd40c",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "99e3933c-d0a5-4487-b25e-0d511bd1d6ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "fcb08d4d-d4ae-424c-80a4-408a1c0436a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e3933c-d0a5-4487-b25e-0d511bd1d6ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0123840-0a75-4b7a-90dd-2fa825c0b06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e3933c-d0a5-4487-b25e-0d511bd1d6ab",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "a9e75f12-6269-4caa-ac26-6ab236bb4cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "7323536e-a23c-4fb9-92ff-5e97e877de75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9e75f12-6269-4caa-ac26-6ab236bb4cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a43a36-71fd-418f-b5d0-b1c5499cd0f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9e75f12-6269-4caa-ac26-6ab236bb4cff",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "810d1b33-e301-44b1-bbe5-d081f27c76fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "b88302a2-1699-4bb6-bed9-9b9b23e228ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "810d1b33-e301-44b1-bbe5-d081f27c76fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfcb64e1-6e7a-4912-bff9-c3970ce9c1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "810d1b33-e301-44b1-bbe5-d081f27c76fd",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "b7bebd98-5dde-46e7-af60-5c69f8cbecd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "f6ffde05-2966-4034-a86a-e7938b1164e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7bebd98-5dde-46e7-af60-5c69f8cbecd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac6e9ce-afee-4b2f-8af7-390f0aa35947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7bebd98-5dde-46e7-af60-5c69f8cbecd9",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "617757dc-a770-4691-947a-f79e8b68802c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "1c8de5f4-55d3-454c-8ec1-61b196e04939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "617757dc-a770-4691-947a-f79e8b68802c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbabbfe0-f376-408e-b8cf-da0eafd9cf3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "617757dc-a770-4691-947a-f79e8b68802c",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "b1ccd856-92da-446d-8c67-0ba95d8bb184",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "737180ec-8aed-4fec-8ebb-d1f09415acca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1ccd856-92da-446d-8c67-0ba95d8bb184",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eadb0cb8-3fe9-4496-8d41-89737f19dd15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1ccd856-92da-446d-8c67-0ba95d8bb184",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        },
        {
            "id": "00c4f6d8-1d9c-40a3-be35-2f05edfbb71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "compositeImage": {
                "id": "928f12df-909c-4943-9428-361f9c42021f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c4f6d8-1d9c-40a3-be35-2f05edfbb71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddfadbe0-19c9-4c76-8d5f-e68174c7d1e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c4f6d8-1d9c-40a3-be35-2f05edfbb71d",
                    "LayerId": "da300e86-e494-4b60-8e83-553c6b0f49b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "da300e86-e494-4b60-8e83-553c6b0f49b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7d078ee-1068-4aef-aeea-b121c309bf7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 12
}