{
    "id": "5b075644-9972-4aa0-aff4-6c1003c26511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_gethit_down_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d200518f-7303-4eb0-82cb-b87184cae141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b075644-9972-4aa0-aff4-6c1003c26511",
            "compositeImage": {
                "id": "615c2449-21e7-443d-b533-55a6eb430cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d200518f-7303-4eb0-82cb-b87184cae141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "768f7065-9866-4548-855e-8cad152b3d94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d200518f-7303-4eb0-82cb-b87184cae141",
                    "LayerId": "25699798-160a-4b3f-ace6-772be3d239d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "25699798-160a-4b3f-ace6-772be3d239d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b075644-9972-4aa0-aff4-6c1003c26511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}