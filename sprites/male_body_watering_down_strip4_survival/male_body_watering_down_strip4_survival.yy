{
    "id": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_watering_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1c6462b-5761-47a2-b23b-9646e9d61002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "compositeImage": {
                "id": "02d8f144-1e13-41a9-b268-b221628daa7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1c6462b-5761-47a2-b23b-9646e9d61002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb46860c-b11c-448b-b34b-5d5baedcd582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1c6462b-5761-47a2-b23b-9646e9d61002",
                    "LayerId": "00c45224-9111-4777-bd5b-6328d6ce38d3"
                }
            ]
        },
        {
            "id": "94b90dad-1f32-4380-a996-213b0328f76e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "compositeImage": {
                "id": "e48a5e53-9a91-49db-8349-2023ef2addfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94b90dad-1f32-4380-a996-213b0328f76e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67bd8552-9f2a-4b11-ba14-6e3a9e838169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94b90dad-1f32-4380-a996-213b0328f76e",
                    "LayerId": "00c45224-9111-4777-bd5b-6328d6ce38d3"
                }
            ]
        },
        {
            "id": "1f34eb31-eb60-4fa4-aa32-fd4a6d980768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "compositeImage": {
                "id": "3d8febf6-8c86-48fe-a271-7ffd5ef012bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f34eb31-eb60-4fa4-aa32-fd4a6d980768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ef6354-1e0a-4d70-96f5-52d960cab75d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f34eb31-eb60-4fa4-aa32-fd4a6d980768",
                    "LayerId": "00c45224-9111-4777-bd5b-6328d6ce38d3"
                }
            ]
        },
        {
            "id": "14f67890-3f7b-4257-a841-34befa411ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "compositeImage": {
                "id": "ef95aeb6-db72-42b7-8887-e31ad6e07c65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f67890-3f7b-4257-a841-34befa411ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50a78caf-6132-4d30-8806-c163a37203fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f67890-3f7b-4257-a841-34befa411ffd",
                    "LayerId": "00c45224-9111-4777-bd5b-6328d6ce38d3"
                }
            ]
        },
        {
            "id": "b95e98ca-5ff9-4e8f-b1bd-2fee3b3258e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "compositeImage": {
                "id": "a9969954-2d02-4992-9b2b-c928230e9c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b95e98ca-5ff9-4e8f-b1bd-2fee3b3258e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4500935b-7561-4ece-b685-fa3ccd835e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b95e98ca-5ff9-4e8f-b1bd-2fee3b3258e3",
                    "LayerId": "00c45224-9111-4777-bd5b-6328d6ce38d3"
                }
            ]
        },
        {
            "id": "20f3dfc8-52bf-46ab-acc5-a82772f30d90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "compositeImage": {
                "id": "cd396b5e-acde-4882-9a4c-6a07702d035d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20f3dfc8-52bf-46ab-acc5-a82772f30d90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01465a9-d21d-415e-98bd-4b452bb0a428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20f3dfc8-52bf-46ab-acc5-a82772f30d90",
                    "LayerId": "00c45224-9111-4777-bd5b-6328d6ce38d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00c45224-9111-4777-bd5b-6328d6ce38d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3648a6ef-2e5c-4425-94d5-8803f571a07a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}