{
    "id": "9113f7ff-b5bb-4f32-8bb6-2570eff41541",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 7,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b30ad239-6db0-4f90-8b75-3d2ce53a793b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9113f7ff-b5bb-4f32-8bb6-2570eff41541",
            "compositeImage": {
                "id": "c0d73fac-a294-4f01-b418-390cdf796d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30ad239-6db0-4f90-8b75-3d2ce53a793b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ff33bc-996c-418b-8d2a-7dc1df64e600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30ad239-6db0-4f90-8b75-3d2ce53a793b",
                    "LayerId": "7b458d10-a57b-4713-809f-3f44b99afa84"
                }
            ]
        },
        {
            "id": "099f5e4b-2598-4ef1-b7b3-866fb9e393ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9113f7ff-b5bb-4f32-8bb6-2570eff41541",
            "compositeImage": {
                "id": "a2f453bc-2a9d-4071-a0d1-9d70d876b65c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099f5e4b-2598-4ef1-b7b3-866fb9e393ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60662308-c762-4622-b78b-5850c52bb8b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099f5e4b-2598-4ef1-b7b3-866fb9e393ce",
                    "LayerId": "7b458d10-a57b-4713-809f-3f44b99afa84"
                }
            ]
        },
        {
            "id": "ab74d78b-9c73-4c08-894b-af8737f96992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9113f7ff-b5bb-4f32-8bb6-2570eff41541",
            "compositeImage": {
                "id": "e7f9f3dd-9a51-4f7f-95f9-fa660d45d687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab74d78b-9c73-4c08-894b-af8737f96992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d46c764-e1ce-427b-a727-0ec50216e0ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab74d78b-9c73-4c08-894b-af8737f96992",
                    "LayerId": "7b458d10-a57b-4713-809f-3f44b99afa84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "7b458d10-a57b-4713-809f-3f44b99afa84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9113f7ff-b5bb-4f32-8bb6-2570eff41541",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}