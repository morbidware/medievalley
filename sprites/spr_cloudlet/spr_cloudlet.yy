{
    "id": "f9198cd6-e8da-4263-a824-2b8bfb909c59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloudlet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef50c6ac-ee9b-40b8-a140-237249a03ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9198cd6-e8da-4263-a824-2b8bfb909c59",
            "compositeImage": {
                "id": "fad4d936-25f5-4e7e-b809-0d0689a4f751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef50c6ac-ee9b-40b8-a140-237249a03ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "887718a2-90b5-4456-968f-bc74106aa1d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef50c6ac-ee9b-40b8-a140-237249a03ae6",
                    "LayerId": "2322c22f-b4aa-4d18-80f3-04d175712d87"
                }
            ]
        },
        {
            "id": "d79e3c61-9d7e-4919-ae50-83bc50c7a681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9198cd6-e8da-4263-a824-2b8bfb909c59",
            "compositeImage": {
                "id": "6e9ed53c-5626-4ecb-b168-df2722f87a62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d79e3c61-9d7e-4919-ae50-83bc50c7a681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4337d548-c4b8-43f3-aade-32e3cd620817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d79e3c61-9d7e-4919-ae50-83bc50c7a681",
                    "LayerId": "2322c22f-b4aa-4d18-80f3-04d175712d87"
                }
            ]
        },
        {
            "id": "fcaf9055-e201-427f-bcc4-2ca8c4d81087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9198cd6-e8da-4263-a824-2b8bfb909c59",
            "compositeImage": {
                "id": "50ec5304-090c-47fa-abbd-bb86f931c11a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcaf9055-e201-427f-bcc4-2ca8c4d81087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fed806dd-d233-47eb-8101-d3459c636f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcaf9055-e201-427f-bcc4-2ca8c4d81087",
                    "LayerId": "2322c22f-b4aa-4d18-80f3-04d175712d87"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 32,
    "layers": [
        {
            "id": "2322c22f-b4aa-4d18-80f3-04d175712d87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9198cd6-e8da-4263-a824-2b8bfb909c59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}