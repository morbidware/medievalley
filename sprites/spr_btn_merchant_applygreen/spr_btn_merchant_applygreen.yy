{
    "id": "e4f63b8b-c47a-48f6-8c2e-19e44265ae78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_merchant_applygreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4de38d62-a361-4626-8334-b3ce8ed6f30e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f63b8b-c47a-48f6-8c2e-19e44265ae78",
            "compositeImage": {
                "id": "dcfc39dd-954e-493f-8743-d11e1e5ced2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de38d62-a361-4626-8334-b3ce8ed6f30e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87d96aa-fe47-440c-a648-c906934247ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de38d62-a361-4626-8334-b3ce8ed6f30e",
                    "LayerId": "f11a33bb-cace-47cb-8c9d-c810e268ef7f"
                }
            ]
        },
        {
            "id": "76f78349-8c56-4693-9cdf-58e45390c484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f63b8b-c47a-48f6-8c2e-19e44265ae78",
            "compositeImage": {
                "id": "19cb4ee8-a38c-4e81-8fa7-50f4c7bad231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76f78349-8c56-4693-9cdf-58e45390c484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1927f5e-2a0b-4de0-b2f8-4fa17c080f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76f78349-8c56-4693-9cdf-58e45390c484",
                    "LayerId": "f11a33bb-cace-47cb-8c9d-c810e268ef7f"
                }
            ]
        },
        {
            "id": "8a0a4c0e-73d7-43ec-b465-dff56bde9ffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4f63b8b-c47a-48f6-8c2e-19e44265ae78",
            "compositeImage": {
                "id": "e57c6e0d-64f2-4b72-a6ad-c339351a03b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0a4c0e-73d7-43ec-b465-dff56bde9ffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f3883f-ea5d-4dd2-8588-c1f4889491f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0a4c0e-73d7-43ec-b465-dff56bde9ffc",
                    "LayerId": "f11a33bb-cace-47cb-8c9d-c810e268ef7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "f11a33bb-cace-47cb-8c9d-c810e268ef7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4f63b8b-c47a-48f6-8c2e-19e44265ae78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 5
}