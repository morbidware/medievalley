{
    "id": "4200ca34-814f-40ee-be56-2ecc4143d419",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_idle_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 41,
    "bbox_right": 41,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09029166-f848-4f1d-a9c4-6782eb310116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4200ca34-814f-40ee-be56-2ecc4143d419",
            "compositeImage": {
                "id": "c9df1ba3-5c47-4a36-99f8-eba603085465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09029166-f848-4f1d-a9c4-6782eb310116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "456f2b7b-b27c-4cb1-a745-8c993512e487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09029166-f848-4f1d-a9c4-6782eb310116",
                    "LayerId": "31ea5026-3ed6-4ed2-baa3-514c2cbcd1bc"
                }
            ]
        },
        {
            "id": "dd129da9-e8b4-45d7-8667-03936c22ac49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4200ca34-814f-40ee-be56-2ecc4143d419",
            "compositeImage": {
                "id": "e1272d8b-d295-4b3d-8e62-7d268390ca20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd129da9-e8b4-45d7-8667-03936c22ac49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de61a7ea-77d5-4a27-9ba5-7c32a3bf5707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd129da9-e8b4-45d7-8667-03936c22ac49",
                    "LayerId": "31ea5026-3ed6-4ed2-baa3-514c2cbcd1bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "31ea5026-3ed6-4ed2-baa3-514c2cbcd1bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4200ca34-814f-40ee-be56-2ecc4143d419",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}