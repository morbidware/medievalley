{
    "id": "4371cbc7-f224-4359-a030-917c18ef2ca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_wheat_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e147249e-97a3-4f96-b718-f3ccbfd93d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
            "compositeImage": {
                "id": "1cd3fead-2388-41ab-b40f-ee8eb62f196b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e147249e-97a3-4f96-b718-f3ccbfd93d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d585bc-99db-4d88-9b15-17d497466e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e147249e-97a3-4f96-b718-f3ccbfd93d03",
                    "LayerId": "13d55af6-6e89-482b-870d-eb0fc2477f03"
                }
            ]
        },
        {
            "id": "db801433-6add-4163-97eb-9fdd39cb1c59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
            "compositeImage": {
                "id": "c228991d-8b9a-4515-bf42-14ab8eae65fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db801433-6add-4163-97eb-9fdd39cb1c59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d203a7e4-fecb-453d-9289-4fb2f5a2c65a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db801433-6add-4163-97eb-9fdd39cb1c59",
                    "LayerId": "13d55af6-6e89-482b-870d-eb0fc2477f03"
                }
            ]
        },
        {
            "id": "65da975f-506a-4d29-a90f-6b33d5ce9499",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
            "compositeImage": {
                "id": "6f8ce12c-1d0d-45f1-8a57-b321819fc134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65da975f-506a-4d29-a90f-6b33d5ce9499",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e4af5a-e5b8-44f0-9c5d-82329ce65e5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65da975f-506a-4d29-a90f-6b33d5ce9499",
                    "LayerId": "13d55af6-6e89-482b-870d-eb0fc2477f03"
                }
            ]
        },
        {
            "id": "cb44cf54-ca1a-4396-8581-d6aa16ef8603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
            "compositeImage": {
                "id": "e85e0312-7252-49c9-878e-64dbbbdbd1eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb44cf54-ca1a-4396-8581-d6aa16ef8603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52357d71-3527-4ffe-b54f-fa87902bd36b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb44cf54-ca1a-4396-8581-d6aa16ef8603",
                    "LayerId": "13d55af6-6e89-482b-870d-eb0fc2477f03"
                }
            ]
        },
        {
            "id": "2331882e-8a5c-40d6-96cd-beaf4fe904ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
            "compositeImage": {
                "id": "0fff6718-a37d-4f97-9d3d-53703d444d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2331882e-8a5c-40d6-96cd-beaf4fe904ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9789b0d-ef0b-4dfc-96d4-a4154882591d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2331882e-8a5c-40d6-96cd-beaf4fe904ce",
                    "LayerId": "13d55af6-6e89-482b-870d-eb0fc2477f03"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "13d55af6-6e89-482b-870d-eb0fc2477f03",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4371cbc7-f224-4359-a030-917c18ef2ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}