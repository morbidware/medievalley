{
    "id": "f43d2901-e856-4944-8fac-68f342712969",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb15a7e9-a213-42a8-b23b-9ccb3b9a94ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f43d2901-e856-4944-8fac-68f342712969",
            "compositeImage": {
                "id": "de273fe7-f2b0-4d59-a4dd-72ff7d40ee05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb15a7e9-a213-42a8-b23b-9ccb3b9a94ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e7d8ff-8f36-4719-ba2c-4ea065129737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb15a7e9-a213-42a8-b23b-9ccb3b9a94ac",
                    "LayerId": "611f6011-481b-41de-baf2-6dc2f3397301"
                }
            ]
        },
        {
            "id": "2196e1e0-068c-4ec3-9591-e439a4c17adc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f43d2901-e856-4944-8fac-68f342712969",
            "compositeImage": {
                "id": "3ef38df4-aaf9-47dd-a71e-70dc2f9d64b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2196e1e0-068c-4ec3-9591-e439a4c17adc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e7eff0e-d6a0-4cc9-95b1-192e64316b22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2196e1e0-068c-4ec3-9591-e439a4c17adc",
                    "LayerId": "611f6011-481b-41de-baf2-6dc2f3397301"
                }
            ]
        },
        {
            "id": "866b5c86-c32f-4caf-85af-0f97a8fae2cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f43d2901-e856-4944-8fac-68f342712969",
            "compositeImage": {
                "id": "72a74555-169d-4061-a8aa-7ce165ab8ae6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866b5c86-c32f-4caf-85af-0f97a8fae2cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee0ea0c-831a-4cc4-857e-ec901b596e47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866b5c86-c32f-4caf-85af-0f97a8fae2cd",
                    "LayerId": "611f6011-481b-41de-baf2-6dc2f3397301"
                }
            ]
        },
        {
            "id": "cb14e936-410a-4cf6-87c4-875cc362b187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f43d2901-e856-4944-8fac-68f342712969",
            "compositeImage": {
                "id": "30d81623-17e0-4347-bb05-8afb7913e44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb14e936-410a-4cf6-87c4-875cc362b187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7544f9c-e283-467b-941a-6b0bb72d1d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb14e936-410a-4cf6-87c4-875cc362b187",
                    "LayerId": "611f6011-481b-41de-baf2-6dc2f3397301"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "611f6011-481b-41de-baf2-6dc2f3397301",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f43d2901-e856-4944-8fac-68f342712969",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}