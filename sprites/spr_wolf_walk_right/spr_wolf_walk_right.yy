{
    "id": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 8,
    "bbox_right": 47,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a1a2a69-2e5b-48e0-be4e-d8fc7b4eadaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "compositeImage": {
                "id": "f25b040a-8286-4dd1-950d-35ef044ebe0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1a2a69-2e5b-48e0-be4e-d8fc7b4eadaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84fbbead-3eb3-465e-9aa4-f2e9f02d7d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1a2a69-2e5b-48e0-be4e-d8fc7b4eadaf",
                    "LayerId": "6282b924-bcc5-4145-ab81-16460103b391"
                }
            ]
        },
        {
            "id": "86f1ae18-0601-4583-8cc6-b5229b6dd25a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "compositeImage": {
                "id": "7f141a5a-7037-4fc0-9649-231c05aabb6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f1ae18-0601-4583-8cc6-b5229b6dd25a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac973bf3-7c0b-4a91-9618-faa83705fa03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f1ae18-0601-4583-8cc6-b5229b6dd25a",
                    "LayerId": "6282b924-bcc5-4145-ab81-16460103b391"
                }
            ]
        },
        {
            "id": "5d272c86-3daf-492f-a031-0fcd004f9aef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "compositeImage": {
                "id": "b2613e0b-56f9-475f-bacc-072aa6cffb54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d272c86-3daf-492f-a031-0fcd004f9aef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13e51672-92e1-4058-98e8-553651641d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d272c86-3daf-492f-a031-0fcd004f9aef",
                    "LayerId": "6282b924-bcc5-4145-ab81-16460103b391"
                }
            ]
        },
        {
            "id": "0bf8c02c-4323-49e9-87c2-54e13af6885d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "compositeImage": {
                "id": "75afb023-a74e-4321-8580-8ce7a807d37c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bf8c02c-4323-49e9-87c2-54e13af6885d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dbbfafc-e0db-488b-a6aa-cc88261f6225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bf8c02c-4323-49e9-87c2-54e13af6885d",
                    "LayerId": "6282b924-bcc5-4145-ab81-16460103b391"
                }
            ]
        },
        {
            "id": "1200ac65-ebd8-4162-ab73-cf3ed1d72f7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "compositeImage": {
                "id": "2ef9ff17-b248-4277-9e72-d51fa5d87e7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1200ac65-ebd8-4162-ab73-cf3ed1d72f7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "356c7fcb-b649-40cc-a77b-846d2c19fb83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1200ac65-ebd8-4162-ab73-cf3ed1d72f7b",
                    "LayerId": "6282b924-bcc5-4145-ab81-16460103b391"
                }
            ]
        },
        {
            "id": "15b6302b-add7-4979-8e63-19ae70089a11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "compositeImage": {
                "id": "fcf05686-0677-46fe-8efe-6fb6a80435da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15b6302b-add7-4979-8e63-19ae70089a11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7059c8a-c663-44fa-b0e2-eb334c799858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15b6302b-add7-4979-8e63-19ae70089a11",
                    "LayerId": "6282b924-bcc5-4145-ab81-16460103b391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "6282b924-bcc5-4145-ab81-16460103b391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "415c0ac4-5d4d-4ecb-8e9a-38f8b5433a6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}