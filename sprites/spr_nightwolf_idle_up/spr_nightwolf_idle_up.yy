{
    "id": "507f5125-17c8-485c-9613-3376942c0d6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fed1fcb-6528-4bab-8e3a-f337d77e903e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "507f5125-17c8-485c-9613-3376942c0d6c",
            "compositeImage": {
                "id": "0d537416-24c6-4e9c-aabf-28c8d4058c16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fed1fcb-6528-4bab-8e3a-f337d77e903e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78790d5e-b075-498a-a14c-45cd0b486e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fed1fcb-6528-4bab-8e3a-f337d77e903e",
                    "LayerId": "dbc2d4a3-e776-4a2b-8acc-ff23f213aa77"
                }
            ]
        },
        {
            "id": "889753a8-2d41-41b7-b71f-0e257fb9d8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "507f5125-17c8-485c-9613-3376942c0d6c",
            "compositeImage": {
                "id": "3c325990-c0f5-4b73-8e6d-3cfaf69433ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889753a8-2d41-41b7-b71f-0e257fb9d8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1dc8913-e895-459c-bad8-ef39710d3eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889753a8-2d41-41b7-b71f-0e257fb9d8e9",
                    "LayerId": "dbc2d4a3-e776-4a2b-8acc-ff23f213aa77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "dbc2d4a3-e776-4a2b-8acc-ff23f213aa77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "507f5125-17c8-485c-9613-3376942c0d6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}