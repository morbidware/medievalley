{
    "id": "9806e477-9f89-40c3-a4ae-54a424c401b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_kitchen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "471959b1-cbc2-4f27-84b9-cc9fdea08038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9806e477-9f89-40c3-a4ae-54a424c401b0",
            "compositeImage": {
                "id": "eb151da6-b36b-45d3-a93d-9683b9943780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471959b1-cbc2-4f27-84b9-cc9fdea08038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188a049e-3374-4ba4-97e4-001ee8212707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471959b1-cbc2-4f27-84b9-cc9fdea08038",
                    "LayerId": "6c690152-d707-40da-a541-b9abacc4d862"
                }
            ]
        }
    ],
    "gridX": 40,
    "gridY": 72,
    "height": 80,
    "layers": [
        {
            "id": "6c690152-d707-40da-a541-b9abacc4d862",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9806e477-9f89-40c3-a4ae-54a424c401b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 72
}