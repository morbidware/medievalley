{
    "id": "ee9b1c28-1e48-4025-b26c-7521dc76d476",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cda6773-aaaf-4626-8a08-d1e2eacb4dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9b1c28-1e48-4025-b26c-7521dc76d476",
            "compositeImage": {
                "id": "01396757-8d64-46de-b993-631cc9182fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cda6773-aaaf-4626-8a08-d1e2eacb4dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebdd7455-e5b9-46a1-a6f5-312a4259f136",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cda6773-aaaf-4626-8a08-d1e2eacb4dfd",
                    "LayerId": "cfc3ee5d-8cfa-4c05-943f-43e1a7d4df1e"
                }
            ]
        },
        {
            "id": "da7bfe25-b5c9-42b2-9407-a1e016b64bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9b1c28-1e48-4025-b26c-7521dc76d476",
            "compositeImage": {
                "id": "129049bf-0abc-45f8-bda4-50335b0d5598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da7bfe25-b5c9-42b2-9407-a1e016b64bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc08b99a-cec2-48f6-95e5-86d8b4eaeae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da7bfe25-b5c9-42b2-9407-a1e016b64bf2",
                    "LayerId": "cfc3ee5d-8cfa-4c05-943f-43e1a7d4df1e"
                }
            ]
        },
        {
            "id": "7151620d-417b-4961-81df-9826ba222285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9b1c28-1e48-4025-b26c-7521dc76d476",
            "compositeImage": {
                "id": "cafbcdab-fb51-4776-9e1d-0b6b356a69b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7151620d-417b-4961-81df-9826ba222285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22883523-d4d1-4d1b-9902-b6a7c72871fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7151620d-417b-4961-81df-9826ba222285",
                    "LayerId": "cfc3ee5d-8cfa-4c05-943f-43e1a7d4df1e"
                }
            ]
        },
        {
            "id": "cab66ac7-dcfb-4f4c-89a2-d857222aac35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee9b1c28-1e48-4025-b26c-7521dc76d476",
            "compositeImage": {
                "id": "2401927b-b4a2-452d-8d0b-6d2a375b7bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cab66ac7-dcfb-4f4c-89a2-d857222aac35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3684aba-d446-45ba-a36c-e3ca5ed655ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cab66ac7-dcfb-4f4c-89a2-d857222aac35",
                    "LayerId": "cfc3ee5d-8cfa-4c05-943f-43e1a7d4df1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cfc3ee5d-8cfa-4c05-943f-43e1a7d4df1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee9b1c28-1e48-4025-b26c-7521dc76d476",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}