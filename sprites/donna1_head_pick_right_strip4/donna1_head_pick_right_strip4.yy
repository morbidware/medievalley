{
    "id": "03dec3e9-b2e7-4aaf-a7f3-73c29e91892e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3c1507f-4759-4492-b3cc-78dec9054925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dec3e9-b2e7-4aaf-a7f3-73c29e91892e",
            "compositeImage": {
                "id": "c1c85aa7-c857-4ed8-bf74-6652e654ef47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3c1507f-4759-4492-b3cc-78dec9054925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8fef9b0-f0a2-4c32-a603-8f612b63e531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3c1507f-4759-4492-b3cc-78dec9054925",
                    "LayerId": "efa2af96-4c5d-4343-9f16-b0ec39ac5654"
                }
            ]
        },
        {
            "id": "7eb4b7c2-7b12-4fc9-b749-9ef403410da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dec3e9-b2e7-4aaf-a7f3-73c29e91892e",
            "compositeImage": {
                "id": "83abdda9-c622-42b1-937e-22cdb37c9657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eb4b7c2-7b12-4fc9-b749-9ef403410da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09e6da5f-55ca-4313-b3f7-116efcc953fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb4b7c2-7b12-4fc9-b749-9ef403410da5",
                    "LayerId": "efa2af96-4c5d-4343-9f16-b0ec39ac5654"
                }
            ]
        },
        {
            "id": "c12b4462-2513-405e-98c9-1b0c0e1a30d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dec3e9-b2e7-4aaf-a7f3-73c29e91892e",
            "compositeImage": {
                "id": "e7e14d50-efc7-4081-93ca-f58b4b576571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12b4462-2513-405e-98c9-1b0c0e1a30d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd45ab4-0232-4928-b1ce-f334d3be74bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12b4462-2513-405e-98c9-1b0c0e1a30d0",
                    "LayerId": "efa2af96-4c5d-4343-9f16-b0ec39ac5654"
                }
            ]
        },
        {
            "id": "144f9961-8632-4226-be07-0223baefba18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03dec3e9-b2e7-4aaf-a7f3-73c29e91892e",
            "compositeImage": {
                "id": "8ab53dc1-762a-4cda-889c-6a09e9b9744e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "144f9961-8632-4226-be07-0223baefba18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79bea7a5-4fab-4963-9bd9-c46d4ccc75ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "144f9961-8632-4226-be07-0223baefba18",
                    "LayerId": "efa2af96-4c5d-4343-9f16-b0ec39ac5654"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "efa2af96-4c5d-4343-9f16-b0ec39ac5654",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03dec3e9-b2e7-4aaf-a7f3-73c29e91892e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}