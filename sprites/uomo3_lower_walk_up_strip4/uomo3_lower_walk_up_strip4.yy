{
    "id": "dfb0e087-4670-4db5-8083-17b54dfbf3d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63de9196-0157-4b41-aabc-b6d5e0a2a2a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb0e087-4670-4db5-8083-17b54dfbf3d2",
            "compositeImage": {
                "id": "67752a78-6cd5-434e-a526-149f475596f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63de9196-0157-4b41-aabc-b6d5e0a2a2a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b2a593-a2aa-4646-bbb3-b6c65ab5f604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63de9196-0157-4b41-aabc-b6d5e0a2a2a7",
                    "LayerId": "b160813a-3a67-478f-b51f-980a331291d6"
                }
            ]
        },
        {
            "id": "17a45cba-3509-4762-b09c-9b07b88a13b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb0e087-4670-4db5-8083-17b54dfbf3d2",
            "compositeImage": {
                "id": "7e231298-b782-47bc-876b-376378c6ce1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a45cba-3509-4762-b09c-9b07b88a13b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ac9bf0-3f8b-4c01-898a-645d4c978d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a45cba-3509-4762-b09c-9b07b88a13b9",
                    "LayerId": "b160813a-3a67-478f-b51f-980a331291d6"
                }
            ]
        },
        {
            "id": "ac9b2eeb-c3f5-4651-8dbc-3b5cfb26bc7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb0e087-4670-4db5-8083-17b54dfbf3d2",
            "compositeImage": {
                "id": "7aad790e-0ad1-40ea-a34b-2a1b5b5a8b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9b2eeb-c3f5-4651-8dbc-3b5cfb26bc7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1b1da2-a49b-4bd7-8ff0-cee50d4715ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9b2eeb-c3f5-4651-8dbc-3b5cfb26bc7f",
                    "LayerId": "b160813a-3a67-478f-b51f-980a331291d6"
                }
            ]
        },
        {
            "id": "a2e2e2fd-726f-4361-8f9f-c43bc902d9b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb0e087-4670-4db5-8083-17b54dfbf3d2",
            "compositeImage": {
                "id": "50e4f210-58e2-4486-b797-f4f2a301666d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e2e2fd-726f-4361-8f9f-c43bc902d9b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5efbe56f-dfcf-4ee9-878a-65a953f9f250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e2e2fd-726f-4361-8f9f-c43bc902d9b6",
                    "LayerId": "b160813a-3a67-478f-b51f-980a331291d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b160813a-3a67-478f-b51f-980a331291d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfb0e087-4670-4db5-8083-17b54dfbf3d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}