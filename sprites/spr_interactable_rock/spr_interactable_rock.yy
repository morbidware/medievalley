{
    "id": "ea658d2c-1ac4-4bea-a3ff-3c48e3a7141c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e18fc9b-2d15-4080-95f1-7a771b23c9c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea658d2c-1ac4-4bea-a3ff-3c48e3a7141c",
            "compositeImage": {
                "id": "f3b4ead7-2fdf-4c5d-951a-80f9576baa7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e18fc9b-2d15-4080-95f1-7a771b23c9c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "789b8337-ad70-4d67-b615-c24c1ecf5c72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e18fc9b-2d15-4080-95f1-7a771b23c9c5",
                    "LayerId": "07560db9-cfef-438a-9dcf-1ae9b0212261"
                }
            ]
        },
        {
            "id": "036cc314-246e-4db1-9dce-74e94e222957",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea658d2c-1ac4-4bea-a3ff-3c48e3a7141c",
            "compositeImage": {
                "id": "dbf73e94-4de7-414b-87eb-a89a13dc81a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "036cc314-246e-4db1-9dce-74e94e222957",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59faaf08-3f98-47a3-87e7-f239dd91dff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "036cc314-246e-4db1-9dce-74e94e222957",
                    "LayerId": "07560db9-cfef-438a-9dcf-1ae9b0212261"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "07560db9-cfef-438a-9dcf-1ae9b0212261",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea658d2c-1ac4-4bea-a3ff-3c48e3a7141c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}