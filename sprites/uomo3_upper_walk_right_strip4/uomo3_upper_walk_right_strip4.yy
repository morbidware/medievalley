{
    "id": "2bb087b5-24b4-40c1-8586-5e369d76dbd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "656beaf0-8455-4dcf-9862-f0410f68f959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bb087b5-24b4-40c1-8586-5e369d76dbd4",
            "compositeImage": {
                "id": "1ba3fa79-8379-4882-bd8c-1b448f3fc0ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656beaf0-8455-4dcf-9862-f0410f68f959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6cd73ac-1cce-4064-aed5-0c3d84c08804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656beaf0-8455-4dcf-9862-f0410f68f959",
                    "LayerId": "8c266606-53ce-4299-8a7f-2752f64d0a23"
                }
            ]
        },
        {
            "id": "184aa845-127a-418c-a40f-175dc0cbb94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bb087b5-24b4-40c1-8586-5e369d76dbd4",
            "compositeImage": {
                "id": "05b08c81-3e53-42e6-99eb-baf8a0d50fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "184aa845-127a-418c-a40f-175dc0cbb94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d015ef25-f758-4c04-bcdf-0e3f4396f5dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "184aa845-127a-418c-a40f-175dc0cbb94d",
                    "LayerId": "8c266606-53ce-4299-8a7f-2752f64d0a23"
                }
            ]
        },
        {
            "id": "575f3471-f4f3-40f9-848b-af484ce815c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bb087b5-24b4-40c1-8586-5e369d76dbd4",
            "compositeImage": {
                "id": "597d6b71-9bc3-43ec-b961-ed18d9431676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "575f3471-f4f3-40f9-848b-af484ce815c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d7a607-aebf-4f56-83f5-7c45bd29eb47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "575f3471-f4f3-40f9-848b-af484ce815c1",
                    "LayerId": "8c266606-53ce-4299-8a7f-2752f64d0a23"
                }
            ]
        },
        {
            "id": "8cc875be-dbfd-4a7e-8a84-8071050f4bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bb087b5-24b4-40c1-8586-5e369d76dbd4",
            "compositeImage": {
                "id": "1e72766d-e746-4581-a48f-ceb6d5814d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc875be-dbfd-4a7e-8a84-8071050f4bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e766c5-1ab6-44aa-a43a-0740c674e293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc875be-dbfd-4a7e-8a84-8071050f4bb3",
                    "LayerId": "8c266606-53ce-4299-8a7f-2752f64d0a23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8c266606-53ce-4299-8a7f-2752f64d0a23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bb087b5-24b4-40c1-8586-5e369d76dbd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}