{
    "id": "efdb6a3c-69c9-4583-83fd-a5be6f84db55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d712deb-c101-4e2f-a4fa-d5c596cb3368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efdb6a3c-69c9-4583-83fd-a5be6f84db55",
            "compositeImage": {
                "id": "2529c4d7-6174-40aa-963d-13679fb25b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d712deb-c101-4e2f-a4fa-d5c596cb3368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67256ce6-b86f-407a-86cb-cdac1b7fdf7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d712deb-c101-4e2f-a4fa-d5c596cb3368",
                    "LayerId": "bec6e035-959a-487b-92f2-a1f58ec277a3"
                }
            ]
        },
        {
            "id": "08a1f5a4-8955-45bc-86b4-60d37dc92ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efdb6a3c-69c9-4583-83fd-a5be6f84db55",
            "compositeImage": {
                "id": "00241ca3-41ef-4bf6-8224-8410d28c43ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08a1f5a4-8955-45bc-86b4-60d37dc92ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "212b316f-5300-4334-aa85-02c940866671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08a1f5a4-8955-45bc-86b4-60d37dc92ed4",
                    "LayerId": "bec6e035-959a-487b-92f2-a1f58ec277a3"
                }
            ]
        },
        {
            "id": "c205ee2e-01c0-48a6-a323-435b842c6601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efdb6a3c-69c9-4583-83fd-a5be6f84db55",
            "compositeImage": {
                "id": "ae8bb430-cefd-4892-b530-3e5b79c5e1d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c205ee2e-01c0-48a6-a323-435b842c6601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21275017-3ac5-4c6a-9b72-52a2ab6738f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c205ee2e-01c0-48a6-a323-435b842c6601",
                    "LayerId": "bec6e035-959a-487b-92f2-a1f58ec277a3"
                }
            ]
        },
        {
            "id": "e8b54b9b-febc-485c-addb-7563dd75f129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efdb6a3c-69c9-4583-83fd-a5be6f84db55",
            "compositeImage": {
                "id": "8b324a9c-8244-435d-9b10-06edec999ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8b54b9b-febc-485c-addb-7563dd75f129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44e15707-fa44-46f5-9fd9-649d07a061a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8b54b9b-febc-485c-addb-7563dd75f129",
                    "LayerId": "bec6e035-959a-487b-92f2-a1f58ec277a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bec6e035-959a-487b-92f2-a1f58ec277a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efdb6a3c-69c9-4583-83fd-a5be6f84db55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}