{
    "id": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f64eabde-bbde-4b72-8154-ab552e9d21fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "compositeImage": {
                "id": "b69124b6-fac3-4d65-ae38-8860c2951d37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64eabde-bbde-4b72-8154-ab552e9d21fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bca6ab6-cae0-4e94-ad50-829376ae8cc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64eabde-bbde-4b72-8154-ab552e9d21fc",
                    "LayerId": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643"
                }
            ]
        },
        {
            "id": "a5076cdb-6623-4ec0-b58c-e998c1215746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "compositeImage": {
                "id": "87e10b31-9642-4031-b1d6-15caeee04492",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5076cdb-6623-4ec0-b58c-e998c1215746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b51bbd-f1e1-4105-9747-d4b817bd74de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5076cdb-6623-4ec0-b58c-e998c1215746",
                    "LayerId": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643"
                }
            ]
        },
        {
            "id": "ead9c28c-5665-441a-ab31-fa1a2c086a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "compositeImage": {
                "id": "c1388c79-5b0c-45ad-b369-04b9fb4618bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ead9c28c-5665-441a-ab31-fa1a2c086a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43c9822-7f9d-473f-ad44-c4e009c92172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ead9c28c-5665-441a-ab31-fa1a2c086a48",
                    "LayerId": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643"
                }
            ]
        },
        {
            "id": "86ceccb9-cacf-4a26-bde4-18ab4382d61e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "compositeImage": {
                "id": "d9157f1e-4787-4896-a23f-b409ad6cd7c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86ceccb9-cacf-4a26-bde4-18ab4382d61e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2b0ff6-90b7-4b5b-bedd-f21929f03229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86ceccb9-cacf-4a26-bde4-18ab4382d61e",
                    "LayerId": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643"
                }
            ]
        },
        {
            "id": "9fde02a3-daff-4987-99be-5ee41f3b5032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "compositeImage": {
                "id": "b5510649-1bf5-4414-b5a8-619fef5e110a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fde02a3-daff-4987-99be-5ee41f3b5032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175c2bd4-25d9-47ac-97b4-5eeaa6b97e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fde02a3-daff-4987-99be-5ee41f3b5032",
                    "LayerId": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643"
                }
            ]
        },
        {
            "id": "35784cc1-d210-4084-aa47-abeda5ba94dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "compositeImage": {
                "id": "8961c153-f80e-4e89-8961-bae846e7efac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35784cc1-d210-4084-aa47-abeda5ba94dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2443098-ce31-4876-9989-db4369d04165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35784cc1-d210-4084-aa47-abeda5ba94dd",
                    "LayerId": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "eeadf9ff-12d3-47b3-91d3-d2c8a7ef9643",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c638815e-e334-45b1-8822-0e2c1a3ae8aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}