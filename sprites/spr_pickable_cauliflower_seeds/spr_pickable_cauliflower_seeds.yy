{
    "id": "83ca95a6-1729-44b3-b75d-32f7fb5eed40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_cauliflower_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7725efe8-408f-4ce2-8cde-82018e34fcd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83ca95a6-1729-44b3-b75d-32f7fb5eed40",
            "compositeImage": {
                "id": "784b2378-f0fd-43e2-bc9c-f04b083b0039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7725efe8-408f-4ce2-8cde-82018e34fcd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "978c0249-a0f6-468b-b866-a9ea1f3799ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7725efe8-408f-4ce2-8cde-82018e34fcd0",
                    "LayerId": "d6d8f4f6-69db-4fc5-b8f0-f36836dd5240"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d6d8f4f6-69db-4fc5-b8f0-f36836dd5240",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83ca95a6-1729-44b3-b75d-32f7fb5eed40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}