{
    "id": "de5098cc-31ed-48af-b0ea-2dc4963573c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e2855d2-b126-464a-8be0-9c44cdae8913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de5098cc-31ed-48af-b0ea-2dc4963573c6",
            "compositeImage": {
                "id": "abed57dc-9607-48fe-81e5-7169e42c3179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2855d2-b126-464a-8be0-9c44cdae8913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7f43290-aaee-46d9-b425-03a1a1693c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2855d2-b126-464a-8be0-9c44cdae8913",
                    "LayerId": "4901d828-3302-4384-ab1b-4eb625d0397b"
                }
            ]
        },
        {
            "id": "2d9507fd-f854-48ec-ac80-dcf981db0407",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de5098cc-31ed-48af-b0ea-2dc4963573c6",
            "compositeImage": {
                "id": "0594ab90-5e28-494b-9ad7-8e3892962fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d9507fd-f854-48ec-ac80-dcf981db0407",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223c823c-df1f-44f2-9262-8cbfb3144f4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d9507fd-f854-48ec-ac80-dcf981db0407",
                    "LayerId": "4901d828-3302-4384-ab1b-4eb625d0397b"
                }
            ]
        },
        {
            "id": "cf305ec6-f708-4eaf-97c8-92a2a1011052",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de5098cc-31ed-48af-b0ea-2dc4963573c6",
            "compositeImage": {
                "id": "e8a96dfe-fe61-4bb7-b07e-616e76a2ab4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf305ec6-f708-4eaf-97c8-92a2a1011052",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e78db27-8495-4438-a5c3-52c54944ef94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf305ec6-f708-4eaf-97c8-92a2a1011052",
                    "LayerId": "4901d828-3302-4384-ab1b-4eb625d0397b"
                }
            ]
        },
        {
            "id": "5adc594d-5fde-4f3f-98ad-d864fccc9c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de5098cc-31ed-48af-b0ea-2dc4963573c6",
            "compositeImage": {
                "id": "04eaf0eb-a26f-4da6-852b-e82da6b04fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5adc594d-5fde-4f3f-98ad-d864fccc9c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f176df-bf61-4607-9051-c487d052292f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5adc594d-5fde-4f3f-98ad-d864fccc9c17",
                    "LayerId": "4901d828-3302-4384-ab1b-4eb625d0397b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4901d828-3302-4384-ab1b-4eb625d0397b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de5098cc-31ed-48af-b0ea-2dc4963573c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}