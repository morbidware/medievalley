{
    "id": "247cf4bb-0fe7-445b-81c8-dc3f5ecb41dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94c551fd-3160-4b16-8f9f-8c3552e44566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "247cf4bb-0fe7-445b-81c8-dc3f5ecb41dd",
            "compositeImage": {
                "id": "2c3e000c-8b64-412f-a526-28f4f3311afd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c551fd-3160-4b16-8f9f-8c3552e44566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c2f39fc-3487-448a-ba6e-efb70434d2d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c551fd-3160-4b16-8f9f-8c3552e44566",
                    "LayerId": "36e032b8-f168-427d-abf0-58e426e4dd15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "36e032b8-f168-427d-abf0-58e426e4dd15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "247cf4bb-0fe7-445b-81c8-dc3f5ecb41dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}