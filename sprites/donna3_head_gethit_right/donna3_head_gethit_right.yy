{
    "id": "b9586be4-2c4e-4917-8871-18bf00d609f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d13a56f8-ffd8-47ba-bbd7-0d0754106f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9586be4-2c4e-4917-8871-18bf00d609f7",
            "compositeImage": {
                "id": "0dc7660e-467a-4de4-94cf-4db149399b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d13a56f8-ffd8-47ba-bbd7-0d0754106f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff70848-8d5d-4a48-9255-7e0e709f7e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d13a56f8-ffd8-47ba-bbd7-0d0754106f7a",
                    "LayerId": "94a3ab82-6173-4bd3-8861-3e6fcbb79786"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "94a3ab82-6173-4bd3-8861-3e6fcbb79786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9586be4-2c4e-4917-8871-18bf00d609f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}