{
    "id": "75009b0f-8909-400e-aa83-0026e800e2ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "544a3405-6cd4-4ac9-8b07-6e42d5b25a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75009b0f-8909-400e-aa83-0026e800e2ea",
            "compositeImage": {
                "id": "9e4ed6d9-75ed-4b83-a1de-5ce9ace09e13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "544a3405-6cd4-4ac9-8b07-6e42d5b25a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b85c02-09b7-4383-9d9f-56dcd346a179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "544a3405-6cd4-4ac9-8b07-6e42d5b25a96",
                    "LayerId": "079e153e-73bf-4b8e-b545-8f1353b5d074"
                }
            ]
        },
        {
            "id": "f75aa91d-9c0d-4061-a77c-9b247cd804fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75009b0f-8909-400e-aa83-0026e800e2ea",
            "compositeImage": {
                "id": "00a04ffb-f926-4167-8ded-8acb49b0a173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f75aa91d-9c0d-4061-a77c-9b247cd804fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db3c16a7-116c-4bd3-bc4d-e6a6ddba33ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f75aa91d-9c0d-4061-a77c-9b247cd804fc",
                    "LayerId": "079e153e-73bf-4b8e-b545-8f1353b5d074"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "079e153e-73bf-4b8e-b545-8f1353b5d074",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75009b0f-8909-400e-aa83-0026e800e2ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}