{
    "id": "3a6a5244-9455-44e5-bf9a-47215941b002",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_die_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aba3c9fc-84f3-4361-b0ff-ce10300a3f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a6a5244-9455-44e5-bf9a-47215941b002",
            "compositeImage": {
                "id": "e1c73d5b-5842-49c9-9a23-16be6ccd431f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba3c9fc-84f3-4361-b0ff-ce10300a3f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b95b01f-39c0-4684-841f-8147592d73ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba3c9fc-84f3-4361-b0ff-ce10300a3f56",
                    "LayerId": "fec73bd5-ffce-42ce-9b95-2e8893bd42d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "fec73bd5-ffce-42ce-9b95-2e8893bd42d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a6a5244-9455-44e5-bf9a-47215941b002",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}