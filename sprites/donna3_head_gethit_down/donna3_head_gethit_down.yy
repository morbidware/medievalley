{
    "id": "9bcdf489-10c7-4ed0-af40-9168debafd7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "554aa467-07d7-4ddc-82b4-08b13a700816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bcdf489-10c7-4ed0-af40-9168debafd7f",
            "compositeImage": {
                "id": "3bdab0be-2d9c-4def-a925-438a7adca713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554aa467-07d7-4ddc-82b4-08b13a700816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d777f2-3a11-4f2d-81b2-81dbc4f706c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554aa467-07d7-4ddc-82b4-08b13a700816",
                    "LayerId": "d01a1848-15bd-4623-973e-ceb0e5847b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d01a1848-15bd-4623-973e-ceb0e5847b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bcdf489-10c7-4ed0-af40-9168debafd7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}