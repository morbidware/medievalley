{
    "id": "e1cead38-c29b-46fb-8932-2f60dd859812",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ba1a1e9-9a22-4c4f-9b89-c89ca4bc951c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1cead38-c29b-46fb-8932-2f60dd859812",
            "compositeImage": {
                "id": "d7f80a3d-ab7f-42e9-9b09-3e0b73449fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba1a1e9-9a22-4c4f-9b89-c89ca4bc951c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a063c5-248b-4bc2-a521-7032402c4e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba1a1e9-9a22-4c4f-9b89-c89ca4bc951c",
                    "LayerId": "cb375e14-706c-4b2e-8c21-cd7e9402e95d"
                }
            ]
        },
        {
            "id": "77e610ee-8dd7-481e-a45a-792b12e14b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1cead38-c29b-46fb-8932-2f60dd859812",
            "compositeImage": {
                "id": "fa0b4a56-174b-42b5-a7f1-168f8727f271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e610ee-8dd7-481e-a45a-792b12e14b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e43fb0a-fdbe-4590-991f-df587280b041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e610ee-8dd7-481e-a45a-792b12e14b6e",
                    "LayerId": "cb375e14-706c-4b2e-8c21-cd7e9402e95d"
                }
            ]
        },
        {
            "id": "b4f459e3-6558-4900-9b55-19cfe87502e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1cead38-c29b-46fb-8932-2f60dd859812",
            "compositeImage": {
                "id": "1fa684a7-0bf5-4e12-8b24-f94b5aa571a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4f459e3-6558-4900-9b55-19cfe87502e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f95988-2e89-4b55-b643-abd75f07248f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4f459e3-6558-4900-9b55-19cfe87502e1",
                    "LayerId": "cb375e14-706c-4b2e-8c21-cd7e9402e95d"
                }
            ]
        },
        {
            "id": "ae51372d-97d6-42f4-82da-2b93f1b11faf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1cead38-c29b-46fb-8932-2f60dd859812",
            "compositeImage": {
                "id": "a9e060de-bbb7-431e-bd1a-45d449f362df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae51372d-97d6-42f4-82da-2b93f1b11faf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d66fb59-a784-4e27-b6f2-9ecf7cc66bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae51372d-97d6-42f4-82da-2b93f1b11faf",
                    "LayerId": "cb375e14-706c-4b2e-8c21-cd7e9402e95d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cb375e14-706c-4b2e-8c21-cd7e9402e95d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1cead38-c29b-46fb-8932-2f60dd859812",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}