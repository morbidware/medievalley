{
    "id": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_walk_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "515e0597-aef7-4771-9574-b493344e1bd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "compositeImage": {
                "id": "ca5bf1dd-371c-46f4-8881-0f9b460b5ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515e0597-aef7-4771-9574-b493344e1bd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1758b19-5ee0-4b53-95bd-1e833c484d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515e0597-aef7-4771-9574-b493344e1bd9",
                    "LayerId": "a8141dc1-ddac-4df6-904d-47595ae09eb9"
                }
            ]
        },
        {
            "id": "d58f5a95-6284-4d88-b673-1e4cd332b2fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "compositeImage": {
                "id": "ad2eec74-b6d8-4a24-a0a9-029baa120db8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58f5a95-6284-4d88-b673-1e4cd332b2fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2c2b99-e411-4445-82e9-b7500d6d11f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58f5a95-6284-4d88-b673-1e4cd332b2fb",
                    "LayerId": "a8141dc1-ddac-4df6-904d-47595ae09eb9"
                }
            ]
        },
        {
            "id": "cf41cbba-4a35-409c-9f3b-724d76e80943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "compositeImage": {
                "id": "b51a5c05-f353-4fd0-9979-fd96412be2c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf41cbba-4a35-409c-9f3b-724d76e80943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a20abc-d12e-4429-906a-0b9e95141c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf41cbba-4a35-409c-9f3b-724d76e80943",
                    "LayerId": "a8141dc1-ddac-4df6-904d-47595ae09eb9"
                }
            ]
        },
        {
            "id": "0814b6d4-964b-43ff-8f68-92a0a0f40474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "compositeImage": {
                "id": "a24b4e92-9b11-4992-b21b-6f7c6eae9003",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0814b6d4-964b-43ff-8f68-92a0a0f40474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45701975-8766-4658-bd43-1078a8447ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0814b6d4-964b-43ff-8f68-92a0a0f40474",
                    "LayerId": "a8141dc1-ddac-4df6-904d-47595ae09eb9"
                }
            ]
        },
        {
            "id": "c6080fdd-e09c-4b88-afef-a9f1cf3e2f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "compositeImage": {
                "id": "6ce6ea9d-a089-4c54-9501-aac96531b4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6080fdd-e09c-4b88-afef-a9f1cf3e2f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4615c4bc-d7a6-406a-8ab9-c90ea3c8b351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6080fdd-e09c-4b88-afef-a9f1cf3e2f27",
                    "LayerId": "a8141dc1-ddac-4df6-904d-47595ae09eb9"
                }
            ]
        },
        {
            "id": "b10dd66f-3056-44f7-bf39-c80eb0dd4ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "compositeImage": {
                "id": "1f4fcba9-b6f5-4f71-947e-bb596044c1df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10dd66f-3056-44f7-bf39-c80eb0dd4ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7952b694-72ca-4f20-8582-0a660e5bc471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10dd66f-3056-44f7-bf39-c80eb0dd4ac3",
                    "LayerId": "a8141dc1-ddac-4df6-904d-47595ae09eb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a8141dc1-ddac-4df6-904d-47595ae09eb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "186fec44-c78e-42ad-8a5c-59434ad7a99c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}