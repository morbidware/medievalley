{
    "id": "9108f0ad-6283-4e27-9bb7-76235994e688",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_pumpkin_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "820028f1-2f6f-46c1-8eba-6e4d60f29b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9108f0ad-6283-4e27-9bb7-76235994e688",
            "compositeImage": {
                "id": "61b238fb-106f-4cbd-81b2-5fb10e59d151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "820028f1-2f6f-46c1-8eba-6e4d60f29b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496be5d4-3683-4747-8c1b-009593bde3bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "820028f1-2f6f-46c1-8eba-6e4d60f29b57",
                    "LayerId": "a32be560-0096-4e49-a6f9-732b05624ecb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a32be560-0096-4e49-a6f9-732b05624ecb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9108f0ad-6283-4e27-9bb7-76235994e688",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}