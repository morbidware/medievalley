{
    "id": "2843f820-c23a-4004-a1f6-b4d47eaf3e07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ecd3d8e-4911-4231-926d-6f93765de25f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2843f820-c23a-4004-a1f6-b4d47eaf3e07",
            "compositeImage": {
                "id": "6fa54f4e-22bc-4320-8b3d-e5051b5714a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ecd3d8e-4911-4231-926d-6f93765de25f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c880a73a-acab-4d42-863d-fb9e5dada645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ecd3d8e-4911-4231-926d-6f93765de25f",
                    "LayerId": "c1481030-9679-4294-b6d1-2e5ffaa6e210"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1481030-9679-4294-b6d1-2e5ffaa6e210",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2843f820-c23a-4004-a1f6-b4d47eaf3e07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}