{
    "id": "20abe31d-3588-48b1-b3a6-626a06ab8326",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_strawberry_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09141825-10d8-4531-8702-5d33a2084dc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
            "compositeImage": {
                "id": "47fa7c91-ec2b-4b8f-ae6e-c5c81e53e4ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09141825-10d8-4531-8702-5d33a2084dc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97689332-0fe1-4698-84ae-c5eb6ab231f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09141825-10d8-4531-8702-5d33a2084dc1",
                    "LayerId": "de9d882e-2e03-4a97-883d-1421d05fd1af"
                }
            ]
        },
        {
            "id": "0889576d-8c97-4cb5-a5f2-21d288567066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
            "compositeImage": {
                "id": "a760a959-0da0-4328-a085-fbe6bfd20d10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0889576d-8c97-4cb5-a5f2-21d288567066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16d99d64-929d-49de-888c-e9c070402961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0889576d-8c97-4cb5-a5f2-21d288567066",
                    "LayerId": "de9d882e-2e03-4a97-883d-1421d05fd1af"
                }
            ]
        },
        {
            "id": "ddaf1d6c-833b-4f24-ad4d-d100af556c4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
            "compositeImage": {
                "id": "93a510d1-19e0-4ed0-995d-0dcd91a6dbe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddaf1d6c-833b-4f24-ad4d-d100af556c4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6eb34c18-02f8-48a1-bf98-c159c6e9de4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddaf1d6c-833b-4f24-ad4d-d100af556c4a",
                    "LayerId": "de9d882e-2e03-4a97-883d-1421d05fd1af"
                }
            ]
        },
        {
            "id": "c23a928f-879c-4f56-a17c-e31095ca4759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
            "compositeImage": {
                "id": "f6c3b1e6-bd4c-4792-a72c-be26c26a0a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c23a928f-879c-4f56-a17c-e31095ca4759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8092012-1db5-4f58-aa8c-d362a7fc8dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c23a928f-879c-4f56-a17c-e31095ca4759",
                    "LayerId": "de9d882e-2e03-4a97-883d-1421d05fd1af"
                }
            ]
        },
        {
            "id": "c8b7050c-cc4b-4c10-a038-c99baf7ac79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
            "compositeImage": {
                "id": "f74f757a-026d-492b-86fd-c18264f3fbbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b7050c-cc4b-4c10-a038-c99baf7ac79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a215f049-7bc0-4794-9698-8719d1a1f455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b7050c-cc4b-4c10-a038-c99baf7ac79b",
                    "LayerId": "de9d882e-2e03-4a97-883d-1421d05fd1af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "de9d882e-2e03-4a97-883d-1421d05fd1af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20abe31d-3588-48b1-b3a6-626a06ab8326",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 12
}