{
    "id": "f2b9ede2-2a6f-4cbf-addd-aa9b07fe8381",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_apple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f65554f-f7e7-4037-bed6-9e186cd17e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2b9ede2-2a6f-4cbf-addd-aa9b07fe8381",
            "compositeImage": {
                "id": "4edb7beb-6b75-40e8-89e4-61c2fba2533c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f65554f-f7e7-4037-bed6-9e186cd17e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfafc8d0-99f9-4a21-bc87-fa30077f03d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f65554f-f7e7-4037-bed6-9e186cd17e06",
                    "LayerId": "c69b1008-ed44-4823-92c8-c439d521bc2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c69b1008-ed44-4823-92c8-c439d521bc2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2b9ede2-2a6f-4cbf-addd-aa9b07fe8381",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}