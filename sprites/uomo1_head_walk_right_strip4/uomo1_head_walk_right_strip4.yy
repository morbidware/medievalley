{
    "id": "36a912ad-8850-4e6d-a508-95762b75d098",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3869ee31-4556-4c18-a189-12ecbec4b4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36a912ad-8850-4e6d-a508-95762b75d098",
            "compositeImage": {
                "id": "a44b892e-f7b0-4dc6-a0a1-3aa4e57d19b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3869ee31-4556-4c18-a189-12ecbec4b4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02513876-c3d3-42b3-8aea-5c5a578b85f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3869ee31-4556-4c18-a189-12ecbec4b4a5",
                    "LayerId": "3cc86d22-6b6e-4c72-b97a-a4827a855d5d"
                }
            ]
        },
        {
            "id": "ed61ea83-e830-47eb-b84f-20c1f78012e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36a912ad-8850-4e6d-a508-95762b75d098",
            "compositeImage": {
                "id": "6745801d-6766-488f-a42e-023c14bd65c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed61ea83-e830-47eb-b84f-20c1f78012e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d878cedb-3c53-4db4-a6af-af54cdabaa99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed61ea83-e830-47eb-b84f-20c1f78012e0",
                    "LayerId": "3cc86d22-6b6e-4c72-b97a-a4827a855d5d"
                }
            ]
        },
        {
            "id": "7307bb49-a0cb-40ff-852e-3a573213969b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36a912ad-8850-4e6d-a508-95762b75d098",
            "compositeImage": {
                "id": "1e7703bf-111c-447e-9df2-a6af6373c0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7307bb49-a0cb-40ff-852e-3a573213969b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efdbd592-8cb9-4f17-9031-414c0dc56832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7307bb49-a0cb-40ff-852e-3a573213969b",
                    "LayerId": "3cc86d22-6b6e-4c72-b97a-a4827a855d5d"
                }
            ]
        },
        {
            "id": "e815fc20-5a88-48c7-884a-8d6f0d6ff61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36a912ad-8850-4e6d-a508-95762b75d098",
            "compositeImage": {
                "id": "6fbde338-e28c-42d4-9878-feff18344734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e815fc20-5a88-48c7-884a-8d6f0d6ff61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d533980-440c-49c3-9359-5612adafd140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e815fc20-5a88-48c7-884a-8d6f0d6ff61a",
                    "LayerId": "3cc86d22-6b6e-4c72-b97a-a4827a855d5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3cc86d22-6b6e-4c72-b97a-a4827a855d5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36a912ad-8850-4e6d-a508-95762b75d098",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}