{
    "id": "77094276-d94c-4201-b9e3-f1899b9e94bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9856e901-6fe7-48cf-b03e-243f849c46a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77094276-d94c-4201-b9e3-f1899b9e94bc",
            "compositeImage": {
                "id": "484499b4-3a45-4617-beb9-adf50ec93eec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9856e901-6fe7-48cf-b03e-243f849c46a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dbf8d7f-a760-4c15-9202-252cb20a7180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9856e901-6fe7-48cf-b03e-243f849c46a6",
                    "LayerId": "cba5cbd3-950b-4940-a9d6-462acf0d8737"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cba5cbd3-950b-4940-a9d6-462acf0d8737",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77094276-d94c-4201-b9e3-f1899b9e94bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}