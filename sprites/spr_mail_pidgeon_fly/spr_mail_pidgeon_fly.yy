{
    "id": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mail_pidgeon_fly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1a9d09d-5e2d-422b-9165-b4f0ba922f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "189faa67-e0ec-47e6-84c6-eca30c0f223e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a9d09d-5e2d-422b-9165-b4f0ba922f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9beea804-f8b3-433d-8e8c-daf2685d9061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a9d09d-5e2d-422b-9165-b4f0ba922f13",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "0b491639-2c52-4409-81c0-a10f3e57f2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "64e8cd01-43bd-4ec7-b019-bbada528979a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b491639-2c52-4409-81c0-a10f3e57f2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b5ec0e5-94ed-48f3-96ea-366c48d7e75f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b491639-2c52-4409-81c0-a10f3e57f2f4",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "668d2730-932f-42a9-b726-7d168ad1b4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "8f676873-70a0-4fae-96f1-7f39053b026c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668d2730-932f-42a9-b726-7d168ad1b4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f073178-379e-49a6-8551-29c082e4aab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668d2730-932f-42a9-b726-7d168ad1b4c2",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "e15d7ee2-fdf2-45ad-b3ab-8460f97f73e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "8ef6aaf4-0f5e-4d4e-b394-2a0ffcc233ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15d7ee2-fdf2-45ad-b3ab-8460f97f73e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b90dc066-7b56-4a98-a1dc-036f0b5a7b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15d7ee2-fdf2-45ad-b3ab-8460f97f73e7",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "e6c13dd1-8e08-4ea5-b4f3-6ccd7ae23f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "f429d07e-e708-4533-931c-e5eff36874bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6c13dd1-8e08-4ea5-b4f3-6ccd7ae23f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b1678e8-ac18-46b7-8f93-1a9dfe78b9aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6c13dd1-8e08-4ea5-b4f3-6ccd7ae23f8f",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "efe6db8b-1fbf-4d2d-8ef0-f26f3d733576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "82a5a6c9-779c-4d3d-b18b-6344fb59e1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe6db8b-1fbf-4d2d-8ef0-f26f3d733576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e908e0ad-a7e9-4c5c-be84-957a6b558fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe6db8b-1fbf-4d2d-8ef0-f26f3d733576",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "221cc55c-9789-45a7-8225-29f82e818e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "39a3d9cf-dd67-4370-95c1-3771a325018a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221cc55c-9789-45a7-8225-29f82e818e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2100babc-d66e-4c60-b369-d05ca1c7b8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221cc55c-9789-45a7-8225-29f82e818e06",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        },
        {
            "id": "db1d79af-67d4-473c-9693-4753108d048b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "compositeImage": {
                "id": "d8784db2-df17-40a4-a82f-be9d1b131508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1d79af-67d4-473c-9693-4753108d048b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f8d048-4623-4e36-a366-8d24fe2f8f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1d79af-67d4-473c-9693-4753108d048b",
                    "LayerId": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fd4b19b8-bd63-41bb-98b3-7d059cf22fed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d83ec66b-e67f-45d3-8df3-c0b3620a1cf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 15
}