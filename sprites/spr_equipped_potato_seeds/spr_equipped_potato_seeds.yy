{
    "id": "ac3ea93e-b4db-46dd-92c9-e11474e1d535",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potato_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82c6d2cd-afe2-4016-a4b8-ef9c5da61747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac3ea93e-b4db-46dd-92c9-e11474e1d535",
            "compositeImage": {
                "id": "43808288-a634-4004-b84c-8d62e16677cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c6d2cd-afe2-4016-a4b8-ef9c5da61747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d71bc4d1-e79d-41f3-af2c-338b65bd5290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c6d2cd-afe2-4016-a4b8-ef9c5da61747",
                    "LayerId": "58bcf7f1-0b1a-404d-9c69-5c6b8643e5b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "58bcf7f1-0b1a-404d-9c69-5c6b8643e5b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac3ea93e-b4db-46dd-92c9-e11474e1d535",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}