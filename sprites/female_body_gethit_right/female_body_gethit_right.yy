{
    "id": "9feec14c-8987-4904-8af5-99a89720ab04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "149cc8bd-ac5c-481a-a404-834827387a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9feec14c-8987-4904-8af5-99a89720ab04",
            "compositeImage": {
                "id": "dfea63f2-f852-4093-a5f8-da62b272c29a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "149cc8bd-ac5c-481a-a404-834827387a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f319a749-a8bd-48d8-b738-d392891f5794",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "149cc8bd-ac5c-481a-a404-834827387a07",
                    "LayerId": "b042e534-0676-4fc5-9e06-f7bec3b4f4e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b042e534-0676-4fc5-9e06-f7bec3b4f4e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9feec14c-8987-4904-8af5-99a89720ab04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}