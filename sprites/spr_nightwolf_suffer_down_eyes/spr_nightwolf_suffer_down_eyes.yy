{
    "id": "13c0b9fc-63b5-4879-a9da-50a46c17f83d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_suffer_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4975522-cfa5-4f21-93cb-0b3ca0f145e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13c0b9fc-63b5-4879-a9da-50a46c17f83d",
            "compositeImage": {
                "id": "bb78ffc0-680b-4ee6-be4c-4ac4f2f75a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4975522-cfa5-4f21-93cb-0b3ca0f145e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fd61088-48f6-4915-b878-e9749b29419d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4975522-cfa5-4f21-93cb-0b3ca0f145e3",
                    "LayerId": "c2a20903-ca83-4acf-ba14-72217343681e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "c2a20903-ca83-4acf-ba14-72217343681e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13c0b9fc-63b5-4879-a9da-50a46c17f83d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}