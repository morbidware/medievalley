{
    "id": "33a96830-3989-4fad-a09c-2c859d3d8c9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_axe_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "086450bb-489a-4d25-871a-82bbae3426c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a96830-3989-4fad-a09c-2c859d3d8c9a",
            "compositeImage": {
                "id": "0b0c5ef1-8f9d-48dc-a497-9b118a631bed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086450bb-489a-4d25-871a-82bbae3426c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fcea3d-3c27-41db-a8e0-3c06a54afebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086450bb-489a-4d25-871a-82bbae3426c6",
                    "LayerId": "007e04b8-ee79-4e17-afef-ba56f157b8f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "007e04b8-ee79-4e17-afef-ba56f157b8f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33a96830-3989-4fad-a09c-2c859d3d8c9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 13
}