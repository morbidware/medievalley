{
    "id": "4dbd347d-dc9f-4ca9-a728-c162eefc5e28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_close_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b26aca17-d1f5-4500-9207-43107de51140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dbd347d-dc9f-4ca9-a728-c162eefc5e28",
            "compositeImage": {
                "id": "753c90ba-bf0d-4657-a164-c52970bc3617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b26aca17-d1f5-4500-9207-43107de51140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac8bc15-2b5f-411a-9b1c-1b7aaa7d36c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b26aca17-d1f5-4500-9207-43107de51140",
                    "LayerId": "10460d11-b592-4500-8f26-0824e1db5ef5"
                }
            ]
        },
        {
            "id": "10a9bcbb-aacc-4cac-803c-8f76a61b4c70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dbd347d-dc9f-4ca9-a728-c162eefc5e28",
            "compositeImage": {
                "id": "c9cfaa70-4df6-42c1-939d-4e86c6dfcf57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10a9bcbb-aacc-4cac-803c-8f76a61b4c70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c93416c-d561-4c43-9418-1f7f1760fc44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a9bcbb-aacc-4cac-803c-8f76a61b4c70",
                    "LayerId": "10460d11-b592-4500-8f26-0824e1db5ef5"
                }
            ]
        },
        {
            "id": "173ecde7-f1b8-4320-b8a0-5442a8f878f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dbd347d-dc9f-4ca9-a728-c162eefc5e28",
            "compositeImage": {
                "id": "ee869254-7d8f-4150-8303-59e08cbb9b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "173ecde7-f1b8-4320-b8a0-5442a8f878f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925a5705-67a9-4707-94cf-9d7268dd3d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "173ecde7-f1b8-4320-b8a0-5442a8f878f3",
                    "LayerId": "10460d11-b592-4500-8f26-0824e1db5ef5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "10460d11-b592-4500-8f26-0824e1db5ef5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dbd347d-dc9f-4ca9-a728-c162eefc5e28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}