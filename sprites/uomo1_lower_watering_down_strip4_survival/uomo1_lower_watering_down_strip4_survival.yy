{
    "id": "91688103-f021-4195-8c5f-c4ebc341b853",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_watering_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89cbfeab-aace-4b40-bede-64c15463e4af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "compositeImage": {
                "id": "2ac63c9f-a254-443a-a11f-07eb0aed4f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89cbfeab-aace-4b40-bede-64c15463e4af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5275763e-5cbe-4268-b6a4-47f1dd0f8fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89cbfeab-aace-4b40-bede-64c15463e4af",
                    "LayerId": "13437880-b371-45a1-b37c-402a7ca059f6"
                }
            ]
        },
        {
            "id": "ddaf2522-1828-4eff-ba40-d013a0d0b918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "compositeImage": {
                "id": "5cad559b-c4b2-480d-bc6d-4646644dee05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddaf2522-1828-4eff-ba40-d013a0d0b918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad0a9ffb-d6f9-42dd-9a4c-ef415d18bf47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddaf2522-1828-4eff-ba40-d013a0d0b918",
                    "LayerId": "13437880-b371-45a1-b37c-402a7ca059f6"
                }
            ]
        },
        {
            "id": "85ac373b-c624-4f0b-a4a6-7f40715dfe17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "compositeImage": {
                "id": "2ed65b0d-4416-4a47-8235-61e667d44568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85ac373b-c624-4f0b-a4a6-7f40715dfe17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1520a640-6623-49c3-a7e3-3cb75bc29621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85ac373b-c624-4f0b-a4a6-7f40715dfe17",
                    "LayerId": "13437880-b371-45a1-b37c-402a7ca059f6"
                }
            ]
        },
        {
            "id": "8cb74b5d-b564-43a0-9613-2f87670f7e1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "compositeImage": {
                "id": "5727758a-a5da-4ba4-bc5e-f81c87bb094b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb74b5d-b564-43a0-9613-2f87670f7e1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da9294f-6087-4723-9f39-0abd98baa466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb74b5d-b564-43a0-9613-2f87670f7e1d",
                    "LayerId": "13437880-b371-45a1-b37c-402a7ca059f6"
                }
            ]
        },
        {
            "id": "d9f7e8da-c472-4214-9713-d20eb1cb3be7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "compositeImage": {
                "id": "69c6fbc2-d644-4823-b6dc-fcf4867c7141",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f7e8da-c472-4214-9713-d20eb1cb3be7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "126c75c4-ca75-400d-afd7-8ded5940313e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f7e8da-c472-4214-9713-d20eb1cb3be7",
                    "LayerId": "13437880-b371-45a1-b37c-402a7ca059f6"
                }
            ]
        },
        {
            "id": "8e42fecd-43c6-46a8-95d0-015c7396ceff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "compositeImage": {
                "id": "64ab83ca-a4c7-47b4-a2bd-9b0b7cece149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e42fecd-43c6-46a8-95d0-015c7396ceff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30470aaa-1e3f-4d0e-b501-a114298ed7f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e42fecd-43c6-46a8-95d0-015c7396ceff",
                    "LayerId": "13437880-b371-45a1-b37c-402a7ca059f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "13437880-b371-45a1-b37c-402a7ca059f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91688103-f021-4195-8c5f-c4ebc341b853",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}