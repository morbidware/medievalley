{
    "id": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_label",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f22185ca-e78c-48c0-9b05-f14b58168168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "f79e3df9-2439-4bec-9e53-cfd7203a2202",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22185ca-e78c-48c0-9b05-f14b58168168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ff8a03-887f-48f6-9d2e-8befa51cd48a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22185ca-e78c-48c0-9b05-f14b58168168",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "15a109db-1883-4887-a31b-794befe9ea14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "4b1c6295-309d-4ad8-9c65-9df76eea0feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a109db-1883-4887-a31b-794befe9ea14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1f2c208-eca2-4af0-a88b-695f06e250ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a109db-1883-4887-a31b-794befe9ea14",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "832c6b60-664b-40e7-9690-88603c6e2228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "c91e2cdb-b93f-4f79-8836-5566f8013ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "832c6b60-664b-40e7-9690-88603c6e2228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a799d80-c731-46ed-a30e-e00164a050bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "832c6b60-664b-40e7-9690-88603c6e2228",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "2bd57821-d753-452a-a043-2f5caeaaae36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "a38bf35a-d2e2-49bd-a1f4-405f79a79559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bd57821-d753-452a-a043-2f5caeaaae36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80826684-89ca-417c-898b-90885d5e9653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bd57821-d753-452a-a043-2f5caeaaae36",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "64cdc747-11f6-420d-8b21-30b894ddd9be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "25140c28-9467-48a4-85c8-811b0c5034f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64cdc747-11f6-420d-8b21-30b894ddd9be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e409121-ee2a-48bf-a1ab-12598f8d46bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64cdc747-11f6-420d-8b21-30b894ddd9be",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "100db566-c18d-4042-9e99-df451f056131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "1bce18df-df2c-41e3-8aee-dbcd7a99d572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "100db566-c18d-4042-9e99-df451f056131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e723b6e9-69ad-457a-b910-631c008fa02a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "100db566-c18d-4042-9e99-df451f056131",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "a8513beb-c8f3-46d9-9889-0355af5b7a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "18dfadc5-a021-4fac-a055-afac25faa4a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8513beb-c8f3-46d9-9889-0355af5b7a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf08f96-76e5-42b4-8246-ac78c6024a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8513beb-c8f3-46d9-9889-0355af5b7a5d",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "9d1e2049-5036-41ec-b64c-4d11d0425eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "b686e500-4f8e-459f-ba13-c539e10a446d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d1e2049-5036-41ec-b64c-4d11d0425eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b01cb6bf-0969-4558-93a3-18b6fd6cd713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d1e2049-5036-41ec-b64c-4d11d0425eea",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        },
        {
            "id": "0f3bd196-1c85-4d21-8e02-ddc66a9f6705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "compositeImage": {
                "id": "12d6922c-715a-4b3b-a671-1872fb390932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f3bd196-1c85-4d21-8e02-ddc66a9f6705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7faaecfc-3336-48a8-b973-4104bc1eb6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f3bd196-1c85-4d21-8e02-ddc66a9f6705",
                    "LayerId": "8629c32e-e9cf-4804-a512-0005eaf5fada"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "8629c32e-e9cf-4804-a512-0005eaf5fada",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38dfffa0-9ec2-4cd0-9a35-23f75eb6f029",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}