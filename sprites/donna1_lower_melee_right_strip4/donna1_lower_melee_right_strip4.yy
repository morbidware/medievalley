{
    "id": "502bc2cf-7b72-4b6b-987d-60a8e9dfc642",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f38010a2-1835-48fe-95b2-221cb3aa5f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "502bc2cf-7b72-4b6b-987d-60a8e9dfc642",
            "compositeImage": {
                "id": "3b9dc3ec-80bf-4cc4-a1eb-e52304dfb838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38010a2-1835-48fe-95b2-221cb3aa5f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b9c283e-9f10-499b-9126-7cd7c288c644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38010a2-1835-48fe-95b2-221cb3aa5f7e",
                    "LayerId": "12fa121a-f1ed-4c55-bf03-e9fae45b35df"
                }
            ]
        },
        {
            "id": "e4c4febd-62c9-406a-9425-65bc5a4c8dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "502bc2cf-7b72-4b6b-987d-60a8e9dfc642",
            "compositeImage": {
                "id": "a7e66add-6afb-4271-b8c8-8caf7ed4170a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4c4febd-62c9-406a-9425-65bc5a4c8dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7df14c17-1a53-4d27-b8b1-74098a20225b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4c4febd-62c9-406a-9425-65bc5a4c8dbe",
                    "LayerId": "12fa121a-f1ed-4c55-bf03-e9fae45b35df"
                }
            ]
        },
        {
            "id": "21c0128f-58b9-4808-965f-5cb480a4d1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "502bc2cf-7b72-4b6b-987d-60a8e9dfc642",
            "compositeImage": {
                "id": "45c23f40-f793-47ee-9210-e8d696ebc297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21c0128f-58b9-4808-965f-5cb480a4d1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0add7fd-a08e-4b6f-ab5d-7217f73e763f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21c0128f-58b9-4808-965f-5cb480a4d1eb",
                    "LayerId": "12fa121a-f1ed-4c55-bf03-e9fae45b35df"
                }
            ]
        },
        {
            "id": "2e893187-b894-495c-b95f-f4422f77d7cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "502bc2cf-7b72-4b6b-987d-60a8e9dfc642",
            "compositeImage": {
                "id": "77b5af15-6a13-4cbe-9195-49a11b4db95b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e893187-b894-495c-b95f-f4422f77d7cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8ba4eb-66eb-4dc0-95b4-63d23e0763e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e893187-b894-495c-b95f-f4422f77d7cd",
                    "LayerId": "12fa121a-f1ed-4c55-bf03-e9fae45b35df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "12fa121a-f1ed-4c55-bf03-e9fae45b35df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "502bc2cf-7b72-4b6b-987d-60a8e9dfc642",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}