{
    "id": "e0498f31-8cff-4e3c-b13b-8194a4f17b7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "740ed291-d110-41c1-bd01-2b35fa395bbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0498f31-8cff-4e3c-b13b-8194a4f17b7d",
            "compositeImage": {
                "id": "8d21cd07-8d91-474d-9c38-d615bf479a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740ed291-d110-41c1-bd01-2b35fa395bbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb19b0bc-9f45-4b85-b9e4-292cd4d88e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740ed291-d110-41c1-bd01-2b35fa395bbf",
                    "LayerId": "d22e536a-9666-4e6a-a268-5df75934e921"
                }
            ]
        },
        {
            "id": "b48960ce-9027-456b-95e0-640b17ee48de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0498f31-8cff-4e3c-b13b-8194a4f17b7d",
            "compositeImage": {
                "id": "472e4af8-0b7b-429b-a244-f94ffe844dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48960ce-9027-456b-95e0-640b17ee48de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "300d5ff2-4d4d-4896-92c5-35bdd5aa098f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48960ce-9027-456b-95e0-640b17ee48de",
                    "LayerId": "d22e536a-9666-4e6a-a268-5df75934e921"
                }
            ]
        },
        {
            "id": "21f62905-9c19-406a-8858-f4f8e7d5b187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0498f31-8cff-4e3c-b13b-8194a4f17b7d",
            "compositeImage": {
                "id": "46d9b736-d204-4e61-bdbc-956b24b8fac1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f62905-9c19-406a-8858-f4f8e7d5b187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecaaf54f-98a1-429b-94b9-c113bec6ff15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f62905-9c19-406a-8858-f4f8e7d5b187",
                    "LayerId": "d22e536a-9666-4e6a-a268-5df75934e921"
                }
            ]
        },
        {
            "id": "a58f5fa6-d669-423c-b420-4ec10b92857c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0498f31-8cff-4e3c-b13b-8194a4f17b7d",
            "compositeImage": {
                "id": "cfcdc72e-ed97-4e6a-bb49-6bc0739dcd19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a58f5fa6-d669-423c-b420-4ec10b92857c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df277788-9d9c-40c4-98f1-f73dcdadac6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a58f5fa6-d669-423c-b420-4ec10b92857c",
                    "LayerId": "d22e536a-9666-4e6a-a268-5df75934e921"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d22e536a-9666-4e6a-a268-5df75934e921",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0498f31-8cff-4e3c-b13b-8194a4f17b7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}