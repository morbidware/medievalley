{
    "id": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grindstone_top",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e068403d-9f7e-464e-a8fc-fe68caf512df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "ec296175-70db-4037-9c23-a95b2d5188cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e068403d-9f7e-464e-a8fc-fe68caf512df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2454627f-d0ca-4656-8414-4dbf77f415dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e068403d-9f7e-464e-a8fc-fe68caf512df",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "4e39002f-8dc4-4ed6-8f7f-1c99c835a85d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "3d84e960-48d7-4c79-830a-7964032b8948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e39002f-8dc4-4ed6-8f7f-1c99c835a85d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb93acb-a7c8-4a83-b2f7-93e0731060d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e39002f-8dc4-4ed6-8f7f-1c99c835a85d",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "ef9224ff-bffb-4431-ad0f-03f5dd3e72c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "d76a2fd9-d699-4a8a-839a-8a24ecd2bb56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef9224ff-bffb-4431-ad0f-03f5dd3e72c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f881e4a5-bd32-427f-bbca-ae723ca6fa15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef9224ff-bffb-4431-ad0f-03f5dd3e72c9",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "bc588a41-7372-4b7c-812f-3469023d996d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "3ea70aea-53fc-4f66-b0a9-42c7ec6b56f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc588a41-7372-4b7c-812f-3469023d996d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90503cce-8de0-4208-9de6-ada58b54055e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc588a41-7372-4b7c-812f-3469023d996d",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "719c9c4e-94ff-47ed-be12-b06f4b911738",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "c16def29-8f3d-4ad4-b654-fa7328170cee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "719c9c4e-94ff-47ed-be12-b06f4b911738",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4059aa-67ae-4b14-94af-428aefab3eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "719c9c4e-94ff-47ed-be12-b06f4b911738",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "95b39f22-9914-4ece-8610-50d83c0a264f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "f712d771-1f1e-4e77-8ca0-212b2cb404d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b39f22-9914-4ece-8610-50d83c0a264f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "419d295b-f48b-4127-ae60-5a335e251b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b39f22-9914-4ece-8610-50d83c0a264f",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "9957e329-e8a5-45fb-8c4a-72ae78db5b73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "1d9bdd13-c13c-49df-969b-93da3eda2470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9957e329-e8a5-45fb-8c4a-72ae78db5b73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5570e79f-7cc8-4816-9e6e-39e563cc0bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9957e329-e8a5-45fb-8c4a-72ae78db5b73",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        },
        {
            "id": "9dde65ed-5146-4aaa-9566-c37bc96e4a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "compositeImage": {
                "id": "e0d90903-dbcf-436a-a33c-ceb3b30e2ff2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dde65ed-5146-4aaa-9566-c37bc96e4a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2baead3-4f0c-47d5-83cd-009bad302ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dde65ed-5146-4aaa-9566-c37bc96e4a75",
                    "LayerId": "3799afaa-3b40-41a1-841d-2ea1e2d93214"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3799afaa-3b40-41a1-841d-2ea1e2d93214",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9063208-be74-49b4-bee6-cd3e8e154ab9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}