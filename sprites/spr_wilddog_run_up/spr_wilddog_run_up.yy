{
    "id": "1ed552ed-5621-4def-b649-160b44c6c8ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52e1f7dd-2c15-4ba9-bcd3-a1074c0d0d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "compositeImage": {
                "id": "4b283f69-0cd1-4018-bef3-040005bd9996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e1f7dd-2c15-4ba9-bcd3-a1074c0d0d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "617b0332-2f70-42aa-821d-b9117c000f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e1f7dd-2c15-4ba9-bcd3-a1074c0d0d28",
                    "LayerId": "114b6ffe-eb57-4149-8202-3e43ddd9a509"
                }
            ]
        },
        {
            "id": "26ad7756-93a0-4d7d-8c73-f3be769335e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "compositeImage": {
                "id": "bb5a86ea-cd3a-4b27-8f13-78f1f8b97166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ad7756-93a0-4d7d-8c73-f3be769335e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b606fe4-d0ae-4757-9a40-ae230619d2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ad7756-93a0-4d7d-8c73-f3be769335e5",
                    "LayerId": "114b6ffe-eb57-4149-8202-3e43ddd9a509"
                }
            ]
        },
        {
            "id": "80f2fbbb-a9cd-41b9-b864-90b4641d04ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "compositeImage": {
                "id": "318f3968-fca5-403a-b5e1-2673d65b7a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80f2fbbb-a9cd-41b9-b864-90b4641d04ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a6794f-fb91-4c7c-86a6-5b969ec02911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80f2fbbb-a9cd-41b9-b864-90b4641d04ca",
                    "LayerId": "114b6ffe-eb57-4149-8202-3e43ddd9a509"
                }
            ]
        },
        {
            "id": "54f34c10-831e-4dc4-a25b-e0e3d2671474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "compositeImage": {
                "id": "492cce35-4657-4b11-9c8b-517dfa1a3b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54f34c10-831e-4dc4-a25b-e0e3d2671474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34555f0e-6960-4f46-adbc-bc482fa599f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54f34c10-831e-4dc4-a25b-e0e3d2671474",
                    "LayerId": "114b6ffe-eb57-4149-8202-3e43ddd9a509"
                }
            ]
        },
        {
            "id": "ee228286-86fb-4c61-821e-4b137ba82ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "compositeImage": {
                "id": "e53fedbd-a34c-4e45-afa6-7b7f2385301c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee228286-86fb-4c61-821e-4b137ba82ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bae8382-5d79-416e-a569-68123da3d9e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee228286-86fb-4c61-821e-4b137ba82ca8",
                    "LayerId": "114b6ffe-eb57-4149-8202-3e43ddd9a509"
                }
            ]
        },
        {
            "id": "73a77acc-fd7e-4741-b49d-7b4fa9ad7751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "compositeImage": {
                "id": "1c8fd1c1-97be-44e2-b7d9-3d6731d82067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73a77acc-fd7e-4741-b49d-7b4fa9ad7751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "748ee1e2-40ec-4da2-bc8d-886cd09631d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73a77acc-fd7e-4741-b49d-7b4fa9ad7751",
                    "LayerId": "114b6ffe-eb57-4149-8202-3e43ddd9a509"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "114b6ffe-eb57-4149-8202-3e43ddd9a509",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ed552ed-5621-4def-b649-160b44c6c8ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}