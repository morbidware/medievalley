{
    "id": "e411db05-a64e-4471-a094-980db78ba71a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "622e36c9-d178-4241-8917-8b946ff82222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e411db05-a64e-4471-a094-980db78ba71a",
            "compositeImage": {
                "id": "e6cdd2bf-dd3d-4088-8877-89e74001c977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622e36c9-d178-4241-8917-8b946ff82222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2583be0f-f73d-4326-b68f-91c968dbec2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622e36c9-d178-4241-8917-8b946ff82222",
                    "LayerId": "fc4bd36a-7953-4896-93ec-dcad5a0bd418"
                }
            ]
        },
        {
            "id": "ae879bf2-426c-4394-ad4f-9a2863f131cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e411db05-a64e-4471-a094-980db78ba71a",
            "compositeImage": {
                "id": "4c0e4d7a-f6ed-4eb2-85dd-d66b93cef795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae879bf2-426c-4394-ad4f-9a2863f131cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36417612-35c8-4c64-849e-867ffcb7daab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae879bf2-426c-4394-ad4f-9a2863f131cc",
                    "LayerId": "fc4bd36a-7953-4896-93ec-dcad5a0bd418"
                }
            ]
        },
        {
            "id": "4f2da5c1-9f75-4c03-a916-b17e06f2ba7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e411db05-a64e-4471-a094-980db78ba71a",
            "compositeImage": {
                "id": "44bdb8a0-7a56-4c3e-aeca-027023fb5d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2da5c1-9f75-4c03-a916-b17e06f2ba7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1f44e56-7006-44de-bd48-572cf6f4db71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2da5c1-9f75-4c03-a916-b17e06f2ba7e",
                    "LayerId": "fc4bd36a-7953-4896-93ec-dcad5a0bd418"
                }
            ]
        },
        {
            "id": "57a31c5e-1026-4bad-b81e-dc2473c5d614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e411db05-a64e-4471-a094-980db78ba71a",
            "compositeImage": {
                "id": "089e7a9a-d0e8-4d63-a7f7-120fadce096f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a31c5e-1026-4bad-b81e-dc2473c5d614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff749155-b521-4c92-b60a-e0c633b6e34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a31c5e-1026-4bad-b81e-dc2473c5d614",
                    "LayerId": "fc4bd36a-7953-4896-93ec-dcad5a0bd418"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc4bd36a-7953-4896-93ec-dcad5a0bd418",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e411db05-a64e-4471-a094-980db78ba71a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}