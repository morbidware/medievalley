{
    "id": "9f9a89e2-75f8-4a86-9052-515d9284e48e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9fab1a9-e28f-4b54-ac26-d7fa5b29848f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f9a89e2-75f8-4a86-9052-515d9284e48e",
            "compositeImage": {
                "id": "7f01774d-f0b9-47ea-bf1c-b6ba077cf990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9fab1a9-e28f-4b54-ac26-d7fa5b29848f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a88810ac-ace7-425a-9259-a0e1c463f9bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9fab1a9-e28f-4b54-ac26-d7fa5b29848f",
                    "LayerId": "760b4221-4256-49a4-a099-fc1769beaaec"
                }
            ]
        },
        {
            "id": "97310749-0a31-4751-9b70-afc97b809411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f9a89e2-75f8-4a86-9052-515d9284e48e",
            "compositeImage": {
                "id": "001d7817-db4b-43b8-8c36-f0510a75e50b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97310749-0a31-4751-9b70-afc97b809411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e209aff-1e56-4fda-9f65-483d38db20e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97310749-0a31-4751-9b70-afc97b809411",
                    "LayerId": "760b4221-4256-49a4-a099-fc1769beaaec"
                }
            ]
        },
        {
            "id": "e1e84d08-c9fe-4327-b584-1aee2f8e7542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f9a89e2-75f8-4a86-9052-515d9284e48e",
            "compositeImage": {
                "id": "be0f5e7c-0e61-4b61-b76a-8401193342f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e84d08-c9fe-4327-b584-1aee2f8e7542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da144304-1be6-4bcd-ac70-34f23348723f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e84d08-c9fe-4327-b584-1aee2f8e7542",
                    "LayerId": "760b4221-4256-49a4-a099-fc1769beaaec"
                }
            ]
        },
        {
            "id": "bdbb7027-6162-4132-b3ab-112f974d7edb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f9a89e2-75f8-4a86-9052-515d9284e48e",
            "compositeImage": {
                "id": "f9acd5fc-fe5d-4590-a101-295fd40cfa63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdbb7027-6162-4132-b3ab-112f974d7edb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb5d8a6c-b0d8-4eba-921a-a8f5e489ce41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdbb7027-6162-4132-b3ab-112f974d7edb",
                    "LayerId": "760b4221-4256-49a4-a099-fc1769beaaec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "760b4221-4256-49a4-a099-fc1769beaaec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f9a89e2-75f8-4a86-9052-515d9284e48e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}