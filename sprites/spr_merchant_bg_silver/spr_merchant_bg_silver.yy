{
    "id": "b20ac234-d297-4cd6-b29a-3471427e31cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_merchant_bg_silver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 176,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9533d565-9165-49c3-a9fc-034006a354e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b20ac234-d297-4cd6-b29a-3471427e31cd",
            "compositeImage": {
                "id": "1c275994-25ca-4993-8fe6-f138e89791c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9533d565-9165-49c3-a9fc-034006a354e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaff03e2-6c5f-4cc8-899a-8f9dd4be9653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9533d565-9165-49c3-a9fc-034006a354e4",
                    "LayerId": "7a27e5ba-cbd0-4fd7-8bb6-c2801bc1cdf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "7a27e5ba-cbd0-4fd7-8bb6-c2801bc1cdf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b20ac234-d297-4cd6-b29a-3471427e31cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 153,
    "yorig": 88
}