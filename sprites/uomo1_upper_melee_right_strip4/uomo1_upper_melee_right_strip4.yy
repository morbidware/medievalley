{
    "id": "0b00833a-44a3-4b02-a26e-85f9e524d9d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e9763b9-a994-4bb4-8d5c-543adf45a362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b00833a-44a3-4b02-a26e-85f9e524d9d3",
            "compositeImage": {
                "id": "bb448b48-a929-460f-9b86-f07d20135eba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e9763b9-a994-4bb4-8d5c-543adf45a362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89d2747-548d-408f-8302-ba190a457028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e9763b9-a994-4bb4-8d5c-543adf45a362",
                    "LayerId": "71514c56-53d5-482b-8741-d05300dbfdc5"
                }
            ]
        },
        {
            "id": "26f4260a-a0f7-4d14-aafd-e5d0ef5716ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b00833a-44a3-4b02-a26e-85f9e524d9d3",
            "compositeImage": {
                "id": "6f87d891-a053-4467-a79f-3fa25418f6bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f4260a-a0f7-4d14-aafd-e5d0ef5716ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d884d33-158b-44cb-8339-8107c9221e66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f4260a-a0f7-4d14-aafd-e5d0ef5716ea",
                    "LayerId": "71514c56-53d5-482b-8741-d05300dbfdc5"
                }
            ]
        },
        {
            "id": "6d96ba57-29b3-4a11-b767-f9ff72ed94ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b00833a-44a3-4b02-a26e-85f9e524d9d3",
            "compositeImage": {
                "id": "31180f2e-5042-4e46-9869-66c6392f1936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d96ba57-29b3-4a11-b767-f9ff72ed94ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b57cde5-1ffc-4dbc-8170-e363a78311d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d96ba57-29b3-4a11-b767-f9ff72ed94ac",
                    "LayerId": "71514c56-53d5-482b-8741-d05300dbfdc5"
                }
            ]
        },
        {
            "id": "e3e60001-6f03-4af6-ab45-b4069bd0b81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b00833a-44a3-4b02-a26e-85f9e524d9d3",
            "compositeImage": {
                "id": "150b3ad8-90fb-4592-84a5-f561d88ae45c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e60001-6f03-4af6-ab45-b4069bd0b81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64686aee-64e7-47d9-98f2-adc17fdb8747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e60001-6f03-4af6-ab45-b4069bd0b81a",
                    "LayerId": "71514c56-53d5-482b-8741-d05300dbfdc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "71514c56-53d5-482b-8741-d05300dbfdc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b00833a-44a3-4b02-a26e-85f9e524d9d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}