{
    "id": "da26554d-32ea-4fac-bac5-f4acce9baeb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_gethit_up_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "451fabc7-9829-4f87-8704-d2d2bad1ef50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da26554d-32ea-4fac-bac5-f4acce9baeb7",
            "compositeImage": {
                "id": "e78eec36-5322-41ff-a353-570051c096a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "451fabc7-9829-4f87-8704-d2d2bad1ef50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22b74cd5-49fe-4afb-8fae-2a06d2ab9c81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "451fabc7-9829-4f87-8704-d2d2bad1ef50",
                    "LayerId": "fcfd4560-03b0-4223-81dc-f25a3501e294"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fcfd4560-03b0-4223-81dc-f25a3501e294",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da26554d-32ea-4fac-bac5-f4acce9baeb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}