{
    "id": "e5f9c9c9-002f-488f-aaaf-961994f5a260",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipe_highlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6188fca-92a3-49ba-b614-814a9044bf36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5f9c9c9-002f-488f-aaaf-961994f5a260",
            "compositeImage": {
                "id": "decb7ab7-354a-4350-98e5-85857aab2859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6188fca-92a3-49ba-b614-814a9044bf36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da43263-2d08-451e-a738-e8b7a966dae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6188fca-92a3-49ba-b614-814a9044bf36",
                    "LayerId": "6548bd62-d2e5-4d13-91a9-685a46f2cf96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6548bd62-d2e5-4d13-91a9-685a46f2cf96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5f9c9c9-002f-488f-aaaf-961994f5a260",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}