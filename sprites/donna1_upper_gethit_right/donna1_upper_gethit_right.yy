{
    "id": "a3eb3aa9-4c99-4637-9528-67c5988674af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7288111-8659-4aaf-9090-c1b8970bfb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3eb3aa9-4c99-4637-9528-67c5988674af",
            "compositeImage": {
                "id": "5c28733a-cdc7-4ff3-9c51-cbee3db3b86b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7288111-8659-4aaf-9090-c1b8970bfb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5742fdb7-e903-43dd-a1bf-314f79043a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7288111-8659-4aaf-9090-c1b8970bfb3d",
                    "LayerId": "44ddcfe2-ba86-4414-ad47-49d4dde7a142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44ddcfe2-ba86-4414-ad47-49d4dde7a142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3eb3aa9-4c99-4637-9528-67c5988674af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}