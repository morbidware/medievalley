{
    "id": "6d04d55f-ac17-4a18-9c04-25c2a3feba44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7192e77b-3bd4-43f5-bfbd-c42d01e7ca58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d04d55f-ac17-4a18-9c04-25c2a3feba44",
            "compositeImage": {
                "id": "c7cc8a04-6653-4e38-97d8-d0b5529078bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7192e77b-3bd4-43f5-bfbd-c42d01e7ca58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f4a4dc-cca7-466d-b08b-7967b41b7e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7192e77b-3bd4-43f5-bfbd-c42d01e7ca58",
                    "LayerId": "ae459448-33c8-41be-9389-c80fa48b83e3"
                }
            ]
        },
        {
            "id": "f466829a-296e-4e6d-93f8-3ab0b83bf8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d04d55f-ac17-4a18-9c04-25c2a3feba44",
            "compositeImage": {
                "id": "a588b9ef-9b46-4bb5-aa58-44f35980a3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f466829a-296e-4e6d-93f8-3ab0b83bf8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae31cd4-e4e3-47af-9206-7a558a2ccf81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f466829a-296e-4e6d-93f8-3ab0b83bf8b7",
                    "LayerId": "ae459448-33c8-41be-9389-c80fa48b83e3"
                }
            ]
        },
        {
            "id": "c5ad0b36-b317-4beb-b19f-dea37331f348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d04d55f-ac17-4a18-9c04-25c2a3feba44",
            "compositeImage": {
                "id": "0ae06372-8d70-420b-8287-cb0894068dfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5ad0b36-b317-4beb-b19f-dea37331f348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "428b14b5-a7a6-4910-ba40-9966891c7b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5ad0b36-b317-4beb-b19f-dea37331f348",
                    "LayerId": "ae459448-33c8-41be-9389-c80fa48b83e3"
                }
            ]
        },
        {
            "id": "e6a406d3-489d-442d-9cf5-3abfee775341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d04d55f-ac17-4a18-9c04-25c2a3feba44",
            "compositeImage": {
                "id": "bba480e9-c8f1-4fd6-ba1b-a8cb6c8df3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a406d3-489d-442d-9cf5-3abfee775341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186afea3-4cf2-4226-8bf2-331afea087ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a406d3-489d-442d-9cf5-3abfee775341",
                    "LayerId": "ae459448-33c8-41be-9389-c80fa48b83e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ae459448-33c8-41be-9389-c80fa48b83e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d04d55f-ac17-4a18-9c04-25c2a3feba44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}