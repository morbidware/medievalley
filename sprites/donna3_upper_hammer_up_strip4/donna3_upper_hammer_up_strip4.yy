{
    "id": "bd102e7b-36a0-446c-9f52-6f729db0bed9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56fc2773-b144-4458-9ef8-8d20d0e93def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd102e7b-36a0-446c-9f52-6f729db0bed9",
            "compositeImage": {
                "id": "1571bbfa-645a-4ac8-82c6-b45e987d13e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56fc2773-b144-4458-9ef8-8d20d0e93def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d5a1a7-fce1-487e-ba50-7b303028d12f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56fc2773-b144-4458-9ef8-8d20d0e93def",
                    "LayerId": "55ffb853-6e08-4f9b-840a-aa226cf34e18"
                }
            ]
        },
        {
            "id": "67a627ff-16ed-464b-a36e-98f4d661eab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd102e7b-36a0-446c-9f52-6f729db0bed9",
            "compositeImage": {
                "id": "855c625c-3e54-4011-8b83-ccb0b34b06fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a627ff-16ed-464b-a36e-98f4d661eab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c53cbe4-07a2-404f-aba9-454006833c3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a627ff-16ed-464b-a36e-98f4d661eab8",
                    "LayerId": "55ffb853-6e08-4f9b-840a-aa226cf34e18"
                }
            ]
        },
        {
            "id": "8c994887-3a5c-4039-bf4a-7bca6e9de4fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd102e7b-36a0-446c-9f52-6f729db0bed9",
            "compositeImage": {
                "id": "9635827e-2e23-4645-90b1-926342fd6b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c994887-3a5c-4039-bf4a-7bca6e9de4fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0d79393-c996-44eb-a46e-aaf30bc2f7b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c994887-3a5c-4039-bf4a-7bca6e9de4fc",
                    "LayerId": "55ffb853-6e08-4f9b-840a-aa226cf34e18"
                }
            ]
        },
        {
            "id": "b22d47c7-a394-4704-85dc-0070745bdb2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd102e7b-36a0-446c-9f52-6f729db0bed9",
            "compositeImage": {
                "id": "45466add-4137-43d6-bc49-86b486189a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22d47c7-a394-4704-85dc-0070745bdb2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20281138-db8a-474d-929c-0da97c8a32e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22d47c7-a394-4704-85dc-0070745bdb2f",
                    "LayerId": "55ffb853-6e08-4f9b-840a-aa226cf34e18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55ffb853-6e08-4f9b-840a-aa226cf34e18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd102e7b-36a0-446c-9f52-6f729db0bed9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}