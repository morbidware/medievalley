{
    "id": "e6151d0c-fcd5-4646-a50a-5be583d887d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bdd840c-5d1f-4d6a-b1c7-90b4cad54fdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6151d0c-fcd5-4646-a50a-5be583d887d1",
            "compositeImage": {
                "id": "fd81049d-3472-47f6-988e-80caed58be00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bdd840c-5d1f-4d6a-b1c7-90b4cad54fdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc2ebd75-f658-4b40-b9d0-99bcc1646e11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bdd840c-5d1f-4d6a-b1c7-90b4cad54fdc",
                    "LayerId": "cc184038-5521-49ce-967e-e42b6055e214"
                }
            ]
        },
        {
            "id": "76283e05-6f96-4b51-a8a1-6aefdc2ecfd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6151d0c-fcd5-4646-a50a-5be583d887d1",
            "compositeImage": {
                "id": "af981675-12b6-4827-bc18-dbebd00d2c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76283e05-6f96-4b51-a8a1-6aefdc2ecfd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "562a6223-7723-4c66-80f1-d2312455e523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76283e05-6f96-4b51-a8a1-6aefdc2ecfd3",
                    "LayerId": "cc184038-5521-49ce-967e-e42b6055e214"
                }
            ]
        },
        {
            "id": "405fc203-ab92-4e76-a5e8-d2d2d7fe0389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6151d0c-fcd5-4646-a50a-5be583d887d1",
            "compositeImage": {
                "id": "44bd09b3-f57d-4a7c-a5a1-0d813037dce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "405fc203-ab92-4e76-a5e8-d2d2d7fe0389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e4369e-7c46-490b-aa29-605b003b4620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "405fc203-ab92-4e76-a5e8-d2d2d7fe0389",
                    "LayerId": "cc184038-5521-49ce-967e-e42b6055e214"
                }
            ]
        },
        {
            "id": "d586b7aa-0175-45c4-8a2d-3ab525278b97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6151d0c-fcd5-4646-a50a-5be583d887d1",
            "compositeImage": {
                "id": "f909d34b-cdca-4e20-bc40-63284ccd10d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d586b7aa-0175-45c4-8a2d-3ab525278b97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bd80edb-b83d-48ea-9d32-a0b5f818b677",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d586b7aa-0175-45c4-8a2d-3ab525278b97",
                    "LayerId": "cc184038-5521-49ce-967e-e42b6055e214"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc184038-5521-49ce-967e-e42b6055e214",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6151d0c-fcd5-4646-a50a-5be583d887d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}