{
    "id": "2c4ace89-5095-40b4-a9f5-de7c661e52d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_soil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1460830e-6614-42f1-bfe8-74bd188e6cec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c4ace89-5095-40b4-a9f5-de7c661e52d4",
            "compositeImage": {
                "id": "85f94ca6-3fba-4041-bf36-b5044bf981f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1460830e-6614-42f1-bfe8-74bd188e6cec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3469385f-c246-4674-843e-f0fada76eff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1460830e-6614-42f1-bfe8-74bd188e6cec",
                    "LayerId": "5b6bc5b9-1125-4ec0-b1bd-f6a606fb0a85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5b6bc5b9-1125-4ec0-b1bd-f6a606fb0a85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c4ace89-5095-40b4-a9f5-de7c661e52d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}