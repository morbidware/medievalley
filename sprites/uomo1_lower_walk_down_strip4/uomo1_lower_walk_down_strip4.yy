{
    "id": "410dc971-6e19-4b0b-81f8-beb7af982d33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6a9d043-4362-471b-bc24-8b9a2cbbeb2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "410dc971-6e19-4b0b-81f8-beb7af982d33",
            "compositeImage": {
                "id": "308f9e40-47a5-4c4f-9798-aa4cd1d0174e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a9d043-4362-471b-bc24-8b9a2cbbeb2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cfc563a-8da4-4142-8762-90ba3eff4942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a9d043-4362-471b-bc24-8b9a2cbbeb2d",
                    "LayerId": "d6c19765-c0d3-4659-a537-040f80afddb3"
                }
            ]
        },
        {
            "id": "17813bf5-1fb8-45bf-aa8d-d59c684fef5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "410dc971-6e19-4b0b-81f8-beb7af982d33",
            "compositeImage": {
                "id": "c4948608-d032-4ad3-8bd8-6d85025dd3a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17813bf5-1fb8-45bf-aa8d-d59c684fef5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71e44901-4499-4be6-b878-cda83b67ef85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17813bf5-1fb8-45bf-aa8d-d59c684fef5d",
                    "LayerId": "d6c19765-c0d3-4659-a537-040f80afddb3"
                }
            ]
        },
        {
            "id": "1991389d-b7f5-41a9-b174-9d3fd936de93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "410dc971-6e19-4b0b-81f8-beb7af982d33",
            "compositeImage": {
                "id": "6c59c5aa-cb81-406d-9300-4bc5a9f6462b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1991389d-b7f5-41a9-b174-9d3fd936de93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b01ddf7-d01b-49a6-b566-a3e3c0fc5b0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1991389d-b7f5-41a9-b174-9d3fd936de93",
                    "LayerId": "d6c19765-c0d3-4659-a537-040f80afddb3"
                }
            ]
        },
        {
            "id": "a848d6e1-7e30-4bd2-a7f9-7d5a1444b8e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "410dc971-6e19-4b0b-81f8-beb7af982d33",
            "compositeImage": {
                "id": "0dcaec7c-c6bf-48d4-90f5-39d6fba82c05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a848d6e1-7e30-4bd2-a7f9-7d5a1444b8e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46463729-71c9-4aa5-b44e-1ed367c0caf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a848d6e1-7e30-4bd2-a7f9-7d5a1444b8e5",
                    "LayerId": "d6c19765-c0d3-4659-a537-040f80afddb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6c19765-c0d3-4659-a537-040f80afddb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "410dc971-6e19-4b0b-81f8-beb7af982d33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}