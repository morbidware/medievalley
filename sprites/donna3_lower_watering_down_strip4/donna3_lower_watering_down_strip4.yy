{
    "id": "4d0b48c5-1d26-4816-a470-c8d521004e98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88f9fbc6-b5b4-4dd6-b04c-0faa26fc32c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d0b48c5-1d26-4816-a470-c8d521004e98",
            "compositeImage": {
                "id": "2b2479aa-eab5-4c46-aa46-2788418aa68f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f9fbc6-b5b4-4dd6-b04c-0faa26fc32c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84fc905d-a65e-46ff-847d-7e0b9c642187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f9fbc6-b5b4-4dd6-b04c-0faa26fc32c8",
                    "LayerId": "108b9406-ddd7-4884-b873-973d8f5b4000"
                }
            ]
        },
        {
            "id": "518038f5-95f6-4345-a726-ee6f85a4f304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d0b48c5-1d26-4816-a470-c8d521004e98",
            "compositeImage": {
                "id": "cd2fc964-e14e-43e6-b9a5-38d0263b3883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "518038f5-95f6-4345-a726-ee6f85a4f304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "397917b7-6852-4da1-9c89-61a9f588ee60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518038f5-95f6-4345-a726-ee6f85a4f304",
                    "LayerId": "108b9406-ddd7-4884-b873-973d8f5b4000"
                }
            ]
        },
        {
            "id": "476b308b-96cc-4da4-9a17-5bca6c758399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d0b48c5-1d26-4816-a470-c8d521004e98",
            "compositeImage": {
                "id": "91c0afb8-ecab-4669-a45f-10b53f6fd9d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "476b308b-96cc-4da4-9a17-5bca6c758399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5cc292-ff4d-4c70-b9c1-f26187beba9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "476b308b-96cc-4da4-9a17-5bca6c758399",
                    "LayerId": "108b9406-ddd7-4884-b873-973d8f5b4000"
                }
            ]
        },
        {
            "id": "baa13209-03b5-4657-aa6b-b6f978d251d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d0b48c5-1d26-4816-a470-c8d521004e98",
            "compositeImage": {
                "id": "2ff52785-42ff-454b-aa97-97ba82fdc807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baa13209-03b5-4657-aa6b-b6f978d251d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b818ef6-e28d-4866-a600-cd66ea9b17a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baa13209-03b5-4657-aa6b-b6f978d251d8",
                    "LayerId": "108b9406-ddd7-4884-b873-973d8f5b4000"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "108b9406-ddd7-4884-b873-973d8f5b4000",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d0b48c5-1d26-4816-a470-c8d521004e98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}