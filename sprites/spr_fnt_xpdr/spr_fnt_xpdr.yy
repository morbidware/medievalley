{
    "id": "6da72d35-74ba-476f-ae45-511401529a5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_xpdr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15a93b49-c99e-4e90-aec9-e165f1e81d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "9de46fb5-1675-48ca-9c23-fa733e8cf4d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a93b49-c99e-4e90-aec9-e165f1e81d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f681876-154e-4923-a172-af59eea3eaee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a93b49-c99e-4e90-aec9-e165f1e81d12",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "92105820-43f7-46e6-8ada-3a7ba1dcb542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "adc0e943-18f2-4a93-aa0b-4f59717879a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92105820-43f7-46e6-8ada-3a7ba1dcb542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed8910c-370c-4421-95ad-e3f2841b47e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92105820-43f7-46e6-8ada-3a7ba1dcb542",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "21e0875b-7e30-45db-8cd0-f19b62cf95b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "823db0d0-898a-4cc3-8d32-12b89203b4a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e0875b-7e30-45db-8cd0-f19b62cf95b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0bd9305-8ad1-4a21-bcda-3a2c21027524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e0875b-7e30-45db-8cd0-f19b62cf95b4",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "c90460a8-4b23-4003-8abe-2c785fa33ac0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "7efd2cc5-60fa-4922-8805-36596c00dfc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c90460a8-4b23-4003-8abe-2c785fa33ac0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6943de08-5cbe-48fc-bcc3-5a892ab70e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c90460a8-4b23-4003-8abe-2c785fa33ac0",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "7adb953d-9786-492f-94dc-34dbdcfe5252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "f2ef5e6b-bf91-49e0-b621-75388bbedf5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7adb953d-9786-492f-94dc-34dbdcfe5252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19207726-9be5-40b2-87e8-461afa51d7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7adb953d-9786-492f-94dc-34dbdcfe5252",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "202c690c-9110-404a-8fd4-206ec2f1d2c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "52cec9fd-58a1-418b-9db0-4531beb27fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "202c690c-9110-404a-8fd4-206ec2f1d2c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb00e387-0a3e-4e78-bb17-3ef4d128d0e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "202c690c-9110-404a-8fd4-206ec2f1d2c1",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "86f02967-53ca-485b-b9ff-1dcc441b47de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "ec0773b8-4f21-4c82-8972-5da6cb7f9096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f02967-53ca-485b-b9ff-1dcc441b47de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54e733db-55b3-4694-a353-b76929382880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f02967-53ca-485b-b9ff-1dcc441b47de",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "b76f2927-7349-487c-a357-ce02acc1ec8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "b0abc111-e9f5-4b06-8081-bd057f018a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76f2927-7349-487c-a357-ce02acc1ec8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e733b7e-e263-41a0-a07b-e1034e5f6d7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76f2927-7349-487c-a357-ce02acc1ec8a",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "ed504894-e8cc-48a6-9e06-b649b6c1f39d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "9eddf0f0-0b06-4928-86dd-cc2bff95e6b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed504894-e8cc-48a6-9e06-b649b6c1f39d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75aebe45-a62b-41c2-b1b4-e977549ffdec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed504894-e8cc-48a6-9e06-b649b6c1f39d",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "1f61bb63-55f1-4af1-8dac-2d363183bd85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "8c082ceb-72c1-46ae-818b-ce70c3ed7d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f61bb63-55f1-4af1-8dac-2d363183bd85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9202bb52-3ae2-427d-a115-39ec5d56107c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f61bb63-55f1-4af1-8dac-2d363183bd85",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "9458674a-9fbd-49d1-87ab-b044e3a6b2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "b660c013-f6ce-46c4-b0da-57d4d74a4acf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9458674a-9fbd-49d1-87ab-b044e3a6b2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56af1bf8-b3ef-4492-b4f5-89eec53b872e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9458674a-9fbd-49d1-87ab-b044e3a6b2d7",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "5d147627-7488-4393-887f-a7e61641b866",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "5d9a5826-65bc-41aa-b734-4b7e5fa4534c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d147627-7488-4393-887f-a7e61641b866",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722174f9-2a9c-4503-9f5c-4f4a2ef88130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d147627-7488-4393-887f-a7e61641b866",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "1f567c85-7692-43c2-8336-79bd894e87c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "73c01c13-3e31-423b-8a03-4f4eb459640c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f567c85-7692-43c2-8336-79bd894e87c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b473899-7912-4fda-9805-142273d42c9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f567c85-7692-43c2-8336-79bd894e87c6",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "5607de48-5842-467b-bd77-18d1d1cf8017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "e9e2ac96-ab02-4b7d-bfdf-957eab591f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5607de48-5842-467b-bd77-18d1d1cf8017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bff3a9-bc63-4ee9-9551-710dee022db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5607de48-5842-467b-bd77-18d1d1cf8017",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "a36ca710-cf81-45a4-9282-3f9066a8ef99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "f77e1aea-f999-46e1-9f17-04d4c9738fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a36ca710-cf81-45a4-9282-3f9066a8ef99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c47ba717-e5db-4cd3-bb51-d459a28ed170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a36ca710-cf81-45a4-9282-3f9066a8ef99",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "a8751e41-2e98-4f46-b784-faaa8fa332da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "4cdce7f3-49cd-42f0-ba49-04d5dcf5a260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8751e41-2e98-4f46-b784-faaa8fa332da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e56c6b-669f-4fc9-b628-2eb7f2959f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8751e41-2e98-4f46-b784-faaa8fa332da",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "1c6df695-6726-4a41-813e-ccca51134db6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "259aa9f1-c96b-45f4-9b00-186caf16a330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6df695-6726-4a41-813e-ccca51134db6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8be5d2f7-ab00-48b7-952f-67d0d57c9ed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6df695-6726-4a41-813e-ccca51134db6",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "ac254b5d-b66a-4a9a-bf64-e7c37d7a1035",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "64d5d918-96a1-4984-92fb-50c2be3107c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac254b5d-b66a-4a9a-bf64-e7c37d7a1035",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d02f1bff-47b1-47d4-a669-d20af5fa1ea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac254b5d-b66a-4a9a-bf64-e7c37d7a1035",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "7345b40b-2889-4512-8800-a25bf32cb716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "cc6b9518-210c-46e3-93ad-065cce2dbc91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7345b40b-2889-4512-8800-a25bf32cb716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "388da90b-9843-4cde-b156-498722bd4691",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7345b40b-2889-4512-8800-a25bf32cb716",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "afe2df2e-76f1-44e5-9e2d-72e1795cbcd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "f3467af8-df96-469e-aed9-41b1b11b81fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe2df2e-76f1-44e5-9e2d-72e1795cbcd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e44d21-2d7a-403f-85e8-0ba96967afc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe2df2e-76f1-44e5-9e2d-72e1795cbcd2",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "d969247c-de53-4242-83fc-9ee64a4675a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "02e6e5e3-62f2-4735-ad59-da84dd12a467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d969247c-de53-4242-83fc-9ee64a4675a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e2afd94-0906-409e-85a3-13dbc28c56a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d969247c-de53-4242-83fc-9ee64a4675a8",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "fcc95808-0d6c-4247-914f-e038436981c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "a6c07b3c-00c0-44f9-b4d0-77be4ad02eea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcc95808-0d6c-4247-914f-e038436981c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e933c2-53be-4549-89c3-86dee0ca2115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcc95808-0d6c-4247-914f-e038436981c0",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "fb66b45f-1776-42a4-ae7b-863ee59ff648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "283ef58b-deec-439d-bdf0-2281b720e563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb66b45f-1776-42a4-ae7b-863ee59ff648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cca84dd-a135-4c59-a4d7-0f4bd88c1277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb66b45f-1776-42a4-ae7b-863ee59ff648",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "e2bc4e43-aebe-4e16-8e79-d3eededc0f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "defeb5d5-154f-418d-8ea1-899d2d05bd63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2bc4e43-aebe-4e16-8e79-d3eededc0f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d657f9-1ae9-4227-9e58-554f7e105361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2bc4e43-aebe-4e16-8e79-d3eededc0f04",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "b97c7053-3abd-4f85-95a5-8067061ff9e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "ef52399b-125c-45d2-9223-f4706eeb47e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97c7053-3abd-4f85-95a5-8067061ff9e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cb7a85-5a81-46ad-a2cf-1ede8e7e3cbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97c7053-3abd-4f85-95a5-8067061ff9e0",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "4495f810-1f24-4cf7-a63d-b76d2a771551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "ef4a90a9-e8e1-4d47-b275-27513ac0e2ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4495f810-1f24-4cf7-a63d-b76d2a771551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b06ee2b0-6939-4280-93f7-2ec338884220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4495f810-1f24-4cf7-a63d-b76d2a771551",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "3e9468e9-d3c2-40bd-a2ee-5de080a970be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "ddc580bc-a78e-4cf6-9692-7c95906428a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9468e9-d3c2-40bd-a2ee-5de080a970be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2443b084-8e23-4a70-b311-c049978a8594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9468e9-d3c2-40bd-a2ee-5de080a970be",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "9e7a4330-f5ca-4dd9-ac77-ffd058c0ddd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "a7a267d3-d4f3-4f41-95d2-224e0fb871af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e7a4330-f5ca-4dd9-ac77-ffd058c0ddd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547ae8c9-8d6d-4477-a6f5-f22c47d1f309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e7a4330-f5ca-4dd9-ac77-ffd058c0ddd8",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "98c2662c-4b69-4dc9-b1d8-c8e4e73084a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "efb193aa-db0a-4a3a-acad-382ce1ce3369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c2662c-4b69-4dc9-b1d8-c8e4e73084a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0183d30a-f3bd-41f9-8078-ce244255d1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c2662c-4b69-4dc9-b1d8-c8e4e73084a4",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "77444f8f-00a5-40a9-ad15-90e943f233d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "481fae16-2506-4685-81a6-50ea1973cfc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77444f8f-00a5-40a9-ad15-90e943f233d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e96d5ee-2be2-435d-80e0-3e825db17b0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77444f8f-00a5-40a9-ad15-90e943f233d3",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "3eba7367-9317-4033-8fe5-9dda1a996577",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "044f4f51-1efa-442e-8ad4-3a04183c2715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eba7367-9317-4033-8fe5-9dda1a996577",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34a48c83-a373-412f-9e38-8e96325b53ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eba7367-9317-4033-8fe5-9dda1a996577",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "c349192c-5731-4db3-84ad-c4aac5d29c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "7fb91bd8-bedf-4de1-bc28-ee54b00a2002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c349192c-5731-4db3-84ad-c4aac5d29c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fab9072-ad9a-4ced-b041-9d7fcc6565a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c349192c-5731-4db3-84ad-c4aac5d29c72",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "c5cf5892-3eb9-42d7-9fdf-fedf974e73d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "d365d5b6-ef51-428f-911c-a7507e9dd686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5cf5892-3eb9-42d7-9fdf-fedf974e73d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "886ec371-e0fb-41f5-8dae-b95ff935503e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5cf5892-3eb9-42d7-9fdf-fedf974e73d6",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "6469f660-2a96-4ad3-9f5e-44721a7f4abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "e3f2a17c-27b0-404b-949c-41ba7bd92911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6469f660-2a96-4ad3-9f5e-44721a7f4abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d1f19c-1cc0-4eab-9f65-7da0e357ca18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6469f660-2a96-4ad3-9f5e-44721a7f4abc",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "da1c6da9-d6da-4096-956d-7cf37ed299e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "c33a4929-5069-4036-9d74-61943fbf416d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da1c6da9-d6da-4096-956d-7cf37ed299e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "646aed87-713a-4265-a218-f87013763e06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da1c6da9-d6da-4096-956d-7cf37ed299e6",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "41c982bd-3844-4590-a088-67dff60a8784",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "bb822725-21f2-4036-a3e6-0d189e4857e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41c982bd-3844-4590-a088-67dff60a8784",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125679ac-5fb8-47ba-9e98-76824423c721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41c982bd-3844-4590-a088-67dff60a8784",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "c353d290-bf19-4c7d-8b4e-426a63c0d810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "9ceb5a82-1f8b-4ab2-831d-a277a6f676f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c353d290-bf19-4c7d-8b4e-426a63c0d810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ac4000-41cf-4b25-80fc-b5960a026f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c353d290-bf19-4c7d-8b4e-426a63c0d810",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "af5ab8ad-e1be-4f28-9463-135252997fc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "c1eef7f2-f8e4-4d03-900d-036a117a54c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af5ab8ad-e1be-4f28-9463-135252997fc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5960af4f-f9a5-414b-83ee-9ec2d2368859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af5ab8ad-e1be-4f28-9463-135252997fc0",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "e9871e98-dbcc-4180-9979-cad628e5b800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "0bdfe8f1-c719-436f-965a-ae52c23269c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9871e98-dbcc-4180-9979-cad628e5b800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "948fbef7-8a2e-4b8e-80f3-31f4011df973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9871e98-dbcc-4180-9979-cad628e5b800",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "7529773e-0e17-4f87-b7b1-be9e0c671aa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "c386d9a9-29cd-4ca2-8f0b-5eb54036e274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7529773e-0e17-4f87-b7b1-be9e0c671aa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ef5393-1d6e-4b43-b79f-a0c2df8b6ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7529773e-0e17-4f87-b7b1-be9e0c671aa3",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "64fd1c1d-372b-449b-a05a-64480ae46d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "242836e3-d272-4f11-a053-24963f6637e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64fd1c1d-372b-449b-a05a-64480ae46d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bacb6a-0861-4a30-a971-a64ea333fecc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64fd1c1d-372b-449b-a05a-64480ae46d65",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "6e1bc97b-a323-490a-8788-afdd0e5433ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "22cad170-6139-4114-a7c4-31e72e423f65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e1bc97b-a323-490a-8788-afdd0e5433ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d8c35c1-6cf1-4377-8861-745f74c8a2de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e1bc97b-a323-490a-8788-afdd0e5433ba",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "9ff33b33-7716-449c-bf36-cd4362f1b50e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "99768e23-032c-4cb5-b85f-d00fc18e0732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ff33b33-7716-449c-bf36-cd4362f1b50e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cba4950-40cc-4e3b-925b-e24f2260f4cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ff33b33-7716-449c-bf36-cd4362f1b50e",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "aebbb79f-4ae7-408b-b309-36fd56a0f598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "9a88d6fd-e0ee-43c5-b8c4-97d08b310451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aebbb79f-4ae7-408b-b309-36fd56a0f598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c9e076e-9c22-46a4-b12b-671c1ccdc970",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aebbb79f-4ae7-408b-b309-36fd56a0f598",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "03a7c8d6-644a-462f-8d31-bb0f3e61f7da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "fe2a4ae5-edb6-4461-81ac-4538993b49c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a7c8d6-644a-462f-8d31-bb0f3e61f7da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e37039-a55a-4400-9c73-c98e69e3921e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a7c8d6-644a-462f-8d31-bb0f3e61f7da",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "be392805-5cb1-4e4e-b556-8100a84f2051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "72a572ec-ed6e-484c-a868-f9da29bd08a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be392805-5cb1-4e4e-b556-8100a84f2051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2417ce77-9786-4bb8-884d-88af049d50fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be392805-5cb1-4e4e-b556-8100a84f2051",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "46d1082e-2b04-406f-98d5-abe7bac8aedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "f06d7a90-6396-49a6-9eda-f876d392734d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d1082e-2b04-406f-98d5-abe7bac8aedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ecef809-67da-4596-afbb-46b62592876f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d1082e-2b04-406f-98d5-abe7bac8aedf",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "512ffadb-d6ec-4380-aa47-f1a499c64195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "cf2a6920-e375-4403-a583-4bfa34b9a4fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "512ffadb-d6ec-4380-aa47-f1a499c64195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d0fb381-27e0-4cc0-99fd-7e218f4663de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512ffadb-d6ec-4380-aa47-f1a499c64195",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "39814c10-443d-4a41-8299-012b6ea1404a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "d88cc96f-ef80-4aaa-b7f0-fc836cffe83e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39814c10-443d-4a41-8299-012b6ea1404a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b44d4807-f8e9-43eb-8188-fcb2f6a1f7d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39814c10-443d-4a41-8299-012b6ea1404a",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "f5d9b11a-0312-4060-97a8-d6506357b6a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "18028bc4-060b-48a3-ae51-75b69b54bedf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d9b11a-0312-4060-97a8-d6506357b6a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220685f5-77e0-4b7d-bd04-cfd9e727abc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d9b11a-0312-4060-97a8-d6506357b6a8",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "e222770b-a39e-4943-83ea-36ab6b8ee628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "0f5c795b-543b-4fc8-b1bc-401e85abc240",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e222770b-a39e-4943-83ea-36ab6b8ee628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "806cf0e6-e520-4f09-ac16-b76f231e7868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e222770b-a39e-4943-83ea-36ab6b8ee628",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "bb2e15ad-d2c3-423e-8b97-8263b617ccc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "0c822d4e-c20b-4821-8fdc-16b35ff861e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2e15ad-d2c3-423e-8b97-8263b617ccc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b64d8efc-e395-4045-b7cf-c2e51ed28d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2e15ad-d2c3-423e-8b97-8263b617ccc8",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "fcf7483f-a4ba-4ea7-889a-c731e4bdce7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "0b1d7926-7cb9-49db-aee7-a4916f3c2f7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf7483f-a4ba-4ea7-889a-c731e4bdce7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8244077-8c51-4b98-af60-61705145b300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf7483f-a4ba-4ea7-889a-c731e4bdce7f",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "22fb37ef-954c-49b2-8dce-695f9244f072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "b5879d85-7f2f-4f4f-a07f-6955cfea6341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22fb37ef-954c-49b2-8dce-695f9244f072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0f01d1-ccdf-48ac-96f4-ad6047d8ffd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22fb37ef-954c-49b2-8dce-695f9244f072",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "702cec1d-95ac-4d16-81f0-720604545f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "26744e12-2bcf-413e-a40b-74c62c433cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "702cec1d-95ac-4d16-81f0-720604545f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7823557a-c951-4ec1-a06e-167f80965c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "702cec1d-95ac-4d16-81f0-720604545f6e",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "f2d19b9a-fab5-472b-a24b-bd81d20639f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "d5ed46f7-d576-42aa-bd0c-0aa4c3c64068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d19b9a-fab5-472b-a24b-bd81d20639f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fdffc05-710b-429b-a5d0-c4dbdc1e652e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d19b9a-fab5-472b-a24b-bd81d20639f1",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "d8e177f2-2d8f-4887-876d-5978cf151813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "4255e46b-97d5-42ce-81ad-f43d530b6966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e177f2-2d8f-4887-876d-5978cf151813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c610a7be-0306-4ccb-832a-2793f85384c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e177f2-2d8f-4887-876d-5978cf151813",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "00db3f82-4e21-434e-b6ea-c013f01ba615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "974795a0-1516-4556-a50a-843759ef1503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00db3f82-4e21-434e-b6ea-c013f01ba615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75475d86-1c43-4869-9f1a-2f2b50348267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00db3f82-4e21-434e-b6ea-c013f01ba615",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "339a8238-3b95-4f6b-a22d-6a3334df1a09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "c641dd79-a615-441b-8f44-1bea3d68314f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "339a8238-3b95-4f6b-a22d-6a3334df1a09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4103a63e-d1a0-4eff-ad49-818f711a6f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "339a8238-3b95-4f6b-a22d-6a3334df1a09",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "eb319b1b-d22d-45ab-8111-c52104169cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "78d5b286-d7cc-429c-b92b-3c794b924b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb319b1b-d22d-45ab-8111-c52104169cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19235a15-424d-43f9-968e-7484c6385650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb319b1b-d22d-45ab-8111-c52104169cfa",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "f451957e-b5e3-40cb-ba80-6119f45dc9fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "4f2785a9-7970-45b6-9fac-29a686d55b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f451957e-b5e3-40cb-ba80-6119f45dc9fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d56163b4-e779-4ab3-ab06-32e7bd242ad0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f451957e-b5e3-40cb-ba80-6119f45dc9fe",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "491a1bd4-eb10-46ab-89ec-eec852dea08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "f06f5e4f-a5c6-4537-8248-eb2e98d2f3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "491a1bd4-eb10-46ab-89ec-eec852dea08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e0a921-a0c4-45c6-91b7-a3a69fab6c01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "491a1bd4-eb10-46ab-89ec-eec852dea08e",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "24eb94c7-467e-4346-a58e-d13354434752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "53d2d108-60e9-47a0-a920-da44eb6dc35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24eb94c7-467e-4346-a58e-d13354434752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02fd886e-e88e-4609-8497-49bad3dba41d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24eb94c7-467e-4346-a58e-d13354434752",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "a719c431-6c85-4ae1-a8ff-f93bd37ca434",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "30e63386-2d01-442d-b188-8efbd2ba1508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a719c431-6c85-4ae1-a8ff-f93bd37ca434",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd820c2-b428-4be4-ac12-dc3133fc1e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a719c431-6c85-4ae1-a8ff-f93bd37ca434",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "b7cc4165-b9ca-4c66-899e-dee6326d3d46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "1ef2416f-732f-4ad3-b51b-382bf1d40d8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7cc4165-b9ca-4c66-899e-dee6326d3d46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18c3f17-5d6b-4f4b-8e5a-caed12eaed1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7cc4165-b9ca-4c66-899e-dee6326d3d46",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "cf3f20d4-e0ec-4df5-85e0-97248259636b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "0a06a5ff-b07c-4df8-9df6-9e79463e5b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf3f20d4-e0ec-4df5-85e0-97248259636b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30917f83-6c24-4b0f-bfbc-1138b5026936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf3f20d4-e0ec-4df5-85e0-97248259636b",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "008d0a13-5ace-4101-a9bd-5a83b4e037cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "3f4677f2-b79e-4ee0-b248-1e402573cbb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "008d0a13-5ace-4101-a9bd-5a83b4e037cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3eb7e69-4824-4029-8c7f-600fc5babdac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "008d0a13-5ace-4101-a9bd-5a83b4e037cb",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "206b87f5-bd30-4dc8-b8cd-e5830d939432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "5075e182-653e-402b-9d69-88b607817177",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206b87f5-bd30-4dc8-b8cd-e5830d939432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bef020e0-e113-49dd-b382-7bdcb2ab73bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206b87f5-bd30-4dc8-b8cd-e5830d939432",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "0de1db2c-494c-494a-b2b8-3bac53d8afcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "b5a9efd4-45ca-4160-97eb-96dedf95148f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de1db2c-494c-494a-b2b8-3bac53d8afcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e606d82-a651-49b0-bed9-93284284d617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de1db2c-494c-494a-b2b8-3bac53d8afcd",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "f1cb5f97-4ae4-45ff-a1ce-c2c416fc70da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "392509ed-2188-4306-b1eb-68e26ddff048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1cb5f97-4ae4-45ff-a1ce-c2c416fc70da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed4690e1-22b5-4924-826b-ec4494072166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1cb5f97-4ae4-45ff-a1ce-c2c416fc70da",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "0700b68c-a295-4f26-8be0-c343c72cfbff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "01e76e8d-3f00-43a7-87b6-0fa6617886e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0700b68c-a295-4f26-8be0-c343c72cfbff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ead7b5-c408-4cdc-9d84-11235c814f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0700b68c-a295-4f26-8be0-c343c72cfbff",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "76fbd5d9-9715-4b47-81df-d6e3dc623bb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "170fc617-e430-4a82-95ba-923eab918618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76fbd5d9-9715-4b47-81df-d6e3dc623bb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58292937-b892-4150-9c77-367c873ee6ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76fbd5d9-9715-4b47-81df-d6e3dc623bb9",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "71c25915-fa30-437a-9edb-46b9ceadb98e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "947b33db-0217-45d9-8503-90d0b3c061f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c25915-fa30-437a-9edb-46b9ceadb98e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890955ef-b715-4b03-aaa9-fd098260a514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c25915-fa30-437a-9edb-46b9ceadb98e",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "093e05d7-cfcd-411f-a625-57504ba8c5c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "49294738-d0ce-40fe-833b-44d7097b960e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "093e05d7-cfcd-411f-a625-57504ba8c5c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20708c21-68dd-4075-ac42-adf1c17551a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "093e05d7-cfcd-411f-a625-57504ba8c5c8",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        },
        {
            "id": "1e77ed36-cb5e-4bee-9b50-ca550f71f6bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "compositeImage": {
                "id": "38595d27-7fb7-4596-b171-80956e98c52e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e77ed36-cb5e-4bee-9b50-ca550f71f6bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed3c2c5-1d74-41af-9222-9c2596efe8ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e77ed36-cb5e-4bee-9b50-ca550f71f6bf",
                    "LayerId": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "b3e79e7f-37d6-44c2-9f0b-22aae28f590b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6da72d35-74ba-476f-ae45-511401529a5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}