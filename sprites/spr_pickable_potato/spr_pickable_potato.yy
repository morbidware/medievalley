{
    "id": "09dceea5-2186-44eb-bf66-6274c804a1bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potato",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c64dacf1-330c-49f9-940b-01fac1f2409c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09dceea5-2186-44eb-bf66-6274c804a1bb",
            "compositeImage": {
                "id": "e9dd5619-1a16-45d3-9ee4-72fa6d20316e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64dacf1-330c-49f9-940b-01fac1f2409c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9078e58b-33a5-4595-9074-3e3765e12118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64dacf1-330c-49f9-940b-01fac1f2409c",
                    "LayerId": "6fd90ed0-ad61-42c1-9454-c213e304fda6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6fd90ed0-ad61-42c1-9454-c213e304fda6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09dceea5-2186-44eb-bf66-6274c804a1bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}