{
    "id": "07b2e776-eaa2-4426-8998-47277e6b55cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_bonfire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "186fb6f9-a71f-44aa-8b75-a522dda5fef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07b2e776-eaa2-4426-8998-47277e6b55cc",
            "compositeImage": {
                "id": "36d5e50f-286e-4beb-89ed-442ce745042b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186fb6f9-a71f-44aa-8b75-a522dda5fef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4287f516-6e10-41ad-8503-c1126c0ac1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186fb6f9-a71f-44aa-8b75-a522dda5fef0",
                    "LayerId": "78b55386-fedc-4490-b7ad-81a5dce04853"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "78b55386-fedc-4490-b7ad-81a5dce04853",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07b2e776-eaa2-4426-8998-47277e6b55cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}