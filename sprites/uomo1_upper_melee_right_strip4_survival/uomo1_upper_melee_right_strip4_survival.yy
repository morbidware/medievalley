{
    "id": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_melee_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "272657e7-978b-4ff0-90df-33cdb8d49b3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "compositeImage": {
                "id": "653b550c-609f-4c19-b3cf-23baed78f5e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "272657e7-978b-4ff0-90df-33cdb8d49b3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2451b0d0-0cfc-4dc1-8366-c530cd13e981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "272657e7-978b-4ff0-90df-33cdb8d49b3a",
                    "LayerId": "a1b76ea4-f322-4067-a2d5-433f7d28057c"
                }
            ]
        },
        {
            "id": "ecc482a0-7f5f-4ebf-98ed-1d37c2cebd12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "compositeImage": {
                "id": "9e030c6a-c17e-40f9-9b0f-7de5c46731f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc482a0-7f5f-4ebf-98ed-1d37c2cebd12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9362b189-ef88-4885-9922-15d4a64630bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc482a0-7f5f-4ebf-98ed-1d37c2cebd12",
                    "LayerId": "a1b76ea4-f322-4067-a2d5-433f7d28057c"
                }
            ]
        },
        {
            "id": "5756798c-f34e-4810-9d58-ea3c9d627b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "compositeImage": {
                "id": "ff3a5c74-301f-46c0-81f4-cc11e5fda450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5756798c-f34e-4810-9d58-ea3c9d627b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64818608-e5df-4bd9-a42a-df88ab15041d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5756798c-f34e-4810-9d58-ea3c9d627b7d",
                    "LayerId": "a1b76ea4-f322-4067-a2d5-433f7d28057c"
                }
            ]
        },
        {
            "id": "3c1eb63a-c485-48a1-8cd5-e038089659b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "compositeImage": {
                "id": "222000d3-b858-4a4f-b508-975118bd9fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1eb63a-c485-48a1-8cd5-e038089659b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea82e0f0-d19e-4237-b32e-99686c5d0725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1eb63a-c485-48a1-8cd5-e038089659b4",
                    "LayerId": "a1b76ea4-f322-4067-a2d5-433f7d28057c"
                }
            ]
        },
        {
            "id": "d5bd2657-8cc6-44d8-8903-58ef5583adc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "compositeImage": {
                "id": "e05c1d5d-9f6d-4e8f-8873-1aec5f51bc19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5bd2657-8cc6-44d8-8903-58ef5583adc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5e4da5-9a26-4b60-bd8d-4603550736e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5bd2657-8cc6-44d8-8903-58ef5583adc7",
                    "LayerId": "a1b76ea4-f322-4067-a2d5-433f7d28057c"
                }
            ]
        },
        {
            "id": "7008b17a-f5cc-4a88-87b1-4e55d38852aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "compositeImage": {
                "id": "a6fb1dc9-7ec6-41e9-8254-fdfaefa97cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7008b17a-f5cc-4a88-87b1-4e55d38852aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9592cd2b-66d2-4746-9034-f6edd6401079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7008b17a-f5cc-4a88-87b1-4e55d38852aa",
                    "LayerId": "a1b76ea4-f322-4067-a2d5-433f7d28057c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a1b76ea4-f322-4067-a2d5-433f7d28057c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0e17fc1-bc6a-4e0f-ba04-47697917fd60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}