{
    "id": "8a4ed2a5-8507-46aa-b27f-48c834d25e32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fd9b9ea-39a7-41a0-a143-8a39627f20d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a4ed2a5-8507-46aa-b27f-48c834d25e32",
            "compositeImage": {
                "id": "ee3e9711-6f05-478d-9737-c48d15e4a9e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd9b9ea-39a7-41a0-a143-8a39627f20d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f97a71-a6b8-4b70-bf5d-0412452b5b97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd9b9ea-39a7-41a0-a143-8a39627f20d1",
                    "LayerId": "2c6d94dc-6cc5-446e-a87f-96419e447993"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "2c6d94dc-6cc5-446e-a87f-96419e447993",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a4ed2a5-8507-46aa-b27f-48c834d25e32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 1
}