{
    "id": "c8d52135-b9a5-411d-9698-0f924e616473",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_melee_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d687a51-2ee5-4f55-931c-13c0d20bc145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "compositeImage": {
                "id": "216b4955-999d-4e13-b405-6587486997f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d687a51-2ee5-4f55-931c-13c0d20bc145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6020af96-ff3c-4291-9696-28e2abcf3430",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d687a51-2ee5-4f55-931c-13c0d20bc145",
                    "LayerId": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7"
                }
            ]
        },
        {
            "id": "f3d12025-9cc7-4f5b-baaa-5ae29175ea40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "compositeImage": {
                "id": "710d23e2-0948-4697-b37b-454464bd5221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d12025-9cc7-4f5b-baaa-5ae29175ea40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f517ab2e-b3eb-4811-8c39-75090183c1cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d12025-9cc7-4f5b-baaa-5ae29175ea40",
                    "LayerId": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7"
                }
            ]
        },
        {
            "id": "8af5d30b-0efc-461b-9713-945a9fbc5b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "compositeImage": {
                "id": "0aad6ba8-19c8-497b-8b3f-8494da6b681c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af5d30b-0efc-461b-9713-945a9fbc5b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1eae88-0ffc-4abe-80fd-d7034148b238",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af5d30b-0efc-461b-9713-945a9fbc5b3f",
                    "LayerId": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7"
                }
            ]
        },
        {
            "id": "065a0799-73b7-40b3-8034-b634d9fa574c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "compositeImage": {
                "id": "f97239f6-41cf-4368-bc4e-6732183f4d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065a0799-73b7-40b3-8034-b634d9fa574c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "377330eb-f288-4ec6-b330-85c14b6163f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065a0799-73b7-40b3-8034-b634d9fa574c",
                    "LayerId": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7"
                }
            ]
        },
        {
            "id": "f281f684-b45b-4716-8897-e70bfccd428e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "compositeImage": {
                "id": "38877de5-f91d-400d-a230-b2b19caadaff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f281f684-b45b-4716-8897-e70bfccd428e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74eb423c-25e4-48a2-baf1-7920da094cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f281f684-b45b-4716-8897-e70bfccd428e",
                    "LayerId": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7"
                }
            ]
        },
        {
            "id": "83a54448-db18-4301-a006-4c2b87d653b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "compositeImage": {
                "id": "573f5bf9-9c34-40c7-a5b0-a91b9787f21a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83a54448-db18-4301-a006-4c2b87d653b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a3f7b2-1191-47b4-b3a3-12649a75ef55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83a54448-db18-4301-a006-4c2b87d653b6",
                    "LayerId": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ece496f-ea28-4b1d-be2b-44bf8c0a91d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8d52135-b9a5-411d-9698-0f924e616473",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}