{
    "id": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_pick_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f504a41-9b3f-48ad-ad9a-7422d667f774",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "compositeImage": {
                "id": "ed7ab2d7-7228-450e-94eb-231ad864d7fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f504a41-9b3f-48ad-ad9a-7422d667f774",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2dca21-8d2e-4a7d-b557-1b8447192138",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f504a41-9b3f-48ad-ad9a-7422d667f774",
                    "LayerId": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3"
                }
            ]
        },
        {
            "id": "b9f47d5d-f5cc-4139-baae-ffaaf16462a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "compositeImage": {
                "id": "6c9a284c-db49-48c6-a89c-94d66f7d207b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f47d5d-f5cc-4139-baae-ffaaf16462a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d2d206e-b729-4de6-866a-87129fd2caf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f47d5d-f5cc-4139-baae-ffaaf16462a7",
                    "LayerId": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3"
                }
            ]
        },
        {
            "id": "a79fb71e-ba78-450a-b487-bd73471fecaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "compositeImage": {
                "id": "8eb385f9-f3de-416f-aae7-c58ceb29f22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79fb71e-ba78-450a-b487-bd73471fecaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9abcd7c-4ab5-4610-8806-735588fa0917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79fb71e-ba78-450a-b487-bd73471fecaa",
                    "LayerId": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3"
                }
            ]
        },
        {
            "id": "dcbbd00f-ea05-4de9-86a6-3c23e8925987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "compositeImage": {
                "id": "fd88dc9b-567e-4e89-a348-c451bff65fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcbbd00f-ea05-4de9-86a6-3c23e8925987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e0202c-8f42-4213-a9b1-06d352bb6ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcbbd00f-ea05-4de9-86a6-3c23e8925987",
                    "LayerId": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3"
                }
            ]
        },
        {
            "id": "1082b5b3-605a-4709-b86b-1fdfbe54c1be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "compositeImage": {
                "id": "6533ed0a-2a9a-4736-9edb-e6481d20cef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1082b5b3-605a-4709-b86b-1fdfbe54c1be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09f1db6-715e-4ec3-9cd3-537ada374dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1082b5b3-605a-4709-b86b-1fdfbe54c1be",
                    "LayerId": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3"
                }
            ]
        },
        {
            "id": "9184ac53-045d-43bc-b227-f302910c71ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "compositeImage": {
                "id": "5c92afd8-7d5a-49b0-af37-cf2c33412df5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9184ac53-045d-43bc-b227-f302910c71ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bf9e9f-0c45-4474-8820-ba01193931a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9184ac53-045d-43bc-b227-f302910c71ba",
                    "LayerId": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8c88589d-79b1-4daa-8fd7-a2d4f9e873f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1baaaf6-176b-4e38-acac-a6db6bae860b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}