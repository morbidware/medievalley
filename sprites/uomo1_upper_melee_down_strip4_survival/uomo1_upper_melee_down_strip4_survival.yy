{
    "id": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_melee_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fb3bf5f-cde8-4eb6-a137-e5241abccf69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "compositeImage": {
                "id": "75c7b63b-cf5a-4b8c-8ef3-5eeaed05297b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fb3bf5f-cde8-4eb6-a137-e5241abccf69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a01b416c-6a4d-41c0-8fef-80533588f077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fb3bf5f-cde8-4eb6-a137-e5241abccf69",
                    "LayerId": "20c88c9b-9dae-40df-8354-92c4f458a8e2"
                }
            ]
        },
        {
            "id": "494809de-4700-4255-ace2-f4ad7ca03a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "compositeImage": {
                "id": "b8070d84-9f82-4e0f-a80d-0cc3f89d81b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "494809de-4700-4255-ace2-f4ad7ca03a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "140bc047-6162-4419-bf1f-df2e6dd3ebda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "494809de-4700-4255-ace2-f4ad7ca03a5b",
                    "LayerId": "20c88c9b-9dae-40df-8354-92c4f458a8e2"
                }
            ]
        },
        {
            "id": "2df3596b-35b0-485a-8cc0-afcfe75d402f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "compositeImage": {
                "id": "bfd4e565-ef23-4381-8405-3fb0e4f877dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2df3596b-35b0-485a-8cc0-afcfe75d402f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0654cc0f-dc14-4268-b66b-9429ca48ab6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2df3596b-35b0-485a-8cc0-afcfe75d402f",
                    "LayerId": "20c88c9b-9dae-40df-8354-92c4f458a8e2"
                }
            ]
        },
        {
            "id": "f6c96c9c-951b-42f4-8ccf-633bb748f163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "compositeImage": {
                "id": "faecbae7-2998-4689-843c-5062614e9fa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6c96c9c-951b-42f4-8ccf-633bb748f163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8e4cc09-5338-4e23-ba82-ba26d708f0cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6c96c9c-951b-42f4-8ccf-633bb748f163",
                    "LayerId": "20c88c9b-9dae-40df-8354-92c4f458a8e2"
                }
            ]
        },
        {
            "id": "9f46f1b5-04a7-4ebd-af49-6c705806dbde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "compositeImage": {
                "id": "b2c1d75c-dce7-4f56-b5d3-f88aef278a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f46f1b5-04a7-4ebd-af49-6c705806dbde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "246e65ae-e562-4eef-bc52-44b3c2068aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f46f1b5-04a7-4ebd-af49-6c705806dbde",
                    "LayerId": "20c88c9b-9dae-40df-8354-92c4f458a8e2"
                }
            ]
        },
        {
            "id": "d076a77e-72e8-4b8b-815d-d2388d78cf23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "compositeImage": {
                "id": "735b89e8-9d0d-4f2f-b573-744f361c36c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d076a77e-72e8-4b8b-815d-d2388d78cf23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d553cb75-cd1f-47c1-a1e7-a522ad8d709f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d076a77e-72e8-4b8b-815d-d2388d78cf23",
                    "LayerId": "20c88c9b-9dae-40df-8354-92c4f458a8e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "20c88c9b-9dae-40df-8354-92c4f458a8e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f3fcb8a-4afc-4cbf-8a69-4f8019635cbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}