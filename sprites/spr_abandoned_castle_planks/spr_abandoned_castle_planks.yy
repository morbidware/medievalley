{
    "id": "2d9bf76e-a91a-4ecc-8382-21750937d8bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abandoned_castle_planks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14c669a8-7a79-4f60-8317-68382c45d89a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d9bf76e-a91a-4ecc-8382-21750937d8bc",
            "compositeImage": {
                "id": "5aad9bff-c6be-4205-803a-e2e09f3e12c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14c669a8-7a79-4f60-8317-68382c45d89a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd5ca677-a919-4ddf-97e0-e5b8ee27686a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14c669a8-7a79-4f60-8317-68382c45d89a",
                    "LayerId": "b63d0b11-2a9f-4652-92d2-fef587521c0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "b63d0b11-2a9f-4652-92d2-fef587521c0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d9bf76e-a91a-4ecc-8382-21750937d8bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 35
}