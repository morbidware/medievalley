{
    "id": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_refinedstone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "273ec5f0-bd89-4040-9efc-bb0affe60627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "compositeImage": {
                "id": "90fa1cc0-521b-4636-92dc-5c96fde3069a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "273ec5f0-bd89-4040-9efc-bb0affe60627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "752fb8d4-32c0-4bcd-9c3d-c012d97dc9d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "273ec5f0-bd89-4040-9efc-bb0affe60627",
                    "LayerId": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce"
                }
            ]
        },
        {
            "id": "ec6f7646-a47d-4c65-88d0-7ea4c7fc880f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "compositeImage": {
                "id": "54d2a479-d373-4af2-aaab-1f5f39c3f2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6f7646-a47d-4c65-88d0-7ea4c7fc880f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aeb8384-9267-406c-aa68-27e0f8a9cdad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6f7646-a47d-4c65-88d0-7ea4c7fc880f",
                    "LayerId": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce"
                }
            ]
        },
        {
            "id": "47a666a4-a64d-4bd4-883a-6d48b8ced814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "compositeImage": {
                "id": "c33c3569-fbc5-49cb-8e48-f2e0fd2b6f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47a666a4-a64d-4bd4-883a-6d48b8ced814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29eccfe7-240f-4c57-86e6-a533d7481a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47a666a4-a64d-4bd4-883a-6d48b8ced814",
                    "LayerId": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce"
                }
            ]
        },
        {
            "id": "b3d41321-4822-49fd-870c-8cc3972e56fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "compositeImage": {
                "id": "1d2a4701-a034-4cd2-a3c1-bc540f705d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d41321-4822-49fd-870c-8cc3972e56fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c2adea-831d-450c-a524-3de541a52047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d41321-4822-49fd-870c-8cc3972e56fc",
                    "LayerId": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce"
                }
            ]
        },
        {
            "id": "dce70f02-49d8-4cd0-bc17-6020afb7f99f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "compositeImage": {
                "id": "fa1e3800-85d5-4be6-8b4d-e94ed832738f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce70f02-49d8-4cd0-bc17-6020afb7f99f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32366d37-6c33-4222-9f85-dd95a30a8f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce70f02-49d8-4cd0-bc17-6020afb7f99f",
                    "LayerId": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce"
                }
            ]
        },
        {
            "id": "11569edb-d469-4957-a069-9343bc5e44f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "compositeImage": {
                "id": "29952e20-8c8b-43cf-8a42-16360d7a82c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11569edb-d469-4957-a069-9343bc5e44f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c662815b-83e0-4cfd-bce8-4273b11e19cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11569edb-d469-4957-a069-9343bc5e44f9",
                    "LayerId": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4d439d7c-c5db-4cc0-9b7b-7be5f9ea03ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c11f7e69-06a9-4ddd-a590-9f9bb24254c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}