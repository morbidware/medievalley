{
    "id": "6cdd1130-9b66-4f22-8d11-d346feec20a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_onion_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a178961-38f1-4607-a115-3659011531a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cdd1130-9b66-4f22-8d11-d346feec20a9",
            "compositeImage": {
                "id": "d86b8b34-a89a-4182-ad7f-17f7486fff8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a178961-38f1-4607-a115-3659011531a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "790c0716-124a-4b19-9e7c-7af0145c7c81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a178961-38f1-4607-a115-3659011531a8",
                    "LayerId": "bd515689-87c9-405d-bd77-18df0031e88e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bd515689-87c9-405d-bd77-18df0031e88e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cdd1130-9b66-4f22-8d11-d346feec20a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}