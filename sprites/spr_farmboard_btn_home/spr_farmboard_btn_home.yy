{
    "id": "92f04b8b-01ac-496f-b745-cc97f996ce1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_btn_home",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aef09228-40b4-4090-9158-335fbeacd224",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92f04b8b-01ac-496f-b745-cc97f996ce1c",
            "compositeImage": {
                "id": "2ed1fab5-3d5b-43ff-b219-8117feee01ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef09228-40b4-4090-9158-335fbeacd224",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c7a988a-737b-4f35-a166-e307f94f4d92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef09228-40b4-4090-9158-335fbeacd224",
                    "LayerId": "58468f5e-ee98-47a8-b5a0-de6448a40492"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "58468f5e-ee98-47a8-b5a0-de6448a40492",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92f04b8b-01ac-496f-b745-cc97f996ce1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}