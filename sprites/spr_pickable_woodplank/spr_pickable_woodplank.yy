{
    "id": "877785e7-73f0-4fa6-933c-21d23c8ce73b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_woodplank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10cb1131-5c9e-4957-ba37-0f200b9795b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "877785e7-73f0-4fa6-933c-21d23c8ce73b",
            "compositeImage": {
                "id": "a5d8ac4f-32da-49b4-a7b1-6fce2993cb24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10cb1131-5c9e-4957-ba37-0f200b9795b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af23db12-0e39-4116-96bf-8304d7177ba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10cb1131-5c9e-4957-ba37-0f200b9795b7",
                    "LayerId": "8fb01724-e31c-4734-9a1c-d39dedbd28ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8fb01724-e31c-4734-9a1c-d39dedbd28ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "877785e7-73f0-4fa6-933c-21d23c8ce73b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}