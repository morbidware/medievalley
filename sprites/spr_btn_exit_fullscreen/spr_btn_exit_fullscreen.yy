{
    "id": "b81cc68b-6205-4ddf-b819-72302d97db99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_exit_fullscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "def98867-d21e-469a-add7-e70b8fa7b9db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81cc68b-6205-4ddf-b819-72302d97db99",
            "compositeImage": {
                "id": "31fd5c71-fa0e-4251-86a0-dcb62464ac40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def98867-d21e-469a-add7-e70b8fa7b9db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9b2ad5e-7306-42e9-878d-1efee6853111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def98867-d21e-469a-add7-e70b8fa7b9db",
                    "LayerId": "aec00134-4818-4cf6-b869-960db25bbac9"
                }
            ]
        },
        {
            "id": "bab3a557-7ed9-4c32-a8a3-736f5fcbc2e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81cc68b-6205-4ddf-b819-72302d97db99",
            "compositeImage": {
                "id": "d98ca429-bb43-40cc-89ba-188d7a36fca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bab3a557-7ed9-4c32-a8a3-736f5fcbc2e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472f0928-f196-4985-bec6-ac147dbee4ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bab3a557-7ed9-4c32-a8a3-736f5fcbc2e6",
                    "LayerId": "aec00134-4818-4cf6-b869-960db25bbac9"
                }
            ]
        },
        {
            "id": "53cf6603-d3a5-47e9-9bf7-eb1348359b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81cc68b-6205-4ddf-b819-72302d97db99",
            "compositeImage": {
                "id": "98c82f4d-c843-457e-b1b6-5c14992512a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53cf6603-d3a5-47e9-9bf7-eb1348359b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed841d9-4999-4603-90f1-fe5af0015511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53cf6603-d3a5-47e9-9bf7-eb1348359b38",
                    "LayerId": "aec00134-4818-4cf6-b869-960db25bbac9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "aec00134-4818-4cf6-b869-960db25bbac9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b81cc68b-6205-4ddf-b819-72302d97db99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 31,
    "yorig": 4
}