{
    "id": "e09435f7-4227-4e77-9e94-786315f5d9ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ba20d2a-eff9-4f89-b8ce-b029a8f8d72d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e09435f7-4227-4e77-9e94-786315f5d9ba",
            "compositeImage": {
                "id": "ce08c280-8699-4e2a-8702-92229f3d247e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ba20d2a-eff9-4f89-b8ce-b029a8f8d72d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4881b4fb-dfd9-40ba-b403-b96f56ea5e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ba20d2a-eff9-4f89-b8ce-b029a8f8d72d",
                    "LayerId": "4590b708-6d50-46e9-8a50-0f8113b158a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4590b708-6d50-46e9-8a50-0f8113b158a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e09435f7-4227-4e77-9e94-786315f5d9ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}