{
    "id": "183f3bf3-9fd0-4c7d-bb81-e90851578cb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e12cdfc-8f73-4f25-917b-90ac4eaf5cf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183f3bf3-9fd0-4c7d-bb81-e90851578cb0",
            "compositeImage": {
                "id": "77b3f596-9ee2-4cfa-9d78-01288a37953b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e12cdfc-8f73-4f25-917b-90ac4eaf5cf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3617a3ee-ad10-4721-9b5c-9dd9cf639a0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e12cdfc-8f73-4f25-917b-90ac4eaf5cf5",
                    "LayerId": "38b0d0ea-535c-4313-ac3d-87614027b2ea"
                }
            ]
        },
        {
            "id": "1e2d148f-2bcf-44a5-8cfc-84c7809fd00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183f3bf3-9fd0-4c7d-bb81-e90851578cb0",
            "compositeImage": {
                "id": "40fbfa59-8c44-42ac-8d1e-f90aa30b95a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2d148f-2bcf-44a5-8cfc-84c7809fd00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30f3cbe1-6e64-41a7-aae5-6dcfb1b279c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2d148f-2bcf-44a5-8cfc-84c7809fd00c",
                    "LayerId": "38b0d0ea-535c-4313-ac3d-87614027b2ea"
                }
            ]
        },
        {
            "id": "8556a90c-6033-4e44-a8e5-25cd7a584e84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183f3bf3-9fd0-4c7d-bb81-e90851578cb0",
            "compositeImage": {
                "id": "32d53264-9fb9-44ef-8619-f58bee68e60f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8556a90c-6033-4e44-a8e5-25cd7a584e84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a721f609-3d9e-4835-864c-3ade6764d630",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8556a90c-6033-4e44-a8e5-25cd7a584e84",
                    "LayerId": "38b0d0ea-535c-4313-ac3d-87614027b2ea"
                }
            ]
        },
        {
            "id": "29363adf-18f5-41b9-8992-0ee6f95ca520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183f3bf3-9fd0-4c7d-bb81-e90851578cb0",
            "compositeImage": {
                "id": "b9429cc9-e140-4e4f-ad2d-152539dc5cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29363adf-18f5-41b9-8992-0ee6f95ca520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2b9be5f-b408-431a-a0ab-8458e47dd6b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29363adf-18f5-41b9-8992-0ee6f95ca520",
                    "LayerId": "38b0d0ea-535c-4313-ac3d-87614027b2ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "38b0d0ea-535c-4313-ac3d-87614027b2ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "183f3bf3-9fd0-4c7d-bb81-e90851578cb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}