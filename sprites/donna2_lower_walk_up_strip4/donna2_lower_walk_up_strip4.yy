{
    "id": "132c3d97-cfec-4122-aaea-840265a335fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "367a22e8-bcd5-4c9f-a457-943028d8c2a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "132c3d97-cfec-4122-aaea-840265a335fd",
            "compositeImage": {
                "id": "7baef793-cd1a-472c-830b-97a7e9bf0f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "367a22e8-bcd5-4c9f-a457-943028d8c2a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46ea832b-106e-4333-ad74-50b22263c5b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "367a22e8-bcd5-4c9f-a457-943028d8c2a0",
                    "LayerId": "63560c8c-1ac4-48dd-96a5-775bb35d3dcd"
                }
            ]
        },
        {
            "id": "a0cce5af-8924-4593-a99b-25be05bacabd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "132c3d97-cfec-4122-aaea-840265a335fd",
            "compositeImage": {
                "id": "0bfd7bf8-1052-4a04-9e31-8542f2c2feaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0cce5af-8924-4593-a99b-25be05bacabd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a316ca2e-578a-4691-883b-a26ebf66dc33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0cce5af-8924-4593-a99b-25be05bacabd",
                    "LayerId": "63560c8c-1ac4-48dd-96a5-775bb35d3dcd"
                }
            ]
        },
        {
            "id": "a6e8070d-b213-4494-b45b-f2919472e608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "132c3d97-cfec-4122-aaea-840265a335fd",
            "compositeImage": {
                "id": "3aaf95cc-5add-4640-a5bf-4ea0ccd23ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6e8070d-b213-4494-b45b-f2919472e608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad964e7-5b6d-4a29-bc5e-553f211006d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6e8070d-b213-4494-b45b-f2919472e608",
                    "LayerId": "63560c8c-1ac4-48dd-96a5-775bb35d3dcd"
                }
            ]
        },
        {
            "id": "50e0eef0-bad7-4e9d-9519-9934cb34d3a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "132c3d97-cfec-4122-aaea-840265a335fd",
            "compositeImage": {
                "id": "512055e3-95b1-4ec3-8ce7-03859e01cd94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50e0eef0-bad7-4e9d-9519-9934cb34d3a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61a93302-c6e7-4599-b8e8-8ae5e4474c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50e0eef0-bad7-4e9d-9519-9934cb34d3a1",
                    "LayerId": "63560c8c-1ac4-48dd-96a5-775bb35d3dcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "63560c8c-1ac4-48dd-96a5-775bb35d3dcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "132c3d97-cfec-4122-aaea-840265a335fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}