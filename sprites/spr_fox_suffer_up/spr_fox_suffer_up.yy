{
    "id": "2cd5aefd-1e7c-4707-b69a-c56974157800",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_suffer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a2d3ad5-6b36-46cd-8b66-c6e32b4cae7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cd5aefd-1e7c-4707-b69a-c56974157800",
            "compositeImage": {
                "id": "d075bebe-6900-485c-a396-eaab479524e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2d3ad5-6b36-46cd-8b66-c6e32b4cae7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb5f386-74c1-4b53-9df5-a3237e1281af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2d3ad5-6b36-46cd-8b66-c6e32b4cae7b",
                    "LayerId": "9a21c73d-3562-4d17-b2d6-c4670a109dc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "9a21c73d-3562-4d17-b2d6-c4670a109dc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cd5aefd-1e7c-4707-b69a-c56974157800",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}