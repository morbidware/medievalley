{
    "id": "4b7be40f-2fd5-4144-96a3-94c52ce64e62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2694ded-d397-4166-a43b-dc1ed086a8b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b7be40f-2fd5-4144-96a3-94c52ce64e62",
            "compositeImage": {
                "id": "a7a567be-96ae-426c-b91c-3f4a91cf8f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2694ded-d397-4166-a43b-dc1ed086a8b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5687a0-72e9-4aa0-a508-e6fe91e35286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2694ded-d397-4166-a43b-dc1ed086a8b4",
                    "LayerId": "511de5d1-9672-4269-ab33-db068a016a32"
                }
            ]
        },
        {
            "id": "9078b96b-7f5f-4fad-add4-bfc6f78d31f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b7be40f-2fd5-4144-96a3-94c52ce64e62",
            "compositeImage": {
                "id": "cb78786c-3df0-4333-8a4f-3fef18e43c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9078b96b-7f5f-4fad-add4-bfc6f78d31f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a8fa8b-2203-4bc2-acfd-d30292fd85fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9078b96b-7f5f-4fad-add4-bfc6f78d31f3",
                    "LayerId": "511de5d1-9672-4269-ab33-db068a016a32"
                }
            ]
        },
        {
            "id": "d08e867e-27a4-46fb-ba89-2e52c0d08520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b7be40f-2fd5-4144-96a3-94c52ce64e62",
            "compositeImage": {
                "id": "5e3c717a-982f-4306-9d68-ac95b1eb6f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d08e867e-27a4-46fb-ba89-2e52c0d08520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9500326b-de7c-4978-a96b-5269325e4237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d08e867e-27a4-46fb-ba89-2e52c0d08520",
                    "LayerId": "511de5d1-9672-4269-ab33-db068a016a32"
                }
            ]
        },
        {
            "id": "106155fb-4b55-4a62-965b-a88c56a5b23d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b7be40f-2fd5-4144-96a3-94c52ce64e62",
            "compositeImage": {
                "id": "71c1bcfc-1282-4a1b-ab2b-1fc5322e0dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "106155fb-4b55-4a62-965b-a88c56a5b23d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d59fb3e0-1c42-4ca8-8e36-1797d1b390f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "106155fb-4b55-4a62-965b-a88c56a5b23d",
                    "LayerId": "511de5d1-9672-4269-ab33-db068a016a32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "511de5d1-9672-4269-ab33-db068a016a32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b7be40f-2fd5-4144-96a3-94c52ce64e62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}