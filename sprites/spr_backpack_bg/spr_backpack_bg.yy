{
    "id": "0e2ce498-7bb6-4391-bc61-6b5ff53271f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backpack_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 110,
    "bbox_left": 0,
    "bbox_right": 156,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef0bf77b-14e5-4599-ade6-1efb9a8e7d04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2ce498-7bb6-4391-bc61-6b5ff53271f4",
            "compositeImage": {
                "id": "c457dfd7-b1b8-4249-abc9-0e48e40a7e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0bf77b-14e5-4599-ade6-1efb9a8e7d04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76059f7f-b215-4aaa-9287-d3fcfd925cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0bf77b-14e5-4599-ade6-1efb9a8e7d04",
                    "LayerId": "b464ebde-ccd9-426e-80c4-0894ae24e9ca"
                }
            ]
        }
    ],
    "gridX": 156,
    "gridY": 115,
    "height": 111,
    "layers": [
        {
            "id": "b464ebde-ccd9-426e-80c4-0894ae24e9ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e2ce498-7bb6-4391-bc61-6b5ff53271f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 157,
    "xorig": 78,
    "yorig": 55
}