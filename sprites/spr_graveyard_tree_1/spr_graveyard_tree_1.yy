{
    "id": "e70c5578-1a62-4bce-a756-b18b1ac6a6bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tree_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d89cf28-95ef-4a77-919c-74622079a336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e70c5578-1a62-4bce-a756-b18b1ac6a6bd",
            "compositeImage": {
                "id": "0f59766c-a558-408a-a332-2bf7728d9650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d89cf28-95ef-4a77-919c-74622079a336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b34c3ed-237c-4dc0-af83-c9931c1a2ab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d89cf28-95ef-4a77-919c-74622079a336",
                    "LayerId": "6211a828-c6df-4977-a96f-d2f8e6e2c305"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "6211a828-c6df-4977-a96f-d2f8e6e2c305",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e70c5578-1a62-4bce-a756-b18b1ac6a6bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 14,
    "yorig": 45
}