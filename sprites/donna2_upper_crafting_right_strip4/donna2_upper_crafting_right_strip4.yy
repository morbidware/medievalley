{
    "id": "f4c6c924-d2f0-45bd-890e-96029fe4b793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cec70334-5345-4d50-8e3c-23e1e1411693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c6c924-d2f0-45bd-890e-96029fe4b793",
            "compositeImage": {
                "id": "943bb39b-e8a6-4b53-bc61-c79509f3edba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec70334-5345-4d50-8e3c-23e1e1411693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69de4b3c-0c18-4e3b-86e3-c59ec309707f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec70334-5345-4d50-8e3c-23e1e1411693",
                    "LayerId": "1b7842de-c51e-4d0e-be34-35e3692dd6c9"
                }
            ]
        },
        {
            "id": "7d85c207-b31b-4e2d-9937-5d1746f7386f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c6c924-d2f0-45bd-890e-96029fe4b793",
            "compositeImage": {
                "id": "3799fcad-a574-4d3e-a10a-a0b3b487ae9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d85c207-b31b-4e2d-9937-5d1746f7386f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3952d26-9b92-4607-bb7f-3057a98c9d7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d85c207-b31b-4e2d-9937-5d1746f7386f",
                    "LayerId": "1b7842de-c51e-4d0e-be34-35e3692dd6c9"
                }
            ]
        },
        {
            "id": "70847514-ee4d-4256-bdb5-8913e41d7283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c6c924-d2f0-45bd-890e-96029fe4b793",
            "compositeImage": {
                "id": "8b148721-b5a0-4926-886a-4e4438264767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70847514-ee4d-4256-bdb5-8913e41d7283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01a3fc86-13d8-4be0-93e0-718d92873a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70847514-ee4d-4256-bdb5-8913e41d7283",
                    "LayerId": "1b7842de-c51e-4d0e-be34-35e3692dd6c9"
                }
            ]
        },
        {
            "id": "9a662f6d-422e-4ac1-a5df-6c7cac2aa4a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4c6c924-d2f0-45bd-890e-96029fe4b793",
            "compositeImage": {
                "id": "da982eda-d75c-4afa-bae0-3d9e101e8fdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a662f6d-422e-4ac1-a5df-6c7cac2aa4a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df55e7d9-ca4f-4567-b34d-8d3c2a09b018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a662f6d-422e-4ac1-a5df-6c7cac2aa4a1",
                    "LayerId": "1b7842de-c51e-4d0e-be34-35e3692dd6c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1b7842de-c51e-4d0e-be34-35e3692dd6c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4c6c924-d2f0-45bd-890e-96029fe4b793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}