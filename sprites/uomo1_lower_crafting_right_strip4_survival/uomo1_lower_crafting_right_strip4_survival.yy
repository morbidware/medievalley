{
    "id": "35df332f-0244-4ce4-ba71-edeaf77cc377",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_crafting_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5961610e-8de0-400c-bace-8ec48e1de620",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "compositeImage": {
                "id": "d3a9e0ce-3a17-4944-a620-b8d0394c87b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5961610e-8de0-400c-bace-8ec48e1de620",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "499eef61-17e9-4fa0-ab3d-fc98aeba780e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5961610e-8de0-400c-bace-8ec48e1de620",
                    "LayerId": "0d7c8aaa-f853-4361-85d6-16021299df95"
                }
            ]
        },
        {
            "id": "f4bc96eb-4977-45fa-824c-af0238de35a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "compositeImage": {
                "id": "4dfbd971-7046-4cd3-a821-5321eeae626f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4bc96eb-4977-45fa-824c-af0238de35a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20deae7a-45b8-4e9c-92ea-99204f16054b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4bc96eb-4977-45fa-824c-af0238de35a2",
                    "LayerId": "0d7c8aaa-f853-4361-85d6-16021299df95"
                }
            ]
        },
        {
            "id": "f687140f-a184-4010-b0bb-abf331944152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "compositeImage": {
                "id": "de393a92-412d-429a-a205-f29d248ff170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f687140f-a184-4010-b0bb-abf331944152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8b48ba-1cde-4a32-b457-2a9f560a2487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f687140f-a184-4010-b0bb-abf331944152",
                    "LayerId": "0d7c8aaa-f853-4361-85d6-16021299df95"
                }
            ]
        },
        {
            "id": "86c7a593-8947-454b-9e05-5381a1a5c179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "compositeImage": {
                "id": "def43663-d323-4f76-80d2-5b7bbc8f048c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86c7a593-8947-454b-9e05-5381a1a5c179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa2c76e7-51fc-4955-8adc-3fb72c89f10c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86c7a593-8947-454b-9e05-5381a1a5c179",
                    "LayerId": "0d7c8aaa-f853-4361-85d6-16021299df95"
                }
            ]
        },
        {
            "id": "22596af8-463f-4f5c-908b-99ec2f0b7daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "compositeImage": {
                "id": "bb3c6e15-2441-4fb9-b9ca-956b5291c5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22596af8-463f-4f5c-908b-99ec2f0b7daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b36eea70-0340-4949-92ce-2f234988de6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22596af8-463f-4f5c-908b-99ec2f0b7daf",
                    "LayerId": "0d7c8aaa-f853-4361-85d6-16021299df95"
                }
            ]
        },
        {
            "id": "75bbfb63-d084-4bf4-a46c-38d80001c452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "compositeImage": {
                "id": "735a2b37-b253-4ed4-829d-74fee8a53c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75bbfb63-d084-4bf4-a46c-38d80001c452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a1dee6-1f52-4372-98a7-a6a312f62d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75bbfb63-d084-4bf4-a46c-38d80001c452",
                    "LayerId": "0d7c8aaa-f853-4361-85d6-16021299df95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d7c8aaa-f853-4361-85d6-16021299df95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35df332f-0244-4ce4-ba71-edeaf77cc377",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}