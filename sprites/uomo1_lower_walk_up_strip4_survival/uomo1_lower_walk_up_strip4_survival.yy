{
    "id": "a838d842-8dd5-400c-ab11-ed6f09562830",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_walk_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f827cf8a-0494-426d-9433-99b0082134f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "compositeImage": {
                "id": "3307ff51-4d62-4edc-a5b4-c9e2b057febb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f827cf8a-0494-426d-9433-99b0082134f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07597e20-076c-4435-9b85-6159e0ad3575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f827cf8a-0494-426d-9433-99b0082134f0",
                    "LayerId": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4"
                }
            ]
        },
        {
            "id": "44024d5b-64e7-4d0a-b764-72cd924fdae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "compositeImage": {
                "id": "62a1be50-2ca5-43ff-ba4a-94a13326d1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44024d5b-64e7-4d0a-b764-72cd924fdae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ec0533-0921-45d1-b95c-0903f9e5c002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44024d5b-64e7-4d0a-b764-72cd924fdae2",
                    "LayerId": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4"
                }
            ]
        },
        {
            "id": "8b3612cc-96e2-459a-a46e-762e7db2a58f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "compositeImage": {
                "id": "90cad54e-9cc4-4c0e-b08d-664818f7f56d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3612cc-96e2-459a-a46e-762e7db2a58f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e619ae8e-ee5f-43c3-a641-e56f831f5221",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3612cc-96e2-459a-a46e-762e7db2a58f",
                    "LayerId": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4"
                }
            ]
        },
        {
            "id": "4515f5d6-0ccd-4482-8025-c9b5f1c0abda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "compositeImage": {
                "id": "60830f5d-2656-4217-b7e1-090b1cbf448e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4515f5d6-0ccd-4482-8025-c9b5f1c0abda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76475960-d086-4066-9bbb-ec9480c8d8de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4515f5d6-0ccd-4482-8025-c9b5f1c0abda",
                    "LayerId": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4"
                }
            ]
        },
        {
            "id": "5be55687-23e0-452d-bf8d-adde0eecfef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "compositeImage": {
                "id": "2fd189a6-e50a-4d11-9af4-afb5fea9e406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be55687-23e0-452d-bf8d-adde0eecfef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67545590-03f1-4b6b-8892-0ecac5e4c784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be55687-23e0-452d-bf8d-adde0eecfef5",
                    "LayerId": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4"
                }
            ]
        },
        {
            "id": "94d1c30d-c4d6-45e7-a229-441a5a757d37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "compositeImage": {
                "id": "c4136cc4-f536-420a-ac36-3bb66757897e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d1c30d-c4d6-45e7-a229-441a5a757d37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f40bcc-ab65-40a2-af9b-e1b9d18e4639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d1c30d-c4d6-45e7-a229-441a5a757d37",
                    "LayerId": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e0fba40a-53aa-412c-89ab-9ca2a8a18ba4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a838d842-8dd5-400c-ab11-ed6f09562830",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}