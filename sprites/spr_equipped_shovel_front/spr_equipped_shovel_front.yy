{
    "id": "51c90226-2b48-4353-978a-186a3f6d518b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_shovel_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "101959c6-72fe-4cea-8309-74b93ba667e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51c90226-2b48-4353-978a-186a3f6d518b",
            "compositeImage": {
                "id": "51c5c407-5d31-495a-86bb-769b551e64b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101959c6-72fe-4cea-8309-74b93ba667e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd6268dd-c722-46e7-ae17-2791bacdf4a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101959c6-72fe-4cea-8309-74b93ba667e9",
                    "LayerId": "37ed4c48-93fd-4a53-96eb-3b71712620e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "37ed4c48-93fd-4a53-96eb-3b71712620e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51c90226-2b48-4353-978a-186a3f6d518b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 18
}