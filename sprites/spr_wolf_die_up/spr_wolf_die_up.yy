{
    "id": "de4592c6-afd0-43f4-872e-765ccc411b54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_die_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 14,
    "bbox_right": 29,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b22602c-7de1-4a74-9f09-b986526639ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4592c6-afd0-43f4-872e-765ccc411b54",
            "compositeImage": {
                "id": "1addabbf-d9ce-421e-82c4-0247cce7b73b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b22602c-7de1-4a74-9f09-b986526639ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "297c85b5-2b68-4d8a-8d8b-7586f7f19803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b22602c-7de1-4a74-9f09-b986526639ab",
                    "LayerId": "ec1024c9-d720-44d7-a22c-831a70f04ccc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "ec1024c9-d720-44d7-a22c-831a70f04ccc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de4592c6-afd0-43f4-872e-765ccc411b54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}