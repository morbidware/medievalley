{
    "id": "5b210d9d-af35-4d03-ba54-eeb0fecdbbac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9915894f-50f9-4969-b460-9aca6a16914d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b210d9d-af35-4d03-ba54-eeb0fecdbbac",
            "compositeImage": {
                "id": "b495ecf3-dbcb-4905-8a72-83d20f29c038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9915894f-50f9-4969-b460-9aca6a16914d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "766a298d-f6ea-46cc-b14f-bbbeb517f47e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9915894f-50f9-4969-b460-9aca6a16914d",
                    "LayerId": "52732145-27b6-4bed-b764-ef0b5b14d50c"
                }
            ]
        },
        {
            "id": "c362e480-b2ee-41d1-a60d-d963758ceeb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b210d9d-af35-4d03-ba54-eeb0fecdbbac",
            "compositeImage": {
                "id": "06bbb27a-ad6a-4374-8770-5ecb7cf420bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c362e480-b2ee-41d1-a60d-d963758ceeb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f390069a-2e2f-4c24-ad86-d48720be727f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c362e480-b2ee-41d1-a60d-d963758ceeb8",
                    "LayerId": "52732145-27b6-4bed-b764-ef0b5b14d50c"
                }
            ]
        },
        {
            "id": "06e4338c-6f94-4d20-9cca-6f43968f249a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b210d9d-af35-4d03-ba54-eeb0fecdbbac",
            "compositeImage": {
                "id": "7587b6fb-0e40-45f5-aa08-459bf4703e38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e4338c-6f94-4d20-9cca-6f43968f249a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf384447-79fd-42f9-a8d1-eda14dbe86f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e4338c-6f94-4d20-9cca-6f43968f249a",
                    "LayerId": "52732145-27b6-4bed-b764-ef0b5b14d50c"
                }
            ]
        },
        {
            "id": "193bc8c6-46fb-46c5-9bc5-cf3d16ee8fdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b210d9d-af35-4d03-ba54-eeb0fecdbbac",
            "compositeImage": {
                "id": "4fcd27be-50a3-43ae-b0e0-2400fd84c210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193bc8c6-46fb-46c5-9bc5-cf3d16ee8fdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bfaf6ef-0c74-43c3-b9c8-df1f786f97a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193bc8c6-46fb-46c5-9bc5-cf3d16ee8fdd",
                    "LayerId": "52732145-27b6-4bed-b764-ef0b5b14d50c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52732145-27b6-4bed-b764-ef0b5b14d50c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b210d9d-af35-4d03-ba54-eeb0fecdbbac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}