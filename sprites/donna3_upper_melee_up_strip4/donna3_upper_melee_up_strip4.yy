{
    "id": "5b948bb4-db1c-4e84-a10d-a63a5f1d2748",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0119ab9a-85cf-45a4-a6aa-548e5d590ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b948bb4-db1c-4e84-a10d-a63a5f1d2748",
            "compositeImage": {
                "id": "d0cd4cf3-8073-4e15-a347-3cd21b5bc2d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0119ab9a-85cf-45a4-a6aa-548e5d590ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008a0bb8-b1b1-4bee-b3ef-3b20b9877d9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0119ab9a-85cf-45a4-a6aa-548e5d590ac2",
                    "LayerId": "f0ef75a3-b29a-4973-bbe9-1663bce3f798"
                }
            ]
        },
        {
            "id": "21fb02ce-2327-4650-8ebd-a93ea177f728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b948bb4-db1c-4e84-a10d-a63a5f1d2748",
            "compositeImage": {
                "id": "0ad7a796-e05c-4b4f-8343-82a5eaaf8c26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21fb02ce-2327-4650-8ebd-a93ea177f728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cbfe54d-edc2-4510-a02c-728f67844eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21fb02ce-2327-4650-8ebd-a93ea177f728",
                    "LayerId": "f0ef75a3-b29a-4973-bbe9-1663bce3f798"
                }
            ]
        },
        {
            "id": "864e43d6-ddcf-4e3c-af4f-75cfcf212a98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b948bb4-db1c-4e84-a10d-a63a5f1d2748",
            "compositeImage": {
                "id": "5a7b9121-a62b-4a56-b2e2-75f3c193deb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "864e43d6-ddcf-4e3c-af4f-75cfcf212a98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d7a2227-77ac-45fa-828b-92aebb608996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "864e43d6-ddcf-4e3c-af4f-75cfcf212a98",
                    "LayerId": "f0ef75a3-b29a-4973-bbe9-1663bce3f798"
                }
            ]
        },
        {
            "id": "b17d343c-2a03-4b81-8876-bde6b928aa32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b948bb4-db1c-4e84-a10d-a63a5f1d2748",
            "compositeImage": {
                "id": "e584d969-f1ed-4189-9548-1ceb762acb74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b17d343c-2a03-4b81-8876-bde6b928aa32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a52ce15f-388f-40e2-8e59-d9c0c3752025",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17d343c-2a03-4b81-8876-bde6b928aa32",
                    "LayerId": "f0ef75a3-b29a-4973-bbe9-1663bce3f798"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f0ef75a3-b29a-4973-bbe9-1663bce3f798",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b948bb4-db1c-4e84-a10d-a63a5f1d2748",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}