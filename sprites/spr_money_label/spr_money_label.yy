{
    "id": "2d07d17f-4ee3-4ff8-a47e-23bfe5b86d99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_money_label",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c06cb384-1918-4560-bffd-e45e38b3324a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d07d17f-4ee3-4ff8-a47e-23bfe5b86d99",
            "compositeImage": {
                "id": "041654c5-7114-47c8-9d39-aa600b87c9d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c06cb384-1918-4560-bffd-e45e38b3324a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12c55be0-5bc0-47b4-ba4b-8736e17250d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c06cb384-1918-4560-bffd-e45e38b3324a",
                    "LayerId": "adee3fbb-504a-4db9-8e5c-87d010f94875"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "adee3fbb-504a-4db9-8e5c-87d010f94875",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d07d17f-4ee3-4ff8-a47e-23bfe5b86d99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 23,
    "yorig": 6
}