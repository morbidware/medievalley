{
    "id": "7f0c94eb-8c7f-4463-9fac-3ced1967f92b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f39f2c8-4700-462e-a044-485484db182d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f0c94eb-8c7f-4463-9fac-3ced1967f92b",
            "compositeImage": {
                "id": "c43a9c54-d06e-43e9-a7f2-cb105d41d692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f39f2c8-4700-462e-a044-485484db182d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f74fb9-6cec-4225-bf88-6c138620a7fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f39f2c8-4700-462e-a044-485484db182d",
                    "LayerId": "97cbb5aa-a28f-40fe-a4c8-a85a02fa7239"
                }
            ]
        },
        {
            "id": "fd10757d-1b4a-417d-b216-22bca7c1116a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f0c94eb-8c7f-4463-9fac-3ced1967f92b",
            "compositeImage": {
                "id": "544b3181-e607-4a04-a996-35bedfafaabc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd10757d-1b4a-417d-b216-22bca7c1116a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c1f9ff4-de8c-4683-bb1f-4d62439ae11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd10757d-1b4a-417d-b216-22bca7c1116a",
                    "LayerId": "97cbb5aa-a28f-40fe-a4c8-a85a02fa7239"
                }
            ]
        },
        {
            "id": "d6e59380-e0f7-41ce-b662-a881f79eeaeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f0c94eb-8c7f-4463-9fac-3ced1967f92b",
            "compositeImage": {
                "id": "6dfcfdc2-14d5-4d2a-a1ed-b3f380975590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e59380-e0f7-41ce-b662-a881f79eeaeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f2f4a88-81fb-4f55-81d8-769eb6b7dde9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e59380-e0f7-41ce-b662-a881f79eeaeb",
                    "LayerId": "97cbb5aa-a28f-40fe-a4c8-a85a02fa7239"
                }
            ]
        },
        {
            "id": "1700988d-c915-4c7f-a56b-5ea9eed8e2f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f0c94eb-8c7f-4463-9fac-3ced1967f92b",
            "compositeImage": {
                "id": "924f2259-de5e-431d-9d1a-534f522bc369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1700988d-c915-4c7f-a56b-5ea9eed8e2f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e49e63-d2e0-4602-ac91-867b020db6b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1700988d-c915-4c7f-a56b-5ea9eed8e2f2",
                    "LayerId": "97cbb5aa-a28f-40fe-a4c8-a85a02fa7239"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "97cbb5aa-a28f-40fe-a4c8-a85a02fa7239",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f0c94eb-8c7f-4463-9fac-3ced1967f92b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}