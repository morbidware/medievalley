{
    "id": "ce12f28d-46b4-4ebc-9b38-ed6c52567b9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c6f6835-97b0-439e-9d78-41b348992ab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce12f28d-46b4-4ebc-9b38-ed6c52567b9d",
            "compositeImage": {
                "id": "6827797c-f01b-4855-9a51-6cd3ac9aac44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c6f6835-97b0-439e-9d78-41b348992ab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f159c61f-eacf-4f4f-aa3f-4d17298095be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c6f6835-97b0-439e-9d78-41b348992ab2",
                    "LayerId": "f77e1639-3ec4-4fb9-916d-9829ba71c428"
                }
            ]
        },
        {
            "id": "37854b19-7bb6-47ce-a190-b0b1c0ac12b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce12f28d-46b4-4ebc-9b38-ed6c52567b9d",
            "compositeImage": {
                "id": "263c60c7-5d73-412b-be1a-53b68d86f33d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37854b19-7bb6-47ce-a190-b0b1c0ac12b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc0c07f-0a00-492e-a07f-e6d2aeb28346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37854b19-7bb6-47ce-a190-b0b1c0ac12b6",
                    "LayerId": "f77e1639-3ec4-4fb9-916d-9829ba71c428"
                }
            ]
        },
        {
            "id": "f3600bf6-ab4d-4b01-8d80-74f5c5448ae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce12f28d-46b4-4ebc-9b38-ed6c52567b9d",
            "compositeImage": {
                "id": "bed9aa4e-9f37-44ba-99e4-67030b8a8125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3600bf6-ab4d-4b01-8d80-74f5c5448ae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c2e198-958d-47dd-85e3-6a82c3d36960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3600bf6-ab4d-4b01-8d80-74f5c5448ae5",
                    "LayerId": "f77e1639-3ec4-4fb9-916d-9829ba71c428"
                }
            ]
        },
        {
            "id": "05d42a5c-744e-4ab7-ae30-cffdb21784f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce12f28d-46b4-4ebc-9b38-ed6c52567b9d",
            "compositeImage": {
                "id": "544de678-7011-41b8-9524-62caca8673df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d42a5c-744e-4ab7-ae30-cffdb21784f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c1dc07-e8f8-4865-9009-1dfd4dde3f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d42a5c-744e-4ab7-ae30-cffdb21784f1",
                    "LayerId": "f77e1639-3ec4-4fb9-916d-9829ba71c428"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f77e1639-3ec4-4fb9-916d-9829ba71c428",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce12f28d-46b4-4ebc-9b38-ed6c52567b9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}