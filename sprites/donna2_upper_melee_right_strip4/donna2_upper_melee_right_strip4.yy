{
    "id": "4f9c2a2a-ee58-4ba7-8945-0258b627e210",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "553980fd-7416-4c2f-9f7c-e9f83fe64568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f9c2a2a-ee58-4ba7-8945-0258b627e210",
            "compositeImage": {
                "id": "994592c8-4d0a-4511-bb28-8e4b6e85c9c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553980fd-7416-4c2f-9f7c-e9f83fe64568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35b6f86-ed40-49ee-a45e-7247a8fa02c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553980fd-7416-4c2f-9f7c-e9f83fe64568",
                    "LayerId": "c9a134ac-2814-4dfc-8eb9-bb7f637d8110"
                }
            ]
        },
        {
            "id": "4fb5c135-060f-41e2-941b-350e22594613",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f9c2a2a-ee58-4ba7-8945-0258b627e210",
            "compositeImage": {
                "id": "5dbde9c3-dec7-4bd3-9d06-434ad6d3eb64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb5c135-060f-41e2-941b-350e22594613",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2fb30b6-c89b-4baa-a516-24292b643a10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb5c135-060f-41e2-941b-350e22594613",
                    "LayerId": "c9a134ac-2814-4dfc-8eb9-bb7f637d8110"
                }
            ]
        },
        {
            "id": "db1a38d9-7c1b-47e2-8271-94644b40a088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f9c2a2a-ee58-4ba7-8945-0258b627e210",
            "compositeImage": {
                "id": "09874ad3-67be-45c0-81c2-90a38a27eeb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1a38d9-7c1b-47e2-8271-94644b40a088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1cfa42b-7870-4a26-ba0f-29c14418c7cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1a38d9-7c1b-47e2-8271-94644b40a088",
                    "LayerId": "c9a134ac-2814-4dfc-8eb9-bb7f637d8110"
                }
            ]
        },
        {
            "id": "44e7cc8d-de57-41be-b7f0-e7b8f1a37e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f9c2a2a-ee58-4ba7-8945-0258b627e210",
            "compositeImage": {
                "id": "f57f833b-16f6-4d3a-a6fa-f4bcad82d75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44e7cc8d-de57-41be-b7f0-e7b8f1a37e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34a042d-ad91-47e7-8c4e-c8117a05e40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44e7cc8d-de57-41be-b7f0-e7b8f1a37e0a",
                    "LayerId": "c9a134ac-2814-4dfc-8eb9-bb7f637d8110"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c9a134ac-2814-4dfc-8eb9-bb7f637d8110",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f9c2a2a-ee58-4ba7-8945-0258b627e210",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}