{
    "id": "76af007d-8283-457b-9cbd-aec157a00fbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93a54230-7098-4772-be51-27a75a520f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "compositeImage": {
                "id": "7592a837-4002-4124-a130-ed2628fd6dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a54230-7098-4772-be51-27a75a520f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "646583f7-3698-423e-95f7-607b1510f78c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a54230-7098-4772-be51-27a75a520f33",
                    "LayerId": "d368c1ad-9069-4458-bbed-d5b82b03d050"
                }
            ]
        },
        {
            "id": "5f83ccb5-a363-4263-8ace-d3030fb1c4f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "compositeImage": {
                "id": "99fe3e48-ae01-437d-8197-5a37ede5c99a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f83ccb5-a363-4263-8ace-d3030fb1c4f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cee1621-8e9b-4b91-9115-7bc1755adac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f83ccb5-a363-4263-8ace-d3030fb1c4f4",
                    "LayerId": "d368c1ad-9069-4458-bbed-d5b82b03d050"
                }
            ]
        },
        {
            "id": "8099efea-8de8-4619-898c-65caef01e827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "compositeImage": {
                "id": "556bb128-7505-4687-b4ce-374289310c74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8099efea-8de8-4619-898c-65caef01e827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d7275e-4063-49ee-998d-a2e12a13dfd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8099efea-8de8-4619-898c-65caef01e827",
                    "LayerId": "d368c1ad-9069-4458-bbed-d5b82b03d050"
                }
            ]
        },
        {
            "id": "6bc37d63-2ea8-4d03-b91b-3399afb3b2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "compositeImage": {
                "id": "38b446c4-0cf2-4a80-b505-427979913266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc37d63-2ea8-4d03-b91b-3399afb3b2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70afc883-0739-406a-9b4b-f3701acb0287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc37d63-2ea8-4d03-b91b-3399afb3b2c3",
                    "LayerId": "d368c1ad-9069-4458-bbed-d5b82b03d050"
                }
            ]
        },
        {
            "id": "cb7b247d-418e-4d3e-9a84-3d7985f6776f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "compositeImage": {
                "id": "9fa79bbc-d9df-4247-85a9-0cfa49c72a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7b247d-418e-4d3e-9a84-3d7985f6776f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be3100a3-8042-4bd3-8b94-f31858c909e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7b247d-418e-4d3e-9a84-3d7985f6776f",
                    "LayerId": "d368c1ad-9069-4458-bbed-d5b82b03d050"
                }
            ]
        },
        {
            "id": "42b81c4e-9c3f-4b0b-b789-e8f728993ab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "compositeImage": {
                "id": "5fbb6872-d861-4d3f-b383-8a2d6078f5c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b81c4e-9c3f-4b0b-b789-e8f728993ab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faeb57cb-f4e9-4424-bd3f-c5fda716748c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b81c4e-9c3f-4b0b-b789-e8f728993ab4",
                    "LayerId": "d368c1ad-9069-4458-bbed-d5b82b03d050"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "d368c1ad-9069-4458-bbed-d5b82b03d050",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76af007d-8283-457b-9cbd-aec157a00fbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}