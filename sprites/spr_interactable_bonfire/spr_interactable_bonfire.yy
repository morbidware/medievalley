{
    "id": "46d95269-6d02-4d27-b717-02c0cbba5579",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_bonfire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8793b8a-31ed-488c-91e5-aa1c697115ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "compositeImage": {
                "id": "cbd3eb80-2397-4c15-99cf-b5918000d65b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8793b8a-31ed-488c-91e5-aa1c697115ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e89523b-2097-4b07-97a2-7b0bfb040393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8793b8a-31ed-488c-91e5-aa1c697115ff",
                    "LayerId": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a"
                }
            ]
        },
        {
            "id": "e18def61-3e4a-4465-9318-9fdf3ec31b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "compositeImage": {
                "id": "537cf164-ede0-4927-b9e5-8b5b40ad08ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e18def61-3e4a-4465-9318-9fdf3ec31b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f29faef-615e-4450-bc19-17a9942f109c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e18def61-3e4a-4465-9318-9fdf3ec31b75",
                    "LayerId": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a"
                }
            ]
        },
        {
            "id": "a53a05a1-3fde-4f21-991d-4a69b0f0806e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "compositeImage": {
                "id": "0e0217d0-da06-493a-8ce2-61f2cd103c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53a05a1-3fde-4f21-991d-4a69b0f0806e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c09594-336e-4b01-995e-f6946b7e6f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53a05a1-3fde-4f21-991d-4a69b0f0806e",
                    "LayerId": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a"
                }
            ]
        },
        {
            "id": "5bb4ea01-b24f-4fe6-a14c-48615a6a2c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "compositeImage": {
                "id": "4076c005-66af-4d7c-bb3b-cfc4735072eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bb4ea01-b24f-4fe6-a14c-48615a6a2c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4e35ce-64b2-4b16-b82a-71a96d0451c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bb4ea01-b24f-4fe6-a14c-48615a6a2c96",
                    "LayerId": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a"
                }
            ]
        },
        {
            "id": "a3c656f1-4e24-4cce-ab45-895a7338c9b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "compositeImage": {
                "id": "5b2e1f2e-35fc-4217-af46-40ae32c20ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3c656f1-4e24-4cce-ab45-895a7338c9b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15fe7520-0b04-4084-9cbb-934e986ec801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3c656f1-4e24-4cce-ab45-895a7338c9b2",
                    "LayerId": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a"
                }
            ]
        },
        {
            "id": "8d3c983c-1860-4086-8e2b-cd3596bf6c74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "compositeImage": {
                "id": "b163c610-0eb0-40a1-b036-50c782699859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3c983c-1860-4086-8e2b-cd3596bf6c74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1554c0c-9c54-4df6-99ab-ed7c4016528e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3c983c-1860-4086-8e2b-cd3596bf6c74",
                    "LayerId": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "40a8e6df-ffd4-4b8a-8663-65a9ba99be1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46d95269-6d02-4d27-b717-02c0cbba5579",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}