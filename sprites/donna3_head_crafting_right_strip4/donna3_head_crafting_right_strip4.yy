{
    "id": "2e3f1f5c-0b38-4acf-bde1-31cc35288d41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2b529bf-2176-432e-aa70-e155f6fc9f43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3f1f5c-0b38-4acf-bde1-31cc35288d41",
            "compositeImage": {
                "id": "1adbaa7b-b9c7-4807-bf16-a8bad4489900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2b529bf-2176-432e-aa70-e155f6fc9f43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "500cc045-469f-473a-a8bb-0fb862bc0b92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b529bf-2176-432e-aa70-e155f6fc9f43",
                    "LayerId": "986569eb-0d41-43ee-a1f5-58850f7295b3"
                }
            ]
        },
        {
            "id": "ac0a0fb8-c000-4bc7-85af-9ffe945a9191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3f1f5c-0b38-4acf-bde1-31cc35288d41",
            "compositeImage": {
                "id": "12d24983-26cc-4bd2-9687-669bf00018c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac0a0fb8-c000-4bc7-85af-9ffe945a9191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40098255-5c76-4510-9972-e2ab205dd333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac0a0fb8-c000-4bc7-85af-9ffe945a9191",
                    "LayerId": "986569eb-0d41-43ee-a1f5-58850f7295b3"
                }
            ]
        },
        {
            "id": "ff6f8bae-27c4-47c3-aa11-5555f37985c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3f1f5c-0b38-4acf-bde1-31cc35288d41",
            "compositeImage": {
                "id": "fbdb755d-77f5-40ec-98b2-d90fb8ffac38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6f8bae-27c4-47c3-aa11-5555f37985c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e7a6ee-c9da-475f-b1ec-93798651d202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6f8bae-27c4-47c3-aa11-5555f37985c6",
                    "LayerId": "986569eb-0d41-43ee-a1f5-58850f7295b3"
                }
            ]
        },
        {
            "id": "080acd71-0a81-41eb-b115-d0eebf79e34a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e3f1f5c-0b38-4acf-bde1-31cc35288d41",
            "compositeImage": {
                "id": "7aeb7e3e-2105-4d6e-9fa2-fbe8440cbb46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "080acd71-0a81-41eb-b115-d0eebf79e34a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec6a21f-d66d-4acb-98b0-cebe8f4b8852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "080acd71-0a81-41eb-b115-d0eebf79e34a",
                    "LayerId": "986569eb-0d41-43ee-a1f5-58850f7295b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "986569eb-0d41-43ee-a1f5-58850f7295b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e3f1f5c-0b38-4acf-bde1-31cc35288d41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}