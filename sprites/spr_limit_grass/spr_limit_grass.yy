{
    "id": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_limit_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30dc8660-2cf7-44ec-a3e5-b275262b8e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "4a79b25a-31a8-4cfc-b765-5dfc3cdbbd74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30dc8660-2cf7-44ec-a3e5-b275262b8e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a83e7b0-2750-4d3a-82ad-507ccd93c8a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30dc8660-2cf7-44ec-a3e5-b275262b8e5d",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "c0a29de9-9dd2-4a68-b2ba-a5e2eb254412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "1711a2bc-758a-4681-bb00-3c7b8ff9b12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a29de9-9dd2-4a68-b2ba-a5e2eb254412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "343b4f69-1b2a-442b-b325-31fbf9ead48a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a29de9-9dd2-4a68-b2ba-a5e2eb254412",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "2611f6a9-520b-4602-b0d8-6cd50a9e5e80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "9e16b8d1-f77e-441c-aecf-330a83b8a961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2611f6a9-520b-4602-b0d8-6cd50a9e5e80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6c0f159-737e-4bdd-954f-2e5ad9c94a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2611f6a9-520b-4602-b0d8-6cd50a9e5e80",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "47fee1a9-14dc-4128-a676-02c52043ed9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "4d4d6e17-2b0e-465e-b29b-33c1aadc37ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47fee1a9-14dc-4128-a676-02c52043ed9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0daeaf12-2c74-48b1-a868-56ac67f7b663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47fee1a9-14dc-4128-a676-02c52043ed9c",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "17a33d3f-2be4-4272-9aa8-4a745f9ea77f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "6e1a28fd-5c54-4631-983d-f31db9aae9ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a33d3f-2be4-4272-9aa8-4a745f9ea77f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c85891d-bf95-406d-987d-4e3f9b41b212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a33d3f-2be4-4272-9aa8-4a745f9ea77f",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "f243a76a-ecfa-4ad2-97d1-f8073d59e135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "6b7228df-7a7a-4d7c-9273-e970275e7c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f243a76a-ecfa-4ad2-97d1-f8073d59e135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e196354-4950-46e0-968a-d54530e9e891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f243a76a-ecfa-4ad2-97d1-f8073d59e135",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "dc19c253-882d-4e1a-bd33-ca9e1455c6ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "df402c2a-63d7-4a20-8442-3296f0cd3676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc19c253-882d-4e1a-bd33-ca9e1455c6ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "073738b6-05df-4934-92ed-f798a3d46ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc19c253-882d-4e1a-bd33-ca9e1455c6ae",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "eb6a3b10-357e-4da6-8836-8c09c1224ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "1186fbd6-9141-45cc-9485-69cedb6c81c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb6a3b10-357e-4da6-8836-8c09c1224ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bd610f9-c94b-4611-8b89-106cb7393386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb6a3b10-357e-4da6-8836-8c09c1224ad6",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "792885eb-2606-4df5-a31d-25f19e8d184e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "c5963c92-015e-4780-9581-8c046d1a6581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "792885eb-2606-4df5-a31d-25f19e8d184e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94fd39ba-ed3b-4964-84a5-a394db56c284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "792885eb-2606-4df5-a31d-25f19e8d184e",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "3280d2ca-9ecd-40a9-ba39-21a3ad762683",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "5ecb0297-1591-45a1-a759-200c30b3f42b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3280d2ca-9ecd-40a9-ba39-21a3ad762683",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc26f94e-a459-4d59-9b33-215c8af5f96b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3280d2ca-9ecd-40a9-ba39-21a3ad762683",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "d8327d44-3f96-404a-95dc-6ea5a14676fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "c8c35d52-0a0e-4d76-aa49-6b667f24f2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8327d44-3f96-404a-95dc-6ea5a14676fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4019739f-85b7-4396-a8a8-d79ca63689ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8327d44-3f96-404a-95dc-6ea5a14676fa",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "d286bbc0-490b-443b-ab10-a1fe85b7fe92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "57ff4a91-34b7-4906-8da6-aff6a619ab35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d286bbc0-490b-443b-ab10-a1fe85b7fe92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d27aa45-d091-4b85-a2c0-6878f5725396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d286bbc0-490b-443b-ab10-a1fe85b7fe92",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "e3493149-21cb-472d-9695-3fbe6e7e9b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "284a988d-3e57-4013-ac87-f0d3f0be9f48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3493149-21cb-472d-9695-3fbe6e7e9b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e3c08a3-2f3b-482e-aadc-f736d282770b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3493149-21cb-472d-9695-3fbe6e7e9b52",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        },
        {
            "id": "eea1d3ce-3402-4d4e-8dfd-ad743e4db425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "compositeImage": {
                "id": "b482cd3b-c773-42a6-b8fe-cd8d0be8be94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eea1d3ce-3402-4d4e-8dfd-ad743e4db425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a15bd8-edf4-46da-83e3-e1f77d9d55ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eea1d3ce-3402-4d4e-8dfd-ad743e4db425",
                    "LayerId": "6ce45c2a-6425-4e91-88c6-31f77c474dfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ce45c2a-6425-4e91-88c6-31f77c474dfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af83c8d3-79d9-4b7f-bb51-c172b0d2417d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}