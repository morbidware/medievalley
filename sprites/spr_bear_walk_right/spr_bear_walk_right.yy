{
    "id": "a26e3415-0351-460a-b686-d1d16105afe0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ba80eb9-0151-4b97-815f-6aae5f6585af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "compositeImage": {
                "id": "b4796d6e-a62e-4052-b11c-36e46e4f4e4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba80eb9-0151-4b97-815f-6aae5f6585af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92700db8-8765-4904-b738-b26797f3d9ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba80eb9-0151-4b97-815f-6aae5f6585af",
                    "LayerId": "960348e6-0475-452b-9aaf-6c03afd513b7"
                }
            ]
        },
        {
            "id": "1e8537e0-b677-444a-ae28-49e660279365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "compositeImage": {
                "id": "72b88c9c-0697-4765-a0a4-91b3e224ade5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8537e0-b677-444a-ae28-49e660279365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c05a66-be93-49de-ae13-83142b2611b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8537e0-b677-444a-ae28-49e660279365",
                    "LayerId": "960348e6-0475-452b-9aaf-6c03afd513b7"
                }
            ]
        },
        {
            "id": "4fda0bc5-4fc3-4ef4-9e09-bceaa1685724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "compositeImage": {
                "id": "fbdebe41-0424-4bbc-a900-c32585dbe4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fda0bc5-4fc3-4ef4-9e09-bceaa1685724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e18f10-9da5-46d3-9101-5e42b6e18821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fda0bc5-4fc3-4ef4-9e09-bceaa1685724",
                    "LayerId": "960348e6-0475-452b-9aaf-6c03afd513b7"
                }
            ]
        },
        {
            "id": "caaf64bd-d39c-4351-89ae-0ba46bc452dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "compositeImage": {
                "id": "ad655c03-bff4-4468-96d7-a53d007e3700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caaf64bd-d39c-4351-89ae-0ba46bc452dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ffe11cf-da08-44f3-af15-ac3c17ca7496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caaf64bd-d39c-4351-89ae-0ba46bc452dc",
                    "LayerId": "960348e6-0475-452b-9aaf-6c03afd513b7"
                }
            ]
        },
        {
            "id": "47f7246d-6348-4fe1-9cd7-59b7537f2ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "compositeImage": {
                "id": "bdf37e69-6d64-4c27-8cc1-cbae5cb7abb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f7246d-6348-4fe1-9cd7-59b7537f2ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8cb4a9-7e1f-4a3f-a751-a85cc4080124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f7246d-6348-4fe1-9cd7-59b7537f2ead",
                    "LayerId": "960348e6-0475-452b-9aaf-6c03afd513b7"
                }
            ]
        },
        {
            "id": "d3d6cdb7-eae1-4f03-b1ea-edc5b2bd0bd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "compositeImage": {
                "id": "ef4a3d4e-5c0c-4027-9174-1c64d4c1b656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3d6cdb7-eae1-4f03-b1ea-edc5b2bd0bd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73efb46d-1268-4e5f-ad77-fa3e8dfce657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3d6cdb7-eae1-4f03-b1ea-edc5b2bd0bd4",
                    "LayerId": "960348e6-0475-452b-9aaf-6c03afd513b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "960348e6-0475-452b-9aaf-6c03afd513b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a26e3415-0351-460a-b686-d1d16105afe0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 44
}