{
    "id": "b1647cb8-2d2e-41e9-975c-a61f5010887a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4431e2f3-ec55-44e6-a1b2-984bf52a2206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1647cb8-2d2e-41e9-975c-a61f5010887a",
            "compositeImage": {
                "id": "cf8fdd5f-d770-4f8c-b5a6-097cf3b1a246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4431e2f3-ec55-44e6-a1b2-984bf52a2206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34d3a61-aab0-47b9-96b7-82b9fa14f12f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4431e2f3-ec55-44e6-a1b2-984bf52a2206",
                    "LayerId": "87f258a6-63f9-427f-a7e7-e8915d97846a"
                }
            ]
        },
        {
            "id": "9d4c2089-3023-4af7-b8a4-9226b54e059c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1647cb8-2d2e-41e9-975c-a61f5010887a",
            "compositeImage": {
                "id": "c1cc9a76-6e5b-4a55-a74b-4e97b220cf6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d4c2089-3023-4af7-b8a4-9226b54e059c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39462fcb-7b6a-47ef-b610-dde9a5eef05b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d4c2089-3023-4af7-b8a4-9226b54e059c",
                    "LayerId": "87f258a6-63f9-427f-a7e7-e8915d97846a"
                }
            ]
        },
        {
            "id": "b427de46-8c57-4b8a-894a-a4981632d7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1647cb8-2d2e-41e9-975c-a61f5010887a",
            "compositeImage": {
                "id": "8aa96479-749d-4c68-b94f-8e02f0a13df5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b427de46-8c57-4b8a-894a-a4981632d7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5c2013-8003-4b50-9cb2-845ab873bb82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b427de46-8c57-4b8a-894a-a4981632d7b8",
                    "LayerId": "87f258a6-63f9-427f-a7e7-e8915d97846a"
                }
            ]
        },
        {
            "id": "5b2b192d-444d-419b-93dc-8afeb4aa1a7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1647cb8-2d2e-41e9-975c-a61f5010887a",
            "compositeImage": {
                "id": "4dbb92ae-659a-4006-b811-dee4f667765c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b2b192d-444d-419b-93dc-8afeb4aa1a7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f5e11e-0676-4ecd-90b2-05c682673a58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b2b192d-444d-419b-93dc-8afeb4aa1a7c",
                    "LayerId": "87f258a6-63f9-427f-a7e7-e8915d97846a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "87f258a6-63f9-427f-a7e7-e8915d97846a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1647cb8-2d2e-41e9-975c-a61f5010887a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}