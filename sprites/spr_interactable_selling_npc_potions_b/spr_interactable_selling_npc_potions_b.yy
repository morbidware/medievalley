{
    "id": "e8d71332-63a1-4012-b291-e6c115a364f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_selling_npc_potions_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24c56eb4-0765-42a3-8b76-6274e05054c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8d71332-63a1-4012-b291-e6c115a364f1",
            "compositeImage": {
                "id": "f5672b49-f3cd-4d73-a860-ab1324f91feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c56eb4-0765-42a3-8b76-6274e05054c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07464d4-2020-4d50-8189-99f48b543cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c56eb4-0765-42a3-8b76-6274e05054c1",
                    "LayerId": "a7b70a23-d14a-4deb-b365-7ca3ca2aaab4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a7b70a23-d14a-4deb-b365-7ca3ca2aaab4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8d71332-63a1-4012-b291-e6c115a364f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}