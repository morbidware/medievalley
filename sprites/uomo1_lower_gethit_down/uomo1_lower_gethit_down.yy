{
    "id": "474e6752-75fa-4f33-a047-8d186ca7e2b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e48d9dc-e09e-4f15-9ad8-e705eb4d2022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474e6752-75fa-4f33-a047-8d186ca7e2b7",
            "compositeImage": {
                "id": "f0b81eca-7b3e-41c3-9f51-93d4068c06f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e48d9dc-e09e-4f15-9ad8-e705eb4d2022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab55b080-f17f-4620-9a7b-6786153d49de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e48d9dc-e09e-4f15-9ad8-e705eb4d2022",
                    "LayerId": "9ddd899c-cf84-424b-865c-8d0bc737ed13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9ddd899c-cf84-424b-865c-8d0bc737ed13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "474e6752-75fa-4f33-a047-8d186ca7e2b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}