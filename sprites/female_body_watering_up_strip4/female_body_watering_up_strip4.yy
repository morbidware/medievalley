{
    "id": "91ce81df-0b8b-4cd0-a488-443f33bbb146",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 6,
    "bbox_right": 9,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "856efc45-d33c-4c41-b493-8374d21229ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ce81df-0b8b-4cd0-a488-443f33bbb146",
            "compositeImage": {
                "id": "d2a7c42c-e7ec-42de-899a-7a5a2e542dd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "856efc45-d33c-4c41-b493-8374d21229ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff4cfbc-9eee-493f-8414-916063089154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "856efc45-d33c-4c41-b493-8374d21229ab",
                    "LayerId": "da2ec954-2e1f-44ef-8d0b-3298830578a6"
                }
            ]
        },
        {
            "id": "26c47428-05e7-4911-b071-c0d472390b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ce81df-0b8b-4cd0-a488-443f33bbb146",
            "compositeImage": {
                "id": "6bb39de0-d55d-4787-9352-aff3d95bba86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c47428-05e7-4911-b071-c0d472390b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "545bbba1-2f28-4bf2-9533-ef262883d5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c47428-05e7-4911-b071-c0d472390b90",
                    "LayerId": "da2ec954-2e1f-44ef-8d0b-3298830578a6"
                }
            ]
        },
        {
            "id": "bf45e600-1a0e-48a3-acde-ef510951144a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ce81df-0b8b-4cd0-a488-443f33bbb146",
            "compositeImage": {
                "id": "2b44f915-123c-421d-a5ac-8e75e91618a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf45e600-1a0e-48a3-acde-ef510951144a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095e4cbf-44c8-4b0d-b0b9-93ee0093b5f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf45e600-1a0e-48a3-acde-ef510951144a",
                    "LayerId": "da2ec954-2e1f-44ef-8d0b-3298830578a6"
                }
            ]
        },
        {
            "id": "d4a63d5d-79e9-4b86-89c3-0b9d65714a52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91ce81df-0b8b-4cd0-a488-443f33bbb146",
            "compositeImage": {
                "id": "ff4faf3c-503d-4bf1-8352-da35c50a1e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a63d5d-79e9-4b86-89c3-0b9d65714a52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9fb323-5e05-4ea9-a63c-2b07f6118714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a63d5d-79e9-4b86-89c3-0b9d65714a52",
                    "LayerId": "da2ec954-2e1f-44ef-8d0b-3298830578a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "da2ec954-2e1f-44ef-8d0b-3298830578a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91ce81df-0b8b-4cd0-a488-443f33bbb146",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}