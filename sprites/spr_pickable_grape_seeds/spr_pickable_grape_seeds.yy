{
    "id": "71f725b3-878d-44ed-b3b8-1fb9f85c553f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_grape_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cb4a945-2487-4368-a4bb-bedbbe332337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71f725b3-878d-44ed-b3b8-1fb9f85c553f",
            "compositeImage": {
                "id": "3d039be7-ec05-4646-a77d-37b83ffbb719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb4a945-2487-4368-a4bb-bedbbe332337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b2039d-9790-4e1f-89f2-033158f14104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb4a945-2487-4368-a4bb-bedbbe332337",
                    "LayerId": "33f528a6-1614-447f-ba09-08bd9f5ff387"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "33f528a6-1614-447f-ba09-08bd9f5ff387",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71f725b3-878d-44ed-b3b8-1fb9f85c553f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}