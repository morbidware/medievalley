{
    "id": "8ba469bc-5710-46c1-8fa5-7814c2e8b869",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6634046d-237d-44da-8471-b4648456cae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ba469bc-5710-46c1-8fa5-7814c2e8b869",
            "compositeImage": {
                "id": "ace42365-2467-42d8-bf02-543b4f532e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6634046d-237d-44da-8471-b4648456cae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8dd0576-ef78-4865-9d8a-414497dd91f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6634046d-237d-44da-8471-b4648456cae3",
                    "LayerId": "a47bacc4-942e-4d34-bdb9-891a62f1f936"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a47bacc4-942e-4d34-bdb9-891a62f1f936",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ba469bc-5710-46c1-8fa5-7814c2e8b869",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 13,
    "yorig": 21
}