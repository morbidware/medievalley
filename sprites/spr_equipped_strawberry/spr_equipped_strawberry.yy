{
    "id": "d17ce366-61e5-46d3-acd4-c29b701aff80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_strawberry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa801043-40bf-4068-aac3-30b277acfb5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d17ce366-61e5-46d3-acd4-c29b701aff80",
            "compositeImage": {
                "id": "edfe4941-2ef6-4e80-a112-166779c52f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa801043-40bf-4068-aac3-30b277acfb5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072466d1-4221-4bf4-bf7c-097d3bf29e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa801043-40bf-4068-aac3-30b277acfb5c",
                    "LayerId": "64e783ad-a70a-4d4c-bd30-facb629b6aa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "64e783ad-a70a-4d4c-bd30-facb629b6aa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d17ce366-61e5-46d3-acd4-c29b701aff80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}