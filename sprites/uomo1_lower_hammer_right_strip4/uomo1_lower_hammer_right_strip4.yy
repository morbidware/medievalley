{
    "id": "2a53afd5-2852-43d1-8bc8-5577bed3f72e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a784c395-6a71-4773-a35b-a08d032fb4e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a53afd5-2852-43d1-8bc8-5577bed3f72e",
            "compositeImage": {
                "id": "13aed88f-2238-4091-95be-4930794e828f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a784c395-6a71-4773-a35b-a08d032fb4e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a741a6b-d8ee-4d84-b632-6bb275f87851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a784c395-6a71-4773-a35b-a08d032fb4e5",
                    "LayerId": "4527b12c-24e7-487d-b427-14584512ecc1"
                }
            ]
        },
        {
            "id": "6733930d-b3aa-4d9c-81bc-ab6d43ce8a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a53afd5-2852-43d1-8bc8-5577bed3f72e",
            "compositeImage": {
                "id": "bc0882bc-348d-4a86-871e-6df667a8d92d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6733930d-b3aa-4d9c-81bc-ab6d43ce8a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82f8bfcc-c742-4560-bd71-ed62fb98544e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6733930d-b3aa-4d9c-81bc-ab6d43ce8a39",
                    "LayerId": "4527b12c-24e7-487d-b427-14584512ecc1"
                }
            ]
        },
        {
            "id": "9de9571e-6cb2-4b59-8062-a4cd3c6f3a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a53afd5-2852-43d1-8bc8-5577bed3f72e",
            "compositeImage": {
                "id": "b05370cf-eef8-459b-965d-8069c42f67c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de9571e-6cb2-4b59-8062-a4cd3c6f3a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01aae8e6-31f8-4766-9dce-a632a20679d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de9571e-6cb2-4b59-8062-a4cd3c6f3a0f",
                    "LayerId": "4527b12c-24e7-487d-b427-14584512ecc1"
                }
            ]
        },
        {
            "id": "17a288e6-15be-427b-bb51-554ea0492b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a53afd5-2852-43d1-8bc8-5577bed3f72e",
            "compositeImage": {
                "id": "cb636fd8-5a69-46e3-bb5c-dc228d1bc03f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a288e6-15be-427b-bb51-554ea0492b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c604052d-5b62-4a79-91fa-3d0be695125c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a288e6-15be-427b-bb51-554ea0492b48",
                    "LayerId": "4527b12c-24e7-487d-b427-14584512ecc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4527b12c-24e7-487d-b427-14584512ecc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a53afd5-2852-43d1-8bc8-5577bed3f72e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}