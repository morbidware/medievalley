{
    "id": "b39d624a-33b7-466d-9fd9-5d0a1ef4517a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_carrot_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d597b1a-797b-41b7-837a-21d03dafcb55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b39d624a-33b7-466d-9fd9-5d0a1ef4517a",
            "compositeImage": {
                "id": "5b6f1ba7-be81-4ca0-be6d-877c2f5b152f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d597b1a-797b-41b7-837a-21d03dafcb55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17e22fb7-b737-422a-90a1-3d18ecf5dc54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d597b1a-797b-41b7-837a-21d03dafcb55",
                    "LayerId": "ae2dfd65-8b41-4e2f-a44b-e38a55529793"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ae2dfd65-8b41-4e2f-a44b-e38a55529793",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b39d624a-33b7-466d-9fd9-5d0a1ef4517a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}