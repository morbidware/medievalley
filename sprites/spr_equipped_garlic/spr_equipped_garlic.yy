{
    "id": "5078d70f-7566-4547-b0fe-0d2101e1136c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_garlic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9799f1e-3fa7-44cf-ac9d-f4f95d36ce45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5078d70f-7566-4547-b0fe-0d2101e1136c",
            "compositeImage": {
                "id": "9a81e21a-a592-4bf9-b314-c1a6ae195138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9799f1e-3fa7-44cf-ac9d-f4f95d36ce45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9b88068-98f5-4d0a-93a2-b9f229e11bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9799f1e-3fa7-44cf-ac9d-f4f95d36ce45",
                    "LayerId": "1bd2c562-372e-43f4-b504-45a3cbbd86ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1bd2c562-372e-43f4-b504-45a3cbbd86ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5078d70f-7566-4547-b0fe-0d2101e1136c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}