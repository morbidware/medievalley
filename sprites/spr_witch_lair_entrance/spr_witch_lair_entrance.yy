{
    "id": "ada7ea6e-036b-4172-bdae-826807e629ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_witch_lair_entrance",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 212,
    "bbox_left": 0,
    "bbox_right": 246,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4816b1b6-a9c8-4c65-8c9c-743de613172c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ada7ea6e-036b-4172-bdae-826807e629ed",
            "compositeImage": {
                "id": "80e7df2e-ecd4-412a-a652-0d151b753f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4816b1b6-a9c8-4c65-8c9c-743de613172c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa119460-eb65-44bc-a499-d12ef3eb83fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4816b1b6-a9c8-4c65-8c9c-743de613172c",
                    "LayerId": "cbf53044-0544-47df-a327-1d7c64a92546"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 216,
    "layers": [
        {
            "id": "cbf53044-0544-47df-a327-1d7c64a92546",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ada7ea6e-036b-4172-bdae-826807e629ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 247,
    "xorig": 124,
    "yorig": 118
}