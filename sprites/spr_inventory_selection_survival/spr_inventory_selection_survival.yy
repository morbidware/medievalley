{
    "id": "ec3f877e-1ef4-439e-8258-9b4a24e41aec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_selection_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57f2acc9-80a7-4f56-8841-54b5a5cf20e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec3f877e-1ef4-439e-8258-9b4a24e41aec",
            "compositeImage": {
                "id": "901124f4-35a4-40b4-9375-1a71414a1c58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f2acc9-80a7-4f56-8841-54b5a5cf20e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db00126-9d89-4ce4-85db-36d203d803aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f2acc9-80a7-4f56-8841-54b5a5cf20e6",
                    "LayerId": "e816f114-dee1-46f8-aa6a-36a127e59b20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "e816f114-dee1-46f8-aa6a-36a127e59b20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec3f877e-1ef4-439e-8258-9b4a24e41aec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}