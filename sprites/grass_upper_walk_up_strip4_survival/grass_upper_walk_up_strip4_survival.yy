{
    "id": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_walk_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb3348fc-07de-4239-82d4-68b3eeb69f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "compositeImage": {
                "id": "4cdf699a-b4f3-4bfb-97f6-407a702279da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb3348fc-07de-4239-82d4-68b3eeb69f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6dab0f-038c-49eb-9cd2-e858d542401a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb3348fc-07de-4239-82d4-68b3eeb69f71",
                    "LayerId": "2832d261-161a-49ab-bf20-339f198e06f8"
                }
            ]
        },
        {
            "id": "7bc53e65-f3e4-4042-88a0-0dc3937cae1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "compositeImage": {
                "id": "5c533b1e-113b-4158-84a2-a33afc6a4d65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bc53e65-f3e4-4042-88a0-0dc3937cae1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b6e0931-51c9-48c8-b6d3-ac38b91804e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bc53e65-f3e4-4042-88a0-0dc3937cae1f",
                    "LayerId": "2832d261-161a-49ab-bf20-339f198e06f8"
                }
            ]
        },
        {
            "id": "e044de1b-40c7-4e68-b893-5814476bd864",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "compositeImage": {
                "id": "efaa1287-6b43-489f-a82f-c05b6cf57085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e044de1b-40c7-4e68-b893-5814476bd864",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "959e929a-4364-4614-96f8-940b5b1d4782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e044de1b-40c7-4e68-b893-5814476bd864",
                    "LayerId": "2832d261-161a-49ab-bf20-339f198e06f8"
                }
            ]
        },
        {
            "id": "8b27c1c4-647c-4f35-8ffa-2b9ea0afae44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "compositeImage": {
                "id": "20f6644c-fbbf-4e25-9490-8e96f92f4694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b27c1c4-647c-4f35-8ffa-2b9ea0afae44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b61fc0fe-5d81-45e1-b7fe-b13ee6e7ee20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b27c1c4-647c-4f35-8ffa-2b9ea0afae44",
                    "LayerId": "2832d261-161a-49ab-bf20-339f198e06f8"
                }
            ]
        },
        {
            "id": "6203e75f-612a-4971-b28d-19be8c4569ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "compositeImage": {
                "id": "b4aa5f14-185e-4268-a814-49cf9538a0d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6203e75f-612a-4971-b28d-19be8c4569ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d4eceee-08ab-4801-8111-9919ef07b906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6203e75f-612a-4971-b28d-19be8c4569ab",
                    "LayerId": "2832d261-161a-49ab-bf20-339f198e06f8"
                }
            ]
        },
        {
            "id": "0179e389-39e4-4afd-8fb1-a75195e13346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "compositeImage": {
                "id": "bacbfbd7-34c6-4206-8ccc-4cc5cf528be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0179e389-39e4-4afd-8fb1-a75195e13346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3896bb6-16cd-4498-bbfc-6823a54459d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0179e389-39e4-4afd-8fb1-a75195e13346",
                    "LayerId": "2832d261-161a-49ab-bf20-339f198e06f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2832d261-161a-49ab-bf20-339f198e06f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b2af1b3-325a-4039-b9a2-fec12a141aa7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}