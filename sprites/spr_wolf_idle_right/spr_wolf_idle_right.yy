{
    "id": "73e72f19-6ffd-4b28-a5da-c77a2366206e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 9,
    "bbox_right": 47,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "081ea610-5f32-417c-95ba-8e1db4b0ec36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73e72f19-6ffd-4b28-a5da-c77a2366206e",
            "compositeImage": {
                "id": "8113d69e-20c7-4d03-b6f8-2a8ddde6e972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "081ea610-5f32-417c-95ba-8e1db4b0ec36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89347922-31f6-4b0a-99c6-f6696c184fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "081ea610-5f32-417c-95ba-8e1db4b0ec36",
                    "LayerId": "649d9ce9-9fa3-4f4a-b95c-2b4387fee93a"
                }
            ]
        },
        {
            "id": "7220ccb9-24e8-4c2a-9c1f-f0eac84c0873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73e72f19-6ffd-4b28-a5da-c77a2366206e",
            "compositeImage": {
                "id": "671956a6-cd77-486a-9a35-3ad735fa63f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7220ccb9-24e8-4c2a-9c1f-f0eac84c0873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a548d2e-1128-4c7b-9d73-62ce5aed132e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7220ccb9-24e8-4c2a-9c1f-f0eac84c0873",
                    "LayerId": "649d9ce9-9fa3-4f4a-b95c-2b4387fee93a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "649d9ce9-9fa3-4f4a-b95c-2b4387fee93a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73e72f19-6ffd-4b28-a5da-c77a2366206e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}