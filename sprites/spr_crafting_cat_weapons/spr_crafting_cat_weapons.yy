{
    "id": "0e5ee584-bbc9-416d-a603-fddcd5753ce9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_weapons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ac253b1-7de3-4ae0-a0b2-a8c5cfdaefd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e5ee584-bbc9-416d-a603-fddcd5753ce9",
            "compositeImage": {
                "id": "f5661898-58e2-4c1b-9023-d7dbcfefe564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac253b1-7de3-4ae0-a0b2-a8c5cfdaefd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "430e4fb3-233e-48a5-9470-9ae47daf24ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac253b1-7de3-4ae0-a0b2-a8c5cfdaefd5",
                    "LayerId": "f9b8b592-66b0-44ad-acea-e77f6e84932e"
                }
            ]
        },
        {
            "id": "46d5d14d-998b-4b71-8952-31bd6a440b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e5ee584-bbc9-416d-a603-fddcd5753ce9",
            "compositeImage": {
                "id": "2adda2b2-bb52-4f1c-a245-daa1810ce975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d5d14d-998b-4b71-8952-31bd6a440b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b53d74f6-fa47-4f3a-bd9d-38b80fa16fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d5d14d-998b-4b71-8952-31bd6a440b57",
                    "LayerId": "f9b8b592-66b0-44ad-acea-e77f6e84932e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "f9b8b592-66b0-44ad-acea-e77f6e84932e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e5ee584-bbc9-416d-a603-fddcd5753ce9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}