{
    "id": "6178fa0f-60e1-4ad2-9b63-69aba0f8ec55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf8cdf78-67eb-4f89-a1dd-9040548c3e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178fa0f-60e1-4ad2-9b63-69aba0f8ec55",
            "compositeImage": {
                "id": "abae70df-2e0b-4cd4-91fc-f46e3c28abcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf8cdf78-67eb-4f89-a1dd-9040548c3e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b1c070-d075-48a3-a024-362823cc62eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf8cdf78-67eb-4f89-a1dd-9040548c3e7f",
                    "LayerId": "8ccb2718-1f4b-438d-9ad5-9ff269e2682b"
                }
            ]
        },
        {
            "id": "8570a1e1-95de-4f16-a860-0f07f9afd380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178fa0f-60e1-4ad2-9b63-69aba0f8ec55",
            "compositeImage": {
                "id": "e90bbd5d-53f0-43f3-a4b4-35031d77366b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8570a1e1-95de-4f16-a860-0f07f9afd380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11106b39-d7ba-4474-8e3a-b9cb8eaada7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8570a1e1-95de-4f16-a860-0f07f9afd380",
                    "LayerId": "8ccb2718-1f4b-438d-9ad5-9ff269e2682b"
                }
            ]
        },
        {
            "id": "7fc053f2-f300-4174-8928-6c80dee84f60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6178fa0f-60e1-4ad2-9b63-69aba0f8ec55",
            "compositeImage": {
                "id": "a8096c6b-2587-4578-9942-9a47aed0d677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fc053f2-f300-4174-8928-6c80dee84f60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ae3a63b-ae6d-4163-9424-ea309e947255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fc053f2-f300-4174-8928-6c80dee84f60",
                    "LayerId": "8ccb2718-1f4b-438d-9ad5-9ff269e2682b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "8ccb2718-1f4b-438d-9ad5-9ff269e2682b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6178fa0f-60e1-4ad2-9b63-69aba0f8ec55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}