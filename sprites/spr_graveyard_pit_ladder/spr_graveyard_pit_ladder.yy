{
    "id": "c4c495df-4e1b-4304-9242-88495c7e6596",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_pit_ladder",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38185ae1-3c18-4097-afcc-3be788d13ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c495df-4e1b-4304-9242-88495c7e6596",
            "compositeImage": {
                "id": "bfcbf8c7-e996-42a3-a760-e03c11dcc29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38185ae1-3c18-4097-afcc-3be788d13ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a115021d-75e2-4c1e-b01c-03cbc0ac8e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38185ae1-3c18-4097-afcc-3be788d13ea6",
                    "LayerId": "2221dd22-6392-451a-836f-354a76129dab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2221dd22-6392-451a-836f-354a76129dab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4c495df-4e1b-4304-9242-88495c7e6596",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}