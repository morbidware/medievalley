{
    "id": "df8acf2b-170c-40bb-8a5c-da13b8504fae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_flower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 1,
    "bbox_right": 4,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3218e438-765c-44ea-9216-101e4466fd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df8acf2b-170c-40bb-8a5c-da13b8504fae",
            "compositeImage": {
                "id": "1c4fcaac-5756-4eb9-aa69-71af7be2d5a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3218e438-765c-44ea-9216-101e4466fd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d7284f-d2e1-4da1-b963-c595df49526c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3218e438-765c-44ea-9216-101e4466fd2c",
                    "LayerId": "c4930863-9c45-440e-8a53-1a1f77ee3693"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "c4930863-9c45-440e-8a53-1a1f77ee3693",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df8acf2b-170c-40bb-8a5c-da13b8504fae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}