{
    "id": "8df57b97-8c74-4e9d-b07d-e73ab0c3e511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_selling_npc_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b22c95b7-a32c-4a43-8460-fa6d9aab0eb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8df57b97-8c74-4e9d-b07d-e73ab0c3e511",
            "compositeImage": {
                "id": "18e2c370-ec52-4988-8344-7c33e92a1b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22c95b7-a32c-4a43-8460-fa6d9aab0eb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd81909-692d-48da-a72b-f59f5697b74e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22c95b7-a32c-4a43-8460-fa6d9aab0eb1",
                    "LayerId": "97071266-cdd2-4db3-b32b-f307af33d2f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "97071266-cdd2-4db3-b32b-f307af33d2f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8df57b97-8c74-4e9d-b07d-e73ab0c3e511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}