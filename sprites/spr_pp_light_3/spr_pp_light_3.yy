{
    "id": "f491bdbd-b65d-4b6a-a256-efbd3d901990",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pp_light_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 4,
    "bbox_right": 255,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "ddec9253-72c7-4cb7-8ff4-c1e7f511b5c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f491bdbd-b65d-4b6a-a256-efbd3d901990",
            "compositeImage": {
                "id": "89f34dd2-8d69-4f2e-b479-080e83456e35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddec9253-72c7-4cb7-8ff4-c1e7f511b5c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "529c4056-b7d5-4115-91ba-f74f8cb01bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddec9253-72c7-4cb7-8ff4-c1e7f511b5c0",
                    "LayerId": "88b31911-6fb3-445d-96fa-3d04ea691941"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "88b31911-6fb3-445d-96fa-3d04ea691941",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f491bdbd-b65d-4b6a-a256-efbd3d901990",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}