{
    "id": "23663d96-f9c8-48ea-9de9-f4afead010c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_pallet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb59751f-30d9-4bd1-8ba8-c5e10e0a921d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23663d96-f9c8-48ea-9de9-f4afead010c8",
            "compositeImage": {
                "id": "f15ef23e-6ee0-444d-be67-21b4ed7316ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb59751f-30d9-4bd1-8ba8-c5e10e0a921d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be75a9d4-0d6b-4601-bc16-0c99b3b9c9ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb59751f-30d9-4bd1-8ba8-c5e10e0a921d",
                    "LayerId": "5cd28e80-bbc8-410d-8f2d-e03898890892"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5cd28e80-bbc8-410d-8f2d-e03898890892",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23663d96-f9c8-48ea-9de9-f4afead010c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}