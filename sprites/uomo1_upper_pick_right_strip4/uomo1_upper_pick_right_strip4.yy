{
    "id": "b3637d0e-ae2c-45d2-99fa-ffa58213b321",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2843cb9b-e2f9-4311-af22-88300c5a111f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3637d0e-ae2c-45d2-99fa-ffa58213b321",
            "compositeImage": {
                "id": "c976df72-b1c4-48a8-8617-96ff9e24e5c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2843cb9b-e2f9-4311-af22-88300c5a111f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cebff94c-1206-4445-8a30-a946391f070f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2843cb9b-e2f9-4311-af22-88300c5a111f",
                    "LayerId": "77cdd022-285a-44d4-a929-c5b2cc1d1bfe"
                }
            ]
        },
        {
            "id": "3e00e73d-75fe-466b-b031-13206c1b9ff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3637d0e-ae2c-45d2-99fa-ffa58213b321",
            "compositeImage": {
                "id": "e7d50ad5-f42f-4411-bbd3-4572dd3daab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e00e73d-75fe-466b-b031-13206c1b9ff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6434ddd-bb4c-4689-b477-0f55ca53cadc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e00e73d-75fe-466b-b031-13206c1b9ff8",
                    "LayerId": "77cdd022-285a-44d4-a929-c5b2cc1d1bfe"
                }
            ]
        },
        {
            "id": "b04cefc9-4615-4d4e-abe6-ecebd690238f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3637d0e-ae2c-45d2-99fa-ffa58213b321",
            "compositeImage": {
                "id": "806a7f0e-5ae3-4e64-a83c-c425b64ebb09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04cefc9-4615-4d4e-abe6-ecebd690238f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4ba8f5-6aaf-49cd-ae54-d488e99bd8ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04cefc9-4615-4d4e-abe6-ecebd690238f",
                    "LayerId": "77cdd022-285a-44d4-a929-c5b2cc1d1bfe"
                }
            ]
        },
        {
            "id": "46d22ad9-aba0-4724-9b47-1fe7dee29c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3637d0e-ae2c-45d2-99fa-ffa58213b321",
            "compositeImage": {
                "id": "b31718e1-1b16-4417-9041-89a4ba3b3423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d22ad9-aba0-4724-9b47-1fe7dee29c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85ced16a-9804-4557-88ca-71ae81d1142f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d22ad9-aba0-4724-9b47-1fe7dee29c6e",
                    "LayerId": "77cdd022-285a-44d4-a929-c5b2cc1d1bfe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "77cdd022-285a-44d4-a929-c5b2cc1d1bfe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3637d0e-ae2c-45d2-99fa-ffa58213b321",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}