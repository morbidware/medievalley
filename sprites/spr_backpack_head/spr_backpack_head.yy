{
    "id": "c033b2e2-31ed-49f4-bcea-32fdc1f5845d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backpack_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec660e76-9c60-4af0-9dba-c81e127f8653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c033b2e2-31ed-49f4-bcea-32fdc1f5845d",
            "compositeImage": {
                "id": "a6c55af9-fa0a-427b-9c09-3911e8fc5ce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec660e76-9c60-4af0-9dba-c81e127f8653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81b32120-a42d-4788-8065-45760cc12960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec660e76-9c60-4af0-9dba-c81e127f8653",
                    "LayerId": "5a10d69f-ff59-43ff-bd0c-382559280a4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "5a10d69f-ff59-43ff-bd0c-382559280a4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c033b2e2-31ed-49f4-bcea-32fdc1f5845d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 6,
    "yorig": 7
}