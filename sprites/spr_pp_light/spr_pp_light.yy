{
    "id": "3e0f6c00-c456-4a9e-98e9-831db7285bca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pp_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "25233b67-9cfc-474a-ab48-30d7a825e48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e0f6c00-c456-4a9e-98e9-831db7285bca",
            "compositeImage": {
                "id": "b9c205b6-4819-4a7b-8e32-b2bb0a650178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25233b67-9cfc-474a-ab48-30d7a825e48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69e2eb5-362c-4d03-87e4-74e17289d2dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25233b67-9cfc-474a-ab48-30d7a825e48c",
                    "LayerId": "f32b192d-ad16-454c-9415-acd338b61b98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f32b192d-ad16-454c-9415-acd338b61b98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e0f6c00-c456-4a9e-98e9-831db7285bca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}