{
    "id": "cbc25ed2-db75-4dca-953c-3d8a5cfa8608",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_die_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14e3db27-8fda-436d-8614-5acc8daf6e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbc25ed2-db75-4dca-953c-3d8a5cfa8608",
            "compositeImage": {
                "id": "4d0fa0a3-0327-4323-a55c-2a87981b7041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14e3db27-8fda-436d-8614-5acc8daf6e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac355b19-f585-4594-b007-5985b3bd0e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14e3db27-8fda-436d-8614-5acc8daf6e81",
                    "LayerId": "cadb33a9-63ba-45ce-b7e3-10e16a609fa4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "cadb33a9-63ba-45ce-b7e3-10e16a609fa4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbc25ed2-db75-4dca-953c-3d8a5cfa8608",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}