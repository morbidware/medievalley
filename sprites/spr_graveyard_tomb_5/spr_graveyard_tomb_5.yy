{
    "id": "50694c3a-f7c5-4b22-a33e-b300e5efa084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a61e4847-8609-4bed-b573-07c7aa146074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50694c3a-f7c5-4b22-a33e-b300e5efa084",
            "compositeImage": {
                "id": "9b817794-eab3-494d-a41d-165af0a7a67c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61e4847-8609-4bed-b573-07c7aa146074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db6ef862-dcac-4581-98a3-d1c4a2979c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61e4847-8609-4bed-b573-07c7aa146074",
                    "LayerId": "c7f305db-6110-4496-9919-78c98f94d785"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7f305db-6110-4496-9919-78c98f94d785",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50694c3a-f7c5-4b22-a33e-b300e5efa084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 18,
    "yorig": 25
}