{
    "id": "c21dcbe7-113f-4735-b3fd-984b2db71744",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36c97bf8-58a9-4ef8-b01c-114106b93f6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c21dcbe7-113f-4735-b3fd-984b2db71744",
            "compositeImage": {
                "id": "3ff8b89b-47c4-4ebb-9e95-6a052075eb8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c97bf8-58a9-4ef8-b01c-114106b93f6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f5bb227-5778-4f26-87e8-8f3a2a8e62bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c97bf8-58a9-4ef8-b01c-114106b93f6c",
                    "LayerId": "a537a28c-762b-4a0b-96be-5935be7310d8"
                }
            ]
        },
        {
            "id": "d8fd6992-7da1-437c-b54d-63b668945d8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c21dcbe7-113f-4735-b3fd-984b2db71744",
            "compositeImage": {
                "id": "24da95aa-9e4a-4eac-928e-e80a887003dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8fd6992-7da1-437c-b54d-63b668945d8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db2c9f9-c273-4e52-9314-ecf8f9653341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8fd6992-7da1-437c-b54d-63b668945d8a",
                    "LayerId": "a537a28c-762b-4a0b-96be-5935be7310d8"
                }
            ]
        },
        {
            "id": "b03c861a-5f97-49b0-a007-f286ce6501bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c21dcbe7-113f-4735-b3fd-984b2db71744",
            "compositeImage": {
                "id": "fac730c3-f90e-462e-bf08-a7f4f4fa5d55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b03c861a-5f97-49b0-a007-f286ce6501bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49bb9496-970b-4871-922c-f5944f94efe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b03c861a-5f97-49b0-a007-f286ce6501bf",
                    "LayerId": "a537a28c-762b-4a0b-96be-5935be7310d8"
                }
            ]
        },
        {
            "id": "0f2d404e-f036-47a3-ae60-d6824453b99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c21dcbe7-113f-4735-b3fd-984b2db71744",
            "compositeImage": {
                "id": "ff107326-d32e-48e9-a959-f8ae31824d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f2d404e-f036-47a3-ae60-d6824453b99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dc0ae2d-4185-4afd-b31e-c2a144a7a7ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f2d404e-f036-47a3-ae60-d6824453b99b",
                    "LayerId": "a537a28c-762b-4a0b-96be-5935be7310d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a537a28c-762b-4a0b-96be-5935be7310d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c21dcbe7-113f-4735-b3fd-984b2db71744",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}