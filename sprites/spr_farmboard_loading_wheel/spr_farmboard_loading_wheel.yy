{
    "id": "834abef2-03b6-41d4-b43a-6574e3565a3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_loading_wheel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2fde64c-129d-4208-9462-7d7f74822bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "compositeImage": {
                "id": "bbefc00c-9409-414d-81bf-694a418ee6a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2fde64c-129d-4208-9462-7d7f74822bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77cba8d7-f0ff-4f7d-9ed8-634df6e9b4d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2fde64c-129d-4208-9462-7d7f74822bf1",
                    "LayerId": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401"
                }
            ]
        },
        {
            "id": "24ff4e51-ff40-4fa9-987d-9ed7120afec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "compositeImage": {
                "id": "99ab8ba1-9c0f-4510-aa0f-9ae1ac2135a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ff4e51-ff40-4fa9-987d-9ed7120afec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066947f5-3fe1-43e6-9ed7-5a0444924bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ff4e51-ff40-4fa9-987d-9ed7120afec5",
                    "LayerId": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401"
                }
            ]
        },
        {
            "id": "b4c27824-168e-4b2e-aac9-79a160ec4535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "compositeImage": {
                "id": "ee7c5470-05b9-4e7a-b4df-da538dc65b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c27824-168e-4b2e-aac9-79a160ec4535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94f36f81-3188-4abd-9282-e414971bad33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c27824-168e-4b2e-aac9-79a160ec4535",
                    "LayerId": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401"
                }
            ]
        },
        {
            "id": "982c2592-b8d5-4956-8d42-d5b1ba6b5e17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "compositeImage": {
                "id": "05623bb3-d960-4d4a-9dca-7375f5501ff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982c2592-b8d5-4956-8d42-d5b1ba6b5e17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a12f9e02-3a1c-4abd-a457-2fac872f66b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982c2592-b8d5-4956-8d42-d5b1ba6b5e17",
                    "LayerId": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401"
                }
            ]
        },
        {
            "id": "1b2df610-05d4-4c0e-8a56-9943871d38ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "compositeImage": {
                "id": "50e6a856-027d-4798-977b-9a8cba943bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b2df610-05d4-4c0e-8a56-9943871d38ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f696874b-092f-48b3-901f-01430528ba72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b2df610-05d4-4c0e-8a56-9943871d38ea",
                    "LayerId": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401"
                }
            ]
        },
        {
            "id": "79c2043d-3542-4d80-98aa-856f199332e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "compositeImage": {
                "id": "976a53a3-5142-454d-b318-02885d8fd307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c2043d-3542-4d80-98aa-856f199332e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e774ce-d69c-487d-ab94-610b7eefcc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c2043d-3542-4d80-98aa-856f199332e8",
                    "LayerId": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "02e6c2f3-6b8d-4c2b-b1ea-d4ee3f114401",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "834abef2-03b6-41d4-b43a-6574e3565a3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}