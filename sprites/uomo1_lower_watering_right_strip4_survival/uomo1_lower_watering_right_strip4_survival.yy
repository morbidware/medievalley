{
    "id": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_watering_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5b1eabd-7fe7-4ed6-8d9f-a6086c83409a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "compositeImage": {
                "id": "4f4aefbc-c466-40d4-9f75-bea3697b0b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b1eabd-7fe7-4ed6-8d9f-a6086c83409a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f002e4a-cf67-4b31-a512-cbef9f1df342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b1eabd-7fe7-4ed6-8d9f-a6086c83409a",
                    "LayerId": "be895b0d-5b1c-4b0e-9a36-e37179e26d80"
                }
            ]
        },
        {
            "id": "2c4a4329-0976-4009-97b4-ec8ef5289c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "compositeImage": {
                "id": "da570ec6-907a-4d71-a69f-c6c7f814bfd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c4a4329-0976-4009-97b4-ec8ef5289c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe63e7e1-cc2c-4540-b2a5-bad4a75e6a0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c4a4329-0976-4009-97b4-ec8ef5289c08",
                    "LayerId": "be895b0d-5b1c-4b0e-9a36-e37179e26d80"
                }
            ]
        },
        {
            "id": "ccae6ef9-740b-4681-bee3-b3cd5666eca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "compositeImage": {
                "id": "968f9cdb-8f0e-419c-92c9-676515fce2a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccae6ef9-740b-4681-bee3-b3cd5666eca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "051df593-eb1c-4f01-ae50-f4ffda72454b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccae6ef9-740b-4681-bee3-b3cd5666eca8",
                    "LayerId": "be895b0d-5b1c-4b0e-9a36-e37179e26d80"
                }
            ]
        },
        {
            "id": "0cd7a49b-85a7-486a-a089-9905b69c8049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "compositeImage": {
                "id": "fa9eb310-4153-439e-8b0b-b97542042782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd7a49b-85a7-486a-a089-9905b69c8049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "463dc614-3add-41ae-8b37-013fc3c166c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd7a49b-85a7-486a-a089-9905b69c8049",
                    "LayerId": "be895b0d-5b1c-4b0e-9a36-e37179e26d80"
                }
            ]
        },
        {
            "id": "4d3a3f5a-791b-457e-b903-33d6eb1f4ce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "compositeImage": {
                "id": "f3ec1a3c-d88f-44f3-a21b-96a79d3d614d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d3a3f5a-791b-457e-b903-33d6eb1f4ce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e632c5a-eebb-4cc2-8209-6b456e51b1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d3a3f5a-791b-457e-b903-33d6eb1f4ce0",
                    "LayerId": "be895b0d-5b1c-4b0e-9a36-e37179e26d80"
                }
            ]
        },
        {
            "id": "9b8f23dc-eff4-42f3-9461-cedb8b9cc8f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "compositeImage": {
                "id": "bccb95df-9510-4d61-bfa1-c1e3e32a914d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b8f23dc-eff4-42f3-9461-cedb8b9cc8f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7641ada-d424-4ffe-8af7-79953134a232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b8f23dc-eff4-42f3-9461-cedb8b9cc8f2",
                    "LayerId": "be895b0d-5b1c-4b0e-9a36-e37179e26d80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "be895b0d-5b1c-4b0e-9a36-e37179e26d80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be80cb40-3766-4140-bbe2-b2b1f49fb5d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}