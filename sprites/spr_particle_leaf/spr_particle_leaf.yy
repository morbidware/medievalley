{
    "id": "adc7644e-ebac-4bcf-a2c6-1a30df199861",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_leaf",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5076811f-978f-46c8-a22e-695c53ec6888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc7644e-ebac-4bcf-a2c6-1a30df199861",
            "compositeImage": {
                "id": "dc9d92bb-d12a-4fcf-8ef7-3458783bb72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5076811f-978f-46c8-a22e-695c53ec6888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbc0b71f-e5ee-4171-a4c6-798eecaf4673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5076811f-978f-46c8-a22e-695c53ec6888",
                    "LayerId": "a908bede-57ef-4870-b8a1-3b54b9e424be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "a908bede-57ef-4870-b8a1-3b54b9e424be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adc7644e-ebac-4bcf-a2c6-1a30df199861",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}