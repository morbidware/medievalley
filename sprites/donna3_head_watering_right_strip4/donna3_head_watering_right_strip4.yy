{
    "id": "34a33387-1e90-4fd7-a407-5a7759e845e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "597c7035-b59a-41dd-9e58-871388013f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34a33387-1e90-4fd7-a407-5a7759e845e8",
            "compositeImage": {
                "id": "17bcd050-6073-480b-b727-203d191ff409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "597c7035-b59a-41dd-9e58-871388013f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb56303-4fc1-44c6-862d-c7b9477e2b7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "597c7035-b59a-41dd-9e58-871388013f96",
                    "LayerId": "965b4a14-fc6b-4151-ad41-7d8c4cfdee8c"
                }
            ]
        },
        {
            "id": "1b0703cc-465e-4375-bc6f-026aee411d94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34a33387-1e90-4fd7-a407-5a7759e845e8",
            "compositeImage": {
                "id": "aaf5dee7-a378-419b-904e-5ab3be362abd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b0703cc-465e-4375-bc6f-026aee411d94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "779601f2-fa89-45b9-899a-5d6a7e937014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b0703cc-465e-4375-bc6f-026aee411d94",
                    "LayerId": "965b4a14-fc6b-4151-ad41-7d8c4cfdee8c"
                }
            ]
        },
        {
            "id": "b4bf68b9-4098-4957-90d7-5e2af84aee2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34a33387-1e90-4fd7-a407-5a7759e845e8",
            "compositeImage": {
                "id": "a7120a4e-d9ac-456c-bd1b-685e2fdf6940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4bf68b9-4098-4957-90d7-5e2af84aee2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3871c6-f70f-4b59-a4cb-f331f8f9b374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4bf68b9-4098-4957-90d7-5e2af84aee2b",
                    "LayerId": "965b4a14-fc6b-4151-ad41-7d8c4cfdee8c"
                }
            ]
        },
        {
            "id": "660a9af4-ef66-4fde-8a61-f79b99d745bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34a33387-1e90-4fd7-a407-5a7759e845e8",
            "compositeImage": {
                "id": "3c6e1775-6bd4-434a-8b54-13e0ae7c7c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660a9af4-ef66-4fde-8a61-f79b99d745bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da6fe56c-8b10-4d86-ac4a-5c12b4abff61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660a9af4-ef66-4fde-8a61-f79b99d745bb",
                    "LayerId": "965b4a14-fc6b-4151-ad41-7d8c4cfdee8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "965b4a14-fc6b-4151-ad41-7d8c4cfdee8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34a33387-1e90-4fd7-a407-5a7759e845e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}