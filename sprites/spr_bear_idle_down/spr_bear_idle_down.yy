{
    "id": "eaef265b-d3bd-4b9b-9a34-32fe468254fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30dd9149-091d-42ba-bc81-c39823620a8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eaef265b-d3bd-4b9b-9a34-32fe468254fc",
            "compositeImage": {
                "id": "8b08ef71-966a-40ca-a627-df12462dc944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30dd9149-091d-42ba-bc81-c39823620a8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3490716b-44af-41fa-9a05-de16c1dfc2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30dd9149-091d-42ba-bc81-c39823620a8a",
                    "LayerId": "4acf5b34-3afd-4c8c-9239-fb9eb48fc127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4acf5b34-3afd-4c8c-9239-fb9eb48fc127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eaef265b-d3bd-4b9b-9a34-32fe468254fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}