{
    "id": "645f6f44-be67-44da-840a-5ade1b27cdc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_melee_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1c9ddef-e216-44de-b918-3533e8f5293d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "compositeImage": {
                "id": "ad7a3cd4-a80a-477c-bd84-16bf27985b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c9ddef-e216-44de-b918-3533e8f5293d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a85a82b6-e6b9-4dbc-9386-f47b546c8fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c9ddef-e216-44de-b918-3533e8f5293d",
                    "LayerId": "0bbbc771-d73c-4a55-a1aa-01925ba92493"
                }
            ]
        },
        {
            "id": "c17818f6-6950-428c-baea-8beded41afc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "compositeImage": {
                "id": "6633dc40-9810-4de0-a56a-cab91a262d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c17818f6-6950-428c-baea-8beded41afc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aafaeca9-fe8f-4007-b637-3a216ec45268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17818f6-6950-428c-baea-8beded41afc1",
                    "LayerId": "0bbbc771-d73c-4a55-a1aa-01925ba92493"
                }
            ]
        },
        {
            "id": "c963f3e2-f184-4894-9776-510cf99e829f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "compositeImage": {
                "id": "1628b495-e2a8-4636-8dac-63a990589ece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c963f3e2-f184-4894-9776-510cf99e829f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd24b24-754b-475d-b706-652b32641a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c963f3e2-f184-4894-9776-510cf99e829f",
                    "LayerId": "0bbbc771-d73c-4a55-a1aa-01925ba92493"
                }
            ]
        },
        {
            "id": "0e61bb2b-d605-423a-b729-f1a901238807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "compositeImage": {
                "id": "a091e8f9-130a-45ff-871e-1ea3574dfe5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e61bb2b-d605-423a-b729-f1a901238807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ea490ba-835a-43ce-a35c-58d0adbda7d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e61bb2b-d605-423a-b729-f1a901238807",
                    "LayerId": "0bbbc771-d73c-4a55-a1aa-01925ba92493"
                }
            ]
        },
        {
            "id": "8b9dfae8-6fb3-48c3-86ba-e8b514505565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "compositeImage": {
                "id": "6c85688e-22e9-4188-a4bf-e005b0ce79e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b9dfae8-6fb3-48c3-86ba-e8b514505565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1652ea65-1983-421d-8c56-ef912cc9c2e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b9dfae8-6fb3-48c3-86ba-e8b514505565",
                    "LayerId": "0bbbc771-d73c-4a55-a1aa-01925ba92493"
                }
            ]
        },
        {
            "id": "b426c3e1-384d-48a8-a7bb-7310eef7b057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "compositeImage": {
                "id": "d0f89330-02ac-4cc0-959b-903c7b87a2b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b426c3e1-384d-48a8-a7bb-7310eef7b057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38347fcc-5b38-4c9b-a5b4-9d0ee4382010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b426c3e1-384d-48a8-a7bb-7310eef7b057",
                    "LayerId": "0bbbc771-d73c-4a55-a1aa-01925ba92493"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0bbbc771-d73c-4a55-a1aa-01925ba92493",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "645f6f44-be67-44da-840a-5ade1b27cdc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}