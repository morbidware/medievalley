{
    "id": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mailbox_container_read",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00d756aa-4e5b-4bdc-b11f-74f49c082e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "12dfe063-afe2-4ecd-aceb-81db594f0129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d756aa-4e5b-4bdc-b11f-74f49c082e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb02c26-9840-4fc2-8393-a3f4769ec7fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d756aa-4e5b-4bdc-b11f-74f49c082e5b",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "3ced457a-d56e-42cf-9597-a53542c33978",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "6c57acc8-14e9-4cd4-b411-e8c987aadf81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ced457a-d56e-42cf-9597-a53542c33978",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e9d79d-c1cd-416c-b200-805600aa78ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ced457a-d56e-42cf-9597-a53542c33978",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "5cd24955-cdf3-4e5d-8cba-c15de828a408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "7a99ec39-43b9-4fc5-a4aa-13810fdea2e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cd24955-cdf3-4e5d-8cba-c15de828a408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcd5e2b5-829e-47bf-a48d-b33ddc429f02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cd24955-cdf3-4e5d-8cba-c15de828a408",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "bf55081d-539c-4106-8589-4b8dbdb94d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "aaf28347-1570-47e4-9606-e9b16d85c45f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf55081d-539c-4106-8589-4b8dbdb94d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b3f744-f4d1-4f18-b26b-df1908114033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf55081d-539c-4106-8589-4b8dbdb94d0b",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "5602c818-9df4-4d0a-b607-e845012b5fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "58a3708a-c548-4f0f-9560-656ba055dadd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5602c818-9df4-4d0a-b607-e845012b5fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0846e8bd-ba91-46a3-b5ff-698dd4acc226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5602c818-9df4-4d0a-b607-e845012b5fe3",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "7afa6b75-7c02-46b6-a708-53e10eb8812f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "f22773a0-9337-4c46-9a27-e6aad0353bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7afa6b75-7c02-46b6-a708-53e10eb8812f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7e248c-ae07-4c84-a329-2352f0c75827",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7afa6b75-7c02-46b6-a708-53e10eb8812f",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "bf61fdfe-5986-4ff8-af36-4072c731f7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "9835a1f2-a49a-4555-983c-7364d5db259e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf61fdfe-5986-4ff8-af36-4072c731f7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "874690ec-10f2-4974-bf23-1cc101a6abd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf61fdfe-5986-4ff8-af36-4072c731f7b8",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "2b2ea37f-0517-4660-a9c6-b0fa5c03094a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "aceaa5b6-8790-4448-858b-6f0c6c902670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b2ea37f-0517-4660-a9c6-b0fa5c03094a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee0655b1-ca49-4195-9cb4-8771aa9ac0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b2ea37f-0517-4660-a9c6-b0fa5c03094a",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        },
        {
            "id": "0a157e62-2ab5-4af3-ac28-59a4dfe5be97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "compositeImage": {
                "id": "21ede128-95a2-4d1c-9b5e-878529b41163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a157e62-2ab5-4af3-ac28-59a4dfe5be97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7649722e-bf9c-4bca-af31-5bcd9080139b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a157e62-2ab5-4af3-ac28-59a4dfe5be97",
                    "LayerId": "ca996c73-7d77-46f6-b9cc-d75a9000e494"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "ca996c73-7d77-46f6-b9cc-d75a9000e494",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77e6596f-3c8f-4a81-89c2-a76dfee588ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 0,
    "yorig": 0
}