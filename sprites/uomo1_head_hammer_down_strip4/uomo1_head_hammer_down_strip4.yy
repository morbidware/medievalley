{
    "id": "8e4d4f68-15b2-4aa3-8a83-b35cb9580b8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9772b35d-efc2-4f1c-939c-b1945a7518ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e4d4f68-15b2-4aa3-8a83-b35cb9580b8b",
            "compositeImage": {
                "id": "28485818-2b89-4807-bf0c-fb3390197dd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9772b35d-efc2-4f1c-939c-b1945a7518ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ec779c-3a31-4cf9-bc98-65f2d528a993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9772b35d-efc2-4f1c-939c-b1945a7518ad",
                    "LayerId": "32e83188-fd72-473f-88d9-62f4e73290bc"
                }
            ]
        },
        {
            "id": "fffb78a3-dd35-4dca-9fb0-5ca037e06e55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e4d4f68-15b2-4aa3-8a83-b35cb9580b8b",
            "compositeImage": {
                "id": "3c1a895f-0b27-4ccf-8ceb-0723287792b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fffb78a3-dd35-4dca-9fb0-5ca037e06e55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e48dac2-35e0-4438-95a4-ca7151e22c07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fffb78a3-dd35-4dca-9fb0-5ca037e06e55",
                    "LayerId": "32e83188-fd72-473f-88d9-62f4e73290bc"
                }
            ]
        },
        {
            "id": "50e7ab83-30c9-4602-a83a-eb15b82f8de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e4d4f68-15b2-4aa3-8a83-b35cb9580b8b",
            "compositeImage": {
                "id": "5d065499-9d9f-4eef-8894-52ce43450029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50e7ab83-30c9-4602-a83a-eb15b82f8de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a9c05f-41fb-4dc5-a451-5b8f639c74af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50e7ab83-30c9-4602-a83a-eb15b82f8de5",
                    "LayerId": "32e83188-fd72-473f-88d9-62f4e73290bc"
                }
            ]
        },
        {
            "id": "1d9299ea-e934-42ab-9c3a-8b16d84daf18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e4d4f68-15b2-4aa3-8a83-b35cb9580b8b",
            "compositeImage": {
                "id": "65ed2166-797a-44a4-b647-c8ce84bc29c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d9299ea-e934-42ab-9c3a-8b16d84daf18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925d9085-f00c-4848-b218-42b2da89f475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d9299ea-e934-42ab-9c3a-8b16d84daf18",
                    "LayerId": "32e83188-fd72-473f-88d9-62f4e73290bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "32e83188-fd72-473f-88d9-62f4e73290bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e4d4f68-15b2-4aa3-8a83-b35cb9580b8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}