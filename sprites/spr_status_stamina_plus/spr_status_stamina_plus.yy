{
    "id": "f9ef179f-672a-480c-901b-f28ba350d7e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_status_stamina_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99cc3b4b-0eff-4128-af71-130df39a5aff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9ef179f-672a-480c-901b-f28ba350d7e0",
            "compositeImage": {
                "id": "974c1c15-a608-4079-bb69-9eda2d6aa05f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cc3b4b-0eff-4128-af71-130df39a5aff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ae3fad-f6b1-4fbd-b07b-df4537410d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cc3b4b-0eff-4128-af71-130df39a5aff",
                    "LayerId": "7fb1d8db-6fa9-425f-b4ca-1e747b1d6a9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "7fb1d8db-6fa9-425f-b4ca-1e747b1d6a9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9ef179f-672a-480c-901b-f28ba350d7e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}