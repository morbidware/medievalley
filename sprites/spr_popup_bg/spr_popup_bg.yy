{
    "id": "786b9a3d-ae78-4996-a76a-ee7aa536d462",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_popup_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "493b7f42-59fc-46bb-815f-8a98e664da09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "786b9a3d-ae78-4996-a76a-ee7aa536d462",
            "compositeImage": {
                "id": "ba686c9a-a261-456d-a842-1830ba89fba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493b7f42-59fc-46bb-815f-8a98e664da09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057e2194-d701-464d-9b5b-677cdc0d3e43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493b7f42-59fc-46bb-815f-8a98e664da09",
                    "LayerId": "8b416e01-2984-4898-b2eb-cadbf1868da7"
                }
            ]
        }
    ],
    "gridX": 160,
    "gridY": 90,
    "height": 90,
    "layers": [
        {
            "id": "8b416e01-2984-4898-b2eb-cadbf1868da7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "786b9a3d-ae78-4996-a76a-ee7aa536d462",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 45
}