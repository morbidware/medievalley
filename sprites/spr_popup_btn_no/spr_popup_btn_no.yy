{
    "id": "5e66c6c8-50e7-4444-9e9a-5ba4355b2506",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_popup_btn_no",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a27d89a5-1991-4068-acfe-b6aa3935b59e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e66c6c8-50e7-4444-9e9a-5ba4355b2506",
            "compositeImage": {
                "id": "1b1f51da-52c3-4f66-9421-5d3a849c3737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a27d89a5-1991-4068-acfe-b6aa3935b59e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "936954a8-4b3a-48aa-88ce-385fe9e4b146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a27d89a5-1991-4068-acfe-b6aa3935b59e",
                    "LayerId": "f34d3d02-d046-4b65-8d19-370af2f927b5"
                }
            ]
        }
    ],
    "gridX": 160,
    "gridY": 90,
    "height": 12,
    "layers": [
        {
            "id": "f34d3d02-d046-4b65-8d19-370af2f927b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e66c6c8-50e7-4444-9e9a-5ba4355b2506",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 6
}