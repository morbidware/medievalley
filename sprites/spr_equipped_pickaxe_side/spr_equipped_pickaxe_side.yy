{
    "id": "7c62d90f-901a-4a3c-b5e6-befddf6b7d2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_pickaxe_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a901ed50-8db3-4d32-9a3a-987a281f3688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c62d90f-901a-4a3c-b5e6-befddf6b7d2f",
            "compositeImage": {
                "id": "b056d63c-3077-460e-8016-e7265c88aa9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a901ed50-8db3-4d32-9a3a-987a281f3688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f4fbc5e-21a1-46b2-a3a1-3c35071581ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a901ed50-8db3-4d32-9a3a-987a281f3688",
                    "LayerId": "2531fcf0-3e7a-4876-975a-f8ac4e72f9c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2531fcf0-3e7a-4876-975a-f8ac4e72f9c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c62d90f-901a-4a3c-b5e6-befddf6b7d2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 13
}