{
    "id": "9cdc15bc-7697-4a88-8585-f965184338ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_pick_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a981d00a-55f9-42ff-bdc2-f80b60f40bef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "compositeImage": {
                "id": "3dfe08e4-6212-4446-9eab-1caf6d55cd39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a981d00a-55f9-42ff-bdc2-f80b60f40bef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3cc2b18-c64a-4e05-8e01-819af032b11f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a981d00a-55f9-42ff-bdc2-f80b60f40bef",
                    "LayerId": "108393de-d218-4a0b-b95b-8281641e23cc"
                }
            ]
        },
        {
            "id": "0897b2ce-fdb2-4d71-8a5e-fe76dd9364a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "compositeImage": {
                "id": "b67abe03-48f1-41f4-a927-f1374704042c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0897b2ce-fdb2-4d71-8a5e-fe76dd9364a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7854460c-d022-4079-a420-f22ff57c991e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0897b2ce-fdb2-4d71-8a5e-fe76dd9364a5",
                    "LayerId": "108393de-d218-4a0b-b95b-8281641e23cc"
                }
            ]
        },
        {
            "id": "59f8e8db-b533-4574-b9dd-0dcf304ac6dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "compositeImage": {
                "id": "66c93193-dee1-46ae-adb5-a788ef3fa3a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f8e8db-b533-4574-b9dd-0dcf304ac6dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edadc6cf-c06e-46d0-a817-075c7b7b0d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f8e8db-b533-4574-b9dd-0dcf304ac6dc",
                    "LayerId": "108393de-d218-4a0b-b95b-8281641e23cc"
                }
            ]
        },
        {
            "id": "31ef8138-fb3d-4292-a184-04f100fc22f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "compositeImage": {
                "id": "8384549f-e4d6-4fa9-a074-37da64a90369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31ef8138-fb3d-4292-a184-04f100fc22f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e295e103-0a91-4cd6-8cb4-8490d4f08e99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31ef8138-fb3d-4292-a184-04f100fc22f6",
                    "LayerId": "108393de-d218-4a0b-b95b-8281641e23cc"
                }
            ]
        },
        {
            "id": "561fc480-33db-474b-8512-607bca4ccc57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "compositeImage": {
                "id": "af3aa837-c25f-45d6-9a2a-c23ced6d620b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "561fc480-33db-474b-8512-607bca4ccc57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "def07510-ef66-435b-afef-a293a84bb2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "561fc480-33db-474b-8512-607bca4ccc57",
                    "LayerId": "108393de-d218-4a0b-b95b-8281641e23cc"
                }
            ]
        },
        {
            "id": "c684db97-e792-46fc-9f2b-cdfc2726d3a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "compositeImage": {
                "id": "b3f03717-b17c-4780-9afd-76caeb13f53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c684db97-e792-46fc-9f2b-cdfc2726d3a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7922fb57-14d2-4b51-aafa-95cc0afa6798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c684db97-e792-46fc-9f2b-cdfc2726d3a3",
                    "LayerId": "108393de-d218-4a0b-b95b-8281641e23cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "108393de-d218-4a0b-b95b-8281641e23cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cdc15bc-7697-4a88-8585-f965184338ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}