{
    "id": "c041b040-d4a0-47bd-aa14-71fb745de9a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_merchant_cancel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc02b501-9aa1-413b-8024-ca5fb5a9225a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c041b040-d4a0-47bd-aa14-71fb745de9a3",
            "compositeImage": {
                "id": "4c33c4bb-90bb-4c4d-a333-34bc513c63bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc02b501-9aa1-413b-8024-ca5fb5a9225a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02404f36-5e4b-4e3d-a41c-838f5c24349b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc02b501-9aa1-413b-8024-ca5fb5a9225a",
                    "LayerId": "174f571c-ac39-4495-a2bd-ed546dc22741"
                }
            ]
        },
        {
            "id": "fe45ba34-c162-42bb-bc82-322e71605186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c041b040-d4a0-47bd-aa14-71fb745de9a3",
            "compositeImage": {
                "id": "3722c79a-c3f2-4c41-90d4-a89f93c6fa68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe45ba34-c162-42bb-bc82-322e71605186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547ae8f3-594c-4601-88ad-7771b881146d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe45ba34-c162-42bb-bc82-322e71605186",
                    "LayerId": "174f571c-ac39-4495-a2bd-ed546dc22741"
                }
            ]
        },
        {
            "id": "d38cf3c6-1ae9-42e2-b1ae-a02d1a6ffc3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c041b040-d4a0-47bd-aa14-71fb745de9a3",
            "compositeImage": {
                "id": "10e9deab-d2c0-4a3f-8a79-4c7e9cee0f41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d38cf3c6-1ae9-42e2-b1ae-a02d1a6ffc3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb5680b-7804-4088-a64c-69a28bde59b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d38cf3c6-1ae9-42e2-b1ae-a02d1a6ffc3d",
                    "LayerId": "174f571c-ac39-4495-a2bd-ed546dc22741"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "174f571c-ac39-4495-a2bd-ed546dc22741",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c041b040-d4a0-47bd-aa14-71fb745de9a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 5
}