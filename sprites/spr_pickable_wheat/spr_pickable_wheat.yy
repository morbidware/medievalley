{
    "id": "0ee9e069-b174-46d3-b329-54e476264729",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_wheat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76939cff-3d2c-4da6-b7e4-a18ea4460127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ee9e069-b174-46d3-b329-54e476264729",
            "compositeImage": {
                "id": "b6246c3d-f724-45f8-ab16-f09f849d88a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76939cff-3d2c-4da6-b7e4-a18ea4460127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb55237c-f380-415f-b0b8-9a7b086ad6db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76939cff-3d2c-4da6-b7e4-a18ea4460127",
                    "LayerId": "ecacb1e7-e5e8-4e39-b0de-1d370b151ee9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ecacb1e7-e5e8-4e39-b0de-1d370b151ee9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ee9e069-b174-46d3-b329-54e476264729",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 9
}