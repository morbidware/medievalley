{
    "id": "5a28ddfc-36f9-4ffd-8f52-8e7b8e1e3bb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ab4674a-f45f-4305-83d8-90b7c952ef9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a28ddfc-36f9-4ffd-8f52-8e7b8e1e3bb9",
            "compositeImage": {
                "id": "b102f982-8da3-4a02-b45d-00091a0130ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab4674a-f45f-4305-83d8-90b7c952ef9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca60dfdb-cad6-415b-9bff-06e965fec02c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab4674a-f45f-4305-83d8-90b7c952ef9f",
                    "LayerId": "96b6070d-032d-4e18-abda-b500b124f77b"
                }
            ]
        },
        {
            "id": "3e5616b6-7d42-4a33-bcb7-29942cc8a071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a28ddfc-36f9-4ffd-8f52-8e7b8e1e3bb9",
            "compositeImage": {
                "id": "7655f55d-079e-4ebe-bac3-0144f645af83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e5616b6-7d42-4a33-bcb7-29942cc8a071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5971c77e-1973-40fb-a40b-808bec456484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e5616b6-7d42-4a33-bcb7-29942cc8a071",
                    "LayerId": "96b6070d-032d-4e18-abda-b500b124f77b"
                }
            ]
        },
        {
            "id": "84f87eb6-a073-4a0f-8efb-a6bef13c2bcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a28ddfc-36f9-4ffd-8f52-8e7b8e1e3bb9",
            "compositeImage": {
                "id": "aed2bcd9-990b-453d-8b7c-d9873bde0094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84f87eb6-a073-4a0f-8efb-a6bef13c2bcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81d53359-ac2d-4bdd-a87e-8c30c951632e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84f87eb6-a073-4a0f-8efb-a6bef13c2bcd",
                    "LayerId": "96b6070d-032d-4e18-abda-b500b124f77b"
                }
            ]
        },
        {
            "id": "dc0d38bf-136f-4a90-860c-d36d341a304f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a28ddfc-36f9-4ffd-8f52-8e7b8e1e3bb9",
            "compositeImage": {
                "id": "cabc53bd-52bb-4ae4-b10a-0c2013f5b098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc0d38bf-136f-4a90-860c-d36d341a304f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78cec79b-141f-4488-9906-e869ebf81b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc0d38bf-136f-4a90-860c-d36d341a304f",
                    "LayerId": "96b6070d-032d-4e18-abda-b500b124f77b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "96b6070d-032d-4e18-abda-b500b124f77b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a28ddfc-36f9-4ffd-8f52-8e7b8e1e3bb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}