{
    "id": "28fc08b3-41e7-493b-9df4-38afb6b972d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_suffer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 8,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7d4e9af-ba44-4b15-9df0-02a378009b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28fc08b3-41e7-493b-9df4-38afb6b972d7",
            "compositeImage": {
                "id": "4ca4d8fa-e8c7-421c-8ea5-ab7ecae2c1e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7d4e9af-ba44-4b15-9df0-02a378009b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7d01efd-f462-4fb6-b054-0132bf152949",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d4e9af-ba44-4b15-9df0-02a378009b89",
                    "LayerId": "906a120d-84ab-4c16-b218-f9c4f1b8d76f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "906a120d-84ab-4c16-b218-f9c4f1b8d76f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28fc08b3-41e7-493b-9df4-38afb6b972d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}