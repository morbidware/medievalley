{
    "id": "d32bdbb7-2240-4dd2-96b0-c8d7242b11cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_barsmall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09088db6-4fc5-4663-9994-015d59502d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d32bdbb7-2240-4dd2-96b0-c8d7242b11cf",
            "compositeImage": {
                "id": "471db42f-04f8-4272-b4ef-32841900fd9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09088db6-4fc5-4663-9994-015d59502d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a1a4a0-3aae-4aee-9e8a-1054eb353dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09088db6-4fc5-4663-9994-015d59502d9b",
                    "LayerId": "8e06b69b-ad1f-42b0-99b2-fd62ccca5a45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "8e06b69b-ad1f-42b0-99b2-fd62ccca5a45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d32bdbb7-2240-4dd2-96b0-c8d7242b11cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 9
}