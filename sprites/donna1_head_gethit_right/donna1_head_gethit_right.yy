{
    "id": "5c30362c-07b5-45ed-be30-ec685e7abe62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f57854b-9bca-423f-90d8-31ebd3e8fcdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c30362c-07b5-45ed-be30-ec685e7abe62",
            "compositeImage": {
                "id": "18e8a91e-19cf-4253-befd-4e84c456b063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f57854b-9bca-423f-90d8-31ebd3e8fcdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99f8229-939f-4184-9e17-2942e4f735b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f57854b-9bca-423f-90d8-31ebd3e8fcdf",
                    "LayerId": "44fabc92-f593-4d07-b905-db3322259a77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44fabc92-f593-4d07-b905-db3322259a77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c30362c-07b5-45ed-be30-ec685e7abe62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}