{
    "id": "f70ae196-9000-4521-bd5e-1f22f0aaa9b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e91fb860-7d8c-4992-b804-61423dce2aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70ae196-9000-4521-bd5e-1f22f0aaa9b9",
            "compositeImage": {
                "id": "8137ec94-cd60-47b5-a67e-e1c8fb58710f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91fb860-7d8c-4992-b804-61423dce2aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f376b7ec-d3ab-47dc-bbe1-f6812d5aa567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91fb860-7d8c-4992-b804-61423dce2aac",
                    "LayerId": "62412509-6e18-4445-a858-16d4f234c9c4"
                }
            ]
        },
        {
            "id": "1b215088-b469-4e3d-a05c-321302ed9c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70ae196-9000-4521-bd5e-1f22f0aaa9b9",
            "compositeImage": {
                "id": "406a7b91-7cb1-4ad2-909e-35d4a95df519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b215088-b469-4e3d-a05c-321302ed9c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e21630-ca76-498b-a71d-51d44e984685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b215088-b469-4e3d-a05c-321302ed9c27",
                    "LayerId": "62412509-6e18-4445-a858-16d4f234c9c4"
                }
            ]
        },
        {
            "id": "380738f1-d2b2-46fe-a337-7255c801071b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70ae196-9000-4521-bd5e-1f22f0aaa9b9",
            "compositeImage": {
                "id": "243e4ad1-b96c-4511-baee-bef705fd3d8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380738f1-d2b2-46fe-a337-7255c801071b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d111272-578d-4e7f-b54f-c1b7ed1be01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380738f1-d2b2-46fe-a337-7255c801071b",
                    "LayerId": "62412509-6e18-4445-a858-16d4f234c9c4"
                }
            ]
        },
        {
            "id": "46c48ee9-64b4-4587-9cdf-bb59f7a315e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f70ae196-9000-4521-bd5e-1f22f0aaa9b9",
            "compositeImage": {
                "id": "712dd23b-5139-406c-9acc-cb18bd9d9352",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c48ee9-64b4-4587-9cdf-bb59f7a315e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d6e4a2-48a5-4572-a7d3-638ce4bdb62e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c48ee9-64b4-4587-9cdf-bb59f7a315e0",
                    "LayerId": "62412509-6e18-4445-a858-16d4f234c9c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "62412509-6e18-4445-a858-16d4f234c9c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f70ae196-9000-4521-bd5e-1f22f0aaa9b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}