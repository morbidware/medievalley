{
    "id": "db16e4f2-b8fe-4d20-9353-52cdc55ca6b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "792c97c8-1787-49b6-99a9-30c2bb33861d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db16e4f2-b8fe-4d20-9353-52cdc55ca6b0",
            "compositeImage": {
                "id": "f7d67adb-f713-423d-8836-46a6b6c739ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "792c97c8-1787-49b6-99a9-30c2bb33861d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ab55b4-e4bf-479b-8118-4f3c8307f907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "792c97c8-1787-49b6-99a9-30c2bb33861d",
                    "LayerId": "6e9d4187-a3b6-4f7d-b224-0314438c8a97"
                }
            ]
        },
        {
            "id": "353aec4f-ff7c-42ce-92e6-075db1e108db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db16e4f2-b8fe-4d20-9353-52cdc55ca6b0",
            "compositeImage": {
                "id": "c421f60d-05a9-4f16-8d08-ac2b855b065d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "353aec4f-ff7c-42ce-92e6-075db1e108db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a68d295b-c539-4d7c-90a8-797e94dbdc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "353aec4f-ff7c-42ce-92e6-075db1e108db",
                    "LayerId": "6e9d4187-a3b6-4f7d-b224-0314438c8a97"
                }
            ]
        },
        {
            "id": "d7c8aa8e-3eaf-488e-9b31-783ea9802598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db16e4f2-b8fe-4d20-9353-52cdc55ca6b0",
            "compositeImage": {
                "id": "fcf4d0a9-c67f-4f26-9d61-c5670dae65cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7c8aa8e-3eaf-488e-9b31-783ea9802598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4da002e9-3003-433d-ac79-3aaa371d4159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7c8aa8e-3eaf-488e-9b31-783ea9802598",
                    "LayerId": "6e9d4187-a3b6-4f7d-b224-0314438c8a97"
                }
            ]
        },
        {
            "id": "68b74c49-6ab8-496a-a67e-60fb8fb981fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db16e4f2-b8fe-4d20-9353-52cdc55ca6b0",
            "compositeImage": {
                "id": "c1ca218a-f8f1-4592-8bd3-92fd9c7c6632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68b74c49-6ab8-496a-a67e-60fb8fb981fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61780c6a-eea6-421c-86d5-9b1d587858de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68b74c49-6ab8-496a-a67e-60fb8fb981fa",
                    "LayerId": "6e9d4187-a3b6-4f7d-b224-0314438c8a97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6e9d4187-a3b6-4f7d-b224-0314438c8a97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db16e4f2-b8fe-4d20-9353-52cdc55ca6b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}