{
    "id": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_walk_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "819a0099-cd68-4111-a47a-e9a48f400d5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "compositeImage": {
                "id": "c118fd39-efa3-4cae-83f3-d887c8a6c46c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "819a0099-cd68-4111-a47a-e9a48f400d5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb37249-f522-42d6-ad26-b5cdd51f93f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "819a0099-cd68-4111-a47a-e9a48f400d5f",
                    "LayerId": "8f76c562-f220-4e92-9c45-5ad98c4520df"
                }
            ]
        },
        {
            "id": "38b3129d-d363-434c-b09b-12fb4dddf7c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "compositeImage": {
                "id": "ccfa4e59-9950-4506-a943-f2aa8bf1fa3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b3129d-d363-434c-b09b-12fb4dddf7c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ebf80d-597c-40d3-8ee6-63d722e537a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b3129d-d363-434c-b09b-12fb4dddf7c0",
                    "LayerId": "8f76c562-f220-4e92-9c45-5ad98c4520df"
                }
            ]
        },
        {
            "id": "4a8bd7ea-b6a2-40ee-97e8-fad153cafc42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "compositeImage": {
                "id": "420ddfa9-da78-4f85-888f-3da4eddabc77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a8bd7ea-b6a2-40ee-97e8-fad153cafc42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a3ea01-81dd-4fda-a827-13f2efadd89d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a8bd7ea-b6a2-40ee-97e8-fad153cafc42",
                    "LayerId": "8f76c562-f220-4e92-9c45-5ad98c4520df"
                }
            ]
        },
        {
            "id": "ddadf1cf-4f1c-4af3-ae35-39233e8ee4b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "compositeImage": {
                "id": "e989e44e-f92e-4488-9387-a5a011356b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddadf1cf-4f1c-4af3-ae35-39233e8ee4b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "416746d7-1ab9-49d2-9a3f-54bb5ec0a93a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddadf1cf-4f1c-4af3-ae35-39233e8ee4b7",
                    "LayerId": "8f76c562-f220-4e92-9c45-5ad98c4520df"
                }
            ]
        },
        {
            "id": "0b29fc11-a73b-48e2-b81c-178bb72b477b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "compositeImage": {
                "id": "f55814d3-c433-4e5d-9275-a9c6642d1c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b29fc11-a73b-48e2-b81c-178bb72b477b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44876cab-16d0-4dc5-8430-d5a57fe02daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b29fc11-a73b-48e2-b81c-178bb72b477b",
                    "LayerId": "8f76c562-f220-4e92-9c45-5ad98c4520df"
                }
            ]
        },
        {
            "id": "76844f46-2af9-4222-b103-cf30248f217f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "compositeImage": {
                "id": "9c4a5c2e-587c-4bb7-a741-f7b02be1edad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76844f46-2af9-4222-b103-cf30248f217f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd67e6d9-5f42-4ede-ad0a-1e8c977e0854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76844f46-2af9-4222-b103-cf30248f217f",
                    "LayerId": "8f76c562-f220-4e92-9c45-5ad98c4520df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f76c562-f220-4e92-9c45-5ad98c4520df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a953efb-76f1-438c-b24d-9a8ff98bfba8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}