{
    "id": "27269ab5-ca63-48a5-8279-25488ca6325a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_die_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 10,
    "bbox_right": 21,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92353f67-10d1-49be-a958-4ecb71600097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27269ab5-ca63-48a5-8279-25488ca6325a",
            "compositeImage": {
                "id": "02ac3ddb-1f7a-4bfe-8e01-f6d487339417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92353f67-10d1-49be-a958-4ecb71600097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f3a82d-95b0-448a-93bb-47d18e6c30fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92353f67-10d1-49be-a958-4ecb71600097",
                    "LayerId": "da10f270-5cf6-4181-8091-3abbd396889a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "da10f270-5cf6-4181-8091-3abbd396889a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27269ab5-ca63-48a5-8279-25488ca6325a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}