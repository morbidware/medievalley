{
    "id": "8778853d-d8ad-48c0-aa00-87db644d7718",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_challenge_malus_play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f965770e-3bc8-4e43-ad96-93a5b7bd1839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8778853d-d8ad-48c0-aa00-87db644d7718",
            "compositeImage": {
                "id": "0ddbfb4c-8931-49a7-9438-5d4545bc5e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f965770e-3bc8-4e43-ad96-93a5b7bd1839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657eae32-7b9f-4e20-ae35-8e7b54ef068a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f965770e-3bc8-4e43-ad96-93a5b7bd1839",
                    "LayerId": "c565a07d-6891-4589-8038-922b45676c9b"
                }
            ]
        },
        {
            "id": "bbeb5e79-40a3-44f0-84d5-fb70449373b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8778853d-d8ad-48c0-aa00-87db644d7718",
            "compositeImage": {
                "id": "5c6840d0-f23c-4553-80d3-264a96723971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbeb5e79-40a3-44f0-84d5-fb70449373b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff3ce228-973a-419b-8ab0-b39fcecbcab2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbeb5e79-40a3-44f0-84d5-fb70449373b0",
                    "LayerId": "c565a07d-6891-4589-8038-922b45676c9b"
                }
            ]
        },
        {
            "id": "84409515-0974-4fcd-b42f-ea442c325291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8778853d-d8ad-48c0-aa00-87db644d7718",
            "compositeImage": {
                "id": "3c49a1ed-5381-4fd2-9bca-d61755c66cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84409515-0974-4fcd-b42f-ea442c325291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23a86747-63d1-4620-9be0-818c8027d66c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84409515-0974-4fcd-b42f-ea442c325291",
                    "LayerId": "c565a07d-6891-4589-8038-922b45676c9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c565a07d-6891-4589-8038-922b45676c9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8778853d-d8ad-48c0-aa00-87db644d7718",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 12
}