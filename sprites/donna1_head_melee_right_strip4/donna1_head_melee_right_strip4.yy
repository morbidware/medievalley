{
    "id": "90219704-42b2-4b74-af8f-0f30f3c4d858",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f785f4ab-53d8-48a4-9c67-e073ebe003cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90219704-42b2-4b74-af8f-0f30f3c4d858",
            "compositeImage": {
                "id": "d6dafa8a-ce00-459b-9b78-48481e5c0379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f785f4ab-53d8-48a4-9c67-e073ebe003cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e189a3-2179-47d2-bdc2-11797a7e0404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f785f4ab-53d8-48a4-9c67-e073ebe003cf",
                    "LayerId": "dd79440b-1776-4809-a04d-2676bae38bcd"
                }
            ]
        },
        {
            "id": "b9027984-0bae-452d-9c3f-3b5b837971ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90219704-42b2-4b74-af8f-0f30f3c4d858",
            "compositeImage": {
                "id": "4728932f-8477-48a6-a432-b95d76d68fee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9027984-0bae-452d-9c3f-3b5b837971ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56684d6a-9b9d-46e7-a162-d0f0516ca310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9027984-0bae-452d-9c3f-3b5b837971ad",
                    "LayerId": "dd79440b-1776-4809-a04d-2676bae38bcd"
                }
            ]
        },
        {
            "id": "fc0e8ab2-a1fe-4298-91d6-a0ea0c6bfbbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90219704-42b2-4b74-af8f-0f30f3c4d858",
            "compositeImage": {
                "id": "5beb92b6-d7bf-4592-b6f7-6d2a66581454",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0e8ab2-a1fe-4298-91d6-a0ea0c6bfbbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a57846-5cc8-4950-8ab4-0c362cf36050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0e8ab2-a1fe-4298-91d6-a0ea0c6bfbbe",
                    "LayerId": "dd79440b-1776-4809-a04d-2676bae38bcd"
                }
            ]
        },
        {
            "id": "e5bf83df-671b-4950-a078-b21d80391b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90219704-42b2-4b74-af8f-0f30f3c4d858",
            "compositeImage": {
                "id": "7d7e5137-8058-46c1-8515-2891bf8bf7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5bf83df-671b-4950-a078-b21d80391b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a71b898-11b7-4630-80cf-5fb441dc895b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5bf83df-671b-4950-a078-b21d80391b46",
                    "LayerId": "dd79440b-1776-4809-a04d-2676bae38bcd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd79440b-1776-4809-a04d-2676bae38bcd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90219704-42b2-4b74-af8f-0f30f3c4d858",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}