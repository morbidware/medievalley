{
    "id": "5d3721ad-3511-4849-bdfb-c9d0200cd90c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b7bae22-8b3d-4166-90a0-1324e54914f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d3721ad-3511-4849-bdfb-c9d0200cd90c",
            "compositeImage": {
                "id": "6f495b79-ac1e-4835-87c0-cc187981d627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b7bae22-8b3d-4166-90a0-1324e54914f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24fd04a-c4ed-45e6-97db-982754fb5198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7bae22-8b3d-4166-90a0-1324e54914f6",
                    "LayerId": "f31c71d4-5295-4d10-bc33-b12fe46a5f07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f31c71d4-5295-4d10-bc33-b12fe46a5f07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d3721ad-3511-4849-bdfb-c9d0200cd90c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}