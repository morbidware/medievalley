{
    "id": "19dc3995-2e06-42f8-9936-d7ba06be6c90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1657a917-c411-44fe-913c-6b881cb43fd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19dc3995-2e06-42f8-9936-d7ba06be6c90",
            "compositeImage": {
                "id": "f71782dd-e9f2-4f1b-9b00-e23909fc80a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1657a917-c411-44fe-913c-6b881cb43fd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04848c41-02fe-4d35-8502-5dc35b6346ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1657a917-c411-44fe-913c-6b881cb43fd1",
                    "LayerId": "a3de3782-9a56-4d71-94a1-72e59ef1fc94"
                }
            ]
        },
        {
            "id": "fef4979a-c548-4f8e-95a8-c83efc0be0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19dc3995-2e06-42f8-9936-d7ba06be6c90",
            "compositeImage": {
                "id": "ca18395c-c7a9-428b-a06b-99085cf76375",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef4979a-c548-4f8e-95a8-c83efc0be0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8b0e12a-16ae-4c6e-9e2c-d5efad2f7994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef4979a-c548-4f8e-95a8-c83efc0be0b5",
                    "LayerId": "a3de3782-9a56-4d71-94a1-72e59ef1fc94"
                }
            ]
        },
        {
            "id": "431f1e97-7d47-4046-a306-ea662c48d783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19dc3995-2e06-42f8-9936-d7ba06be6c90",
            "compositeImage": {
                "id": "03db379d-ec87-4b23-945b-48306dd0f128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "431f1e97-7d47-4046-a306-ea662c48d783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dfc379f-18b7-419c-9541-ce5ffc3af3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "431f1e97-7d47-4046-a306-ea662c48d783",
                    "LayerId": "a3de3782-9a56-4d71-94a1-72e59ef1fc94"
                }
            ]
        },
        {
            "id": "6a72ff1a-443f-4b3f-99bd-0b154a44d2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19dc3995-2e06-42f8-9936-d7ba06be6c90",
            "compositeImage": {
                "id": "842af2c7-ea9f-4f0b-8ecc-757930414817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a72ff1a-443f-4b3f-99bd-0b154a44d2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b84f0310-4cc5-46de-b756-fc36880dd4e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a72ff1a-443f-4b3f-99bd-0b154a44d2e4",
                    "LayerId": "a3de3782-9a56-4d71-94a1-72e59ef1fc94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a3de3782-9a56-4d71-94a1-72e59ef1fc94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19dc3995-2e06-42f8-9936-d7ba06be6c90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}