{
    "id": "a07cf255-315f-4104-a5b8-48a9d7750391",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_wildgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ab696c3-20e4-4e58-9800-eaa9e22f1762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a07cf255-315f-4104-a5b8-48a9d7750391",
            "compositeImage": {
                "id": "699e4f8a-8d4b-414a-b97a-aa61a3b1551d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ab696c3-20e4-4e58-9800-eaa9e22f1762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c9c570-19f0-4ff5-b8a4-81654232776c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ab696c3-20e4-4e58-9800-eaa9e22f1762",
                    "LayerId": "b76c89c7-5d87-4b23-854f-9e6a0c6062d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b76c89c7-5d87-4b23-854f-9e6a0c6062d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a07cf255-315f-4104-a5b8-48a9d7750391",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}