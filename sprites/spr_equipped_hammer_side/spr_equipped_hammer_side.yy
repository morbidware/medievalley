{
    "id": "ac08855d-a49d-4292-b5d6-dbfec76c511d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_hammer_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05a15f29-8b27-4382-8729-1df797fc9fa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac08855d-a49d-4292-b5d6-dbfec76c511d",
            "compositeImage": {
                "id": "fe3dca7d-a179-4115-87f0-ec8571dcf1a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a15f29-8b27-4382-8729-1df797fc9fa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9868d135-5c17-48ed-8f9e-ffb46acb4652",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a15f29-8b27-4382-8729-1df797fc9fa7",
                    "LayerId": "adf4f7d0-d623-4aa9-aa86-a18aa528a01e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "adf4f7d0-d623-4aa9-aa86-a18aa528a01e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac08855d-a49d-4292-b5d6-dbfec76c511d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 13
}