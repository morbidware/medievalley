{
    "id": "5196f759-441f-41ff-9f5a-1a3d8786bb37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_thumbsdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 6,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dca2e44-c749-401c-962b-f2bdcded8688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5196f759-441f-41ff-9f5a-1a3d8786bb37",
            "compositeImage": {
                "id": "b4aa22ef-defa-47ab-b55e-258dbf65bafd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dca2e44-c749-401c-962b-f2bdcded8688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9fd8d30-67d8-48c9-b408-dda04d3ef748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dca2e44-c749-401c-962b-f2bdcded8688",
                    "LayerId": "00328acf-260d-4551-9a3b-73dd5731de64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "00328acf-260d-4551-9a3b-73dd5731de64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5196f759-441f-41ff-9f5a-1a3d8786bb37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}