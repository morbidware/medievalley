{
    "id": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_plowed_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c190b1ff-92fb-4a18-940c-c5997ffb3ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "ddb315df-5b8c-4cb9-a295-2d55a25193df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c190b1ff-92fb-4a18-940c-c5997ffb3ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d4ab12a-3aa7-4b3f-a78f-c28c5fa82784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c190b1ff-92fb-4a18-940c-c5997ffb3ed9",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "2106527e-624e-43e7-8b2a-d56996bc7f94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "012ae648-49f7-4850-8d45-ee5656d1b4bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2106527e-624e-43e7-8b2a-d56996bc7f94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e56fb6-b36c-4f2b-8524-86ae164f7623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2106527e-624e-43e7-8b2a-d56996bc7f94",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "867f81e4-7912-465a-b82f-ac3f1ee22d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "a10446ec-cc99-4c9e-8461-9832b5543f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "867f81e4-7912-465a-b82f-ac3f1ee22d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d7f0c34-e676-495d-a20a-aeb182726a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "867f81e4-7912-465a-b82f-ac3f1ee22d3b",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "235fa2e9-4cbd-4897-ad13-93c299a2c379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "098b1063-cf16-4bd8-a58b-78a418c48fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235fa2e9-4cbd-4897-ad13-93c299a2c379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0257deff-5375-4a92-9f61-0c48a88dfef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235fa2e9-4cbd-4897-ad13-93c299a2c379",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "50f2f5de-2eaa-40b4-8529-c948ce5c5c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "af06ca77-e507-443d-89dd-4b3f78762332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f2f5de-2eaa-40b4-8529-c948ce5c5c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08f9445-85e8-4226-a68c-0b9c7d138c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f2f5de-2eaa-40b4-8529-c948ce5c5c7f",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "f7b7a28e-5cca-4e43-8f1a-a8c054a61d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "2e1da0f8-b6f4-4810-9df6-2954e44c3163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b7a28e-5cca-4e43-8f1a-a8c054a61d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba901970-acf2-4a1e-8d5b-fc5cc34903d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b7a28e-5cca-4e43-8f1a-a8c054a61d9b",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "fb983fd8-9735-4840-b253-f580f3f07d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "2a656658-ab6c-497b-93d3-55f2f3fdcdd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb983fd8-9735-4840-b253-f580f3f07d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "816f0342-7e09-4a65-9531-b37c01f97d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb983fd8-9735-4840-b253-f580f3f07d2e",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "12e27c84-a5e1-446c-b9c7-dc531ddf3e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "dca887c8-e9b6-4fd3-ace3-6b4177339aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e27c84-a5e1-446c-b9c7-dc531ddf3e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "247a9f0a-32b7-4661-bd4f-32494ce8cf25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e27c84-a5e1-446c-b9c7-dc531ddf3e8b",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "7d1eb7b8-b148-4a4a-bdd7-a841dfa127ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "9a609598-0d0d-4ae2-8b66-4d328be34b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d1eb7b8-b148-4a4a-bdd7-a841dfa127ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac796b28-271b-428d-8941-2ab41a5d4cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d1eb7b8-b148-4a4a-bdd7-a841dfa127ad",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "f2c86409-02b3-4da0-98f7-833ac5779e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "4c6184d4-3bac-4914-a67b-ee8c4c1f8b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c86409-02b3-4da0-98f7-833ac5779e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c1915ec-9fc8-45f9-96d4-e351a009318b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c86409-02b3-4da0-98f7-833ac5779e70",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "e421fdf0-f920-491f-93e0-2efd7c9743f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "0caa1a15-84a0-4d90-b9cb-cb141a1adadc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e421fdf0-f920-491f-93e0-2efd7c9743f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996a6538-14c0-46e9-bed8-503d3491215b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e421fdf0-f920-491f-93e0-2efd7c9743f5",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "5355decc-fea3-4a5b-aeeb-6be2d75264c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "52e6c624-64b9-4dec-9de4-1cbf265aa7bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5355decc-fea3-4a5b-aeeb-6be2d75264c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2269fdb8-b26e-437b-beec-0a9611ba7f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5355decc-fea3-4a5b-aeeb-6be2d75264c7",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "c51fafa4-68ec-4cee-877b-c72907c0cd44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "18695fe1-7f4c-448c-89d2-6110862246b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51fafa4-68ec-4cee-877b-c72907c0cd44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93423689-138a-4956-96fd-6330c0ce4d55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51fafa4-68ec-4cee-877b-c72907c0cd44",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "ce24708d-27bc-4771-a342-f49edcf4085a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "8f6d65d4-dc6b-4dc5-a220-392b5a6d49d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce24708d-27bc-4771-a342-f49edcf4085a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac66246c-f457-49d4-84df-5375c5f756b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce24708d-27bc-4771-a342-f49edcf4085a",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "7291e120-d911-4cdf-b7c2-6c699fa0721d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "4b5de6bc-81ad-4ece-80f4-cb9ececbf087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7291e120-d911-4cdf-b7c2-6c699fa0721d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "490e2e96-4e32-4481-a7f6-761686da5874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7291e120-d911-4cdf-b7c2-6c699fa0721d",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "2911f472-f6b8-489b-9868-e1ec2fa51413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "007bc53e-55ec-4000-bf58-ce5a6a30aa1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2911f472-f6b8-489b-9868-e1ec2fa51413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1212a552-d41f-4d94-8238-0d6126f8a40a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2911f472-f6b8-489b-9868-e1ec2fa51413",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "94d92ae4-b01d-4b53-b88f-b8ca4b73f773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "39fba965-8036-47e4-abb8-ad7240093e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d92ae4-b01d-4b53-b88f-b8ca4b73f773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a89069-b2c0-4cf3-a6bb-0745384bac91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d92ae4-b01d-4b53-b88f-b8ca4b73f773",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "613bd8b0-5c70-4f82-9364-da3f1068ca8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "0426653d-c5a3-4944-b103-3900afdc2cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "613bd8b0-5c70-4f82-9364-da3f1068ca8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a654489-fa66-4f91-9b93-50d70c6f66fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "613bd8b0-5c70-4f82-9364-da3f1068ca8c",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "acbf7482-99f7-4998-818e-c34bf32914b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "f69e223f-d1dd-4912-b6d7-45165922518b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acbf7482-99f7-4998-818e-c34bf32914b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e02625cb-eb66-438c-913f-79710b8f4fa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acbf7482-99f7-4998-818e-c34bf32914b5",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "fa775eea-6c8c-44cd-b325-82c7de642145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "83105a91-9d07-4319-a48b-dd8ee71b4de1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa775eea-6c8c-44cd-b325-82c7de642145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5727f823-d7c7-435c-8958-68dd91f0b715",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa775eea-6c8c-44cd-b325-82c7de642145",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "434bfa40-d621-42fb-8677-baf11537014d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "799d3a6d-0605-4c09-a411-53b7f6d16d85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "434bfa40-d621-42fb-8677-baf11537014d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3642bc3a-1ba2-42dc-9de0-4b1e84ad6f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "434bfa40-d621-42fb-8677-baf11537014d",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "b7465bc6-f5b4-42e6-afee-d6f4f89fdd7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "3870d21e-4ce0-4bc7-a83a-001b2dacba5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7465bc6-f5b4-42e6-afee-d6f4f89fdd7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f6c34a2-7502-4d66-8b40-b39ac268f491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7465bc6-f5b4-42e6-afee-d6f4f89fdd7d",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "bf1a3c63-db97-4785-900d-3767d658ee62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "f3a50925-ea6f-4339-af74-5d819f9d9c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf1a3c63-db97-4785-900d-3767d658ee62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e72ab2c-edbd-4bdb-a5d4-ed254aa2dcd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf1a3c63-db97-4785-900d-3767d658ee62",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "02bcbf53-9093-4d45-99c3-7705e32fbc43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "81da2655-a65c-47d6-af92-e84355058105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02bcbf53-9093-4d45-99c3-7705e32fbc43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1410ea7-e58b-4e76-841e-13ced6068fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02bcbf53-9093-4d45-99c3-7705e32fbc43",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "1937e6d0-6217-42c2-a3ce-2018abd7ec5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "c19302fb-7f73-467e-9f28-811675ca8425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1937e6d0-6217-42c2-a3ce-2018abd7ec5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bb4d810-c2c9-4910-bbd5-d69b1a2546ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1937e6d0-6217-42c2-a3ce-2018abd7ec5a",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "0a9dfb25-3089-47c3-8028-baf2d5ffd4e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "1fce9492-8f46-422b-9deb-5124a69b4d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a9dfb25-3089-47c3-8028-baf2d5ffd4e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5190de8e-4166-4030-b742-3d313aae7082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a9dfb25-3089-47c3-8028-baf2d5ffd4e3",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "1b78f83b-9c4f-424b-84ec-45093321d525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "aa05e4be-1111-4723-b3c4-a9278e3f839d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b78f83b-9c4f-424b-84ec-45093321d525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c6504c-3281-40d4-a832-50b162846b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b78f83b-9c4f-424b-84ec-45093321d525",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "fca43a61-2c1d-48ec-b1bc-33cb1df70638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "a0cb8009-6ec0-42c8-8474-80c077a4753d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fca43a61-2c1d-48ec-b1bc-33cb1df70638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2474f7aa-e713-43fd-af5d-b508bf06df5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fca43a61-2c1d-48ec-b1bc-33cb1df70638",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "1ac8be22-dd2a-426e-9b09-00999d3686fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "62ea916d-303a-4425-a6cc-dbdc508a4a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac8be22-dd2a-426e-9b09-00999d3686fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd741d2a-6712-4176-8bfc-dded75bfbc37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac8be22-dd2a-426e-9b09-00999d3686fa",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "cf4ebf79-272c-4374-b90b-394a0ddab2cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "f519d7d5-6941-4f7a-be7a-a33c9627b0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4ebf79-272c-4374-b90b-394a0ddab2cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ee1bbc6-adda-44dd-8afe-63e2842c58b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4ebf79-272c-4374-b90b-394a0ddab2cc",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "600b4a7a-1f67-4364-a2ba-4500ab442471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "72e54329-26c6-4f85-8ae8-974d907a92e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "600b4a7a-1f67-4364-a2ba-4500ab442471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8db5f9c2-b510-40ef-a348-3a85bad5eb1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "600b4a7a-1f67-4364-a2ba-4500ab442471",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "491eac6d-ba86-4a88-b102-31d5d00502fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "c88bd49d-ff33-4fdb-828e-4c960276d1d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "491eac6d-ba86-4a88-b102-31d5d00502fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a08a5fc7-7b1c-4ba7-bef9-0a3a867286a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "491eac6d-ba86-4a88-b102-31d5d00502fa",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "c0724e75-1dae-40b3-9c59-ce43a06c5513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "809f5654-ec02-4937-baeb-b6af242fe475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0724e75-1dae-40b3-9c59-ce43a06c5513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a66f9d80-0406-4def-842b-a744823c6323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0724e75-1dae-40b3-9c59-ce43a06c5513",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "aa7f6109-639b-48aa-aa17-0ae9b6bc5cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "cfbd6c1f-d1fb-47de-b45b-cac7d352d034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7f6109-639b-48aa-aa17-0ae9b6bc5cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f199d8b-3e57-40d6-917e-7597c82173a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7f6109-639b-48aa-aa17-0ae9b6bc5cb2",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "64877588-8088-43fc-8890-1869eda04c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "e984ab61-8c5a-44e6-968f-290317cea142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64877588-8088-43fc-8890-1869eda04c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d11822c0-fd73-4d45-8273-b8008ea52783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64877588-8088-43fc-8890-1869eda04c16",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "0cd3dd59-5a76-4fe5-b552-483416eea699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "ecac75f0-4770-46ed-a736-14a162af8f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd3dd59-5a76-4fe5-b552-483416eea699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f894f53b-aa34-40c9-adc1-78b94eba0a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd3dd59-5a76-4fe5-b552-483416eea699",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "4a352018-ec71-4dc9-8b21-a08701eaf84d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "50a7968f-98f6-4d43-8176-27d3b6b927c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a352018-ec71-4dc9-8b21-a08701eaf84d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541f13a0-2630-4ec1-adbe-1368f0ee70ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a352018-ec71-4dc9-8b21-a08701eaf84d",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "ec68a6e7-779c-4028-aebf-be38f76e8eef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "e60d8ee5-75ac-42ca-bf5c-a08224c7461f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec68a6e7-779c-4028-aebf-be38f76e8eef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3074887d-f146-48c9-915b-fe681ddf97d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec68a6e7-779c-4028-aebf-be38f76e8eef",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "a335b27d-9c9a-4e6d-a2c0-1668ed59cae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "1239ae99-2317-4a9d-9536-2d57e645b16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a335b27d-9c9a-4e6d-a2c0-1668ed59cae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd547c83-ddd3-4394-b46f-f198f8d5c26d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a335b27d-9c9a-4e6d-a2c0-1668ed59cae0",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "997fb6fe-12f0-4058-95a9-70d205ef7816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "b76f2b23-6254-407d-b623-49c2c280c5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "997fb6fe-12f0-4058-95a9-70d205ef7816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057235a5-8b14-425e-83fe-1f482c18bcbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "997fb6fe-12f0-4058-95a9-70d205ef7816",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "0c84a3e0-0815-4514-8711-fef4d65702c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "60e0bdc8-6da2-4986-be1f-e1eb0ec27b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c84a3e0-0815-4514-8711-fef4d65702c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d563e53-6c92-4fa5-9e11-50180e586757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c84a3e0-0815-4514-8711-fef4d65702c6",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "8891d423-69bc-44bb-9ba9-d206aef5e3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "6841daba-587b-46a4-aa49-dfcd706dec19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8891d423-69bc-44bb-9ba9-d206aef5e3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9783175c-4fa3-4243-ae88-740d319d4cda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8891d423-69bc-44bb-9ba9-d206aef5e3d0",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "1ca1ca96-eaad-481e-ba91-e6e6d11c52e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "b20882d4-9f7e-449d-b07b-8fddebe25102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca1ca96-eaad-481e-ba91-e6e6d11c52e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feed8bc9-f5f2-485d-8aaf-1bab7b0d4642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca1ca96-eaad-481e-ba91-e6e6d11c52e8",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "6ed417b7-e8d7-45af-bdbb-33dd64567f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "10dd53c6-9125-4282-b601-37dfedb527f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed417b7-e8d7-45af-bdbb-33dd64567f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6a4b2db-d887-4a8e-a651-112a9a27d384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed417b7-e8d7-45af-bdbb-33dd64567f51",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "c12bf802-745e-4fa3-a99e-30716170d3c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "0894043d-c5ee-453d-9b25-cf9a8a6b7db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12bf802-745e-4fa3-a99e-30716170d3c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cc471b2-ee3d-4f59-846d-c5fddfc9c0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12bf802-745e-4fa3-a99e-30716170d3c0",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "fdac3b19-038c-4456-8b1b-5682386a44f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "8bffd3d1-c2a7-4b81-a050-f98e00350ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdac3b19-038c-4456-8b1b-5682386a44f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71fa95fd-43af-41da-b1d3-08d717ef979a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdac3b19-038c-4456-8b1b-5682386a44f4",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "b2b35b3f-46ef-45db-b76a-06a844445a73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "1f648e8e-82f4-459d-a582-a579c21642a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2b35b3f-46ef-45db-b76a-06a844445a73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "205e456e-150f-41b4-9bcd-74c68ceae14a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2b35b3f-46ef-45db-b76a-06a844445a73",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        },
        {
            "id": "a369e9ee-bb05-4ad4-b520-151a56b8bc41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "compositeImage": {
                "id": "a10d3411-2983-43dc-ab90-bc3090cc8dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a369e9ee-bb05-4ad4-b520-151a56b8bc41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cc5aa32-8211-404b-bf5c-b9b7b76007a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a369e9ee-bb05-4ad4-b520-151a56b8bc41",
                    "LayerId": "d599f054-00c5-4957-9388-9a81f47b48db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d599f054-00c5-4957-9388-9a81f47b48db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a613a0b-f5dd-40d3-aa81-c67cb682ca51",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}