{
    "id": "1b331d6f-be4e-4cad-8c91-a581326f2931",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_market",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cef260b-faa0-4edd-a07e-5be5f1770f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b331d6f-be4e-4cad-8c91-a581326f2931",
            "compositeImage": {
                "id": "512f5b05-c73c-4ed4-b8ba-3bdc38bd17c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cef260b-faa0-4edd-a07e-5be5f1770f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8244c14-7e97-4884-9869-e2d7bd7e2a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cef260b-faa0-4edd-a07e-5be5f1770f24",
                    "LayerId": "d4d2beae-c0eb-4e04-9b74-edebc9633d73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "d4d2beae-c0eb-4e04-9b74-edebc9633d73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b331d6f-be4e-4cad-8c91-a581326f2931",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}