{
    "id": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f7620e6-83d5-469f-9ff5-c54b31aeb185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "compositeImage": {
                "id": "317d33bc-6353-4860-97e2-dc92c6119185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7620e6-83d5-469f-9ff5-c54b31aeb185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141b6c14-c67d-4dfc-a5d1-e4b98854f325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7620e6-83d5-469f-9ff5-c54b31aeb185",
                    "LayerId": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e"
                }
            ]
        },
        {
            "id": "0388195f-f0a5-42c7-8f1f-1bd6807bd7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "compositeImage": {
                "id": "da18ec26-834f-49f9-9833-d1b0bf6f46f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0388195f-f0a5-42c7-8f1f-1bd6807bd7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c10300-4041-4a0b-9c60-becacd36826b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0388195f-f0a5-42c7-8f1f-1bd6807bd7b8",
                    "LayerId": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e"
                }
            ]
        },
        {
            "id": "ddcf454c-c790-4dcf-8ce9-7cca641b4023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "compositeImage": {
                "id": "f5d37c69-b2d1-45a4-81dc-572fcf94f1a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddcf454c-c790-4dcf-8ce9-7cca641b4023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a07f966a-6880-4ecc-9597-549b8884906f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcf454c-c790-4dcf-8ce9-7cca641b4023",
                    "LayerId": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e"
                }
            ]
        },
        {
            "id": "fb6179c6-2d5d-4aaa-b036-64fa43183c53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "compositeImage": {
                "id": "16019503-752c-4cab-ad0d-bc6980af7dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6179c6-2d5d-4aaa-b036-64fa43183c53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a61a2653-eb9d-4c23-8f70-8802054a039f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6179c6-2d5d-4aaa-b036-64fa43183c53",
                    "LayerId": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e"
                }
            ]
        },
        {
            "id": "46d1f3c3-504a-4b8f-acf9-5ea182fdab58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "compositeImage": {
                "id": "ca90de23-6d9c-4172-8823-1d9e8c77c288",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d1f3c3-504a-4b8f-acf9-5ea182fdab58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac177a8-ef73-41de-95a8-f170d5edd564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d1f3c3-504a-4b8f-acf9-5ea182fdab58",
                    "LayerId": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e"
                }
            ]
        },
        {
            "id": "ba23f521-7e6f-4ef6-9598-8f16e251f90e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "compositeImage": {
                "id": "57cd6d8a-88fc-4947-982a-5bf8202cf48c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba23f521-7e6f-4ef6-9598-8f16e251f90e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "654fb88b-f738-403f-a636-7fb6ec98a23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba23f521-7e6f-4ef6-9598-8f16e251f90e",
                    "LayerId": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "5cc5c828-78bc-49bc-b5a4-a34ea9b50a0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d0d1487-b58f-4f7f-ab6e-c3e84a2a0066",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}