{
    "id": "662815ec-cb3c-4481-bd58-7e02628e7605",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_tomatoes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ade37be-651c-469c-9094-ee2f8a215ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "662815ec-cb3c-4481-bd58-7e02628e7605",
            "compositeImage": {
                "id": "dc89c49f-54dd-45a9-97bc-c76d29213d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ade37be-651c-469c-9094-ee2f8a215ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117169f2-f924-4fa0-a72c-86d363e9b287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ade37be-651c-469c-9094-ee2f8a215ab5",
                    "LayerId": "cc90a469-0856-420c-8a53-81d913803769"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cc90a469-0856-420c-8a53-81d913803769",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "662815ec-cb3c-4481-bd58-7e02628e7605",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}