{
    "id": "1519a3f3-45ec-4605-ba16-f3a051e8de30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd83131c-e4c9-4224-8179-daf563868f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1519a3f3-45ec-4605-ba16-f3a051e8de30",
            "compositeImage": {
                "id": "7ac90d3c-d8dc-47f7-9d6d-fee0ad004f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd83131c-e4c9-4224-8179-daf563868f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e6e9e1-c52b-42e5-b20d-d42674a98fad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd83131c-e4c9-4224-8179-daf563868f28",
                    "LayerId": "91f2489b-1a09-4c53-aa1b-a4653e2289f9"
                }
            ]
        },
        {
            "id": "7896c842-97b5-4343-ad17-457424e7b6bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1519a3f3-45ec-4605-ba16-f3a051e8de30",
            "compositeImage": {
                "id": "2201d9ff-1bdd-45e6-8945-2b5d8c9619a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7896c842-97b5-4343-ad17-457424e7b6bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6918ef71-7501-4f24-9ca0-d86c5536a0ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7896c842-97b5-4343-ad17-457424e7b6bb",
                    "LayerId": "91f2489b-1a09-4c53-aa1b-a4653e2289f9"
                }
            ]
        },
        {
            "id": "a9b1809c-3779-4bec-bfd8-9aae839629b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1519a3f3-45ec-4605-ba16-f3a051e8de30",
            "compositeImage": {
                "id": "e43c89e9-759b-4534-b6e1-2c8fc1dc9a8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9b1809c-3779-4bec-bfd8-9aae839629b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "143187a9-6e80-4d64-bceb-1aee7e87b6f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9b1809c-3779-4bec-bfd8-9aae839629b9",
                    "LayerId": "91f2489b-1a09-4c53-aa1b-a4653e2289f9"
                }
            ]
        },
        {
            "id": "152f054f-0228-417d-ab1a-0eafdb107e3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1519a3f3-45ec-4605-ba16-f3a051e8de30",
            "compositeImage": {
                "id": "7e3c1f12-6650-4559-97cb-5c3f891c2c95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "152f054f-0228-417d-ab1a-0eafdb107e3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f229c8e-5010-4941-969e-6adb731cb027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "152f054f-0228-417d-ab1a-0eafdb107e3a",
                    "LayerId": "91f2489b-1a09-4c53-aa1b-a4653e2289f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "91f2489b-1a09-4c53-aa1b-a4653e2289f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1519a3f3-45ec-4605-ba16-f3a051e8de30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}