{
    "id": "a7200c15-8327-43af-a033-1e89c9f39857",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 7,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b5c6575-14f4-4439-b4f2-11a75155e929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7200c15-8327-43af-a033-1e89c9f39857",
            "compositeImage": {
                "id": "e4e61c93-b3c8-42ee-968e-6e6022462e42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b5c6575-14f4-4439-b4f2-11a75155e929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5c976c4-5f7e-41c1-8239-353a83d97a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b5c6575-14f4-4439-b4f2-11a75155e929",
                    "LayerId": "fa714add-8e3e-4957-8e71-98c46e5313da"
                }
            ]
        },
        {
            "id": "e21a00c3-a36f-4bc7-8715-322379a58a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7200c15-8327-43af-a033-1e89c9f39857",
            "compositeImage": {
                "id": "50972198-34fc-4205-9e09-931d1b4327d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21a00c3-a36f-4bc7-8715-322379a58a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48afe26c-2905-4a42-b592-2ab07f37a0b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21a00c3-a36f-4bc7-8715-322379a58a2e",
                    "LayerId": "fa714add-8e3e-4957-8e71-98c46e5313da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "fa714add-8e3e-4957-8e71-98c46e5313da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7200c15-8327-43af-a033-1e89c9f39857",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}