{
    "id": "4c9f42ae-fa9a-4ac9-9ee7-6e9f847a3b6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_mailbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4475abfb-0c2b-4417-9658-c969bcee0d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c9f42ae-fa9a-4ac9-9ee7-6e9f847a3b6a",
            "compositeImage": {
                "id": "15e175c2-797a-4640-a0f3-f9053971b269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4475abfb-0c2b-4417-9658-c969bcee0d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164cc3e6-efc5-457b-b70d-f7723671e3f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4475abfb-0c2b-4417-9658-c969bcee0d1f",
                    "LayerId": "a3a62796-404f-4ee0-8689-c04ab926c134"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a3a62796-404f-4ee0-8689-c04ab926c134",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c9f42ae-fa9a-4ac9-9ee7-6e9f847a3b6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 28
}