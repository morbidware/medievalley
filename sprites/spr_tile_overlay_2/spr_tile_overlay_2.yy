{
    "id": "9fbe17c5-689f-4738-8424-7179525b1d66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_overlay_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64e9a0d7-2a08-4422-a4a4-f9a506d826e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fbe17c5-689f-4738-8424-7179525b1d66",
            "compositeImage": {
                "id": "1433ed2c-c567-4fd7-a306-99491e36a180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64e9a0d7-2a08-4422-a4a4-f9a506d826e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836f865b-75d5-4744-99a5-c7ef955829da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64e9a0d7-2a08-4422-a4a4-f9a506d826e6",
                    "LayerId": "e35bdca1-0a27-4530-ab1d-60723ab17131"
                }
            ]
        },
        {
            "id": "ef515d9c-3f38-4daf-933a-7435b811fa70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fbe17c5-689f-4738-8424-7179525b1d66",
            "compositeImage": {
                "id": "356594c1-3f4f-4940-b548-03dc03a1bf1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef515d9c-3f38-4daf-933a-7435b811fa70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa37d08a-c809-4359-bc42-b39b55e468cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef515d9c-3f38-4daf-933a-7435b811fa70",
                    "LayerId": "e35bdca1-0a27-4530-ab1d-60723ab17131"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e35bdca1-0a27-4530-ab1d-60723ab17131",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fbe17c5-689f-4738-8424-7179525b1d66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}