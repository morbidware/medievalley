{
    "id": "b1962672-5726-4662-b471-1fbda280a579",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_money_label_big",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56b97514-2614-4f47-8cfd-770e1376e6dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1962672-5726-4662-b471-1fbda280a579",
            "compositeImage": {
                "id": "ddf56442-0304-49db-b3eb-8d85e7b19229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56b97514-2614-4f47-8cfd-770e1376e6dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b83d3ea8-7ac8-4608-9220-52048bdef8d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56b97514-2614-4f47-8cfd-770e1376e6dd",
                    "LayerId": "ea20fb86-0427-49e8-89fc-7da3d5877d65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ea20fb86-0427-49e8-89fc-7da3d5877d65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1962672-5726-4662-b471-1fbda280a579",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 40,
    "yorig": 0
}