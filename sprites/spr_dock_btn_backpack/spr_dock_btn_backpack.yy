{
    "id": "90c4668f-e809-42db-a958-f34f67f90674",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_backpack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b0eceea-25d2-41ff-8150-7093aae79842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90c4668f-e809-42db-a958-f34f67f90674",
            "compositeImage": {
                "id": "b53e3bf1-6d16-4d0e-a3f7-41eae947d89f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b0eceea-25d2-41ff-8150-7093aae79842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63c8f29-50b7-4dd6-abce-1f56845b2e61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b0eceea-25d2-41ff-8150-7093aae79842",
                    "LayerId": "dc26b2c7-9d65-4517-8ca4-eb5d8f6643ab"
                }
            ]
        },
        {
            "id": "572e2fa1-677a-4bc1-8638-41659f35b965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90c4668f-e809-42db-a958-f34f67f90674",
            "compositeImage": {
                "id": "058e5a36-a165-4605-8cc5-6442edb30ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "572e2fa1-677a-4bc1-8638-41659f35b965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4813cfe7-3beb-4c3b-b165-006344e605bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "572e2fa1-677a-4bc1-8638-41659f35b965",
                    "LayerId": "dc26b2c7-9d65-4517-8ca4-eb5d8f6643ab"
                }
            ]
        },
        {
            "id": "d217d63d-cba6-465e-88c2-85654d33a894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90c4668f-e809-42db-a958-f34f67f90674",
            "compositeImage": {
                "id": "05d5fc81-c249-4df8-813c-14942e48c2af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d217d63d-cba6-465e-88c2-85654d33a894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29dd11bd-e777-4099-86a2-279380d5a446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d217d63d-cba6-465e-88c2-85654d33a894",
                    "LayerId": "dc26b2c7-9d65-4517-8ca4-eb5d8f6643ab"
                }
            ]
        },
        {
            "id": "a0006e1d-1ef4-4d8a-b0c7-76b2391750df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90c4668f-e809-42db-a958-f34f67f90674",
            "compositeImage": {
                "id": "2edd6b87-4dbf-45a2-baa2-ace2e5357096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0006e1d-1ef4-4d8a-b0c7-76b2391750df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610bcd42-caee-4091-b4d2-2162254c79ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0006e1d-1ef4-4d8a-b0c7-76b2391750df",
                    "LayerId": "dc26b2c7-9d65-4517-8ca4-eb5d8f6643ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "dc26b2c7-9d65-4517-8ca4-eb5d8f6643ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90c4668f-e809-42db-a958-f34f67f90674",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}