{
    "id": "1a7bd9b2-3d35-4799-937e-377435d1f869",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_corner_tr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72a009c5-3314-4a55-869a-acbafe4c146f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a7bd9b2-3d35-4799-937e-377435d1f869",
            "compositeImage": {
                "id": "17669676-a4db-4137-83f5-2065ee5068bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a009c5-3314-4a55-869a-acbafe4c146f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a609524-d7eb-438d-997a-50275489b71e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a009c5-3314-4a55-869a-acbafe4c146f",
                    "LayerId": "07640800-df22-4238-8adc-208fb327f2ca"
                }
            ]
        },
        {
            "id": "029a81d0-7419-4705-892e-777efa07c0d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a7bd9b2-3d35-4799-937e-377435d1f869",
            "compositeImage": {
                "id": "a1d9f1c9-07b9-4270-91b7-182e03cc6ba8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029a81d0-7419-4705-892e-777efa07c0d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdd90800-05a6-4495-a7a4-ebc00a15c570",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029a81d0-7419-4705-892e-777efa07c0d9",
                    "LayerId": "07640800-df22-4238-8adc-208fb327f2ca"
                }
            ]
        },
        {
            "id": "0873a388-483c-47b4-bf7c-f346d983eb4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a7bd9b2-3d35-4799-937e-377435d1f869",
            "compositeImage": {
                "id": "7250c1ba-abeb-4810-8743-8ced6f2f38b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0873a388-483c-47b4-bf7c-f346d983eb4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecbc16f2-aa75-4c43-95c5-f50a6babb770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0873a388-483c-47b4-bf7c-f346d983eb4c",
                    "LayerId": "07640800-df22-4238-8adc-208fb327f2ca"
                }
            ]
        },
        {
            "id": "fc05d2c7-433c-4037-80e3-9e2b200f6225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a7bd9b2-3d35-4799-937e-377435d1f869",
            "compositeImage": {
                "id": "43522f21-2be1-4cac-b214-bdbcc923f6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc05d2c7-433c-4037-80e3-9e2b200f6225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2443e4-7ac3-47fc-acd8-5f95b61e3ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc05d2c7-433c-4037-80e3-9e2b200f6225",
                    "LayerId": "07640800-df22-4238-8adc-208fb327f2ca"
                }
            ]
        },
        {
            "id": "7cce5f1b-19fb-46da-b605-24ed6a3b09eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a7bd9b2-3d35-4799-937e-377435d1f869",
            "compositeImage": {
                "id": "4638b7c1-dca4-49ef-85d4-15ed6cffe4e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cce5f1b-19fb-46da-b605-24ed6a3b09eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3e298d-557e-47e8-aefd-dfe745c0658b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cce5f1b-19fb-46da-b605-24ed6a3b09eb",
                    "LayerId": "07640800-df22-4238-8adc-208fb327f2ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "07640800-df22-4238-8adc-208fb327f2ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a7bd9b2-3d35-4799-937e-377435d1f869",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}