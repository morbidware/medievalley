{
    "id": "4d9d729c-a8a3-4b2e-8b43-2b6e3df68d68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9ecba61-d422-4db9-b131-61761712bb4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9d729c-a8a3-4b2e-8b43-2b6e3df68d68",
            "compositeImage": {
                "id": "b361d801-1c54-453d-84b9-79b930075bef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ecba61-d422-4db9-b131-61761712bb4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0a96de-8372-4b85-a278-f43a87981cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ecba61-d422-4db9-b131-61761712bb4e",
                    "LayerId": "32995649-86b5-4fb0-b99e-ca854dce7565"
                }
            ]
        },
        {
            "id": "41cb3e4d-3a76-4926-92b4-1ce74496d895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9d729c-a8a3-4b2e-8b43-2b6e3df68d68",
            "compositeImage": {
                "id": "b67d5e59-ec73-4f00-a0f5-18e15ab3defb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41cb3e4d-3a76-4926-92b4-1ce74496d895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2c4d89-bcc6-4898-b5e8-74a86281db9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41cb3e4d-3a76-4926-92b4-1ce74496d895",
                    "LayerId": "32995649-86b5-4fb0-b99e-ca854dce7565"
                }
            ]
        },
        {
            "id": "88214dca-30a0-4756-a912-588843828c17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9d729c-a8a3-4b2e-8b43-2b6e3df68d68",
            "compositeImage": {
                "id": "e0c37a47-4d58-45a4-a6dd-b01788f68dc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88214dca-30a0-4756-a912-588843828c17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "441b26e1-c2d0-4a6d-b374-c2cfc8556a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88214dca-30a0-4756-a912-588843828c17",
                    "LayerId": "32995649-86b5-4fb0-b99e-ca854dce7565"
                }
            ]
        },
        {
            "id": "84b1f3f7-171a-4868-9729-944ea7ff73b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d9d729c-a8a3-4b2e-8b43-2b6e3df68d68",
            "compositeImage": {
                "id": "fc5effca-7df3-4335-a408-d9c9681711ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84b1f3f7-171a-4868-9729-944ea7ff73b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a83390-05c4-46aa-a17d-ffe68cccb4d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84b1f3f7-171a-4868-9729-944ea7ff73b8",
                    "LayerId": "32995649-86b5-4fb0-b99e-ca854dce7565"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "32995649-86b5-4fb0-b99e-ca854dce7565",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d9d729c-a8a3-4b2e-8b43-2b6e3df68d68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}