{
    "id": "693847aa-d01a-40a0-82a5-652dc37454f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_sword_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95dd01e9-e7e4-4ff8-acbe-ca61d2230032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "693847aa-d01a-40a0-82a5-652dc37454f5",
            "compositeImage": {
                "id": "3d0f7ac4-34d3-4ccf-8ed6-40944406948a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95dd01e9-e7e4-4ff8-acbe-ca61d2230032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b82ffde-b97c-4070-95db-b88720416767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95dd01e9-e7e4-4ff8-acbe-ca61d2230032",
                    "LayerId": "c5f13adb-8f50-4f1b-a80f-062715ce3640"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c5f13adb-8f50-4f1b-a80f-062715ce3640",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "693847aa-d01a-40a0-82a5-652dc37454f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 1,
    "yorig": 14
}