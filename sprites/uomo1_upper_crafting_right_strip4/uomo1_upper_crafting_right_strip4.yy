{
    "id": "88a72d53-ab61-424b-a594-ff76f7fad5fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afeeed9b-c5bf-43e7-a5af-1f24c72dfac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88a72d53-ab61-424b-a594-ff76f7fad5fd",
            "compositeImage": {
                "id": "fd70176d-354b-43dd-9a62-7eb52d1bab0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afeeed9b-c5bf-43e7-a5af-1f24c72dfac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7de2b643-1b44-4695-91f2-65b484882ae5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afeeed9b-c5bf-43e7-a5af-1f24c72dfac6",
                    "LayerId": "86acca53-b6f7-43c5-84db-017e2698e830"
                }
            ]
        },
        {
            "id": "9cb394a9-8f6f-4e20-9564-5f02a1f9a16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88a72d53-ab61-424b-a594-ff76f7fad5fd",
            "compositeImage": {
                "id": "1f52c184-5cf4-4a44-ba8a-9e2e8d6623ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cb394a9-8f6f-4e20-9564-5f02a1f9a16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddec9b3-c9ee-4ca9-ada7-e33be97f1cd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cb394a9-8f6f-4e20-9564-5f02a1f9a16a",
                    "LayerId": "86acca53-b6f7-43c5-84db-017e2698e830"
                }
            ]
        },
        {
            "id": "b71e3487-4e8c-4278-a4e3-a4aed2efe520",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88a72d53-ab61-424b-a594-ff76f7fad5fd",
            "compositeImage": {
                "id": "be20a21c-34ef-41a2-b3f1-4037db944787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71e3487-4e8c-4278-a4e3-a4aed2efe520",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "694ae902-240d-4f5f-8403-3c40e51ca5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71e3487-4e8c-4278-a4e3-a4aed2efe520",
                    "LayerId": "86acca53-b6f7-43c5-84db-017e2698e830"
                }
            ]
        },
        {
            "id": "3c23a4bc-bbe9-4a4b-8634-c7dd5045c06d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88a72d53-ab61-424b-a594-ff76f7fad5fd",
            "compositeImage": {
                "id": "4dfb1ed0-0da3-44b7-8a06-5186392864f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c23a4bc-bbe9-4a4b-8634-c7dd5045c06d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d27066-57d4-454b-be28-6edf0d8b67f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c23a4bc-bbe9-4a4b-8634-c7dd5045c06d",
                    "LayerId": "86acca53-b6f7-43c5-84db-017e2698e830"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "86acca53-b6f7-43c5-84db-017e2698e830",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88a72d53-ab61-424b-a594-ff76f7fad5fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}