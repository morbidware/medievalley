{
    "id": "726bf2ab-3d50-4ed2-8e48-7214b2a7cef1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challenge_letter_cup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "068c3488-53d6-4c21-9c3f-d8df273a6c9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "726bf2ab-3d50-4ed2-8e48-7214b2a7cef1",
            "compositeImage": {
                "id": "c4e67751-fb65-44dc-9cb9-b49ac0d45787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "068c3488-53d6-4c21-9c3f-d8df273a6c9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa6bb62-e52b-4f81-851a-a788c67fd012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "068c3488-53d6-4c21-9c3f-d8df273a6c9d",
                    "LayerId": "e28ffc4b-1a87-4b56-8416-1c02e47590c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "e28ffc4b-1a87-4b56-8416-1c02e47590c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "726bf2ab-3d50-4ed2-8e48-7214b2a7cef1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 28,
    "yorig": 36
}