{
    "id": "b1573338-a64a-4b87-b1b9-71d1249ae898",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_walk_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfaab6b8-f578-4ec2-acfa-fb0e16f2d5be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "compositeImage": {
                "id": "25bfb73e-e55f-4acf-8711-a50e010ee49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfaab6b8-f578-4ec2-acfa-fb0e16f2d5be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2673e14b-db08-4c5b-84b3-48f5cd8faf88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfaab6b8-f578-4ec2-acfa-fb0e16f2d5be",
                    "LayerId": "4eb27601-dab6-4be5-ab9f-1313d389196a"
                }
            ]
        },
        {
            "id": "e39deb15-970c-40bf-a487-799c5c2e26eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "compositeImage": {
                "id": "7364b710-0006-4acb-b460-c7ebb5ea2ac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39deb15-970c-40bf-a487-799c5c2e26eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c914c16e-bd64-4175-83cf-be8f22947796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39deb15-970c-40bf-a487-799c5c2e26eb",
                    "LayerId": "4eb27601-dab6-4be5-ab9f-1313d389196a"
                }
            ]
        },
        {
            "id": "67cbfac3-be55-48da-b414-f024bb580fbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "compositeImage": {
                "id": "ce7421b1-eb46-4390-a437-8a11329b430a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67cbfac3-be55-48da-b414-f024bb580fbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a68169-ac73-4e21-b1e7-66d10d4f5612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67cbfac3-be55-48da-b414-f024bb580fbe",
                    "LayerId": "4eb27601-dab6-4be5-ab9f-1313d389196a"
                }
            ]
        },
        {
            "id": "bd5a52ee-e392-4f3e-a776-f83afd603603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "compositeImage": {
                "id": "c862c06d-0070-4b7a-8800-f7d8fdbc7d0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd5a52ee-e392-4f3e-a776-f83afd603603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9560bd2-6c8b-4cd4-bfd1-e0e2f83b361f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd5a52ee-e392-4f3e-a776-f83afd603603",
                    "LayerId": "4eb27601-dab6-4be5-ab9f-1313d389196a"
                }
            ]
        },
        {
            "id": "8332926d-50a0-4371-8dd3-bbf2b9b63f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "compositeImage": {
                "id": "6270a06e-cad8-418e-9624-fd176959aebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8332926d-50a0-4371-8dd3-bbf2b9b63f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c88f4376-1a19-45da-a825-60ae5b03e299",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8332926d-50a0-4371-8dd3-bbf2b9b63f48",
                    "LayerId": "4eb27601-dab6-4be5-ab9f-1313d389196a"
                }
            ]
        },
        {
            "id": "ac4179dc-0385-4d09-9030-3b0b99bb0f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "compositeImage": {
                "id": "95606439-9e75-4338-8199-4602ec7fe0a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4179dc-0385-4d09-9030-3b0b99bb0f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b9ea39b-bf0e-4354-ad98-8c1677cfb229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4179dc-0385-4d09-9030-3b0b99bb0f99",
                    "LayerId": "4eb27601-dab6-4be5-ab9f-1313d389196a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4eb27601-dab6-4be5-ab9f-1313d389196a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1573338-a64a-4b87-b1b9-71d1249ae898",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}