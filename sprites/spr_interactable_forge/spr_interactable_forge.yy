{
    "id": "e1e52d73-d000-46a7-874c-305a38e1ee7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_forge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a84da272-3711-4a6f-9c6f-0112a3a827e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1e52d73-d000-46a7-874c-305a38e1ee7b",
            "compositeImage": {
                "id": "67a90108-6cd5-464c-9cb7-f139da2bc069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84da272-3711-4a6f-9c6f-0112a3a827e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc0a902-52ab-4225-8fe7-6ab987efecc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84da272-3711-4a6f-9c6f-0112a3a827e5",
                    "LayerId": "cedc3afc-7902-4838-857d-45f25a2885bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "cedc3afc-7902-4838-857d-45f25a2885bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1e52d73-d000-46a7-874c-305a38e1ee7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 72
}