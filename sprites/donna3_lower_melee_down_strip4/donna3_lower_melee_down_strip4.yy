{
    "id": "4ef6d301-48b4-4164-8ee2-38b9944f5f7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "210205c9-7c33-42e9-904a-1b4aca85162d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef6d301-48b4-4164-8ee2-38b9944f5f7e",
            "compositeImage": {
                "id": "c402f88e-f334-4d29-9291-70497b6ffbc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "210205c9-7c33-42e9-904a-1b4aca85162d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "219e972d-9181-44bf-9da5-ac524ee64413",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "210205c9-7c33-42e9-904a-1b4aca85162d",
                    "LayerId": "02fa6d74-adad-4944-b5dd-3dcdfc0cf072"
                }
            ]
        },
        {
            "id": "dee05b46-2d26-4ba1-9ad8-8e840a6c803f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef6d301-48b4-4164-8ee2-38b9944f5f7e",
            "compositeImage": {
                "id": "295f5a6f-beab-4e21-9e91-8041e8542c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee05b46-2d26-4ba1-9ad8-8e840a6c803f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75dd5462-9f3c-4d58-a25a-034fcfae7e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee05b46-2d26-4ba1-9ad8-8e840a6c803f",
                    "LayerId": "02fa6d74-adad-4944-b5dd-3dcdfc0cf072"
                }
            ]
        },
        {
            "id": "45722c21-8fe1-4c6e-9f9e-18a9fe4a4284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef6d301-48b4-4164-8ee2-38b9944f5f7e",
            "compositeImage": {
                "id": "e3add912-a221-485f-aad4-55a1d9849301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45722c21-8fe1-4c6e-9f9e-18a9fe4a4284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0dc1ecd-735a-4f1d-ab1c-6c21b62cd0c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45722c21-8fe1-4c6e-9f9e-18a9fe4a4284",
                    "LayerId": "02fa6d74-adad-4944-b5dd-3dcdfc0cf072"
                }
            ]
        },
        {
            "id": "7f4e515b-e678-40e3-b3be-43ffeb51ede4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef6d301-48b4-4164-8ee2-38b9944f5f7e",
            "compositeImage": {
                "id": "8fd2e609-9939-489e-8d7e-c9c3fbeba164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4e515b-e678-40e3-b3be-43ffeb51ede4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f93a0a-004e-4857-8789-f1076b5e17cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4e515b-e678-40e3-b3be-43ffeb51ede4",
                    "LayerId": "02fa6d74-adad-4944-b5dd-3dcdfc0cf072"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "02fa6d74-adad-4944-b5dd-3dcdfc0cf072",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ef6d301-48b4-4164-8ee2-38b9944f5f7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}