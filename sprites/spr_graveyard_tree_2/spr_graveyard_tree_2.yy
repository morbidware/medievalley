{
    "id": "a5f535c8-f9e1-4b21-8409-d09cc186884d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tree_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 6,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e950a3bf-0e01-4ff8-bd8e-60e5e6ffcdc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5f535c8-f9e1-4b21-8409-d09cc186884d",
            "compositeImage": {
                "id": "e4b1990e-ffb0-46d0-927d-fbcd6b9f2d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e950a3bf-0e01-4ff8-bd8e-60e5e6ffcdc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a503ec9d-07ff-45c3-bce0-90761ed881fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e950a3bf-0e01-4ff8-bd8e-60e5e6ffcdc9",
                    "LayerId": "737306cb-59cf-4517-bd99-fa2554ca9e7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "737306cb-59cf-4517-bd99-fa2554ca9e7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5f535c8-f9e1-4b21-8409-d09cc186884d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 14,
    "yorig": 45
}