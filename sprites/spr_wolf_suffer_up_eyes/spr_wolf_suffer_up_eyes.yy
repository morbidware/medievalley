{
    "id": "1be5c8c5-6f3b-449f-b824-89f68611f3c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_suffer_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af0aa251-fa7f-4c01-afbd-ccdc9f3cd335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1be5c8c5-6f3b-449f-b824-89f68611f3c9",
            "compositeImage": {
                "id": "c841d727-6277-4632-83f8-1e1115fe9697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af0aa251-fa7f-4c01-afbd-ccdc9f3cd335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17541a56-7df7-4809-8238-d75e7c26c4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af0aa251-fa7f-4c01-afbd-ccdc9f3cd335",
                    "LayerId": "5cb451ff-d9b4-4bf4-b0fa-fcdd8d30df9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "5cb451ff-d9b4-4bf4-b0fa-fcdd8d30df9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1be5c8c5-6f3b-449f-b824-89f68611f3c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}