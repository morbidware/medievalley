{
    "id": "cc418de0-9e32-43fb-bcf9-e74e79d1b986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "695e291b-093d-435e-9f76-2471c6f27db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc418de0-9e32-43fb-bcf9-e74e79d1b986",
            "compositeImage": {
                "id": "f72626af-3187-45c0-8ad8-d3814acd080c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "695e291b-093d-435e-9f76-2471c6f27db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0136c10c-e396-46a0-a73f-1a4dc2077b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "695e291b-093d-435e-9f76-2471c6f27db2",
                    "LayerId": "d296db02-c1b3-401d-9739-ccc46c61070a"
                }
            ]
        },
        {
            "id": "886e27f7-e8a2-47eb-b3cc-8e3142804423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc418de0-9e32-43fb-bcf9-e74e79d1b986",
            "compositeImage": {
                "id": "c49e75d2-ae31-41c8-b480-a75a5a45435e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886e27f7-e8a2-47eb-b3cc-8e3142804423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3909030d-5782-4125-bd57-70ec7416956a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886e27f7-e8a2-47eb-b3cc-8e3142804423",
                    "LayerId": "d296db02-c1b3-401d-9739-ccc46c61070a"
                }
            ]
        },
        {
            "id": "71e16725-8b4f-4749-90aa-d6cd3249462a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc418de0-9e32-43fb-bcf9-e74e79d1b986",
            "compositeImage": {
                "id": "d399c6f1-2914-40b0-b7c9-ad16fd6e59de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71e16725-8b4f-4749-90aa-d6cd3249462a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec823526-ebfd-412d-9610-a8708b1af00f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71e16725-8b4f-4749-90aa-d6cd3249462a",
                    "LayerId": "d296db02-c1b3-401d-9739-ccc46c61070a"
                }
            ]
        },
        {
            "id": "d22a9d63-2c1a-42a3-934c-082459933573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc418de0-9e32-43fb-bcf9-e74e79d1b986",
            "compositeImage": {
                "id": "9ec701a1-5233-4e49-ae0a-f9dc2aab55f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d22a9d63-2c1a-42a3-934c-082459933573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cdff4df-9ddc-42dd-acbb-0e30e8a7b7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d22a9d63-2c1a-42a3-934c-082459933573",
                    "LayerId": "d296db02-c1b3-401d-9739-ccc46c61070a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d296db02-c1b3-401d-9739-ccc46c61070a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc418de0-9e32-43fb-bcf9-e74e79d1b986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}