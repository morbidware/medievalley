{
    "id": "1a20eb56-3678-4290-aef6-214a5712bfea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af6a292a-33dc-4c22-87de-8731676f6618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a20eb56-3678-4290-aef6-214a5712bfea",
            "compositeImage": {
                "id": "98984b69-e784-4162-9737-27c25c9d5f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6a292a-33dc-4c22-87de-8731676f6618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b617f81e-11fd-44bb-8a57-268f98932e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6a292a-33dc-4c22-87de-8731676f6618",
                    "LayerId": "5d0f5922-5425-4f1a-aa15-2e172be90c37"
                }
            ]
        },
        {
            "id": "5d4039f3-be23-48da-b646-d40a04e2a586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a20eb56-3678-4290-aef6-214a5712bfea",
            "compositeImage": {
                "id": "260e2c96-d0b4-4796-961c-f335c62ca52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4039f3-be23-48da-b646-d40a04e2a586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6fb0d75-7009-4472-bd62-a01c5e3703bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4039f3-be23-48da-b646-d40a04e2a586",
                    "LayerId": "5d0f5922-5425-4f1a-aa15-2e172be90c37"
                }
            ]
        },
        {
            "id": "cbc870e0-e47e-480a-9cf5-487c80e4edea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a20eb56-3678-4290-aef6-214a5712bfea",
            "compositeImage": {
                "id": "68d0532b-df5d-4ea8-a6c3-c9c94628c840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbc870e0-e47e-480a-9cf5-487c80e4edea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d2eeda9-3811-4d25-96c4-62b1e78260de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbc870e0-e47e-480a-9cf5-487c80e4edea",
                    "LayerId": "5d0f5922-5425-4f1a-aa15-2e172be90c37"
                }
            ]
        },
        {
            "id": "1b1de855-0629-4187-9711-46c8c81f9525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a20eb56-3678-4290-aef6-214a5712bfea",
            "compositeImage": {
                "id": "e81f07c0-fd89-45d3-8855-ff5933f99c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1de855-0629-4187-9711-46c8c81f9525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5812f23a-8a16-4a9f-add6-900cdd7fe9ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1de855-0629-4187-9711-46c8c81f9525",
                    "LayerId": "5d0f5922-5425-4f1a-aa15-2e172be90c37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5d0f5922-5425-4f1a-aa15-2e172be90c37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a20eb56-3678-4290-aef6-214a5712bfea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}