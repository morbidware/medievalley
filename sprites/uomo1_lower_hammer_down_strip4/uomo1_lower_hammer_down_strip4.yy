{
    "id": "2d16c4d4-0dcf-4a2e-89fa-101ea8136931",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4186ac3-97fd-427f-8aaf-05e281ef113a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d16c4d4-0dcf-4a2e-89fa-101ea8136931",
            "compositeImage": {
                "id": "8f461f9a-0d52-40a2-aa23-9df5df214ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4186ac3-97fd-427f-8aaf-05e281ef113a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88cb445b-2b49-4832-99f5-d6dfe1588cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4186ac3-97fd-427f-8aaf-05e281ef113a",
                    "LayerId": "0eb6df70-f4bd-4216-88e1-077e417bf613"
                }
            ]
        },
        {
            "id": "319899ee-fbe1-48c2-9d7b-7cf68d377589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d16c4d4-0dcf-4a2e-89fa-101ea8136931",
            "compositeImage": {
                "id": "e4c4f789-a045-4c60-9b81-a9bb22f11205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "319899ee-fbe1-48c2-9d7b-7cf68d377589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0d8d28b-f22f-4333-bedc-7f2286e8bc13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "319899ee-fbe1-48c2-9d7b-7cf68d377589",
                    "LayerId": "0eb6df70-f4bd-4216-88e1-077e417bf613"
                }
            ]
        },
        {
            "id": "228596ac-9959-4dbb-91fa-160bb9f5f4c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d16c4d4-0dcf-4a2e-89fa-101ea8136931",
            "compositeImage": {
                "id": "446ad088-e719-4bc8-9f84-06a80208f86f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228596ac-9959-4dbb-91fa-160bb9f5f4c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a323444-57d5-4dc3-a214-daa6bac0a84e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228596ac-9959-4dbb-91fa-160bb9f5f4c8",
                    "LayerId": "0eb6df70-f4bd-4216-88e1-077e417bf613"
                }
            ]
        },
        {
            "id": "a37bb6af-e30a-4cae-9987-3c8dd63af2c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d16c4d4-0dcf-4a2e-89fa-101ea8136931",
            "compositeImage": {
                "id": "22b9d0de-0b5d-4152-b6d0-77fadfad796d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a37bb6af-e30a-4cae-9987-3c8dd63af2c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d0ecba-ea27-42d2-b52c-56aca957ed10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a37bb6af-e30a-4cae-9987-3c8dd63af2c6",
                    "LayerId": "0eb6df70-f4bd-4216-88e1-077e417bf613"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0eb6df70-f4bd-4216-88e1-077e417bf613",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d16c4d4-0dcf-4a2e-89fa-101ea8136931",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}