{
    "id": "e11cb1e6-5966-4f6e-b72a-d7ebed97730e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_suffer_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2b4a933-ce94-4177-adcb-b7e9ef5c4d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e11cb1e6-5966-4f6e-b72a-d7ebed97730e",
            "compositeImage": {
                "id": "2dc4c7fe-7ee7-454f-b245-c65cdfae9a38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2b4a933-ce94-4177-adcb-b7e9ef5c4d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8204e79e-74f4-4fb0-9148-61315204f343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2b4a933-ce94-4177-adcb-b7e9ef5c4d32",
                    "LayerId": "84e7a529-7d8d-4f91-9a58-9f4abddc5025"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "84e7a529-7d8d-4f91-9a58-9f4abddc5025",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e11cb1e6-5966-4f6e-b72a-d7ebed97730e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}