{
    "id": "ab8c4da3-b99c-4edf-b98a-8fbc7f49eb24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_square_top",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85b24053-23d9-4f03-a5c6-1a74e79b141f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ab8c4da3-b99c-4edf-b98a-8fbc7f49eb24",
            "compositeImage": {
                "id": "8af58674-51b0-46a9-9b7c-d7f3d1fcf188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b24053-23d9-4f03-a5c6-1a74e79b141f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "559ee1d7-6d18-49bf-aca5-9d3628165f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b24053-23d9-4f03-a5c6-1a74e79b141f",
                    "LayerId": "5b6eb2f2-59bb-44f6-bcbc-424d82f57c30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5b6eb2f2-59bb-44f6-bcbc-424d82f57c30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ab8c4da3-b99c-4edf-b98a-8fbc7f49eb24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}