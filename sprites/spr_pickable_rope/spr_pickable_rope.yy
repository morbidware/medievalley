{
    "id": "68fa76ec-9b9a-4998-ba90-f3601152a9e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_rope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68be22f8-4512-45dc-8138-2eac89339e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68fa76ec-9b9a-4998-ba90-f3601152a9e1",
            "compositeImage": {
                "id": "41ce11e3-797a-4293-97f7-e4389df2c5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68be22f8-4512-45dc-8138-2eac89339e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b03b55-474a-4853-be78-95d7b58de900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68be22f8-4512-45dc-8138-2eac89339e1f",
                    "LayerId": "9a5ed200-3472-4490-8597-fe51ed238c85"
                }
            ]
        },
        {
            "id": "fb809d20-568a-4c4e-8aeb-053bc29721bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68fa76ec-9b9a-4998-ba90-f3601152a9e1",
            "compositeImage": {
                "id": "c989d7dd-c4f1-4c74-a81b-4d5c58b9c2fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb809d20-568a-4c4e-8aeb-053bc29721bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d93f3599-51e6-4596-90f9-0e0a15bf9ac0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb809d20-568a-4c4e-8aeb-053bc29721bc",
                    "LayerId": "9a5ed200-3472-4490-8597-fe51ed238c85"
                }
            ]
        },
        {
            "id": "2168787d-1cc0-4004-a67a-bd48da639df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68fa76ec-9b9a-4998-ba90-f3601152a9e1",
            "compositeImage": {
                "id": "65ac716c-6996-48d8-8dca-5dd63332ef81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2168787d-1cc0-4004-a67a-bd48da639df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd4afe9-c04f-4819-b6f4-920e08559cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2168787d-1cc0-4004-a67a-bd48da639df3",
                    "LayerId": "9a5ed200-3472-4490-8597-fe51ed238c85"
                }
            ]
        },
        {
            "id": "b82d45a8-03e5-43e3-86c3-6c7f8e1eb767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68fa76ec-9b9a-4998-ba90-f3601152a9e1",
            "compositeImage": {
                "id": "9693e4b4-a419-49ff-b8c7-a953ab080d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b82d45a8-03e5-43e3-86c3-6c7f8e1eb767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beefcd8c-201b-47b0-ac32-88edfa609938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b82d45a8-03e5-43e3-86c3-6c7f8e1eb767",
                    "LayerId": "9a5ed200-3472-4490-8597-fe51ed238c85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9a5ed200-3472-4490-8597-fe51ed238c85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68fa76ec-9b9a-4998-ba90-f3601152a9e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}