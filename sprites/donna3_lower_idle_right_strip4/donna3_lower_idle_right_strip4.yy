{
    "id": "1e205182-8a2f-441c-b720-f57c13dc225d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b49cd6ff-42a8-4130-815c-42f3e7c76294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e205182-8a2f-441c-b720-f57c13dc225d",
            "compositeImage": {
                "id": "ad5f834a-1102-42e0-b11c-90f5410efd68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b49cd6ff-42a8-4130-815c-42f3e7c76294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d03a2f-0d4d-45a1-bf44-3fb09556b7f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b49cd6ff-42a8-4130-815c-42f3e7c76294",
                    "LayerId": "d42f495a-4a1b-4be7-acea-8b07917facb7"
                }
            ]
        },
        {
            "id": "a349d9f9-f9a9-4a1b-bc90-e1811052c726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e205182-8a2f-441c-b720-f57c13dc225d",
            "compositeImage": {
                "id": "4b7aee65-acb8-47ec-a18a-9bee279fb438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a349d9f9-f9a9-4a1b-bc90-e1811052c726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a687d6-1239-40b6-a22d-51284ec9caba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a349d9f9-f9a9-4a1b-bc90-e1811052c726",
                    "LayerId": "d42f495a-4a1b-4be7-acea-8b07917facb7"
                }
            ]
        },
        {
            "id": "f2b01759-a1fa-443a-ac85-83789cb1146f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e205182-8a2f-441c-b720-f57c13dc225d",
            "compositeImage": {
                "id": "23740f39-df42-49a3-8543-8cbafbe1aefc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2b01759-a1fa-443a-ac85-83789cb1146f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0fd45a-e5f7-43d2-b77d-053b40969ac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2b01759-a1fa-443a-ac85-83789cb1146f",
                    "LayerId": "d42f495a-4a1b-4be7-acea-8b07917facb7"
                }
            ]
        },
        {
            "id": "45cca631-fe64-47bf-bda0-e4d7ec1b5244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e205182-8a2f-441c-b720-f57c13dc225d",
            "compositeImage": {
                "id": "33cebce0-d86c-4361-bcf8-9e66014f1193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45cca631-fe64-47bf-bda0-e4d7ec1b5244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "389b8230-ef21-4bdf-a62c-165c34283b39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45cca631-fe64-47bf-bda0-e4d7ec1b5244",
                    "LayerId": "d42f495a-4a1b-4be7-acea-8b07917facb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d42f495a-4a1b-4be7-acea-8b07917facb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e205182-8a2f-441c-b720-f57c13dc225d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}