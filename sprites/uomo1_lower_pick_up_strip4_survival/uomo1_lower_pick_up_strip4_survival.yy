{
    "id": "32ecfb25-b767-4945-b229-d84715b38af4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_pick_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "515d2e13-c5f2-4c1e-87d7-c4099d2789fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "compositeImage": {
                "id": "5cd4a920-b53d-44db-9b59-631e555465c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "515d2e13-c5f2-4c1e-87d7-c4099d2789fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b91832-422f-4f0e-868d-305a04f670c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "515d2e13-c5f2-4c1e-87d7-c4099d2789fe",
                    "LayerId": "5f45b425-799d-43d8-b892-0a2172c85c93"
                }
            ]
        },
        {
            "id": "39752691-c017-4cac-b0a3-4e8208a81ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "compositeImage": {
                "id": "3a5c8944-1f7b-4f4a-8921-e0867a5c58a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39752691-c017-4cac-b0a3-4e8208a81ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2720ff4c-fafb-4013-9fd7-1b6090ea0fbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39752691-c017-4cac-b0a3-4e8208a81ccc",
                    "LayerId": "5f45b425-799d-43d8-b892-0a2172c85c93"
                }
            ]
        },
        {
            "id": "99c51fe3-8d89-4446-816b-445b36c5a500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "compositeImage": {
                "id": "27af1405-50f6-4fee-ab3a-7ac632316f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99c51fe3-8d89-4446-816b-445b36c5a500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c1c320f-4d83-421c-838e-93bc33797973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99c51fe3-8d89-4446-816b-445b36c5a500",
                    "LayerId": "5f45b425-799d-43d8-b892-0a2172c85c93"
                }
            ]
        },
        {
            "id": "904cce11-5b0f-4b1e-85a6-1206facbb2fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "compositeImage": {
                "id": "a19696d2-5c1e-4c9b-96cd-d9acf22c2c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "904cce11-5b0f-4b1e-85a6-1206facbb2fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98f534d9-2a49-4367-a3cb-2dd77390ec5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "904cce11-5b0f-4b1e-85a6-1206facbb2fd",
                    "LayerId": "5f45b425-799d-43d8-b892-0a2172c85c93"
                }
            ]
        },
        {
            "id": "2d202d21-04a1-4edf-9072-83cbaa25f7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "compositeImage": {
                "id": "3f7e5afe-aaa0-46ea-9701-b28408a95813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d202d21-04a1-4edf-9072-83cbaa25f7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33dc4bb-762a-4718-9452-319ea7a5203b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d202d21-04a1-4edf-9072-83cbaa25f7ca",
                    "LayerId": "5f45b425-799d-43d8-b892-0a2172c85c93"
                }
            ]
        },
        {
            "id": "7629b55b-c5ed-4636-8168-7509ecb7aa25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "compositeImage": {
                "id": "2a85dca9-a672-4642-90b9-ff7d1835ad63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7629b55b-c5ed-4636-8168-7509ecb7aa25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecaecb37-7979-4b55-8f67-d7d6511a5504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7629b55b-c5ed-4636-8168-7509ecb7aa25",
                    "LayerId": "5f45b425-799d-43d8-b892-0a2172c85c93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5f45b425-799d-43d8-b892-0a2172c85c93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32ecfb25-b767-4945-b229-d84715b38af4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}