{
    "id": "ea533252-0ed6-4cc2-9d4a-57d3dc92a987",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_char",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5035652e-8a2b-4b48-96f7-8d825bb83c7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea533252-0ed6-4cc2-9d4a-57d3dc92a987",
            "compositeImage": {
                "id": "e1adf85b-506c-436a-8a08-f18188836c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5035652e-8a2b-4b48-96f7-8d825bb83c7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "470a1010-842e-4e54-8a5a-6ccee9ea8316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5035652e-8a2b-4b48-96f7-8d825bb83c7f",
                    "LayerId": "7ff5af17-fa04-459f-bebc-3969f8292070"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7ff5af17-fa04-459f-bebc-3969f8292070",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea533252-0ed6-4cc2-9d4a-57d3dc92a987",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}