{
    "id": "3bc2b2c2-13b7-45e3-b247-a6ba784d0a86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_thumbsup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 6,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87658e08-94a3-41f5-9840-f2e913d773d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bc2b2c2-13b7-45e3-b247-a6ba784d0a86",
            "compositeImage": {
                "id": "d034bbec-4bf8-4819-9a6b-19be194d655d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87658e08-94a3-41f5-9840-f2e913d773d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7330320-040d-458e-a10b-644061f13324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87658e08-94a3-41f5-9840-f2e913d773d6",
                    "LayerId": "24419e9e-2eff-4d1b-bd16-d2dbb8e3e7c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "24419e9e-2eff-4d1b-bd16-d2dbb8e3e7c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bc2b2c2-13b7-45e3-b247-a6ba784d0a86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}