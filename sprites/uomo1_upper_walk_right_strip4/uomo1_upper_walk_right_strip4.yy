{
    "id": "2603e47b-4e85-47cb-b98b-a2df8f31f326",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "569b5ca7-1605-4cf9-bf11-f9cd194ca65e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2603e47b-4e85-47cb-b98b-a2df8f31f326",
            "compositeImage": {
                "id": "41822e38-833a-48fb-a103-c097442d6e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569b5ca7-1605-4cf9-bf11-f9cd194ca65e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85d6db85-adcf-497c-8478-07b558a5784b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569b5ca7-1605-4cf9-bf11-f9cd194ca65e",
                    "LayerId": "a2e155cc-2f40-4157-9d50-22ff86ad3613"
                }
            ]
        },
        {
            "id": "20423c13-6bac-4a09-9827-90e874ea87a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2603e47b-4e85-47cb-b98b-a2df8f31f326",
            "compositeImage": {
                "id": "e17c368b-4191-42fc-b841-bf1bec54705b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20423c13-6bac-4a09-9827-90e874ea87a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b15f743b-3691-4248-8750-11499a13a089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20423c13-6bac-4a09-9827-90e874ea87a0",
                    "LayerId": "a2e155cc-2f40-4157-9d50-22ff86ad3613"
                }
            ]
        },
        {
            "id": "67289094-3ad3-4d04-a1fe-3e331a0dbe29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2603e47b-4e85-47cb-b98b-a2df8f31f326",
            "compositeImage": {
                "id": "16285f97-23c4-4bc0-9d5e-d0f78c225413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67289094-3ad3-4d04-a1fe-3e331a0dbe29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "673d00c5-1a3e-454a-92e4-02771bd827f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67289094-3ad3-4d04-a1fe-3e331a0dbe29",
                    "LayerId": "a2e155cc-2f40-4157-9d50-22ff86ad3613"
                }
            ]
        },
        {
            "id": "aae083d0-da83-4e05-89ee-259517274cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2603e47b-4e85-47cb-b98b-a2df8f31f326",
            "compositeImage": {
                "id": "b266580a-ef0d-48a9-a357-0f66235546d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aae083d0-da83-4e05-89ee-259517274cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2d0244-3599-4110-a1a3-1bbb1c02fab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aae083d0-da83-4e05-89ee-259517274cff",
                    "LayerId": "a2e155cc-2f40-4157-9d50-22ff86ad3613"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2e155cc-2f40-4157-9d50-22ff86ad3613",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2603e47b-4e85-47cb-b98b-a2df8f31f326",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}