{
    "id": "c7674dca-21ba-4b9b-b14b-9bedeff3791c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_chest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa02d87c-2e48-40c7-b4a2-0fd5eb7fb78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7674dca-21ba-4b9b-b14b-9bedeff3791c",
            "compositeImage": {
                "id": "1ea7cbdd-fae8-417c-83f2-e9e9ffeb5d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa02d87c-2e48-40c7-b4a2-0fd5eb7fb78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3507f7d2-f293-432e-bd48-b507ea1bbaa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa02d87c-2e48-40c7-b4a2-0fd5eb7fb78d",
                    "LayerId": "853e8540-cc12-4d66-8ca8-ae22f59ab2b1"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 16,
    "height": 16,
    "layers": [
        {
            "id": "853e8540-cc12-4d66-8ca8-ae22f59ab2b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7674dca-21ba-4b9b-b14b-9bedeff3791c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}