{
    "id": "9c62d4f5-0aef-49b3-914f-b9545eb317e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c8e4e40-cc10-4fd5-a769-8fe7e3c398bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c62d4f5-0aef-49b3-914f-b9545eb317e1",
            "compositeImage": {
                "id": "39d7d73f-7409-4522-9da0-d9665b85b2f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c8e4e40-cc10-4fd5-a769-8fe7e3c398bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32072e53-304a-47aa-87eb-e69410aa9a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c8e4e40-cc10-4fd5-a769-8fe7e3c398bd",
                    "LayerId": "107efe28-0f61-412b-b4bd-ea1323e94dbe"
                }
            ]
        },
        {
            "id": "823d2ab8-5aab-4073-9c54-415f11b92dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c62d4f5-0aef-49b3-914f-b9545eb317e1",
            "compositeImage": {
                "id": "83afd381-970a-4298-861c-b984441fbb9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823d2ab8-5aab-4073-9c54-415f11b92dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44985b90-5a68-4d38-8e30-4980f09ceb05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823d2ab8-5aab-4073-9c54-415f11b92dce",
                    "LayerId": "107efe28-0f61-412b-b4bd-ea1323e94dbe"
                }
            ]
        },
        {
            "id": "a71ee872-7528-4997-8e67-2a11cb64159f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c62d4f5-0aef-49b3-914f-b9545eb317e1",
            "compositeImage": {
                "id": "14ea9295-c965-4dc1-b80c-0df4ccebed03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a71ee872-7528-4997-8e67-2a11cb64159f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5515c266-130e-49b3-a812-28da973ac389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a71ee872-7528-4997-8e67-2a11cb64159f",
                    "LayerId": "107efe28-0f61-412b-b4bd-ea1323e94dbe"
                }
            ]
        },
        {
            "id": "e3f53204-5f07-4278-9cc2-35c97ad05d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c62d4f5-0aef-49b3-914f-b9545eb317e1",
            "compositeImage": {
                "id": "6c900785-15c9-4d16-820c-3297fa304a44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f53204-5f07-4278-9cc2-35c97ad05d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a99908f6-578b-4403-b1c4-4b2580dc440d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f53204-5f07-4278-9cc2-35c97ad05d51",
                    "LayerId": "107efe28-0f61-412b-b4bd-ea1323e94dbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "107efe28-0f61-412b-b4bd-ea1323e94dbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c62d4f5-0aef-49b3-914f-b9545eb317e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}