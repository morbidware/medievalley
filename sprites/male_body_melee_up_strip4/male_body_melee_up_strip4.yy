{
    "id": "96b74a63-2f96-4d28-a31c-bcfe903ca9af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68cc2088-6f9d-419e-9d15-3395b502a681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b74a63-2f96-4d28-a31c-bcfe903ca9af",
            "compositeImage": {
                "id": "2fc0feae-1569-4064-a054-bd30557d91ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68cc2088-6f9d-419e-9d15-3395b502a681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc1e99a-b83f-4bf4-adea-c1a434f91658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68cc2088-6f9d-419e-9d15-3395b502a681",
                    "LayerId": "438afb8d-8f6e-42fa-b89e-e2305a5817b3"
                }
            ]
        },
        {
            "id": "2e96b2d1-1c02-4891-8f65-aefb05cb5ab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b74a63-2f96-4d28-a31c-bcfe903ca9af",
            "compositeImage": {
                "id": "cd8a1e47-ae39-41d4-a68b-c02ed33e3566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e96b2d1-1c02-4891-8f65-aefb05cb5ab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32cdb85e-fee5-4b61-aebb-5cdce4f8ecda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e96b2d1-1c02-4891-8f65-aefb05cb5ab1",
                    "LayerId": "438afb8d-8f6e-42fa-b89e-e2305a5817b3"
                }
            ]
        },
        {
            "id": "59b2004e-e7b4-450d-b9c2-3ff90470381e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b74a63-2f96-4d28-a31c-bcfe903ca9af",
            "compositeImage": {
                "id": "aa43e38e-a941-412a-b19f-d0604e43164a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b2004e-e7b4-450d-b9c2-3ff90470381e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e373069-eb33-4b43-9b18-0070af3321c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b2004e-e7b4-450d-b9c2-3ff90470381e",
                    "LayerId": "438afb8d-8f6e-42fa-b89e-e2305a5817b3"
                }
            ]
        },
        {
            "id": "9bafb952-6095-48f2-9f1f-44f6056a63b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b74a63-2f96-4d28-a31c-bcfe903ca9af",
            "compositeImage": {
                "id": "3381df62-124f-4865-8656-153de3560c5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bafb952-6095-48f2-9f1f-44f6056a63b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b6307e9-6f18-4c58-a3e3-58e8b1f7d59c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bafb952-6095-48f2-9f1f-44f6056a63b4",
                    "LayerId": "438afb8d-8f6e-42fa-b89e-e2305a5817b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "438afb8d-8f6e-42fa-b89e-e2305a5817b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96b74a63-2f96-4d28-a31c-bcfe903ca9af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}