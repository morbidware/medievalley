{
    "id": "f6717264-6d89-42d6-af91-debfaddd6c95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_suffer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5337eee-04e9-4f05-bc60-ce337796a1a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6717264-6d89-42d6-af91-debfaddd6c95",
            "compositeImage": {
                "id": "60c7b68b-e092-46c9-845b-7e61ebe9dd0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5337eee-04e9-4f05-bc60-ce337796a1a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0c84a4d-535f-4d75-81a7-02e3d20b1534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5337eee-04e9-4f05-bc60-ce337796a1a1",
                    "LayerId": "6a8f2f9c-4796-4a4f-a3f2-c026f4bad267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6a8f2f9c-4796-4a4f-a3f2-c026f4bad267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6717264-6d89-42d6-af91-debfaddd6c95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}