{
    "id": "7688ffe3-5cbe-4406-8cff-efe546493153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_potato_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3823e8e-1eb5-4c25-ac3b-5e833d2b6e85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
            "compositeImage": {
                "id": "fd0b699f-006f-4cad-9d34-0648bd77873b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3823e8e-1eb5-4c25-ac3b-5e833d2b6e85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49ccb0ca-c1f9-415b-80ee-d04a5a2e41f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3823e8e-1eb5-4c25-ac3b-5e833d2b6e85",
                    "LayerId": "e73137fe-db7d-46c8-af4f-4667c5d36411"
                }
            ]
        },
        {
            "id": "516b085a-acb7-4234-81cb-aef42a142622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
            "compositeImage": {
                "id": "8833c724-1fb3-4fb4-bffa-31c39a40d2d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516b085a-acb7-4234-81cb-aef42a142622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d7d656-febc-438d-8473-b27940408770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516b085a-acb7-4234-81cb-aef42a142622",
                    "LayerId": "e73137fe-db7d-46c8-af4f-4667c5d36411"
                }
            ]
        },
        {
            "id": "3e01abff-2048-403d-b412-2b3802721b18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
            "compositeImage": {
                "id": "64f7dc17-1136-4c83-b998-f90748fb3aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e01abff-2048-403d-b412-2b3802721b18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c6f2b1-b1aa-41cd-a4d2-e44f1787267c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e01abff-2048-403d-b412-2b3802721b18",
                    "LayerId": "e73137fe-db7d-46c8-af4f-4667c5d36411"
                }
            ]
        },
        {
            "id": "11bd34ca-02a3-4cf1-b72e-51c5d2348ab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
            "compositeImage": {
                "id": "f4573e98-d4e8-4989-aaf9-23b48645cb65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11bd34ca-02a3-4cf1-b72e-51c5d2348ab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1f7826-33a5-426a-8a4b-8f1e7e10afe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11bd34ca-02a3-4cf1-b72e-51c5d2348ab0",
                    "LayerId": "e73137fe-db7d-46c8-af4f-4667c5d36411"
                }
            ]
        },
        {
            "id": "f3e0445a-d1ff-4262-8428-257893dc69b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
            "compositeImage": {
                "id": "09b1aa0c-28f4-460d-9aca-4924e5f2737b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3e0445a-d1ff-4262-8428-257893dc69b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1a44adc-f43f-45ab-aec4-d63022c110a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3e0445a-d1ff-4262-8428-257893dc69b9",
                    "LayerId": "e73137fe-db7d-46c8-af4f-4667c5d36411"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e73137fe-db7d-46c8-af4f-4667c5d36411",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7688ffe3-5cbe-4406-8cff-efe546493153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 26
}