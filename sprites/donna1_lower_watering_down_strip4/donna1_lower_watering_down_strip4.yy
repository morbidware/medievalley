{
    "id": "325934dc-5ad2-4162-ae7e-8c3c551aded2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85a3bff3-e0f7-4497-8e89-8b8b46a44e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325934dc-5ad2-4162-ae7e-8c3c551aded2",
            "compositeImage": {
                "id": "89aa15b3-bffa-4215-b9cf-212939a57b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85a3bff3-e0f7-4497-8e89-8b8b46a44e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c204659-d205-46eb-a382-09d9374aafc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85a3bff3-e0f7-4497-8e89-8b8b46a44e5e",
                    "LayerId": "a1a5dbe1-898b-4ea7-b377-bd6d039b23f5"
                }
            ]
        },
        {
            "id": "9a16d683-0aac-4810-86b5-7314ec39329b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325934dc-5ad2-4162-ae7e-8c3c551aded2",
            "compositeImage": {
                "id": "10ad19ce-92da-4a55-8d92-828656141190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a16d683-0aac-4810-86b5-7314ec39329b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef893f73-d395-4849-9be8-70c31baaa6fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a16d683-0aac-4810-86b5-7314ec39329b",
                    "LayerId": "a1a5dbe1-898b-4ea7-b377-bd6d039b23f5"
                }
            ]
        },
        {
            "id": "fa430be3-5e24-4364-9ec9-261f9250387b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325934dc-5ad2-4162-ae7e-8c3c551aded2",
            "compositeImage": {
                "id": "473194d7-d032-4f7c-bf04-72b0deb65def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa430be3-5e24-4364-9ec9-261f9250387b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c12bd62-d57e-4df4-9bd9-69683d878f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa430be3-5e24-4364-9ec9-261f9250387b",
                    "LayerId": "a1a5dbe1-898b-4ea7-b377-bd6d039b23f5"
                }
            ]
        },
        {
            "id": "c6c7e801-1798-4e2c-9469-5f52922c3977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "325934dc-5ad2-4162-ae7e-8c3c551aded2",
            "compositeImage": {
                "id": "c502b21e-0252-4b3c-9bca-6c2501be74f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c7e801-1798-4e2c-9469-5f52922c3977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e7a37b-0987-4581-a539-e5514b9c07fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c7e801-1798-4e2c-9469-5f52922c3977",
                    "LayerId": "a1a5dbe1-898b-4ea7-b377-bd6d039b23f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a1a5dbe1-898b-4ea7-b377-bd6d039b23f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "325934dc-5ad2-4162-ae7e-8c3c551aded2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}