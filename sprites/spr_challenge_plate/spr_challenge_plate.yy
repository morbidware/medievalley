{
    "id": "0fbd15fd-5724-4df9-a6f1-b49f520e1cff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_challenge_plate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 2,
    "bbox_right": 86,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4d848ff-5e52-4557-9c54-a94b55f96e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fbd15fd-5724-4df9-a6f1-b49f520e1cff",
            "compositeImage": {
                "id": "9b171638-ff1a-40a0-b272-dff9a6a1e95b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d848ff-5e52-4557-9c54-a94b55f96e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c533ff-5b15-4f3c-bdc8-ed08c906deb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d848ff-5e52-4557-9c54-a94b55f96e69",
                    "LayerId": "331b6c90-8fdf-4124-8be9-690e055a1795"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "331b6c90-8fdf-4124-8be9-690e055a1795",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fbd15fd-5724-4df9-a6f1-b49f520e1cff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 45,
    "yorig": 42
}