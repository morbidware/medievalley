{
    "id": "3e399353-85c5-45e5-8f2c-4ce572a24457",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f430b30-371c-4b29-9021-6dec25d6ae05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "compositeImage": {
                "id": "3774a0f1-531f-4e8c-8e44-d59cdfa80677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f430b30-371c-4b29-9021-6dec25d6ae05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f50353d-d2c8-4502-911f-125fe3fbf668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f430b30-371c-4b29-9021-6dec25d6ae05",
                    "LayerId": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2"
                }
            ]
        },
        {
            "id": "d0c124ce-3fcd-421a-b5fc-5f8d2ba9e4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "compositeImage": {
                "id": "8271263d-fc63-46ca-b1c0-104a37547bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c124ce-3fcd-421a-b5fc-5f8d2ba9e4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec13f7f6-84b4-48ca-a2ba-a32252bc27a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c124ce-3fcd-421a-b5fc-5f8d2ba9e4cc",
                    "LayerId": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2"
                }
            ]
        },
        {
            "id": "3f1d311a-d368-4148-8787-9c9fdbaaed8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "compositeImage": {
                "id": "24ce733a-df0b-4346-823d-af5d669cae35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f1d311a-d368-4148-8787-9c9fdbaaed8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89fe9e72-f45d-49ac-91a5-2bbd681252e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f1d311a-d368-4148-8787-9c9fdbaaed8b",
                    "LayerId": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2"
                }
            ]
        },
        {
            "id": "5f8db936-0c3d-4d98-b0b7-e6e9411c44f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "compositeImage": {
                "id": "c6e734e8-b235-4143-af62-fba35cb9b58e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f8db936-0c3d-4d98-b0b7-e6e9411c44f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "642ece9f-7bb5-4692-8be9-5d85949e5e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f8db936-0c3d-4d98-b0b7-e6e9411c44f9",
                    "LayerId": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2"
                }
            ]
        },
        {
            "id": "9bb7a599-9989-4828-a7f3-9a217f9410fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "compositeImage": {
                "id": "557c1e56-5658-4261-9003-3e323789830c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb7a599-9989-4828-a7f3-9a217f9410fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6df7fb9f-74c5-4894-8788-3a9c4ade33b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb7a599-9989-4828-a7f3-9a217f9410fc",
                    "LayerId": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2"
                }
            ]
        },
        {
            "id": "016cbd9f-6cec-4f92-9ea5-523fff38f710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "compositeImage": {
                "id": "341fa11a-83b4-4abb-b247-668dbc5ddbea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016cbd9f-6cec-4f92-9ea5-523fff38f710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1422a16-35d2-4692-862e-7426b6ee12aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016cbd9f-6cec-4f92-9ea5-523fff38f710",
                    "LayerId": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "e6ec90c4-e3af-4d1d-96f6-0f2f94ae90c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e399353-85c5-45e5-8f2c-4ce572a24457",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 44
}