{
    "id": "9bca96bd-1a9a-4520-9565-e2e2a3cae3c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5cc154b-abcd-4aad-a90f-9b22fd467aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bca96bd-1a9a-4520-9565-e2e2a3cae3c4",
            "compositeImage": {
                "id": "80ea3c45-4cac-456e-a6e4-50cb15a0509c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cc154b-abcd-4aad-a90f-9b22fd467aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dccc77b8-6c23-4478-8e40-648eaa890c4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cc154b-abcd-4aad-a90f-9b22fd467aec",
                    "LayerId": "cc73e85c-202b-4c9e-bc6a-4b3ea78bae92"
                }
            ]
        },
        {
            "id": "32b52dc6-1d88-4c39-99db-99d13f996e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bca96bd-1a9a-4520-9565-e2e2a3cae3c4",
            "compositeImage": {
                "id": "43f1432d-7983-462a-afb9-b446b3918c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32b52dc6-1d88-4c39-99db-99d13f996e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f816082-a238-4cec-b750-f01b986ed2fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32b52dc6-1d88-4c39-99db-99d13f996e7f",
                    "LayerId": "cc73e85c-202b-4c9e-bc6a-4b3ea78bae92"
                }
            ]
        },
        {
            "id": "87048681-31e5-40cd-80fb-1d6a167eff85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bca96bd-1a9a-4520-9565-e2e2a3cae3c4",
            "compositeImage": {
                "id": "a53de289-e633-4f27-b1a1-3b60c9b647af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87048681-31e5-40cd-80fb-1d6a167eff85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc82b35-02ec-4b7a-9124-345c8b2baf80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87048681-31e5-40cd-80fb-1d6a167eff85",
                    "LayerId": "cc73e85c-202b-4c9e-bc6a-4b3ea78bae92"
                }
            ]
        },
        {
            "id": "509704d0-ac18-4e99-9add-66514b7e02d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bca96bd-1a9a-4520-9565-e2e2a3cae3c4",
            "compositeImage": {
                "id": "8e4db59c-af80-418d-a0a4-52dd21fc7f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "509704d0-ac18-4e99-9add-66514b7e02d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "828b656b-5ddc-4d2e-9297-c6c6beccb011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509704d0-ac18-4e99-9add-66514b7e02d0",
                    "LayerId": "cc73e85c-202b-4c9e-bc6a-4b3ea78bae92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc73e85c-202b-4c9e-bc6a-4b3ea78bae92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bca96bd-1a9a-4520-9565-e2e2a3cae3c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}