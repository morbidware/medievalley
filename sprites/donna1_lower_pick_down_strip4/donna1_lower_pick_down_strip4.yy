{
    "id": "8118bb9a-df45-45f7-ae95-cca6306075a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d082258b-23e9-410d-8a4c-ef8d77e4bbc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8118bb9a-df45-45f7-ae95-cca6306075a4",
            "compositeImage": {
                "id": "3f0f21cd-4071-4e63-8618-f44dbf348c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d082258b-23e9-410d-8a4c-ef8d77e4bbc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0739dc-84fd-4b44-aae1-2398861047b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d082258b-23e9-410d-8a4c-ef8d77e4bbc2",
                    "LayerId": "48cd7e3c-51a6-4b8b-83f4-e018f7039c91"
                }
            ]
        },
        {
            "id": "26ff25ec-a529-45b1-897e-6aa89bcac27b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8118bb9a-df45-45f7-ae95-cca6306075a4",
            "compositeImage": {
                "id": "af238f96-db9f-48ce-9047-5a94336e26aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ff25ec-a529-45b1-897e-6aa89bcac27b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132bfdf9-634f-4a52-ac53-bccab41b47d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ff25ec-a529-45b1-897e-6aa89bcac27b",
                    "LayerId": "48cd7e3c-51a6-4b8b-83f4-e018f7039c91"
                }
            ]
        },
        {
            "id": "a4166ec6-649e-4ae4-9eb4-de6c31152e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8118bb9a-df45-45f7-ae95-cca6306075a4",
            "compositeImage": {
                "id": "3adb54e3-b874-43fb-a37d-88e2d5bd1cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4166ec6-649e-4ae4-9eb4-de6c31152e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff60abb-2d46-4069-967b-7846d96cbbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4166ec6-649e-4ae4-9eb4-de6c31152e3c",
                    "LayerId": "48cd7e3c-51a6-4b8b-83f4-e018f7039c91"
                }
            ]
        },
        {
            "id": "bc3bcbb4-b87e-45c3-807c-4b5e392fde4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8118bb9a-df45-45f7-ae95-cca6306075a4",
            "compositeImage": {
                "id": "6e558d39-f0a4-4bf3-be37-d427e73c3b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc3bcbb4-b87e-45c3-807c-4b5e392fde4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef80cfd4-3e6d-4965-acaa-b42506b24e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc3bcbb4-b87e-45c3-807c-4b5e392fde4c",
                    "LayerId": "48cd7e3c-51a6-4b8b-83f4-e018f7039c91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "48cd7e3c-51a6-4b8b-83f4-e018f7039c91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8118bb9a-df45-45f7-ae95-cca6306075a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}