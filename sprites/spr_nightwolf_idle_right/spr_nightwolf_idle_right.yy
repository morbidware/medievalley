{
    "id": "67de36b3-4ab3-43d3-92e4-da70943cf3cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 9,
    "bbox_right": 47,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f06ae1f-2cef-4254-83ea-1ca45dbd4019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67de36b3-4ab3-43d3-92e4-da70943cf3cd",
            "compositeImage": {
                "id": "3aff1d9f-a31a-431c-8e1e-e1e113d48c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f06ae1f-2cef-4254-83ea-1ca45dbd4019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "575d6636-5314-4563-b549-1835db8d190e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f06ae1f-2cef-4254-83ea-1ca45dbd4019",
                    "LayerId": "1bd53b56-6be2-413c-bd2d-e5c9db06c11e"
                }
            ]
        },
        {
            "id": "72f7ecc0-6988-412e-96ef-9370b876e437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67de36b3-4ab3-43d3-92e4-da70943cf3cd",
            "compositeImage": {
                "id": "1cb08f38-4d88-445f-975d-0a88dd02b05c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f7ecc0-6988-412e-96ef-9370b876e437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffc41f3-cceb-401c-b94d-80ae0a839ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f7ecc0-6988-412e-96ef-9370b876e437",
                    "LayerId": "1bd53b56-6be2-413c-bd2d-e5c9db06c11e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "1bd53b56-6be2-413c-bd2d-e5c9db06c11e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67de36b3-4ab3-43d3-92e4-da70943cf3cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}