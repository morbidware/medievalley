{
    "id": "a5b5d839-7c3b-42bc-8ef8-1a4463d855fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_flint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cf2a06f-f450-409b-ac21-e2253e4a9297",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5b5d839-7c3b-42bc-8ef8-1a4463d855fa",
            "compositeImage": {
                "id": "93e9ea6b-3758-48c8-a261-695ec7369fad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf2a06f-f450-409b-ac21-e2253e4a9297",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f264c377-cc1d-46c1-b0da-3093b3dfa824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf2a06f-f450-409b-ac21-e2253e4a9297",
                    "LayerId": "9423da2b-8b53-477e-9f9d-6bf19f3f9987"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9423da2b-8b53-477e-9f9d-6bf19f3f9987",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5b5d839-7c3b-42bc-8ef8-1a4463d855fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}