{
    "id": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mailbox_container_unread",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "802fa07e-5cab-4ece-ad24-504bd1f61789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "3e8ddc7f-978f-4c80-8ce4-6f7b4b6abb3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "802fa07e-5cab-4ece-ad24-504bd1f61789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c09f2c76-5ca5-47e2-a73f-bdf265151165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802fa07e-5cab-4ece-ad24-504bd1f61789",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "9d934023-b344-44ca-a73d-ac3b863faa1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "b7d709ff-f645-48d7-99b4-bdcc8fba30d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d934023-b344-44ca-a73d-ac3b863faa1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e21857-ac71-46e9-a309-5fcec3a014c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d934023-b344-44ca-a73d-ac3b863faa1b",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "cbeed883-04b5-45b2-86fd-da2e7452398b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "db96eca5-8cf2-4f56-b7df-923db6422dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbeed883-04b5-45b2-86fd-da2e7452398b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702c51b3-876a-4891-913a-ca40d0a6a526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbeed883-04b5-45b2-86fd-da2e7452398b",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "176d7394-721f-4730-8e56-a2fc0dce15e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "e7292ef9-6341-4b6b-954b-64b963eb9df2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "176d7394-721f-4730-8e56-a2fc0dce15e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9f5c62-d19b-4112-81fb-d7f19fa51176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "176d7394-721f-4730-8e56-a2fc0dce15e5",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "f1ca35a6-de6d-4516-9c5d-639ff8cce6e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "f8824489-359b-432e-b83f-7acb50444f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ca35a6-de6d-4516-9c5d-639ff8cce6e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29216b2b-a6f8-4deb-8b20-7fa15a5d86f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ca35a6-de6d-4516-9c5d-639ff8cce6e2",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "6aaa8f9c-6a92-47f9-a5ad-cd899d44ae22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "42be6285-c7f7-4030-82a0-fe1dc1585f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aaa8f9c-6a92-47f9-a5ad-cd899d44ae22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f0a7d58-c50a-430d-add2-66bfac189915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aaa8f9c-6a92-47f9-a5ad-cd899d44ae22",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "b50e6fb2-4c6b-49be-bc2c-3b9d5181b6f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "3496d97f-e7d5-4ed1-9875-2d086c1cf5bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50e6fb2-4c6b-49be-bc2c-3b9d5181b6f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a3be1c-bee5-4863-8c82-2f59deef3101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50e6fb2-4c6b-49be-bc2c-3b9d5181b6f1",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "b1b46447-2305-4733-aeb8-703af5d78a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "9772e693-fad3-4802-9994-c9dd61250ddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1b46447-2305-4733-aeb8-703af5d78a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed0eca4-baa2-4ad3-a9ea-313d79d7d2a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1b46447-2305-4733-aeb8-703af5d78a93",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        },
        {
            "id": "b25b75b5-9111-4f56-a1fd-20a9e2e4e722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "compositeImage": {
                "id": "80cdf9c7-0a2e-47a3-bb43-bfd4f5e4440b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25b75b5-9111-4f56-a1fd-20a9e2e4e722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3365e02-e9eb-45a1-a2ec-cbda093305eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25b75b5-9111-4f56-a1fd-20a9e2e4e722",
                    "LayerId": "8daa0bf2-437e-46ab-9786-f1929af8bebf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "8daa0bf2-437e-46ab-9786-f1929af8bebf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b36d3a5b-b7e7-41fd-97fe-435d7feab153",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 0,
    "yorig": 0
}