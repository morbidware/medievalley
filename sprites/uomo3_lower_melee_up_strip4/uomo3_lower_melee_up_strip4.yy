{
    "id": "9f81d8b0-8a4e-4d58-b825-f864d78c67ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b11d6081-cbb2-445a-bbe4-7864323ca1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f81d8b0-8a4e-4d58-b825-f864d78c67ea",
            "compositeImage": {
                "id": "8b7e4029-21c0-4f39-8a66-3ac4524b7998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11d6081-cbb2-445a-bbe4-7864323ca1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8578323f-2717-40c6-a334-aa145c9c36be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11d6081-cbb2-445a-bbe4-7864323ca1a9",
                    "LayerId": "dd2ae9fc-420a-456f-a510-881fc2775d84"
                }
            ]
        },
        {
            "id": "92534ea0-9cc7-4518-a263-6b6aca7ffac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f81d8b0-8a4e-4d58-b825-f864d78c67ea",
            "compositeImage": {
                "id": "04ec22ba-141f-405a-995e-7ba239dbb024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92534ea0-9cc7-4518-a263-6b6aca7ffac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be31a6e1-19ee-41a2-88bc-6ec0b6ef4dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92534ea0-9cc7-4518-a263-6b6aca7ffac1",
                    "LayerId": "dd2ae9fc-420a-456f-a510-881fc2775d84"
                }
            ]
        },
        {
            "id": "e5a929e4-dd97-40f1-b294-c00884bfd935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f81d8b0-8a4e-4d58-b825-f864d78c67ea",
            "compositeImage": {
                "id": "cfdc7824-6a1b-4629-b561-73cf4de13b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a929e4-dd97-40f1-b294-c00884bfd935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40cd2e85-47b8-48c0-b4bf-a5709377358c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a929e4-dd97-40f1-b294-c00884bfd935",
                    "LayerId": "dd2ae9fc-420a-456f-a510-881fc2775d84"
                }
            ]
        },
        {
            "id": "4ff638e0-4f60-46f4-889f-241648b5c3b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f81d8b0-8a4e-4d58-b825-f864d78c67ea",
            "compositeImage": {
                "id": "aa9532c3-9623-4ff7-8483-4d14b5f79f6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff638e0-4f60-46f4-889f-241648b5c3b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7da868b6-29ff-433b-944f-8bfe7306435e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff638e0-4f60-46f4-889f-241648b5c3b3",
                    "LayerId": "dd2ae9fc-420a-456f-a510-881fc2775d84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd2ae9fc-420a-456f-a510-881fc2775d84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f81d8b0-8a4e-4d58-b825-f864d78c67ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}