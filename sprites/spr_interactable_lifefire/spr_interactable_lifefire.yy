{
    "id": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_lifefire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98651662-49e1-4cf4-9bb6-3210ed597dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "compositeImage": {
                "id": "cb6b9e11-07b8-45af-bf07-2a70b6987349",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98651662-49e1-4cf4-9bb6-3210ed597dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5729a40d-1135-4068-af96-f617283334a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98651662-49e1-4cf4-9bb6-3210ed597dea",
                    "LayerId": "8f5a6d50-830a-4fb9-a864-c20922241bd1"
                }
            ]
        },
        {
            "id": "76d188f5-5ba8-4daa-9c12-a70101b94e9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "compositeImage": {
                "id": "762862d0-081d-4818-8f32-ec217a454c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76d188f5-5ba8-4daa-9c12-a70101b94e9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "904b5fe6-cf84-4b2b-b3af-ee94a5b838e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76d188f5-5ba8-4daa-9c12-a70101b94e9c",
                    "LayerId": "8f5a6d50-830a-4fb9-a864-c20922241bd1"
                }
            ]
        },
        {
            "id": "49bd4c75-592c-4780-8554-564978904365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "compositeImage": {
                "id": "a6893259-6047-45d5-aec1-d93ff20ca03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49bd4c75-592c-4780-8554-564978904365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675ea23e-eb91-47c6-a48b-35e43d0a145c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49bd4c75-592c-4780-8554-564978904365",
                    "LayerId": "8f5a6d50-830a-4fb9-a864-c20922241bd1"
                }
            ]
        },
        {
            "id": "066129ff-70e0-4a9c-ba8d-04820583f7df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "compositeImage": {
                "id": "4cf10244-b1d5-46d5-8681-0e4548c94602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "066129ff-70e0-4a9c-ba8d-04820583f7df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dc9778-be1f-4ef0-8230-bb3846ecb007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "066129ff-70e0-4a9c-ba8d-04820583f7df",
                    "LayerId": "8f5a6d50-830a-4fb9-a864-c20922241bd1"
                }
            ]
        },
        {
            "id": "b2363b5d-ba65-4d83-8684-2525fbbb955b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "compositeImage": {
                "id": "44fa43dd-a19f-44b8-b910-b170cc786d8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2363b5d-ba65-4d83-8684-2525fbbb955b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08391f0e-046f-4478-a4a6-a8195a58ef2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2363b5d-ba65-4d83-8684-2525fbbb955b",
                    "LayerId": "8f5a6d50-830a-4fb9-a864-c20922241bd1"
                }
            ]
        },
        {
            "id": "ad06f7fe-e4cf-4348-a0fa-47a6176d4da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "compositeImage": {
                "id": "7c68a7ff-7dce-4220-a397-12a60cea3c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad06f7fe-e4cf-4348-a0fa-47a6176d4da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88dd2cf4-6545-401e-bf8e-f3ead60f26ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad06f7fe-e4cf-4348-a0fa-47a6176d4da5",
                    "LayerId": "8f5a6d50-830a-4fb9-a864-c20922241bd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f5a6d50-830a-4fb9-a864-c20922241bd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20b61e48-c532-4b1d-9cf2-d9667824c09b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 24
}