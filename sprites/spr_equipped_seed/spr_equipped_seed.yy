{
    "id": "1c7ef30e-08fd-4f79-b72f-9af23f1cf658",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_seed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "537f4908-6d1c-4acf-bf63-f561a03ef2e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c7ef30e-08fd-4f79-b72f-9af23f1cf658",
            "compositeImage": {
                "id": "75029b48-18c8-4892-8c57-9aae9d2655da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537f4908-6d1c-4acf-bf63-f561a03ef2e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67aa5997-4613-45bf-8770-2740a37e5fde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537f4908-6d1c-4acf-bf63-f561a03ef2e9",
                    "LayerId": "53b2f6d4-ef21-4f50-b219-28116c798f33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "53b2f6d4-ef21-4f50-b219-28116c798f33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c7ef30e-08fd-4f79-b72f-9af23f1cf658",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}