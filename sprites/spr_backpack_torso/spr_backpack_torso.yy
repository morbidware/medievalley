{
    "id": "20ddbeb9-faf4-46ea-826f-5c0fa63eb569",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backpack_torso",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "945e2d5a-d897-41f0-8158-2680e5b5bc6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20ddbeb9-faf4-46ea-826f-5c0fa63eb569",
            "compositeImage": {
                "id": "e16e7fd9-d113-48ee-8539-f9256a9cece7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945e2d5a-d897-41f0-8158-2680e5b5bc6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12993926-cf17-4469-bfc1-bb76f2ee690d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945e2d5a-d897-41f0-8158-2680e5b5bc6e",
                    "LayerId": "6c0ed4ff-e37e-473b-9e27-f53004ff00a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "6c0ed4ff-e37e-473b-9e27-f53004ff00a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20ddbeb9-faf4-46ea-826f-5c0fa63eb569",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 6
}