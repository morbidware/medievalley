{
    "id": "87091459-080c-4c6f-ad57-c211cd3038d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "383bbc8e-7264-4e77-a203-15e532744cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87091459-080c-4c6f-ad57-c211cd3038d1",
            "compositeImage": {
                "id": "fd01ca5e-f224-431b-a7e3-703021962f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383bbc8e-7264-4e77-a203-15e532744cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96fe02e6-d549-4273-aa4c-f8c9f42fe64b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383bbc8e-7264-4e77-a203-15e532744cc0",
                    "LayerId": "b5e16b9f-72ae-4daa-8759-25d769335667"
                }
            ]
        },
        {
            "id": "b9f0c1ea-c40a-451f-bb6f-6ca4b0b183a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87091459-080c-4c6f-ad57-c211cd3038d1",
            "compositeImage": {
                "id": "e3d3fffd-cc43-476b-951e-cb103ab7f327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f0c1ea-c40a-451f-bb6f-6ca4b0b183a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3f655ee-c2af-48cf-b8ab-23f00aabf287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f0c1ea-c40a-451f-bb6f-6ca4b0b183a2",
                    "LayerId": "b5e16b9f-72ae-4daa-8759-25d769335667"
                }
            ]
        },
        {
            "id": "f663274d-7c0e-4fa9-8d6c-2e76d2cdf3ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87091459-080c-4c6f-ad57-c211cd3038d1",
            "compositeImage": {
                "id": "181b19c8-d983-4a18-842e-907878d9e7fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f663274d-7c0e-4fa9-8d6c-2e76d2cdf3ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "955ac3ab-37e0-41fb-a973-a932f74d0849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f663274d-7c0e-4fa9-8d6c-2e76d2cdf3ea",
                    "LayerId": "b5e16b9f-72ae-4daa-8759-25d769335667"
                }
            ]
        },
        {
            "id": "73ccd7bf-dce5-40d5-9c8d-1d192aeb5df3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87091459-080c-4c6f-ad57-c211cd3038d1",
            "compositeImage": {
                "id": "b1e84b38-244e-42b3-b649-329bcd8efe05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ccd7bf-dce5-40d5-9c8d-1d192aeb5df3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a6d9da-0003-439d-b3fb-e347f928b473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ccd7bf-dce5-40d5-9c8d-1d192aeb5df3",
                    "LayerId": "b5e16b9f-72ae-4daa-8759-25d769335667"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5e16b9f-72ae-4daa-8759-25d769335667",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87091459-080c-4c6f-ad57-c211cd3038d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}