{
    "id": "ced8410c-726e-4296-b8b1-d6b88bc910ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6491697c-db15-4554-94de-8ddd4084e5e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced8410c-726e-4296-b8b1-d6b88bc910ff",
            "compositeImage": {
                "id": "95429283-5c3e-4549-8450-1db7f199d9da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6491697c-db15-4554-94de-8ddd4084e5e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d1c773-4d03-4b20-9ccd-b87dcd4142e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6491697c-db15-4554-94de-8ddd4084e5e6",
                    "LayerId": "dbf20389-84e5-45a5-9d7d-f6a4e2aa242f"
                }
            ]
        },
        {
            "id": "605ec213-ad3f-465b-8ac4-569d795ea1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced8410c-726e-4296-b8b1-d6b88bc910ff",
            "compositeImage": {
                "id": "ae20ca05-0c76-460f-93a5-ba34c346811e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "605ec213-ad3f-465b-8ac4-569d795ea1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7572c3ac-c9a1-458a-8010-2861cb6638eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605ec213-ad3f-465b-8ac4-569d795ea1d2",
                    "LayerId": "dbf20389-84e5-45a5-9d7d-f6a4e2aa242f"
                }
            ]
        },
        {
            "id": "2908ed32-daab-493b-8543-4531874a567e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced8410c-726e-4296-b8b1-d6b88bc910ff",
            "compositeImage": {
                "id": "41fb6eeb-7b5c-40f0-9038-2a57d53e943a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2908ed32-daab-493b-8543-4531874a567e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b31934ea-f0aa-44cf-bb5d-85ed0a2eff3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2908ed32-daab-493b-8543-4531874a567e",
                    "LayerId": "dbf20389-84e5-45a5-9d7d-f6a4e2aa242f"
                }
            ]
        },
        {
            "id": "4c20e285-d972-4a60-b070-ced6782a1849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced8410c-726e-4296-b8b1-d6b88bc910ff",
            "compositeImage": {
                "id": "7adf0b90-f794-498b-a56a-a6d7e7523068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c20e285-d972-4a60-b070-ced6782a1849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c2cb53-baec-4687-b990-13d335c61308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c20e285-d972-4a60-b070-ced6782a1849",
                    "LayerId": "dbf20389-84e5-45a5-9d7d-f6a4e2aa242f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dbf20389-84e5-45a5-9d7d-f6a4e2aa242f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ced8410c-726e-4296-b8b1-d6b88bc910ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}