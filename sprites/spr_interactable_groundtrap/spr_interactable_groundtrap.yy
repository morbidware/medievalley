{
    "id": "98274654-1ffd-47da-a654-81df74d42bac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_groundtrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2ad2dea-59bb-4274-ab2e-1825a8fcc315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98274654-1ffd-47da-a654-81df74d42bac",
            "compositeImage": {
                "id": "84652251-6e62-4600-9a32-9f36aa03bbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ad2dea-59bb-4274-ab2e-1825a8fcc315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129408a9-3947-4fb3-ab7e-129c6ec4ceb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ad2dea-59bb-4274-ab2e-1825a8fcc315",
                    "LayerId": "225a5b72-42ff-4c46-b309-784ce60dd018"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 16,
    "layers": [
        {
            "id": "225a5b72-42ff-4c46-b309-784ce60dd018",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98274654-1ffd-47da-a654-81df74d42bac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}