{
    "id": "4f495792-2cc8-4e7e-8b03-db1542f994e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_gethit_up_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14bf8f26-d89b-44c2-80e9-7a56f3c1f12e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f495792-2cc8-4e7e-8b03-db1542f994e8",
            "compositeImage": {
                "id": "cfb92c8a-652a-41ee-999d-2622ba1cab42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14bf8f26-d89b-44c2-80e9-7a56f3c1f12e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078c076e-d214-4fed-9911-d8410e62b8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14bf8f26-d89b-44c2-80e9-7a56f3c1f12e",
                    "LayerId": "7d93b294-99d7-4df5-b6bb-77075fd62ab5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7d93b294-99d7-4df5-b6bb-77075fd62ab5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f495792-2cc8-4e7e-8b03-db1542f994e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}