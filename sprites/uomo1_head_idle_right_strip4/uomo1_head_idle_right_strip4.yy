{
    "id": "21af3f38-e0b1-4e97-be8d-60d56a73cae2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aabb636e-eda4-4927-962a-4ecfc132484a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21af3f38-e0b1-4e97-be8d-60d56a73cae2",
            "compositeImage": {
                "id": "fcfc1fb4-9e85-4958-a9d8-b76b59013e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabb636e-eda4-4927-962a-4ecfc132484a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "187a9073-eec5-48c7-871e-45a8da15225f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabb636e-eda4-4927-962a-4ecfc132484a",
                    "LayerId": "000d0a5b-2574-44d8-97e9-8c184475808f"
                }
            ]
        },
        {
            "id": "94c22c54-6645-4934-9d1e-e404bfd59cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21af3f38-e0b1-4e97-be8d-60d56a73cae2",
            "compositeImage": {
                "id": "83fca409-9124-435e-8a59-31bfc3199d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c22c54-6645-4934-9d1e-e404bfd59cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02c01925-b042-4fe0-8061-287810cdd64c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c22c54-6645-4934-9d1e-e404bfd59cde",
                    "LayerId": "000d0a5b-2574-44d8-97e9-8c184475808f"
                }
            ]
        },
        {
            "id": "ff6390ae-a6e0-4bd3-b20c-6021cd282d33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21af3f38-e0b1-4e97-be8d-60d56a73cae2",
            "compositeImage": {
                "id": "9ae286b6-d32e-4e52-be88-e6205a5531ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6390ae-a6e0-4bd3-b20c-6021cd282d33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33ff43a5-aeb2-41c5-bee2-2d89c72c8e3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6390ae-a6e0-4bd3-b20c-6021cd282d33",
                    "LayerId": "000d0a5b-2574-44d8-97e9-8c184475808f"
                }
            ]
        },
        {
            "id": "f6f40617-e6cd-436d-b6c9-63a8e10f48ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21af3f38-e0b1-4e97-be8d-60d56a73cae2",
            "compositeImage": {
                "id": "59feb31e-da8a-4a69-bab5-0ca9a08ffb42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f40617-e6cd-436d-b6c9-63a8e10f48ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8074679-2fb4-47c8-bd85-af83d65f2e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f40617-e6cd-436d-b6c9-63a8e10f48ca",
                    "LayerId": "000d0a5b-2574-44d8-97e9-8c184475808f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "000d0a5b-2574-44d8-97e9-8c184475808f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21af3f38-e0b1-4e97-be8d-60d56a73cae2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}