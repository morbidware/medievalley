{
    "id": "14e0c60b-d036-42a0-90f1-dc659fd3bcb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_die_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f6892e3-58af-4a05-a230-89ffe2f33ae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "14e0c60b-d036-42a0-90f1-dc659fd3bcb6",
            "compositeImage": {
                "id": "774917d4-0647-4cef-b4b7-448b545a9e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f6892e3-58af-4a05-a230-89ffe2f33ae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54528faf-b5ea-4dc1-aa8a-4ac0624f8d14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f6892e3-58af-4a05-a230-89ffe2f33ae2",
                    "LayerId": "49e073d7-cd5e-419d-92e7-baff896cff98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "49e073d7-cd5e-419d-92e7-baff896cff98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "14e0c60b-d036-42a0-90f1-dc659fd3bcb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 44
}