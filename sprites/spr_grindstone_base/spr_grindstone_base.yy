{
    "id": "749ab329-a86e-4bc0-8fc8-7feaf00c146b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grindstone_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c58e176d-10d8-4f9c-a72e-6e0de3fd89ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749ab329-a86e-4bc0-8fc8-7feaf00c146b",
            "compositeImage": {
                "id": "6b8093af-af1d-4717-8f76-b002a3af49fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c58e176d-10d8-4f9c-a72e-6e0de3fd89ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcd3f515-f481-47fc-b337-5e98948a4c3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c58e176d-10d8-4f9c-a72e-6e0de3fd89ef",
                    "LayerId": "88928209-ce4d-4270-8f49-af1c17ba049b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "88928209-ce4d-4270-8f49-af1c17ba049b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "749ab329-a86e-4bc0-8fc8-7feaf00c146b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}