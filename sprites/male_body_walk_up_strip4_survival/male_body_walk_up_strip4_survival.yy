{
    "id": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_walk_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1323c0e9-45c7-40ce-bea6-5bd2b58fbf2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "compositeImage": {
                "id": "ccb1bbc8-e29a-4101-8fd7-d892494bec1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1323c0e9-45c7-40ce-bea6-5bd2b58fbf2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ba2bf30-7b0b-4826-bf25-c07948c47c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1323c0e9-45c7-40ce-bea6-5bd2b58fbf2b",
                    "LayerId": "060aff53-8147-48d2-98ee-a2013b14b901"
                }
            ]
        },
        {
            "id": "b90a2837-2d38-4dbe-be2d-b8bfd0305cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "compositeImage": {
                "id": "5b60421f-40c2-432f-8136-f9111325a97c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90a2837-2d38-4dbe-be2d-b8bfd0305cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd34e3ca-bcca-4ffe-b38c-925f5f4dbd25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90a2837-2d38-4dbe-be2d-b8bfd0305cc1",
                    "LayerId": "060aff53-8147-48d2-98ee-a2013b14b901"
                }
            ]
        },
        {
            "id": "2b5d4870-bd31-4b19-b58f-0df6c4fb8214",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "compositeImage": {
                "id": "66abd80b-d79f-4d8b-b0f5-f30648481576",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5d4870-bd31-4b19-b58f-0df6c4fb8214",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb038cea-0f48-4bd6-af77-568db86a7b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5d4870-bd31-4b19-b58f-0df6c4fb8214",
                    "LayerId": "060aff53-8147-48d2-98ee-a2013b14b901"
                }
            ]
        },
        {
            "id": "9a86e79f-58ae-4e4a-aeba-ef8470b449b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "compositeImage": {
                "id": "2045ee25-5d96-4dd9-bab1-7d88ebf74536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a86e79f-58ae-4e4a-aeba-ef8470b449b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d40da1-4579-400a-8479-97fb96290739",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a86e79f-58ae-4e4a-aeba-ef8470b449b0",
                    "LayerId": "060aff53-8147-48d2-98ee-a2013b14b901"
                }
            ]
        },
        {
            "id": "ce3fe3ed-a9a3-422e-98c2-ff54f2be41ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "compositeImage": {
                "id": "3b510695-7b68-407c-a783-f9f1e5f5dbe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce3fe3ed-a9a3-422e-98c2-ff54f2be41ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7f17d5-211e-4897-8cea-c6173119fa40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce3fe3ed-a9a3-422e-98c2-ff54f2be41ed",
                    "LayerId": "060aff53-8147-48d2-98ee-a2013b14b901"
                }
            ]
        },
        {
            "id": "02483a12-9842-43f6-b206-b6815006e7bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "compositeImage": {
                "id": "352bb9d1-a759-410f-a799-3d8cb89b669a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02483a12-9842-43f6-b206-b6815006e7bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c266636-8562-42f5-891a-dcc17732df25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02483a12-9842-43f6-b206-b6815006e7bf",
                    "LayerId": "060aff53-8147-48d2-98ee-a2013b14b901"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "060aff53-8147-48d2-98ee-a2013b14b901",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a6fa03d-1426-4f56-b0f5-9d3663e4db49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}