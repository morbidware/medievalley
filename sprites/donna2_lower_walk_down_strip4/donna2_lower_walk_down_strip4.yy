{
    "id": "44ad13de-b07a-4bfe-993b-5909eec5389f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4841cc36-6937-472e-9b60-2cb0a5ade9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ad13de-b07a-4bfe-993b-5909eec5389f",
            "compositeImage": {
                "id": "76740e7a-fca5-4b95-9d60-99cebb33e89c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4841cc36-6937-472e-9b60-2cb0a5ade9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2877ab34-2174-4bd8-aea1-3de4ac729f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4841cc36-6937-472e-9b60-2cb0a5ade9ea",
                    "LayerId": "5ea1ecc8-4289-473c-bb3c-2bd76b3dcae3"
                }
            ]
        },
        {
            "id": "8494c035-5c10-4ee1-91c9-492e3e99a248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ad13de-b07a-4bfe-993b-5909eec5389f",
            "compositeImage": {
                "id": "98d312c1-276b-42c4-9587-c87b083145e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8494c035-5c10-4ee1-91c9-492e3e99a248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef44043d-cc6b-4826-8c95-d487ba80d80d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8494c035-5c10-4ee1-91c9-492e3e99a248",
                    "LayerId": "5ea1ecc8-4289-473c-bb3c-2bd76b3dcae3"
                }
            ]
        },
        {
            "id": "f19f4e26-5ccc-4011-93de-fc240bea44b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ad13de-b07a-4bfe-993b-5909eec5389f",
            "compositeImage": {
                "id": "d1831ed0-e97b-49ce-b888-249b55f085c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f19f4e26-5ccc-4011-93de-fc240bea44b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d90a546d-ea37-40a1-82ef-e518ab5a3235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f19f4e26-5ccc-4011-93de-fc240bea44b9",
                    "LayerId": "5ea1ecc8-4289-473c-bb3c-2bd76b3dcae3"
                }
            ]
        },
        {
            "id": "4fe82ff1-c924-4bd4-8537-708cb537d5db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44ad13de-b07a-4bfe-993b-5909eec5389f",
            "compositeImage": {
                "id": "2f836685-5bbd-4066-98f8-a5201d7f344e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fe82ff1-c924-4bd4-8537-708cb537d5db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7a26f2b-04fc-4c86-aa15-63ac7c422d51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fe82ff1-c924-4bd4-8537-708cb537d5db",
                    "LayerId": "5ea1ecc8-4289-473c-bb3c-2bd76b3dcae3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ea1ecc8-4289-473c-bb3c-2bd76b3dcae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44ad13de-b07a-4bfe-993b-5909eec5389f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}