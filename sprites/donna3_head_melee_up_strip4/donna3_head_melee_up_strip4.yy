{
    "id": "97bf560f-a7c6-4ab1-9f6b-eb6e21608b3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e6279c4-3e08-4729-a61e-92cd7e35a368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97bf560f-a7c6-4ab1-9f6b-eb6e21608b3a",
            "compositeImage": {
                "id": "4b0a2834-1695-4d85-9ecb-854f6e0953cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e6279c4-3e08-4729-a61e-92cd7e35a368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5112bd7-9975-4b55-82b6-a850ee82f985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e6279c4-3e08-4729-a61e-92cd7e35a368",
                    "LayerId": "9d85c33e-92d8-4024-84dd-d2e6b1b3ec36"
                }
            ]
        },
        {
            "id": "040f9fd5-ebc8-42fe-a104-bd7dbf34a4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97bf560f-a7c6-4ab1-9f6b-eb6e21608b3a",
            "compositeImage": {
                "id": "b0af7595-0ecb-4c89-9b21-36bb04e333fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "040f9fd5-ebc8-42fe-a104-bd7dbf34a4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2b46d0-6c2b-4966-bcc0-abef7846d63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "040f9fd5-ebc8-42fe-a104-bd7dbf34a4bc",
                    "LayerId": "9d85c33e-92d8-4024-84dd-d2e6b1b3ec36"
                }
            ]
        },
        {
            "id": "d3901b7c-81e5-44c5-87d1-209fbed7ca75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97bf560f-a7c6-4ab1-9f6b-eb6e21608b3a",
            "compositeImage": {
                "id": "3f6882ee-8710-4053-b035-7dc7c20dc484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3901b7c-81e5-44c5-87d1-209fbed7ca75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132fbe53-af5c-461a-829c-a0f50990354f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3901b7c-81e5-44c5-87d1-209fbed7ca75",
                    "LayerId": "9d85c33e-92d8-4024-84dd-d2e6b1b3ec36"
                }
            ]
        },
        {
            "id": "fbb5ffe8-5345-4748-8075-5aa517b7004f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97bf560f-a7c6-4ab1-9f6b-eb6e21608b3a",
            "compositeImage": {
                "id": "182d298c-0090-4190-8ac8-f31a2d2a1cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb5ffe8-5345-4748-8075-5aa517b7004f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e4d6d9-bbf4-4ecc-b149-e6519289dfe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb5ffe8-5345-4748-8075-5aa517b7004f",
                    "LayerId": "9d85c33e-92d8-4024-84dd-d2e6b1b3ec36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d85c33e-92d8-4024-84dd-d2e6b1b3ec36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97bf560f-a7c6-4ab1-9f6b-eb6e21608b3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}