{
    "id": "6557a29d-db2a-4c2e-9cbb-3783df6ad1b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b6e8d85-bfc7-4ffd-86eb-b7ba247f58bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6557a29d-db2a-4c2e-9cbb-3783df6ad1b8",
            "compositeImage": {
                "id": "e47427fe-2001-41b8-9ad5-1a51481fda40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6e8d85-bfc7-4ffd-86eb-b7ba247f58bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd296f4a-1325-4850-96a3-40fc0e50b1b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6e8d85-bfc7-4ffd-86eb-b7ba247f58bc",
                    "LayerId": "116482fd-e2e6-4c5d-aab7-a2f3cf9c0991"
                }
            ]
        },
        {
            "id": "e1c3f1a7-28b4-4fd8-9ee3-7e41b5ac3949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6557a29d-db2a-4c2e-9cbb-3783df6ad1b8",
            "compositeImage": {
                "id": "f99f3fd1-d868-4862-8f09-e0248a7fc882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c3f1a7-28b4-4fd8-9ee3-7e41b5ac3949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3802f17-a5b1-4a34-a5b2-9f63f19f0927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c3f1a7-28b4-4fd8-9ee3-7e41b5ac3949",
                    "LayerId": "116482fd-e2e6-4c5d-aab7-a2f3cf9c0991"
                }
            ]
        },
        {
            "id": "7332518a-0c91-4512-b72b-da8603b8eeb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6557a29d-db2a-4c2e-9cbb-3783df6ad1b8",
            "compositeImage": {
                "id": "008c4832-01d8-44ce-9e5b-afb7713a66ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7332518a-0c91-4512-b72b-da8603b8eeb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0ec95bc-4105-46bd-8cd9-45ac1f50a5db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7332518a-0c91-4512-b72b-da8603b8eeb3",
                    "LayerId": "116482fd-e2e6-4c5d-aab7-a2f3cf9c0991"
                }
            ]
        },
        {
            "id": "dfcf2fd4-f209-4914-9681-3b5045b71c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6557a29d-db2a-4c2e-9cbb-3783df6ad1b8",
            "compositeImage": {
                "id": "c576e345-3f01-45b7-96fb-82b051510038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfcf2fd4-f209-4914-9681-3b5045b71c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54772bc5-9b84-4485-8e84-32a3b4907d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfcf2fd4-f209-4914-9681-3b5045b71c44",
                    "LayerId": "116482fd-e2e6-4c5d-aab7-a2f3cf9c0991"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "116482fd-e2e6-4c5d-aab7-a2f3cf9c0991",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6557a29d-db2a-4c2e-9cbb-3783df6ad1b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}