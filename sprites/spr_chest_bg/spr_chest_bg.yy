{
    "id": "a6366e53-8240-4e4c-942d-c3d726fc0e84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chest_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "025e8fb3-8a68-43b1-93d1-318ec8485008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6366e53-8240-4e4c-942d-c3d726fc0e84",
            "compositeImage": {
                "id": "c2fc46c8-4e6b-43b3-ac2c-5f615636b9c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025e8fb3-8a68-43b1-93d1-318ec8485008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe41a7d-c415-4857-8202-7f34f3c3a3d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025e8fb3-8a68-43b1-93d1-318ec8485008",
                    "LayerId": "9318a7af-51dc-4834-bd06-c0d54b3383d0"
                }
            ]
        }
    ],
    "gridX": 29,
    "gridY": 29,
    "height": 60,
    "layers": [
        {
            "id": "9318a7af-51dc-4834-bd06-c0d54b3383d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6366e53-8240-4e4c-942d-c3d726fc0e84",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}