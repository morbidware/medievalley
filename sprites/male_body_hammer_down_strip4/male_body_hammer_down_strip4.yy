{
    "id": "bfd8e3c0-1792-43e8-9d5f-9a5e9d559840",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8e13eb4-46fa-4694-8ca7-5c1fedbf023a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd8e3c0-1792-43e8-9d5f-9a5e9d559840",
            "compositeImage": {
                "id": "fb114a6d-891d-4861-8109-9348885e3b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e13eb4-46fa-4694-8ca7-5c1fedbf023a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a20ac86f-e958-4995-ad3b-00bbf13309bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e13eb4-46fa-4694-8ca7-5c1fedbf023a",
                    "LayerId": "df414fbf-7bdb-41cd-ac40-5a8c70d13178"
                }
            ]
        },
        {
            "id": "84d3ebdb-3d5f-4c49-a6c7-1bf44186ba16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd8e3c0-1792-43e8-9d5f-9a5e9d559840",
            "compositeImage": {
                "id": "6e7645b5-7c98-4e1f-88ef-926667bfb52d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d3ebdb-3d5f-4c49-a6c7-1bf44186ba16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45858214-b3ce-4a0f-8d88-feccc149febf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d3ebdb-3d5f-4c49-a6c7-1bf44186ba16",
                    "LayerId": "df414fbf-7bdb-41cd-ac40-5a8c70d13178"
                }
            ]
        },
        {
            "id": "760c1e9b-ff9e-48ab-9ede-c5e8d0d65772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd8e3c0-1792-43e8-9d5f-9a5e9d559840",
            "compositeImage": {
                "id": "e77b0385-51c2-4bc0-afc2-6225ec170574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760c1e9b-ff9e-48ab-9ede-c5e8d0d65772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ec36319-d020-45f7-9f76-370ce243bfa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760c1e9b-ff9e-48ab-9ede-c5e8d0d65772",
                    "LayerId": "df414fbf-7bdb-41cd-ac40-5a8c70d13178"
                }
            ]
        },
        {
            "id": "ee7c8514-9d44-41c7-8f20-2868ef43e3b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd8e3c0-1792-43e8-9d5f-9a5e9d559840",
            "compositeImage": {
                "id": "231f040a-984c-40f4-8043-bcaee5b9d1ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee7c8514-9d44-41c7-8f20-2868ef43e3b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "477d88a2-0445-4380-a9a4-dce30df297d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee7c8514-9d44-41c7-8f20-2868ef43e3b3",
                    "LayerId": "df414fbf-7bdb-41cd-ac40-5a8c70d13178"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "df414fbf-7bdb-41cd-ac40-5a8c70d13178",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfd8e3c0-1792-43e8-9d5f-9a5e9d559840",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}