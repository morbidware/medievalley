{
    "id": "b0769366-7feb-4991-8556-06aab9c5ad24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_cookedmeat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7d66bbe-0705-4214-b21e-ee8af5a9a04e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0769366-7feb-4991-8556-06aab9c5ad24",
            "compositeImage": {
                "id": "20862a51-f168-472b-9bfa-f287c591cb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d66bbe-0705-4214-b21e-ee8af5a9a04e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bcfceac-7c45-459f-bb3d-833ec4d3c403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d66bbe-0705-4214-b21e-ee8af5a9a04e",
                    "LayerId": "093a6c18-73fb-4db0-be65-153268e2fc0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "093a6c18-73fb-4db0-be65-153268e2fc0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0769366-7feb-4991-8556-06aab9c5ad24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}