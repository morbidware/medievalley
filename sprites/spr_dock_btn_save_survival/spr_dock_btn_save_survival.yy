{
    "id": "7d3b9703-c42a-4dce-909b-23394a1d0810",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_save_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01a73ef0-5376-4a8f-a98a-735a54932212",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3b9703-c42a-4dce-909b-23394a1d0810",
            "compositeImage": {
                "id": "b9acf947-de91-48f4-9c96-f102cb7e5b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01a73ef0-5376-4a8f-a98a-735a54932212",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef070298-18e1-40ea-8a0a-b6a7d17ddfb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01a73ef0-5376-4a8f-a98a-735a54932212",
                    "LayerId": "000fab7c-75e9-43bd-abac-646a9d3616f9"
                }
            ]
        },
        {
            "id": "748a76fc-8cff-4911-96ce-b6c7d44d1def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3b9703-c42a-4dce-909b-23394a1d0810",
            "compositeImage": {
                "id": "d8d2af05-42cf-4515-ae0c-b38046207567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748a76fc-8cff-4911-96ce-b6c7d44d1def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1b308a5-e00e-45b7-a07f-e52d1d58cca2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748a76fc-8cff-4911-96ce-b6c7d44d1def",
                    "LayerId": "000fab7c-75e9-43bd-abac-646a9d3616f9"
                }
            ]
        },
        {
            "id": "1d934eb8-a5f4-46f5-b209-b0710ede9f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d3b9703-c42a-4dce-909b-23394a1d0810",
            "compositeImage": {
                "id": "de015ad9-4d21-4388-bc48-5cc7d060958a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d934eb8-a5f4-46f5-b209-b0710ede9f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1857d236-9efc-40b8-800c-3b079df04899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d934eb8-a5f4-46f5-b209-b0710ede9f4c",
                    "LayerId": "000fab7c-75e9-43bd-abac-646a9d3616f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "000fab7c-75e9-43bd-abac-646a9d3616f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d3b9703-c42a-4dce-909b-23394a1d0810",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}