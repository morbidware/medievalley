{
    "id": "49d51a2c-5656-420b-be76-f5b071cc030c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_grindstone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00c20f33-2621-4f53-bd58-b844c6b3de1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49d51a2c-5656-420b-be76-f5b071cc030c",
            "compositeImage": {
                "id": "fc24125e-9d81-4857-bfa8-3ed2b6e7a211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c20f33-2621-4f53-bd58-b844c6b3de1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d616855-6fff-4795-81f4-60f73b54ce72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c20f33-2621-4f53-bd58-b844c6b3de1c",
                    "LayerId": "14a5d961-c69f-4f20-9ff5-a3527286371a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "14a5d961-c69f-4f20-9ff5-a3527286371a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49d51a2c-5656-420b-be76-f5b071cc030c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}