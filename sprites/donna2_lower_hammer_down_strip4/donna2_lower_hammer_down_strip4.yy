{
    "id": "a2aa0b96-54d6-4050-a773-723c2a215981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17e1aad9-20d3-408a-ac3e-568d3aa800b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2aa0b96-54d6-4050-a773-723c2a215981",
            "compositeImage": {
                "id": "321393e6-1457-4f18-bf41-41b2e747b5e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e1aad9-20d3-408a-ac3e-568d3aa800b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69734a65-9158-4a1a-9778-fdb055c45e0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e1aad9-20d3-408a-ac3e-568d3aa800b2",
                    "LayerId": "a2cbaa1a-115b-4aed-9fa6-6db9cd7f7101"
                }
            ]
        },
        {
            "id": "56022fbd-a5c0-4787-a72d-1739f2953bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2aa0b96-54d6-4050-a773-723c2a215981",
            "compositeImage": {
                "id": "72c0d205-be36-4f49-97cd-2619d5d038aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56022fbd-a5c0-4787-a72d-1739f2953bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "309a2858-5a54-48ed-b28f-4e07fa20def3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56022fbd-a5c0-4787-a72d-1739f2953bc4",
                    "LayerId": "a2cbaa1a-115b-4aed-9fa6-6db9cd7f7101"
                }
            ]
        },
        {
            "id": "e9ae5df1-318f-4d1e-8854-be7e2a11ddd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2aa0b96-54d6-4050-a773-723c2a215981",
            "compositeImage": {
                "id": "d759cef8-d494-4c3c-9c6b-b89c23389f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ae5df1-318f-4d1e-8854-be7e2a11ddd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e07c18f8-5ca2-4082-b503-07b3ecf1f838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ae5df1-318f-4d1e-8854-be7e2a11ddd8",
                    "LayerId": "a2cbaa1a-115b-4aed-9fa6-6db9cd7f7101"
                }
            ]
        },
        {
            "id": "84d0d06b-4de2-4469-984e-8af395ff131e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2aa0b96-54d6-4050-a773-723c2a215981",
            "compositeImage": {
                "id": "93d673c1-83ab-4110-9f67-0f82e7006e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d0d06b-4de2-4469-984e-8af395ff131e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc1a738-2bf3-4f0f-8921-d265803826ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d0d06b-4de2-4469-984e-8af395ff131e",
                    "LayerId": "a2cbaa1a-115b-4aed-9fa6-6db9cd7f7101"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2cbaa1a-115b-4aed-9fa6-6db9cd7f7101",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2aa0b96-54d6-4050-a773-723c2a215981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}