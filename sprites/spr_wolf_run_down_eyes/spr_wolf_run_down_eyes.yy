{
    "id": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_run_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 21,
    "bbox_right": 26,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c69bfd7-90c7-4ae4-9e2b-e88b06e7daa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "compositeImage": {
                "id": "9536afb6-40ac-48fb-a122-5b296f71c0c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c69bfd7-90c7-4ae4-9e2b-e88b06e7daa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db31c2d-87de-40d5-9d9e-f539709ab4f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c69bfd7-90c7-4ae4-9e2b-e88b06e7daa5",
                    "LayerId": "9f40093d-7640-4182-8c34-c1b656f1635f"
                }
            ]
        },
        {
            "id": "5ddab938-2fb8-4b18-ab4c-d2d4a5e259df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "compositeImage": {
                "id": "a3c317cb-3c23-4b16-90b2-9936074ed5d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ddab938-2fb8-4b18-ab4c-d2d4a5e259df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3921a6fd-a4fd-4462-8fe4-3de067bfeb83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ddab938-2fb8-4b18-ab4c-d2d4a5e259df",
                    "LayerId": "9f40093d-7640-4182-8c34-c1b656f1635f"
                }
            ]
        },
        {
            "id": "53d2bb7e-0793-4b27-94be-ae3ca3ffb07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "compositeImage": {
                "id": "23163f30-1291-4a1f-8711-ffb233e3a22b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53d2bb7e-0793-4b27-94be-ae3ca3ffb07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a149826c-0cf9-4e35-824e-112f8f4be4e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53d2bb7e-0793-4b27-94be-ae3ca3ffb07b",
                    "LayerId": "9f40093d-7640-4182-8c34-c1b656f1635f"
                }
            ]
        },
        {
            "id": "b0c03c61-0619-497b-9e34-da38fd6765c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "compositeImage": {
                "id": "c9c58c15-1ef3-447d-a138-e7342e0256a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0c03c61-0619-497b-9e34-da38fd6765c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6a8949f-29e5-4297-a7e4-1e81a3b96d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0c03c61-0619-497b-9e34-da38fd6765c5",
                    "LayerId": "9f40093d-7640-4182-8c34-c1b656f1635f"
                }
            ]
        },
        {
            "id": "64ddf0bc-b3fd-439b-8e8c-6e7c256070ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "compositeImage": {
                "id": "4da237c7-4bab-43ad-a7c3-99440e0c4ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ddf0bc-b3fd-439b-8e8c-6e7c256070ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04af73a5-fd39-49f5-90b6-518595ae34cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ddf0bc-b3fd-439b-8e8c-6e7c256070ad",
                    "LayerId": "9f40093d-7640-4182-8c34-c1b656f1635f"
                }
            ]
        },
        {
            "id": "c91d39ca-0bff-4bff-84bb-774b3a32032c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "compositeImage": {
                "id": "e56192a8-67a5-455c-820d-48f894d2b2a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c91d39ca-0bff-4bff-84bb-774b3a32032c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff61314-714f-4e1f-aa94-55d7743530b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c91d39ca-0bff-4bff-84bb-774b3a32032c",
                    "LayerId": "9f40093d-7640-4182-8c34-c1b656f1635f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "9f40093d-7640-4182-8c34-c1b656f1635f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83c92a07-9a60-496d-834d-a238fa2bbdb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}