{
    "id": "1e0ab0e3-0d3a-4a75-ba81-74c14e6396a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stairs_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "241a0dcb-383a-4861-ae28-233a26570d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e0ab0e3-0d3a-4a75-ba81-74c14e6396a5",
            "compositeImage": {
                "id": "6f262b83-694b-4437-847c-1882aee06c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241a0dcb-383a-4861-ae28-233a26570d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f1daf33-9fb3-4f02-8900-5f99f61e31b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241a0dcb-383a-4861-ae28-233a26570d84",
                    "LayerId": "a6e39f76-d1f9-4df5-abe9-85cfa1f92652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a6e39f76-d1f9-4df5-abe9-85cfa1f92652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e0ab0e3-0d3a-4a75-ba81-74c14e6396a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 8,
    "yorig": 8
}