{
    "id": "a8e78dd7-831b-4ab5-9fda-55f8e629fec9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mailbox_read_unread",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93a2320f-8e38-40ac-8314-ace7e17e398b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8e78dd7-831b-4ab5-9fda-55f8e629fec9",
            "compositeImage": {
                "id": "4a97bb65-5c2a-4257-b763-414e770dd20c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a2320f-8e38-40ac-8314-ace7e17e398b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154e2f4b-ccf1-4919-b844-446294d1a88a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a2320f-8e38-40ac-8314-ace7e17e398b",
                    "LayerId": "7b61832d-cff9-4df0-9a7e-5261011bf1f7"
                }
            ]
        },
        {
            "id": "b0558aa9-0e84-410c-8168-7a63e137ddef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8e78dd7-831b-4ab5-9fda-55f8e629fec9",
            "compositeImage": {
                "id": "46f20769-1c6d-4e88-a6c5-0ff6929dc6a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0558aa9-0e84-410c-8168-7a63e137ddef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9f6f6e-26f6-4b11-9d42-c566bc97f96c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0558aa9-0e84-410c-8168-7a63e137ddef",
                    "LayerId": "7b61832d-cff9-4df0-9a7e-5261011bf1f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b61832d-cff9-4df0-9a7e-5261011bf1f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8e78dd7-831b-4ab5-9fda-55f8e629fec9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}