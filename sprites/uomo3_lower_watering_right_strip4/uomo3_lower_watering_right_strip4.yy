{
    "id": "8b4f00ec-ac91-4174-8e1d-b42383448598",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "123f9894-f2bf-4e40-85db-06b1a5447a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b4f00ec-ac91-4174-8e1d-b42383448598",
            "compositeImage": {
                "id": "16390c8c-5594-4f75-8a61-61d3529337c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123f9894-f2bf-4e40-85db-06b1a5447a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb896ff-b2ee-4d60-9bf3-48104f7db65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123f9894-f2bf-4e40-85db-06b1a5447a87",
                    "LayerId": "bb537c23-b346-4a9f-8de2-8bec416e596e"
                }
            ]
        },
        {
            "id": "1e4f4046-1c36-4253-930b-99c99e9eee8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b4f00ec-ac91-4174-8e1d-b42383448598",
            "compositeImage": {
                "id": "eb86507f-2a52-42da-883b-4f675ebd5d11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e4f4046-1c36-4253-930b-99c99e9eee8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4a4eae-654f-4f6a-8bbf-0b5c46c39cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4f4046-1c36-4253-930b-99c99e9eee8b",
                    "LayerId": "bb537c23-b346-4a9f-8de2-8bec416e596e"
                }
            ]
        },
        {
            "id": "f954a2dc-1f3e-4888-a172-f721c15cdc5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b4f00ec-ac91-4174-8e1d-b42383448598",
            "compositeImage": {
                "id": "1800759e-8da9-42e6-9bbe-0b1f328db81d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f954a2dc-1f3e-4888-a172-f721c15cdc5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3573259b-ad07-47d3-bfa6-a48e77c18124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f954a2dc-1f3e-4888-a172-f721c15cdc5a",
                    "LayerId": "bb537c23-b346-4a9f-8de2-8bec416e596e"
                }
            ]
        },
        {
            "id": "2dbe5907-fa59-403b-a7cc-da743c9a17b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b4f00ec-ac91-4174-8e1d-b42383448598",
            "compositeImage": {
                "id": "f5567718-79f3-4832-8790-9227db838497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dbe5907-fa59-403b-a7cc-da743c9a17b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7b6f4c4-598b-4f8a-b2e8-a82ce4de2628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dbe5907-fa59-403b-a7cc-da743c9a17b5",
                    "LayerId": "bb537c23-b346-4a9f-8de2-8bec416e596e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bb537c23-b346-4a9f-8de2-8bec416e596e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b4f00ec-ac91-4174-8e1d-b42383448598",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}