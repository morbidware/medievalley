{
    "id": "22d81275-76a2-4883-a1af-c1ee7801a0da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_pine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 6,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 185,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58338d58-f3e2-466e-98a1-856d8b72a762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22d81275-76a2-4883-a1af-c1ee7801a0da",
            "compositeImage": {
                "id": "c091681a-e064-42fd-887b-477fcbfb9d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58338d58-f3e2-466e-98a1-856d8b72a762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee4adc0-7628-4139-a8ba-e7c753123cc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58338d58-f3e2-466e-98a1-856d8b72a762",
                    "LayerId": "6cc587da-ff1a-4c5f-a599-0deb91a16a25"
                }
            ]
        },
        {
            "id": "12282be9-80d7-483a-9ff8-c2faa313f4bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22d81275-76a2-4883-a1af-c1ee7801a0da",
            "compositeImage": {
                "id": "caee682d-b9ee-4bca-a605-d524cb027a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12282be9-80d7-483a-9ff8-c2faa313f4bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fac6fd47-c8ac-457f-9ee8-a0d40a3ddb2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12282be9-80d7-483a-9ff8-c2faa313f4bf",
                    "LayerId": "6cc587da-ff1a-4c5f-a599-0deb91a16a25"
                }
            ]
        },
        {
            "id": "815d9dd1-52d0-48bc-8b91-73a0618d5db0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22d81275-76a2-4883-a1af-c1ee7801a0da",
            "compositeImage": {
                "id": "5e5ecd77-d10c-44ef-8616-fdddf47d84ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815d9dd1-52d0-48bc-8b91-73a0618d5db0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7a429a0-44c7-4845-b227-478755cefd5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815d9dd1-52d0-48bc-8b91-73a0618d5db0",
                    "LayerId": "6cc587da-ff1a-4c5f-a599-0deb91a16a25"
                }
            ]
        },
        {
            "id": "9ec20022-dd9b-460d-9aa3-0fe5629d8a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22d81275-76a2-4883-a1af-c1ee7801a0da",
            "compositeImage": {
                "id": "378b2cf3-8367-42b9-82ae-d4f81a35c70b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec20022-dd9b-460d-9aa3-0fe5629d8a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f86e077-c754-48c8-beb3-93f766bc6f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec20022-dd9b-460d-9aa3-0fe5629d8a0c",
                    "LayerId": "6cc587da-ff1a-4c5f-a599-0deb91a16a25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "6cc587da-ff1a-4c5f-a599-0deb91a16a25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22d81275-76a2-4883-a1af-c1ee7801a0da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 29,
    "yorig": 73
}