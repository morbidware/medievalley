{
    "id": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_watering_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6af929fd-28cb-41a0-8ad0-87fd3cf00db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "compositeImage": {
                "id": "36461e3b-3a7e-4dc8-a79b-14dcea89e2e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6af929fd-28cb-41a0-8ad0-87fd3cf00db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad03c4a-aaca-4ade-bb37-dba1d3d74696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6af929fd-28cb-41a0-8ad0-87fd3cf00db7",
                    "LayerId": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1"
                }
            ]
        },
        {
            "id": "29c997ed-7db6-48f7-9901-f3bd5356f9d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "compositeImage": {
                "id": "41b8b903-fae8-4f0f-bf0a-911558848466",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c997ed-7db6-48f7-9901-f3bd5356f9d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29da6c07-15b6-488f-ba1d-ae4fc3a62d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c997ed-7db6-48f7-9901-f3bd5356f9d6",
                    "LayerId": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1"
                }
            ]
        },
        {
            "id": "650df011-ee64-457e-b93a-bc6a1bbc2172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "compositeImage": {
                "id": "767c6bc2-5708-491e-8434-29e8dfe6361d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "650df011-ee64-457e-b93a-bc6a1bbc2172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99bcecdd-6563-4ea6-a07f-df1055b268c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "650df011-ee64-457e-b93a-bc6a1bbc2172",
                    "LayerId": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1"
                }
            ]
        },
        {
            "id": "8a380292-b236-4748-a5be-903a68f66bfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "compositeImage": {
                "id": "57d24083-5d98-45f4-ab97-689dcbc7dd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a380292-b236-4748-a5be-903a68f66bfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c432a418-ae6e-4166-8ec8-2caf2b6c55f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a380292-b236-4748-a5be-903a68f66bfc",
                    "LayerId": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1"
                }
            ]
        },
        {
            "id": "34361b0a-09ae-4803-abe7-35d06fce5e03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "compositeImage": {
                "id": "1de45799-c457-4acd-8b16-29f1e7a01971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34361b0a-09ae-4803-abe7-35d06fce5e03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e3410d-2745-450d-b2cc-156bce822d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34361b0a-09ae-4803-abe7-35d06fce5e03",
                    "LayerId": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1"
                }
            ]
        },
        {
            "id": "21da1582-ed72-4633-a17d-e2a39626888a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "compositeImage": {
                "id": "90af1ae8-a809-45b3-9bd6-79b1a036b35d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21da1582-ed72-4633-a17d-e2a39626888a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0dd17e-348b-46e1-98ea-3316804edbb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21da1582-ed72-4633-a17d-e2a39626888a",
                    "LayerId": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "43f9dad5-5a0f-4b7c-9917-9d115588a4a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c44ce4f3-26af-43df-bf88-1aa371e7d4dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}