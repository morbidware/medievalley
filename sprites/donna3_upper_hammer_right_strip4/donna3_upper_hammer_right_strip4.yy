{
    "id": "07a73c2e-4574-43bb-b9e0-fa726fcc8aa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b29ef66-2271-4301-b0e7-61b382d3b935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a73c2e-4574-43bb-b9e0-fa726fcc8aa1",
            "compositeImage": {
                "id": "048d4532-ca39-4d79-9764-26381a4c8f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b29ef66-2271-4301-b0e7-61b382d3b935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ceffee1-06ec-4585-83b7-d4a10a5f7106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b29ef66-2271-4301-b0e7-61b382d3b935",
                    "LayerId": "50766844-fdd9-496b-b16d-c41362c38110"
                }
            ]
        },
        {
            "id": "e86c25ea-f714-4f60-baba-4aa72ca4df96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a73c2e-4574-43bb-b9e0-fa726fcc8aa1",
            "compositeImage": {
                "id": "aceb2a1d-ffb3-4f80-afdf-3595c9265f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86c25ea-f714-4f60-baba-4aa72ca4df96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc4d299-3a92-4850-b3bf-ae5453d38c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86c25ea-f714-4f60-baba-4aa72ca4df96",
                    "LayerId": "50766844-fdd9-496b-b16d-c41362c38110"
                }
            ]
        },
        {
            "id": "d0fa11da-125e-42ba-8fc8-ee3f412c42b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a73c2e-4574-43bb-b9e0-fa726fcc8aa1",
            "compositeImage": {
                "id": "860fbe4b-5b9e-4f72-be36-668797a25879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0fa11da-125e-42ba-8fc8-ee3f412c42b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad4ec8a-c4ea-4f5f-8319-e077fdc01b20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0fa11da-125e-42ba-8fc8-ee3f412c42b7",
                    "LayerId": "50766844-fdd9-496b-b16d-c41362c38110"
                }
            ]
        },
        {
            "id": "e61ccd1b-7210-4779-ba20-4f62bce228b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a73c2e-4574-43bb-b9e0-fa726fcc8aa1",
            "compositeImage": {
                "id": "1ac8f036-8e54-467e-b1c7-c162617f345c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e61ccd1b-7210-4779-ba20-4f62bce228b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c38efcb-e6a2-4daa-a543-9dc3e6ffd6de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e61ccd1b-7210-4779-ba20-4f62bce228b9",
                    "LayerId": "50766844-fdd9-496b-b16d-c41362c38110"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "50766844-fdd9-496b-b16d-c41362c38110",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a73c2e-4574-43bb-b9e0-fa726fcc8aa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}