{
    "id": "65903efa-44f9-43c4-93a3-85399dcedb78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_sleeping_z",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be1bdced-1143-46a3-aea6-8b04bc982a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65903efa-44f9-43c4-93a3-85399dcedb78",
            "compositeImage": {
                "id": "2fede47a-8fbe-46e1-bd2b-c893e77e0da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1bdced-1143-46a3-aea6-8b04bc982a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baa99106-4bb5-4c0e-aeda-66bc1d78ea71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1bdced-1143-46a3-aea6-8b04bc982a56",
                    "LayerId": "448367e1-4694-4c80-a237-c4962e526a3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "448367e1-4694-4c80-a237-c4962e526a3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65903efa-44f9-43c4-93a3-85399dcedb78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}