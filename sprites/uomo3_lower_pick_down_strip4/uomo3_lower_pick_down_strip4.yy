{
    "id": "6094672e-722a-402f-b418-c9e6da781150",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34643c34-b94d-4cde-9dc0-285b48af0e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6094672e-722a-402f-b418-c9e6da781150",
            "compositeImage": {
                "id": "ca0319ae-9e7b-40c1-92fd-ded1ce317b1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34643c34-b94d-4cde-9dc0-285b48af0e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3088ba-7323-480a-9c90-304f73041fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34643c34-b94d-4cde-9dc0-285b48af0e66",
                    "LayerId": "af3366ed-83d2-4cc8-a105-e9f4a7741331"
                }
            ]
        },
        {
            "id": "09f0992f-3167-40ac-ae5e-137759320a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6094672e-722a-402f-b418-c9e6da781150",
            "compositeImage": {
                "id": "c0e9d9f8-c76f-44f9-a400-eb7807e5abca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09f0992f-3167-40ac-ae5e-137759320a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "227612fb-5b43-49bb-b1d0-dbe2d4e744c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09f0992f-3167-40ac-ae5e-137759320a0a",
                    "LayerId": "af3366ed-83d2-4cc8-a105-e9f4a7741331"
                }
            ]
        },
        {
            "id": "ebea8bef-229f-4b88-a3d0-cec1f1e71931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6094672e-722a-402f-b418-c9e6da781150",
            "compositeImage": {
                "id": "43503c13-b8ee-4769-b1bd-7694f6fbf14c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebea8bef-229f-4b88-a3d0-cec1f1e71931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfa8d2f1-84da-43b3-ac11-ef6d546efee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebea8bef-229f-4b88-a3d0-cec1f1e71931",
                    "LayerId": "af3366ed-83d2-4cc8-a105-e9f4a7741331"
                }
            ]
        },
        {
            "id": "6ed143b7-9c34-4ebc-9f4f-f0e092b31f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6094672e-722a-402f-b418-c9e6da781150",
            "compositeImage": {
                "id": "756c91bd-06bb-4540-93a7-029bb400fc65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed143b7-9c34-4ebc-9f4f-f0e092b31f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43917d41-b31c-4c0b-9411-cbbac7f997e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed143b7-9c34-4ebc-9f4f-f0e092b31f9b",
                    "LayerId": "af3366ed-83d2-4cc8-a105-e9f4a7741331"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af3366ed-83d2-4cc8-a105-e9f4a7741331",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6094672e-722a-402f-b418-c9e6da781150",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}