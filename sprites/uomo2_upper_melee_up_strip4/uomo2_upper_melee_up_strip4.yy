{
    "id": "b10e51f2-7cb1-4b84-ae6d-7a1bc51d3ef6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc6e6b57-6156-40a7-982e-7ecceb339350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b10e51f2-7cb1-4b84-ae6d-7a1bc51d3ef6",
            "compositeImage": {
                "id": "c0c565f6-cbe0-4f20-b5a5-5098ab81d607",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc6e6b57-6156-40a7-982e-7ecceb339350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38704d67-e67a-42be-a82c-7a31de74cb43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc6e6b57-6156-40a7-982e-7ecceb339350",
                    "LayerId": "9fb7fe00-b308-435c-8472-f2fc5b8c2471"
                }
            ]
        },
        {
            "id": "f53b9e9b-cbf8-4b2f-b13b-f6c7370cdc29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b10e51f2-7cb1-4b84-ae6d-7a1bc51d3ef6",
            "compositeImage": {
                "id": "4ad7171b-63ee-4d7a-9984-2b458767be92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53b9e9b-cbf8-4b2f-b13b-f6c7370cdc29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "602dbef6-3f02-44f0-bf48-036912acf4b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53b9e9b-cbf8-4b2f-b13b-f6c7370cdc29",
                    "LayerId": "9fb7fe00-b308-435c-8472-f2fc5b8c2471"
                }
            ]
        },
        {
            "id": "417238f8-f56b-4b3f-baa9-74b5a725af82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b10e51f2-7cb1-4b84-ae6d-7a1bc51d3ef6",
            "compositeImage": {
                "id": "234a9182-8ce4-4f87-b65b-e9f2c0bb1cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417238f8-f56b-4b3f-baa9-74b5a725af82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487efe8d-7ac5-4fe4-8868-5d39a238fd06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417238f8-f56b-4b3f-baa9-74b5a725af82",
                    "LayerId": "9fb7fe00-b308-435c-8472-f2fc5b8c2471"
                }
            ]
        },
        {
            "id": "d7a9e91f-fdb6-469a-a788-7f941b19e586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b10e51f2-7cb1-4b84-ae6d-7a1bc51d3ef6",
            "compositeImage": {
                "id": "60afc6e0-d0fe-4cba-bc23-ce9d1be34200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a9e91f-fdb6-469a-a788-7f941b19e586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2db890d-0018-43da-860a-d6f05001ca4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a9e91f-fdb6-469a-a788-7f941b19e586",
                    "LayerId": "9fb7fe00-b308-435c-8472-f2fc5b8c2471"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fb7fe00-b308-435c-8472-f2fc5b8c2471",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b10e51f2-7cb1-4b84-ae6d-7a1bc51d3ef6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}