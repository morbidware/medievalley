{
    "id": "41c22f9d-cfec-4e93-9435-aa381b4d4c75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a661ba6b-4576-44a6-bc79-c2e831261a45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c22f9d-cfec-4e93-9435-aa381b4d4c75",
            "compositeImage": {
                "id": "a068c940-33dd-41c7-bce4-59bfd5a87bbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a661ba6b-4576-44a6-bc79-c2e831261a45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c5ff2d6-5244-4003-b7f5-a7e70c5bdccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a661ba6b-4576-44a6-bc79-c2e831261a45",
                    "LayerId": "1b253210-8e08-4423-9a36-c2972739f59e"
                }
            ]
        },
        {
            "id": "c8f68387-28be-4633-ae64-96c601d2812f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c22f9d-cfec-4e93-9435-aa381b4d4c75",
            "compositeImage": {
                "id": "e5cca45f-f6f9-4019-9168-db93fbafbfc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f68387-28be-4633-ae64-96c601d2812f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f481ef9-3392-4f94-a126-17ba152f3038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f68387-28be-4633-ae64-96c601d2812f",
                    "LayerId": "1b253210-8e08-4423-9a36-c2972739f59e"
                }
            ]
        },
        {
            "id": "21e38358-6796-4a47-9188-a12d06afeee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41c22f9d-cfec-4e93-9435-aa381b4d4c75",
            "compositeImage": {
                "id": "33c7a881-315a-450a-a427-339843147d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21e38358-6796-4a47-9188-a12d06afeee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12224401-97dd-4b9b-ade6-168968d5e741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21e38358-6796-4a47-9188-a12d06afeee5",
                    "LayerId": "1b253210-8e08-4423-9a36-c2972739f59e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1b253210-8e08-4423-9a36-c2972739f59e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41c22f9d-cfec-4e93-9435-aa381b4d4c75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}