{
    "id": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_watering_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acf23f83-8520-44d3-9cac-add1c06960ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "compositeImage": {
                "id": "716a084e-e49c-4e77-afed-f517a55da8ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acf23f83-8520-44d3-9cac-add1c06960ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c9b02b-859e-4f70-93dc-9f2a4bfadab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acf23f83-8520-44d3-9cac-add1c06960ad",
                    "LayerId": "91f7ec81-93ae-450d-bb03-d3648040bc01"
                }
            ]
        },
        {
            "id": "d9068183-1a45-4369-9dfa-023cab9640b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "compositeImage": {
                "id": "ecbc0bca-3a3d-46b4-b098-814785a02ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9068183-1a45-4369-9dfa-023cab9640b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf53a1d-ace9-42ba-a97a-b669f5b06cef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9068183-1a45-4369-9dfa-023cab9640b7",
                    "LayerId": "91f7ec81-93ae-450d-bb03-d3648040bc01"
                }
            ]
        },
        {
            "id": "00950b2c-f912-4972-a709-a89f92e249c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "compositeImage": {
                "id": "d4e4fd08-1b44-4bce-bc97-4b249611fb79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00950b2c-f912-4972-a709-a89f92e249c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "195f7095-5dbd-467b-a792-4135a1fb44f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00950b2c-f912-4972-a709-a89f92e249c8",
                    "LayerId": "91f7ec81-93ae-450d-bb03-d3648040bc01"
                }
            ]
        },
        {
            "id": "d68e7249-33e8-4a21-8b4c-61870a85ea5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "compositeImage": {
                "id": "116db039-ccb0-4e6c-8337-6bf0fd3fe47b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d68e7249-33e8-4a21-8b4c-61870a85ea5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811b3396-82dd-4e05-9067-9b8c343d15de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d68e7249-33e8-4a21-8b4c-61870a85ea5e",
                    "LayerId": "91f7ec81-93ae-450d-bb03-d3648040bc01"
                }
            ]
        },
        {
            "id": "5f8b2704-ecfd-4195-8b51-2efb3a4e45b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "compositeImage": {
                "id": "19b6dcd2-1556-4627-a4f6-f290c185494c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f8b2704-ecfd-4195-8b51-2efb3a4e45b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c45ad307-f661-44eb-91c0-9b825d3a8c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f8b2704-ecfd-4195-8b51-2efb3a4e45b9",
                    "LayerId": "91f7ec81-93ae-450d-bb03-d3648040bc01"
                }
            ]
        },
        {
            "id": "f51f294a-16dc-4290-8c78-b06d6f7b6a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "compositeImage": {
                "id": "1cc279b2-4734-40a6-9985-ccd30948669c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f51f294a-16dc-4290-8c78-b06d6f7b6a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2edf8459-04c5-4fb6-99e5-44396a7f57d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f51f294a-16dc-4290-8c78-b06d6f7b6a89",
                    "LayerId": "91f7ec81-93ae-450d-bb03-d3648040bc01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "91f7ec81-93ae-450d-bb03-d3648040bc01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30ab9d62-2c5a-41e3-9f3d-cfaefb991c2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}