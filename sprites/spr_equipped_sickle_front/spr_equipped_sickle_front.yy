{
    "id": "4bd5480f-28b8-4274-95e6-121cc8038bad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_sickle_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64c5cf44-a707-46c5-95b7-87959426d8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bd5480f-28b8-4274-95e6-121cc8038bad",
            "compositeImage": {
                "id": "52d87125-8434-45b3-9a24-a9dab2a5fce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c5cf44-a707-46c5-95b7-87959426d8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1454509-7acf-42e9-b964-235e8c12bff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c5cf44-a707-46c5-95b7-87959426d8ea",
                    "LayerId": "84b0ab59-ab1f-4618-896f-46a979e66993"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "84b0ab59-ab1f-4618-896f-46a979e66993",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bd5480f-28b8-4274-95e6-121cc8038bad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 1,
    "yorig": 14
}