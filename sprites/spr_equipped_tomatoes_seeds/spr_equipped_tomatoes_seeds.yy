{
    "id": "c6859948-4f84-415f-adc9-bcf17903e006",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_tomatoes_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "189d34b5-0a69-4329-9a8f-3d0b6f8f917a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6859948-4f84-415f-adc9-bcf17903e006",
            "compositeImage": {
                "id": "93fca984-4108-4a55-a366-7a8b33bf6826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189d34b5-0a69-4329-9a8f-3d0b6f8f917a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb49c165-3977-478f-bef7-15e7bb0c0669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189d34b5-0a69-4329-9a8f-3d0b6f8f917a",
                    "LayerId": "a87ee2fa-48a1-4b04-8fdb-6a9a03ca10f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a87ee2fa-48a1-4b04-8fdb-6a9a03ca10f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6859948-4f84-415f-adc9-bcf17903e006",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}