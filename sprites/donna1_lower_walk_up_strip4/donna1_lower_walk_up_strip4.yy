{
    "id": "fcf86bc6-998f-4d94-bb6a-cebd64e8862a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5b93ea1-e872-4928-9df2-1267287e9d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcf86bc6-998f-4d94-bb6a-cebd64e8862a",
            "compositeImage": {
                "id": "ee5c63ac-dfc1-4bfe-a04d-9ebf06fcffea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b93ea1-e872-4928-9df2-1267287e9d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f068e4ab-ee6b-4190-ad31-cae999ae671e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b93ea1-e872-4928-9df2-1267287e9d68",
                    "LayerId": "84326868-71bb-44c6-839d-2cd7c0c0b9ad"
                }
            ]
        },
        {
            "id": "69888a19-8861-4504-bc80-f9ca631e899b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcf86bc6-998f-4d94-bb6a-cebd64e8862a",
            "compositeImage": {
                "id": "7ba0fcd5-bbb1-41b9-bfd0-74be4c2baf78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69888a19-8861-4504-bc80-f9ca631e899b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46064e9d-281f-42a2-8844-814395957712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69888a19-8861-4504-bc80-f9ca631e899b",
                    "LayerId": "84326868-71bb-44c6-839d-2cd7c0c0b9ad"
                }
            ]
        },
        {
            "id": "961597ae-a364-49ab-a84d-0ad642e6b544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcf86bc6-998f-4d94-bb6a-cebd64e8862a",
            "compositeImage": {
                "id": "88ad08f8-0859-4d0d-9b8c-06298dd44311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "961597ae-a364-49ab-a84d-0ad642e6b544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af25c4ff-d0b8-423e-bd7f-c4d6a274e35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "961597ae-a364-49ab-a84d-0ad642e6b544",
                    "LayerId": "84326868-71bb-44c6-839d-2cd7c0c0b9ad"
                }
            ]
        },
        {
            "id": "a580c537-3b9e-4acf-9cee-e3002f4eddad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcf86bc6-998f-4d94-bb6a-cebd64e8862a",
            "compositeImage": {
                "id": "d86e7a44-c414-4dba-9d33-43b956eeaf58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a580c537-3b9e-4acf-9cee-e3002f4eddad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad48f846-c1c2-441b-8eb6-e30857ef8d62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a580c537-3b9e-4acf-9cee-e3002f4eddad",
                    "LayerId": "84326868-71bb-44c6-839d-2cd7c0c0b9ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84326868-71bb-44c6-839d-2cd7c0c0b9ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcf86bc6-998f-4d94-bb6a-cebd64e8862a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}