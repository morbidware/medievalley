{
    "id": "6e4c774c-23b7-45c5-9ab4-084b36a0cf59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_amanitamushroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "949d46b7-55a8-419a-9333-7d47a2897e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e4c774c-23b7-45c5-9ab4-084b36a0cf59",
            "compositeImage": {
                "id": "af50ef92-7ff5-4bc0-8b45-a1c44e8b849a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949d46b7-55a8-419a-9333-7d47a2897e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d26c247-327a-4b9f-a32b-105ea16182d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949d46b7-55a8-419a-9333-7d47a2897e3e",
                    "LayerId": "79f2909e-039b-4c94-b9fc-17663dbb7a09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79f2909e-039b-4c94-b9fc-17663dbb7a09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e4c774c-23b7-45c5-9ab4-084b36a0cf59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}