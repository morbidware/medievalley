{
    "id": "31c3cbcb-c2b0-408c-a07e-3e0deb0dad94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6534cd3-deff-40f9-8a0a-30b609ab23bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c3cbcb-c2b0-408c-a07e-3e0deb0dad94",
            "compositeImage": {
                "id": "9ad1170f-2148-4c4e-b7fb-65a5546afa2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6534cd3-deff-40f9-8a0a-30b609ab23bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "882889b3-a0c3-460d-afb5-42dc398a3878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6534cd3-deff-40f9-8a0a-30b609ab23bb",
                    "LayerId": "d043fd70-8b0d-4a6a-a6ba-52661552ba0c"
                }
            ]
        },
        {
            "id": "f8a640c6-362a-4ac4-a869-1baf435b8dcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c3cbcb-c2b0-408c-a07e-3e0deb0dad94",
            "compositeImage": {
                "id": "5becab19-00b0-40bd-895f-1af8eae0769e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a640c6-362a-4ac4-a869-1baf435b8dcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "405ad718-558f-4206-83bc-173fed9cd44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a640c6-362a-4ac4-a869-1baf435b8dcc",
                    "LayerId": "d043fd70-8b0d-4a6a-a6ba-52661552ba0c"
                }
            ]
        },
        {
            "id": "e660443f-6394-4c10-b04b-75d3ad8f43dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c3cbcb-c2b0-408c-a07e-3e0deb0dad94",
            "compositeImage": {
                "id": "de688983-2616-4f9b-b16a-f9d57891ab31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e660443f-6394-4c10-b04b-75d3ad8f43dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd3f3454-9d2b-408c-949a-730dabb8a74e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e660443f-6394-4c10-b04b-75d3ad8f43dc",
                    "LayerId": "d043fd70-8b0d-4a6a-a6ba-52661552ba0c"
                }
            ]
        },
        {
            "id": "43ce8e12-b8d7-4813-bd32-a3ea72ef9d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31c3cbcb-c2b0-408c-a07e-3e0deb0dad94",
            "compositeImage": {
                "id": "b29d54ea-1f41-45e7-8bb1-c516a63eed4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ce8e12-b8d7-4813-bd32-a3ea72ef9d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d042e5a-7439-4dfa-b067-0e36389140ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ce8e12-b8d7-4813-bd32-a3ea72ef9d59",
                    "LayerId": "d043fd70-8b0d-4a6a-a6ba-52661552ba0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d043fd70-8b0d-4a6a-a6ba-52661552ba0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31c3cbcb-c2b0-408c-a07e-3e0deb0dad94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}