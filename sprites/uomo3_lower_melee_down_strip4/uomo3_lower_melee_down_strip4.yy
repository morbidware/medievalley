{
    "id": "84544be3-060f-494c-bee3-3748ee63bde4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f48f820-2cdd-497d-baef-1033f8bc87bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84544be3-060f-494c-bee3-3748ee63bde4",
            "compositeImage": {
                "id": "bffbfb8c-e078-4463-92a5-ba4244f7d55c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f48f820-2cdd-497d-baef-1033f8bc87bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "069cce60-d68a-4671-8701-d75c5447c97d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f48f820-2cdd-497d-baef-1033f8bc87bb",
                    "LayerId": "6e91d0c6-a261-4633-b79a-211431934804"
                }
            ]
        },
        {
            "id": "8b408150-ebaa-411b-933c-745b77cafad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84544be3-060f-494c-bee3-3748ee63bde4",
            "compositeImage": {
                "id": "51e897e6-ecd7-4138-9775-54b6e8b2c320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b408150-ebaa-411b-933c-745b77cafad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e1c7419-2ec6-4f62-88cd-64ade9b96dd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b408150-ebaa-411b-933c-745b77cafad5",
                    "LayerId": "6e91d0c6-a261-4633-b79a-211431934804"
                }
            ]
        },
        {
            "id": "083a5b55-e823-46f8-8b81-b9c26fe1d7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84544be3-060f-494c-bee3-3748ee63bde4",
            "compositeImage": {
                "id": "5e44847f-66d9-427f-820d-f5aafd6f07af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083a5b55-e823-46f8-8b81-b9c26fe1d7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdda15f8-8011-4ebf-999e-86885318be2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083a5b55-e823-46f8-8b81-b9c26fe1d7b4",
                    "LayerId": "6e91d0c6-a261-4633-b79a-211431934804"
                }
            ]
        },
        {
            "id": "0549755d-eb3f-4ba0-847b-991c522064cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84544be3-060f-494c-bee3-3748ee63bde4",
            "compositeImage": {
                "id": "69cb553c-1cc8-4a7f-a7be-7eaec4fc11d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0549755d-eb3f-4ba0-847b-991c522064cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119a2097-6db4-4c02-a7ad-8f04c2c76584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0549755d-eb3f-4ba0-847b-991c522064cb",
                    "LayerId": "6e91d0c6-a261-4633-b79a-211431934804"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6e91d0c6-a261-4633-b79a-211431934804",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84544be3-060f-494c-bee3-3748ee63bde4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}