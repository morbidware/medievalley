{
    "id": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_chestnuttree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 6,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e143254c-a81d-490b-a52e-425bd4b88ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "compositeImage": {
                "id": "4c499e22-f58f-4711-b374-69bfcf71eee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e143254c-a81d-490b-a52e-425bd4b88ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04652658-72f4-4a6e-b1d9-89cef16671aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e143254c-a81d-490b-a52e-425bd4b88ef3",
                    "LayerId": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c"
                }
            ]
        },
        {
            "id": "dcff247c-34a8-4b7d-9eb8-a72a30225b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "compositeImage": {
                "id": "7b0fbaa5-8516-4ca6-8bea-68c81bf5ae29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcff247c-34a8-4b7d-9eb8-a72a30225b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3445b54-7e64-43fb-be14-2c6f74569c6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcff247c-34a8-4b7d-9eb8-a72a30225b5b",
                    "LayerId": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c"
                }
            ]
        },
        {
            "id": "32d9ff21-2424-47a0-bf4f-dd6154002593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "compositeImage": {
                "id": "f1678f23-a9d9-40ea-a6a8-d879941bcfa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32d9ff21-2424-47a0-bf4f-dd6154002593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2598cba-b250-492c-8993-f18deb82ae38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d9ff21-2424-47a0-bf4f-dd6154002593",
                    "LayerId": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c"
                }
            ]
        },
        {
            "id": "1433bbd1-0ebc-427f-97d2-00191e5fa0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "compositeImage": {
                "id": "006d860c-dc5f-4df2-b16b-fa4003d5e801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1433bbd1-0ebc-427f-97d2-00191e5fa0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644958c9-30a1-4eb6-ac09-3ff92f29e4b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1433bbd1-0ebc-427f-97d2-00191e5fa0b2",
                    "LayerId": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c"
                }
            ]
        },
        {
            "id": "9f7b8d54-710a-45b6-843a-b91721336218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "compositeImage": {
                "id": "cb4990e9-94a5-4afd-9ffc-91d9c58b388b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f7b8d54-710a-45b6-843a-b91721336218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e059eac2-b3a9-4bfb-9793-47b2c2714611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f7b8d54-710a-45b6-843a-b91721336218",
                    "LayerId": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c"
                }
            ]
        },
        {
            "id": "2bff8403-51db-492d-a5d9-5df425d5f6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "compositeImage": {
                "id": "7d2d2681-93c7-4666-9921-b618876695a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bff8403-51db-492d-a5d9-5df425d5f6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f67b39b6-f15a-4756-b55a-65a518db0eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bff8403-51db-492d-a5d9-5df425d5f6b5",
                    "LayerId": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "f2f1a53b-9d17-46ff-b2a6-d0eb42c3b86c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e61f261-81a6-4b03-843b-4980f3a7d10a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 77
}