{
    "id": "844194c9-0575-48f1-9930-78fa5c04bddc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dc9d112f-69e1-4877-b423-52d5d5c050d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "844194c9-0575-48f1-9930-78fa5c04bddc",
            "compositeImage": {
                "id": "57d3d30e-fa41-4b98-bf97-08a3a18ab033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc9d112f-69e1-4877-b423-52d5d5c050d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f9c4a2-9eb8-4398-bc67-82f1410c72ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc9d112f-69e1-4877-b423-52d5d5c050d6",
                    "LayerId": "6c261bf5-d037-4b23-bf20-0013a42f8fc6"
                }
            ]
        },
        {
            "id": "1bfc1b57-78e2-4e22-a583-e224633a8b8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "844194c9-0575-48f1-9930-78fa5c04bddc",
            "compositeImage": {
                "id": "da1a7f35-68b9-48ac-a185-818cc4d4e213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bfc1b57-78e2-4e22-a583-e224633a8b8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86003a68-592d-44e8-a12e-3ea178ddc8f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bfc1b57-78e2-4e22-a583-e224633a8b8f",
                    "LayerId": "6c261bf5-d037-4b23-bf20-0013a42f8fc6"
                }
            ]
        },
        {
            "id": "08003369-eb00-46a3-8719-c3d29f128063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "844194c9-0575-48f1-9930-78fa5c04bddc",
            "compositeImage": {
                "id": "8f0ba515-1d62-4457-9042-573d166a5333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08003369-eb00-46a3-8719-c3d29f128063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94cfa36e-5125-44d5-a46b-c4585e8a2287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08003369-eb00-46a3-8719-c3d29f128063",
                    "LayerId": "6c261bf5-d037-4b23-bf20-0013a42f8fc6"
                }
            ]
        },
        {
            "id": "a0e713dc-09e0-40a9-b8da-9dd9063602aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "844194c9-0575-48f1-9930-78fa5c04bddc",
            "compositeImage": {
                "id": "e17cb964-5d98-45f7-9ded-70c2e62d80c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e713dc-09e0-40a9-b8da-9dd9063602aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c9b358-d57d-4b05-80b9-7208f6e702fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e713dc-09e0-40a9-b8da-9dd9063602aa",
                    "LayerId": "6c261bf5-d037-4b23-bf20-0013a42f8fc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6c261bf5-d037-4b23-bf20-0013a42f8fc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "844194c9-0575-48f1-9930-78fa5c04bddc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}