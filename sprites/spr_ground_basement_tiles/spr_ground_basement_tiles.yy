{
    "id": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_basement_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b5c8fcb-23ff-4107-866c-9948f586d6fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "5d7a73c2-8c5c-42a3-a162-53a0e6e29b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5c8fcb-23ff-4107-866c-9948f586d6fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb98c67-6239-4d28-a17b-0dc6d64bdd7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5c8fcb-23ff-4107-866c-9948f586d6fd",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "24bb0c6a-6685-429b-b490-a35eb01b6911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "da9f47ff-5a01-4f2e-b2cc-e7fbb12181d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24bb0c6a-6685-429b-b490-a35eb01b6911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fedc78a8-68ea-4157-a5f9-b0cd2a2b8bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24bb0c6a-6685-429b-b490-a35eb01b6911",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "f9ef30a6-e08f-4aa2-8c8b-254c302609db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "44bd7f72-3d34-4506-9cd4-0bb1053d5242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9ef30a6-e08f-4aa2-8c8b-254c302609db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e38ea89-1ba7-4c5f-9303-36d291277476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9ef30a6-e08f-4aa2-8c8b-254c302609db",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "a121e066-ec76-4d86-b092-653b16762fe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "edcef522-5b87-47e7-a0bb-dc6c4ef05994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a121e066-ec76-4d86-b092-653b16762fe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a60f707-9d16-4913-8136-5458754e7895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a121e066-ec76-4d86-b092-653b16762fe4",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "d06ae09e-5775-4f82-8d4a-f397eae045ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "89cf7cf7-5c52-43b2-869b-5cab07d4afae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d06ae09e-5775-4f82-8d4a-f397eae045ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d4db77-6533-48a8-86f4-1e8cc804f1e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d06ae09e-5775-4f82-8d4a-f397eae045ba",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "508bf1d7-8e4c-4e18-acc0-5599d23538a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "15685f84-5c82-46c3-8e18-6642cccfe8e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "508bf1d7-8e4c-4e18-acc0-5599d23538a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0694d8e7-69d8-4439-bc23-7dde09c9c235",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "508bf1d7-8e4c-4e18-acc0-5599d23538a4",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "65a6c043-b84b-4083-9f50-378381d37d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "ca89d0bc-3dc2-4cc1-a7fb-a88b35a38c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a6c043-b84b-4083-9f50-378381d37d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2a2dfb-e3b3-4fd2-af97-1433367dd757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a6c043-b84b-4083-9f50-378381d37d51",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "99ddfce3-268e-4b1e-9a2b-1a976abc5803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "2cfe3cdd-84e5-4307-818e-a720aafaf25b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99ddfce3-268e-4b1e-9a2b-1a976abc5803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90171b2f-13e7-438c-be2b-06efde8132c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99ddfce3-268e-4b1e-9a2b-1a976abc5803",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "0cd792dd-5b5b-47d5-aabc-56fda1b56370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "6866c929-448f-4152-9adb-617cc4ea1919",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cd792dd-5b5b-47d5-aabc-56fda1b56370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25dbad70-f5af-4fa0-beef-d5ec3d5c17cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cd792dd-5b5b-47d5-aabc-56fda1b56370",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "a8fb2c8d-6bb9-4e64-b45e-83ba5749e0e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "b2d410be-c36c-4c8d-8573-34fab90c5563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8fb2c8d-6bb9-4e64-b45e-83ba5749e0e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd765d00-b62d-479c-8bef-6df192525203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8fb2c8d-6bb9-4e64-b45e-83ba5749e0e5",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "10dff751-eb11-4dea-ae2e-1f0b2891bca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "d72595ad-3905-42a1-9d3a-0d865724330d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10dff751-eb11-4dea-ae2e-1f0b2891bca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ad912e-524f-4843-9fdb-b725dc04f130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10dff751-eb11-4dea-ae2e-1f0b2891bca5",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "83e1b1a4-abaf-4bfa-aa79-6937933693e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "b9a9d39d-caf6-4fbc-ba7d-f649070a33c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e1b1a4-abaf-4bfa-aa79-6937933693e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e011d6-6be9-4670-bd33-db6b2f3800b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e1b1a4-abaf-4bfa-aa79-6937933693e6",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "492350e9-4b73-4364-98d1-62c89fcfa22b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "17cd451a-2ebb-4f46-8cd0-1b1f8b6c79d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492350e9-4b73-4364-98d1-62c89fcfa22b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72329788-ba69-4bb5-b988-01117224a2a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492350e9-4b73-4364-98d1-62c89fcfa22b",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "aeace2e4-0d83-458a-94d4-dfb62b2faefb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "30bca955-267e-4ae0-924c-9073faa3fd85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeace2e4-0d83-458a-94d4-dfb62b2faefb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba360ec-1431-462d-9e75-48dc3fef9d79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeace2e4-0d83-458a-94d4-dfb62b2faefb",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "e4583ef2-3f92-4264-94a0-3a89f98741cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "ffc1b298-6260-4ced-9f39-1157649b088e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4583ef2-3f92-4264-94a0-3a89f98741cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a242fef-db90-47b7-97ef-5a33c6221bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4583ef2-3f92-4264-94a0-3a89f98741cc",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "5ef51f65-2a5a-499c-ba85-f82ddb6d629b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "3366a7e2-59d8-4280-8671-095d6d516bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef51f65-2a5a-499c-ba85-f82ddb6d629b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad92ff1-0100-4d20-9257-55e18923c3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef51f65-2a5a-499c-ba85-f82ddb6d629b",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "6ef0e7de-e3d7-48ec-9eee-1dceacdd0773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "c33f7438-063a-431c-a67d-e71ca89d6217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef0e7de-e3d7-48ec-9eee-1dceacdd0773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a102fa-636c-49e9-9a38-243aeb7ac2f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef0e7de-e3d7-48ec-9eee-1dceacdd0773",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "cc7d4351-8efc-4c1e-bc95-713ae2299372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "594111fd-98c8-4548-a8fb-8d4bda9aa8c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7d4351-8efc-4c1e-bc95-713ae2299372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d862e3e8-5c05-4ea1-a811-8b60dbd3c591",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7d4351-8efc-4c1e-bc95-713ae2299372",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "73d2e5be-b0a1-4cb7-8e40-74d561845cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "48dc5eb6-7038-4902-9908-eae3abc4fc35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73d2e5be-b0a1-4cb7-8e40-74d561845cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a7d56ae-2953-482c-a933-8aa6cdb09fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73d2e5be-b0a1-4cb7-8e40-74d561845cda",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "944a97d8-906c-40e0-a5f8-b16812dc4011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "a6f6c345-2988-4e8d-b4b9-b0909d057dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "944a97d8-906c-40e0-a5f8-b16812dc4011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b7bd15-cfb4-4a84-a333-32905e2cc4f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944a97d8-906c-40e0-a5f8-b16812dc4011",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "9e04b344-f720-4a12-bc09-c280cb294198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "ecaffec8-d98d-4cdf-b9b5-0059513dfc7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e04b344-f720-4a12-bc09-c280cb294198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5122f48-b977-4fa7-910e-f23c232f3db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e04b344-f720-4a12-bc09-c280cb294198",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "be3fa3fa-1cbc-420c-8ed8-a2d989dd84ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "37e1ddc5-3384-4e2b-ac53-9781bb974f12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3fa3fa-1cbc-420c-8ed8-a2d989dd84ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4827792d-5c20-4565-88b8-2f628a8f414f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3fa3fa-1cbc-420c-8ed8-a2d989dd84ca",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "ed80d654-a070-466c-9fe4-962c0bc11483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "b09ca75a-3a46-459e-ad59-a7b3e6de14a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed80d654-a070-466c-9fe4-962c0bc11483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60420a69-dfc4-4e14-b267-6d2942c97d66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed80d654-a070-466c-9fe4-962c0bc11483",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "8b1e4a7c-f801-4d1f-a6a3-ed2679037d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "a93e784e-5aa9-4eb1-8518-6acbd3febaf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1e4a7c-f801-4d1f-a6a3-ed2679037d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de59d8fa-e1c8-496e-bb8e-c52d303598bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1e4a7c-f801-4d1f-a6a3-ed2679037d48",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "98e5dae7-3dc7-4fdd-9611-881f6854c06e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "d250f32c-b46f-4cb0-8d11-bbb3d5b6a287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98e5dae7-3dc7-4fdd-9611-881f6854c06e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd6a428-c3b1-4532-94f8-a0a3f588115f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98e5dae7-3dc7-4fdd-9611-881f6854c06e",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "965238da-366d-42b8-8e09-db1f8e2524f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "9cc9f9a2-0ad0-4ad9-a673-bcd40ad8fe5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "965238da-366d-42b8-8e09-db1f8e2524f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc98d8e-13f6-484d-8969-142510a412c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "965238da-366d-42b8-8e09-db1f8e2524f5",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "bfe7931f-adb1-4c28-bb56-aae01aca9fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "363795b5-c626-4dac-a225-94a4e1d51409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe7931f-adb1-4c28-bb56-aae01aca9fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99dde268-d52b-4a78-bf10-c46b984bc943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe7931f-adb1-4c28-bb56-aae01aca9fa5",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "eb74b3f9-535f-44e7-b1c8-62b88c9b6e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "b3de899b-4e11-4401-8984-d896526ace20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb74b3f9-535f-44e7-b1c8-62b88c9b6e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b8b9420-3e60-492d-b4a4-a1f265332133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb74b3f9-535f-44e7-b1c8-62b88c9b6e67",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "dd656953-0815-4c7c-b6d6-366626276871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "959d637c-388e-4a69-9583-f7b83350da9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd656953-0815-4c7c-b6d6-366626276871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf3ecf6-0581-47c6-988d-2e5330626051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd656953-0815-4c7c-b6d6-366626276871",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "b4ada79a-efef-4494-8a55-d7c4ec76e093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "68e8e3ea-3704-4ae2-9035-3a568a605e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ada79a-efef-4494-8a55-d7c4ec76e093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "177158b6-a363-4d58-9373-8e5adc958df4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ada79a-efef-4494-8a55-d7c4ec76e093",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "ec64d545-5b22-4bad-9ab3-bcc0d8ec693a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "1ba098b1-0e8f-451f-9a1b-6a1b8bfb1043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec64d545-5b22-4bad-9ab3-bcc0d8ec693a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0a59829-099b-482d-875a-b208b6ffef0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec64d545-5b22-4bad-9ab3-bcc0d8ec693a",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "a22ac8d3-3c83-4cc9-a617-2c7e3427d59f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "6947465c-627b-4482-a147-6dc90e7c97f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22ac8d3-3c83-4cc9-a617-2c7e3427d59f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ca6c30-8bb6-4eb3-97d1-0ccc59a76dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22ac8d3-3c83-4cc9-a617-2c7e3427d59f",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "f3f96a30-f943-457c-91ee-638e5acc4c1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "d61e7990-d140-407b-b0f4-c466b29f26b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f96a30-f943-457c-91ee-638e5acc4c1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f0bff18-ca01-4353-bc0e-6f42d8d9e89b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f96a30-f943-457c-91ee-638e5acc4c1f",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "74caebe0-6ad5-4b3f-9f20-15b22e405a77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "17000e8f-234a-4192-b3da-32b21533f32b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74caebe0-6ad5-4b3f-9f20-15b22e405a77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc9bc45-1f4c-4588-ae2f-0b17d52397a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74caebe0-6ad5-4b3f-9f20-15b22e405a77",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "45b898ee-fa60-4c74-85fb-f19e6715f763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "2f681413-23b3-4907-ab5a-53864d9e8e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45b898ee-fa60-4c74-85fb-f19e6715f763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5744f00-fd7d-4f49-8023-8b09aa4ea1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45b898ee-fa60-4c74-85fb-f19e6715f763",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "731347b8-a244-4c16-b9b2-8e352d79f700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "d1a7416e-3cdd-49b0-8d8c-5d496542cc2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "731347b8-a244-4c16-b9b2-8e352d79f700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb542d1-1b85-469a-a5b0-3720923b9bd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "731347b8-a244-4c16-b9b2-8e352d79f700",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "afd9bbe5-29a0-4336-b53c-2a4e8299c9c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "9518477c-1005-4acc-9efb-609acb709af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afd9bbe5-29a0-4336-b53c-2a4e8299c9c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d6e2e99-f84c-4865-9839-e4169f8cfb36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afd9bbe5-29a0-4336-b53c-2a4e8299c9c6",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "8a209d69-f1e2-4411-8f0d-da6e3f691fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "f6112558-b0e8-4877-9b8c-57223cc23970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a209d69-f1e2-4411-8f0d-da6e3f691fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78832ac0-9da5-4233-9ded-baca4b86fde1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a209d69-f1e2-4411-8f0d-da6e3f691fb6",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "e61411aa-a50f-49da-a0c1-aceaa6885689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "f27a54d2-c66f-4e43-95ae-9af77068b324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e61411aa-a50f-49da-a0c1-aceaa6885689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "070ee6a4-d29c-461b-b1a4-4998f6535c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e61411aa-a50f-49da-a0c1-aceaa6885689",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "00c257e1-3992-4ca5-b27f-5c3b7016c6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "2f32e5d8-6d92-41f1-ae4b-68d02a3cd948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c257e1-3992-4ca5-b27f-5c3b7016c6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86409609-b103-4783-bc2c-f7453d71e734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c257e1-3992-4ca5-b27f-5c3b7016c6ed",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "63e444b3-7067-4150-a143-de14fc55407e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "20c0a99f-5bef-400a-a99d-ff04c1f6aa52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63e444b3-7067-4150-a143-de14fc55407e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9445c3d0-706d-450d-81dc-6aecf8cacbf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63e444b3-7067-4150-a143-de14fc55407e",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "f37aa3fa-3096-4532-a85c-d8e4ac179979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "8af43733-9c8c-4c2f-98db-05556e985a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f37aa3fa-3096-4532-a85c-d8e4ac179979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799047c5-c1c0-4904-a8cf-ddd06cf336fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f37aa3fa-3096-4532-a85c-d8e4ac179979",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "a83ead20-a2f3-48ce-8f85-54d28011cb78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "0f8ecebb-eedf-488d-a1bf-6ccb1c60e773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a83ead20-a2f3-48ce-8f85-54d28011cb78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770e1294-e699-41b0-ae95-619a67e5eed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a83ead20-a2f3-48ce-8f85-54d28011cb78",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "c5b3a7ed-df05-4824-8548-991ca725c8c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "e895adc4-9754-4452-b66b-32b4da261cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b3a7ed-df05-4824-8548-991ca725c8c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0ee8c66-0854-4200-a930-28d160662a95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b3a7ed-df05-4824-8548-991ca725c8c0",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "7fba5b77-f562-472a-9b70-9bed58ed0114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "361a681e-4007-40a6-b073-2aeb8fcdd12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fba5b77-f562-472a-9b70-9bed58ed0114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4d332a5-d072-4ae3-95d0-0594cd7523d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fba5b77-f562-472a-9b70-9bed58ed0114",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "b7fb2abd-57dc-49e2-a032-33bdd48d6bdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "e51bb7c3-0315-4de7-a5fa-555d35cb9aa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7fb2abd-57dc-49e2-a032-33bdd48d6bdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80d37063-51c0-496e-8500-177da7f7ffac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7fb2abd-57dc-49e2-a032-33bdd48d6bdf",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "79f910e9-0b7d-4900-8ed2-07e7979f4103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "445a3aec-9dd5-4a6c-a65f-b80e846f467e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f910e9-0b7d-4900-8ed2-07e7979f4103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "442396e8-f7b5-44e9-bb36-03bf61a38741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f910e9-0b7d-4900-8ed2-07e7979f4103",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "cee1b51e-3b1f-4983-8a27-32969fb75a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "3b53b1d8-8310-4f21-a354-53ef35c4a08d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee1b51e-3b1f-4983-8a27-32969fb75a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb3e21a-5a37-4331-998b-17a85c01c369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee1b51e-3b1f-4983-8a27-32969fb75a65",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "b77f7778-92c2-45a2-9705-81ba875b94c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "ee3d63fd-f77b-4236-a3af-defc44b16f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77f7778-92c2-45a2-9705-81ba875b94c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e24feffc-a90b-449a-9afc-91cc6acb0e7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77f7778-92c2-45a2-9705-81ba875b94c7",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "fddd3d65-5edd-4a99-9db9-d24799f5069f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "298de2cc-d398-436a-8ddb-ac5f65272e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fddd3d65-5edd-4a99-9db9-d24799f5069f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faf2ad2f-dcf5-428c-9378-f5f10d43006d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fddd3d65-5edd-4a99-9db9-d24799f5069f",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "6c7b1894-dfa3-4991-8732-29f5e4a984c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "060a06db-2385-4bb9-80e9-cfb94f170473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7b1894-dfa3-4991-8732-29f5e4a984c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54902ff3-3f14-47b8-a1c8-c7cfd9031609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7b1894-dfa3-4991-8732-29f5e4a984c9",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "9cf9666a-b51b-46b1-96fc-808054726255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "9e592616-2daa-419a-8aaf-b7ff2c60f70b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cf9666a-b51b-46b1-96fc-808054726255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d86263-5bfb-43a8-81ac-5337de06d4f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cf9666a-b51b-46b1-96fc-808054726255",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "6006d8d5-e6a8-46f3-b5fc-f1790c66e15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "cffa68e3-d717-401d-b312-1c54f07774da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6006d8d5-e6a8-46f3-b5fc-f1790c66e15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9265b4-1569-46ac-905d-6c5e18a0b490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6006d8d5-e6a8-46f3-b5fc-f1790c66e15b",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "a0ce87ba-def5-4c23-b002-8438269b4f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "0adf4a8d-259d-45b0-a540-a301141f9360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0ce87ba-def5-4c23-b002-8438269b4f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d8530d-4869-4b51-bbdf-2860fc82b5c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0ce87ba-def5-4c23-b002-8438269b4f1d",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "4adf7e0b-b04f-46f3-82ca-a5990a3e14f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "f92e5d30-8d43-41b5-8903-114dc0ce7673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4adf7e0b-b04f-46f3-82ca-a5990a3e14f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18c870e0-df16-4b72-8da4-e862a69f8b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4adf7e0b-b04f-46f3-82ca-a5990a3e14f5",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "82a06f1c-57db-45f0-837d-0a8347dc832e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "570e529a-95e9-4c14-9d4a-8c55e418fd32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a06f1c-57db-45f0-837d-0a8347dc832e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03abef46-57df-4d3f-9b9c-b8cda8bd813d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a06f1c-57db-45f0-837d-0a8347dc832e",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        },
        {
            "id": "d248ef97-8aaa-498a-9b91-fceea7b9b09e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "compositeImage": {
                "id": "ddc5a306-4a78-4c84-a013-b31aa9fcf190",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d248ef97-8aaa-498a-9b91-fceea7b9b09e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e716e6-087d-4696-a993-774b008ed0ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d248ef97-8aaa-498a-9b91-fceea7b9b09e",
                    "LayerId": "206d7a72-117b-45b0-aead-4f91c6ac5fc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "206d7a72-117b-45b0-aead-4f91c6ac5fc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95ca3536-2ebc-42cb-8361-5e2bf7087148",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 55
}