{
    "id": "e4e5c42e-ef47-44eb-b24e-8971ea34045d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "061a21db-8401-4be7-8368-3bfc7e703590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e5c42e-ef47-44eb-b24e-8971ea34045d",
            "compositeImage": {
                "id": "5f894054-753b-4525-a9d2-6c630ec52c6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061a21db-8401-4be7-8368-3bfc7e703590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26c6e4f-8342-4e7e-8144-d0c682cdf140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061a21db-8401-4be7-8368-3bfc7e703590",
                    "LayerId": "b625da51-77b6-4949-88f1-2edd9edd2a46"
                }
            ]
        },
        {
            "id": "3dea8170-d5b0-48f3-b24f-6b635a6bf147",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e5c42e-ef47-44eb-b24e-8971ea34045d",
            "compositeImage": {
                "id": "c2088615-cae4-494e-86ab-0e16cf393067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dea8170-d5b0-48f3-b24f-6b635a6bf147",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b83ea6f-77be-4695-a0a4-de59169f0747",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dea8170-d5b0-48f3-b24f-6b635a6bf147",
                    "LayerId": "b625da51-77b6-4949-88f1-2edd9edd2a46"
                }
            ]
        },
        {
            "id": "90c0f6cc-eb4b-4420-8a82-5dc730ca9ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e5c42e-ef47-44eb-b24e-8971ea34045d",
            "compositeImage": {
                "id": "f6577b35-2cd5-47fd-978f-64ac641cc655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90c0f6cc-eb4b-4420-8a82-5dc730ca9ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37ab2acb-e8db-429c-94f8-52405d469f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90c0f6cc-eb4b-4420-8a82-5dc730ca9ebd",
                    "LayerId": "b625da51-77b6-4949-88f1-2edd9edd2a46"
                }
            ]
        },
        {
            "id": "76948d16-0b3b-4b99-bca5-b3b0513645ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e5c42e-ef47-44eb-b24e-8971ea34045d",
            "compositeImage": {
                "id": "a79edeff-01fc-4561-8abd-89ddb899038e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76948d16-0b3b-4b99-bca5-b3b0513645ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39807c81-2c17-41d0-ae69-a4b3495ba9bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76948d16-0b3b-4b99-bca5-b3b0513645ed",
                    "LayerId": "b625da51-77b6-4949-88f1-2edd9edd2a46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b625da51-77b6-4949-88f1-2edd9edd2a46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4e5c42e-ef47-44eb-b24e-8971ea34045d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}