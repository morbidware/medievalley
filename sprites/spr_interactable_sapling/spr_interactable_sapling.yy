{
    "id": "4cfb8be6-909a-46bc-9abb-cd494464577b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_sapling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ec2814a-f940-4ada-9e0c-ecf9ff887eff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cfb8be6-909a-46bc-9abb-cd494464577b",
            "compositeImage": {
                "id": "1864c2a5-0c1c-4e51-a40a-d67c457bd7e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec2814a-f940-4ada-9e0c-ecf9ff887eff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4228d43-4d0e-44b9-8ae3-329ecacd7166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec2814a-f940-4ada-9e0c-ecf9ff887eff",
                    "LayerId": "fc6ef92e-6f11-42a0-88c5-b3b9a30ad114"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "fc6ef92e-6f11-42a0-88c5-b3b9a30ad114",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cfb8be6-909a-46bc-9abb-cd494464577b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 15
}