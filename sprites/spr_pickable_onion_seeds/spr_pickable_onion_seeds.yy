{
    "id": "1ff56eca-28fa-4fd6-863b-10012bfb44d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_onion_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1db842e-7899-4a81-8759-cd5e00c895a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ff56eca-28fa-4fd6-863b-10012bfb44d0",
            "compositeImage": {
                "id": "8bbe5b4b-30c4-4261-8e48-e19a956e4cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1db842e-7899-4a81-8759-cd5e00c895a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d279562c-0302-46a3-9ba2-5b53fe99ac8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1db842e-7899-4a81-8759-cd5e00c895a2",
                    "LayerId": "c34d2b47-bf4d-4182-9739-9263102a72c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c34d2b47-bf4d-4182-9739-9263102a72c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ff56eca-28fa-4fd6-863b-10012bfb44d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}