{
    "id": "41eb98f4-5859-4aa4-b822-6358834eead9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_merchant_highlight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41dd1000-1908-40cf-bfb0-149782ec1b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41eb98f4-5859-4aa4-b822-6358834eead9",
            "compositeImage": {
                "id": "cf070a3d-ea7f-4acd-9d90-d4f093528d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41dd1000-1908-40cf-bfb0-149782ec1b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487fdb0f-3d65-4c6f-b4c6-c4565b112cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41dd1000-1908-40cf-bfb0-149782ec1b48",
                    "LayerId": "349f337e-a392-4dcc-99e0-aeb4ff680a39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "349f337e-a392-4dcc-99e0-aeb4ff680a39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41eb98f4-5859-4aa4-b822-6358834eead9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 39,
    "yorig": 6
}