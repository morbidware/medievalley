{
    "id": "1c10cc09-b8ad-49ea-97e3-a54f42856943",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potion_strength",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96302011-5da6-4c6e-8e07-bce062add4da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c10cc09-b8ad-49ea-97e3-a54f42856943",
            "compositeImage": {
                "id": "6cfe9a2a-5dc6-4c5d-b740-786a0fede2dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96302011-5da6-4c6e-8e07-bce062add4da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06509b5b-d8a2-4b5a-bbba-a27eedd25e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96302011-5da6-4c6e-8e07-bce062add4da",
                    "LayerId": "e29f4bb5-9496-4032-ba1a-3e7489784ff9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e29f4bb5-9496-4032-ba1a-3e7489784ff9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c10cc09-b8ad-49ea-97e3-a54f42856943",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}