{
    "id": "f81bf287-cbad-4f43-b476-fbd51e368fcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "246e49dc-0e9f-48b8-9356-378a506d61fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f81bf287-cbad-4f43-b476-fbd51e368fcc",
            "compositeImage": {
                "id": "94e0cbbe-beb8-453e-bebf-1b886c45b409",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246e49dc-0e9f-48b8-9356-378a506d61fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5e48815-f98e-4882-bdaf-7d79a2c9272a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246e49dc-0e9f-48b8-9356-378a506d61fe",
                    "LayerId": "93c4f10c-756c-4328-af20-eae00ee60500"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "93c4f10c-756c-4328-af20-eae00ee60500",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f81bf287-cbad-4f43-b476-fbd51e368fcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 27
}