{
    "id": "542964dc-445a-4919-aa3e-1ed312747cbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9113f013-3e91-43a7-b6e6-95f9cc7b4b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542964dc-445a-4919-aa3e-1ed312747cbe",
            "compositeImage": {
                "id": "02f0bab7-9519-44a9-8b5a-9d10a9a83848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9113f013-3e91-43a7-b6e6-95f9cc7b4b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b9cd0c-797d-44d9-80f7-f2c85997cb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9113f013-3e91-43a7-b6e6-95f9cc7b4b90",
                    "LayerId": "a2611df4-ed28-43a0-abaf-eb06ea77d3c2"
                }
            ]
        },
        {
            "id": "750f0d24-7639-436c-8cec-7d5cc721f81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542964dc-445a-4919-aa3e-1ed312747cbe",
            "compositeImage": {
                "id": "d66b7ed2-a80e-46d9-9541-99f834c199b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750f0d24-7639-436c-8cec-7d5cc721f81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad48eba-f17d-44a6-837e-9aa0a3fcccb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750f0d24-7639-436c-8cec-7d5cc721f81b",
                    "LayerId": "a2611df4-ed28-43a0-abaf-eb06ea77d3c2"
                }
            ]
        },
        {
            "id": "89a03a04-8c2b-40a6-9a3b-a255e94c2ecb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542964dc-445a-4919-aa3e-1ed312747cbe",
            "compositeImage": {
                "id": "d1f2f918-e8ad-405b-9b2e-b5c2b1cd1187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89a03a04-8c2b-40a6-9a3b-a255e94c2ecb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a333e2e8-bc30-41d2-ba36-69ed10db696b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89a03a04-8c2b-40a6-9a3b-a255e94c2ecb",
                    "LayerId": "a2611df4-ed28-43a0-abaf-eb06ea77d3c2"
                }
            ]
        },
        {
            "id": "bd260963-ec22-4082-8f9c-0b619730829e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "542964dc-445a-4919-aa3e-1ed312747cbe",
            "compositeImage": {
                "id": "f37c3f59-6a50-4cba-8992-379831fdcf1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd260963-ec22-4082-8f9c-0b619730829e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c392ca7d-e8ee-4d07-aa1b-dbbb537f7160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd260963-ec22-4082-8f9c-0b619730829e",
                    "LayerId": "a2611df4-ed28-43a0-abaf-eb06ea77d3c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2611df4-ed28-43a0-abaf-eb06ea77d3c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "542964dc-445a-4919-aa3e-1ed312747cbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}