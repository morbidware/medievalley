{
    "id": "8003ab0a-2fd5-41df-b5e5-50bf160ff938",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29ab9e48-832a-4107-8d83-dd607f6171db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8003ab0a-2fd5-41df-b5e5-50bf160ff938",
            "compositeImage": {
                "id": "68a99e59-1577-498a-8e40-2fee12b8cde7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29ab9e48-832a-4107-8d83-dd607f6171db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a286ba7-6ed6-4eb7-be76-a022ab82c5e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29ab9e48-832a-4107-8d83-dd607f6171db",
                    "LayerId": "18ed2191-96d1-4ef9-b06d-35721b9177dc"
                }
            ]
        },
        {
            "id": "4ea24f08-789b-4375-84bf-e60e8849fae2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8003ab0a-2fd5-41df-b5e5-50bf160ff938",
            "compositeImage": {
                "id": "63d0b425-cba4-415f-888a-836e74982ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ea24f08-789b-4375-84bf-e60e8849fae2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed11400-42f8-4a41-8ee2-21a832dcdc58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ea24f08-789b-4375-84bf-e60e8849fae2",
                    "LayerId": "18ed2191-96d1-4ef9-b06d-35721b9177dc"
                }
            ]
        },
        {
            "id": "d6431c30-3357-446a-ac16-5b9b8d28ee5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8003ab0a-2fd5-41df-b5e5-50bf160ff938",
            "compositeImage": {
                "id": "315d2160-ae3e-4df2-b2fa-76d475f2a9ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6431c30-3357-446a-ac16-5b9b8d28ee5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d23aa5-b778-41ea-8493-aa73a679c0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6431c30-3357-446a-ac16-5b9b8d28ee5a",
                    "LayerId": "18ed2191-96d1-4ef9-b06d-35721b9177dc"
                }
            ]
        },
        {
            "id": "c277b7c7-f575-439b-abe3-af97fdb81d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8003ab0a-2fd5-41df-b5e5-50bf160ff938",
            "compositeImage": {
                "id": "20b5bf5c-f6f2-4c75-a70e-286168c02dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c277b7c7-f575-439b-abe3-af97fdb81d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ba394a-f5c1-4281-9369-bebbc448bd66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c277b7c7-f575-439b-abe3-af97fdb81d47",
                    "LayerId": "18ed2191-96d1-4ef9-b06d-35721b9177dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "18ed2191-96d1-4ef9-b06d-35721b9177dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8003ab0a-2fd5-41df-b5e5-50bf160ff938",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}