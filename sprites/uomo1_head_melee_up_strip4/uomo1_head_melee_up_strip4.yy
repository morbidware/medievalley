{
    "id": "9bc18c42-b5f0-4655-b69c-5f2fc85151a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e798623c-c417-4673-9646-cc4aed5d03c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc18c42-b5f0-4655-b69c-5f2fc85151a5",
            "compositeImage": {
                "id": "eed43f2d-d255-4808-bc93-9f83a34859a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e798623c-c417-4673-9646-cc4aed5d03c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e84dbd-a013-43eb-8889-adf80a2d9a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e798623c-c417-4673-9646-cc4aed5d03c8",
                    "LayerId": "6b7f4661-5843-40bb-90f9-5e0fe3246ce6"
                }
            ]
        },
        {
            "id": "3084f4a5-ed1e-4c9a-a5c9-9b7d7e629a91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc18c42-b5f0-4655-b69c-5f2fc85151a5",
            "compositeImage": {
                "id": "956f3ea0-d0b3-4095-b83e-5bd386cd4489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3084f4a5-ed1e-4c9a-a5c9-9b7d7e629a91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "189a64b5-7156-4af5-8b22-8a0e4b161e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3084f4a5-ed1e-4c9a-a5c9-9b7d7e629a91",
                    "LayerId": "6b7f4661-5843-40bb-90f9-5e0fe3246ce6"
                }
            ]
        },
        {
            "id": "cee2de5d-7947-483f-af8b-bf76cf29142f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc18c42-b5f0-4655-b69c-5f2fc85151a5",
            "compositeImage": {
                "id": "05e940f6-c1fd-418a-9b35-78901e74f342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee2de5d-7947-483f-af8b-bf76cf29142f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d40b2a1f-3b1e-4bcf-bab3-2d4e2814a84d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee2de5d-7947-483f-af8b-bf76cf29142f",
                    "LayerId": "6b7f4661-5843-40bb-90f9-5e0fe3246ce6"
                }
            ]
        },
        {
            "id": "9505dc87-a878-4a6b-9bbd-6ac9f1ed3bdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bc18c42-b5f0-4655-b69c-5f2fc85151a5",
            "compositeImage": {
                "id": "308596d5-6a0a-4da5-99fc-a98c512ed053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9505dc87-a878-4a6b-9bbd-6ac9f1ed3bdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8cdf876-011b-481a-8681-150d3acbc2aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9505dc87-a878-4a6b-9bbd-6ac9f1ed3bdd",
                    "LayerId": "6b7f4661-5843-40bb-90f9-5e0fe3246ce6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b7f4661-5843-40bb-90f9-5e0fe3246ce6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bc18c42-b5f0-4655-b69c-5f2fc85151a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}