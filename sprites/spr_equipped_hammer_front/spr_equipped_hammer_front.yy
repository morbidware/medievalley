{
    "id": "da0aacf0-cbac-4709-a982-286ac4c9ba7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_hammer_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60ba492e-59c7-40fd-a4c8-c0bf5f97444a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da0aacf0-cbac-4709-a982-286ac4c9ba7b",
            "compositeImage": {
                "id": "20b978af-2822-4ea9-97b3-0d241535e95d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60ba492e-59c7-40fd-a4c8-c0bf5f97444a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a9453e3-ee7a-4c42-a466-d11cba2c208f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60ba492e-59c7-40fd-a4c8-c0bf5f97444a",
                    "LayerId": "62ddfcb1-2987-42c8-9349-6e0804e94788"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "62ddfcb1-2987-42c8-9349-6e0804e94788",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da0aacf0-cbac-4709-a982-286ac4c9ba7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 18
}