{
    "id": "463036db-61c7-4679-a3aa-9acc5eb31150",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water_sparkle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baf41ad4-6a46-4e4e-99b8-1428ebd50566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "463036db-61c7-4679-a3aa-9acc5eb31150",
            "compositeImage": {
                "id": "ec507001-c488-44d7-b92f-43c3447d83b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baf41ad4-6a46-4e4e-99b8-1428ebd50566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda17815-1068-4f58-8620-93c98e80f500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baf41ad4-6a46-4e4e-99b8-1428ebd50566",
                    "LayerId": "381731cc-112c-4279-a22f-a2daa8f14688"
                }
            ]
        },
        {
            "id": "9a010b33-7a31-48f6-9563-36f549521997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "463036db-61c7-4679-a3aa-9acc5eb31150",
            "compositeImage": {
                "id": "2c9c4c41-0026-4079-8b39-5b2564ef693e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a010b33-7a31-48f6-9563-36f549521997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf56524e-cdb4-49f6-8004-7c69835eac2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a010b33-7a31-48f6-9563-36f549521997",
                    "LayerId": "381731cc-112c-4279-a22f-a2daa8f14688"
                }
            ]
        },
        {
            "id": "178638f2-cb85-426a-b424-4e657d11c051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "463036db-61c7-4679-a3aa-9acc5eb31150",
            "compositeImage": {
                "id": "9b7680a1-0ef9-4b77-8235-0fd1a0cdc963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "178638f2-cb85-426a-b424-4e657d11c051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81410884-7ed4-4e96-8ff3-758c9983e5ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "178638f2-cb85-426a-b424-4e657d11c051",
                    "LayerId": "381731cc-112c-4279-a22f-a2daa8f14688"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "381731cc-112c-4279-a22f-a2daa8f14688",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "463036db-61c7-4679-a3aa-9acc5eb31150",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}