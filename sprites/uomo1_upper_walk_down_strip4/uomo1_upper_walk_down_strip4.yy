{
    "id": "53200e78-c485-47c7-bc88-df6fca6a76a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56f11599-e54d-421d-9f39-a3216f309ec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53200e78-c485-47c7-bc88-df6fca6a76a6",
            "compositeImage": {
                "id": "ff2df074-7973-4e53-be9c-b763b10fab5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56f11599-e54d-421d-9f39-a3216f309ec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5bcf66-5e84-4e0a-a9fe-1daa4ac7ba32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56f11599-e54d-421d-9f39-a3216f309ec3",
                    "LayerId": "4236e2d4-0af4-4b42-aee3-2646d606bc8c"
                }
            ]
        },
        {
            "id": "e97d25b4-8720-4c94-aee2-35fa838d2f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53200e78-c485-47c7-bc88-df6fca6a76a6",
            "compositeImage": {
                "id": "32ecf835-0aab-4585-a5c0-4cb94dbf3b4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97d25b4-8720-4c94-aee2-35fa838d2f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeaf30f3-58e0-47d1-89ed-ee9dc9aee94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97d25b4-8720-4c94-aee2-35fa838d2f16",
                    "LayerId": "4236e2d4-0af4-4b42-aee3-2646d606bc8c"
                }
            ]
        },
        {
            "id": "d7caa8a3-50aa-4e6e-bb44-a4b555ccd63a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53200e78-c485-47c7-bc88-df6fca6a76a6",
            "compositeImage": {
                "id": "a5bde6ab-215e-410e-97d7-e0ded2952de7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7caa8a3-50aa-4e6e-bb44-a4b555ccd63a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367dd25f-b994-4231-bc4a-6253a5524caa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7caa8a3-50aa-4e6e-bb44-a4b555ccd63a",
                    "LayerId": "4236e2d4-0af4-4b42-aee3-2646d606bc8c"
                }
            ]
        },
        {
            "id": "b6cdac90-936f-4e82-853e-1a61fc2350c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53200e78-c485-47c7-bc88-df6fca6a76a6",
            "compositeImage": {
                "id": "fd662fc4-a87a-4a38-90c9-f41eba2e13c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6cdac90-936f-4e82-853e-1a61fc2350c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39edbefc-e9c3-4dd3-a0fb-dc5b857a1476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6cdac90-936f-4e82-853e-1a61fc2350c3",
                    "LayerId": "4236e2d4-0af4-4b42-aee3-2646d606bc8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4236e2d4-0af4-4b42-aee3-2646d606bc8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53200e78-c485-47c7-bc88-df6fca6a76a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}