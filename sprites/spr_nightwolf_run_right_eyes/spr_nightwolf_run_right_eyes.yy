{
    "id": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_run_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 40,
    "bbox_right": 41,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3277711b-6725-41ea-9e7a-68865d7ea58c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "compositeImage": {
                "id": "0bce610c-f868-4b24-98ac-2b6eb839e6c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3277711b-6725-41ea-9e7a-68865d7ea58c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "878a1af3-3754-4a5c-94fc-300add200213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3277711b-6725-41ea-9e7a-68865d7ea58c",
                    "LayerId": "1668481c-4feb-47ff-8bd7-93ee0122c342"
                }
            ]
        },
        {
            "id": "47c78f4f-f9f0-46c8-9d79-2b5949f1004f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "compositeImage": {
                "id": "de220908-53e6-46d3-80f0-3cf0bdb99ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47c78f4f-f9f0-46c8-9d79-2b5949f1004f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d48d66-a302-49a8-ad70-62269fc967c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47c78f4f-f9f0-46c8-9d79-2b5949f1004f",
                    "LayerId": "1668481c-4feb-47ff-8bd7-93ee0122c342"
                }
            ]
        },
        {
            "id": "fb159696-9385-4ae1-8578-63d77cd0f943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "compositeImage": {
                "id": "7fb659ab-a714-4387-9d2c-33005459d13e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb159696-9385-4ae1-8578-63d77cd0f943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f019dc7-9552-479f-b5bb-eb0577aaeb44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb159696-9385-4ae1-8578-63d77cd0f943",
                    "LayerId": "1668481c-4feb-47ff-8bd7-93ee0122c342"
                }
            ]
        },
        {
            "id": "fbca87b1-be49-4df8-8a14-bd5a5cf0a6a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "compositeImage": {
                "id": "48287dd0-3d57-4a14-90f4-f3e05abe1e11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbca87b1-be49-4df8-8a14-bd5a5cf0a6a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b351d5e-5a01-4c29-a2ed-153589c16f20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbca87b1-be49-4df8-8a14-bd5a5cf0a6a3",
                    "LayerId": "1668481c-4feb-47ff-8bd7-93ee0122c342"
                }
            ]
        },
        {
            "id": "170802e6-f857-4440-8a94-ae18dfa7b653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "compositeImage": {
                "id": "45f65528-082b-4be6-9dbd-7ce20af95815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "170802e6-f857-4440-8a94-ae18dfa7b653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a47e34b-77ac-47e3-9b6b-2ef05535ff44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "170802e6-f857-4440-8a94-ae18dfa7b653",
                    "LayerId": "1668481c-4feb-47ff-8bd7-93ee0122c342"
                }
            ]
        },
        {
            "id": "4adecf90-638b-4ee1-9ccc-6ca33a44c4ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "compositeImage": {
                "id": "9342af0e-d21a-4489-9a98-991cb6b34169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4adecf90-638b-4ee1-9ccc-6ca33a44c4ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7f6a0a9-1ff4-4a28-b61e-798471fcb870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4adecf90-638b-4ee1-9ccc-6ca33a44c4ca",
                    "LayerId": "1668481c-4feb-47ff-8bd7-93ee0122c342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "1668481c-4feb-47ff-8bd7-93ee0122c342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31ea4073-2916-497e-9ef7-99b5da2b86a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}