{
    "id": "15d717de-5987-4420-9b88-79cc8cb8375c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3ed020f-7145-4afc-8a31-0eede12f49bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d717de-5987-4420-9b88-79cc8cb8375c",
            "compositeImage": {
                "id": "cfa84f04-5fa7-43d0-92cc-5b8b581526ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ed020f-7145-4afc-8a31-0eede12f49bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c9b9a24-c9b1-4be6-9c18-2e6c11dde1f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ed020f-7145-4afc-8a31-0eede12f49bd",
                    "LayerId": "1e86fb9a-480e-493d-982c-ac580427cc45"
                }
            ]
        },
        {
            "id": "f7dbf572-d337-4010-80c0-43f9c218ecdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d717de-5987-4420-9b88-79cc8cb8375c",
            "compositeImage": {
                "id": "7af269f9-5878-46b0-a112-a38907c57aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7dbf572-d337-4010-80c0-43f9c218ecdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22edbe46-cbe8-47c1-9974-3eb2ebd5b890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7dbf572-d337-4010-80c0-43f9c218ecdf",
                    "LayerId": "1e86fb9a-480e-493d-982c-ac580427cc45"
                }
            ]
        },
        {
            "id": "589e279d-d3fc-42e6-827e-77c225466a59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d717de-5987-4420-9b88-79cc8cb8375c",
            "compositeImage": {
                "id": "b4803055-6637-4d18-915c-eb8231b6f9ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "589e279d-d3fc-42e6-827e-77c225466a59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ecfa64f-b837-4de9-8491-d2f762fedd68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "589e279d-d3fc-42e6-827e-77c225466a59",
                    "LayerId": "1e86fb9a-480e-493d-982c-ac580427cc45"
                }
            ]
        },
        {
            "id": "2f6c2285-cbc4-41c6-ac07-90018d1fb4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15d717de-5987-4420-9b88-79cc8cb8375c",
            "compositeImage": {
                "id": "509ae3d0-bbf7-45eb-aea6-731544cc9fbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6c2285-cbc4-41c6-ac07-90018d1fb4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1390a394-ef74-4c75-8de2-4876b5d901b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6c2285-cbc4-41c6-ac07-90018d1fb4a3",
                    "LayerId": "1e86fb9a-480e-493d-982c-ac580427cc45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1e86fb9a-480e-493d-982c-ac580427cc45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15d717de-5987-4420-9b88-79cc8cb8375c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}