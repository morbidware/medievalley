{
    "id": "8b112fbb-23d5-4813-a759-a332d92976cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 39,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7efe6db-4c59-4847-aca5-2949aa61103b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b112fbb-23d5-4813-a759-a332d92976cd",
            "compositeImage": {
                "id": "bf58cc5e-ec8f-4145-ab16-c5a4f6b88890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7efe6db-4c59-4847-aca5-2949aa61103b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c045aed-9aa2-4b64-a95e-76e0cc8513a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7efe6db-4c59-4847-aca5-2949aa61103b",
                    "LayerId": "e91a18e0-34a2-466a-adde-087cc3fe8f4b"
                }
            ]
        },
        {
            "id": "ed11c2cf-c824-4163-b503-1f41d8f22fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b112fbb-23d5-4813-a759-a332d92976cd",
            "compositeImage": {
                "id": "03c5e7ff-6b60-402f-b43e-5907142d9d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed11c2cf-c824-4163-b503-1f41d8f22fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1570fc77-6bb1-441c-86aa-05561cabbb06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed11c2cf-c824-4163-b503-1f41d8f22fa3",
                    "LayerId": "e91a18e0-34a2-466a-adde-087cc3fe8f4b"
                }
            ]
        },
        {
            "id": "4426af51-38da-42c8-ae96-1a569097a8b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b112fbb-23d5-4813-a759-a332d92976cd",
            "compositeImage": {
                "id": "33a85d42-cf97-4759-b651-6e3568b6c0ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4426af51-38da-42c8-ae96-1a569097a8b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5cf580f-15ba-474a-8c94-7b4213454056",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4426af51-38da-42c8-ae96-1a569097a8b7",
                    "LayerId": "e91a18e0-34a2-466a-adde-087cc3fe8f4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e91a18e0-34a2-466a-adde-087cc3fe8f4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b112fbb-23d5-4813-a759-a332d92976cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}