{
    "id": "c16e511e-7f73-4e47-8d96-f990c22f715f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_cliff_b_hmap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b6d776e-2aa8-44fc-8059-bd4f92d004e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c16e511e-7f73-4e47-8d96-f990c22f715f",
            "compositeImage": {
                "id": "ea1c2219-7ed9-49f3-bcf5-400972bc9088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6d776e-2aa8-44fc-8059-bd4f92d004e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dff6c93-7c83-4101-a7af-ede40eeed53d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6d776e-2aa8-44fc-8059-bd4f92d004e1",
                    "LayerId": "810cbd68-e4f3-4894-8ef1-1459d9c31f63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "810cbd68-e4f3-4894-8ef1-1459d9c31f63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c16e511e-7f73-4e47-8d96-f990c22f715f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 18,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}