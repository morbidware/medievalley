{
    "id": "aca52aa6-766a-4f89-ae38-c2917f139d74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_soil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f93c48b-718c-4456-9643-65eb55afb44c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca52aa6-766a-4f89-ae38-c2917f139d74",
            "compositeImage": {
                "id": "a983df65-9a93-46f1-8ae6-d63962b1fc1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f93c48b-718c-4456-9643-65eb55afb44c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0299bf-c0ee-4e81-bcb3-df53a5484608",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f93c48b-718c-4456-9643-65eb55afb44c",
                    "LayerId": "d8b81254-9ac2-4be1-baaf-b4a193b543fd"
                }
            ]
        },
        {
            "id": "416627dd-d813-4850-888b-b0b6c34dfc6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca52aa6-766a-4f89-ae38-c2917f139d74",
            "compositeImage": {
                "id": "903e55ce-f6bf-45fc-8af9-2b607fc032e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416627dd-d813-4850-888b-b0b6c34dfc6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115a72cb-a84b-4643-8a6f-46a92af12c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416627dd-d813-4850-888b-b0b6c34dfc6b",
                    "LayerId": "d8b81254-9ac2-4be1-baaf-b4a193b543fd"
                }
            ]
        },
        {
            "id": "3cc289a9-c9fe-4617-b8ae-2a7dd95a2296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca52aa6-766a-4f89-ae38-c2917f139d74",
            "compositeImage": {
                "id": "efab188b-1c8f-4439-b2ef-da8a5068d1b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc289a9-c9fe-4617-b8ae-2a7dd95a2296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7677e384-6c4e-4c4b-9ff1-2e6a326e9b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc289a9-c9fe-4617-b8ae-2a7dd95a2296",
                    "LayerId": "d8b81254-9ac2-4be1-baaf-b4a193b543fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "d8b81254-9ac2-4be1-baaf-b4a193b543fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aca52aa6-766a-4f89-ae38-c2917f139d74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}