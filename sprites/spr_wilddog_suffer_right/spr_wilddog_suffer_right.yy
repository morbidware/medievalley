{
    "id": "0923a2bb-2ff0-4188-86ea-97e44bccef78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_suffer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 38,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7da2a010-8b75-4d39-9db6-d1b0c8394b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0923a2bb-2ff0-4188-86ea-97e44bccef78",
            "compositeImage": {
                "id": "d9960e75-959b-4087-ad0a-7ee1ea6fde0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da2a010-8b75-4d39-9db6-d1b0c8394b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a37e615a-566b-42ae-98e7-a4a6bf6da22d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da2a010-8b75-4d39-9db6-d1b0c8394b8b",
                    "LayerId": "04fe3788-314a-402e-ba82-d66d0757ec85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04fe3788-314a-402e-ba82-d66d0757ec85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0923a2bb-2ff0-4188-86ea-97e44bccef78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}