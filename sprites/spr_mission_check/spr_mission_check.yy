{
    "id": "1d16f987-e14c-4053-8af4-fa70f340bf1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mission_check",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9081c3bd-0afc-4901-9ce8-fca0d96f3e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d16f987-e14c-4053-8af4-fa70f340bf1c",
            "compositeImage": {
                "id": "32e523bc-84b6-431c-a88a-b8d35e49c604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9081c3bd-0afc-4901-9ce8-fca0d96f3e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b5b610-efd5-4eea-b355-a8573f89de08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9081c3bd-0afc-4901-9ce8-fca0d96f3e5e",
                    "LayerId": "ba4641f2-39c1-4ecc-8552-5face38bce83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "ba4641f2-39c1-4ecc-8552-5face38bce83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d16f987-e14c-4053-8af4-fa70f340bf1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}