{
    "id": "5b5514d7-6230-44cf-bdf1-57e6fe9cad19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_wheat_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d0056a7-cde7-4060-8089-632a5067e637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b5514d7-6230-44cf-bdf1-57e6fe9cad19",
            "compositeImage": {
                "id": "2a5383ee-c717-4a60-84d3-20f8c99de587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0056a7-cde7-4060-8089-632a5067e637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a66d59db-b488-430b-b7f9-c8d249acfa28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0056a7-cde7-4060-8089-632a5067e637",
                    "LayerId": "bb738811-3345-485b-b0d5-1d15f4e5c4a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bb738811-3345-485b-b0d5-1d15f4e5c4a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b5514d7-6230-44cf-bdf1-57e6fe9cad19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}