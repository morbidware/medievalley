{
    "id": "5cd58aef-66af-4838-9368-075e39a655c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96051c9b-7814-4c9e-a146-8a25fe660ce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd58aef-66af-4838-9368-075e39a655c4",
            "compositeImage": {
                "id": "3d31175b-5007-444d-8b27-e9902e75f995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96051c9b-7814-4c9e-a146-8a25fe660ce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42ee14ed-2e51-42b2-8cc1-272d1b50e208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96051c9b-7814-4c9e-a146-8a25fe660ce2",
                    "LayerId": "27ed6179-76d5-4c3c-9e49-83082b7998e9"
                }
            ]
        },
        {
            "id": "434dac53-fc27-4267-9f05-0587f0a59187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd58aef-66af-4838-9368-075e39a655c4",
            "compositeImage": {
                "id": "0fd9027b-bfce-469b-a608-dab9d0018485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "434dac53-fc27-4267-9f05-0587f0a59187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "104c5c32-18f9-484f-b092-0c9b1a79cb36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "434dac53-fc27-4267-9f05-0587f0a59187",
                    "LayerId": "27ed6179-76d5-4c3c-9e49-83082b7998e9"
                }
            ]
        },
        {
            "id": "f60b548e-34cc-4b47-9f9a-b145856117c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd58aef-66af-4838-9368-075e39a655c4",
            "compositeImage": {
                "id": "9604428c-536b-4c83-b76a-52321ad40c64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60b548e-34cc-4b47-9f9a-b145856117c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b48ba7f-8620-4176-9a15-3b7c3d1fa857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60b548e-34cc-4b47-9f9a-b145856117c1",
                    "LayerId": "27ed6179-76d5-4c3c-9e49-83082b7998e9"
                }
            ]
        },
        {
            "id": "db3a2b9a-c4fe-4d87-ac41-6446c96ce084",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cd58aef-66af-4838-9368-075e39a655c4",
            "compositeImage": {
                "id": "a2d4fc1d-cf54-4223-acf2-5c6df20ef3dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3a2b9a-c4fe-4d87-ac41-6446c96ce084",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c382001-f3fc-4a5e-a32e-c29b45fde9d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3a2b9a-c4fe-4d87-ac41-6446c96ce084",
                    "LayerId": "27ed6179-76d5-4c3c-9e49-83082b7998e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "27ed6179-76d5-4c3c-9e49-83082b7998e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cd58aef-66af-4838-9368-075e39a655c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}