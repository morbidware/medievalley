{
    "id": "785c7d23-699d-4991-aac1-b0ce29cb0805",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83305343-5316-41f6-8332-f3ad446c4733",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "785c7d23-699d-4991-aac1-b0ce29cb0805",
            "compositeImage": {
                "id": "8803dec0-4a1b-4392-b9f8-dc5898cafcd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83305343-5316-41f6-8332-f3ad446c4733",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b1b56e-6c43-4f37-99c0-47f59cc055d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83305343-5316-41f6-8332-f3ad446c4733",
                    "LayerId": "47f45ca3-e4be-4180-bef4-ecacf9297605"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47f45ca3-e4be-4180-bef4-ecacf9297605",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "785c7d23-699d-4991-aac1-b0ce29cb0805",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}