{
    "id": "1a98ab06-51c8-4f83-a7f9-588ec1a25f0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89e68116-dacc-4ce3-b119-fae354cc711f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a98ab06-51c8-4f83-a7f9-588ec1a25f0f",
            "compositeImage": {
                "id": "43ae28d4-0f40-429a-80a7-4c279d7c018a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e68116-dacc-4ce3-b119-fae354cc711f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39217462-d0a9-4fc3-975f-c40ba6986839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e68116-dacc-4ce3-b119-fae354cc711f",
                    "LayerId": "f36eb64e-f35f-48dc-b1be-eecff12ffc31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f36eb64e-f35f-48dc-b1be-eecff12ffc31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a98ab06-51c8-4f83-a7f9-588ec1a25f0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}