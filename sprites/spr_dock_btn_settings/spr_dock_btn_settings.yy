{
    "id": "e27190e2-4c18-4dcd-8762-f6faaff3030b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_settings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f50b2b79-9454-450c-bf4c-6a5d8134d8c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e27190e2-4c18-4dcd-8762-f6faaff3030b",
            "compositeImage": {
                "id": "dce93724-ef04-45cd-b9d8-ffb20db20f82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f50b2b79-9454-450c-bf4c-6a5d8134d8c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f506627-8aab-45ea-90ff-67b634d882e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f50b2b79-9454-450c-bf4c-6a5d8134d8c9",
                    "LayerId": "1dca5b73-ee24-4ccc-af7c-7a834c637de5"
                }
            ]
        },
        {
            "id": "3ca47875-ecb7-405a-8600-86d040d47c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e27190e2-4c18-4dcd-8762-f6faaff3030b",
            "compositeImage": {
                "id": "9e040daa-0647-4258-b9f3-d817aa658342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ca47875-ecb7-405a-8600-86d040d47c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86319dd9-a48f-49aa-ad89-be11f2f775e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ca47875-ecb7-405a-8600-86d040d47c09",
                    "LayerId": "1dca5b73-ee24-4ccc-af7c-7a834c637de5"
                }
            ]
        },
        {
            "id": "c0c327b3-005d-4131-8cb5-66fdf77f4a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e27190e2-4c18-4dcd-8762-f6faaff3030b",
            "compositeImage": {
                "id": "3e8c3c5e-e0f8-4034-ad01-232a1b4cb74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c327b3-005d-4131-8cb5-66fdf77f4a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2befe39-8c2b-4219-b10b-874a0917053c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c327b3-005d-4131-8cb5-66fdf77f4a3a",
                    "LayerId": "1dca5b73-ee24-4ccc-af7c-7a834c637de5"
                }
            ]
        },
        {
            "id": "2e5f150f-46a3-453b-ace3-6a553863f2ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e27190e2-4c18-4dcd-8762-f6faaff3030b",
            "compositeImage": {
                "id": "5143202e-a9ee-4694-ab1f-be7dc2701220",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5f150f-46a3-453b-ace3-6a553863f2ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e86bd0-520d-4dd1-85ab-e0f824c593d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5f150f-46a3-453b-ace3-6a553863f2ab",
                    "LayerId": "1dca5b73-ee24-4ccc-af7c-7a834c637de5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "1dca5b73-ee24-4ccc-af7c-7a834c637de5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e27190e2-4c18-4dcd-8762-f6faaff3030b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}