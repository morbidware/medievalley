{
    "id": "abc630ff-b373-48d5-8a07-a0b97bf0c932",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_screen_fade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65b9f5ce-a93c-46a6-8a63-f73c61c32944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abc630ff-b373-48d5-8a07-a0b97bf0c932",
            "compositeImage": {
                "id": "dfdb3aed-ec99-4853-9551-a9dd5dd5fd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65b9f5ce-a93c-46a6-8a63-f73c61c32944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79cb4ccd-9cf9-421d-baf5-115b88dd9508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65b9f5ce-a93c-46a6-8a63-f73c61c32944",
                    "LayerId": "d16b65f7-1e6a-4d6e-950f-25b9d960a553"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d16b65f7-1e6a-4d6e-950f-25b9d960a553",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abc630ff-b373-48d5-8a07-a0b97bf0c932",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}