{
    "id": "ed622e12-63f5-4d5f-a282-682d6c2018d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_status_life_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "258cfbc5-fcc7-429b-aecb-3015af6fdc4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed622e12-63f5-4d5f-a282-682d6c2018d7",
            "compositeImage": {
                "id": "fe6cc1bc-989b-4c46-8bb4-1cd40819a860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "258cfbc5-fcc7-429b-aecb-3015af6fdc4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18a29132-eb93-4586-980c-a1e99b943951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "258cfbc5-fcc7-429b-aecb-3015af6fdc4c",
                    "LayerId": "8192a872-a961-4042-9b15-f1ae38ee5c8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "8192a872-a961-4042-9b15-f1ae38ee5c8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed622e12-63f5-4d5f-a282-682d6c2018d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}