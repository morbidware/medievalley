{
    "id": "2fac75fd-cb89-4bbe-a439-80929431665e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_redbeans_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d47e7a99-5417-46fd-9f29-4a3c55aae703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fac75fd-cb89-4bbe-a439-80929431665e",
            "compositeImage": {
                "id": "c84ba8cd-1991-4ab6-9d70-26975ad4e5a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d47e7a99-5417-46fd-9f29-4a3c55aae703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9623484c-911b-4494-9731-3f44152e465d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d47e7a99-5417-46fd-9f29-4a3c55aae703",
                    "LayerId": "1e07f55a-50ac-49f8-b421-1bb32d1f7850"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e07f55a-50ac-49f8-b421-1bb32d1f7850",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fac75fd-cb89-4bbe-a439-80929431665e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}