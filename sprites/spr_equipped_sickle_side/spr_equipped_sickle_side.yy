{
    "id": "2709c7c8-d8bc-488a-aabb-a97e1801198c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_sickle_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "829e925c-c4d1-4891-a241-7fe9464c1351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2709c7c8-d8bc-488a-aabb-a97e1801198c",
            "compositeImage": {
                "id": "d49df7f0-323d-4d0c-b936-c5bae1d6c1d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "829e925c-c4d1-4891-a241-7fe9464c1351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e7fd898-7c3a-445b-9aaa-48aa666789e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "829e925c-c4d1-4891-a241-7fe9464c1351",
                    "LayerId": "d35fe825-af68-46bf-9049-dd02692ce8f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d35fe825-af68-46bf-9049-dd02692ce8f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2709c7c8-d8bc-488a-aabb-a97e1801198c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 1,
    "yorig": 14
}