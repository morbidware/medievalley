{
    "id": "02a9df59-1880-4857-a26d-11ad63a187de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1592485-53b7-4657-b08e-a27e75397fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "compositeImage": {
                "id": "82203c15-cc34-48c8-a534-91b1d12c3f2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1592485-53b7-4657-b08e-a27e75397fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a65b4d6-e28d-4acb-a884-49271d673a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1592485-53b7-4657-b08e-a27e75397fb6",
                    "LayerId": "85872ab7-361e-41ab-8bc9-9c59724eace6"
                }
            ]
        },
        {
            "id": "57b77813-eeea-41c7-8e83-2bbee22ddd25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "compositeImage": {
                "id": "fccb39b4-4891-4bcf-a2d5-a344d1c1415b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b77813-eeea-41c7-8e83-2bbee22ddd25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3593e539-c3c3-4e13-88b1-7aed66d06c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b77813-eeea-41c7-8e83-2bbee22ddd25",
                    "LayerId": "85872ab7-361e-41ab-8bc9-9c59724eace6"
                }
            ]
        },
        {
            "id": "e7bfc6d6-1376-41bc-8858-2f51fd58d2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "compositeImage": {
                "id": "f52fc89f-3572-4371-88b6-a2db8006353d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7bfc6d6-1376-41bc-8858-2f51fd58d2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4152ae42-5aba-4bd9-b109-3b05062ebfd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7bfc6d6-1376-41bc-8858-2f51fd58d2fc",
                    "LayerId": "85872ab7-361e-41ab-8bc9-9c59724eace6"
                }
            ]
        },
        {
            "id": "5e59a817-d294-4ee8-9fbd-5f4505221e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "compositeImage": {
                "id": "93403bf7-7381-4d0d-a4f4-4fffeb531611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e59a817-d294-4ee8-9fbd-5f4505221e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff1464c-34a4-4afe-b310-42932f0ddce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e59a817-d294-4ee8-9fbd-5f4505221e58",
                    "LayerId": "85872ab7-361e-41ab-8bc9-9c59724eace6"
                }
            ]
        },
        {
            "id": "b21052fa-8d24-4c57-82bf-08aaa9d08ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "compositeImage": {
                "id": "12e37769-d860-45dd-af16-6cdafe6e8ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b21052fa-8d24-4c57-82bf-08aaa9d08ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06a00c3e-6859-4198-985f-1d423ffbafec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b21052fa-8d24-4c57-82bf-08aaa9d08ceb",
                    "LayerId": "85872ab7-361e-41ab-8bc9-9c59724eace6"
                }
            ]
        },
        {
            "id": "57198829-1f60-4c63-8cd2-cbfe41982fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "compositeImage": {
                "id": "170a1d7a-478b-4d8b-908a-b73ff42f90d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57198829-1f60-4c63-8cd2-cbfe41982fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b432a1d7-7d3f-4913-80be-3b27c8fb6f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57198829-1f60-4c63-8cd2-cbfe41982fe3",
                    "LayerId": "85872ab7-361e-41ab-8bc9-9c59724eace6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "85872ab7-361e-41ab-8bc9-9c59724eace6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02a9df59-1880-4857-a26d-11ad63a187de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}