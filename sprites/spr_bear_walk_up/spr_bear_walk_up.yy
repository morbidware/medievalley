{
    "id": "53545722-67b7-4f0d-8d4f-360473d48102",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 14,
    "bbox_right": 33,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "136cb416-00fe-48cd-84a4-2262e1202587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53545722-67b7-4f0d-8d4f-360473d48102",
            "compositeImage": {
                "id": "857d2eea-bac2-40bb-bfee-c9d5fac7f91d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136cb416-00fe-48cd-84a4-2262e1202587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe7109e-c618-4a3d-97c4-c32a43f82da2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136cb416-00fe-48cd-84a4-2262e1202587",
                    "LayerId": "a3a1414e-4564-48c9-9e39-7ee914902377"
                }
            ]
        },
        {
            "id": "52c1e77e-2192-4ee4-8492-5f7181eb294c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53545722-67b7-4f0d-8d4f-360473d48102",
            "compositeImage": {
                "id": "467e8132-384d-4228-85f6-877405b35430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c1e77e-2192-4ee4-8492-5f7181eb294c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b531e5b-db1f-456c-8bc0-764d038babbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c1e77e-2192-4ee4-8492-5f7181eb294c",
                    "LayerId": "a3a1414e-4564-48c9-9e39-7ee914902377"
                }
            ]
        },
        {
            "id": "a26ec15b-839a-4682-ad73-cf9147972223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53545722-67b7-4f0d-8d4f-360473d48102",
            "compositeImage": {
                "id": "38589dc0-406e-4880-b41a-c248ac2a43b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26ec15b-839a-4682-ad73-cf9147972223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bf10ab2-f005-49cd-834a-42cf653cc30c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26ec15b-839a-4682-ad73-cf9147972223",
                    "LayerId": "a3a1414e-4564-48c9-9e39-7ee914902377"
                }
            ]
        },
        {
            "id": "7e28664f-98fc-4571-9ba5-3efe1fc9ca15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53545722-67b7-4f0d-8d4f-360473d48102",
            "compositeImage": {
                "id": "94103a20-1a48-4b1b-b111-a1b9436ae501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e28664f-98fc-4571-9ba5-3efe1fc9ca15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f359a4f3-ec8c-4cf9-974c-48631672d21b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e28664f-98fc-4571-9ba5-3efe1fc9ca15",
                    "LayerId": "a3a1414e-4564-48c9-9e39-7ee914902377"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a3a1414e-4564-48c9-9e39-7ee914902377",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53545722-67b7-4f0d-8d4f-360473d48102",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}