{
    "id": "149f5e57-3d56-4280-9e00-33c55bf08a50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78de0769-4b8d-46b4-bfab-21da44f1f565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149f5e57-3d56-4280-9e00-33c55bf08a50",
            "compositeImage": {
                "id": "a2609fac-e343-4714-bfd9-a56ec7f2ffb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78de0769-4b8d-46b4-bfab-21da44f1f565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f39547c-0362-4a1d-8307-7fe5ace09a36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78de0769-4b8d-46b4-bfab-21da44f1f565",
                    "LayerId": "ddb619d6-5a04-4010-a02f-73294d0b19a6"
                }
            ]
        },
        {
            "id": "c64c7841-212b-4a6b-8f3b-6e28a582290c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149f5e57-3d56-4280-9e00-33c55bf08a50",
            "compositeImage": {
                "id": "a45121f5-1a26-4aef-8e83-986623012764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c64c7841-212b-4a6b-8f3b-6e28a582290c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709d8b7a-9d72-4e97-b66d-f930c9214738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c64c7841-212b-4a6b-8f3b-6e28a582290c",
                    "LayerId": "ddb619d6-5a04-4010-a02f-73294d0b19a6"
                }
            ]
        },
        {
            "id": "92e834ed-9aa7-4400-b8c1-6f0c725b6a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149f5e57-3d56-4280-9e00-33c55bf08a50",
            "compositeImage": {
                "id": "8e5392ff-b79c-4dd1-aebb-77f041b1bf04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e834ed-9aa7-4400-b8c1-6f0c725b6a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e419de-304a-4d1d-818a-5d8a9b4865d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e834ed-9aa7-4400-b8c1-6f0c725b6a90",
                    "LayerId": "ddb619d6-5a04-4010-a02f-73294d0b19a6"
                }
            ]
        },
        {
            "id": "ef58cde4-9b83-4148-9178-06ee80e679c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149f5e57-3d56-4280-9e00-33c55bf08a50",
            "compositeImage": {
                "id": "314b516d-96e2-4c05-98f2-f4cecadcd971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef58cde4-9b83-4148-9178-06ee80e679c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d215f37f-516f-4a75-a5c0-976ea1153d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef58cde4-9b83-4148-9178-06ee80e679c7",
                    "LayerId": "ddb619d6-5a04-4010-a02f-73294d0b19a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ddb619d6-5a04-4010-a02f-73294d0b19a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "149f5e57-3d56-4280-9e00-33c55bf08a50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}