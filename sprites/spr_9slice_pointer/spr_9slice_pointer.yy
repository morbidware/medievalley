{
    "id": "a385c582-21cc-421c-beb6-25c4420d8b68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9slice_pointer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adcdb2ea-54b1-4320-be7f-4707a16d160c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "baadcd3e-c42f-4746-b88d-ccb9dcd78c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adcdb2ea-54b1-4320-be7f-4707a16d160c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e726a3f-9f01-4fa5-a27b-5d8b80633886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcdb2ea-54b1-4320-be7f-4707a16d160c",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "8e2c44df-a480-4794-aaa1-e28feffaf1af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adcdb2ea-54b1-4320-be7f-4707a16d160c",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "4074b397-8954-4cac-9b20-930147692cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "640dcfd1-7af8-4c55-a0fe-5633b816a417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4074b397-8954-4cac-9b20-930147692cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c61a11a-8b07-4f0c-89cd-defb8846ef45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4074b397-8954-4cac-9b20-930147692cb8",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "b46470e9-ceca-4848-8b35-d9bcb23d9273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4074b397-8954-4cac-9b20-930147692cb8",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "2420777e-f5b0-4aa5-8ac6-18d531f374fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "b3d960c1-5821-4c6e-ac44-15a1934c9446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2420777e-f5b0-4aa5-8ac6-18d531f374fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2cf886-1568-4179-bf2d-6728f9822407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2420777e-f5b0-4aa5-8ac6-18d531f374fb",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "d08d9ce4-bc53-40f7-be39-385ddb388713",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2420777e-f5b0-4aa5-8ac6-18d531f374fb",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "3196399f-e62e-4d82-a4ff-d75069013a0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "c9eaf6a4-ce36-462c-bdf4-3921fadf3776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3196399f-e62e-4d82-a4ff-d75069013a0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce26989-9dc5-4581-983a-a956658c43f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3196399f-e62e-4d82-a4ff-d75069013a0b",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "6189b7a3-3adb-4f84-9c26-b7c160e067c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3196399f-e62e-4d82-a4ff-d75069013a0b",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "68d2841b-70af-4e06-bf28-fc325066bd4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "5fba90db-9f9c-480e-b353-61a1b4fef04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d2841b-70af-4e06-bf28-fc325066bd4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7aa5abd8-132a-4e0d-be8f-2294630eda1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d2841b-70af-4e06-bf28-fc325066bd4e",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "d9dc7d97-0dd1-4750-904c-af4f2ea3bdce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d2841b-70af-4e06-bf28-fc325066bd4e",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "e5b31f9f-62b3-4b89-98ee-dce988c04f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "6469ab26-055e-4aeb-9d5f-6932256b04b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b31f9f-62b3-4b89-98ee-dce988c04f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aab23239-570e-42c2-9e6a-7b2216ae0f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b31f9f-62b3-4b89-98ee-dce988c04f99",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "0c44d78f-e685-4953-a50f-55d51b106c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b31f9f-62b3-4b89-98ee-dce988c04f99",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "5dd79998-a1ae-4ede-9dc4-37a483a861a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "cc6df76e-ee70-4fe2-9163-9e7a048f253b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd79998-a1ae-4ede-9dc4-37a483a861a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6045d5a2-ff60-4f35-861c-fac5ae05faf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd79998-a1ae-4ede-9dc4-37a483a861a5",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "ab126e6b-9365-46de-9d66-9412a7ce879b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd79998-a1ae-4ede-9dc4-37a483a861a5",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "2e44544d-f8dc-4b4d-8988-3fa8a7d39908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "beeabf9a-8e01-4b8c-b653-7592de83c5de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e44544d-f8dc-4b4d-8988-3fa8a7d39908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "514f894e-7620-46c0-9653-0c1779a1beb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e44544d-f8dc-4b4d-8988-3fa8a7d39908",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "6ff1aaa1-c67a-43a0-a7cd-8f54bc2ebb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e44544d-f8dc-4b4d-8988-3fa8a7d39908",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        },
        {
            "id": "605a8869-52ae-4224-97a3-b9aaa70573e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "compositeImage": {
                "id": "3ebfa1ef-64e7-4ddd-b3e2-c6e7635432a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "605a8869-52ae-4224-97a3-b9aaa70573e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cf4a389-c008-4274-ab3f-fb232398b09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605a8869-52ae-4224-97a3-b9aaa70573e9",
                    "LayerId": "5004ed47-31b3-45a3-b771-6a5c0d3c3573"
                },
                {
                    "id": "b3d251f6-7c80-464b-b143-d1ea04ff1189",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605a8869-52ae-4224-97a3-b9aaa70573e9",
                    "LayerId": "e1934a60-69bc-4490-aa60-80dd19c757d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5004ed47-31b3-45a3-b771-6a5c0d3c3573",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e1934a60-69bc-4490-aa60-80dd19c757d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a385c582-21cc-421c-beb6-25c4420d8b68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}