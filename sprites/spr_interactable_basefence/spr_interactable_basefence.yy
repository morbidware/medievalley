{
    "id": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_basefence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e98635ba-5bc1-45c9-87a1-744dbba6d23d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "0c13cd3c-98ad-4139-a77d-294fb5653171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98635ba-5bc1-45c9-87a1-744dbba6d23d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "395a90df-0219-48f7-bc6a-69bae5d20a9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98635ba-5bc1-45c9-87a1-744dbba6d23d",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "a70c06ab-6dec-47d6-bdcb-232a1b3d8571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "e564f1b4-783d-465c-8d32-a66c22a7711a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a70c06ab-6dec-47d6-bdcb-232a1b3d8571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58683112-8f2a-4edc-8680-7f8267d2c99c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a70c06ab-6dec-47d6-bdcb-232a1b3d8571",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "7ab39bb7-291e-45da-af70-e9a952e91bae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "44914d2c-7fb2-4ca6-bca4-00ee42ad1a14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab39bb7-291e-45da-af70-e9a952e91bae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeac10b2-5746-4b04-9205-5de8ca20a634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab39bb7-291e-45da-af70-e9a952e91bae",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "af3eda6e-4e70-4e24-a13e-85bec73d2a48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "6bdf7d3a-85f5-49b0-b27b-e565c43909b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3eda6e-4e70-4e24-a13e-85bec73d2a48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f85919-a057-41e7-b342-d1383d094fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3eda6e-4e70-4e24-a13e-85bec73d2a48",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "8c11054f-258a-4e1b-9d54-b806693c16f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "968f1203-0841-4bfc-84f6-81c88bb628c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c11054f-258a-4e1b-9d54-b806693c16f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "777ec08f-406e-4b4c-8b97-2a14b558ef40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c11054f-258a-4e1b-9d54-b806693c16f6",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "772fa8bc-0011-4c48-9e1e-53068dcd8be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "00ab5b9e-1bb4-46bb-978c-3920f1cc4ae7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772fa8bc-0011-4c48-9e1e-53068dcd8be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a58671b-6bd2-4fc7-a1e8-c4c68b6de868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772fa8bc-0011-4c48-9e1e-53068dcd8be6",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "cc830b10-dc98-4869-a23e-91e7e86ea09b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "e1db7d35-60d9-4a8b-b992-17109dc51f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc830b10-dc98-4869-a23e-91e7e86ea09b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecaf52cc-8202-4c33-a457-1d8e101ab792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc830b10-dc98-4869-a23e-91e7e86ea09b",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "55052a74-2ab4-4ca3-a16a-b46f4bda1197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "8118fe4c-bfec-45bb-8be1-4b1b011b8f21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55052a74-2ab4-4ca3-a16a-b46f4bda1197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e992d50c-500c-4a0c-ad02-0d8354c574c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55052a74-2ab4-4ca3-a16a-b46f4bda1197",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "ed6bae1e-ab07-4c5f-be74-bbaf6d6d216f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "994f2ce4-5ddf-4978-b078-216f33ce4d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6bae1e-ab07-4c5f-be74-bbaf6d6d216f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ee0ac7-3d78-4c22-a1ff-46c1cbcd9f52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6bae1e-ab07-4c5f-be74-bbaf6d6d216f",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "2ba8bc11-5782-4148-91a6-4b3fc3829ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "83b94184-55e1-4f30-bb6c-73192b7dcba1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ba8bc11-5782-4148-91a6-4b3fc3829ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601bb183-815b-4b95-8de4-762c13098e21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ba8bc11-5782-4148-91a6-4b3fc3829ce4",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "a4c9314c-73d2-4c03-8971-0876e8d7055f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "d0482943-d223-404c-8ae4-f413a03feb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c9314c-73d2-4c03-8971-0876e8d7055f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b27c2e6-a9df-4844-a439-295fce57c260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c9314c-73d2-4c03-8971-0876e8d7055f",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "99cc134b-b3e2-4b52-b7d8-d32f2cfa56d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "5cfa16d3-1686-4b45-b05e-258ff9a93dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cc134b-b3e2-4b52-b7d8-d32f2cfa56d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ace8618-d7e1-4cd2-8847-7580df147b57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cc134b-b3e2-4b52-b7d8-d32f2cfa56d4",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "aa82f678-2e8b-4b6f-886f-180792a05a41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "bd8ca883-cd6c-4b88-ab89-78c08aa56442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa82f678-2e8b-4b6f-886f-180792a05a41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3c11dd-655f-458b-bd80-6c42451460b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa82f678-2e8b-4b6f-886f-180792a05a41",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "3682755e-827e-48e2-a0ab-48340996fda9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "9f9b0ec9-e42b-4ce9-9a9e-0dbdf6dbbc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3682755e-827e-48e2-a0ab-48340996fda9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8589a1-5741-4c58-8c2c-62d6c6c52140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3682755e-827e-48e2-a0ab-48340996fda9",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "7446419f-a811-480c-83eb-ab574244295e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "9a61822d-c3d5-445c-b1ce-5059e4a6a437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7446419f-a811-480c-83eb-ab574244295e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dabb550-3d9d-4dd2-aba1-36f7d9cf3255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7446419f-a811-480c-83eb-ab574244295e",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        },
        {
            "id": "ab33024c-6519-4063-a1e0-8c24b4439807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "compositeImage": {
                "id": "2938438e-6460-4f84-ad94-e053c066d91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab33024c-6519-4063-a1e0-8c24b4439807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e9b6905-112f-4773-b76a-fea733baa644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab33024c-6519-4063-a1e0-8c24b4439807",
                    "LayerId": "06823ddc-4152-42b7-9fde-78a50ea9aa86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "06823ddc-4152-42b7-9fde-78a50ea9aa86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb3c2d61-c272-465a-b5a5-4b9c03bed2ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 29
}