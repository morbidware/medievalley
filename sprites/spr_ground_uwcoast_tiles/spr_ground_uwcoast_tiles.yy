{
    "id": "bef422bc-8ecf-4405-a98d-34218aebf616",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_uwcoast_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "802b2907-a0df-4682-8e53-1e7d6cf9c3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "bd72cee7-4667-430d-9979-773f33a7d1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "802b2907-a0df-4682-8e53-1e7d6cf9c3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c17308-2dd5-459a-81c6-ff3e0ec25076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "802b2907-a0df-4682-8e53-1e7d6cf9c3be",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "e7e4c30e-8b66-4415-8e53-1093675539a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "f2afd3d5-2a44-40bc-8cf3-f44fb8826d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e4c30e-8b66-4415-8e53-1093675539a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a518e3a-d3e3-49d5-a77a-2251f4359b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e4c30e-8b66-4415-8e53-1093675539a1",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "f10908e6-ac0a-4a78-b103-d27deb950c31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "4f5b1ed6-f8b7-4f7b-9317-26ce719fc6a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10908e6-ac0a-4a78-b103-d27deb950c31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "252c008f-89e2-4e38-8994-d3d21f388459",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10908e6-ac0a-4a78-b103-d27deb950c31",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "d33c02fd-37a6-405f-a576-a9ca40ef78a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "34f7d3cb-9be8-4ab3-908f-161501823c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33c02fd-37a6-405f-a576-a9ca40ef78a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb18b486-3cdb-47e5-aa9b-d0b961d70ffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33c02fd-37a6-405f-a576-a9ca40ef78a4",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "bda2494a-d63a-4b5d-b15d-8e92e1b1a673",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "fa841ff0-38d7-4fdd-ab66-7dc4b1b7c30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda2494a-d63a-4b5d-b15d-8e92e1b1a673",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ae4e8f-020f-4b97-b33d-ccaae5dcf29c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda2494a-d63a-4b5d-b15d-8e92e1b1a673",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "e03ff1cb-170d-4649-812a-4bdf6edf54b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "ff8722d9-4979-4589-97e7-e22f44f482a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e03ff1cb-170d-4649-812a-4bdf6edf54b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c9357c-92e8-4ae6-a957-b722970bdfe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e03ff1cb-170d-4649-812a-4bdf6edf54b2",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "e6012221-9392-45cb-bc9a-1356d5dc4048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "3c6120b1-b678-47b9-8e18-386f4f9c2b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6012221-9392-45cb-bc9a-1356d5dc4048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f64e1f1-bd9f-4f73-afdf-e5d30b36cf59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6012221-9392-45cb-bc9a-1356d5dc4048",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "6abfaada-9ee8-48d2-8e0f-18e9c4043842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "48d5f3bc-bfe5-49b7-a61f-ee3f9c3e8102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abfaada-9ee8-48d2-8e0f-18e9c4043842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66319a8c-bdee-4571-a69b-643e40d8ab57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abfaada-9ee8-48d2-8e0f-18e9c4043842",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "1b9afaf0-0221-4168-a77f-20b1235b505b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "5c7b3433-eff5-40e4-a6f9-914453edacb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b9afaf0-0221-4168-a77f-20b1235b505b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dea762c-131f-42be-bd05-3d5f915a10e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b9afaf0-0221-4168-a77f-20b1235b505b",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "18d8a996-4634-4cb2-882a-ec848ef436e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "a84a8585-49c1-4516-9c60-6270a0f5ed2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18d8a996-4634-4cb2-882a-ec848ef436e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d363662-09c4-456b-9035-6dcaa4fbfa4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18d8a996-4634-4cb2-882a-ec848ef436e3",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "666b5f36-aa20-4db0-a099-53cddb945c11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "2d2c4e5f-22a5-4a19-8b68-aa1d89a2f6f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "666b5f36-aa20-4db0-a099-53cddb945c11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a9f71f8-dc3a-4a91-8326-72f2cc3cc2a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "666b5f36-aa20-4db0-a099-53cddb945c11",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "7858699f-28ed-4b15-8206-7d0e89ee8ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "fef0910e-b42a-48e6-b68f-a0fbb8435feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7858699f-28ed-4b15-8206-7d0e89ee8ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d7ead23-f888-4fce-81ba-3bf8b58be08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7858699f-28ed-4b15-8206-7d0e89ee8ccc",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "8189b05a-f44f-49c4-ad47-b3ae7047f8f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "71cd4421-7d72-440b-8f0c-23a93954f3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8189b05a-f44f-49c4-ad47-b3ae7047f8f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c5ae05-d0a7-4d17-87a7-2177fbb1d77e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8189b05a-f44f-49c4-ad47-b3ae7047f8f4",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "23ba1d75-ca53-4286-a323-1a3a293b036e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "9ec70116-963b-4878-93c0-5f96f93bfae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23ba1d75-ca53-4286-a323-1a3a293b036e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "411ff986-c7bb-4ff5-886f-378060befaa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23ba1d75-ca53-4286-a323-1a3a293b036e",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "b30dab45-30b4-4ccf-9927-a815747d46a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "73e42b79-c9e8-4587-82c3-0b7e3de5e005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b30dab45-30b4-4ccf-9927-a815747d46a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9a946a-2f4d-4248-8ce3-e5c7b707746f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b30dab45-30b4-4ccf-9927-a815747d46a1",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "e84b10e9-2ebc-4493-9eb8-34e45e67c129",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "f80900b8-3bd9-4d20-af70-e408222d3bc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84b10e9-2ebc-4493-9eb8-34e45e67c129",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3288432-05bc-4e3f-8159-a06b0fa2cfce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84b10e9-2ebc-4493-9eb8-34e45e67c129",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "257ec4ad-372a-4f46-b016-94134af5acea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "c90db3d5-6d86-4fba-bd33-3185038ca215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "257ec4ad-372a-4f46-b016-94134af5acea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ca5d6a-3896-4e2a-8344-06d4986f61f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "257ec4ad-372a-4f46-b016-94134af5acea",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "ec198189-3f24-4c38-8666-d9966c512754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "a212e4e0-5c8b-46ac-ad61-3cb759793e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec198189-3f24-4c38-8666-d9966c512754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08f6fd9f-0ede-41d2-9f23-0ab145101c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec198189-3f24-4c38-8666-d9966c512754",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "57eb1eab-8e74-4c81-8c33-51286d0dfb1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "c73c23eb-7575-4e30-a7d8-92df451915a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57eb1eab-8e74-4c81-8c33-51286d0dfb1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1357d7b-a49b-4b08-8925-9882528841a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57eb1eab-8e74-4c81-8c33-51286d0dfb1f",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "f2530211-2d5d-44c8-8a56-e2605fc0cc45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "e87b9d54-4cae-4a5b-9121-5815979eb432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2530211-2d5d-44c8-8a56-e2605fc0cc45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7eb8a2-9adf-4a2f-8ff0-725b685c45f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2530211-2d5d-44c8-8a56-e2605fc0cc45",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "4191a832-342d-4288-8200-88a1161e80a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "5eabd9ed-133d-4882-be0a-c594f4deb9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4191a832-342d-4288-8200-88a1161e80a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8cd7016-c079-40ca-b398-82310edeb306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4191a832-342d-4288-8200-88a1161e80a8",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "a16b2583-15a1-44bf-844e-7f2858ae5906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "e5c4137e-1014-40dd-b00c-7da1ffd6d02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16b2583-15a1-44bf-844e-7f2858ae5906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91d9c4f4-4f20-4db7-a740-cce950729f9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16b2583-15a1-44bf-844e-7f2858ae5906",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "310714c2-1afd-4959-949a-433d93d1dbbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "5098c680-82b8-440c-9b09-e7c0cf20e9b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310714c2-1afd-4959-949a-433d93d1dbbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6cb3926-37cc-4736-892d-7c005c999de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310714c2-1afd-4959-949a-433d93d1dbbc",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "7d8303b6-3b9d-4f30-98eb-85e229999b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "cbbca9bd-b61a-4551-9483-f534e8ec80d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8303b6-3b9d-4f30-98eb-85e229999b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72bb0812-bea5-41e5-b2ce-3b09ea49729b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8303b6-3b9d-4f30-98eb-85e229999b30",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "87bfe767-3fa8-4d71-9eb3-276de486667e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "ddf04c78-d1fd-4e21-b1bd-032fea2ba2ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bfe767-3fa8-4d71-9eb3-276de486667e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ceac58-ad44-40d6-94c0-714f0e41d84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bfe767-3fa8-4d71-9eb3-276de486667e",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "b4c5da3a-f57e-46be-b5f3-fdd7d3ac7dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "6c652fa5-6c7d-48f7-ac36-7eff8c15bd65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c5da3a-f57e-46be-b5f3-fdd7d3ac7dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e39e8f8-709c-463f-b61b-76985acb5925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c5da3a-f57e-46be-b5f3-fdd7d3ac7dd5",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "4d704439-c02f-4ac9-b6cf-9e1c4d5fa984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "bafa0017-71bc-4171-ad61-6bd960eba907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d704439-c02f-4ac9-b6cf-9e1c4d5fa984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba21f392-2671-421f-9dd6-669df0d70e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d704439-c02f-4ac9-b6cf-9e1c4d5fa984",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "9ad09028-fa5c-47e0-9109-7723c556a276",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "7c16804e-db68-4686-b0b0-42dd38e206db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad09028-fa5c-47e0-9109-7723c556a276",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2277944a-20c2-4b25-8f18-56e7fa94d7ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad09028-fa5c-47e0-9109-7723c556a276",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "8d7c25b4-091e-4c98-9953-9bafb41c708f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "407b5222-3172-4d50-8eaf-91f0dc3c52ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d7c25b4-091e-4c98-9953-9bafb41c708f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce94c858-59fe-43de-af6c-60bf88e070fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d7c25b4-091e-4c98-9953-9bafb41c708f",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "404bedc8-9f99-423c-b7ef-d946c6aea76c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "641a6ba7-dbf4-4f3a-a33e-6e84b913e0dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "404bedc8-9f99-423c-b7ef-d946c6aea76c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ecd4d2c-a35a-465f-811e-04c26f15fb18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "404bedc8-9f99-423c-b7ef-d946c6aea76c",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "66b0be10-8fbd-4915-a53e-6c7bdee9744c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "2c592a8a-a25d-41b2-bd6b-73e5ea75f7fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b0be10-8fbd-4915-a53e-6c7bdee9744c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b4de714-ae8b-4a82-8395-72f40fb906c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b0be10-8fbd-4915-a53e-6c7bdee9744c",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "211fd335-3349-44a4-a417-2d4719487bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "259fc98b-2e06-40aa-a64d-f21a0cba3579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "211fd335-3349-44a4-a417-2d4719487bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547f13e9-0384-43a7-9349-868c2d3c80d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "211fd335-3349-44a4-a417-2d4719487bb2",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "a37e09ce-3ea6-48fc-9b3c-8f13f08cf543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "945ad8b6-c385-48de-93eb-05eed4d6181c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a37e09ce-3ea6-48fc-9b3c-8f13f08cf543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd877a6-453e-4739-bbde-c8b58219a4a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a37e09ce-3ea6-48fc-9b3c-8f13f08cf543",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "7fa38c8e-d1e3-4b60-9139-559bcef73c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "17fef438-5913-4f4e-b85e-7b89047f854f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa38c8e-d1e3-4b60-9139-559bcef73c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6772ff5-dcd8-4dd2-a175-0918f7aef36e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa38c8e-d1e3-4b60-9139-559bcef73c43",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "a67b9221-eb3a-43e6-a88f-019dfdb254ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "4616a3bf-3806-4228-b91c-c8cbb41eaf79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a67b9221-eb3a-43e6-a88f-019dfdb254ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799baebd-4573-417d-8226-396f4d2d28e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a67b9221-eb3a-43e6-a88f-019dfdb254ae",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "b2fc4123-6423-4ab2-a039-2490f066fa17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "f8330014-830a-4ad1-b680-aa3b946124b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2fc4123-6423-4ab2-a039-2490f066fa17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b348b4a8-5503-4a47-9679-482d59281cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2fc4123-6423-4ab2-a039-2490f066fa17",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "0d47393b-da66-4c6a-aa41-e66eed6b6d59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "0f852d1c-7ffe-458a-9846-ddf345c8624c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d47393b-da66-4c6a-aa41-e66eed6b6d59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b9de81-35d7-41e7-bce2-3d521225616e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d47393b-da66-4c6a-aa41-e66eed6b6d59",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "3192c71e-5925-4b61-807a-da9c4b4bbbca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "419045fb-eb38-4517-b319-7f41aa337035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3192c71e-5925-4b61-807a-da9c4b4bbbca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bae1987e-b0ff-4812-b948-6f24498089a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3192c71e-5925-4b61-807a-da9c4b4bbbca",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "87869109-070c-4585-9b2f-ce96bcf6b587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "a1115b34-6508-4d1a-be12-810bcf292d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87869109-070c-4585-9b2f-ce96bcf6b587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83fc884f-f7ac-4bcf-9e15-3e13f3bc432b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87869109-070c-4585-9b2f-ce96bcf6b587",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "e14f641d-fbea-4ea5-9e18-89f4fd4a96e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "f36802cc-ee6a-4094-86a6-ffe7be5bf5fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e14f641d-fbea-4ea5-9e18-89f4fd4a96e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4055bb6e-f538-44dd-a2ab-7d36f20e5a1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e14f641d-fbea-4ea5-9e18-89f4fd4a96e7",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "52cdfb1b-03a4-4d39-a915-09a79b7f9c0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "93e4dae4-0b9b-4ab1-8e25-88e4f5bbcade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52cdfb1b-03a4-4d39-a915-09a79b7f9c0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46cc598c-1cdc-4475-91bd-0667a83a5daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52cdfb1b-03a4-4d39-a915-09a79b7f9c0d",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "eb906d0d-7a61-4231-91c2-df60f15b1ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "87efe07f-b82d-4f3c-8764-049e50f926ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb906d0d-7a61-4231-91c2-df60f15b1ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9016ce4f-bc52-4042-bf36-890645583d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb906d0d-7a61-4231-91c2-df60f15b1ac2",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "64189037-0012-46f7-87be-1b0e9367b560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "128b3d16-693c-41d4-bdaf-75b6dbacf17c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64189037-0012-46f7-87be-1b0e9367b560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e511c224-e42f-4056-9bd5-442573ea2b52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64189037-0012-46f7-87be-1b0e9367b560",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "02b49e61-fa70-4acb-9bd8-c1d40e921198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "b19c8ad7-3b7d-4823-a670-4b3effb4d9ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b49e61-fa70-4acb-9bd8-c1d40e921198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4b579d-3bb4-4fbb-ad05-69ba311af8d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b49e61-fa70-4acb-9bd8-c1d40e921198",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "9aba3036-e8d9-4084-9d1e-6c5b7e2d39bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "7f2a380e-b394-4beb-9bdd-56add754c1a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aba3036-e8d9-4084-9d1e-6c5b7e2d39bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a448966-92c2-4c76-9b4d-8dd5d3e40b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aba3036-e8d9-4084-9d1e-6c5b7e2d39bb",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "ee4ec90d-e947-4f15-b769-ee7987ac38a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "8419bc87-edce-4337-afd7-4a4acda49c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee4ec90d-e947-4f15-b769-ee7987ac38a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea96ed3-4bc4-4d8f-aeb2-2871cb9b2f13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee4ec90d-e947-4f15-b769-ee7987ac38a2",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "9b58020b-8460-410b-a682-6c918a136b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "066558b0-99b3-42dc-967c-181aa6ad6a60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b58020b-8460-410b-a682-6c918a136b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88de9f57-fb5d-4afb-bc07-47f35d01fa56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b58020b-8460-410b-a682-6c918a136b8c",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "c101ee55-48c1-4c8e-beb0-38eb7ef75cdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "13abf5da-efba-4a91-b318-c7e996fab0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c101ee55-48c1-4c8e-beb0-38eb7ef75cdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d29b64-36c8-48b6-a719-4c01bacfe0e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c101ee55-48c1-4c8e-beb0-38eb7ef75cdf",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        },
        {
            "id": "a386ff4a-c976-418a-b20d-11b94ae72fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "compositeImage": {
                "id": "03e305e6-da9d-459c-9227-cd191ce25ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a386ff4a-c976-418a-b20d-11b94ae72fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4efc22-98f0-4b47-b919-f637b77806a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a386ff4a-c976-418a-b20d-11b94ae72fbf",
                    "LayerId": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1fbb051f-f067-4b88-b37a-3bab4fb4e7f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bef422bc-8ecf-4405-a98d-34218aebf616",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 24
}