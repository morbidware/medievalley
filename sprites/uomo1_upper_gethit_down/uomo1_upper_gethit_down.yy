{
    "id": "b93546aa-1a8a-4374-b078-7e9f22dc30cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e60f1641-6da2-4fe2-9381-08bb43f47e10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b93546aa-1a8a-4374-b078-7e9f22dc30cc",
            "compositeImage": {
                "id": "4f8d35ea-e808-4f2e-ab6e-fffe4ad336ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e60f1641-6da2-4fe2-9381-08bb43f47e10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72860d98-af15-4c70-92f3-c659024d293b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e60f1641-6da2-4fe2-9381-08bb43f47e10",
                    "LayerId": "4518b20b-2431-489d-bf01-8df39dd34367"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4518b20b-2431-489d-bf01-8df39dd34367",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b93546aa-1a8a-4374-b078-7e9f22dc30cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}