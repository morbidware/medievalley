{
    "id": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_hammer_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f533f77-8a62-4a07-b18a-f096435deaed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "compositeImage": {
                "id": "9594b288-00d4-4977-adb9-9bc080cfa98a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f533f77-8a62-4a07-b18a-f096435deaed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11df2712-a51e-4206-aadc-e159f75475db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f533f77-8a62-4a07-b18a-f096435deaed",
                    "LayerId": "0e321914-bc26-49a5-8d61-7ca92796c519"
                }
            ]
        },
        {
            "id": "e6ffa3a8-cedc-4bfb-9f19-afbef0a6d681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "compositeImage": {
                "id": "80d6db44-796d-44a5-9e62-0a6f3f1cd197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ffa3a8-cedc-4bfb-9f19-afbef0a6d681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b88c8e6-469e-45c2-8530-9ef1daf0d0b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ffa3a8-cedc-4bfb-9f19-afbef0a6d681",
                    "LayerId": "0e321914-bc26-49a5-8d61-7ca92796c519"
                }
            ]
        },
        {
            "id": "8b0a6f7d-5133-4cae-b153-5212f30913bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "compositeImage": {
                "id": "47c70ead-a7fc-401b-a390-847354e7ce10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0a6f7d-5133-4cae-b153-5212f30913bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecfd74b5-a1ba-4618-be51-3bd0cd1004a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0a6f7d-5133-4cae-b153-5212f30913bc",
                    "LayerId": "0e321914-bc26-49a5-8d61-7ca92796c519"
                }
            ]
        },
        {
            "id": "a83699de-ae1b-4551-8b96-94d7a5b27a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "compositeImage": {
                "id": "98f75e5c-66bc-4aa7-a738-ea3e5db8df80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a83699de-ae1b-4551-8b96-94d7a5b27a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b831025c-4ec5-452b-9374-36344efcaeea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a83699de-ae1b-4551-8b96-94d7a5b27a16",
                    "LayerId": "0e321914-bc26-49a5-8d61-7ca92796c519"
                }
            ]
        },
        {
            "id": "cc7659d4-7cbd-4873-82d8-42071339242a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "compositeImage": {
                "id": "b827fe55-0d16-473d-bccd-284d66539b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7659d4-7cbd-4873-82d8-42071339242a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121421db-57d7-41d5-94b6-71f37d822ed2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7659d4-7cbd-4873-82d8-42071339242a",
                    "LayerId": "0e321914-bc26-49a5-8d61-7ca92796c519"
                }
            ]
        },
        {
            "id": "647b1b8c-be2b-48cc-8324-5c127a4a5ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "compositeImage": {
                "id": "b280f82d-7dd6-48ef-b9fc-82334f410711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "647b1b8c-be2b-48cc-8324-5c127a4a5ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f84be8ee-b498-49b7-94ee-7e189dcc74ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "647b1b8c-be2b-48cc-8324-5c127a4a5ecc",
                    "LayerId": "0e321914-bc26-49a5-8d61-7ca92796c519"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0e321914-bc26-49a5-8d61-7ca92796c519",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03edf3e4-fad4-402b-8d9a-e59bc7ce3ec3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}