{
    "id": "e9459517-9af5-418b-a7c0-07dffc802dcd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_tools",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dda884a9-e652-4e07-8648-c401fdfb1d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9459517-9af5-418b-a7c0-07dffc802dcd",
            "compositeImage": {
                "id": "d134e07c-2047-4d5e-b6b8-dcecfcd8c75e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda884a9-e652-4e07-8648-c401fdfb1d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8eabba-8a00-43d5-89a4-a94fccf6322a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda884a9-e652-4e07-8648-c401fdfb1d4d",
                    "LayerId": "4b8645f3-5d79-4f58-800e-2fad0b2cd883"
                }
            ]
        },
        {
            "id": "94780751-d65d-4235-8d4a-5b9ae36fe16d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9459517-9af5-418b-a7c0-07dffc802dcd",
            "compositeImage": {
                "id": "f98cbeb1-0179-409f-8bcf-21972ef2b63d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94780751-d65d-4235-8d4a-5b9ae36fe16d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b01aef9-f275-4c28-a379-c3ea4586871e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94780751-d65d-4235-8d4a-5b9ae36fe16d",
                    "LayerId": "4b8645f3-5d79-4f58-800e-2fad0b2cd883"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "4b8645f3-5d79-4f58-800e-2fad0b2cd883",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9459517-9af5-418b-a7c0-07dffc802dcd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}