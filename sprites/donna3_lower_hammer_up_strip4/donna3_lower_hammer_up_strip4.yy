{
    "id": "191bb03c-cc11-4bb7-a8b2-8cc21708edcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7148e91-ae22-4f32-8f7e-dd834b89766a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191bb03c-cc11-4bb7-a8b2-8cc21708edcc",
            "compositeImage": {
                "id": "b2e44fd3-7a8d-4b8c-a971-a7f196f3b37c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7148e91-ae22-4f32-8f7e-dd834b89766a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08844ceb-be6b-4dae-b230-24c82ec30a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7148e91-ae22-4f32-8f7e-dd834b89766a",
                    "LayerId": "2d857de7-ccc6-4313-adda-a8594d9dd593"
                }
            ]
        },
        {
            "id": "0cc3470b-5c8d-4f97-8d63-7ed54027f051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191bb03c-cc11-4bb7-a8b2-8cc21708edcc",
            "compositeImage": {
                "id": "582eb0f1-856b-46d5-b44b-e5edca064600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cc3470b-5c8d-4f97-8d63-7ed54027f051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d167b063-2bf4-41a1-9fa5-4ccd6a3511fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cc3470b-5c8d-4f97-8d63-7ed54027f051",
                    "LayerId": "2d857de7-ccc6-4313-adda-a8594d9dd593"
                }
            ]
        },
        {
            "id": "220579e5-6f0c-4668-a263-05086106cdd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191bb03c-cc11-4bb7-a8b2-8cc21708edcc",
            "compositeImage": {
                "id": "2d4550e7-7a6f-4f22-a2f4-e181afce8a9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220579e5-6f0c-4668-a263-05086106cdd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c033849-aaa6-47a3-aaf7-a3e4a5a0e555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220579e5-6f0c-4668-a263-05086106cdd1",
                    "LayerId": "2d857de7-ccc6-4313-adda-a8594d9dd593"
                }
            ]
        },
        {
            "id": "5801c3de-0ffe-4905-af6c-25f6cbdc20ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "191bb03c-cc11-4bb7-a8b2-8cc21708edcc",
            "compositeImage": {
                "id": "d4e21255-8365-45bb-816b-c0f31c9ac7e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5801c3de-0ffe-4905-af6c-25f6cbdc20ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f0aecde-9204-4067-84f8-6f71971a4da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5801c3de-0ffe-4905-af6c-25f6cbdc20ec",
                    "LayerId": "2d857de7-ccc6-4313-adda-a8594d9dd593"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2d857de7-ccc6-4313-adda-a8594d9dd593",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "191bb03c-cc11-4bb7-a8b2-8cc21708edcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}