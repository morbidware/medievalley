{
    "id": "a1f6f98d-9b67-409d-a792-529e858155f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_hop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06840ed7-57c3-4a89-b4b9-a85431b3e5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1f6f98d-9b67-409d-a792-529e858155f7",
            "compositeImage": {
                "id": "389451dc-d695-414b-be15-a81e27ebc7fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06840ed7-57c3-4a89-b4b9-a85431b3e5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eacf0f7-b7f4-4421-9f7f-fe30d700ea5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06840ed7-57c3-4a89-b4b9-a85431b3e5b5",
                    "LayerId": "2d33bf69-4191-4550-8852-2c1419699a4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2d33bf69-4191-4550-8852-2c1419699a4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1f6f98d-9b67-409d-a792-529e858155f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}