{
    "id": "117ad46a-cf8c-42cc-9d2f-2ef1fe9498e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fee29b5a-a6d5-4993-8bee-995d582d6463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117ad46a-cf8c-42cc-9d2f-2ef1fe9498e2",
            "compositeImage": {
                "id": "8881b07c-9a3e-4557-9b32-8da13c9a8b95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee29b5a-a6d5-4993-8bee-995d582d6463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36a5b9d-1ae2-43c5-8446-3f3b4b74fc13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee29b5a-a6d5-4993-8bee-995d582d6463",
                    "LayerId": "acfa6b44-8cca-4955-ad0a-7392d2f0b825"
                }
            ]
        },
        {
            "id": "6847baa2-3051-4432-a30e-1a1b607a1902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117ad46a-cf8c-42cc-9d2f-2ef1fe9498e2",
            "compositeImage": {
                "id": "133e693b-5d1d-4d1c-b69a-bff42edec711",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6847baa2-3051-4432-a30e-1a1b607a1902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036bb6f4-8a21-4f6c-92ce-17e35346dfe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6847baa2-3051-4432-a30e-1a1b607a1902",
                    "LayerId": "acfa6b44-8cca-4955-ad0a-7392d2f0b825"
                }
            ]
        },
        {
            "id": "96c8ea7b-bd13-4c58-af76-02954eaefc61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117ad46a-cf8c-42cc-9d2f-2ef1fe9498e2",
            "compositeImage": {
                "id": "2073c203-01a7-4411-b401-0f491703b35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c8ea7b-bd13-4c58-af76-02954eaefc61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0673af6-5d9f-44c8-8a2f-a8d3add129f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c8ea7b-bd13-4c58-af76-02954eaefc61",
                    "LayerId": "acfa6b44-8cca-4955-ad0a-7392d2f0b825"
                }
            ]
        },
        {
            "id": "4c23f9c2-c0b4-48c7-b081-10beaa57e799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "117ad46a-cf8c-42cc-9d2f-2ef1fe9498e2",
            "compositeImage": {
                "id": "0a378fb9-d533-4807-a205-239c0a442af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c23f9c2-c0b4-48c7-b081-10beaa57e799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be9c8e7-7ca5-4363-96f4-062abc979f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c23f9c2-c0b4-48c7-b081-10beaa57e799",
                    "LayerId": "acfa6b44-8cca-4955-ad0a-7392d2f0b825"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "acfa6b44-8cca-4955-ad0a-7392d2f0b825",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "117ad46a-cf8c-42cc-9d2f-2ef1fe9498e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}