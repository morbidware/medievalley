{
    "id": "f7b36083-c70e-467b-99c2-bfb6c970bcdf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df48addb-f8fb-4f30-b307-3e2dc0d5fa36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7b36083-c70e-467b-99c2-bfb6c970bcdf",
            "compositeImage": {
                "id": "2ad3ab50-ecf1-4e6f-ab8e-ef9453ee765a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df48addb-f8fb-4f30-b307-3e2dc0d5fa36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90465d29-8276-49df-a1ac-f7cbaf2cd5a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df48addb-f8fb-4f30-b307-3e2dc0d5fa36",
                    "LayerId": "414b9177-430c-4399-ad32-c3c829d7ef51"
                }
            ]
        },
        {
            "id": "31d451ec-0da8-4263-8488-ed301896fe46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7b36083-c70e-467b-99c2-bfb6c970bcdf",
            "compositeImage": {
                "id": "e95b38d0-c642-494e-8921-da62179dea35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d451ec-0da8-4263-8488-ed301896fe46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79544f04-3e13-4e30-8867-27a84d6d0ad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d451ec-0da8-4263-8488-ed301896fe46",
                    "LayerId": "414b9177-430c-4399-ad32-c3c829d7ef51"
                }
            ]
        },
        {
            "id": "460cd4c3-1852-45ae-a1df-a95cd4bdf873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7b36083-c70e-467b-99c2-bfb6c970bcdf",
            "compositeImage": {
                "id": "08e78731-1bef-4a66-b888-e11a35623633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "460cd4c3-1852-45ae-a1df-a95cd4bdf873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f7f0564-c4b3-470e-8bed-7552fa685cef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "460cd4c3-1852-45ae-a1df-a95cd4bdf873",
                    "LayerId": "414b9177-430c-4399-ad32-c3c829d7ef51"
                }
            ]
        },
        {
            "id": "6bdf2e83-3b65-4348-bdd1-33c964f8f845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7b36083-c70e-467b-99c2-bfb6c970bcdf",
            "compositeImage": {
                "id": "f923e0d1-2d37-4b33-b9c3-dbfa87db5196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bdf2e83-3b65-4348-bdd1-33c964f8f845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f51c37de-0eac-4523-95f2-92fce3dda584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bdf2e83-3b65-4348-bdd1-33c964f8f845",
                    "LayerId": "414b9177-430c-4399-ad32-c3c829d7ef51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "414b9177-430c-4399-ad32-c3c829d7ef51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7b36083-c70e-467b-99c2-bfb6c970bcdf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}