{
    "id": "8ba7e79a-3bca-468d-bd46-6a0fa8c7c255",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_hypericum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a394ee8a-99f2-4494-a618-76f59e0832f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ba7e79a-3bca-468d-bd46-6a0fa8c7c255",
            "compositeImage": {
                "id": "428f0c8b-69f2-41bf-8ca3-f071237150f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a394ee8a-99f2-4494-a618-76f59e0832f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8c1d83-06f8-4bae-b4d3-a6f23f95dd0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a394ee8a-99f2-4494-a618-76f59e0832f7",
                    "LayerId": "4aea1f2b-cc14-431e-86c8-655c0b2f4059"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4aea1f2b-cc14-431e-86c8-655c0b2f4059",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ba7e79a-3bca-468d-bd46-6a0fa8c7c255",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}