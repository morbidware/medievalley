{
    "id": "92465fa6-0d24-4f86-b19b-c75c6158627e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_popup_btn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1f07d4c-2779-4276-8d86-a8e30dc6e202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92465fa6-0d24-4f86-b19b-c75c6158627e",
            "compositeImage": {
                "id": "4d33c517-ca0d-4b0e-8f0b-f602c2de0728",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f07d4c-2779-4276-8d86-a8e30dc6e202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cef5aaa4-c69a-470c-8e0b-299a61802943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f07d4c-2779-4276-8d86-a8e30dc6e202",
                    "LayerId": "216dd3a2-2bdf-4e7d-b26a-8a759565ee15"
                }
            ]
        }
    ],
    "gridX": 160,
    "gridY": 90,
    "height": 12,
    "layers": [
        {
            "id": "216dd3a2-2bdf-4e7d-b26a-8a759565ee15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92465fa6-0d24-4f86-b19b-c75c6158627e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 6
}