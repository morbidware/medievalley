{
    "id": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_watering_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3ba5dfc-66f8-4684-bf59-99440af4b590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "compositeImage": {
                "id": "4a1e7f6d-82dc-4c6f-834f-d74f05c747ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ba5dfc-66f8-4684-bf59-99440af4b590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c79acc-0cf6-4367-965d-2d15d284659b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ba5dfc-66f8-4684-bf59-99440af4b590",
                    "LayerId": "7a02fc00-6341-4076-90ea-8142b35e1e1d"
                }
            ]
        },
        {
            "id": "5eae67a4-f1eb-4160-92af-9778c215bf29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "compositeImage": {
                "id": "dce55e18-e082-456b-91ba-fca6f6ff270a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5eae67a4-f1eb-4160-92af-9778c215bf29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531de7ff-a736-49ba-b389-2f117832b832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5eae67a4-f1eb-4160-92af-9778c215bf29",
                    "LayerId": "7a02fc00-6341-4076-90ea-8142b35e1e1d"
                }
            ]
        },
        {
            "id": "0e745b94-4069-49ec-8706-232640310e19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "compositeImage": {
                "id": "d79416ff-f487-4e67-9f66-3b970a1008d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e745b94-4069-49ec-8706-232640310e19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42652790-d48a-48d0-b528-a78bae952ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e745b94-4069-49ec-8706-232640310e19",
                    "LayerId": "7a02fc00-6341-4076-90ea-8142b35e1e1d"
                }
            ]
        },
        {
            "id": "3cd46cde-459b-4fbb-a0c0-c80932204f4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "compositeImage": {
                "id": "292e5545-d484-4c6c-b8e6-bb71fbc79b7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cd46cde-459b-4fbb-a0c0-c80932204f4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ca63da-a7ee-4b0c-b3bf-a3212156d81b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cd46cde-459b-4fbb-a0c0-c80932204f4c",
                    "LayerId": "7a02fc00-6341-4076-90ea-8142b35e1e1d"
                }
            ]
        },
        {
            "id": "dcb15db9-46e8-4637-9858-229bff961af4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "compositeImage": {
                "id": "f6dc91c2-13f1-4f6c-b2f6-59b54ca197b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb15db9-46e8-4637-9858-229bff961af4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de776519-da99-4c80-a3f0-3a93feaad547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb15db9-46e8-4637-9858-229bff961af4",
                    "LayerId": "7a02fc00-6341-4076-90ea-8142b35e1e1d"
                }
            ]
        },
        {
            "id": "76055517-810e-4718-bacc-e4d7f04aa3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "compositeImage": {
                "id": "262b1da2-2d5c-4817-8ed5-81533b727970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76055517-810e-4718-bacc-e4d7f04aa3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32c67bf7-a0ed-416d-9f73-1d434ba12100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76055517-810e-4718-bacc-e4d7f04aa3c6",
                    "LayerId": "7a02fc00-6341-4076-90ea-8142b35e1e1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a02fc00-6341-4076-90ea-8142b35e1e1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5570f0f8-6172-4e3b-acd7-f48db51bea2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}