{
    "id": "a678abc8-e719-45a8-a68b-7ef265122855",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_cauliflower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "859814ea-e71f-4c81-848f-8647f0bed4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a678abc8-e719-45a8-a68b-7ef265122855",
            "compositeImage": {
                "id": "48b05edd-9173-4067-9333-c4a4a0326603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "859814ea-e71f-4c81-848f-8647f0bed4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d2fb62-66c9-4fda-98dc-6f171c63b8c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "859814ea-e71f-4c81-848f-8647f0bed4d3",
                    "LayerId": "d667bc6b-a23f-43a0-88c2-777d52450873"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d667bc6b-a23f-43a0-88c2-777d52450873",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a678abc8-e719-45a8-a68b-7ef265122855",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}