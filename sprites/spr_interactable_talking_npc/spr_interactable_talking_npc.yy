{
    "id": "0cbab8ac-4aec-49b2-9b11-04bf0ba13d8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_talking_npc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2795eb02-d860-4fc7-9c90-845f2b2bcd03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cbab8ac-4aec-49b2-9b11-04bf0ba13d8e",
            "compositeImage": {
                "id": "37570b7b-4fbe-4ce8-a070-82f250541b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2795eb02-d860-4fc7-9c90-845f2b2bcd03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b51ef6-282d-499e-ae49-f4875e2f6b1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2795eb02-d860-4fc7-9c90-845f2b2bcd03",
                    "LayerId": "0305464a-c822-4c74-885f-b5777d26eb46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0305464a-c822-4c74-885f-b5777d26eb46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cbab8ac-4aec-49b2-9b11-04bf0ba13d8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}