{
    "id": "f57a4672-be4d-46ce-96c2-b13f38e163e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 4,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1c97dfd-4d15-43ee-8f90-c3de3cd240bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f57a4672-be4d-46ce-96c2-b13f38e163e7",
            "compositeImage": {
                "id": "53c194ed-68bc-4cd1-b4a8-3e8f8f931d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1c97dfd-4d15-43ee-8f90-c3de3cd240bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9739da86-9004-442c-8266-f6b4016284a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1c97dfd-4d15-43ee-8f90-c3de3cd240bb",
                    "LayerId": "77ee7937-526f-412b-9b4a-853e409a05cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "77ee7937-526f-412b-9b4a-853e409a05cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f57a4672-be4d-46ce-96c2-b13f38e163e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}