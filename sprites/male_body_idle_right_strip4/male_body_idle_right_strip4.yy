{
    "id": "bb291996-b464-4beb-9a1d-dfa8f16e0c2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4643357e-dd31-4691-9f45-aa92a97d3c11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb291996-b464-4beb-9a1d-dfa8f16e0c2f",
            "compositeImage": {
                "id": "fe6bcefb-b4a8-49d9-82a3-cbfc8f468940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4643357e-dd31-4691-9f45-aa92a97d3c11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5753db97-0552-4dae-9946-a495f2f5cc57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4643357e-dd31-4691-9f45-aa92a97d3c11",
                    "LayerId": "507289e0-4798-4a9a-bf89-9efe5ac5265c"
                }
            ]
        },
        {
            "id": "a90c78e7-5a33-47e9-b38b-2a583216dc47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb291996-b464-4beb-9a1d-dfa8f16e0c2f",
            "compositeImage": {
                "id": "66ce41f4-a148-4427-a559-92f3234f151b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90c78e7-5a33-47e9-b38b-2a583216dc47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66d3fca-1761-4f8a-8b66-e012e3e6202b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90c78e7-5a33-47e9-b38b-2a583216dc47",
                    "LayerId": "507289e0-4798-4a9a-bf89-9efe5ac5265c"
                }
            ]
        },
        {
            "id": "ca26300c-297d-4c79-856c-58e6928b3df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb291996-b464-4beb-9a1d-dfa8f16e0c2f",
            "compositeImage": {
                "id": "37aed99d-1904-4bd6-a713-44cba4cc7a4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca26300c-297d-4c79-856c-58e6928b3df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06b7a31d-2280-4816-87c1-c22d3e2f15ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca26300c-297d-4c79-856c-58e6928b3df4",
                    "LayerId": "507289e0-4798-4a9a-bf89-9efe5ac5265c"
                }
            ]
        },
        {
            "id": "887776f2-cb03-42bb-8ac1-5f30a635f8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb291996-b464-4beb-9a1d-dfa8f16e0c2f",
            "compositeImage": {
                "id": "634a6bed-0686-494f-8170-f9126f752fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "887776f2-cb03-42bb-8ac1-5f30a635f8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1b8ccf1-90f2-4643-8f76-e950e39c5b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "887776f2-cb03-42bb-8ac1-5f30a635f8a5",
                    "LayerId": "507289e0-4798-4a9a-bf89-9efe5ac5265c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "507289e0-4798-4a9a-bf89-9efe5ac5265c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb291996-b464-4beb-9a1d-dfa8f16e0c2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}