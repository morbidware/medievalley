{
    "id": "0d1132e5-4fdf-4783-a970-0fe674e06211",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1d03c96-d37f-4e07-9941-7a885aa1df2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d1132e5-4fdf-4783-a970-0fe674e06211",
            "compositeImage": {
                "id": "381ed6c3-3f39-40a1-8bb3-82f358548977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d03c96-d37f-4e07-9941-7a885aa1df2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d6d75b-6079-4d71-a5b7-6f932f9e83d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d03c96-d37f-4e07-9941-7a885aa1df2a",
                    "LayerId": "bf1ef958-4cac-455b-b2bb-5c5572cc3e79"
                }
            ]
        },
        {
            "id": "88aef6a6-f5ea-4df7-84ef-86994994f559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d1132e5-4fdf-4783-a970-0fe674e06211",
            "compositeImage": {
                "id": "1724da48-76ba-4539-802b-fae65e3c4caa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88aef6a6-f5ea-4df7-84ef-86994994f559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0167eda4-afe8-4ce1-9eb0-fc6d0bb8d9a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88aef6a6-f5ea-4df7-84ef-86994994f559",
                    "LayerId": "bf1ef958-4cac-455b-b2bb-5c5572cc3e79"
                }
            ]
        },
        {
            "id": "1943ae77-b233-48be-a6cf-0a5ea7efbff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d1132e5-4fdf-4783-a970-0fe674e06211",
            "compositeImage": {
                "id": "91ebd819-985a-4880-9b68-5da89f044f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1943ae77-b233-48be-a6cf-0a5ea7efbff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82203e8-83d0-4ccf-be14-560a21ecd526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1943ae77-b233-48be-a6cf-0a5ea7efbff3",
                    "LayerId": "bf1ef958-4cac-455b-b2bb-5c5572cc3e79"
                }
            ]
        },
        {
            "id": "f0db4644-17db-4577-9ba5-7f05b0235567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d1132e5-4fdf-4783-a970-0fe674e06211",
            "compositeImage": {
                "id": "c2b0b749-36a6-4b8c-9044-b407ea4dba9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0db4644-17db-4577-9ba5-7f05b0235567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36699850-9ed5-481b-a6d1-db0e625ac7b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0db4644-17db-4577-9ba5-7f05b0235567",
                    "LayerId": "bf1ef958-4cac-455b-b2bb-5c5572cc3e79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf1ef958-4cac-455b-b2bb-5c5572cc3e79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d1132e5-4fdf-4783-a970-0fe674e06211",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}