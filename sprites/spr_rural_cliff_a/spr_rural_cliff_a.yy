{
    "id": "b48c6820-5a4a-4b63-bde5-789e67a58444",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_cliff_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31171c51-ec97-487b-a92d-413efa3acba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b48c6820-5a4a-4b63-bde5-789e67a58444",
            "compositeImage": {
                "id": "d18ab338-12ec-41c1-8a0e-283efb797500",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31171c51-ec97-487b-a92d-413efa3acba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847228ca-b067-41ae-b28f-8f44899182f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31171c51-ec97-487b-a92d-413efa3acba6",
                    "LayerId": "eb0110ad-02db-4790-9b36-b286b6556aa1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eb0110ad-02db-4790-9b36-b286b6556aa1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b48c6820-5a4a-4b63-bde5-789e67a58444",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 18,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}