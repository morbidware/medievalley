{
    "id": "21964d04-86bf-4cfb-8aaa-6763d49774c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potion_life",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8266cfd3-bd48-48e6-b380-c53d6d59e622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21964d04-86bf-4cfb-8aaa-6763d49774c8",
            "compositeImage": {
                "id": "5470bfc6-152b-44a0-95c7-1ccb796bc27e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8266cfd3-bd48-48e6-b380-c53d6d59e622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5d291f3-0a37-450a-ac6b-5a03e017dfe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8266cfd3-bd48-48e6-b380-c53d6d59e622",
                    "LayerId": "72128ac6-d329-4756-8b30-b22f26b5b8bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "72128ac6-d329-4756-8b30-b22f26b5b8bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21964d04-86bf-4cfb-8aaa-6763d49774c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}