{
    "id": "ece15142-7520-41ec-b775-95d737544fd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f0e5de1-df7e-4f14-8ecf-6baa16ba2a0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece15142-7520-41ec-b775-95d737544fd0",
            "compositeImage": {
                "id": "7c8544c2-d10d-4243-9a30-b6a4dd9b1ab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f0e5de1-df7e-4f14-8ecf-6baa16ba2a0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9998b02-30a7-413e-906f-01013d5b86ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f0e5de1-df7e-4f14-8ecf-6baa16ba2a0d",
                    "LayerId": "5b2f74f4-b298-470c-8da9-064972adc82a"
                }
            ]
        },
        {
            "id": "f3f3af89-89d2-430b-8db6-7b6fa6297432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece15142-7520-41ec-b775-95d737544fd0",
            "compositeImage": {
                "id": "602e6e5f-65a7-4fe0-93bd-efe530535ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3f3af89-89d2-430b-8db6-7b6fa6297432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58577ac1-fbe8-4566-9325-eb99bfc0f576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3f3af89-89d2-430b-8db6-7b6fa6297432",
                    "LayerId": "5b2f74f4-b298-470c-8da9-064972adc82a"
                }
            ]
        },
        {
            "id": "206067c4-48ec-4f70-83da-a14228500ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece15142-7520-41ec-b775-95d737544fd0",
            "compositeImage": {
                "id": "9a4b4e44-1c23-4614-85a4-ad57a4ca276c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206067c4-48ec-4f70-83da-a14228500ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c974f800-aef9-482f-b93f-c5e604492dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206067c4-48ec-4f70-83da-a14228500ffd",
                    "LayerId": "5b2f74f4-b298-470c-8da9-064972adc82a"
                }
            ]
        },
        {
            "id": "f33492f7-238a-4175-b02a-479b0a18a30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ece15142-7520-41ec-b775-95d737544fd0",
            "compositeImage": {
                "id": "01585615-fcd2-4118-9a68-5a23b8579ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f33492f7-238a-4175-b02a-479b0a18a30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d698b81f-7766-4268-a4ec-6130b82d6440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f33492f7-238a-4175-b02a-479b0a18a30d",
                    "LayerId": "5b2f74f4-b298-470c-8da9-064972adc82a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5b2f74f4-b298-470c-8da9-064972adc82a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ece15142-7520-41ec-b775-95d737544fd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}