{
    "id": "0b7526e7-0b99-4707-9bff-1ae155fc7dac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a760b85-945d-4852-a7bf-255d1bca678b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7526e7-0b99-4707-9bff-1ae155fc7dac",
            "compositeImage": {
                "id": "8b2e09d3-7564-4989-a4b8-6b1154fa0c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a760b85-945d-4852-a7bf-255d1bca678b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded14e00-e9d2-4ae2-9850-1163b28ce0c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a760b85-945d-4852-a7bf-255d1bca678b",
                    "LayerId": "3e732942-dfcc-486d-908f-deefec4925c1"
                }
            ]
        },
        {
            "id": "ea17d81a-8461-4092-87ea-fd0dc8fecd50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7526e7-0b99-4707-9bff-1ae155fc7dac",
            "compositeImage": {
                "id": "966db99f-22a7-4329-9d48-4ffc5190d44c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea17d81a-8461-4092-87ea-fd0dc8fecd50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21eeb730-ed86-4fc1-a05e-0b6f4295a076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea17d81a-8461-4092-87ea-fd0dc8fecd50",
                    "LayerId": "3e732942-dfcc-486d-908f-deefec4925c1"
                }
            ]
        },
        {
            "id": "1e3a6d11-ce17-41d6-82e8-9656dd2d624d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7526e7-0b99-4707-9bff-1ae155fc7dac",
            "compositeImage": {
                "id": "e92d666a-502e-4869-be5d-d354c40843c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e3a6d11-ce17-41d6-82e8-9656dd2d624d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a7c4ea-f021-493e-b1fd-81de2bf8aedb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e3a6d11-ce17-41d6-82e8-9656dd2d624d",
                    "LayerId": "3e732942-dfcc-486d-908f-deefec4925c1"
                }
            ]
        },
        {
            "id": "e9d5e5bb-9fbc-4063-a190-adf6356dc03e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b7526e7-0b99-4707-9bff-1ae155fc7dac",
            "compositeImage": {
                "id": "717c0802-2aa9-4d58-9645-56c056af1873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9d5e5bb-9fbc-4063-a190-adf6356dc03e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719cccc0-21f9-4dfe-a11b-ea53fd412ce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9d5e5bb-9fbc-4063-a190-adf6356dc03e",
                    "LayerId": "3e732942-dfcc-486d-908f-deefec4925c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3e732942-dfcc-486d-908f-deefec4925c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b7526e7-0b99-4707-9bff-1ae155fc7dac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}