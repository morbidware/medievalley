{
    "id": "ebf093ef-8a30-42ac-a3e4-1ce6612eeaea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f761f0d-7a85-4dee-a19c-b2d28e55cbc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf093ef-8a30-42ac-a3e4-1ce6612eeaea",
            "compositeImage": {
                "id": "8844e186-a154-43ac-b46c-661b3f08c7c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f761f0d-7a85-4dee-a19c-b2d28e55cbc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b401debf-8739-4bff-9c9f-536009045157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f761f0d-7a85-4dee-a19c-b2d28e55cbc7",
                    "LayerId": "6d642421-8c44-4da3-9e56-c71bac0989b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6d642421-8c44-4da3-9e56-c71bac0989b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebf093ef-8a30-42ac-a3e4-1ce6612eeaea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}