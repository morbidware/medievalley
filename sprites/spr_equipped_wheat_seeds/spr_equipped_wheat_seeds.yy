{
    "id": "9dabe866-853d-4a1a-bbcf-69da555c6a95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_wheat_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85b1d64e-85c8-4808-8ed4-c0dc78e6d320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dabe866-853d-4a1a-bbcf-69da555c6a95",
            "compositeImage": {
                "id": "352688c3-7732-4ff7-ba14-c6b2db52f2d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b1d64e-85c8-4808-8ed4-c0dc78e6d320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76c9795c-b978-4eb0-b849-2476ab669a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b1d64e-85c8-4808-8ed4-c0dc78e6d320",
                    "LayerId": "7ce9a318-8b54-4c41-a45f-eda12f9eab01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7ce9a318-8b54-4c41-a45f-eda12f9eab01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dabe866-853d-4a1a-bbcf-69da555c6a95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}