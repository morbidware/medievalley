{
    "id": "2d5a8a69-ab1d-4011-9c38-7dc59d173ada",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a6b5131-e36f-41ba-8fc9-215ce7553bb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d5a8a69-ab1d-4011-9c38-7dc59d173ada",
            "compositeImage": {
                "id": "77d6b3df-f341-4410-b976-594dfa0794f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a6b5131-e36f-41ba-8fc9-215ce7553bb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa8108c-fe9e-4046-b66a-fd9c4bf4b761",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a6b5131-e36f-41ba-8fc9-215ce7553bb4",
                    "LayerId": "b4f76d1c-ba1b-4b0c-86ba-b37cb3dbabaf"
                }
            ]
        },
        {
            "id": "ec7d4663-77b1-4d79-81d3-6cc0deb7a7b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d5a8a69-ab1d-4011-9c38-7dc59d173ada",
            "compositeImage": {
                "id": "44564e6c-9909-4582-a3ce-ce2562c7c5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec7d4663-77b1-4d79-81d3-6cc0deb7a7b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a47e2df-801a-4f1f-a76f-40ab12da645b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec7d4663-77b1-4d79-81d3-6cc0deb7a7b2",
                    "LayerId": "b4f76d1c-ba1b-4b0c-86ba-b37cb3dbabaf"
                }
            ]
        },
        {
            "id": "746ebc9f-b2a9-46f1-b009-39872ee2a2cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d5a8a69-ab1d-4011-9c38-7dc59d173ada",
            "compositeImage": {
                "id": "d732e00e-8cfe-4772-a130-75968437f9f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "746ebc9f-b2a9-46f1-b009-39872ee2a2cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d132fa-40cd-4e51-ad3f-c8c4a4932840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "746ebc9f-b2a9-46f1-b009-39872ee2a2cf",
                    "LayerId": "b4f76d1c-ba1b-4b0c-86ba-b37cb3dbabaf"
                }
            ]
        },
        {
            "id": "c20f54fc-e741-4e81-a0f0-0597a53adc43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d5a8a69-ab1d-4011-9c38-7dc59d173ada",
            "compositeImage": {
                "id": "c5b22b62-509b-42e3-9249-44820e9f598f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c20f54fc-e741-4e81-a0f0-0597a53adc43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "363094a7-eea7-4d10-aad6-547bf65756cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20f54fc-e741-4e81-a0f0-0597a53adc43",
                    "LayerId": "b4f76d1c-ba1b-4b0c-86ba-b37cb3dbabaf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b4f76d1c-ba1b-4b0c-86ba-b37cb3dbabaf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d5a8a69-ab1d-4011-9c38-7dc59d173ada",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}