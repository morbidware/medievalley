{
    "id": "3b5bc1f7-6c2f-4c7a-bf71-8187c17866da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 5,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90217598-f0b9-40ff-9c72-a6b988459360",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5bc1f7-6c2f-4c7a-bf71-8187c17866da",
            "compositeImage": {
                "id": "3b1dea71-99fe-4c00-99a2-8bf8a3090dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90217598-f0b9-40ff-9c72-a6b988459360",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28473d91-a637-41e9-b7ca-1b1d91f13241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90217598-f0b9-40ff-9c72-a6b988459360",
                    "LayerId": "ae02a896-1b79-4a59-b76b-4200cb2b89f7"
                }
            ]
        },
        {
            "id": "8a4a4537-664d-4986-bd84-b8ce8fad5c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5bc1f7-6c2f-4c7a-bf71-8187c17866da",
            "compositeImage": {
                "id": "e7dbd0df-8896-4cdb-9047-32759a26c828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4a4537-664d-4986-bd84-b8ce8fad5c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fdf5ac5-f1df-48d4-9c59-f3bfc8c1606e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4a4537-664d-4986-bd84-b8ce8fad5c9e",
                    "LayerId": "ae02a896-1b79-4a59-b76b-4200cb2b89f7"
                }
            ]
        },
        {
            "id": "0ed5be71-b200-427c-9228-806ac776b06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5bc1f7-6c2f-4c7a-bf71-8187c17866da",
            "compositeImage": {
                "id": "f106921d-9f7b-499f-8509-de7f8a4e97ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ed5be71-b200-427c-9228-806ac776b06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e1246f-3375-495a-85a2-6216acec383c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ed5be71-b200-427c-9228-806ac776b06b",
                    "LayerId": "ae02a896-1b79-4a59-b76b-4200cb2b89f7"
                }
            ]
        },
        {
            "id": "9196396d-2863-4f4e-aa09-c9ce63e18b5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b5bc1f7-6c2f-4c7a-bf71-8187c17866da",
            "compositeImage": {
                "id": "73b58c62-b4f6-4bdf-8b38-459186cf4118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9196396d-2863-4f4e-aa09-c9ce63e18b5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e2b571d-0805-43dc-82e3-4a4eb0ec1648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9196396d-2863-4f4e-aa09-c9ce63e18b5e",
                    "LayerId": "ae02a896-1b79-4a59-b76b-4200cb2b89f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ae02a896-1b79-4a59-b76b-4200cb2b89f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b5bc1f7-6c2f-4c7a-bf71-8187c17866da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}