{
    "id": "badeb8d7-2eab-4d7f-afbe-991bc1c55e5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "076115ba-1499-43a8-bb39-4932024a2797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badeb8d7-2eab-4d7f-afbe-991bc1c55e5c",
            "compositeImage": {
                "id": "ecc84a1f-4d19-4f10-bf17-6ddceff0acde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "076115ba-1499-43a8-bb39-4932024a2797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693fdcb6-cd09-49c7-8f8b-5050c3403377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "076115ba-1499-43a8-bb39-4932024a2797",
                    "LayerId": "9b8a1907-bcaa-448f-a408-2622a7c059a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9b8a1907-bcaa-448f-a408-2622a7c059a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "badeb8d7-2eab-4d7f-afbe-991bc1c55e5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}