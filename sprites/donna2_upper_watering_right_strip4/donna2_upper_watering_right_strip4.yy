{
    "id": "dfd4a94e-b0f3-44fb-b520-5bb585b4930e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b90159c2-12c7-4014-b2a0-d0d7532ad7ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfd4a94e-b0f3-44fb-b520-5bb585b4930e",
            "compositeImage": {
                "id": "c1be9df0-be2c-4c9e-adf1-37dac9f1db2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90159c2-12c7-4014-b2a0-d0d7532ad7ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab922598-c480-4ea3-8146-5bc021d541fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90159c2-12c7-4014-b2a0-d0d7532ad7ab",
                    "LayerId": "d5f21f2b-86f9-4f21-bd22-8efc07f73457"
                }
            ]
        },
        {
            "id": "215c00eb-b360-40b0-9c3a-462fb3a7765a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfd4a94e-b0f3-44fb-b520-5bb585b4930e",
            "compositeImage": {
                "id": "7002f356-d54f-48a1-bef8-8f5986d20b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "215c00eb-b360-40b0-9c3a-462fb3a7765a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feb1e452-064b-409f-803a-34f47d33c3e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "215c00eb-b360-40b0-9c3a-462fb3a7765a",
                    "LayerId": "d5f21f2b-86f9-4f21-bd22-8efc07f73457"
                }
            ]
        },
        {
            "id": "291ed2e7-0c35-4edf-b185-8ae4fa88936e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfd4a94e-b0f3-44fb-b520-5bb585b4930e",
            "compositeImage": {
                "id": "ab5e32c1-2ba2-4f4f-8d32-a1d5be231589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "291ed2e7-0c35-4edf-b185-8ae4fa88936e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3bddd1f-c97a-488d-bea5-3d0508cb71e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "291ed2e7-0c35-4edf-b185-8ae4fa88936e",
                    "LayerId": "d5f21f2b-86f9-4f21-bd22-8efc07f73457"
                }
            ]
        },
        {
            "id": "88ceae08-9f55-4962-9ad8-a5e0ac586a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfd4a94e-b0f3-44fb-b520-5bb585b4930e",
            "compositeImage": {
                "id": "4c6415a0-10a7-4ce1-bd03-927dea3c18d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ceae08-9f55-4962-9ad8-a5e0ac586a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3645ef72-7cf2-4f21-8168-26edf1545559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ceae08-9f55-4962-9ad8-a5e0ac586a3d",
                    "LayerId": "d5f21f2b-86f9-4f21-bd22-8efc07f73457"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5f21f2b-86f9-4f21-bd22-8efc07f73457",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfd4a94e-b0f3-44fb-b520-5bb585b4930e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}