{
    "id": "2d678a19-80c6-43a0-aa75-e93437124591",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3593f4d-3f49-4576-98a9-dc9cfd68407b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d678a19-80c6-43a0-aa75-e93437124591",
            "compositeImage": {
                "id": "7d8e1da4-4609-4507-bedd-a696e4ed4f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3593f4d-3f49-4576-98a9-dc9cfd68407b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58c7498d-a39c-4b81-9c85-e12fee2850dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3593f4d-3f49-4576-98a9-dc9cfd68407b",
                    "LayerId": "411add61-2d52-4efc-83f4-ddaf63edcf2d"
                }
            ]
        },
        {
            "id": "33c9cd77-71a4-40fe-ac6c-e2203438a8cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d678a19-80c6-43a0-aa75-e93437124591",
            "compositeImage": {
                "id": "bb6a45d0-3799-4e42-9f91-6f11c2d1ed47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c9cd77-71a4-40fe-ac6c-e2203438a8cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab82e80-6565-42b8-91dd-afc4fe6f9d92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c9cd77-71a4-40fe-ac6c-e2203438a8cb",
                    "LayerId": "411add61-2d52-4efc-83f4-ddaf63edcf2d"
                }
            ]
        },
        {
            "id": "29065e08-72d6-48f7-8067-b27f63a42189",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d678a19-80c6-43a0-aa75-e93437124591",
            "compositeImage": {
                "id": "23d7aebb-3de6-4fc2-b68d-ef64e8ddb0a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29065e08-72d6-48f7-8067-b27f63a42189",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb93e7f-687d-4321-9b43-bc8622ba047d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29065e08-72d6-48f7-8067-b27f63a42189",
                    "LayerId": "411add61-2d52-4efc-83f4-ddaf63edcf2d"
                }
            ]
        },
        {
            "id": "2918fba8-c48a-4d34-b5d2-981765695361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d678a19-80c6-43a0-aa75-e93437124591",
            "compositeImage": {
                "id": "5faa60f0-d0d5-4e51-a06d-8bcd68866848",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2918fba8-c48a-4d34-b5d2-981765695361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69df626-76ce-4aef-81a3-01d71c83fd6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2918fba8-c48a-4d34-b5d2-981765695361",
                    "LayerId": "411add61-2d52-4efc-83f4-ddaf63edcf2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "411add61-2d52-4efc-83f4-ddaf63edcf2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d678a19-80c6-43a0-aa75-e93437124591",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}