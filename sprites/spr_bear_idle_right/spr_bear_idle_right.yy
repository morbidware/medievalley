{
    "id": "86925110-8163-4b80-bcfd-a76062132dd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e4b0fad-4a4e-4b74-a23b-5e85965ebba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86925110-8163-4b80-bcfd-a76062132dd4",
            "compositeImage": {
                "id": "7024da4b-7beb-44fc-abfc-007a67c96a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e4b0fad-4a4e-4b74-a23b-5e85965ebba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87aedf31-d53c-4141-a2a8-7da6cb8d746d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e4b0fad-4a4e-4b74-a23b-5e85965ebba8",
                    "LayerId": "37f1d6dd-9068-4f1f-ac33-7b71cc45be6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "37f1d6dd-9068-4f1f-ac33-7b71cc45be6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86925110-8163-4b80-bcfd-a76062132dd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 44
}