{
    "id": "031c3bf9-2130-41b2-be31-07adad539717",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "799d658b-b8cd-4dcf-8987-cec1854c0372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "031c3bf9-2130-41b2-be31-07adad539717",
            "compositeImage": {
                "id": "a1d4f8f3-1557-409f-9078-fe131a270ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799d658b-b8cd-4dcf-8987-cec1854c0372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44fbd9d1-b332-47fc-b0f9-d69bb187d7aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799d658b-b8cd-4dcf-8987-cec1854c0372",
                    "LayerId": "a6b9b2a3-0ce9-4ee6-aa4e-813c4e2d306c"
                }
            ]
        },
        {
            "id": "a5aa6a67-3dd3-4008-8ccf-26a13241f226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "031c3bf9-2130-41b2-be31-07adad539717",
            "compositeImage": {
                "id": "888220e7-d23d-4d8b-9c1a-30d057f74cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5aa6a67-3dd3-4008-8ccf-26a13241f226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7512dda6-aa98-4051-b5c3-69c39b1b3ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5aa6a67-3dd3-4008-8ccf-26a13241f226",
                    "LayerId": "a6b9b2a3-0ce9-4ee6-aa4e-813c4e2d306c"
                }
            ]
        },
        {
            "id": "817ca33c-ca93-43a4-92c9-009e1c7cea4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "031c3bf9-2130-41b2-be31-07adad539717",
            "compositeImage": {
                "id": "fa1d57f7-c586-47b3-af63-9f5a179b2f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "817ca33c-ca93-43a4-92c9-009e1c7cea4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f595293a-8e2b-4a3d-b53a-f9df58928be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "817ca33c-ca93-43a4-92c9-009e1c7cea4b",
                    "LayerId": "a6b9b2a3-0ce9-4ee6-aa4e-813c4e2d306c"
                }
            ]
        },
        {
            "id": "7cc920b3-5354-4388-9def-7be0f9c91167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "031c3bf9-2130-41b2-be31-07adad539717",
            "compositeImage": {
                "id": "a0a640bf-d938-4211-bc10-c2b24b005d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc920b3-5354-4388-9def-7be0f9c91167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba56ae63-bc2c-45f1-afdf-bf45b3deed31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc920b3-5354-4388-9def-7be0f9c91167",
                    "LayerId": "a6b9b2a3-0ce9-4ee6-aa4e-813c4e2d306c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a6b9b2a3-0ce9-4ee6-aa4e-813c4e2d306c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "031c3bf9-2130-41b2-be31-07adad539717",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}