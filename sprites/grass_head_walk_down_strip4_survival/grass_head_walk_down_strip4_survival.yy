{
    "id": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_walk_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f12954c3-82c7-458e-99c3-6e96aef27e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "compositeImage": {
                "id": "3d8ff63e-833b-4335-a1d4-9063631868f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12954c3-82c7-458e-99c3-6e96aef27e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54dca7c3-1b28-4132-a29a-41c2d9dc1fc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12954c3-82c7-458e-99c3-6e96aef27e14",
                    "LayerId": "23a45abf-789b-4b60-863f-adaca31e1a99"
                }
            ]
        },
        {
            "id": "0b7bdba1-bf51-4fdc-b2b9-d6a451a5ea48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "compositeImage": {
                "id": "420ca02b-4816-4d9d-8074-5cd5127a1f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7bdba1-bf51-4fdc-b2b9-d6a451a5ea48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "201375fa-1a2c-4120-9b0e-514894fff049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7bdba1-bf51-4fdc-b2b9-d6a451a5ea48",
                    "LayerId": "23a45abf-789b-4b60-863f-adaca31e1a99"
                }
            ]
        },
        {
            "id": "74aa83a7-a366-4893-a8cc-091c231fa91f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "compositeImage": {
                "id": "4b8ce494-dd05-456c-99f0-b50ceb964e23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74aa83a7-a366-4893-a8cc-091c231fa91f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa63e0ed-70ae-4801-8102-cc4e19aa872e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74aa83a7-a366-4893-a8cc-091c231fa91f",
                    "LayerId": "23a45abf-789b-4b60-863f-adaca31e1a99"
                }
            ]
        },
        {
            "id": "d1a85c5e-2956-496b-9678-1e074e6ea26d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "compositeImage": {
                "id": "636c0b0c-a42c-4f5c-8b4b-9843d15c021e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1a85c5e-2956-496b-9678-1e074e6ea26d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3545cfe-5a81-4ffd-afac-7bb5e963eb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1a85c5e-2956-496b-9678-1e074e6ea26d",
                    "LayerId": "23a45abf-789b-4b60-863f-adaca31e1a99"
                }
            ]
        },
        {
            "id": "f45d4b72-33ca-44af-a0d0-e4e08182869f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "compositeImage": {
                "id": "9ce785a6-2f78-4b90-8edd-eee5ae77fed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45d4b72-33ca-44af-a0d0-e4e08182869f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f384f2e-6e61-45df-a3a3-909833d2fe88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45d4b72-33ca-44af-a0d0-e4e08182869f",
                    "LayerId": "23a45abf-789b-4b60-863f-adaca31e1a99"
                }
            ]
        },
        {
            "id": "2e9e742c-ea74-4f37-bdde-67380e6398d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "compositeImage": {
                "id": "25c3f2e0-d279-4680-a801-20fbe997173f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e9e742c-ea74-4f37-bdde-67380e6398d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3e7d42-7fe2-4453-b62a-5283896245f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e9e742c-ea74-4f37-bdde-67380e6398d0",
                    "LayerId": "23a45abf-789b-4b60-863f-adaca31e1a99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "23a45abf-789b-4b60-863f-adaca31e1a99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16fabc1e-b9b5-458a-8a74-0011f802f1b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}