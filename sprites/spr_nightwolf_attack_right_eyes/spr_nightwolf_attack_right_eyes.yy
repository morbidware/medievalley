{
    "id": "0b96e9c3-9133-4160-b3cc-ec69c1776ada",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_attack_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 38,
    "bbox_right": 41,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13a9507d-e1ed-4ad7-9b37-699e3e3858b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b96e9c3-9133-4160-b3cc-ec69c1776ada",
            "compositeImage": {
                "id": "e8adfe6b-2ed7-498f-b657-5f89f1d572c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a9507d-e1ed-4ad7-9b37-699e3e3858b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6977802-3ca8-4e4d-8d8a-0f8e0070d1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a9507d-e1ed-4ad7-9b37-699e3e3858b5",
                    "LayerId": "5fb0f47e-9981-4cf9-a59d-36b3efae9371"
                }
            ]
        },
        {
            "id": "77ee8deb-88c2-412a-8574-495a089e8943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b96e9c3-9133-4160-b3cc-ec69c1776ada",
            "compositeImage": {
                "id": "ef9132fc-c199-4d2f-8352-6a2970118193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ee8deb-88c2-412a-8574-495a089e8943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7829377e-852f-4b9f-9dae-5e6d65b8d54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ee8deb-88c2-412a-8574-495a089e8943",
                    "LayerId": "5fb0f47e-9981-4cf9-a59d-36b3efae9371"
                }
            ]
        },
        {
            "id": "6d5dc806-b5e4-49d7-893c-1e1c909aa2cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b96e9c3-9133-4160-b3cc-ec69c1776ada",
            "compositeImage": {
                "id": "0d8507cc-8210-4e23-badf-057fad3d0b52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d5dc806-b5e4-49d7-893c-1e1c909aa2cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5921cd78-55fe-4288-927e-3dc070dc7c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d5dc806-b5e4-49d7-893c-1e1c909aa2cb",
                    "LayerId": "5fb0f47e-9981-4cf9-a59d-36b3efae9371"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "5fb0f47e-9981-4cf9-a59d-36b3efae9371",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b96e9c3-9133-4160-b3cc-ec69c1776ada",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}