{
    "id": "070d89fe-7b30-49a3-b252-ad223b07c592",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_hammer_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5a8db3b-4843-4b29-b407-862e8b3d7f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "compositeImage": {
                "id": "972af1cb-a14b-4ffd-8f3d-870c6abd47ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a8db3b-4843-4b29-b407-862e8b3d7f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2425bd40-5be3-4254-907f-df1bc3374c0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a8db3b-4843-4b29-b407-862e8b3d7f2c",
                    "LayerId": "351e4b05-544c-455b-983f-2ee093d6c4c1"
                }
            ]
        },
        {
            "id": "421a6841-e3b9-413b-b2e3-b21781b49a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "compositeImage": {
                "id": "10b64ddc-6aed-49ef-9108-73799eb4ca3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421a6841-e3b9-413b-b2e3-b21781b49a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b2a95c-dfec-4aa6-950b-b96197984917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421a6841-e3b9-413b-b2e3-b21781b49a92",
                    "LayerId": "351e4b05-544c-455b-983f-2ee093d6c4c1"
                }
            ]
        },
        {
            "id": "cee507f5-5139-4890-a44d-999807db7ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "compositeImage": {
                "id": "384db7a7-982d-4957-98b3-a57e810f27b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee507f5-5139-4890-a44d-999807db7ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29992ae5-26c6-471b-b287-2432a490177f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee507f5-5139-4890-a44d-999807db7ed2",
                    "LayerId": "351e4b05-544c-455b-983f-2ee093d6c4c1"
                }
            ]
        },
        {
            "id": "0383884a-dc6c-4a06-be23-8913e75e09f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "compositeImage": {
                "id": "89ac355d-3f33-44e6-9c28-2b742b58377e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0383884a-dc6c-4a06-be23-8913e75e09f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede16259-eb93-43c9-aa7a-a04e6e7bcc02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0383884a-dc6c-4a06-be23-8913e75e09f0",
                    "LayerId": "351e4b05-544c-455b-983f-2ee093d6c4c1"
                }
            ]
        },
        {
            "id": "9f044eb6-8a5b-45c1-ac0f-7a7d4c08d565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "compositeImage": {
                "id": "c5c8fa39-5738-40a5-95eb-ec3b3ebd3707",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f044eb6-8a5b-45c1-ac0f-7a7d4c08d565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e006b3-987f-4deb-8aee-68e2e5622269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f044eb6-8a5b-45c1-ac0f-7a7d4c08d565",
                    "LayerId": "351e4b05-544c-455b-983f-2ee093d6c4c1"
                }
            ]
        },
        {
            "id": "bd872818-ba8e-440d-8195-0e145c1bef75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "compositeImage": {
                "id": "cb774565-6772-4e4a-9d7e-4c6262dfefee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd872818-ba8e-440d-8195-0e145c1bef75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8934a921-05a8-4eaf-82ac-afee5ceae122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd872818-ba8e-440d-8195-0e145c1bef75",
                    "LayerId": "351e4b05-544c-455b-983f-2ee093d6c4c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "351e4b05-544c-455b-983f-2ee093d6c4c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "070d89fe-7b30-49a3-b252-ad223b07c592",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}