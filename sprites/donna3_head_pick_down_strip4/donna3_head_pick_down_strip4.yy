{
    "id": "66a80b2d-aa5b-42ef-b752-4eae972d866d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b4d2eb1-5b96-4fcf-a59d-1dda61121a66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a80b2d-aa5b-42ef-b752-4eae972d866d",
            "compositeImage": {
                "id": "68325406-87d9-4046-9f48-397597344cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4d2eb1-5b96-4fcf-a59d-1dda61121a66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6001e771-9c6b-4a74-8af5-7832c6807d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4d2eb1-5b96-4fcf-a59d-1dda61121a66",
                    "LayerId": "f6442a9d-5387-41c5-9296-fd703cd07d53"
                }
            ]
        },
        {
            "id": "cffe09c1-6a5b-4857-b592-02df5d4590f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a80b2d-aa5b-42ef-b752-4eae972d866d",
            "compositeImage": {
                "id": "d1a5dc0c-075b-4a7a-a2f3-91b03849138b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffe09c1-6a5b-4857-b592-02df5d4590f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6873c6a-8be3-433f-92ea-38fa2412255a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffe09c1-6a5b-4857-b592-02df5d4590f6",
                    "LayerId": "f6442a9d-5387-41c5-9296-fd703cd07d53"
                }
            ]
        },
        {
            "id": "9fff037c-3728-4f46-93e7-d7a6159f6201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a80b2d-aa5b-42ef-b752-4eae972d866d",
            "compositeImage": {
                "id": "1265f6d5-b93f-486d-be74-542aa6871b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fff037c-3728-4f46-93e7-d7a6159f6201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ffbd5b2-371b-465a-860d-f03fad13fe9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fff037c-3728-4f46-93e7-d7a6159f6201",
                    "LayerId": "f6442a9d-5387-41c5-9296-fd703cd07d53"
                }
            ]
        },
        {
            "id": "e45da56b-2b2e-4534-905b-b7d96b351b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a80b2d-aa5b-42ef-b752-4eae972d866d",
            "compositeImage": {
                "id": "b26f3476-2a97-461a-b022-2a0ed19648ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e45da56b-2b2e-4534-905b-b7d96b351b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f857d4-ddf6-4c9d-93fc-67ecb2865cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45da56b-2b2e-4534-905b-b7d96b351b64",
                    "LayerId": "f6442a9d-5387-41c5-9296-fd703cd07d53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f6442a9d-5387-41c5-9296-fd703cd07d53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66a80b2d-aa5b-42ef-b752-4eae972d866d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}