{
    "id": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_walk_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 41,
    "bbox_right": 41,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05940ab2-b550-413f-8b9c-686902e06871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "compositeImage": {
                "id": "54f0384f-f0a6-4a4d-9661-a7c5660ee2de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05940ab2-b550-413f-8b9c-686902e06871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcf8b46b-2f8b-4074-86a0-d0afd7960352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05940ab2-b550-413f-8b9c-686902e06871",
                    "LayerId": "6465f0ac-930f-4506-a08a-3878e9340268"
                }
            ]
        },
        {
            "id": "b4d1f9cd-ac12-43d6-a362-ac23c7b7b52a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "compositeImage": {
                "id": "adbc311a-d78e-4687-a1a1-240f764b5b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d1f9cd-ac12-43d6-a362-ac23c7b7b52a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454607d3-24ad-4762-a805-7ac882e36d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d1f9cd-ac12-43d6-a362-ac23c7b7b52a",
                    "LayerId": "6465f0ac-930f-4506-a08a-3878e9340268"
                }
            ]
        },
        {
            "id": "58e552e6-8d38-4f00-a790-665685828bec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "compositeImage": {
                "id": "e8165837-a10c-4d21-8355-01763b04fd13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e552e6-8d38-4f00-a790-665685828bec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95953fd9-a3c8-43be-89be-a2aa205afb40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e552e6-8d38-4f00-a790-665685828bec",
                    "LayerId": "6465f0ac-930f-4506-a08a-3878e9340268"
                }
            ]
        },
        {
            "id": "705c6e3f-c1bf-4050-92f5-0f10647d8c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "compositeImage": {
                "id": "bce13497-126e-41f1-842b-8b6f2b6bb9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705c6e3f-c1bf-4050-92f5-0f10647d8c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96f0a562-8efa-49db-a82f-072e5d3fdd93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705c6e3f-c1bf-4050-92f5-0f10647d8c8a",
                    "LayerId": "6465f0ac-930f-4506-a08a-3878e9340268"
                }
            ]
        },
        {
            "id": "87d040c7-651b-4982-8ffa-320ffd88fa1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "compositeImage": {
                "id": "ad1a7f86-4242-4237-9149-ff282465c717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d040c7-651b-4982-8ffa-320ffd88fa1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a845a39-4562-42f4-8a23-64daca34fc60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d040c7-651b-4982-8ffa-320ffd88fa1e",
                    "LayerId": "6465f0ac-930f-4506-a08a-3878e9340268"
                }
            ]
        },
        {
            "id": "07acfbda-504a-4256-9781-c52b12d2307f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "compositeImage": {
                "id": "9cfb810c-616d-4a41-a6c5-d78040734870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07acfbda-504a-4256-9781-c52b12d2307f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e13cbb-bbf6-46ce-b91d-369e4ceef450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07acfbda-504a-4256-9781-c52b12d2307f",
                    "LayerId": "6465f0ac-930f-4506-a08a-3878e9340268"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "6465f0ac-930f-4506-a08a-3878e9340268",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1c7c3ae-16e4-487d-9024-b0ce25565692",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}