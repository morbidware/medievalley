{
    "id": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_corner_bl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ff70295-14e0-4999-a332-efe3a6fcac44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
            "compositeImage": {
                "id": "c7f6d2e1-0c3b-48cf-a87f-cc6cfdb3eadf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff70295-14e0-4999-a332-efe3a6fcac44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d42cecc0-e2f8-432e-91f8-a30ba32f840e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff70295-14e0-4999-a332-efe3a6fcac44",
                    "LayerId": "f94d89c4-b5a6-4966-a8cd-517874632e91"
                }
            ]
        },
        {
            "id": "7dc465f0-1cee-46b8-a1af-eba3b638da96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
            "compositeImage": {
                "id": "4e4197f8-96a5-4700-a0b0-1522ade774e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dc465f0-1cee-46b8-a1af-eba3b638da96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4da2ae-eb5a-4fe7-acdc-246d01bd1aa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dc465f0-1cee-46b8-a1af-eba3b638da96",
                    "LayerId": "f94d89c4-b5a6-4966-a8cd-517874632e91"
                }
            ]
        },
        {
            "id": "6bf10c9b-f77f-4374-a268-71ca1e450c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
            "compositeImage": {
                "id": "83149205-1419-4d09-85c1-ee48aaf4016f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bf10c9b-f77f-4374-a268-71ca1e450c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d12708-c709-4fe5-9ad6-56fa31bccd14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bf10c9b-f77f-4374-a268-71ca1e450c00",
                    "LayerId": "f94d89c4-b5a6-4966-a8cd-517874632e91"
                }
            ]
        },
        {
            "id": "3da9b4a4-1d7e-40cb-9235-6612f667c826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
            "compositeImage": {
                "id": "0979e1fa-6b8f-40da-b3df-01ffee4b3729",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da9b4a4-1d7e-40cb-9235-6612f667c826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce553359-2e1a-420a-959c-d8f7c3d4397f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da9b4a4-1d7e-40cb-9235-6612f667c826",
                    "LayerId": "f94d89c4-b5a6-4966-a8cd-517874632e91"
                }
            ]
        },
        {
            "id": "35d5f706-b098-4153-8300-c7b9812f6603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
            "compositeImage": {
                "id": "6fe7acbc-c64f-4cb7-b20b-ae79ed8a222f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d5f706-b098-4153-8300-c7b9812f6603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b49902-afed-482f-9fde-19f5987bf6f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d5f706-b098-4153-8300-c7b9812f6603",
                    "LayerId": "f94d89c4-b5a6-4966-a8cd-517874632e91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f94d89c4-b5a6-4966-a8cd-517874632e91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16785635-b5fd-42a1-beee-1e75e2c0bed6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}