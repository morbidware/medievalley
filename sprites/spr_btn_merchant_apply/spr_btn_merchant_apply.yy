{
    "id": "58cc33a3-f98b-4595-bf12-3556bb5eff31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_merchant_apply",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e887f9d8-eff9-4f04-a3d4-f44e112c2e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cc33a3-f98b-4595-bf12-3556bb5eff31",
            "compositeImage": {
                "id": "31e02e4e-a97f-4762-86bd-d7411c8e0e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e887f9d8-eff9-4f04-a3d4-f44e112c2e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8733101-7c3e-4253-86c5-c86eb25e9d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e887f9d8-eff9-4f04-a3d4-f44e112c2e16",
                    "LayerId": "773646a4-4538-412d-a2b4-43137e1271d9"
                }
            ]
        },
        {
            "id": "20782a73-5132-42c2-a53c-c4c2133699e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cc33a3-f98b-4595-bf12-3556bb5eff31",
            "compositeImage": {
                "id": "b6822fe3-4e7f-4867-b01e-72eda0798b40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20782a73-5132-42c2-a53c-c4c2133699e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c75885-2269-4ab9-a6be-09039f09aeb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20782a73-5132-42c2-a53c-c4c2133699e8",
                    "LayerId": "773646a4-4538-412d-a2b4-43137e1271d9"
                }
            ]
        },
        {
            "id": "6bd7d881-986f-4b49-bb16-835118a24af3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58cc33a3-f98b-4595-bf12-3556bb5eff31",
            "compositeImage": {
                "id": "2020f109-1ab4-4eaf-8872-d31888667786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd7d881-986f-4b49-bb16-835118a24af3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494b7e40-8772-4531-bab1-93f12c13aa94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd7d881-986f-4b49-bb16-835118a24af3",
                    "LayerId": "773646a4-4538-412d-a2b4-43137e1271d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "773646a4-4538-412d-a2b4-43137e1271d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58cc33a3-f98b-4595-bf12-3556bb5eff31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 5
}