{
    "id": "d7bcdf71-c91e-4337-8af0-3e1b20a59493",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abandoned_castle_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 290,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05aeb325-e391-42d9-b53f-faef5e8b6ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7bcdf71-c91e-4337-8af0-3e1b20a59493",
            "compositeImage": {
                "id": "21e633f6-f93d-442b-8bd8-c986ea7833f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05aeb325-e391-42d9-b53f-faef5e8b6ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "195cf016-2ccf-4349-af03-781e2fad97e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05aeb325-e391-42d9-b53f-faef5e8b6ae6",
                    "LayerId": "38fa0ce4-3105-4d22-98ce-30d5cb68375e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "38fa0ce4-3105-4d22-98ce-30d5cb68375e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7bcdf71-c91e-4337-8af0-3e1b20a59493",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 291,
    "xorig": 145,
    "yorig": 106
}