{
    "id": "23b6a11a-80a1-46d1-ab92-0a46ae47ac20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_home_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a443eb0f-63e7-4a48-9e93-d774e4f58604",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23b6a11a-80a1-46d1-ab92-0a46ae47ac20",
            "compositeImage": {
                "id": "f3c893f2-ddd3-473f-9ca0-dcf92577f3a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a443eb0f-63e7-4a48-9e93-d774e4f58604",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62baa9f-5909-44d5-aa8b-ebe0950b99e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a443eb0f-63e7-4a48-9e93-d774e4f58604",
                    "LayerId": "1d39f4e8-b295-4a87-b48e-c9813b3ff26e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "1d39f4e8-b295-4a87-b48e-c9813b3ff26e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23b6a11a-80a1-46d1-ab92-0a46ae47ac20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 135
}