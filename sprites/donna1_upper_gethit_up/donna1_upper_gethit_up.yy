{
    "id": "877c7f0c-e6b4-40db-be37-216902d48f31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de291da2-58fd-43b7-8bb1-75025bb6800d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "877c7f0c-e6b4-40db-be37-216902d48f31",
            "compositeImage": {
                "id": "a969f2b9-8911-4a1b-877b-8d085aadbee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de291da2-58fd-43b7-8bb1-75025bb6800d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be5a1993-d44c-41e0-9c4a-14c8ad0fb7b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de291da2-58fd-43b7-8bb1-75025bb6800d",
                    "LayerId": "a6ae8e67-c13d-4f4d-9fca-cf7b27de3f3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a6ae8e67-c13d-4f4d-9fca-cf7b27de3f3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "877c7f0c-e6b4-40db-be37-216902d48f31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}