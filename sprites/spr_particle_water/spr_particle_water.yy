{
    "id": "d28c31b4-f583-4c24-93fe-90b48cdaafe6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4b44e1f-2340-4bd0-b55d-cf848a290fb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d28c31b4-f583-4c24-93fe-90b48cdaafe6",
            "compositeImage": {
                "id": "57bb5bf8-99f7-4a71-9774-03019567361a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b44e1f-2340-4bd0-b55d-cf848a290fb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a4199e-449d-4531-84d1-f8572632e360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b44e1f-2340-4bd0-b55d-cf848a290fb2",
                    "LayerId": "49ec7fbd-707b-4135-aba3-b55b74d7484c"
                }
            ]
        },
        {
            "id": "06edb277-bf99-4ab4-b746-ba2aacc0afa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d28c31b4-f583-4c24-93fe-90b48cdaafe6",
            "compositeImage": {
                "id": "cf9817ad-c409-44b8-979c-622ccbe328f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06edb277-bf99-4ab4-b746-ba2aacc0afa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2abe8af8-af77-46ea-aee8-5070f94018fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06edb277-bf99-4ab4-b746-ba2aacc0afa9",
                    "LayerId": "49ec7fbd-707b-4135-aba3-b55b74d7484c"
                }
            ]
        },
        {
            "id": "0446631a-12a8-4c11-98a2-33f478767a20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d28c31b4-f583-4c24-93fe-90b48cdaafe6",
            "compositeImage": {
                "id": "ccd81ccc-926d-4a6f-a128-ae7e71ac5a9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0446631a-12a8-4c11-98a2-33f478767a20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf4c290-ced9-40e8-ad1d-c543ebc4fb5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0446631a-12a8-4c11-98a2-33f478767a20",
                    "LayerId": "49ec7fbd-707b-4135-aba3-b55b74d7484c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "49ec7fbd-707b-4135-aba3-b55b74d7484c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d28c31b4-f583-4c24-93fe-90b48cdaafe6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}