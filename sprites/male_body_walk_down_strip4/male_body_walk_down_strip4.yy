{
    "id": "0c0c9960-39ea-4d35-8b64-d7df6a2b9f31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb101a01-861a-40b8-87d2-b6b8f90e04a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0c9960-39ea-4d35-8b64-d7df6a2b9f31",
            "compositeImage": {
                "id": "e5bfcec0-0dc1-48c3-a33a-be5e29a38bd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb101a01-861a-40b8-87d2-b6b8f90e04a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3d0e031-72cc-40f7-b17c-33a357f2cc3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb101a01-861a-40b8-87d2-b6b8f90e04a1",
                    "LayerId": "730b3f62-d5bb-40f4-bbe4-d2c861b68fe8"
                }
            ]
        },
        {
            "id": "30ee20d1-d985-4ed3-86eb-5964454eaf52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0c9960-39ea-4d35-8b64-d7df6a2b9f31",
            "compositeImage": {
                "id": "98ad93dd-8a85-477a-bcde-5d3451c7d72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ee20d1-d985-4ed3-86eb-5964454eaf52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b159e0-a5a3-4d73-ab31-cf151ae1c43c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ee20d1-d985-4ed3-86eb-5964454eaf52",
                    "LayerId": "730b3f62-d5bb-40f4-bbe4-d2c861b68fe8"
                }
            ]
        },
        {
            "id": "059aaf05-71e6-45d5-b24c-477c306fb05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0c9960-39ea-4d35-8b64-d7df6a2b9f31",
            "compositeImage": {
                "id": "1cf67267-6ae5-4955-9365-0b4a12de82d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059aaf05-71e6-45d5-b24c-477c306fb05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f88b107-a544-4a83-9add-67df0892590c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059aaf05-71e6-45d5-b24c-477c306fb05d",
                    "LayerId": "730b3f62-d5bb-40f4-bbe4-d2c861b68fe8"
                }
            ]
        },
        {
            "id": "3d4e3eb6-b0fc-49b0-8f83-0f999db48078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c0c9960-39ea-4d35-8b64-d7df6a2b9f31",
            "compositeImage": {
                "id": "e9f830dd-b930-4ae1-9e8d-d07ea9d06ead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4e3eb6-b0fc-49b0-8f83-0f999db48078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd9fd3ef-832a-44c0-901a-fdfbfa557111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4e3eb6-b0fc-49b0-8f83-0f999db48078",
                    "LayerId": "730b3f62-d5bb-40f4-bbe4-d2c861b68fe8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "730b3f62-d5bb-40f4-bbe4-d2c861b68fe8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c0c9960-39ea-4d35-8b64-d7df6a2b9f31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}