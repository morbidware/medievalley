{
    "id": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_hammer_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe6bfb55-430c-4d1d-af39-65c24910b13c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "compositeImage": {
                "id": "baa85031-567f-4db1-95b8-0c78ff501024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe6bfb55-430c-4d1d-af39-65c24910b13c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7a891c1-de74-4259-81ab-3ed8bde02472",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe6bfb55-430c-4d1d-af39-65c24910b13c",
                    "LayerId": "05be202c-33fb-4252-9614-272ae8c2d18b"
                }
            ]
        },
        {
            "id": "85f6441a-9819-4537-bde8-976f1440b8cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "compositeImage": {
                "id": "b0299f5c-b7fa-4f6c-8f15-c67e4e608cf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f6441a-9819-4537-bde8-976f1440b8cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe070fae-1cc5-4f44-a847-da9c7434ada2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f6441a-9819-4537-bde8-976f1440b8cf",
                    "LayerId": "05be202c-33fb-4252-9614-272ae8c2d18b"
                }
            ]
        },
        {
            "id": "3e6abcb9-50ed-4e28-9fde-4326f4cd4b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "compositeImage": {
                "id": "eca64c6a-d156-4704-b97a-f382f9bf52df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6abcb9-50ed-4e28-9fde-4326f4cd4b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea29b182-72f5-4f69-8c29-c9ec76251b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6abcb9-50ed-4e28-9fde-4326f4cd4b1f",
                    "LayerId": "05be202c-33fb-4252-9614-272ae8c2d18b"
                }
            ]
        },
        {
            "id": "4a711880-6486-46c7-a8bc-482d5d176b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "compositeImage": {
                "id": "16667e0c-bae0-4697-bc31-8f49c0476dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a711880-6486-46c7-a8bc-482d5d176b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09155ed7-757e-4ebc-a246-a6a5cb1dda35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a711880-6486-46c7-a8bc-482d5d176b83",
                    "LayerId": "05be202c-33fb-4252-9614-272ae8c2d18b"
                }
            ]
        },
        {
            "id": "04f635c8-70c9-4649-b8a5-22d28361a6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "compositeImage": {
                "id": "8318f6c2-3fd4-473b-b4e5-7aeff63f423b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f635c8-70c9-4649-b8a5-22d28361a6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b74dc3-bef6-4bfd-a65d-35fd782af637",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f635c8-70c9-4649-b8a5-22d28361a6e4",
                    "LayerId": "05be202c-33fb-4252-9614-272ae8c2d18b"
                }
            ]
        },
        {
            "id": "1338a2e1-1733-48b5-b301-b0e61735a43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "compositeImage": {
                "id": "0e15ff87-f140-4370-a554-3c8d196b385f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1338a2e1-1733-48b5-b301-b0e61735a43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9b5335-56f5-4f1f-beb5-fa666c452c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1338a2e1-1733-48b5-b301-b0e61735a43a",
                    "LayerId": "05be202c-33fb-4252-9614-272ae8c2d18b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "05be202c-33fb-4252-9614-272ae8c2d18b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91251ba8-8e94-4ffa-ae62-1359a2f5fcba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}