{
    "id": "24629d2e-be98-464c-8019-92117e0dd7e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_table",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78bac9bc-3b50-483a-af2d-db57195532a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24629d2e-be98-464c-8019-92117e0dd7e7",
            "compositeImage": {
                "id": "b2650a9f-61f0-402f-be63-097781d10295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78bac9bc-3b50-483a-af2d-db57195532a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a409cdd0-132f-4626-b9f7-18b58e3216a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78bac9bc-3b50-483a-af2d-db57195532a7",
                    "LayerId": "4cdfb6ee-c91a-49d6-a887-fa0b383a3de4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4cdfb6ee-c91a-49d6-a887-fa0b383a3de4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24629d2e-be98-464c-8019-92117e0dd7e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 40
}