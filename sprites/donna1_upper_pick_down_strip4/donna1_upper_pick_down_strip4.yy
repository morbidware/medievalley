{
    "id": "685b7a6f-77c2-4893-b4d5-48c1eecede4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45f4b7d6-dc7d-416c-99bd-1dbcd3ce866b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "685b7a6f-77c2-4893-b4d5-48c1eecede4d",
            "compositeImage": {
                "id": "fbcc20b6-086c-42f3-a97a-916fefb3351e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45f4b7d6-dc7d-416c-99bd-1dbcd3ce866b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0458a716-982a-496d-95e8-94c6a3daf1fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45f4b7d6-dc7d-416c-99bd-1dbcd3ce866b",
                    "LayerId": "ba74238d-c441-4447-be39-d614f58e5637"
                }
            ]
        },
        {
            "id": "d70b6bb7-5e78-4222-9789-0958538749ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "685b7a6f-77c2-4893-b4d5-48c1eecede4d",
            "compositeImage": {
                "id": "5b413212-b0df-438b-9c19-0cecf7579465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d70b6bb7-5e78-4222-9789-0958538749ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58384ee-c9a7-4e9b-97f8-a939b1d0df7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d70b6bb7-5e78-4222-9789-0958538749ed",
                    "LayerId": "ba74238d-c441-4447-be39-d614f58e5637"
                }
            ]
        },
        {
            "id": "64af62b0-9cd9-4299-8140-8f3bf89f8c24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "685b7a6f-77c2-4893-b4d5-48c1eecede4d",
            "compositeImage": {
                "id": "9169e093-51d1-4b8f-8a0f-caf6af55bb17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64af62b0-9cd9-4299-8140-8f3bf89f8c24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d0e0a7-0700-4afe-a93d-9871eb35b321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64af62b0-9cd9-4299-8140-8f3bf89f8c24",
                    "LayerId": "ba74238d-c441-4447-be39-d614f58e5637"
                }
            ]
        },
        {
            "id": "623db9c6-87b6-409c-bd1f-40d0e4ddf9d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "685b7a6f-77c2-4893-b4d5-48c1eecede4d",
            "compositeImage": {
                "id": "921ca346-7eed-412d-add6-232a2899e798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623db9c6-87b6-409c-bd1f-40d0e4ddf9d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c63c9b-1511-40c2-b332-027350146d26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623db9c6-87b6-409c-bd1f-40d0e4ddf9d1",
                    "LayerId": "ba74238d-c441-4447-be39-d614f58e5637"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba74238d-c441-4447-be39-d614f58e5637",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "685b7a6f-77c2-4893-b4d5-48c1eecede4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}