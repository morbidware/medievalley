{
    "id": "0ccb2731-ed2a-4b92-ac62-51bb8293c326",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 33,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3475248-68ba-4eb7-85c4-b657e2b03ec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ccb2731-ed2a-4b92-ac62-51bb8293c326",
            "compositeImage": {
                "id": "9536849e-8905-4fc3-b404-3ce8b6c12844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3475248-68ba-4eb7-85c4-b657e2b03ec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0ae5b8-8371-4fde-9c4a-501aeec3f79a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3475248-68ba-4eb7-85c4-b657e2b03ec0",
                    "LayerId": "d2b3e2e9-5399-405c-bbb7-32be0d6ec74a"
                }
            ]
        },
        {
            "id": "742b785e-cc27-4f8f-af1e-06f70fcc805f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ccb2731-ed2a-4b92-ac62-51bb8293c326",
            "compositeImage": {
                "id": "23ae504e-124b-49c6-bef8-7645f2b81ab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742b785e-cc27-4f8f-af1e-06f70fcc805f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fda97063-1e51-4637-b8e7-96c82abad14c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742b785e-cc27-4f8f-af1e-06f70fcc805f",
                    "LayerId": "d2b3e2e9-5399-405c-bbb7-32be0d6ec74a"
                }
            ]
        },
        {
            "id": "f17d1246-aa2e-4240-bc87-822742d25ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ccb2731-ed2a-4b92-ac62-51bb8293c326",
            "compositeImage": {
                "id": "44175b79-0f34-4d6e-b183-d3cbc0f04c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f17d1246-aa2e-4240-bc87-822742d25ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2603643-4c26-47a4-9894-933b85973473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f17d1246-aa2e-4240-bc87-822742d25ce6",
                    "LayerId": "d2b3e2e9-5399-405c-bbb7-32be0d6ec74a"
                }
            ]
        },
        {
            "id": "c2ca35d1-02a8-4aa3-9342-58d0d60d3626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ccb2731-ed2a-4b92-ac62-51bb8293c326",
            "compositeImage": {
                "id": "6cf5ece6-5180-4036-b227-c220f2c3bf07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2ca35d1-02a8-4aa3-9342-58d0d60d3626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cce922b9-f0af-418a-be82-ecf67c0e0f64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2ca35d1-02a8-4aa3-9342-58d0d60d3626",
                    "LayerId": "d2b3e2e9-5399-405c-bbb7-32be0d6ec74a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "d2b3e2e9-5399-405c-bbb7-32be0d6ec74a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ccb2731-ed2a-4b92-ac62-51bb8293c326",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}