{
    "id": "944c8a86-823c-4c1d-baed-9366a599fde1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdd74641-86c1-465d-8153-eee9f7163c8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944c8a86-823c-4c1d-baed-9366a599fde1",
            "compositeImage": {
                "id": "b2aba5ed-c410-4afd-bab9-e00653e89989",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd74641-86c1-465d-8153-eee9f7163c8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6afdea24-23be-409a-b11d-24d3db539780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd74641-86c1-465d-8153-eee9f7163c8b",
                    "LayerId": "8b512566-c7a5-4997-8e5c-139b9fc38676"
                }
            ]
        },
        {
            "id": "f0d9fd17-f960-4cf2-9e3b-079d4966f235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944c8a86-823c-4c1d-baed-9366a599fde1",
            "compositeImage": {
                "id": "d065c177-6d89-448b-ab70-5bba7cd23f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d9fd17-f960-4cf2-9e3b-079d4966f235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ebedb6-2ec4-4459-83ff-c0c361d9d457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d9fd17-f960-4cf2-9e3b-079d4966f235",
                    "LayerId": "8b512566-c7a5-4997-8e5c-139b9fc38676"
                }
            ]
        },
        {
            "id": "cd67c3ff-fb4a-4238-ae0c-0d2a9f570adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944c8a86-823c-4c1d-baed-9366a599fde1",
            "compositeImage": {
                "id": "7fdf2c60-a002-4198-aaa2-2a0848d8aa8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd67c3ff-fb4a-4238-ae0c-0d2a9f570adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f50bb9f2-fa3a-4c87-89be-13aff756d202",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd67c3ff-fb4a-4238-ae0c-0d2a9f570adb",
                    "LayerId": "8b512566-c7a5-4997-8e5c-139b9fc38676"
                }
            ]
        },
        {
            "id": "29838f18-1233-4d57-82ad-f7067ec2ac42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "944c8a86-823c-4c1d-baed-9366a599fde1",
            "compositeImage": {
                "id": "4a32b654-d6f1-42b9-8f4f-e3be9715efd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29838f18-1233-4d57-82ad-f7067ec2ac42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f500fe-bdb3-4b86-8e8d-e105dad733ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29838f18-1233-4d57-82ad-f7067ec2ac42",
                    "LayerId": "8b512566-c7a5-4997-8e5c-139b9fc38676"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b512566-c7a5-4997-8e5c-139b9fc38676",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "944c8a86-823c-4c1d-baed-9366a599fde1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}