{
    "id": "c304c03d-72ce-4244-b081-6cefa88e5799",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a15d408e-7f67-44b6-a451-285d5ec60f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c304c03d-72ce-4244-b081-6cefa88e5799",
            "compositeImage": {
                "id": "88398e99-eca2-42a0-90bc-e464812b8037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a15d408e-7f67-44b6-a451-285d5ec60f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc7de28-3cd5-4ac9-b77c-8500de211c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a15d408e-7f67-44b6-a451-285d5ec60f96",
                    "LayerId": "d802f082-65dc-4da5-87e7-ee514bbe79ab"
                }
            ]
        },
        {
            "id": "45ddd2c7-33d8-4584-a14a-f6065b74c1b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c304c03d-72ce-4244-b081-6cefa88e5799",
            "compositeImage": {
                "id": "cb6d6f7d-da95-43a6-9330-51c814fafb31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ddd2c7-33d8-4584-a14a-f6065b74c1b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "255f5a35-83e2-444b-9b90-4e0f5c813164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ddd2c7-33d8-4584-a14a-f6065b74c1b9",
                    "LayerId": "d802f082-65dc-4da5-87e7-ee514bbe79ab"
                }
            ]
        },
        {
            "id": "a40469b4-1da4-479b-92cf-0b1ba0934783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c304c03d-72ce-4244-b081-6cefa88e5799",
            "compositeImage": {
                "id": "8196af50-466c-48ca-932e-e6a2e536a26b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40469b4-1da4-479b-92cf-0b1ba0934783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cca5efa8-7b32-4524-a496-e01573f20343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40469b4-1da4-479b-92cf-0b1ba0934783",
                    "LayerId": "d802f082-65dc-4da5-87e7-ee514bbe79ab"
                }
            ]
        },
        {
            "id": "fe508b76-65f4-464e-9a67-2f7150793fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c304c03d-72ce-4244-b081-6cefa88e5799",
            "compositeImage": {
                "id": "faed5cfb-b97a-4b55-910f-49957e13471b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe508b76-65f4-464e-9a67-2f7150793fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14d47c5-1488-42eb-ab62-4256953d163a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe508b76-65f4-464e-9a67-2f7150793fad",
                    "LayerId": "d802f082-65dc-4da5-87e7-ee514bbe79ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d802f082-65dc-4da5-87e7-ee514bbe79ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c304c03d-72ce-4244-b081-6cefa88e5799",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}