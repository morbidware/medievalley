{
    "id": "1a2002b5-6248-4fc3-978d-aebcd3cbc920",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "552ee28f-1e31-4cd5-a4db-300fc3d5d3f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a2002b5-6248-4fc3-978d-aebcd3cbc920",
            "compositeImage": {
                "id": "b8490daf-5f7f-4eff-ab29-cdcfd6fe93fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "552ee28f-1e31-4cd5-a4db-300fc3d5d3f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49117dc4-a15b-430d-b436-d83fb0666935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "552ee28f-1e31-4cd5-a4db-300fc3d5d3f7",
                    "LayerId": "ac83a7d7-0dd2-4ba2-a25c-f787ad8ce27a"
                }
            ]
        },
        {
            "id": "7eb3bb19-8494-4de8-b766-014d55ea38aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a2002b5-6248-4fc3-978d-aebcd3cbc920",
            "compositeImage": {
                "id": "2dbf8f51-4b51-4b46-a4ef-b363879d5a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eb3bb19-8494-4de8-b766-014d55ea38aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb34e15-bbab-462c-b38a-530285ba0c7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eb3bb19-8494-4de8-b766-014d55ea38aa",
                    "LayerId": "ac83a7d7-0dd2-4ba2-a25c-f787ad8ce27a"
                }
            ]
        },
        {
            "id": "dc2556b3-f498-403a-9301-5fb063e05d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a2002b5-6248-4fc3-978d-aebcd3cbc920",
            "compositeImage": {
                "id": "6461540e-f352-4ebb-88c8-09f3953b2054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc2556b3-f498-403a-9301-5fb063e05d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28e46304-12d0-42e7-b734-4dbb9e5033f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc2556b3-f498-403a-9301-5fb063e05d0d",
                    "LayerId": "ac83a7d7-0dd2-4ba2-a25c-f787ad8ce27a"
                }
            ]
        },
        {
            "id": "244efc2c-cc76-4b75-a1f2-26db90e15016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a2002b5-6248-4fc3-978d-aebcd3cbc920",
            "compositeImage": {
                "id": "6ba72d57-04e6-497b-9b1a-bf5c0744ec6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "244efc2c-cc76-4b75-a1f2-26db90e15016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad212b67-c36f-43fc-96fb-b031ba99f798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "244efc2c-cc76-4b75-a1f2-26db90e15016",
                    "LayerId": "ac83a7d7-0dd2-4ba2-a25c-f787ad8ce27a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac83a7d7-0dd2-4ba2-a25c-f787ad8ce27a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a2002b5-6248-4fc3-978d-aebcd3cbc920",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}