{
    "id": "438de8ff-36bc-439f-ac77-086216a7d335",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35d2f79a-84a1-409f-ad5e-2eb7323bcc79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "438de8ff-36bc-439f-ac77-086216a7d335",
            "compositeImage": {
                "id": "3bf811a4-bfa4-479b-94a9-b68f1e9e1944",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d2f79a-84a1-409f-ad5e-2eb7323bcc79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "015f134e-401d-4f06-b48f-f721b748e668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d2f79a-84a1-409f-ad5e-2eb7323bcc79",
                    "LayerId": "3ab8d03a-6ed8-410d-93b6-12b6201e7e37"
                }
            ]
        },
        {
            "id": "115dff83-5e74-4a47-a1bf-d854fd1aada4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "438de8ff-36bc-439f-ac77-086216a7d335",
            "compositeImage": {
                "id": "7f2cb5e0-6eef-457a-8915-3ea405847ce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115dff83-5e74-4a47-a1bf-d854fd1aada4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b70b4d-1e44-471e-aea5-200290bfc2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115dff83-5e74-4a47-a1bf-d854fd1aada4",
                    "LayerId": "3ab8d03a-6ed8-410d-93b6-12b6201e7e37"
                }
            ]
        },
        {
            "id": "6171fa00-79b2-47a1-b948-d24430df045a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "438de8ff-36bc-439f-ac77-086216a7d335",
            "compositeImage": {
                "id": "d5e5e8fb-f78e-4144-a4eb-1c25168e5df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6171fa00-79b2-47a1-b948-d24430df045a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c40ac17b-fba4-4a02-91ab-3bdd43e443a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6171fa00-79b2-47a1-b948-d24430df045a",
                    "LayerId": "3ab8d03a-6ed8-410d-93b6-12b6201e7e37"
                }
            ]
        },
        {
            "id": "4feb719e-0b19-4a6e-947b-394e277c79f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "438de8ff-36bc-439f-ac77-086216a7d335",
            "compositeImage": {
                "id": "4151b042-828a-4c27-8ab9-2adffb0e6b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4feb719e-0b19-4a6e-947b-394e277c79f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f7de9a-bc48-4c71-887e-001f87603f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4feb719e-0b19-4a6e-947b-394e277c79f1",
                    "LayerId": "3ab8d03a-6ed8-410d-93b6-12b6201e7e37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3ab8d03a-6ed8-410d-93b6-12b6201e7e37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "438de8ff-36bc-439f-ac77-086216a7d335",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}