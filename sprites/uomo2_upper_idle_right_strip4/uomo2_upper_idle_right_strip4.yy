{
    "id": "087432a6-282b-4378-b59d-309f59df910e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33be44d5-eb63-4031-8e0d-166731673c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087432a6-282b-4378-b59d-309f59df910e",
            "compositeImage": {
                "id": "6f2c1292-0d9f-4e31-a97b-c8d2c1c4f199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33be44d5-eb63-4031-8e0d-166731673c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658d0ee6-5490-410e-be94-b0c5d5f60f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33be44d5-eb63-4031-8e0d-166731673c5c",
                    "LayerId": "5ff95dd4-067b-4137-916b-f1ca361179a7"
                }
            ]
        },
        {
            "id": "239d2db8-2085-4032-90a6-b7fcc7802628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087432a6-282b-4378-b59d-309f59df910e",
            "compositeImage": {
                "id": "73325660-4d30-470a-99bc-1cde1de8cf19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "239d2db8-2085-4032-90a6-b7fcc7802628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d767def-7fc2-4447-817f-7b759c39b407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239d2db8-2085-4032-90a6-b7fcc7802628",
                    "LayerId": "5ff95dd4-067b-4137-916b-f1ca361179a7"
                }
            ]
        },
        {
            "id": "0e9fb403-89d2-4ad8-8cf0-1121438f41f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087432a6-282b-4378-b59d-309f59df910e",
            "compositeImage": {
                "id": "ff8e10b7-f5c0-4771-93a1-29f5f64c7d93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e9fb403-89d2-4ad8-8cf0-1121438f41f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3a87446-c16b-4714-8c2c-e8beb53aee8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e9fb403-89d2-4ad8-8cf0-1121438f41f9",
                    "LayerId": "5ff95dd4-067b-4137-916b-f1ca361179a7"
                }
            ]
        },
        {
            "id": "b7a55cb9-2c23-4a0c-aed4-c72ceda444cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087432a6-282b-4378-b59d-309f59df910e",
            "compositeImage": {
                "id": "39676dbf-8717-46a7-8404-ac54061cfc21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a55cb9-2c23-4a0c-aed4-c72ceda444cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d4db427-4b88-40b2-8a5f-f12fa5afb058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a55cb9-2c23-4a0c-aed4-c72ceda444cb",
                    "LayerId": "5ff95dd4-067b-4137-916b-f1ca361179a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ff95dd4-067b-4137-916b-f1ca361179a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "087432a6-282b-4378-b59d-309f59df910e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}