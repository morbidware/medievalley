{
    "id": "5492f431-e240-4ad4-a819-ad1d89ac31d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 14,
    "bbox_right": 33,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84381b63-f513-40b6-9cad-b612c8bcdb7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5492f431-e240-4ad4-a819-ad1d89ac31d2",
            "compositeImage": {
                "id": "df1fda34-1604-4962-b048-c2dfb4aa35c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84381b63-f513-40b6-9cad-b612c8bcdb7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4011d884-05b8-45aa-8268-0e09c32cd9d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84381b63-f513-40b6-9cad-b612c8bcdb7e",
                    "LayerId": "f24536fa-b74f-4a4e-a4e9-137035aa8b6e"
                }
            ]
        },
        {
            "id": "35ace59e-d459-4570-8a2e-5e10db421ae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5492f431-e240-4ad4-a819-ad1d89ac31d2",
            "compositeImage": {
                "id": "478ac6a4-9424-49ae-bb19-413ec771554d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ace59e-d459-4570-8a2e-5e10db421ae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e08bf3-0c45-467c-9281-9e21f3d03c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ace59e-d459-4570-8a2e-5e10db421ae5",
                    "LayerId": "f24536fa-b74f-4a4e-a4e9-137035aa8b6e"
                }
            ]
        },
        {
            "id": "12e711a4-c3d9-439c-9aed-94476e71a2c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5492f431-e240-4ad4-a819-ad1d89ac31d2",
            "compositeImage": {
                "id": "53202a42-83c2-4d53-813c-cc4b34bbe15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e711a4-c3d9-439c-9aed-94476e71a2c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dcac4e1-0930-449a-9718-8453dbe6b9aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e711a4-c3d9-439c-9aed-94476e71a2c2",
                    "LayerId": "f24536fa-b74f-4a4e-a4e9-137035aa8b6e"
                }
            ]
        },
        {
            "id": "bd729acb-1c60-4b20-be3c-297de6b0c1ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5492f431-e240-4ad4-a819-ad1d89ac31d2",
            "compositeImage": {
                "id": "790e8782-1878-4f8a-9363-75326e17ebf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd729acb-1c60-4b20-be3c-297de6b0c1ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef7ba909-a922-4f8a-beba-8f971ed0858a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd729acb-1c60-4b20-be3c-297de6b0c1ca",
                    "LayerId": "f24536fa-b74f-4a4e-a4e9-137035aa8b6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "f24536fa-b74f-4a4e-a4e9-137035aa8b6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5492f431-e240-4ad4-a819-ad1d89ac31d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}