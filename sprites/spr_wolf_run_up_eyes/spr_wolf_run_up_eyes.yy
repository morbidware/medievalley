{
    "id": "ea893a96-f190-4c14-a363-e181af27bbe2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_run_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e86b02c4-0e7d-4004-b1ed-3088efa5d80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea893a96-f190-4c14-a363-e181af27bbe2",
            "compositeImage": {
                "id": "1d0aa19c-6abb-402c-b440-d8388b6dfe8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86b02c4-0e7d-4004-b1ed-3088efa5d80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0380c1c3-0950-4c23-a675-469974bb4160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86b02c4-0e7d-4004-b1ed-3088efa5d80b",
                    "LayerId": "29f4d090-dc77-4c6e-9fc8-9fd10371a3ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "29f4d090-dc77-4c6e-9fc8-9fd10371a3ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea893a96-f190-4c14-a363-e181af27bbe2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}