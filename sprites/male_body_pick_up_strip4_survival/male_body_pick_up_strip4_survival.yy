{
    "id": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_pick_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01d121e4-8f1d-40c2-a7d9-6b8f9706f096",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "compositeImage": {
                "id": "b39c67c1-61ea-4c8d-95be-b18baf7bd9d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01d121e4-8f1d-40c2-a7d9-6b8f9706f096",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4096afc6-4666-4f32-9b94-6beefda32040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01d121e4-8f1d-40c2-a7d9-6b8f9706f096",
                    "LayerId": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf"
                }
            ]
        },
        {
            "id": "5d492674-c08a-4f89-af83-046ae56b80ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "compositeImage": {
                "id": "4835f783-2be2-454a-b0ae-5f760b0a2c4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d492674-c08a-4f89-af83-046ae56b80ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51e65a89-30e1-4326-af9c-a73dee937d30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d492674-c08a-4f89-af83-046ae56b80ed",
                    "LayerId": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf"
                }
            ]
        },
        {
            "id": "0fb71af6-0702-48af-8525-f71b87382f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "compositeImage": {
                "id": "34c5b61e-8d3d-422a-a87c-26c900259792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fb71af6-0702-48af-8525-f71b87382f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e70b9e-3973-4047-b82a-3723cba0e9bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fb71af6-0702-48af-8525-f71b87382f06",
                    "LayerId": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf"
                }
            ]
        },
        {
            "id": "e9900ec2-6c56-447b-ae9e-3fc650830d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "compositeImage": {
                "id": "0c31ae57-9124-4f57-ba63-84f796ce959f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9900ec2-6c56-447b-ae9e-3fc650830d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea79419-7f2f-4a72-99a7-820095ef8aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9900ec2-6c56-447b-ae9e-3fc650830d30",
                    "LayerId": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf"
                }
            ]
        },
        {
            "id": "8cc9fd5f-5415-47f1-8fd7-67f723344af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "compositeImage": {
                "id": "e6c1d386-c959-4d7d-a5be-243ee1fc0ac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cc9fd5f-5415-47f1-8fd7-67f723344af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e756469f-669b-4354-b794-8f6e6a04e792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cc9fd5f-5415-47f1-8fd7-67f723344af7",
                    "LayerId": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf"
                }
            ]
        },
        {
            "id": "1f139b6f-a512-40da-9564-612bdebf38c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "compositeImage": {
                "id": "e321415e-8623-43e2-a9db-e6e4a952ef46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f139b6f-a512-40da-9564-612bdebf38c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8ae5f8-7608-4259-82f3-7b66684b8a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f139b6f-a512-40da-9564-612bdebf38c2",
                    "LayerId": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c8a7cba2-c12e-4c46-ac6d-9a4b3c9055bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52f101fe-83ac-47fe-b06c-5c2ebcf7acef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}