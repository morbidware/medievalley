{
    "id": "7e32847b-97c6-4a24-a2a2-8a04746816e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_sugarbeet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4aa6e369-5224-4a66-8f3e-bd1682e62d66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e32847b-97c6-4a24-a2a2-8a04746816e8",
            "compositeImage": {
                "id": "a09a4ac3-e18b-4419-85aa-10e1922a3616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4aa6e369-5224-4a66-8f3e-bd1682e62d66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "403a4ada-8903-4519-a72f-31da6dcb7b0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4aa6e369-5224-4a66-8f3e-bd1682e62d66",
                    "LayerId": "b9ffd00e-5971-493e-9e73-907d9e26c1cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b9ffd00e-5971-493e-9e73-907d9e26c1cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e32847b-97c6-4a24-a2a2-8a04746816e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}