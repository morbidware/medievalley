{
    "id": "25ad093b-39f5-4d10-b424-c61a02ef6a73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_hoe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a12dfe0e-bdd1-46d6-a2fe-9ce1ba542fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25ad093b-39f5-4d10-b424-c61a02ef6a73",
            "compositeImage": {
                "id": "b6ce052e-947c-4c73-8527-b79477c8a82f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a12dfe0e-bdd1-46d6-a2fe-9ce1ba542fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4571f0d-9581-4744-9788-7b9fc4edcda6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a12dfe0e-bdd1-46d6-a2fe-9ce1ba542fd8",
                    "LayerId": "cae29853-1ad3-48f8-9c5c-18fbd2d22c58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cae29853-1ad3-48f8-9c5c-18fbd2d22c58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25ad093b-39f5-4d10-b424-c61a02ef6a73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}