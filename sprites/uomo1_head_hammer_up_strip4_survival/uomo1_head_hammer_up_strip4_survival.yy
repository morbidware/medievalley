{
    "id": "98a29026-9081-4225-a9be-fe603ab7c9fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_hammer_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d15001b-939f-4e04-8c19-1b8069b64d19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "compositeImage": {
                "id": "f9fcc316-53cc-4326-a390-9bc86267c5dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d15001b-939f-4e04-8c19-1b8069b64d19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33dacd11-7403-44ee-b149-65fa87334826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d15001b-939f-4e04-8c19-1b8069b64d19",
                    "LayerId": "6b696b83-28c3-4c9f-8361-435e96701c19"
                }
            ]
        },
        {
            "id": "fe73973a-bd54-4923-8481-09e898305227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "compositeImage": {
                "id": "905ad256-24aa-404a-b2c9-c4acaf3d1c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe73973a-bd54-4923-8481-09e898305227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60adafea-5548-4436-958c-09d49b65410f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe73973a-bd54-4923-8481-09e898305227",
                    "LayerId": "6b696b83-28c3-4c9f-8361-435e96701c19"
                }
            ]
        },
        {
            "id": "8b6b0f7d-c087-4c39-aad3-1a788f16e79e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "compositeImage": {
                "id": "fe99b375-1df0-4e9f-98c7-e60d3aa38dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b6b0f7d-c087-4c39-aad3-1a788f16e79e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f126ab4-f6f1-441c-a1c2-c58aa42c4357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b6b0f7d-c087-4c39-aad3-1a788f16e79e",
                    "LayerId": "6b696b83-28c3-4c9f-8361-435e96701c19"
                }
            ]
        },
        {
            "id": "b18d5438-11de-4f2c-ab96-bb6ef220d20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "compositeImage": {
                "id": "2b2cb5c2-9ea4-40f6-ac22-fce9630842dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b18d5438-11de-4f2c-ab96-bb6ef220d20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49520239-60f3-456c-b5f6-efbdd361c540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b18d5438-11de-4f2c-ab96-bb6ef220d20a",
                    "LayerId": "6b696b83-28c3-4c9f-8361-435e96701c19"
                }
            ]
        },
        {
            "id": "7172d772-fc8c-4035-bbd5-fe0f91010944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "compositeImage": {
                "id": "1a75cf26-d4ab-47fd-b9ea-005bee7850ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7172d772-fc8c-4035-bbd5-fe0f91010944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66152c7c-060c-4f23-882f-dc128204a5c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7172d772-fc8c-4035-bbd5-fe0f91010944",
                    "LayerId": "6b696b83-28c3-4c9f-8361-435e96701c19"
                }
            ]
        },
        {
            "id": "ddab92ca-c47d-4a08-a3d1-e1076347d396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "compositeImage": {
                "id": "e8471025-f540-47aa-be0e-a688555b85a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddab92ca-c47d-4a08-a3d1-e1076347d396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d6498dc-4d37-4709-aa46-d6cd9f9520d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddab92ca-c47d-4a08-a3d1-e1076347d396",
                    "LayerId": "6b696b83-28c3-4c9f-8361-435e96701c19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b696b83-28c3-4c9f-8361-435e96701c19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98a29026-9081-4225-a9be-fe603ab7c9fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}