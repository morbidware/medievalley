{
    "id": "6c65b223-a690-4537-bcd3-281fa0edbbd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92e05af4-313c-4be5-8002-ba68f9f51910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c65b223-a690-4537-bcd3-281fa0edbbd2",
            "compositeImage": {
                "id": "d9cc15e2-e7a6-4ebf-bdff-2434fecf32b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92e05af4-313c-4be5-8002-ba68f9f51910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee601b91-38b5-4dce-81f9-bc8a1f074c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92e05af4-313c-4be5-8002-ba68f9f51910",
                    "LayerId": "ce311dd1-cb92-45bf-a3ea-3dc041044677"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "ce311dd1-cb92-45bf-a3ea-3dc041044677",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c65b223-a690-4537-bcd3-281fa0edbbd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 125
}