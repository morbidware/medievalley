{
    "id": "af3f3a60-b0a0-4d6f-afbc-39745318ce8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_home_bg_challenge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8187c3bd-cf78-41cf-a36e-4b1bbbb43f00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3f3a60-b0a0-4d6f-afbc-39745318ce8e",
            "compositeImage": {
                "id": "b6af63fc-58e9-4354-9448-3a9cb16caeb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8187c3bd-cf78-41cf-a36e-4b1bbbb43f00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17a623bc-e8a4-49d7-b1cf-942efeebb066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8187c3bd-cf78-41cf-a36e-4b1bbbb43f00",
                    "LayerId": "9db40d58-f77b-4457-9cf0-2d84301a9886"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "9db40d58-f77b-4457-9cf0-2d84301a9886",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af3f3a60-b0a0-4d6f-afbc-39745318ce8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 135
}