{
    "id": "c57ac7be-51cb-453a-ac56-320aa50c15d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_arrow_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39ae5855-3045-44d6-9b50-4f2792b87cdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c57ac7be-51cb-453a-ac56-320aa50c15d4",
            "compositeImage": {
                "id": "fe1b9cf0-d00f-4b61-a746-cd57b9d5e15c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ae5855-3045-44d6-9b50-4f2792b87cdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f04effa-0a30-4b00-acdf-4c273ae37a8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ae5855-3045-44d6-9b50-4f2792b87cdb",
                    "LayerId": "f2394101-6133-4288-b35e-46e67698845c"
                },
                {
                    "id": "19cf8b9d-fc7c-4fe5-9675-8e0adda2be65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ae5855-3045-44d6-9b50-4f2792b87cdb",
                    "LayerId": "d3b26167-f1f1-4425-890e-7a8bc32efc34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f2394101-6133-4288-b35e-46e67698845c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c57ac7be-51cb-453a-ac56-320aa50c15d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d3b26167-f1f1-4425-890e-7a8bc32efc34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c57ac7be-51cb-453a-ac56-320aa50c15d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}