{
    "id": "0d884026-5dd4-4c5e-b77d-819aaccbba2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5914381e-f287-4217-8823-744c740ec186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d884026-5dd4-4c5e-b77d-819aaccbba2b",
            "compositeImage": {
                "id": "9b492e71-b97f-4a86-9d92-a1e2c0378e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5914381e-f287-4217-8823-744c740ec186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db22737-1086-4c2a-add2-82312cd867f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5914381e-f287-4217-8823-744c740ec186",
                    "LayerId": "7cd9f204-bc73-445d-a8a4-23166dd2a9fe"
                }
            ]
        },
        {
            "id": "8d5912ad-fd94-4ebb-b74c-6dc215f8ebec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d884026-5dd4-4c5e-b77d-819aaccbba2b",
            "compositeImage": {
                "id": "cdc9a5db-6edc-4372-88d0-561f6dae3647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5912ad-fd94-4ebb-b74c-6dc215f8ebec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b0956d-0c2a-498e-b3f5-7e163710dabd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5912ad-fd94-4ebb-b74c-6dc215f8ebec",
                    "LayerId": "7cd9f204-bc73-445d-a8a4-23166dd2a9fe"
                }
            ]
        },
        {
            "id": "7d2ff9ec-0784-4901-a66b-cad677fe6ddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d884026-5dd4-4c5e-b77d-819aaccbba2b",
            "compositeImage": {
                "id": "5b74ca36-8b67-48c7-825b-4d1dc34464df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2ff9ec-0784-4901-a66b-cad677fe6ddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fab4dc-6743-4368-adaa-842894c29e03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2ff9ec-0784-4901-a66b-cad677fe6ddb",
                    "LayerId": "7cd9f204-bc73-445d-a8a4-23166dd2a9fe"
                }
            ]
        },
        {
            "id": "aabf5fe7-e9c5-46d8-bb74-1b0862c39fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d884026-5dd4-4c5e-b77d-819aaccbba2b",
            "compositeImage": {
                "id": "c8341464-1f80-4875-a388-13e8b233ffc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabf5fe7-e9c5-46d8-bb74-1b0862c39fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d8d3c97-5c3c-4299-a7b9-0bfe484272ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabf5fe7-e9c5-46d8-bb74-1b0862c39fcb",
                    "LayerId": "7cd9f204-bc73-445d-a8a4-23166dd2a9fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7cd9f204-bc73-445d-a8a4-23166dd2a9fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d884026-5dd4-4c5e-b77d-819aaccbba2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}