{
    "id": "ae26cbd3-4901-4862-8640-63b764e473bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_home_survival_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ed6f336-bc34-4bf8-850f-0ac2d204ec4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "3a3f7aaa-3f1f-48ba-97a6-53fc1e99dc48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ed6f336-bc34-4bf8-850f-0ac2d204ec4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e3dc6b8-da30-456c-b017-75ae72f859af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ed6f336-bc34-4bf8-850f-0ac2d204ec4b",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "ac028f64-37d4-4140-a827-fa7efdcbf5bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "1a722d71-a530-40ec-b5cf-2ebc75c72ec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac028f64-37d4-4140-a827-fa7efdcbf5bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a30f5f-4aae-40a3-9944-1464b896db56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac028f64-37d4-4140-a827-fa7efdcbf5bc",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "1fb33617-bfd2-452f-942f-70718b08e0a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "20e24d30-3595-4c52-bdd9-be36658479f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb33617-bfd2-452f-942f-70718b08e0a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "178a281d-5549-4c5c-b62f-ce869116f8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb33617-bfd2-452f-942f-70718b08e0a5",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "be005d48-29e7-458a-8316-e850bcdeed3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "16c07a0f-7d26-4231-855c-f10d5f7ac939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be005d48-29e7-458a-8316-e850bcdeed3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2156c2ed-3ca5-41e1-bcd6-9de0438cc4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be005d48-29e7-458a-8316-e850bcdeed3c",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "7d328ac8-1e08-415d-bc8d-ba6b8440a845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "dc708e77-d298-456a-9c23-d5f2af1cc076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d328ac8-1e08-415d-bc8d-ba6b8440a845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13b422ba-f765-49a5-a289-38166abbdf85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d328ac8-1e08-415d-bc8d-ba6b8440a845",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "c4b2c123-db3f-4f90-84a5-b9957e11694a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "d81496e7-0dca-440b-beb0-4e431b22f11a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b2c123-db3f-4f90-84a5-b9957e11694a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "978133a3-40f4-4e92-acb6-9708a0cc1d52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b2c123-db3f-4f90-84a5-b9957e11694a",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "01f62b26-f966-447d-a486-dc49735cc8fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "d454f1ed-a62b-44e6-a789-bb6a8e29b49b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01f62b26-f966-447d-a486-dc49735cc8fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73e591f2-712c-47f7-9b42-341dd6915dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01f62b26-f966-447d-a486-dc49735cc8fc",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "296aa514-1ed0-48c0-a7b7-82b06d629c98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "04e895d2-ac00-4c0a-8d79-3b846d9afac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "296aa514-1ed0-48c0-a7b7-82b06d629c98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc0cd6f-449b-4c3c-af53-74afb20d4ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "296aa514-1ed0-48c0-a7b7-82b06d629c98",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "9bafc121-e573-4fcd-a2d1-1585aa641d6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "30d3a560-9366-43d5-b0c2-2b61f5994234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bafc121-e573-4fcd-a2d1-1585aa641d6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b12e223-3690-41d2-8c54-613d584bfabe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bafc121-e573-4fcd-a2d1-1585aa641d6c",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "7fecff40-023a-49bc-8955-961a3ffb408c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "2386915d-ac73-4cdc-b6a1-68ccb014b22e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fecff40-023a-49bc-8955-961a3ffb408c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3b0922-8861-47ca-a8ad-e74b31855fbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fecff40-023a-49bc-8955-961a3ffb408c",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "35c9cc5d-5c8d-4311-aaac-c15d9aa26345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "9bbf47ac-f974-41b6-97d4-d4223dfbce5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35c9cc5d-5c8d-4311-aaac-c15d9aa26345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693908b5-ef7c-4965-b8d1-bdb642c90bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35c9cc5d-5c8d-4311-aaac-c15d9aa26345",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "4046a281-a681-4755-9328-efd57ab8151f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "c96a2343-4846-4895-aa45-2c4376690cdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4046a281-a681-4755-9328-efd57ab8151f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bab6034d-c20c-4850-add3-88702fbc801e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4046a281-a681-4755-9328-efd57ab8151f",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "1977bcd7-5b89-4d49-a0f6-c01388bf20d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "85e86b66-b6b7-468f-a645-3d79cf4984a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1977bcd7-5b89-4d49-a0f6-c01388bf20d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c967d44b-8c4c-4677-bd44-7ffe25326277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1977bcd7-5b89-4d49-a0f6-c01388bf20d2",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "f7ce1312-e35a-4296-8e10-f39890342119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "952bbc17-bee1-4005-9b28-24cb4fbaea47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ce1312-e35a-4296-8e10-f39890342119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bfe268-650c-4a7f-a51f-b0be325dfbce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ce1312-e35a-4296-8e10-f39890342119",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "44bb80d0-a504-40c6-89e1-d3f1a91434ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "f222e998-b89d-4f95-b99d-5d17ecbaade9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44bb80d0-a504-40c6-89e1-d3f1a91434ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af11c14d-3cf1-48fa-90e8-56d2e5370c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44bb80d0-a504-40c6-89e1-d3f1a91434ec",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "82448fff-050a-4a3f-b861-a89bade4f732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "1dc579d5-59e2-4a5f-a547-f03e31876295",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82448fff-050a-4a3f-b861-a89bade4f732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01779658-95a0-4fa1-9dcd-1f86d86bc948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82448fff-050a-4a3f-b861-a89bade4f732",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "0afeeec6-5841-431f-89a0-b890a07075f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "5cf662b4-42f9-4447-8232-5bfc9599ca1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0afeeec6-5841-431f-89a0-b890a07075f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c7c331-c13a-4722-86ef-d7d84cf8985a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0afeeec6-5841-431f-89a0-b890a07075f0",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        },
        {
            "id": "39e74a15-39f5-46b0-8634-d1cf45b1c812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "compositeImage": {
                "id": "20e0edc3-5c46-4c23-b7fa-dacf4d2730f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e74a15-39f5-46b0-8634-d1cf45b1c812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c157bf62-29fb-4cbc-852f-e2958dd88649",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e74a15-39f5-46b0-8634-d1cf45b1c812",
                    "LayerId": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "780be796-4e2d-4e69-bbe2-1a9f3ee052b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae26cbd3-4901-4862-8640-63b764e473bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 135
}