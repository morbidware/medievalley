{
    "id": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_crafting_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "651b17e8-b998-414c-95c4-e5e34869d0ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "compositeImage": {
                "id": "98a7d00b-7720-485d-8002-95dfc54169b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "651b17e8-b998-414c-95c4-e5e34869d0ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5c9068-e074-4bda-a53b-ca7538bdf288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "651b17e8-b998-414c-95c4-e5e34869d0ec",
                    "LayerId": "b793fa32-f1da-44df-ac32-ad3bb6097fb4"
                }
            ]
        },
        {
            "id": "adb18045-c8a1-465c-a7cf-46193172090f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "compositeImage": {
                "id": "2854cb43-75f5-4f36-aec1-41524596b222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adb18045-c8a1-465c-a7cf-46193172090f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df4449b7-f058-4c95-be09-b8782bf66c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb18045-c8a1-465c-a7cf-46193172090f",
                    "LayerId": "b793fa32-f1da-44df-ac32-ad3bb6097fb4"
                }
            ]
        },
        {
            "id": "057fe280-4c57-40ee-9f54-1872e4468ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "compositeImage": {
                "id": "974e4dc8-34f7-489d-a45b-74e96c866639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057fe280-4c57-40ee-9f54-1872e4468ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b73f37-0866-4403-9d55-6abae2778c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057fe280-4c57-40ee-9f54-1872e4468ac1",
                    "LayerId": "b793fa32-f1da-44df-ac32-ad3bb6097fb4"
                }
            ]
        },
        {
            "id": "9c05b152-7dba-476d-94ff-f5ee16cda798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "compositeImage": {
                "id": "24aea918-f187-4d26-b406-959546abbfba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c05b152-7dba-476d-94ff-f5ee16cda798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e639177-2503-4980-9657-403ed3a403f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c05b152-7dba-476d-94ff-f5ee16cda798",
                    "LayerId": "b793fa32-f1da-44df-ac32-ad3bb6097fb4"
                }
            ]
        },
        {
            "id": "93e22d42-2866-40dd-bc34-d2df46f5d687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "compositeImage": {
                "id": "5eabac50-77bc-4219-b004-31350c92d2dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e22d42-2866-40dd-bc34-d2df46f5d687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e4beeb0-1a9e-4caf-998e-ab9b3d3052b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e22d42-2866-40dd-bc34-d2df46f5d687",
                    "LayerId": "b793fa32-f1da-44df-ac32-ad3bb6097fb4"
                }
            ]
        },
        {
            "id": "d9bc6a34-5d4b-4faa-a1a1-9f2c8e17c1b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "compositeImage": {
                "id": "efa1468b-d74e-4616-9f9e-01b8fe477d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9bc6a34-5d4b-4faa-a1a1-9f2c8e17c1b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffb22344-94f9-438f-ab37-21360f1a9e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9bc6a34-5d4b-4faa-a1a1-9f2c8e17c1b0",
                    "LayerId": "b793fa32-f1da-44df-ac32-ad3bb6097fb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b793fa32-f1da-44df-ac32-ad3bb6097fb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7f3443b-74b2-421b-94fd-fde7a0f041aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}