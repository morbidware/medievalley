{
    "id": "9260cc44-a841-4e59-8b19-a14f75c66941",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11297deb-4ca8-4787-bd99-b9f1fb28e0fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9260cc44-a841-4e59-8b19-a14f75c66941",
            "compositeImage": {
                "id": "6fe18cda-76c3-42b1-a850-1cb8a567aa4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11297deb-4ca8-4787-bd99-b9f1fb28e0fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b0dc894-f753-4aa5-aa00-478bffffa6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11297deb-4ca8-4787-bd99-b9f1fb28e0fe",
                    "LayerId": "f8097d6c-a40e-4992-ac69-a4ce194defd4"
                }
            ]
        },
        {
            "id": "b6e978a0-e27c-488c-ac89-aac7e197632f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9260cc44-a841-4e59-8b19-a14f75c66941",
            "compositeImage": {
                "id": "27a227ec-919a-44a7-94ea-9c0c50a81e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e978a0-e27c-488c-ac89-aac7e197632f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ccf37d-e861-4028-9434-f32faf49bf98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e978a0-e27c-488c-ac89-aac7e197632f",
                    "LayerId": "f8097d6c-a40e-4992-ac69-a4ce194defd4"
                }
            ]
        },
        {
            "id": "e755b6ef-0bcd-40b4-a09b-724e1d80ad39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9260cc44-a841-4e59-8b19-a14f75c66941",
            "compositeImage": {
                "id": "2f95cf54-63f9-4e1d-83fc-bab8124cfc8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e755b6ef-0bcd-40b4-a09b-724e1d80ad39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bf31398-390e-405a-9328-e3a937e5fca0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e755b6ef-0bcd-40b4-a09b-724e1d80ad39",
                    "LayerId": "f8097d6c-a40e-4992-ac69-a4ce194defd4"
                }
            ]
        },
        {
            "id": "1158c611-68d2-460b-99dd-73a864d0f5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9260cc44-a841-4e59-8b19-a14f75c66941",
            "compositeImage": {
                "id": "2aa0f161-376a-4e51-93ba-6695cd499bf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1158c611-68d2-460b-99dd-73a864d0f5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0dd0d65-5637-482d-92a3-b3ce52f0ee49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1158c611-68d2-460b-99dd-73a864d0f5ec",
                    "LayerId": "f8097d6c-a40e-4992-ac69-a4ce194defd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f8097d6c-a40e-4992-ac69-a4ce194defd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9260cc44-a841-4e59-8b19-a14f75c66941",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}