{
    "id": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_redbeans_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88f1bd74-6fe2-438a-a4cc-ffeb34cd54d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
            "compositeImage": {
                "id": "261c721f-0a6b-451b-9b4a-e59176762231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f1bd74-6fe2-438a-a4cc-ffeb34cd54d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a1ea3b-35e2-4099-a2db-6b9501ddb776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f1bd74-6fe2-438a-a4cc-ffeb34cd54d7",
                    "LayerId": "fc3dcbc5-e9e1-4f11-b159-650a8c2b9e53"
                }
            ]
        },
        {
            "id": "677af716-d343-42ed-bab9-0d3ab9559036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
            "compositeImage": {
                "id": "2f5bc44b-106f-44c3-b295-1d4900d86148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "677af716-d343-42ed-bab9-0d3ab9559036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea6f7839-f31d-4871-a772-498a769985c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "677af716-d343-42ed-bab9-0d3ab9559036",
                    "LayerId": "fc3dcbc5-e9e1-4f11-b159-650a8c2b9e53"
                }
            ]
        },
        {
            "id": "896d0867-b1d7-4891-bd03-ebf87918ced5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
            "compositeImage": {
                "id": "cec9453d-ce6d-465c-8687-5bc476f9c7f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896d0867-b1d7-4891-bd03-ebf87918ced5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86d63527-9bb0-41a7-a50a-45b12266f2bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896d0867-b1d7-4891-bd03-ebf87918ced5",
                    "LayerId": "fc3dcbc5-e9e1-4f11-b159-650a8c2b9e53"
                }
            ]
        },
        {
            "id": "98561daa-7016-4882-a3be-69e9c604d35e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
            "compositeImage": {
                "id": "551fe75f-7e3a-4f09-ba6b-e6aa62b99650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98561daa-7016-4882-a3be-69e9c604d35e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed05cd6-0e56-4370-88ae-d71dc3139171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98561daa-7016-4882-a3be-69e9c604d35e",
                    "LayerId": "fc3dcbc5-e9e1-4f11-b159-650a8c2b9e53"
                }
            ]
        },
        {
            "id": "01530c8c-e75d-4bdb-9301-6207ab7cb069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
            "compositeImage": {
                "id": "6f25506e-806b-4653-a976-84b2c57aac22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01530c8c-e75d-4bdb-9301-6207ab7cb069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b05b945-65c2-4b10-9c1d-b828cf6e2242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01530c8c-e75d-4bdb-9301-6207ab7cb069",
                    "LayerId": "fc3dcbc5-e9e1-4f11-b159-650a8c2b9e53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc3dcbc5-e9e1-4f11-b159-650a8c2b9e53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bfb13df-7abd-4fd9-a061-b17f0c59632c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 25
}