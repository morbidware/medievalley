{
    "id": "5bb4d84c-de5d-42b9-bc39-199facc84161",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_hammer_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f03fe73f-4591-4570-9a16-9eea63d02ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "compositeImage": {
                "id": "ec36bce1-b874-45cb-a1c2-ab0ab48d868a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f03fe73f-4591-4570-9a16-9eea63d02ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b503133e-6246-4964-92d9-d09ec53f5c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f03fe73f-4591-4570-9a16-9eea63d02ef1",
                    "LayerId": "e9de3854-99d8-4f73-8e22-8004c2fb412a"
                }
            ]
        },
        {
            "id": "aacd29b4-1ee7-47b6-8366-9dc419054dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "compositeImage": {
                "id": "8a9f6168-c789-496c-9dc4-11396a0b14f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aacd29b4-1ee7-47b6-8366-9dc419054dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04bfdc0f-1abf-4c95-84ad-b5ffbb490130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aacd29b4-1ee7-47b6-8366-9dc419054dad",
                    "LayerId": "e9de3854-99d8-4f73-8e22-8004c2fb412a"
                }
            ]
        },
        {
            "id": "5b0cc42d-ac8a-435d-878d-f1530758a9c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "compositeImage": {
                "id": "606e2acd-3c31-4b1c-ac10-798423e14dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0cc42d-ac8a-435d-878d-f1530758a9c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5592a42e-25ee-4f9c-94e5-5859178d8540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0cc42d-ac8a-435d-878d-f1530758a9c7",
                    "LayerId": "e9de3854-99d8-4f73-8e22-8004c2fb412a"
                }
            ]
        },
        {
            "id": "82b06a80-8c41-4168-9c9b-b14da833c097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "compositeImage": {
                "id": "8d63a53a-2ecd-468f-9a3d-6c10a3c434b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82b06a80-8c41-4168-9c9b-b14da833c097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6592dfc8-5fc5-4c0b-94d2-2f80f7e5eb4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82b06a80-8c41-4168-9c9b-b14da833c097",
                    "LayerId": "e9de3854-99d8-4f73-8e22-8004c2fb412a"
                }
            ]
        },
        {
            "id": "74659b04-4629-4fe5-8464-fa5d65555b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "compositeImage": {
                "id": "70e955ab-09ba-4b95-adba-f2fce3321281",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74659b04-4629-4fe5-8464-fa5d65555b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0d29a2-b27e-44de-acc5-d0f2148476a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74659b04-4629-4fe5-8464-fa5d65555b5f",
                    "LayerId": "e9de3854-99d8-4f73-8e22-8004c2fb412a"
                }
            ]
        },
        {
            "id": "822a8a7c-4a12-4d9a-b1cc-ea5423773ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "compositeImage": {
                "id": "93a61f45-b2c4-4914-93d9-4be91e0d3474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822a8a7c-4a12-4d9a-b1cc-ea5423773ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6288013b-43e1-4e9a-97c7-d9d8961d1653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822a8a7c-4a12-4d9a-b1cc-ea5423773ae1",
                    "LayerId": "e9de3854-99d8-4f73-8e22-8004c2fb412a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e9de3854-99d8-4f73-8e22-8004c2fb412a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bb4d84c-de5d-42b9-bc39-199facc84161",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}