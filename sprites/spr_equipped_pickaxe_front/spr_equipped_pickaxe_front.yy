{
    "id": "94ddfc44-1fef-47ba-aaf7-06e76de74e28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_pickaxe_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad5efd4d-8a85-4728-a841-2f8bff63fce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94ddfc44-1fef-47ba-aaf7-06e76de74e28",
            "compositeImage": {
                "id": "200b461b-529d-4973-8255-a79390692853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad5efd4d-8a85-4728-a841-2f8bff63fce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4679353a-5684-4569-9394-1282e0679366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad5efd4d-8a85-4728-a841-2f8bff63fce4",
                    "LayerId": "1c4eefe0-6fda-4312-8bc0-40824c214b9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "1c4eefe0-6fda-4312-8bc0-40824c214b9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94ddfc44-1fef-47ba-aaf7-06e76de74e28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 2,
    "yorig": 18
}