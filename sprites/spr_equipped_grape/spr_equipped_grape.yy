{
    "id": "c1a42422-8f4b-4155-aa41-d7a0b51edc72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_grape",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31dbdd2a-c93d-4048-a6ee-59be57d68480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1a42422-8f4b-4155-aa41-d7a0b51edc72",
            "compositeImage": {
                "id": "0eaf4290-76cc-4f14-83e5-95f42a5cc258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31dbdd2a-c93d-4048-a6ee-59be57d68480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "940123f0-4901-405a-a738-b95025886a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31dbdd2a-c93d-4048-a6ee-59be57d68480",
                    "LayerId": "dfd9baf7-546d-4bcd-a719-ba5fce0edc1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dfd9baf7-546d-4bcd-a719-ba5fce0edc1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1a42422-8f4b-4155-aa41-d7a0b51edc72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}