{
    "id": "f5483d2d-afe6-473b-9290-4a7192a626a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_challengetorch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d60bcd6e-3a6e-4a94-90f5-96b8840ee3dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5483d2d-afe6-473b-9290-4a7192a626a6",
            "compositeImage": {
                "id": "f463c1ef-8040-4189-b17e-eba50167959a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d60bcd6e-3a6e-4a94-90f5-96b8840ee3dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e004d163-075c-4385-a7e5-77e178a481e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d60bcd6e-3a6e-4a94-90f5-96b8840ee3dd",
                    "LayerId": "70fea5e0-c29a-4a70-9762-41b9f9fd55a0"
                }
            ]
        },
        {
            "id": "424fa470-fd24-493c-b39b-eeb5f38c4b0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5483d2d-afe6-473b-9290-4a7192a626a6",
            "compositeImage": {
                "id": "e30c035b-2aa0-4a55-9d1d-6aaf4c447774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "424fa470-fd24-493c-b39b-eeb5f38c4b0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7f6fabf-1244-4ea4-82f2-18440b041510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "424fa470-fd24-493c-b39b-eeb5f38c4b0b",
                    "LayerId": "70fea5e0-c29a-4a70-9762-41b9f9fd55a0"
                }
            ]
        },
        {
            "id": "23e3b54b-38bf-4911-a658-86510c63a03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5483d2d-afe6-473b-9290-4a7192a626a6",
            "compositeImage": {
                "id": "42b92e47-d581-4af6-9d8a-7bffb0326403",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23e3b54b-38bf-4911-a658-86510c63a03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6612b665-545c-48d7-b122-87a711357b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23e3b54b-38bf-4911-a658-86510c63a03b",
                    "LayerId": "70fea5e0-c29a-4a70-9762-41b9f9fd55a0"
                }
            ]
        },
        {
            "id": "2d1becf1-5e85-406f-9508-4344c9087a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5483d2d-afe6-473b-9290-4a7192a626a6",
            "compositeImage": {
                "id": "664b2796-c9e3-4583-963e-921b4d4d1978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d1becf1-5e85-406f-9508-4344c9087a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf0f83e-96f8-4039-852a-e772f4559520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d1becf1-5e85-406f-9508-4344c9087a38",
                    "LayerId": "70fea5e0-c29a-4a70-9762-41b9f9fd55a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "70fea5e0-c29a-4a70-9762-41b9f9fd55a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5483d2d-afe6-473b-9290-4a7192a626a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 47
}