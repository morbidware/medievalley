{
    "id": "444f3f81-7237-4e60-bc4d-11da3a956647",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_attack_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "134bf174-5d11-4b0a-b070-a460dad9c015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "444f3f81-7237-4e60-bc4d-11da3a956647",
            "compositeImage": {
                "id": "056a27a2-a771-4a04-bb9c-7ac0ff97b97e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "134bf174-5d11-4b0a-b070-a460dad9c015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c0dce9-7649-42d9-81b8-717c5a9a27ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "134bf174-5d11-4b0a-b070-a460dad9c015",
                    "LayerId": "f856f832-7750-45e6-9812-ff44ba542cf0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "f856f832-7750-45e6-9812-ff44ba542cf0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "444f3f81-7237-4e60-bc4d-11da3a956647",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}