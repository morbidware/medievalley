{
    "id": "92136367-8ffa-4d2e-b637-e6b55afcd085",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_die_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 15,
    "bbox_right": 30,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eedffe0-7091-4558-87ce-06029f297d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92136367-8ffa-4d2e-b637-e6b55afcd085",
            "compositeImage": {
                "id": "9ad44946-dc80-4842-83b9-10d73d28fa33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eedffe0-7091-4558-87ce-06029f297d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44b12e87-12ef-4f14-befe-15e957bb395e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eedffe0-7091-4558-87ce-06029f297d6a",
                    "LayerId": "c362a259-fa3c-4f32-b421-cc98dc93b765"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "c362a259-fa3c-4f32-b421-cc98dc93b765",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92136367-8ffa-4d2e-b637-e6b55afcd085",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}