{
    "id": "41768e17-185b-4719-8fb8-c9f14afe098a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71db7727-4553-4cde-bace-640508459b54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41768e17-185b-4719-8fb8-c9f14afe098a",
            "compositeImage": {
                "id": "5863bb6a-e496-46a4-81c9-e145b6db60cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71db7727-4553-4cde-bace-640508459b54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe2015f9-7f00-4a17-8215-2a6ed999e81e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71db7727-4553-4cde-bace-640508459b54",
                    "LayerId": "11c36638-4480-46a3-83ec-092c48736b9d"
                }
            ]
        },
        {
            "id": "a9b5c84d-7e5e-4968-a8e0-8c2568b40430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41768e17-185b-4719-8fb8-c9f14afe098a",
            "compositeImage": {
                "id": "394e0ff4-8170-4b9c-a76d-d7d65c836ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9b5c84d-7e5e-4968-a8e0-8c2568b40430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0864bd76-d193-4087-9afa-fa95bbd26bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9b5c84d-7e5e-4968-a8e0-8c2568b40430",
                    "LayerId": "11c36638-4480-46a3-83ec-092c48736b9d"
                }
            ]
        },
        {
            "id": "157b400b-5690-49ec-b44a-a28bb73cb2ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41768e17-185b-4719-8fb8-c9f14afe098a",
            "compositeImage": {
                "id": "e227a5c8-499c-40b8-8213-c6b79e5db2f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "157b400b-5690-49ec-b44a-a28bb73cb2ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e03a8c0e-a8f9-461d-afbd-2803e4e46b8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "157b400b-5690-49ec-b44a-a28bb73cb2ef",
                    "LayerId": "11c36638-4480-46a3-83ec-092c48736b9d"
                }
            ]
        },
        {
            "id": "d61f7afa-c77c-4428-acbe-fbb89f92c54b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41768e17-185b-4719-8fb8-c9f14afe098a",
            "compositeImage": {
                "id": "2d21d0ef-18d2-4aaa-9d38-efb4f49feaec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61f7afa-c77c-4428-acbe-fbb89f92c54b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ffb3a0-3338-4ca3-a8c7-88665e086eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61f7afa-c77c-4428-acbe-fbb89f92c54b",
                    "LayerId": "11c36638-4480-46a3-83ec-092c48736b9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "11c36638-4480-46a3-83ec-092c48736b9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41768e17-185b-4719-8fb8-c9f14afe098a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}