{
    "id": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_standardbold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a88f16c-87ee-44a8-ab35-97f817a786ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "462761cc-afa3-49d9-a4a7-b163ba298a10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a88f16c-87ee-44a8-ab35-97f817a786ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97b35c9a-49cb-4cfd-b211-1a4e2a5fe923",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a88f16c-87ee-44a8-ab35-97f817a786ce",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "3d605a1f-4504-4486-a34c-4dfe687cd3c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "e5c691ad-d07f-42cc-b9cf-31824c4929c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d605a1f-4504-4486-a34c-4dfe687cd3c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acbf0451-7def-4cd9-a1be-50d2f57652dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d605a1f-4504-4486-a34c-4dfe687cd3c9",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f20791ed-c04f-4491-b439-40cc7306025a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "636b91b1-99b7-4516-99b4-b22a5cdb15f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f20791ed-c04f-4491-b439-40cc7306025a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd90d01-3e08-4a18-a580-5e852bd4391f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f20791ed-c04f-4491-b439-40cc7306025a",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "9d51b9ec-95bf-422f-82e6-06b74a8e4038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "7ad70aa9-1f02-4e9e-8450-72e1579f1696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d51b9ec-95bf-422f-82e6-06b74a8e4038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56a1fdbc-bb9f-4601-9362-48f3f87ce3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d51b9ec-95bf-422f-82e6-06b74a8e4038",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "40d0ffbc-1554-4833-8d67-988d09409868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "fc4a9054-0a51-4bde-a696-412f93119a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d0ffbc-1554-4833-8d67-988d09409868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efdd7000-e223-48ca-8809-6727160ffe14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d0ffbc-1554-4833-8d67-988d09409868",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f1fc5d58-0ad6-4439-b47a-960fc92bb922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "d11f465a-ce06-412a-9a5d-24681e10db9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fc5d58-0ad6-4439-b47a-960fc92bb922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5630d70-a49b-42d2-b06f-960aed928096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fc5d58-0ad6-4439-b47a-960fc92bb922",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "b8dadd7e-d743-4fa9-91fc-6e985d523d9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "872f3cde-c9e8-47d3-b70a-961746334592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8dadd7e-d743-4fa9-91fc-6e985d523d9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05433c1d-5fab-48bc-9d14-1e304657ae7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8dadd7e-d743-4fa9-91fc-6e985d523d9c",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "03a17032-4c20-4e08-9b5b-5df8f5aaa173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "d3d48fa6-dadb-44b4-b96b-8485aec02284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a17032-4c20-4e08-9b5b-5df8f5aaa173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cbba78-31c4-4ece-bd9b-c371eb5899e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a17032-4c20-4e08-9b5b-5df8f5aaa173",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "dc5978c0-6ede-4d11-a52c-bca7012728b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "63b90ebc-e741-488e-9198-26437d3a1abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5978c0-6ede-4d11-a52c-bca7012728b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f025628-077f-41fb-8264-a81b48589c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5978c0-6ede-4d11-a52c-bca7012728b6",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "dc669c0d-e4fa-4dc1-ae60-b961941edddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "35269ca6-4b6b-4e35-b822-5ab3d6310511",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc669c0d-e4fa-4dc1-ae60-b961941edddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ad84e2b-1dd1-4e22-951d-84954f0e613d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc669c0d-e4fa-4dc1-ae60-b961941edddd",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "c48b380b-d5e6-4730-8354-39412ff810b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "8f551a12-c9d3-4f8b-9122-07eb66461e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48b380b-d5e6-4730-8354-39412ff810b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e184dc5-6f45-4647-9c8b-bd08b15ac3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48b380b-d5e6-4730-8354-39412ff810b5",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "d701f816-055e-4554-bfc5-e84bff13dde2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "99adaeff-d361-41f2-8829-df92aad878b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d701f816-055e-4554-bfc5-e84bff13dde2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1532cbbc-49a9-4a81-b987-ce8a3c142bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d701f816-055e-4554-bfc5-e84bff13dde2",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "1399f926-6632-4ae4-8069-5a0c0e196d21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "fc374e03-9ae7-4606-a81c-eab4602e0f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1399f926-6632-4ae4-8069-5a0c0e196d21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37eabc3-5a6b-460f-8828-fe08d3ccbe55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1399f926-6632-4ae4-8069-5a0c0e196d21",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "2437800f-b347-407a-9806-a6ccd31c14bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "79ab1b1d-cb05-48c8-80ba-80134459fa0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2437800f-b347-407a-9806-a6ccd31c14bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d990bf-9552-4626-91cf-ad1727bbb755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2437800f-b347-407a-9806-a6ccd31c14bd",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "1a05e077-c81f-4876-9827-b8a97268473c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "182180de-b63c-4b3e-a4ac-e3c92554ae43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a05e077-c81f-4876-9827-b8a97268473c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf2b0e02-3f49-4754-bb5e-ae30ab5758c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a05e077-c81f-4876-9827-b8a97268473c",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "a1180ea5-b473-466d-824b-66f1271ecfbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "08b6c9b7-9330-4fd4-b5bd-b2bc2892388d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1180ea5-b473-466d-824b-66f1271ecfbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3859817-9cac-482d-8217-863b94310bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1180ea5-b473-466d-824b-66f1271ecfbe",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "a5012cbe-b079-4ed2-b2f1-b92cbd5f9c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "7e64d583-e42e-498d-a99b-f2cbc9c07733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5012cbe-b079-4ed2-b2f1-b92cbd5f9c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20036b4e-061c-448f-98d1-65288d9f4617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5012cbe-b079-4ed2-b2f1-b92cbd5f9c47",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "e63415da-0af4-48e5-b0d0-af6266035c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "30900520-91c0-4537-8651-28a8ceb88cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e63415da-0af4-48e5-b0d0-af6266035c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ca1d380-50d6-480d-ae4c-25ef92898a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e63415da-0af4-48e5-b0d0-af6266035c19",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "06aa884c-6196-48ee-8a5a-d18d610d1a85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "8945425f-fa1a-4f23-a6c6-72c3ba24b1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06aa884c-6196-48ee-8a5a-d18d610d1a85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fef6b09e-6438-4887-9154-ad148e7e63d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06aa884c-6196-48ee-8a5a-d18d610d1a85",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "114fdc90-e6a2-4f19-999f-441c2ee6ad28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "6aab44ff-268f-4b92-a576-a33c79d0aa9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "114fdc90-e6a2-4f19-999f-441c2ee6ad28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9da3425b-5015-458c-b051-abe7c82f9100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "114fdc90-e6a2-4f19-999f-441c2ee6ad28",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "9d4a5990-dfe0-495e-a205-6e6cab92218f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "b233f301-9c1b-494f-90f4-264dbc40fce5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d4a5990-dfe0-495e-a205-6e6cab92218f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0c499f8-7319-41af-90d3-95d63c10fe08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d4a5990-dfe0-495e-a205-6e6cab92218f",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "67beafdf-df94-4850-b780-35b74c0d715a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "88bae64c-3b18-446e-add0-9f4195c71150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67beafdf-df94-4850-b780-35b74c0d715a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2fa59b4-fe0e-4824-a370-f25bbe5c21a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67beafdf-df94-4850-b780-35b74c0d715a",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "934da740-ac55-490b-8915-4345eed16018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "fc9d626f-b316-4a5a-81e6-f02e4c0d95f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934da740-ac55-490b-8915-4345eed16018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1db5eb6a-46f2-4610-b50e-c874925d4c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934da740-ac55-490b-8915-4345eed16018",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "e1bb69fb-9653-4481-9f94-0a733a58107b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "bb1875ec-4ed8-4fa9-a615-f158b860655d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1bb69fb-9653-4481-9f94-0a733a58107b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e79d7dd-ba34-444e-b808-f88d2e621398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1bb69fb-9653-4481-9f94-0a733a58107b",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "960bb375-e6f6-4ce7-a64f-a278fab63020",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "6d99ba20-d846-4866-81ed-47969b4f4eec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960bb375-e6f6-4ce7-a64f-a278fab63020",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5599b983-dc78-4e72-a686-3b201b399f9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960bb375-e6f6-4ce7-a64f-a278fab63020",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "12e34858-45c6-41ab-935c-5a2f1f0b2844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "a8856900-de7e-4769-96c5-a4040e9afe75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e34858-45c6-41ab-935c-5a2f1f0b2844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d2674c-c524-4d10-9971-7cde8b802d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e34858-45c6-41ab-935c-5a2f1f0b2844",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "e8d30926-446f-4882-a316-6120b8162160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "093f1c83-4407-4188-bed3-86fd12c35336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d30926-446f-4882-a316-6120b8162160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8be41f94-0cee-4fa0-8e45-b88d3d8fc995",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d30926-446f-4882-a316-6120b8162160",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "8b740867-bde0-4e2f-ac6f-9d1976e06c14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "599cfa4b-a095-4b38-a2d4-f96e9ed3518a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b740867-bde0-4e2f-ac6f-9d1976e06c14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec03d2e-f673-4a87-89df-2d028629d48a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b740867-bde0-4e2f-ac6f-9d1976e06c14",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "c8f69b56-8c3a-4d89-8c5f-ea152a59ba65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "fd3688d3-0eca-4689-b383-564565ede3ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f69b56-8c3a-4d89-8c5f-ea152a59ba65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc1fe123-7dd1-4f08-89b1-7c930e10f1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f69b56-8c3a-4d89-8c5f-ea152a59ba65",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "3b201fbb-15d4-4118-9084-769987abd845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "759344b7-3e2a-4b12-9c41-17a4ad5b37d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b201fbb-15d4-4118-9084-769987abd845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "754fe286-7ddd-4819-b074-be63380b7a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b201fbb-15d4-4118-9084-769987abd845",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "2b2e8b97-83ef-4f6b-9410-c7e23c2edf74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "8942f06d-2415-4d2a-8020-1d035362991b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b2e8b97-83ef-4f6b-9410-c7e23c2edf74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b7b834e-c657-48fe-88f3-45df19a01d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b2e8b97-83ef-4f6b-9410-c7e23c2edf74",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "6e40c6ab-90d1-418b-bdeb-21236b091ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "b7c417cf-a36b-49c9-8e1b-407d703d0264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e40c6ab-90d1-418b-bdeb-21236b091ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0cdd54c-e2ae-407a-9918-676fe7f58595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e40c6ab-90d1-418b-bdeb-21236b091ee4",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "3e96b07a-0a0d-4201-8122-45d117de2cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "d48da4fd-a739-459b-8c70-efb1f95efc9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e96b07a-0a0d-4201-8122-45d117de2cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc5ee61-b8a8-4e2b-9687-84a5e8b5390b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e96b07a-0a0d-4201-8122-45d117de2cf0",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "cd29c7e8-9d64-49aa-8dca-b936c9ddad48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "31d4505b-81a9-4696-b22b-53997ba753b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd29c7e8-9d64-49aa-8dca-b936c9ddad48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93502274-9e4e-4cdb-a989-3952c03bfd18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd29c7e8-9d64-49aa-8dca-b936c9ddad48",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f2894bf3-a312-4ac6-9810-96f56e62ded5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "ae4a27fd-8fba-4df3-a417-dabe84b8e3d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2894bf3-a312-4ac6-9810-96f56e62ded5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba834f8e-a51e-4bc8-88b1-0a9a01887c02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2894bf3-a312-4ac6-9810-96f56e62ded5",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "437acaf0-d271-4f86-8271-29f51c51ad16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "197ddc58-b051-47fe-b81d-09d2d5f08b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "437acaf0-d271-4f86-8271-29f51c51ad16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06455295-8970-424c-83b0-091d88ce53bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "437acaf0-d271-4f86-8271-29f51c51ad16",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "fa391bd8-662e-4be8-886e-067e79aeba23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "cbd48c89-a4cd-4426-95f1-fb5429ac5b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa391bd8-662e-4be8-886e-067e79aeba23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97935bec-2bc4-4eac-828c-604d9b866a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa391bd8-662e-4be8-886e-067e79aeba23",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "765118d5-95e9-465a-b406-659dec8a1581",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "506eb62c-14d3-4323-9078-6d1d460b6aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "765118d5-95e9-465a-b406-659dec8a1581",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b44d6b3-0c2f-4dc8-a604-fefcfe9248f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "765118d5-95e9-465a-b406-659dec8a1581",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "32ad3e1d-1b29-4a08-94b2-3bafa5204ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "00975a05-2ed9-4dba-9e79-59cb70e8cdfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32ad3e1d-1b29-4a08-94b2-3bafa5204ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e83f9e0b-3c06-44db-bc22-7a4e6012ca29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32ad3e1d-1b29-4a08-94b2-3bafa5204ffd",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "d0d0a412-7049-4601-bdbe-38a2ebcecf1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "173fb4db-a8e3-41b5-b5ff-3d4f9e5cf28c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d0a412-7049-4601-bdbe-38a2ebcecf1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b34ca4-da9d-4e07-ab69-e9034d47a927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d0a412-7049-4601-bdbe-38a2ebcecf1a",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f192ce99-ad54-4de2-b7a7-f6b24b16514a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "5fddfd05-30df-4ea2-9aa3-fe3b4fa4ee15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f192ce99-ad54-4de2-b7a7-f6b24b16514a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "007aa812-34d9-4fe0-99f7-c47bc10baa59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f192ce99-ad54-4de2-b7a7-f6b24b16514a",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "bdc7392b-a521-4119-ac5e-09bacdf2c171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "51979331-0790-4078-a2b0-bdc328dca8ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdc7392b-a521-4119-ac5e-09bacdf2c171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9b723b1-2942-4252-932e-d6695eda0080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdc7392b-a521-4119-ac5e-09bacdf2c171",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f4b0d955-357d-43c9-b04c-621d7ab5da19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "960253c1-f032-466d-85f8-38e74dd7e597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b0d955-357d-43c9-b04c-621d7ab5da19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaaea4b2-f813-4299-83d7-692c28f7d449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b0d955-357d-43c9-b04c-621d7ab5da19",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "1ec325b1-d462-4366-88c5-d82b2b92973e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "e6fdd83b-0556-4fde-b2b8-615d8b3f28ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ec325b1-d462-4366-88c5-d82b2b92973e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f45e84-d805-4c84-8619-b73898039e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ec325b1-d462-4366-88c5-d82b2b92973e",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "70481a1b-4228-473a-8940-86f71fc899c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "febc9334-cb59-4508-a9ec-3328e89f240e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70481a1b-4228-473a-8940-86f71fc899c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f3e029f-6f11-4c60-aad2-9c39c777ebee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70481a1b-4228-473a-8940-86f71fc899c3",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "e22819f8-877b-45e0-a339-19889a42b992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "9900e445-2876-49b9-ba99-86595fd00cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e22819f8-877b-45e0-a339-19889a42b992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22de8d3-42a7-4236-9d10-79a7b8a42772",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e22819f8-877b-45e0-a339-19889a42b992",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "ebaf6ba4-10f9-40e2-85f3-a5b6f57faf8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "e1901065-84dd-4b14-b7f4-ebe4a0538ac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebaf6ba4-10f9-40e2-85f3-a5b6f57faf8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b6f14a-30d1-4c62-bd7f-76a372670eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebaf6ba4-10f9-40e2-85f3-a5b6f57faf8e",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "4d07530f-4167-48f3-9b16-562ca3932039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "23602dbe-783e-418b-aa8a-2e6ac6fbeb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d07530f-4167-48f3-9b16-562ca3932039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07bb7de1-ef9b-4433-b277-46d41ff7dae8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d07530f-4167-48f3-9b16-562ca3932039",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "3d66985f-f771-473b-8994-7e1188119a1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "fdae5601-6e63-4aae-9d0a-55d78e42c9bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d66985f-f771-473b-8994-7e1188119a1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfdb5db9-abf1-409e-b8b9-f4c8a4996e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d66985f-f771-473b-8994-7e1188119a1d",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f4bcc758-77d8-4251-8475-1a6987987c88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "8a5f30ba-529f-4007-ab91-ba7288341ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4bcc758-77d8-4251-8475-1a6987987c88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "236a607d-062e-40ce-846e-65f1c6617a0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4bcc758-77d8-4251-8475-1a6987987c88",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "a45e4f7e-f8ee-4b72-b38a-66a6bcb36ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "68aad6ba-dd42-47c5-b0ca-84702107ab31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a45e4f7e-f8ee-4b72-b38a-66a6bcb36ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f95770d-4470-46b2-a062-5931a22f5301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a45e4f7e-f8ee-4b72-b38a-66a6bcb36ef5",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "6ef85a9d-9b50-40c9-b4c3-94584c3015c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "f97c46a4-7124-4166-afec-e471df582bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ef85a9d-9b50-40c9-b4c3-94584c3015c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde72671-6387-42ba-9eaf-2f7e4e0eb1e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ef85a9d-9b50-40c9-b4c3-94584c3015c4",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "a15e0793-ecc8-4d0a-ab44-ade665c502d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "edc947ec-8f1b-4b0f-af2b-052d820cc218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a15e0793-ecc8-4d0a-ab44-ade665c502d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86153e27-2214-4205-99dd-adbf78eaa51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a15e0793-ecc8-4d0a-ab44-ade665c502d6",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "6b1a70d5-95f9-4eb6-801c-614de5f7dc9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "d47c03e6-490d-4c32-b770-db29ec7fcdbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1a70d5-95f9-4eb6-801c-614de5f7dc9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5827a13-85aa-476c-b54b-6e7c16b32c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1a70d5-95f9-4eb6-801c-614de5f7dc9f",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "875e8890-e7d5-4abc-9001-11a878f69c71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "25f9cd45-5570-466b-9482-4cc4db8640ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875e8890-e7d5-4abc-9001-11a878f69c71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88f0865-de26-4d84-a42f-b268c5f7c6c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875e8890-e7d5-4abc-9001-11a878f69c71",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "2e112d99-8b8a-45ed-bb75-ea428301360c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "5a38d3fc-7592-44d5-8ce6-b5984193d44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e112d99-8b8a-45ed-bb75-ea428301360c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a331d23-254f-4950-80a4-0ec81be16e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e112d99-8b8a-45ed-bb75-ea428301360c",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "dbc14759-f66d-4be8-8280-5ed80b981227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "70a88be5-8efb-49a8-a202-e106c8061686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbc14759-f66d-4be8-8280-5ed80b981227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a58e3e34-e109-4a7d-b597-fa8fc361aa29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbc14759-f66d-4be8-8280-5ed80b981227",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "fdf434d1-9f21-4cba-a605-4559e8c3e689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "330108b7-6b5f-4213-9b0a-c6235e933186",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf434d1-9f21-4cba-a605-4559e8c3e689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "296744e4-a016-4828-8879-7894a5269650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf434d1-9f21-4cba-a605-4559e8c3e689",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "65e1ca0f-9d27-4f24-8eca-594f934479b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "37625ca2-9e3f-4db3-84bf-fbb755f61ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e1ca0f-9d27-4f24-8eca-594f934479b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482f21ea-190f-4007-ae42-74488ce136b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e1ca0f-9d27-4f24-8eca-594f934479b5",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "36796e15-2b20-4811-abb9-724dcc732abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "b5f768b1-5569-4b9e-b055-0f6a821b498b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36796e15-2b20-4811-abb9-724dcc732abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88114424-6d65-4e20-8b75-58b51b504290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36796e15-2b20-4811-abb9-724dcc732abc",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "c9ce6e86-b61b-4590-8abd-90c3a92246a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "79a21faf-dd21-4ce5-90a2-0f91c67c36ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ce6e86-b61b-4590-8abd-90c3a92246a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004c7791-a286-4fd7-b200-d488df36e142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ce6e86-b61b-4590-8abd-90c3a92246a5",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "df4cbe0f-2909-4a15-96e9-fb29d804536b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "959dfb4f-0167-47c3-8ca9-757516349136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df4cbe0f-2909-4a15-96e9-fb29d804536b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df700df-0953-47bf-814e-2614cfbb0985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df4cbe0f-2909-4a15-96e9-fb29d804536b",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "9fa3264b-bf72-40e9-a8a5-0a193df54500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "c9638600-e319-4b1f-a76b-d978465aea92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fa3264b-bf72-40e9-a8a5-0a193df54500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e42400b9-1cf4-410e-a6c6-0f99438a0776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fa3264b-bf72-40e9-a8a5-0a193df54500",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "fd3bbb18-a292-42db-a168-2453e4569fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "c2d1aa79-49d6-4127-b3d0-8b18bda1b28e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd3bbb18-a292-42db-a168-2453e4569fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c5b373-3126-4c49-8c9c-aecf32f940ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd3bbb18-a292-42db-a168-2453e4569fac",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "15bd7fc6-bf60-4962-a9e1-3093ab9baa17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "cd3f9941-8624-4b89-9b58-a42a475230f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15bd7fc6-bf60-4962-a9e1-3093ab9baa17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b560280e-ea66-4bc1-b369-728fb5d7d4d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15bd7fc6-bf60-4962-a9e1-3093ab9baa17",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "f082c18a-fd20-402e-b16a-726a36bff804",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "5602c7a7-e3e8-48d5-9bc5-497c9b8ef2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f082c18a-fd20-402e-b16a-726a36bff804",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c7ece8-66c3-449a-ad38-e3a40da99945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f082c18a-fd20-402e-b16a-726a36bff804",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "caba8b84-3d79-4a29-9bce-6b0ec7748f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "da46e0e6-78e2-4485-9f41-6007d981233b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caba8b84-3d79-4a29-9bce-6b0ec7748f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e994145-126f-421b-b0bc-46db1f018c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caba8b84-3d79-4a29-9bce-6b0ec7748f79",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "6126fe94-1791-4dee-9b26-34dafe8c650f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "79263444-cbd5-4ee4-9acb-12d0a5c8c3c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6126fe94-1791-4dee-9b26-34dafe8c650f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30bd1a11-58cc-45dd-a3eb-7485ee0a90d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6126fe94-1791-4dee-9b26-34dafe8c650f",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "5ff11d66-87df-4bc9-9884-e1e48bbee801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "bf6968ce-58b0-4fd0-b6ac-0a8105407a86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff11d66-87df-4bc9-9884-e1e48bbee801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5f66747-adda-4a78-8634-05b16c12ab20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff11d66-87df-4bc9-9884-e1e48bbee801",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "d1855a1a-7723-467d-a3e9-e823568f6ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "3fe843da-b968-412f-a9bb-0b1379e007fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1855a1a-7723-467d-a3e9-e823568f6ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44fa8e9c-86c0-4f66-868d-9e78ff335d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1855a1a-7723-467d-a3e9-e823568f6ae1",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "7775035a-45c6-4294-992b-a6e9ea0259b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "aaada865-6ad4-4d4e-87cf-16bbaee8edb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7775035a-45c6-4294-992b-a6e9ea0259b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b075fb10-7598-4d48-a307-a02fe519d158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7775035a-45c6-4294-992b-a6e9ea0259b2",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "8f1252f5-7d69-46ab-8e19-a44dafb3b32e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "c458b9e7-5c6c-4cc9-b8e2-5397b5bde3ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f1252f5-7d69-46ab-8e19-a44dafb3b32e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf231d6e-bc9b-4214-8dca-5f00b6d3cf68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f1252f5-7d69-46ab-8e19-a44dafb3b32e",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "34fb2532-4ca5-4998-a83f-98e3a9d74a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "22926c15-94b2-4d21-abca-831ff41c442c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34fb2532-4ca5-4998-a83f-98e3a9d74a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca43ed79-b0ba-47f7-a4d2-687aa7f5fbf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34fb2532-4ca5-4998-a83f-98e3a9d74a26",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "799bd1eb-e25a-4128-ac5f-dc4114e891fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "95f899d0-fdc1-440e-9db8-36d781fd25e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "799bd1eb-e25a-4128-ac5f-dc4114e891fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc4b52c-4767-479b-8aba-4975f9d2a491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "799bd1eb-e25a-4128-ac5f-dc4114e891fa",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "95a07b83-74ac-40f2-8ced-0acda85e5598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "24022ef4-1153-4af2-8f51-7ad15129cf62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a07b83-74ac-40f2-8ced-0acda85e5598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34b58b7-c7d1-4446-a992-dcd6959420d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a07b83-74ac-40f2-8ced-0acda85e5598",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "63885b9c-a581-43fa-ad4d-e320c5698678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "f4143d13-611f-41ee-8dd7-53eb44897350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63885b9c-a581-43fa-ad4d-e320c5698678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f90029f6-8fbf-43ae-a7c2-7e16afa1d99a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63885b9c-a581-43fa-ad4d-e320c5698678",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "9f0396d0-08d0-4b04-93a1-08924c965e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "d033e7fa-63bf-47b0-9a8a-e88efcef58df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f0396d0-08d0-4b04-93a1-08924c965e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffe088ed-4e47-4c5a-b215-b5291d8b69db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f0396d0-08d0-4b04-93a1-08924c965e58",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "2967a136-19ab-4056-ab84-76a40f76a136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "971f56c4-0457-47d4-9fd4-bd6eafc6837f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2967a136-19ab-4056-ab84-76a40f76a136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e7a311-6706-4e7a-9b5c-5e959f6ac147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2967a136-19ab-4056-ab84-76a40f76a136",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        },
        {
            "id": "7acdba14-a87d-4f38-9044-03aaccb9e252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "compositeImage": {
                "id": "5f17deeb-34c9-4d3d-b290-e7494e267ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7acdba14-a87d-4f38-9044-03aaccb9e252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0c9da97-fc99-4c94-b738-91aab38fe4c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7acdba14-a87d-4f38-9044-03aaccb9e252",
                    "LayerId": "0a76c3ee-6cbb-40aa-a662-b987774b285c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "0a76c3ee-6cbb-40aa-a662-b987774b285c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03c7c49a-ec24-4a3d-b7dd-35253a178817",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}