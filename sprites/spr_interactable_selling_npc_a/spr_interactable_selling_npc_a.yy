{
    "id": "1cea9112-bec0-4cf1-967a-0a0c96f2aaf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_selling_npc_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb9276b2-a14b-4485-a35e-322803b925c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cea9112-bec0-4cf1-967a-0a0c96f2aaf4",
            "compositeImage": {
                "id": "1ee90f7f-4c6b-4cbf-b7e0-c7df93310561",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb9276b2-a14b-4485-a35e-322803b925c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32a7577-4aee-4b2a-9b19-53d2e3d3a14a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb9276b2-a14b-4485-a35e-322803b925c7",
                    "LayerId": "960e6cd8-106e-48ac-bad8-bf47ed073fc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "960e6cd8-106e-48ac-bad8-bf47ed073fc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cea9112-bec0-4cf1-967a-0a0c96f2aaf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}