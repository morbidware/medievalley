{
    "id": "4b780ccb-d5d8-45a1-8c2a-e22a6caf9712",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_idle_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 22,
    "bbox_right": 27,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f88b266d-605c-496a-9df5-f491144b181c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b780ccb-d5d8-45a1-8c2a-e22a6caf9712",
            "compositeImage": {
                "id": "a942b9fa-ebf7-435f-837a-6314fae4d6e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f88b266d-605c-496a-9df5-f491144b181c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63edb953-ea6f-48fe-b420-279ded9fc339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f88b266d-605c-496a-9df5-f491144b181c",
                    "LayerId": "d99f4f2a-f092-49f5-a73c-9118494728cb"
                }
            ]
        },
        {
            "id": "ae76231f-fd49-49c7-84bf-697f3abf04e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b780ccb-d5d8-45a1-8c2a-e22a6caf9712",
            "compositeImage": {
                "id": "57e369fa-7159-4d37-bad6-1649b7274d34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae76231f-fd49-49c7-84bf-697f3abf04e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335b154f-4c28-4fb9-8f8b-63f499cfe804",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae76231f-fd49-49c7-84bf-697f3abf04e0",
                    "LayerId": "d99f4f2a-f092-49f5-a73c-9118494728cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "d99f4f2a-f092-49f5-a73c-9118494728cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b780ccb-d5d8-45a1-8c2a-e22a6caf9712",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}