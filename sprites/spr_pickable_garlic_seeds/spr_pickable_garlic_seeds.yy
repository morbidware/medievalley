{
    "id": "8e771e5d-3bc5-4733-8ce0-45a488297501",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_garlic_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c50fcbc7-8db6-45af-aeaf-f60a8630d6cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e771e5d-3bc5-4733-8ce0-45a488297501",
            "compositeImage": {
                "id": "29335db2-011c-4081-aa3d-df9e9331fbb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50fcbc7-8db6-45af-aeaf-f60a8630d6cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9379e0-bab2-4477-8f12-dd1eb24dd72c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50fcbc7-8db6-45af-aeaf-f60a8630d6cb",
                    "LayerId": "55898e81-27a7-413a-9637-39ea14e51503"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "55898e81-27a7-413a-9637-39ea14e51503",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e771e5d-3bc5-4733-8ce0-45a488297501",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}