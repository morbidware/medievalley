{
    "id": "e0108523-b3d0-482e-969d-fde969eb622e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_basefencedoor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9329a8aa-0176-4d7e-9467-373a56a0289d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0108523-b3d0-482e-969d-fde969eb622e",
            "compositeImage": {
                "id": "27773360-e773-4813-a9f4-f7607d2773e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9329a8aa-0176-4d7e-9467-373a56a0289d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f94929c-5e2c-480e-b1a7-1ea957a9a559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9329a8aa-0176-4d7e-9467-373a56a0289d",
                    "LayerId": "41a21e85-7694-43c3-93c7-8517906d79fc"
                }
            ]
        },
        {
            "id": "4659bce8-1198-4aea-8ed0-1f94f771851a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0108523-b3d0-482e-969d-fde969eb622e",
            "compositeImage": {
                "id": "fdc13207-ebfc-4e22-b438-15b629c2413f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4659bce8-1198-4aea-8ed0-1f94f771851a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ebe0936-b000-403f-93ea-211e3e63ab2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4659bce8-1198-4aea-8ed0-1f94f771851a",
                    "LayerId": "41a21e85-7694-43c3-93c7-8517906d79fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "41a21e85-7694-43c3-93c7-8517906d79fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0108523-b3d0-482e-969d-fde969eb622e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 29
}