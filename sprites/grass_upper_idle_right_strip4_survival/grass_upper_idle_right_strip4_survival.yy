{
    "id": "a0369ae4-c776-4ec7-a5a1-53528f01833d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_idle_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8cd4916-0046-4c72-8c2a-60a40d5a758a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0369ae4-c776-4ec7-a5a1-53528f01833d",
            "compositeImage": {
                "id": "b0df70cd-fede-4b2b-968f-0ad1c02cff02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8cd4916-0046-4c72-8c2a-60a40d5a758a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b42d87-5aae-4699-b7c1-6c441723f21f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8cd4916-0046-4c72-8c2a-60a40d5a758a",
                    "LayerId": "582a6cf4-857a-46ef-9682-6283a9f01b51"
                }
            ]
        },
        {
            "id": "681a54da-d6fe-4649-b3c6-c4ae3bb4edb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0369ae4-c776-4ec7-a5a1-53528f01833d",
            "compositeImage": {
                "id": "de796e84-0a2f-4ce1-923c-8bdff598bb41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "681a54da-d6fe-4649-b3c6-c4ae3bb4edb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8936143c-84ff-44a1-bbc4-9520d456ffaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "681a54da-d6fe-4649-b3c6-c4ae3bb4edb2",
                    "LayerId": "582a6cf4-857a-46ef-9682-6283a9f01b51"
                }
            ]
        },
        {
            "id": "20f46fc5-3917-47b7-aab7-4abe1424f707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0369ae4-c776-4ec7-a5a1-53528f01833d",
            "compositeImage": {
                "id": "b57729d4-b6d9-49d1-8170-f1ec3317922f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20f46fc5-3917-47b7-aab7-4abe1424f707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b01a7da-603f-4ef5-b8fe-9e0f14ddc0f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20f46fc5-3917-47b7-aab7-4abe1424f707",
                    "LayerId": "582a6cf4-857a-46ef-9682-6283a9f01b51"
                }
            ]
        },
        {
            "id": "86661abd-cb20-4e7e-a5c3-b68216bbb8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0369ae4-c776-4ec7-a5a1-53528f01833d",
            "compositeImage": {
                "id": "01c13763-dde2-49df-aa1b-cd981ca264fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86661abd-cb20-4e7e-a5c3-b68216bbb8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9de3593-425b-4862-91a6-cd9e5ea29e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86661abd-cb20-4e7e-a5c3-b68216bbb8ea",
                    "LayerId": "582a6cf4-857a-46ef-9682-6283a9f01b51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "582a6cf4-857a-46ef-9682-6283a9f01b51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0369ae4-c776-4ec7-a5a1-53528f01833d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}