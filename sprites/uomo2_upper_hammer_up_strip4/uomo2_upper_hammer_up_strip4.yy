{
    "id": "43cd966a-0fb9-4ea9-9efa-626d5bfda6c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4118673-18e4-4bb3-b171-f4b652baa81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cd966a-0fb9-4ea9-9efa-626d5bfda6c5",
            "compositeImage": {
                "id": "c3080338-1b10-4522-a24f-121bd9df67c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4118673-18e4-4bb3-b171-f4b652baa81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9227cee-3e51-4473-bb1d-21c6966f1a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4118673-18e4-4bb3-b171-f4b652baa81b",
                    "LayerId": "2a1ae453-119d-4698-bc07-a772bf858424"
                }
            ]
        },
        {
            "id": "31b5a48a-38f1-4d19-b30f-d900a8d91cfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cd966a-0fb9-4ea9-9efa-626d5bfda6c5",
            "compositeImage": {
                "id": "b2e94385-73a7-452f-9238-3f8e5a41fe6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31b5a48a-38f1-4d19-b30f-d900a8d91cfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8285ba53-c93c-4b57-a5c9-201f09cc83a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31b5a48a-38f1-4d19-b30f-d900a8d91cfe",
                    "LayerId": "2a1ae453-119d-4698-bc07-a772bf858424"
                }
            ]
        },
        {
            "id": "4c718cc9-4cbd-4211-a602-99e8aa347aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cd966a-0fb9-4ea9-9efa-626d5bfda6c5",
            "compositeImage": {
                "id": "963ed52f-7161-440f-b6ec-0c78e3ab2c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c718cc9-4cbd-4211-a602-99e8aa347aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f76ae6-7440-4494-baed-6a423224cc75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c718cc9-4cbd-4211-a602-99e8aa347aa5",
                    "LayerId": "2a1ae453-119d-4698-bc07-a772bf858424"
                }
            ]
        },
        {
            "id": "0502739d-f57d-4a79-a475-9294dfacfcb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cd966a-0fb9-4ea9-9efa-626d5bfda6c5",
            "compositeImage": {
                "id": "8fccc8da-b324-41c0-a65d-d6e53ae292ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0502739d-f57d-4a79-a475-9294dfacfcb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5bfd6e-251f-4987-b013-540918c3d770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0502739d-f57d-4a79-a475-9294dfacfcb7",
                    "LayerId": "2a1ae453-119d-4698-bc07-a772bf858424"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2a1ae453-119d-4698-bc07-a772bf858424",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43cd966a-0fb9-4ea9-9efa-626d5bfda6c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}