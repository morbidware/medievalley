{
    "id": "6f941e84-2f16-432c-8488-37dbb8e2dfb8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_basefencedoor_open",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba9f5527-aa83-44b5-9073-1ebb86cd5b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f941e84-2f16-432c-8488-37dbb8e2dfb8",
            "compositeImage": {
                "id": "1354af69-c69c-451d-975d-b93d91b34b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba9f5527-aa83-44b5-9073-1ebb86cd5b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59455ec8-d8ed-4b2f-ace9-85202c07a3ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba9f5527-aa83-44b5-9073-1ebb86cd5b28",
                    "LayerId": "e86865c9-bba9-4f76-9b36-e30ca4a6a0c4"
                }
            ]
        },
        {
            "id": "e0419fcd-62f4-48b5-ac8b-db95129e0d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f941e84-2f16-432c-8488-37dbb8e2dfb8",
            "compositeImage": {
                "id": "03dc3c01-016c-4810-be7f-6b7d40081c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0419fcd-62f4-48b5-ac8b-db95129e0d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838a8c32-ad45-484d-a320-b036d0c21fd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0419fcd-62f4-48b5-ac8b-db95129e0d95",
                    "LayerId": "e86865c9-bba9-4f76-9b36-e30ca4a6a0c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e86865c9-bba9-4f76-9b36-e30ca4a6a0c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f941e84-2f16-432c-8488-37dbb8e2dfb8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 29
}