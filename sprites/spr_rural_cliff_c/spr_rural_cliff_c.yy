{
    "id": "e3c65236-1544-4f5b-b3f7-9d5e2b6e1680",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_cliff_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69aa0c67-a3e8-4aea-8efb-5bc4fb4cde28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3c65236-1544-4f5b-b3f7-9d5e2b6e1680",
            "compositeImage": {
                "id": "55881e6a-fd62-4010-9687-69bbb3550616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69aa0c67-a3e8-4aea-8efb-5bc4fb4cde28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6eb0d09-bfd6-44d1-b784-bf820b61118e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69aa0c67-a3e8-4aea-8efb-5bc4fb4cde28",
                    "LayerId": "8f42fab3-ad6b-45e1-816d-e8fd5c500588"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8f42fab3-ad6b-45e1-816d-e8fd5c500588",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3c65236-1544-4f5b-b3f7-9d5e2b6e1680",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 18,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}