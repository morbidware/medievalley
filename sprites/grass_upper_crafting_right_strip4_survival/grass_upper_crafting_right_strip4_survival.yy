{
    "id": "76af5a79-c369-4396-b115-8b789c105e24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_crafting_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f84cc45-1828-4e20-ae9f-0fdc86b2e4c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "compositeImage": {
                "id": "4716263c-68d5-48bd-afc9-aaa53af545e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f84cc45-1828-4e20-ae9f-0fdc86b2e4c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1abb4e-1b21-4071-81d3-a88ca0243ea8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f84cc45-1828-4e20-ae9f-0fdc86b2e4c1",
                    "LayerId": "ce61c14e-efbc-429a-a37d-9dceb9394ab6"
                }
            ]
        },
        {
            "id": "d1c769e2-3a36-4cca-9b24-7570b8dbc7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "compositeImage": {
                "id": "5e9c30a6-cedf-4a44-ba5b-91821dcf5406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c769e2-3a36-4cca-9b24-7570b8dbc7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29fc2289-f3c5-48f7-9d9f-c4383693ae61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c769e2-3a36-4cca-9b24-7570b8dbc7e0",
                    "LayerId": "ce61c14e-efbc-429a-a37d-9dceb9394ab6"
                }
            ]
        },
        {
            "id": "eef245ee-88e4-403c-bd5f-b05e556ca28e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "compositeImage": {
                "id": "9c61ac0a-de6e-4702-8094-d16d30cca6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef245ee-88e4-403c-bd5f-b05e556ca28e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aea49ff-9a86-4254-a7c7-712c4ff54359",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef245ee-88e4-403c-bd5f-b05e556ca28e",
                    "LayerId": "ce61c14e-efbc-429a-a37d-9dceb9394ab6"
                }
            ]
        },
        {
            "id": "95d16c6c-62fe-40cf-a4b9-0662132a0f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "compositeImage": {
                "id": "b5674c76-7403-4661-a0bd-badc672e412b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d16c6c-62fe-40cf-a4b9-0662132a0f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9a6770-0ff9-4ddc-8720-eb6254d8f04e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d16c6c-62fe-40cf-a4b9-0662132a0f55",
                    "LayerId": "ce61c14e-efbc-429a-a37d-9dceb9394ab6"
                }
            ]
        },
        {
            "id": "f2ac3a81-d1df-479f-a43e-5695ff8e46ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "compositeImage": {
                "id": "ea7c55cb-a37d-4a63-855f-a0e911acfb6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ac3a81-d1df-479f-a43e-5695ff8e46ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e922b0c4-9ab6-434a-b35c-04a4deb41673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ac3a81-d1df-479f-a43e-5695ff8e46ab",
                    "LayerId": "ce61c14e-efbc-429a-a37d-9dceb9394ab6"
                }
            ]
        },
        {
            "id": "8d67871a-0556-4fe9-9cbc-aead459dc8da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "compositeImage": {
                "id": "5d2349c6-f78b-495e-ab7a-828cd3555053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d67871a-0556-4fe9-9cbc-aead459dc8da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cff8bbe-fdd7-472c-a5d4-7cd72825c502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d67871a-0556-4fe9-9cbc-aead459dc8da",
                    "LayerId": "ce61c14e-efbc-429a-a37d-9dceb9394ab6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce61c14e-efbc-429a-a37d-9dceb9394ab6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76af5a79-c369-4396-b115-8b789c105e24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}