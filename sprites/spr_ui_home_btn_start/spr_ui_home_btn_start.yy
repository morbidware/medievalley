{
    "id": "f73fff71-3bab-44d6-aaef-94965bce8bb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_home_btn_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bc175db-6ce4-4a8e-910f-55d71f037645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73fff71-3bab-44d6-aaef-94965bce8bb9",
            "compositeImage": {
                "id": "d358aca5-54c8-426e-90ae-0243b7c6e3f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc175db-6ce4-4a8e-910f-55d71f037645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41ce589-16f5-4558-8d28-2c8866759db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc175db-6ce4-4a8e-910f-55d71f037645",
                    "LayerId": "736374e5-2a33-4e7f-be67-c011d2d26927"
                }
            ]
        },
        {
            "id": "379c0d05-6f09-4147-95fc-e8a8c6dacbb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73fff71-3bab-44d6-aaef-94965bce8bb9",
            "compositeImage": {
                "id": "b9ce7d04-6403-4e00-aced-5390a64632ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "379c0d05-6f09-4147-95fc-e8a8c6dacbb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ce82bbb-ae35-48e4-8e6f-ab91058c7a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379c0d05-6f09-4147-95fc-e8a8c6dacbb2",
                    "LayerId": "736374e5-2a33-4e7f-be67-c011d2d26927"
                }
            ]
        },
        {
            "id": "d37d74a0-ada1-4fbf-ae33-1844b292ce24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f73fff71-3bab-44d6-aaef-94965bce8bb9",
            "compositeImage": {
                "id": "c0d51888-746b-4dd3-b57c-94f74bbf492f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d37d74a0-ada1-4fbf-ae33-1844b292ce24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3492404d-5d71-4931-851a-7e36bc9ad105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d37d74a0-ada1-4fbf-ae33-1844b292ce24",
                    "LayerId": "736374e5-2a33-4e7f-be67-c011d2d26927"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "736374e5-2a33-4e7f-be67-c011d2d26927",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f73fff71-3bab-44d6-aaef-94965bce8bb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 9
}