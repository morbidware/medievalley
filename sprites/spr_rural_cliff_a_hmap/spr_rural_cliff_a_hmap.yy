{
    "id": "e4bb6af4-0163-40e9-9852-a7498fa89022",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_cliff_a_hmap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d76bc27-6a3e-4397-9c7d-e41144d8a56b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bb6af4-0163-40e9-9852-a7498fa89022",
            "compositeImage": {
                "id": "401ff045-b1f5-44a4-be44-a67946dff3ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d76bc27-6a3e-4397-9c7d-e41144d8a56b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a9ba39-d509-4016-8652-2eb54c775495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d76bc27-6a3e-4397-9c7d-e41144d8a56b",
                    "LayerId": "69c7001b-409a-48ad-b21d-4307cf14276a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "69c7001b-409a-48ad-b21d-4307cf14276a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4bb6af4-0163-40e9-9852-a7498fa89022",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 18,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}