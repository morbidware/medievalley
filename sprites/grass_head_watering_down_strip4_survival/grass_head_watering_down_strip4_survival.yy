{
    "id": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_watering_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d6fc71d-e76b-4b8a-be97-c03cdbba76fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "compositeImage": {
                "id": "b3bbafee-7d3e-4578-ae55-138c24254b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6fc71d-e76b-4b8a-be97-c03cdbba76fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad8a9a3-1053-474d-a21e-49a5305d1205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6fc71d-e76b-4b8a-be97-c03cdbba76fb",
                    "LayerId": "fa4aaf5d-d31d-4728-a913-da28b83df9c6"
                }
            ]
        },
        {
            "id": "4e6cdfed-40ea-4a4f-83d7-501e650fc77b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "compositeImage": {
                "id": "398fbe42-0616-40af-96c3-6284b392dfda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6cdfed-40ea-4a4f-83d7-501e650fc77b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9accf8a-d3b9-4493-943d-a63609656d1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6cdfed-40ea-4a4f-83d7-501e650fc77b",
                    "LayerId": "fa4aaf5d-d31d-4728-a913-da28b83df9c6"
                }
            ]
        },
        {
            "id": "2e586b4e-f3f1-4411-ad49-50447a9d6eca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "compositeImage": {
                "id": "f237d42e-8422-4a01-a63b-fbfffaa30316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e586b4e-f3f1-4411-ad49-50447a9d6eca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ac299d3-23e4-4efe-8c67-b449b8a46f3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e586b4e-f3f1-4411-ad49-50447a9d6eca",
                    "LayerId": "fa4aaf5d-d31d-4728-a913-da28b83df9c6"
                }
            ]
        },
        {
            "id": "959f12b4-68d4-4291-ab32-087d23eaffcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "compositeImage": {
                "id": "716f9dde-6e81-4cbf-906a-c0aec8b8136a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "959f12b4-68d4-4291-ab32-087d23eaffcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37153336-a22c-40f9-8938-4553858a6b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "959f12b4-68d4-4291-ab32-087d23eaffcd",
                    "LayerId": "fa4aaf5d-d31d-4728-a913-da28b83df9c6"
                }
            ]
        },
        {
            "id": "85502143-8967-4863-af44-3cfdecf59e15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "compositeImage": {
                "id": "a27c8b89-0820-48d1-ac12-3826687510ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85502143-8967-4863-af44-3cfdecf59e15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f608287a-8770-4969-ad25-06925437a171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85502143-8967-4863-af44-3cfdecf59e15",
                    "LayerId": "fa4aaf5d-d31d-4728-a913-da28b83df9c6"
                }
            ]
        },
        {
            "id": "d3fda1ab-da63-4035-9645-5c54ecc9b668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "compositeImage": {
                "id": "8acf5f6a-dbd5-4a09-bb00-05a4d7500461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3fda1ab-da63-4035-9645-5c54ecc9b668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1c0cec2-7f22-406a-99c6-94fc53de844e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3fda1ab-da63-4035-9645-5c54ecc9b668",
                    "LayerId": "fa4aaf5d-d31d-4728-a913-da28b83df9c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa4aaf5d-d31d-4728-a913-da28b83df9c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e91f1dc-91eb-43c2-9223-c2a8433fd4ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}