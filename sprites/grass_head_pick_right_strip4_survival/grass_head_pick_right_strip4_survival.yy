{
    "id": "df383ef2-3dec-44c5-8021-70310fac2680",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_pick_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f98721be-18b8-4cbc-90dc-a10ef854a121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "compositeImage": {
                "id": "5ad0fed2-4b5f-4aa9-9b20-96713f104ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98721be-18b8-4cbc-90dc-a10ef854a121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a85b789-6e79-4b57-9955-cce970caf29b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98721be-18b8-4cbc-90dc-a10ef854a121",
                    "LayerId": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5"
                }
            ]
        },
        {
            "id": "3c0bd868-0fa7-4993-9261-4db0bb5d44c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "compositeImage": {
                "id": "6702dc13-d55c-41d1-b29a-4c9c6ad679a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c0bd868-0fa7-4993-9261-4db0bb5d44c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e5b04ea-ba9b-4c11-a9bf-60038ec10c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c0bd868-0fa7-4993-9261-4db0bb5d44c1",
                    "LayerId": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5"
                }
            ]
        },
        {
            "id": "563129ea-410b-46ea-ade1-76ef26afbd55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "compositeImage": {
                "id": "b6eb6002-2606-40cf-b428-abe3f6f59f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563129ea-410b-46ea-ade1-76ef26afbd55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5d30f5-e88e-40ad-9412-741eb6e28c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563129ea-410b-46ea-ade1-76ef26afbd55",
                    "LayerId": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5"
                }
            ]
        },
        {
            "id": "8033399c-3af8-4317-8afe-37ee4e321380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "compositeImage": {
                "id": "a5ce61ac-fb65-419a-9afa-45a65662a5b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8033399c-3af8-4317-8afe-37ee4e321380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab5452a1-7be5-4eef-b3bc-54e681ebd851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8033399c-3af8-4317-8afe-37ee4e321380",
                    "LayerId": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5"
                }
            ]
        },
        {
            "id": "7cfa30c3-428b-4c5a-84e4-a522712f4d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "compositeImage": {
                "id": "b3f9ba9a-1713-40db-98cc-8d40063977f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfa30c3-428b-4c5a-84e4-a522712f4d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce2913d0-11c4-404a-85e0-6ce83de6a440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfa30c3-428b-4c5a-84e4-a522712f4d3a",
                    "LayerId": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5"
                }
            ]
        },
        {
            "id": "896e257f-970b-4848-9e46-45cbd93a290e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "compositeImage": {
                "id": "631a0d85-5884-4fdc-8983-dfdae48f14c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "896e257f-970b-4848-9e46-45cbd93a290e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e75d916-700b-4af5-ace4-aa077d9bd77f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "896e257f-970b-4848-9e46-45cbd93a290e",
                    "LayerId": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7f4aa281-92d1-4b83-a3b2-a532d06af1a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df383ef2-3dec-44c5-8021-70310fac2680",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}