{
    "id": "db5d39b2-46f8-4e4f-a507-52b0a51be1fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 3,
    "bbox_right": 17,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdedbc01-9f69-44e4-967e-bdc0ead17f65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db5d39b2-46f8-4e4f-a507-52b0a51be1fe",
            "compositeImage": {
                "id": "6227bc62-cb1c-4d2e-bcd4-29a5ae238032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdedbc01-9f69-44e4-967e-bdc0ead17f65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6cf651-e133-42a7-afef-2a20dc4ff0a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdedbc01-9f69-44e4-967e-bdc0ead17f65",
                    "LayerId": "d783c2d9-6ebc-4ae0-8b07-036e8ca90322"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "d783c2d9-6ebc-4ae0-8b07-036e8ca90322",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db5d39b2-46f8-4e4f-a507-52b0a51be1fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}