{
    "id": "1b51808b-315e-4a97-8c73-b8dea9a4ce93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_suffer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 15,
    "bbox_right": 30,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49e39f23-8252-46b4-b76c-307397cc114b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b51808b-315e-4a97-8c73-b8dea9a4ce93",
            "compositeImage": {
                "id": "9a398ec7-dddc-4bfd-afff-536f092813af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49e39f23-8252-46b4-b76c-307397cc114b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb22445b-db08-4784-bc9b-3f2369c545b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49e39f23-8252-46b4-b76c-307397cc114b",
                    "LayerId": "7d617965-8a8b-4030-bd52-d993b5ad3d45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "7d617965-8a8b-4030-bd52-d993b5ad3d45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b51808b-315e-4a97-8c73-b8dea9a4ce93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}