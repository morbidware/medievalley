{
    "id": "db5d0941-e485-445a-ab2e-23d1c02726ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fd03ff6-9125-4553-b76c-85fb42c97d11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db5d0941-e485-445a-ab2e-23d1c02726ae",
            "compositeImage": {
                "id": "6942127e-e640-4d32-a7b5-4620375056a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd03ff6-9125-4553-b76c-85fb42c97d11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c630f702-398d-4ccb-91d1-c1eafe77d322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd03ff6-9125-4553-b76c-85fb42c97d11",
                    "LayerId": "94e55a59-356d-47cd-9a80-1d9aebabbc15"
                }
            ]
        },
        {
            "id": "19854ebb-b2c7-42a8-abf9-ad9f2bded445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db5d0941-e485-445a-ab2e-23d1c02726ae",
            "compositeImage": {
                "id": "3f79ffae-9c06-4c2b-81e1-d14490bf3c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19854ebb-b2c7-42a8-abf9-ad9f2bded445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f45675-d995-498b-934c-a90939fd6ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19854ebb-b2c7-42a8-abf9-ad9f2bded445",
                    "LayerId": "94e55a59-356d-47cd-9a80-1d9aebabbc15"
                }
            ]
        },
        {
            "id": "78f6e026-5664-4e08-bf43-b5ce1a84a708",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db5d0941-e485-445a-ab2e-23d1c02726ae",
            "compositeImage": {
                "id": "da3e4dff-7332-4bd9-bcad-47036aef960c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78f6e026-5664-4e08-bf43-b5ce1a84a708",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79d7e78-bc90-43d8-9caf-668f5dfe8185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78f6e026-5664-4e08-bf43-b5ce1a84a708",
                    "LayerId": "94e55a59-356d-47cd-9a80-1d9aebabbc15"
                }
            ]
        },
        {
            "id": "defe839e-9eda-4f89-a174-c9250a56b336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db5d0941-e485-445a-ab2e-23d1c02726ae",
            "compositeImage": {
                "id": "ff3576a7-2063-44b9-935e-4a14c054dab7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "defe839e-9eda-4f89-a174-c9250a56b336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6aeb6a9-d448-41a1-b66e-303af5a8af26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "defe839e-9eda-4f89-a174-c9250a56b336",
                    "LayerId": "94e55a59-356d-47cd-9a80-1d9aebabbc15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "94e55a59-356d-47cd-9a80-1d9aebabbc15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db5d0941-e485-445a-ab2e-23d1c02726ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}