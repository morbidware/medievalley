{
    "id": "881766fc-403e-40eb-91b4-ef8305bc4cb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_echinacea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35f7c499-16cf-4d56-bd9b-dba3fa67a076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "881766fc-403e-40eb-91b4-ef8305bc4cb2",
            "compositeImage": {
                "id": "1a27f5f8-7f2d-4001-91d7-4037d18f586c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f7c499-16cf-4d56-bd9b-dba3fa67a076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "704c792c-12e8-4e74-b808-cf9e0249a39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f7c499-16cf-4d56-bd9b-dba3fa67a076",
                    "LayerId": "9f08d413-6683-47df-898f-05a92426be1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9f08d413-6683-47df-898f-05a92426be1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "881766fc-403e-40eb-91b4-ef8305bc4cb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 13
}