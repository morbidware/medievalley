{
    "id": "e74198a3-6ea8-41d8-95a0-f1f0bb20d77a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_task_label",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea980dc5-e568-49cf-9606-0a88ab2aa296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e74198a3-6ea8-41d8-95a0-f1f0bb20d77a",
            "compositeImage": {
                "id": "cb61173d-02e6-4dea-9c5b-381c644659e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea980dc5-e568-49cf-9606-0a88ab2aa296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79a4f254-171c-411c-9650-9728f761263d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea980dc5-e568-49cf-9606-0a88ab2aa296",
                    "LayerId": "bff3a8b6-b876-42e9-a9e3-8d2a3daa9d3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bff3a8b6-b876-42e9-a9e3-8d2a3daa9d3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e74198a3-6ea8-41d8-95a0-f1f0bb20d77a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 48,
    "yorig": 0
}