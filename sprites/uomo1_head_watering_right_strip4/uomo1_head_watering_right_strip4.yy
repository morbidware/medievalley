{
    "id": "1d63af7e-a50d-4077-a4ad-e441a6a46ad8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0f0efcf-5d74-41c9-af3d-d885930f2051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d63af7e-a50d-4077-a4ad-e441a6a46ad8",
            "compositeImage": {
                "id": "e52a8cee-8257-4ec8-acbb-d06d04d0db08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f0efcf-5d74-41c9-af3d-d885930f2051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b39fdd14-d0f5-479a-9352-6abc2de7de86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f0efcf-5d74-41c9-af3d-d885930f2051",
                    "LayerId": "3872b674-5af4-4de8-a9d1-0679545cb60b"
                }
            ]
        },
        {
            "id": "6e65a39e-dfdb-4d4f-967c-481f4c03946d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d63af7e-a50d-4077-a4ad-e441a6a46ad8",
            "compositeImage": {
                "id": "1afb5719-2337-44a7-a4aa-2e706c4c740a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e65a39e-dfdb-4d4f-967c-481f4c03946d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b65b6ed-b782-459c-a118-a73c00436b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e65a39e-dfdb-4d4f-967c-481f4c03946d",
                    "LayerId": "3872b674-5af4-4de8-a9d1-0679545cb60b"
                }
            ]
        },
        {
            "id": "2c61ae42-8bfd-4d7a-8fed-14c68bbd78e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d63af7e-a50d-4077-a4ad-e441a6a46ad8",
            "compositeImage": {
                "id": "86d4bce9-9057-402b-9b38-204081a92d00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c61ae42-8bfd-4d7a-8fed-14c68bbd78e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1462645-2c86-4066-bbcb-8e0f7524d983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c61ae42-8bfd-4d7a-8fed-14c68bbd78e4",
                    "LayerId": "3872b674-5af4-4de8-a9d1-0679545cb60b"
                }
            ]
        },
        {
            "id": "62f745f7-4117-44c2-8911-f3fc6b93433c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d63af7e-a50d-4077-a4ad-e441a6a46ad8",
            "compositeImage": {
                "id": "a9cf33a0-719a-4526-933d-acc807cf6661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f745f7-4117-44c2-8911-f3fc6b93433c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e23ea9-944e-490c-9fb8-ca26a07f1886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f745f7-4117-44c2-8911-f3fc6b93433c",
                    "LayerId": "3872b674-5af4-4de8-a9d1-0679545cb60b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3872b674-5af4-4de8-a9d1-0679545cb60b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d63af7e-a50d-4077-a4ad-e441a6a46ad8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}