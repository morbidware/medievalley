{
    "id": "cac5ec00-7ffc-449e-ab9d-d3229d97d566",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bff4bdc-ed25-4979-91c8-0df63b2d9b44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cac5ec00-7ffc-449e-ab9d-d3229d97d566",
            "compositeImage": {
                "id": "3930fcb9-96a9-4457-b3bb-dd14285584b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bff4bdc-ed25-4979-91c8-0df63b2d9b44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63bbd9d4-9428-43ff-bd33-26a87b0f1b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bff4bdc-ed25-4979-91c8-0df63b2d9b44",
                    "LayerId": "9d17d2e9-9085-4901-95f8-1330ae9b0088"
                }
            ]
        },
        {
            "id": "0aed5b29-a0ad-432d-bbfd-03e5769d49bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cac5ec00-7ffc-449e-ab9d-d3229d97d566",
            "compositeImage": {
                "id": "d3914a79-0050-44f7-b3de-ad8198b993e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aed5b29-a0ad-432d-bbfd-03e5769d49bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29d1d1dc-386c-4488-aaf0-e026909dce42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aed5b29-a0ad-432d-bbfd-03e5769d49bc",
                    "LayerId": "9d17d2e9-9085-4901-95f8-1330ae9b0088"
                }
            ]
        },
        {
            "id": "237e344d-7816-4a58-80d3-d7ffed0821b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cac5ec00-7ffc-449e-ab9d-d3229d97d566",
            "compositeImage": {
                "id": "37c7c529-cc08-498a-a165-fc62f8b69aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237e344d-7816-4a58-80d3-d7ffed0821b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c376997-e004-4695-a754-ee622a6ae2ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237e344d-7816-4a58-80d3-d7ffed0821b7",
                    "LayerId": "9d17d2e9-9085-4901-95f8-1330ae9b0088"
                }
            ]
        },
        {
            "id": "53668b4b-dcc5-4138-aaf6-384084bdf8a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cac5ec00-7ffc-449e-ab9d-d3229d97d566",
            "compositeImage": {
                "id": "d047534d-c1cb-4cf4-9d4c-5f376cf1669d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53668b4b-dcc5-4138-aaf6-384084bdf8a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dfbc300-4c71-4068-9a8c-b22a8ec50897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53668b4b-dcc5-4138-aaf6-384084bdf8a9",
                    "LayerId": "9d17d2e9-9085-4901-95f8-1330ae9b0088"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d17d2e9-9085-4901-95f8-1330ae9b0088",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cac5ec00-7ffc-449e-ab9d-d3229d97d566",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}