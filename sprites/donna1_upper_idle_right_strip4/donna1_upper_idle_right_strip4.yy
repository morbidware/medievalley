{
    "id": "e3c649b6-98a2-421e-b536-7696855303df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb2f558d-7a0f-426f-9559-7afe3bf074dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3c649b6-98a2-421e-b536-7696855303df",
            "compositeImage": {
                "id": "1a67d7ee-9e53-432a-af9f-d3452990540d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb2f558d-7a0f-426f-9559-7afe3bf074dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f5c5bc-ecc8-44e0-9307-cbb82a3ba0cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2f558d-7a0f-426f-9559-7afe3bf074dd",
                    "LayerId": "0a3bcbda-4490-4b65-b54c-845c7e2ba296"
                }
            ]
        },
        {
            "id": "ab9493c3-ca9c-45c6-b24c-dceb17a1c1c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3c649b6-98a2-421e-b536-7696855303df",
            "compositeImage": {
                "id": "339a4af6-c5c1-4d69-b4db-5acdc9243e59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab9493c3-ca9c-45c6-b24c-dceb17a1c1c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a6a98a-4d65-41e1-8519-c3b1f9e7fa47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab9493c3-ca9c-45c6-b24c-dceb17a1c1c7",
                    "LayerId": "0a3bcbda-4490-4b65-b54c-845c7e2ba296"
                }
            ]
        },
        {
            "id": "1c6ddcbf-1f36-4d8a-88a1-8e53b5886286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3c649b6-98a2-421e-b536-7696855303df",
            "compositeImage": {
                "id": "e76970c0-f5a1-4144-abf6-9e49a9612875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6ddcbf-1f36-4d8a-88a1-8e53b5886286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3667e0-9ed2-4ecb-ab64-7e3ade585316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6ddcbf-1f36-4d8a-88a1-8e53b5886286",
                    "LayerId": "0a3bcbda-4490-4b65-b54c-845c7e2ba296"
                }
            ]
        },
        {
            "id": "952d2276-9cc6-47b0-a16a-f26c3d5ff011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3c649b6-98a2-421e-b536-7696855303df",
            "compositeImage": {
                "id": "f2326ee3-fffd-4867-90d6-4f28745343d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "952d2276-9cc6-47b0-a16a-f26c3d5ff011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d90a6d7-47bd-48c9-89b7-c6b80c607567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "952d2276-9cc6-47b0-a16a-f26c3d5ff011",
                    "LayerId": "0a3bcbda-4490-4b65-b54c-845c7e2ba296"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0a3bcbda-4490-4b65-b54c-845c7e2ba296",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3c649b6-98a2-421e-b536-7696855303df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}