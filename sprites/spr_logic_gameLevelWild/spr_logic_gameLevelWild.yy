{
    "id": "4a7aaf16-edb4-4464-87e8-be5ed928b5d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logic_gameLevelWild",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "074b31c3-fbc5-4c9d-b367-d28d1233a308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a7aaf16-edb4-4464-87e8-be5ed928b5d4",
            "compositeImage": {
                "id": "b8dfc231-aa60-4650-a28b-bbe7a749c6fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074b31c3-fbc5-4c9d-b367-d28d1233a308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "556e23a6-89c3-40c1-8d63-9cfe9b617581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074b31c3-fbc5-4c9d-b367-d28d1233a308",
                    "LayerId": "095bb1c1-dda9-4385-b766-8f69ae3a4119"
                },
                {
                    "id": "7063ed2e-1e95-48df-95ef-1032e7fcbdae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074b31c3-fbc5-4c9d-b367-d28d1233a308",
                    "LayerId": "f9cf9406-2c12-4937-b47f-ab4d34904232"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "095bb1c1-dda9-4385-b766-8f69ae3a4119",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a7aaf16-edb4-4464-87e8-be5ed928b5d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "f9cf9406-2c12-4937-b47f-ab4d34904232",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a7aaf16-edb4-4464-87e8-be5ed928b5d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}