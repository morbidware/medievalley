{
    "id": "142a0468-b736-418a-97a8-9c35698dc372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_grasssuit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "feaa585b-5100-4fe6-8c7d-0057cf0a821f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142a0468-b736-418a-97a8-9c35698dc372",
            "compositeImage": {
                "id": "3e9626ae-9a6e-400c-991f-d17badc9eaef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feaa585b-5100-4fe6-8c7d-0057cf0a821f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f20f64f0-0b5b-4afd-bf8d-99898e2bda0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feaa585b-5100-4fe6-8c7d-0057cf0a821f",
                    "LayerId": "937b83f7-c2e3-486b-b9b2-c15e1445a737"
                }
            ]
        },
        {
            "id": "95e930a4-cc29-4002-88ff-9aba9cf05fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142a0468-b736-418a-97a8-9c35698dc372",
            "compositeImage": {
                "id": "40f906ae-40d6-4687-b96c-06b46f64d2e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95e930a4-cc29-4002-88ff-9aba9cf05fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "899853a8-07bd-4bae-be17-d3ef3385666a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95e930a4-cc29-4002-88ff-9aba9cf05fdb",
                    "LayerId": "937b83f7-c2e3-486b-b9b2-c15e1445a737"
                }
            ]
        },
        {
            "id": "96a0a20e-9fa5-427c-8873-ea51f24a365c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142a0468-b736-418a-97a8-9c35698dc372",
            "compositeImage": {
                "id": "9a4a2915-f00e-40b8-8777-2e23f9e12a68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96a0a20e-9fa5-427c-8873-ea51f24a365c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ecf3025-91d4-46d8-90d4-a5238ef7de43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96a0a20e-9fa5-427c-8873-ea51f24a365c",
                    "LayerId": "937b83f7-c2e3-486b-b9b2-c15e1445a737"
                }
            ]
        },
        {
            "id": "7b2f09cd-e13b-4a85-824a-b0360afd68a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142a0468-b736-418a-97a8-9c35698dc372",
            "compositeImage": {
                "id": "33d676f5-2122-4d79-837b-de5e12531e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b2f09cd-e13b-4a85-824a-b0360afd68a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a77534-80ab-416c-8203-4c1b871af49d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b2f09cd-e13b-4a85-824a-b0360afd68a7",
                    "LayerId": "937b83f7-c2e3-486b-b9b2-c15e1445a737"
                }
            ]
        },
        {
            "id": "7b8a0bd0-7988-4a0f-8e15-29bf1aed278a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142a0468-b736-418a-97a8-9c35698dc372",
            "compositeImage": {
                "id": "f4661e61-70bb-4d26-bec5-9353288bed84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b8a0bd0-7988-4a0f-8e15-29bf1aed278a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a4ea08-822f-4cbd-8c8a-907245826445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b8a0bd0-7988-4a0f-8e15-29bf1aed278a",
                    "LayerId": "937b83f7-c2e3-486b-b9b2-c15e1445a737"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "937b83f7-c2e3-486b-b9b2-c15e1445a737",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "142a0468-b736-418a-97a8-9c35698dc372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}