{
    "id": "5aeffe4c-b3fc-4433-acea-c39b0feec58f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_sidebar_survival_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 321,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9703bcb-6320-480f-9c70-4ede2e8f94be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aeffe4c-b3fc-4433-acea-c39b0feec58f",
            "compositeImage": {
                "id": "f82ffe22-33fd-48ed-9760-5c8fc825a33d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9703bcb-6320-480f-9c70-4ede2e8f94be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1736058-ac3e-4ac9-9180-528e48f7b50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9703bcb-6320-480f-9c70-4ede2e8f94be",
                    "LayerId": "6cc0203f-5f70-4134-a46f-6ee2ed38f8cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 322,
    "layers": [
        {
            "id": "6cc0203f-5f70-4134-a46f-6ee2ed38f8cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aeffe4c-b3fc-4433-acea-c39b0feec58f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}