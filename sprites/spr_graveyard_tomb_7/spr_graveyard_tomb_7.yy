{
    "id": "395bd899-e0dc-4d0c-8650-5f0b963c68cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a564540a-c257-40d5-b4c8-e2ead4b27c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "395bd899-e0dc-4d0c-8650-5f0b963c68cc",
            "compositeImage": {
                "id": "c4df6eb6-6143-4495-b2ed-ad57678e7725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a564540a-c257-40d5-b4c8-e2ead4b27c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a6f637-8891-46eb-9e46-fb40c86c7116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a564540a-c257-40d5-b4c8-e2ead4b27c8e",
                    "LayerId": "ca762bcd-9350-4a0c-b18a-57db6ca8401a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "ca762bcd-9350-4a0c-b18a-57db6ca8401a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "395bd899-e0dc-4d0c-8650-5f0b963c68cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 34
}