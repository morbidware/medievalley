{
    "id": "82699ebb-9e5f-457c-addf-af7430a52f66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_redcabbage_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea3373e7-33a4-4dd9-8da4-ea0ab9fe4262",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
            "compositeImage": {
                "id": "6f5b4d30-7350-44fc-93d9-7c688a961d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea3373e7-33a4-4dd9-8da4-ea0ab9fe4262",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6e7505-a2d1-44b0-a76d-c977e562561b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea3373e7-33a4-4dd9-8da4-ea0ab9fe4262",
                    "LayerId": "526164f7-0c68-4814-945a-357852a96b14"
                }
            ]
        },
        {
            "id": "909a1fec-c53f-403f-8880-377b63e4283a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
            "compositeImage": {
                "id": "4b8eb6c3-cda7-4b00-8474-140e5f3583e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "909a1fec-c53f-403f-8880-377b63e4283a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce85823-a4da-486f-a621-1577418fda01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "909a1fec-c53f-403f-8880-377b63e4283a",
                    "LayerId": "526164f7-0c68-4814-945a-357852a96b14"
                }
            ]
        },
        {
            "id": "5153e618-927a-4e27-bfe5-7e5704eca911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
            "compositeImage": {
                "id": "aaa45bed-2a4e-4cd8-b371-b4ba70d34de2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5153e618-927a-4e27-bfe5-7e5704eca911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f16b49-6f06-4856-b977-a83d4ea3eaf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5153e618-927a-4e27-bfe5-7e5704eca911",
                    "LayerId": "526164f7-0c68-4814-945a-357852a96b14"
                }
            ]
        },
        {
            "id": "3bfb474e-8559-4983-88a3-2ea2509c6400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
            "compositeImage": {
                "id": "4c700d35-1fd3-4f8b-9dbe-d190e24cfe23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bfb474e-8559-4983-88a3-2ea2509c6400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dfa0902-9e58-40cc-89f9-f78a25a6ee3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bfb474e-8559-4983-88a3-2ea2509c6400",
                    "LayerId": "526164f7-0c68-4814-945a-357852a96b14"
                }
            ]
        },
        {
            "id": "63510f9e-a555-4237-b776-1fecdc1eba64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
            "compositeImage": {
                "id": "ccc25d68-3bb7-4d4f-bdd5-e038514cfeee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63510f9e-a555-4237-b776-1fecdc1eba64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ca830e6-b5a6-4d8b-946f-cfbfb7a66ec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63510f9e-a555-4237-b776-1fecdc1eba64",
                    "LayerId": "526164f7-0c68-4814-945a-357852a96b14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "526164f7-0c68-4814-945a-357852a96b14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82699ebb-9e5f-457c-addf-af7430a52f66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 11
}