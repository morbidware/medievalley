{
    "id": "15b97822-a1bc-4cff-85cd-177c99b94f50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_gethit_down_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6008c3e1-fe65-4aeb-959e-cb084e965a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15b97822-a1bc-4cff-85cd-177c99b94f50",
            "compositeImage": {
                "id": "f71983a9-1d5c-4d42-889f-a125a17c7342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6008c3e1-fe65-4aeb-959e-cb084e965a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "962445db-5f58-4155-bf7b-6a860055fc8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6008c3e1-fe65-4aeb-959e-cb084e965a9e",
                    "LayerId": "06b13d59-1ff6-4207-b9b0-f2bd3a186fdd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "06b13d59-1ff6-4207-b9b0-f2bd3a186fdd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15b97822-a1bc-4cff-85cd-177c99b94f50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}