{
    "id": "00328621-30fe-4e80-86be-6692ff1cb772",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_housefloor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3ed377f-131e-45d1-9038-5c1785970dff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00328621-30fe-4e80-86be-6692ff1cb772",
            "compositeImage": {
                "id": "eaec25d2-7161-4b5d-bf9d-6a73583e3173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3ed377f-131e-45d1-9038-5c1785970dff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d90a9c-3c2c-4ab9-8cdd-ca7ae02f21b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3ed377f-131e-45d1-9038-5c1785970dff",
                    "LayerId": "d06fd805-71a0-4cd3-8e96-228d0f3f4ed9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d06fd805-71a0-4cd3-8e96-228d0f3f4ed9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00328621-30fe-4e80-86be-6692ff1cb772",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}