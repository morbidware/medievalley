{
    "id": "563800ac-19de-463d-b706-9e204dea4dd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31022692-8c3c-4477-9a90-3d8b28cda6bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "563800ac-19de-463d-b706-9e204dea4dd6",
            "compositeImage": {
                "id": "c7676dac-1363-4477-9460-8e3c799f8e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31022692-8c3c-4477-9a90-3d8b28cda6bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ecd208a-62ca-4d32-a44b-8cb62c907a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31022692-8c3c-4477-9a90-3d8b28cda6bc",
                    "LayerId": "eaa6b984-fcd5-4589-a775-1dff6b8f1489"
                }
            ]
        },
        {
            "id": "1f2d9bc7-37ba-460d-ba72-d1cbbb087732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "563800ac-19de-463d-b706-9e204dea4dd6",
            "compositeImage": {
                "id": "bd4191af-2d4d-4d77-8c4d-428f3530b300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2d9bc7-37ba-460d-ba72-d1cbbb087732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c9ddf8-aac1-492f-8598-773080de2bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2d9bc7-37ba-460d-ba72-d1cbbb087732",
                    "LayerId": "eaa6b984-fcd5-4589-a775-1dff6b8f1489"
                }
            ]
        },
        {
            "id": "a10cb0ff-4610-4727-9ba9-1f0bab87585a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "563800ac-19de-463d-b706-9e204dea4dd6",
            "compositeImage": {
                "id": "613a41c3-73f2-45f4-be63-9fff985d51af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a10cb0ff-4610-4727-9ba9-1f0bab87585a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48346c7b-0177-4ecb-8606-3fcff2672a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a10cb0ff-4610-4727-9ba9-1f0bab87585a",
                    "LayerId": "eaa6b984-fcd5-4589-a775-1dff6b8f1489"
                }
            ]
        },
        {
            "id": "0f00e4f1-1731-453c-b8be-ad81a59afd67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "563800ac-19de-463d-b706-9e204dea4dd6",
            "compositeImage": {
                "id": "3b1472e6-d77f-434a-9c44-954c56814e6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f00e4f1-1731-453c-b8be-ad81a59afd67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac376c38-9181-453e-b6d1-ff6b0f033acf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f00e4f1-1731-453c-b8be-ad81a59afd67",
                    "LayerId": "eaa6b984-fcd5-4589-a775-1dff6b8f1489"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eaa6b984-fcd5-4589-a775-1dff6b8f1489",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "563800ac-19de-463d-b706-9e204dea4dd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}