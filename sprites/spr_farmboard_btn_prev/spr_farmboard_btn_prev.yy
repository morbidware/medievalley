{
    "id": "901defc2-4d46-4445-90af-d73da4e90b9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_farmboard_btn_prev",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9375b6a7-fb89-4bc1-84da-637e57fbb5b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "901defc2-4d46-4445-90af-d73da4e90b9a",
            "compositeImage": {
                "id": "9c7949c6-eaab-466b-9cf2-a722b87cbaaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9375b6a7-fb89-4bc1-84da-637e57fbb5b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07c6f276-e9a5-4232-a5c5-48ce1d17f416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9375b6a7-fb89-4bc1-84da-637e57fbb5b6",
                    "LayerId": "d937a8f1-b7d2-4a01-8538-b5cd3ec8de9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d937a8f1-b7d2-4a01-8538-b5cd3ec8de9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "901defc2-4d46-4445-90af-d73da4e90b9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}