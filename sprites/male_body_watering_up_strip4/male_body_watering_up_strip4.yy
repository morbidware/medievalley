{
    "id": "9d601591-db30-4a06-b738-06bedee4766f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91c99df1-7128-436a-8251-a5ade9fc5eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d601591-db30-4a06-b738-06bedee4766f",
            "compositeImage": {
                "id": "e317fe70-04d7-4d66-b339-2231464c10ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c99df1-7128-436a-8251-a5ade9fc5eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46394f2d-221d-4ffe-90cf-25028be9e754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c99df1-7128-436a-8251-a5ade9fc5eeb",
                    "LayerId": "ec30ef06-2cca-435d-9e2a-4275e54675d3"
                }
            ]
        },
        {
            "id": "4a85aacc-cfe0-49cf-a26e-753d8d35013f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d601591-db30-4a06-b738-06bedee4766f",
            "compositeImage": {
                "id": "dbdef4f2-1d96-4b04-b7b8-4b34d26eb829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a85aacc-cfe0-49cf-a26e-753d8d35013f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f0370ed-e882-49a8-9a1e-acd94f7bf79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a85aacc-cfe0-49cf-a26e-753d8d35013f",
                    "LayerId": "ec30ef06-2cca-435d-9e2a-4275e54675d3"
                }
            ]
        },
        {
            "id": "ef6ddffb-b7b6-4fe1-a022-08b26c2dc3b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d601591-db30-4a06-b738-06bedee4766f",
            "compositeImage": {
                "id": "b0b5bbed-2c02-41e4-b9dd-2b31853f67cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef6ddffb-b7b6-4fe1-a022-08b26c2dc3b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac0c4df-c624-43ff-a49d-bb3681901f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef6ddffb-b7b6-4fe1-a022-08b26c2dc3b9",
                    "LayerId": "ec30ef06-2cca-435d-9e2a-4275e54675d3"
                }
            ]
        },
        {
            "id": "9fc3a726-655b-438c-8e51-5b11233ad2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d601591-db30-4a06-b738-06bedee4766f",
            "compositeImage": {
                "id": "9ae80dc1-615d-4f15-9497-9ac31fa82a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc3a726-655b-438c-8e51-5b11233ad2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec54020-4498-4530-84c9-388a4572647a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc3a726-655b-438c-8e51-5b11233ad2d9",
                    "LayerId": "ec30ef06-2cca-435d-9e2a-4275e54675d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ec30ef06-2cca-435d-9e2a-4275e54675d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d601591-db30-4a06-b738-06bedee4766f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}