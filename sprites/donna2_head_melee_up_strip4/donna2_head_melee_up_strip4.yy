{
    "id": "7474588d-0557-49e7-8b8c-b718758961b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "317cf057-5ea2-4b79-8808-956f7bd43550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7474588d-0557-49e7-8b8c-b718758961b0",
            "compositeImage": {
                "id": "d2bf88a3-d4ea-4e9e-802e-bb6b4aa76563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317cf057-5ea2-4b79-8808-956f7bd43550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "607d38b8-cac8-4ff4-98d6-d0cfd4e94c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317cf057-5ea2-4b79-8808-956f7bd43550",
                    "LayerId": "af26934c-77ea-497d-8c80-f56e942197b9"
                }
            ]
        },
        {
            "id": "d731ff03-899f-45f7-ba98-748e8b06c2fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7474588d-0557-49e7-8b8c-b718758961b0",
            "compositeImage": {
                "id": "3d980fa2-d2ed-47dc-8038-8e249b2b55bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d731ff03-899f-45f7-ba98-748e8b06c2fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92c282cb-c252-475b-b094-8536feefb14c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d731ff03-899f-45f7-ba98-748e8b06c2fe",
                    "LayerId": "af26934c-77ea-497d-8c80-f56e942197b9"
                }
            ]
        },
        {
            "id": "4d26cf7e-368b-4d9e-b6fb-8a2fea376312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7474588d-0557-49e7-8b8c-b718758961b0",
            "compositeImage": {
                "id": "5dcf89d4-8459-4931-abf9-533c4e992854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d26cf7e-368b-4d9e-b6fb-8a2fea376312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f39e66-6ee3-485f-bf52-fe01f7af53d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d26cf7e-368b-4d9e-b6fb-8a2fea376312",
                    "LayerId": "af26934c-77ea-497d-8c80-f56e942197b9"
                }
            ]
        },
        {
            "id": "dafdfaf8-7aa0-49f9-bedd-4908fc1e2827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7474588d-0557-49e7-8b8c-b718758961b0",
            "compositeImage": {
                "id": "84cc495e-f40b-4462-a5bf-51fd1a966fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dafdfaf8-7aa0-49f9-bedd-4908fc1e2827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1183bb87-07db-478c-b35c-c870399e6001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dafdfaf8-7aa0-49f9-bedd-4908fc1e2827",
                    "LayerId": "af26934c-77ea-497d-8c80-f56e942197b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af26934c-77ea-497d-8c80-f56e942197b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7474588d-0557-49e7-8b8c-b718758961b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}