{
    "id": "d1abd3b0-3e00-4c24-98a1-d57d510e743e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_corn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f06674d9-a534-42d4-9496-e017747d7b40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1abd3b0-3e00-4c24-98a1-d57d510e743e",
            "compositeImage": {
                "id": "82a3732b-bb77-40bc-a1e2-c967820030ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06674d9-a534-42d4-9496-e017747d7b40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a47bfb5-1131-433c-a958-0c78682060f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06674d9-a534-42d4-9496-e017747d7b40",
                    "LayerId": "9652bac5-253d-4a29-a67f-09e68b3311a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9652bac5-253d-4a29-a67f-09e68b3311a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1abd3b0-3e00-4c24-98a1-d57d510e743e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}