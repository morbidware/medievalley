{
    "id": "91164b3c-01a8-462f-8bec-0117d3f26189",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfb52f0a-bac4-4e60-9f80-db351effe78e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91164b3c-01a8-462f-8bec-0117d3f26189",
            "compositeImage": {
                "id": "94ea19b8-b69c-4504-b134-ff1bfa8459c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb52f0a-bac4-4e60-9f80-db351effe78e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bacf7d-36bd-4719-92f2-dfe425f2c63e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb52f0a-bac4-4e60-9f80-db351effe78e",
                    "LayerId": "e30bf631-7b9a-4f15-b324-6050351c32af"
                }
            ]
        },
        {
            "id": "f40ed7e4-13d8-4ded-ae5b-a2276780f0a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91164b3c-01a8-462f-8bec-0117d3f26189",
            "compositeImage": {
                "id": "20a4d8b6-f00e-4b02-a805-c507281a8f11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f40ed7e4-13d8-4ded-ae5b-a2276780f0a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4321f6de-2981-4d3b-9d5a-4db358fababe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f40ed7e4-13d8-4ded-ae5b-a2276780f0a6",
                    "LayerId": "e30bf631-7b9a-4f15-b324-6050351c32af"
                }
            ]
        },
        {
            "id": "12c3bf30-d52e-432b-9be2-76292ee84f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91164b3c-01a8-462f-8bec-0117d3f26189",
            "compositeImage": {
                "id": "e54d4499-346d-4f29-a484-dd89d15137b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c3bf30-d52e-432b-9be2-76292ee84f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f86e82e-30a7-4b47-acff-b918a3b6b426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c3bf30-d52e-432b-9be2-76292ee84f8f",
                    "LayerId": "e30bf631-7b9a-4f15-b324-6050351c32af"
                }
            ]
        },
        {
            "id": "beb06a18-c915-43b3-a1db-5a96f212ea33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91164b3c-01a8-462f-8bec-0117d3f26189",
            "compositeImage": {
                "id": "8f5da725-37a6-4f86-aedb-7f5620cf5e8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beb06a18-c915-43b3-a1db-5a96f212ea33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3742b8e4-1713-4293-8a5f-9ac140879abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beb06a18-c915-43b3-a1db-5a96f212ea33",
                    "LayerId": "e30bf631-7b9a-4f15-b324-6050351c32af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e30bf631-7b9a-4f15-b324-6050351c32af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91164b3c-01a8-462f-8bec-0117d3f26189",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}