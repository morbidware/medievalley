{
    "id": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_pick_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99911a91-1bbb-44eb-a906-0e51d42215b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "compositeImage": {
                "id": "98566f8b-abaf-420b-a8a8-7b03adce0d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99911a91-1bbb-44eb-a906-0e51d42215b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc7a8bf9-617e-4505-b37f-8e8ade9da197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99911a91-1bbb-44eb-a906-0e51d42215b9",
                    "LayerId": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7"
                }
            ]
        },
        {
            "id": "df2ff74f-1d35-446d-9f1a-59a274c6e63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "compositeImage": {
                "id": "e85bbfab-5055-4088-8b81-2af758782088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df2ff74f-1d35-446d-9f1a-59a274c6e63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e01820-0fc2-473e-b06d-c2258ab011cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df2ff74f-1d35-446d-9f1a-59a274c6e63d",
                    "LayerId": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7"
                }
            ]
        },
        {
            "id": "492f834e-a91b-430c-828d-78b04bfe641e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "compositeImage": {
                "id": "e34149a1-2bb1-4f95-a448-43007f06bedd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492f834e-a91b-430c-828d-78b04bfe641e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847639b7-c6de-4771-aea7-0a225c290ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492f834e-a91b-430c-828d-78b04bfe641e",
                    "LayerId": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7"
                }
            ]
        },
        {
            "id": "0e93331c-3a23-4e29-8651-17f5e983e0b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "compositeImage": {
                "id": "75b492d5-7280-4ebf-8883-4159e6914f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e93331c-3a23-4e29-8651-17f5e983e0b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1edc67c0-d12a-482a-8c78-f4d41fbbffd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e93331c-3a23-4e29-8651-17f5e983e0b2",
                    "LayerId": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7"
                }
            ]
        },
        {
            "id": "d1a607fc-ec07-4497-9ab6-47af5d5d6f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "compositeImage": {
                "id": "cc217980-51f7-4b73-8e68-059b81b5d60e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1a607fc-ec07-4497-9ab6-47af5d5d6f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfae89ef-2713-4bee-89c6-6420ac4fc3e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1a607fc-ec07-4497-9ab6-47af5d5d6f59",
                    "LayerId": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7"
                }
            ]
        },
        {
            "id": "c78d9733-e7c4-473d-a5f4-8332604f3cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "compositeImage": {
                "id": "e7369bb0-10f8-42eb-8e03-cddab02a9e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c78d9733-e7c4-473d-a5f4-8332604f3cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63958e36-0047-4bef-b14e-cb1ffe1a9cf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c78d9733-e7c4-473d-a5f4-8332604f3cda",
                    "LayerId": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56b37d64-2d62-42b1-9b8f-4e2c3f92d8d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cde13d1d-4fc2-461b-b556-6f09a8658e00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}