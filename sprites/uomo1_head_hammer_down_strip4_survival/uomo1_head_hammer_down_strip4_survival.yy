{
    "id": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_hammer_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a130c889-a8fa-4b69-adf2-413da3cda762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "compositeImage": {
                "id": "5beec2f5-c115-40e0-b610-3d9b6a908c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a130c889-a8fa-4b69-adf2-413da3cda762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15712e80-18d5-4f1b-8eb4-f67368587163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a130c889-a8fa-4b69-adf2-413da3cda762",
                    "LayerId": "b525c707-fa39-4b79-8511-564d8ba0e808"
                }
            ]
        },
        {
            "id": "363d11b1-e64a-4f51-a968-0a5fb751e9db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "compositeImage": {
                "id": "f2063e59-5eef-4f6a-814b-eaa10be5f195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "363d11b1-e64a-4f51-a968-0a5fb751e9db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8aef29-dd4e-459e-8551-71aa0f64905b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "363d11b1-e64a-4f51-a968-0a5fb751e9db",
                    "LayerId": "b525c707-fa39-4b79-8511-564d8ba0e808"
                }
            ]
        },
        {
            "id": "c2825032-6383-4da5-a9d2-7ff043eea7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "compositeImage": {
                "id": "1599a874-5ab8-4d26-9c69-02fd5b6dba3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2825032-6383-4da5-a9d2-7ff043eea7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "669b7318-940b-4bfc-890f-23f2a68c9879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2825032-6383-4da5-a9d2-7ff043eea7a7",
                    "LayerId": "b525c707-fa39-4b79-8511-564d8ba0e808"
                }
            ]
        },
        {
            "id": "d5a394e5-a1bc-4019-84d5-69a31d8ff4a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "compositeImage": {
                "id": "f308152b-32d8-45bb-9a88-67e9a83db9d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a394e5-a1bc-4019-84d5-69a31d8ff4a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be9a0ec-83cf-499a-b725-3937125b5d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a394e5-a1bc-4019-84d5-69a31d8ff4a7",
                    "LayerId": "b525c707-fa39-4b79-8511-564d8ba0e808"
                }
            ]
        },
        {
            "id": "66de1f0e-bf19-466f-b1ec-081fc2123741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "compositeImage": {
                "id": "f0c6cf19-e788-4b1f-ae1c-54c8b71699d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66de1f0e-bf19-466f-b1ec-081fc2123741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc0c620-5290-4659-8b51-a29e4fe8c0bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66de1f0e-bf19-466f-b1ec-081fc2123741",
                    "LayerId": "b525c707-fa39-4b79-8511-564d8ba0e808"
                }
            ]
        },
        {
            "id": "0da8689f-2e54-4479-86e1-0f92f087b633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "compositeImage": {
                "id": "66d54362-27db-47d8-a6ad-1dab4c46a289",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0da8689f-2e54-4479-86e1-0f92f087b633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab1002f6-e691-4cf1-b8c5-505bb266c3d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da8689f-2e54-4479-86e1-0f92f087b633",
                    "LayerId": "b525c707-fa39-4b79-8511-564d8ba0e808"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b525c707-fa39-4b79-8511-564d8ba0e808",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26d9e8ae-1ca7-4dd0-8f65-1fdcee43e912",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}