{
    "id": "9bec5711-936c-47ca-99c6-659f7fbbaabd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cdf49c9-9a7e-452b-9409-cec0ba2cdb59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bec5711-936c-47ca-99c6-659f7fbbaabd",
            "compositeImage": {
                "id": "6a0fb23d-3b84-4ac1-b777-4c634f8525cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdf49c9-9a7e-452b-9409-cec0ba2cdb59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8812a8b-2ab9-4b1f-8e33-27b4b36d3165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdf49c9-9a7e-452b-9409-cec0ba2cdb59",
                    "LayerId": "ba37e696-cd89-4170-b0bd-e266fb103f6d"
                }
            ]
        },
        {
            "id": "91ce7c27-a5e7-42ca-ac72-1174bb1c86d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bec5711-936c-47ca-99c6-659f7fbbaabd",
            "compositeImage": {
                "id": "492d37ff-6bae-46c5-a892-64398f5aed49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ce7c27-a5e7-42ca-ac72-1174bb1c86d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033cdfae-1541-42b6-914d-1d8ac3f9c682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ce7c27-a5e7-42ca-ac72-1174bb1c86d0",
                    "LayerId": "ba37e696-cd89-4170-b0bd-e266fb103f6d"
                }
            ]
        },
        {
            "id": "f6d3747d-d570-4222-b785-a19972187a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bec5711-936c-47ca-99c6-659f7fbbaabd",
            "compositeImage": {
                "id": "15780fb8-543d-443c-97c6-87b944330794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d3747d-d570-4222-b785-a19972187a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bbd39d-2598-4e76-b421-f37586c031ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d3747d-d570-4222-b785-a19972187a9b",
                    "LayerId": "ba37e696-cd89-4170-b0bd-e266fb103f6d"
                }
            ]
        },
        {
            "id": "3d2b05e2-c6f6-4b77-ac5b-efee35ee56c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bec5711-936c-47ca-99c6-659f7fbbaabd",
            "compositeImage": {
                "id": "7dd07990-fcc3-4426-a3e0-49b8042179e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2b05e2-c6f6-4b77-ac5b-efee35ee56c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "756ce3f5-26e5-4f6a-ab13-ca15d105d20c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2b05e2-c6f6-4b77-ac5b-efee35ee56c1",
                    "LayerId": "ba37e696-cd89-4170-b0bd-e266fb103f6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba37e696-cd89-4170-b0bd-e266fb103f6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bec5711-936c-47ca-99c6-659f7fbbaabd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}