{
    "id": "2d63193a-b39c-4a8c-9389-d871730e0608",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f90ec2a-f12c-4712-a994-8a1d7239ba5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d63193a-b39c-4a8c-9389-d871730e0608",
            "compositeImage": {
                "id": "3fdaf9c4-208a-46b0-b27c-3c7f51bf1b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f90ec2a-f12c-4712-a994-8a1d7239ba5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0d2578-8c10-4fe2-971f-4b82422ff236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f90ec2a-f12c-4712-a994-8a1d7239ba5b",
                    "LayerId": "0f79868f-69de-475e-be30-221602862b93"
                }
            ]
        },
        {
            "id": "016c1bf8-76a2-4cab-8de5-6b20f7b6db3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d63193a-b39c-4a8c-9389-d871730e0608",
            "compositeImage": {
                "id": "17e48ea7-f5f7-4830-8dcd-721e8b3216d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016c1bf8-76a2-4cab-8de5-6b20f7b6db3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e0616e-9142-49af-80b8-144bf9b97a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016c1bf8-76a2-4cab-8de5-6b20f7b6db3c",
                    "LayerId": "0f79868f-69de-475e-be30-221602862b93"
                }
            ]
        },
        {
            "id": "004b6c8b-f260-4f6d-a31d-3ad06751e267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d63193a-b39c-4a8c-9389-d871730e0608",
            "compositeImage": {
                "id": "75d3d885-b94c-414e-b523-37a59306f8bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "004b6c8b-f260-4f6d-a31d-3ad06751e267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3c2ed5-1068-44a2-b233-cd7f70a48d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "004b6c8b-f260-4f6d-a31d-3ad06751e267",
                    "LayerId": "0f79868f-69de-475e-be30-221602862b93"
                }
            ]
        },
        {
            "id": "ef4e939a-7251-4077-8dde-4f76be8c5363",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d63193a-b39c-4a8c-9389-d871730e0608",
            "compositeImage": {
                "id": "881603d6-4af7-4d2f-a6bc-2cc7473ec6b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef4e939a-7251-4077-8dde-4f76be8c5363",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa126cd0-7b50-405f-8f2d-67d1df7c49de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef4e939a-7251-4077-8dde-4f76be8c5363",
                    "LayerId": "0f79868f-69de-475e-be30-221602862b93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0f79868f-69de-475e-be30-221602862b93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d63193a-b39c-4a8c-9389-d871730e0608",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}