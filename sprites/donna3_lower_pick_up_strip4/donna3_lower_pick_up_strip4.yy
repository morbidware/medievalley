{
    "id": "3317b91c-a67d-4558-a870-4a3a3def9a28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a04b072-2141-493a-bb56-8617ba7fc8d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3317b91c-a67d-4558-a870-4a3a3def9a28",
            "compositeImage": {
                "id": "97559b36-fee7-4f76-ae56-73d074331b07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a04b072-2141-493a-bb56-8617ba7fc8d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6697f6ef-e203-4b43-8327-bfc59be066ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a04b072-2141-493a-bb56-8617ba7fc8d2",
                    "LayerId": "ad90fbff-f6b5-42a0-89c9-d0b61eb1d362"
                }
            ]
        },
        {
            "id": "49b24cc6-f07a-4a4f-bb13-07105222aada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3317b91c-a67d-4558-a870-4a3a3def9a28",
            "compositeImage": {
                "id": "20e145f1-9e13-41e6-972a-f18814a96b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b24cc6-f07a-4a4f-bb13-07105222aada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cefceada-31b8-48eb-82ac-84e29d33088b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b24cc6-f07a-4a4f-bb13-07105222aada",
                    "LayerId": "ad90fbff-f6b5-42a0-89c9-d0b61eb1d362"
                }
            ]
        },
        {
            "id": "d7ae2b45-4d34-469b-a6b9-5ed9b41abdd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3317b91c-a67d-4558-a870-4a3a3def9a28",
            "compositeImage": {
                "id": "277e0446-a6a4-493b-b6c6-02d476195512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ae2b45-4d34-469b-a6b9-5ed9b41abdd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "250142d2-4703-4a9a-a9e7-118c8594baa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ae2b45-4d34-469b-a6b9-5ed9b41abdd0",
                    "LayerId": "ad90fbff-f6b5-42a0-89c9-d0b61eb1d362"
                }
            ]
        },
        {
            "id": "8a170beb-2dc5-46d6-a6a7-0b2a49aff9f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3317b91c-a67d-4558-a870-4a3a3def9a28",
            "compositeImage": {
                "id": "269278a5-490c-4101-a44f-e094ce9871e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a170beb-2dc5-46d6-a6a7-0b2a49aff9f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "986983fc-c8d4-4a05-ad1e-383454b710f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a170beb-2dc5-46d6-a6a7-0b2a49aff9f9",
                    "LayerId": "ad90fbff-f6b5-42a0-89c9-d0b61eb1d362"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ad90fbff-f6b5-42a0-89c9-d0b61eb1d362",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3317b91c-a67d-4558-a870-4a3a3def9a28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}