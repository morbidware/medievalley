{
    "id": "b350dd7a-f1d1-4220-8a08-e27df8bc9916",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 13,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c46b0951-ed82-4a3f-b7af-b4980e823bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b350dd7a-f1d1-4220-8a08-e27df8bc9916",
            "compositeImage": {
                "id": "50ec4813-614f-4ac0-8339-244bf016e80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46b0951-ed82-4a3f-b7af-b4980e823bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4de684d9-bd33-440e-81d9-cc66b01b6d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46b0951-ed82-4a3f-b7af-b4980e823bd6",
                    "LayerId": "79c07c89-716e-47fd-87e8-39da40fcf838"
                }
            ]
        },
        {
            "id": "5d0c74ee-a321-4032-9c3f-1521507943cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b350dd7a-f1d1-4220-8a08-e27df8bc9916",
            "compositeImage": {
                "id": "f67a407a-47a4-4861-9a42-826f3880843e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0c74ee-a321-4032-9c3f-1521507943cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8398282-9b5e-418f-9834-4dbeaf7676ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0c74ee-a321-4032-9c3f-1521507943cf",
                    "LayerId": "79c07c89-716e-47fd-87e8-39da40fcf838"
                }
            ]
        },
        {
            "id": "b91b9a87-c91d-473e-b8ec-84058092d075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b350dd7a-f1d1-4220-8a08-e27df8bc9916",
            "compositeImage": {
                "id": "6e1cbc9c-44ec-4571-bcce-145cf4df1860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b91b9a87-c91d-473e-b8ec-84058092d075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d7096c6-10c2-4fc9-9d5b-fff1867808f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b91b9a87-c91d-473e-b8ec-84058092d075",
                    "LayerId": "79c07c89-716e-47fd-87e8-39da40fcf838"
                }
            ]
        },
        {
            "id": "c413bb16-eff0-4af7-a9b0-c0d96a67521e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b350dd7a-f1d1-4220-8a08-e27df8bc9916",
            "compositeImage": {
                "id": "b7f700ec-9b8e-4eeb-a721-67143143e979",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c413bb16-eff0-4af7-a9b0-c0d96a67521e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8f13d8-cb29-4423-a669-5b92d2a9b689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c413bb16-eff0-4af7-a9b0-c0d96a67521e",
                    "LayerId": "79c07c89-716e-47fd-87e8-39da40fcf838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "79c07c89-716e-47fd-87e8-39da40fcf838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b350dd7a-f1d1-4220-8a08-e27df8bc9916",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}