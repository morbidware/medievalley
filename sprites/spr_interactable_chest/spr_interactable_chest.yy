{
    "id": "c651e1bf-cd87-4d24-9d71-05702123115c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_chest",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f90e154e-9549-4fc3-83d7-ef38cf32ffc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c651e1bf-cd87-4d24-9d71-05702123115c",
            "compositeImage": {
                "id": "ba7809c6-57a3-4ae5-b32a-fcb497d96a15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f90e154e-9549-4fc3-83d7-ef38cf32ffc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01002e97-d6f8-466e-9ea0-e74ebba8f8db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f90e154e-9549-4fc3-83d7-ef38cf32ffc5",
                    "LayerId": "1dd1e997-4e1f-4a66-8ad1-7f30f838dfb9"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 16,
    "height": 32,
    "layers": [
        {
            "id": "1dd1e997-4e1f-4a66-8ad1-7f30f838dfb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c651e1bf-cd87-4d24-9d71-05702123115c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}