{
    "id": "ebb8d525-1200-4142-99bc-80ae7962d95a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_die_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86d9abfe-dbce-4193-ab37-e3f802b19b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebb8d525-1200-4142-99bc-80ae7962d95a",
            "compositeImage": {
                "id": "6a5d4e68-84a2-433a-a3f9-2b8f6e28c830",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86d9abfe-dbce-4193-ab37-e3f802b19b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bfa2eac-2ac8-4e41-876b-587dd6964ed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86d9abfe-dbce-4193-ab37-e3f802b19b99",
                    "LayerId": "80746a71-c7fb-4a27-a102-b6b9015c1682"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "80746a71-c7fb-4a27-a102-b6b9015c1682",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebb8d525-1200-4142-99bc-80ae7962d95a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}