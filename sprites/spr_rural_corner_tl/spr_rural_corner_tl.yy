{
    "id": "df5a680e-7b78-4146-8f58-e601a680ff75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_corner_tl",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dece184d-ad2c-4fcc-ad28-ba753bf2c22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a680e-7b78-4146-8f58-e601a680ff75",
            "compositeImage": {
                "id": "33ddd363-6e16-48f4-9954-c072085312f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dece184d-ad2c-4fcc-ad28-ba753bf2c22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990eedef-fd26-4038-8a15-721cd0f59208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dece184d-ad2c-4fcc-ad28-ba753bf2c22d",
                    "LayerId": "3cef87b7-4e57-486d-bdd0-ed2aea9c6da8"
                }
            ]
        },
        {
            "id": "9e03d47f-214c-432b-bbb7-d0dade9c4e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a680e-7b78-4146-8f58-e601a680ff75",
            "compositeImage": {
                "id": "51f26a13-5e16-4e18-a9e0-de1bc18bbfad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e03d47f-214c-432b-bbb7-d0dade9c4e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb2ac25-7043-46cc-a6ac-619100ac8354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e03d47f-214c-432b-bbb7-d0dade9c4e6c",
                    "LayerId": "3cef87b7-4e57-486d-bdd0-ed2aea9c6da8"
                }
            ]
        },
        {
            "id": "6ee4cc77-214f-49a9-986d-7a6334a2196f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a680e-7b78-4146-8f58-e601a680ff75",
            "compositeImage": {
                "id": "4787cb4c-7d61-470b-99b7-23d275c00942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ee4cc77-214f-49a9-986d-7a6334a2196f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa95f9a-07d4-4ee0-a812-2ba415fff9f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ee4cc77-214f-49a9-986d-7a6334a2196f",
                    "LayerId": "3cef87b7-4e57-486d-bdd0-ed2aea9c6da8"
                }
            ]
        },
        {
            "id": "65624304-d06b-4de9-8a4f-1e4419051211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a680e-7b78-4146-8f58-e601a680ff75",
            "compositeImage": {
                "id": "2b60ecfe-d15c-4526-81a9-d5eb0a02cf1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65624304-d06b-4de9-8a4f-1e4419051211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdba3408-9b85-4256-b387-6bf3fdee29cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65624304-d06b-4de9-8a4f-1e4419051211",
                    "LayerId": "3cef87b7-4e57-486d-bdd0-ed2aea9c6da8"
                }
            ]
        },
        {
            "id": "82d01b63-b049-4b94-abd2-83fded146a7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df5a680e-7b78-4146-8f58-e601a680ff75",
            "compositeImage": {
                "id": "8b713325-682a-4f2d-a42a-d18e43873e15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d01b63-b049-4b94-abd2-83fded146a7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3185eef0-5b4c-4085-9c26-73b9ee89fc07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d01b63-b049-4b94-abd2-83fded146a7f",
                    "LayerId": "3cef87b7-4e57-486d-bdd0-ed2aea9c6da8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3cef87b7-4e57-486d-bdd0-ed2aea9c6da8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df5a680e-7b78-4146-8f58-e601a680ff75",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}