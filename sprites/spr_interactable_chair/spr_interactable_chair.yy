{
    "id": "3f4fdcc4-4e2b-4a46-9bd2-da8c190393f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_chair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1874f521-d387-44d2-a441-834f0fe40368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4fdcc4-4e2b-4a46-9bd2-da8c190393f3",
            "compositeImage": {
                "id": "a822b26b-ec6a-4343-bb8f-5bbaa7bbd64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1874f521-d387-44d2-a441-834f0fe40368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8425a7-3195-4062-a5f4-c2bd7dff43cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1874f521-d387-44d2-a441-834f0fe40368",
                    "LayerId": "7797faf6-1ebc-4622-afce-316552fd16be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "7797faf6-1ebc-4622-afce-316552fd16be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f4fdcc4-4e2b-4a46-9bd2-da8c190393f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 19
}