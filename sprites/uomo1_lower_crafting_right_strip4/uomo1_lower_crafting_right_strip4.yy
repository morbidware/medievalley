{
    "id": "46a32d7d-558a-4a50-9f5b-6729eaae8494",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ba6d808-0630-46d7-8612-9e874b96b700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a32d7d-558a-4a50-9f5b-6729eaae8494",
            "compositeImage": {
                "id": "3afd0019-2d72-4122-a83e-3a451a076072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ba6d808-0630-46d7-8612-9e874b96b700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77ea7c7b-dd4d-423b-b2d4-711137f7ad14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ba6d808-0630-46d7-8612-9e874b96b700",
                    "LayerId": "0c0247b1-5e75-47fb-9cf1-c2ecd0c2134b"
                }
            ]
        },
        {
            "id": "3a8fe854-b4d0-4c07-8c10-f85655376ea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a32d7d-558a-4a50-9f5b-6729eaae8494",
            "compositeImage": {
                "id": "bf106f8f-a7fc-4db7-b073-c75cef1ed1de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8fe854-b4d0-4c07-8c10-f85655376ea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd96350b-8d27-4041-95f1-dff789deb54c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8fe854-b4d0-4c07-8c10-f85655376ea7",
                    "LayerId": "0c0247b1-5e75-47fb-9cf1-c2ecd0c2134b"
                }
            ]
        },
        {
            "id": "3fd0ad7b-797d-44ff-b9e9-1041a2d39ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a32d7d-558a-4a50-9f5b-6729eaae8494",
            "compositeImage": {
                "id": "0425381e-e12c-41af-90b9-2aca02d495d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fd0ad7b-797d-44ff-b9e9-1041a2d39ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f8384e-044e-4221-9192-718403fee73b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fd0ad7b-797d-44ff-b9e9-1041a2d39ed8",
                    "LayerId": "0c0247b1-5e75-47fb-9cf1-c2ecd0c2134b"
                }
            ]
        },
        {
            "id": "070432ed-b8c8-42f8-b6e2-5de458dd35e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46a32d7d-558a-4a50-9f5b-6729eaae8494",
            "compositeImage": {
                "id": "65b57bdb-63c8-49d4-a3ed-66f7313cab04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070432ed-b8c8-42f8-b6e2-5de458dd35e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fee2c5c-ebfc-49a0-9ff6-878362a874e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070432ed-b8c8-42f8-b6e2-5de458dd35e8",
                    "LayerId": "0c0247b1-5e75-47fb-9cf1-c2ecd0c2134b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c0247b1-5e75-47fb-9cf1-c2ecd0c2134b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46a32d7d-558a-4a50-9f5b-6729eaae8494",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}