{
    "id": "1ba78215-7267-4e4e-a327-07c92643f19e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_home_btn_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8892a7e5-d834-4585-9780-352d092aeecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba78215-7267-4e4e-a327-07c92643f19e",
            "compositeImage": {
                "id": "63f1eede-f0af-4210-9d6f-abbd9796cc89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8892a7e5-d834-4585-9780-352d092aeecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2eafe5a3-4c6a-416b-ac25-6751b21ea96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8892a7e5-d834-4585-9780-352d092aeecd",
                    "LayerId": "981ffcf6-f5f2-4345-9e26-94557a22d462"
                }
            ]
        },
        {
            "id": "61505f95-333a-4126-8b22-859facd2061a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba78215-7267-4e4e-a327-07c92643f19e",
            "compositeImage": {
                "id": "900aff3d-154f-4e79-b137-12f53b001d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61505f95-333a-4126-8b22-859facd2061a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91258974-5af9-4897-9c4e-dd95d3dd112e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61505f95-333a-4126-8b22-859facd2061a",
                    "LayerId": "981ffcf6-f5f2-4345-9e26-94557a22d462"
                }
            ]
        },
        {
            "id": "aac4bb26-331d-44e2-bd8f-1922ca976f04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ba78215-7267-4e4e-a327-07c92643f19e",
            "compositeImage": {
                "id": "6df36bef-ecf8-4fd7-91ff-ce3ff06c4717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aac4bb26-331d-44e2-bd8f-1922ca976f04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca2b958c-6a26-4e17-b4e9-5b9dee77d12d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aac4bb26-331d-44e2-bd8f-1922ca976f04",
                    "LayerId": "981ffcf6-f5f2-4345-9e26-94557a22d462"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "981ffcf6-f5f2-4345-9e26-94557a22d462",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ba78215-7267-4e4e-a327-07c92643f19e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 9
}