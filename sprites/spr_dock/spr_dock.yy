{
    "id": "eb576707-6e6b-4445-a56c-f0ad3b2f4570",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0941dc9-057a-4c72-a3d6-31a8d2f39e48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb576707-6e6b-4445-a56c-f0ad3b2f4570",
            "compositeImage": {
                "id": "5c236e6e-15d6-4f2f-a015-c7c377656eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0941dc9-057a-4c72-a3d6-31a8d2f39e48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d82cb2ac-a0dc-4459-ae3a-f21e9e964cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0941dc9-057a-4c72-a3d6-31a8d2f39e48",
                    "LayerId": "fa0fec8a-e5b3-4ed3-843d-f142ab5fce13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 137,
    "layers": [
        {
            "id": "fa0fec8a-e5b3-4ed3-843d-f142ab5fce13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb576707-6e6b-4445-a56c-f0ad3b2f4570",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 0,
    "yorig": 0
}