{
    "id": "5c1b46d1-82de-435f-82d7-83dd29b273b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "516977cc-7a17-418c-9eea-bbec6d004ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1b46d1-82de-435f-82d7-83dd29b273b3",
            "compositeImage": {
                "id": "8c44dfad-28cd-4105-a6e0-8e3dcd8218a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "516977cc-7a17-418c-9eea-bbec6d004ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ebfd84-61a7-4b03-bb7e-004ccf31f1d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "516977cc-7a17-418c-9eea-bbec6d004ce6",
                    "LayerId": "46dc4ae4-fdd2-4f56-810b-e0818bb7c290"
                }
            ]
        },
        {
            "id": "984aadfe-a672-4e50-999e-d808e48423a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c1b46d1-82de-435f-82d7-83dd29b273b3",
            "compositeImage": {
                "id": "976f9270-7fc2-4285-9a5c-ccb66dc2fd43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "984aadfe-a672-4e50-999e-d808e48423a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ba2e04-3cb9-4aa5-9a28-51cf4dadedca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "984aadfe-a672-4e50-999e-d808e48423a4",
                    "LayerId": "46dc4ae4-fdd2-4f56-810b-e0818bb7c290"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "46dc4ae4-fdd2-4f56-810b-e0818bb7c290",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c1b46d1-82de-435f-82d7-83dd29b273b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 21
}