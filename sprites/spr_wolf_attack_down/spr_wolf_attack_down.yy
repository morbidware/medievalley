{
    "id": "6f368a04-5045-448b-aded-f6242f8bcc68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 16,
    "bbox_right": 33,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e97a514b-0a20-4335-8f92-e160385818dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f368a04-5045-448b-aded-f6242f8bcc68",
            "compositeImage": {
                "id": "7c4b95a8-308b-4bb8-9942-d3e49e6eca8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e97a514b-0a20-4335-8f92-e160385818dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688c3b9f-e1e7-44c3-8202-ef8e1c684a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e97a514b-0a20-4335-8f92-e160385818dc",
                    "LayerId": "50599a6a-ffc0-40ff-a245-73d51a03cdab"
                }
            ]
        },
        {
            "id": "dc5bbdea-54f5-4a08-9182-cbe4f10c8518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f368a04-5045-448b-aded-f6242f8bcc68",
            "compositeImage": {
                "id": "7badff6b-60d5-47ae-b678-c7c10f81941a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc5bbdea-54f5-4a08-9182-cbe4f10c8518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b9f496-9e9b-464a-a104-b8e2e0eea8f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc5bbdea-54f5-4a08-9182-cbe4f10c8518",
                    "LayerId": "50599a6a-ffc0-40ff-a245-73d51a03cdab"
                }
            ]
        },
        {
            "id": "f004836b-5dde-41b2-bcdf-12f923cc1bda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f368a04-5045-448b-aded-f6242f8bcc68",
            "compositeImage": {
                "id": "8c0a37a9-7fbb-441e-8578-b3fc4527df94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f004836b-5dde-41b2-bcdf-12f923cc1bda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73c319e-b4b8-432e-b683-9575df9b4de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f004836b-5dde-41b2-bcdf-12f923cc1bda",
                    "LayerId": "50599a6a-ffc0-40ff-a245-73d51a03cdab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "50599a6a-ffc0-40ff-a245-73d51a03cdab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f368a04-5045-448b-aded-f6242f8bcc68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}