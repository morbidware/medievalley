{
    "id": "68669037-41e2-435f-91e3-7ee23e00549c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8417aa5c-e28a-4066-893b-2cff1c815081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68669037-41e2-435f-91e3-7ee23e00549c",
            "compositeImage": {
                "id": "e5568f30-5f91-4706-ae35-b66f38a0b1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8417aa5c-e28a-4066-893b-2cff1c815081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba29e2b2-a847-4b5b-8763-caee783ac6a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8417aa5c-e28a-4066-893b-2cff1c815081",
                    "LayerId": "8675950b-65e5-4302-875f-d9e7b83a7bb5"
                }
            ]
        },
        {
            "id": "2c3e9c28-805a-4ebb-959f-6e104c5987e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68669037-41e2-435f-91e3-7ee23e00549c",
            "compositeImage": {
                "id": "e31c5b65-5fd2-4f4b-bfda-4e2110e080ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3e9c28-805a-4ebb-959f-6e104c5987e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae8c79c-f805-4a00-aa02-52bf4e662d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3e9c28-805a-4ebb-959f-6e104c5987e2",
                    "LayerId": "8675950b-65e5-4302-875f-d9e7b83a7bb5"
                }
            ]
        },
        {
            "id": "721b623d-e29c-4a02-b653-270858e271c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68669037-41e2-435f-91e3-7ee23e00549c",
            "compositeImage": {
                "id": "2e623bf4-def8-40ba-8402-3df6cf22180a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "721b623d-e29c-4a02-b653-270858e271c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb07a9cb-ca58-42c2-883d-92f20316b489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "721b623d-e29c-4a02-b653-270858e271c9",
                    "LayerId": "8675950b-65e5-4302-875f-d9e7b83a7bb5"
                }
            ]
        },
        {
            "id": "175b9d1a-eeb9-4d1b-bbf1-0cc5120931ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68669037-41e2-435f-91e3-7ee23e00549c",
            "compositeImage": {
                "id": "157e1fa9-128e-44b1-9a4f-9ce799030518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "175b9d1a-eeb9-4d1b-bbf1-0cc5120931ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6424ce8-a9cb-462c-812e-fb9db8c4130e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "175b9d1a-eeb9-4d1b-bbf1-0cc5120931ce",
                    "LayerId": "8675950b-65e5-4302-875f-d9e7b83a7bb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8675950b-65e5-4302-875f-d9e7b83a7bb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68669037-41e2-435f-91e3-7ee23e00549c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}