{
    "id": "554ac506-19ef-4de8-bd9a-b261df873239",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "854f0257-f6dd-4487-ae6b-82ab9fad75f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "554ac506-19ef-4de8-bd9a-b261df873239",
            "compositeImage": {
                "id": "9ec4362f-ca39-4934-96c5-ce141843079b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854f0257-f6dd-4487-ae6b-82ab9fad75f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acfa6ad2-5c51-48bf-8e6f-7e170a58d935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854f0257-f6dd-4487-ae6b-82ab9fad75f0",
                    "LayerId": "2e2c5ef5-5fda-454d-82af-399536ae276e"
                }
            ]
        },
        {
            "id": "b7994911-e894-4468-8ae4-e8650e7946db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "554ac506-19ef-4de8-bd9a-b261df873239",
            "compositeImage": {
                "id": "c3d92e1f-e0b9-4557-85f4-913bf0de16e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7994911-e894-4468-8ae4-e8650e7946db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac154376-59a1-4a28-bea2-baf3f1cdcb45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7994911-e894-4468-8ae4-e8650e7946db",
                    "LayerId": "2e2c5ef5-5fda-454d-82af-399536ae276e"
                }
            ]
        },
        {
            "id": "b478f73b-a09a-40f1-bd0e-db48e1c1d635",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "554ac506-19ef-4de8-bd9a-b261df873239",
            "compositeImage": {
                "id": "a8971e71-96f4-43a9-95f9-5e78cf07c0fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b478f73b-a09a-40f1-bd0e-db48e1c1d635",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72bec2df-7d69-4938-82c2-797315a9e726",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b478f73b-a09a-40f1-bd0e-db48e1c1d635",
                    "LayerId": "2e2c5ef5-5fda-454d-82af-399536ae276e"
                }
            ]
        },
        {
            "id": "13faa86a-d545-410c-8cb4-d36d7519063d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "554ac506-19ef-4de8-bd9a-b261df873239",
            "compositeImage": {
                "id": "8213f40d-7e27-4cff-852a-460614c9de52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13faa86a-d545-410c-8cb4-d36d7519063d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569f74d8-7bc1-4d7e-8bb7-0c96a01d2899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13faa86a-d545-410c-8cb4-d36d7519063d",
                    "LayerId": "2e2c5ef5-5fda-454d-82af-399536ae276e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2e2c5ef5-5fda-454d-82af-399536ae276e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "554ac506-19ef-4de8-bd9a-b261df873239",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}