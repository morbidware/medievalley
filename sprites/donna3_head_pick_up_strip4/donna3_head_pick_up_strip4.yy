{
    "id": "b3b0bbc9-fc84-440b-905c-34420bb67f38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17451bfd-497e-4d2a-b8e4-4446c03e25e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b0bbc9-fc84-440b-905c-34420bb67f38",
            "compositeImage": {
                "id": "43f3ca2f-845a-4703-8f00-1e0306b35ea5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17451bfd-497e-4d2a-b8e4-4446c03e25e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadb76f7-8198-4f15-bd82-e1f1997ca78f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17451bfd-497e-4d2a-b8e4-4446c03e25e9",
                    "LayerId": "7bb87ff6-4f81-4def-bdbf-d985b616f23f"
                }
            ]
        },
        {
            "id": "dc1b4d08-48c0-4e0a-922e-0f2432b85070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b0bbc9-fc84-440b-905c-34420bb67f38",
            "compositeImage": {
                "id": "fbcaef78-7aa0-405f-bd63-aa4f4f437647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc1b4d08-48c0-4e0a-922e-0f2432b85070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b8d9ae-b357-4e97-bf77-26953df8e207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc1b4d08-48c0-4e0a-922e-0f2432b85070",
                    "LayerId": "7bb87ff6-4f81-4def-bdbf-d985b616f23f"
                }
            ]
        },
        {
            "id": "20f3f366-0227-43a6-bb18-c39e3fdeadf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b0bbc9-fc84-440b-905c-34420bb67f38",
            "compositeImage": {
                "id": "3bd6ab13-66d8-4074-afbf-7c7ff4a558df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20f3f366-0227-43a6-bb18-c39e3fdeadf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a15af2-5717-4935-9702-1b126e735cc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20f3f366-0227-43a6-bb18-c39e3fdeadf8",
                    "LayerId": "7bb87ff6-4f81-4def-bdbf-d985b616f23f"
                }
            ]
        },
        {
            "id": "5df5cc47-32d3-4c4d-bbb9-a9e131c1c151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b0bbc9-fc84-440b-905c-34420bb67f38",
            "compositeImage": {
                "id": "4c845218-a659-4c51-bc6d-2d7b7ecfd960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df5cc47-32d3-4c4d-bbb9-a9e131c1c151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb84d18-0dc8-4f6f-ae2c-02aa54e76fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df5cc47-32d3-4c4d-bbb9-a9e131c1c151",
                    "LayerId": "7bb87ff6-4f81-4def-bdbf-d985b616f23f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7bb87ff6-4f81-4def-bdbf-d985b616f23f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3b0bbc9-fc84-440b-905c-34420bb67f38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}