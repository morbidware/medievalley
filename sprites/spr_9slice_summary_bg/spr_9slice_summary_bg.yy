{
    "id": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9slice_summary_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b19229d-2b2a-4ccd-a4b4-5b6ca2b88cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "0f7ad185-f9fa-4935-878b-b678499a37b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b19229d-2b2a-4ccd-a4b4-5b6ca2b88cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccb4b666-4681-4ff9-9f8e-12d1e161400c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b19229d-2b2a-4ccd-a4b4-5b6ca2b88cc4",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "955523c1-9de1-4c23-892d-44ba8c816472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "f4a92487-86f1-4e20-a722-7332f0ac30e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955523c1-9de1-4c23-892d-44ba8c816472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83980fe7-bcc4-43e5-be91-2a5b5c36e099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955523c1-9de1-4c23-892d-44ba8c816472",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "71bc6ed5-89ff-495e-87b2-34dde7073014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "8d69d919-c3da-46f4-afa1-e9b35b0adbb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71bc6ed5-89ff-495e-87b2-34dde7073014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011178c3-6593-4a4f-8041-ffefb398025f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71bc6ed5-89ff-495e-87b2-34dde7073014",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "86149a4b-44ae-4b90-9b33-f4ce13e281fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "fdb5c8c6-c736-40df-be11-8bc85af1cd33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86149a4b-44ae-4b90-9b33-f4ce13e281fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aab5f38-6c3a-4e2a-a540-3211105260a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86149a4b-44ae-4b90-9b33-f4ce13e281fd",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "14def52b-787d-457f-8d37-dda956c4f922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "42fb862e-6659-4c6b-b01c-4ad1acded800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14def52b-787d-457f-8d37-dda956c4f922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "993188fe-7905-437a-8cbc-90f1464129c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14def52b-787d-457f-8d37-dda956c4f922",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "02ba93a3-44d0-4914-bdfd-7cd9fe5e37d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "07db96df-a5fe-479f-b3dd-32925ad5dd9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ba93a3-44d0-4914-bdfd-7cd9fe5e37d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac23c96-4e55-4a75-8015-963057152729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ba93a3-44d0-4914-bdfd-7cd9fe5e37d7",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "99cae63f-d27e-4b77-9145-c68d72499f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "55b3259b-91ef-484e-8644-0e298a228824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99cae63f-d27e-4b77-9145-c68d72499f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d99e8b77-024f-4b9d-ad93-ba79fb2e9d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99cae63f-d27e-4b77-9145-c68d72499f29",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "a7c8e971-d9fa-4c7d-97c2-43bbabb6f117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "984557b5-dc7a-4461-a689-0ecb241315be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7c8e971-d9fa-4c7d-97c2-43bbabb6f117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b03e87b-7833-4f72-bee3-250c9e3c29cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7c8e971-d9fa-4c7d-97c2-43bbabb6f117",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        },
        {
            "id": "88dcafa5-8b59-44cc-afd9-f6c26df0f783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "compositeImage": {
                "id": "0bd67a04-d770-46ec-9c08-86afe7302436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88dcafa5-8b59-44cc-afd9-f6c26df0f783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f72364-4cb1-4584-ab14-a2883426beb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88dcafa5-8b59-44cc-afd9-f6c26df0f783",
                    "LayerId": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "5862ebd5-c2d2-45e0-a12a-b4ca3faef47a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccd13676-0da5-4d3a-9eff-ebb8d50859b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 0,
    "yorig": 0
}