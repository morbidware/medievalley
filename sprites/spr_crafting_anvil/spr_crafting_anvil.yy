{
    "id": "92a43f6f-1771-47eb-8b77-f821eca5b048",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_anvil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "946d98a0-f0ea-4785-a627-83cb083db5f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92a43f6f-1771-47eb-8b77-f821eca5b048",
            "compositeImage": {
                "id": "0987d97b-61c0-4c7d-997e-7b852656b531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "946d98a0-f0ea-4785-a627-83cb083db5f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a091efdd-a2cd-4c51-a8ed-0d9f8b5f0b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "946d98a0-f0ea-4785-a627-83cb083db5f4",
                    "LayerId": "a8e2793d-d4c0-4ec4-9fb4-d79896fa9f82"
                }
            ]
        },
        {
            "id": "75c6d35b-547e-4388-9538-f6daaf5d254c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92a43f6f-1771-47eb-8b77-f821eca5b048",
            "compositeImage": {
                "id": "0062e698-26bd-40ac-b1a5-dfbba96f6c6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c6d35b-547e-4388-9538-f6daaf5d254c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "423d6d0b-6731-4673-b5cb-5e3c4f053d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c6d35b-547e-4388-9538-f6daaf5d254c",
                    "LayerId": "a8e2793d-d4c0-4ec4-9fb4-d79896fa9f82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "a8e2793d-d4c0-4ec4-9fb4-d79896fa9f82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92a43f6f-1771-47eb-8b77-f821eca5b048",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}