{
    "id": "885433fc-6bee-4b9e-93dd-cf1e9e041bf3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1adf8bd5-e360-4867-a696-de00bc553c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "885433fc-6bee-4b9e-93dd-cf1e9e041bf3",
            "compositeImage": {
                "id": "91eda412-6642-4fe1-9532-7a0ec6b993cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1adf8bd5-e360-4867-a696-de00bc553c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770c7d14-151f-45a4-8f52-effffb6e1ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1adf8bd5-e360-4867-a696-de00bc553c66",
                    "LayerId": "faafca04-237d-4572-b139-77ade95b086e"
                }
            ]
        },
        {
            "id": "b95b3055-3b21-4e13-923b-472ef5a1de46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "885433fc-6bee-4b9e-93dd-cf1e9e041bf3",
            "compositeImage": {
                "id": "73dc0b45-d803-4704-9f78-a69e2787b201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b95b3055-3b21-4e13-923b-472ef5a1de46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cbaf332-fea2-428f-913f-f264bc47391d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b95b3055-3b21-4e13-923b-472ef5a1de46",
                    "LayerId": "faafca04-237d-4572-b139-77ade95b086e"
                }
            ]
        },
        {
            "id": "74c18a11-8f37-485f-9470-c8d9c13d3d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "885433fc-6bee-4b9e-93dd-cf1e9e041bf3",
            "compositeImage": {
                "id": "dde472b8-9fef-499e-b475-e13ad7ee9257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74c18a11-8f37-485f-9470-c8d9c13d3d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45bb9ab4-b398-4f0d-9c9b-f437c380eb35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74c18a11-8f37-485f-9470-c8d9c13d3d56",
                    "LayerId": "faafca04-237d-4572-b139-77ade95b086e"
                }
            ]
        },
        {
            "id": "a55f8d5a-0127-45ed-939c-ddb204b68aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "885433fc-6bee-4b9e-93dd-cf1e9e041bf3",
            "compositeImage": {
                "id": "5a59d04f-c488-420c-982d-d02b3c3c7a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a55f8d5a-0127-45ed-939c-ddb204b68aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f560646f-6e55-403b-bb6c-f3edcd3128a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a55f8d5a-0127-45ed-939c-ddb204b68aad",
                    "LayerId": "faafca04-237d-4572-b139-77ade95b086e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "faafca04-237d-4572-b139-77ade95b086e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "885433fc-6bee-4b9e-93dd-cf1e9e041bf3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}