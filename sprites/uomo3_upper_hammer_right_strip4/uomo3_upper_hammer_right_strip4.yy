{
    "id": "fde1bf30-2aab-417a-9f56-b06b7735dc7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "749ea538-a4b0-4fee-aad0-269864ce13c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde1bf30-2aab-417a-9f56-b06b7735dc7c",
            "compositeImage": {
                "id": "11c16c3b-399f-4f55-a8cb-b4022c2a2f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749ea538-a4b0-4fee-aad0-269864ce13c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6510f2c-3fe9-446b-a557-300016d550e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749ea538-a4b0-4fee-aad0-269864ce13c5",
                    "LayerId": "667c350c-ebcd-4e56-be44-a43448985c7c"
                }
            ]
        },
        {
            "id": "c2ba6d22-777a-43b3-acc2-4cb407369be1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde1bf30-2aab-417a-9f56-b06b7735dc7c",
            "compositeImage": {
                "id": "1e243342-1dbe-42a2-9cec-449117367684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2ba6d22-777a-43b3-acc2-4cb407369be1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4152c1a7-743e-4473-94e2-203bd8af9487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2ba6d22-777a-43b3-acc2-4cb407369be1",
                    "LayerId": "667c350c-ebcd-4e56-be44-a43448985c7c"
                }
            ]
        },
        {
            "id": "cecc3c88-4516-4ecf-a3b7-97ff4936975d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde1bf30-2aab-417a-9f56-b06b7735dc7c",
            "compositeImage": {
                "id": "7be10907-6750-4116-b1a3-eae8ae07fee3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecc3c88-4516-4ecf-a3b7-97ff4936975d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f67dfdf0-6583-4afb-b2ff-1b0adb7ed03b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecc3c88-4516-4ecf-a3b7-97ff4936975d",
                    "LayerId": "667c350c-ebcd-4e56-be44-a43448985c7c"
                }
            ]
        },
        {
            "id": "af480ed4-49bb-45af-b22a-9fb0c81ea33b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fde1bf30-2aab-417a-9f56-b06b7735dc7c",
            "compositeImage": {
                "id": "4c63789b-ea4b-4446-af86-8b6f4e847331",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af480ed4-49bb-45af-b22a-9fb0c81ea33b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddced876-6b18-41d2-93be-69f7812a5a75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af480ed4-49bb-45af-b22a-9fb0c81ea33b",
                    "LayerId": "667c350c-ebcd-4e56-be44-a43448985c7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "667c350c-ebcd-4e56-be44-a43448985c7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fde1bf30-2aab-417a-9f56-b06b7735dc7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}