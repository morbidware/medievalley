{
    "id": "c82873ac-3af1-4025-ae0a-de4f6a945183",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "550b66a9-1759-442d-b802-ab3208164350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82873ac-3af1-4025-ae0a-de4f6a945183",
            "compositeImage": {
                "id": "0fe8aba0-371e-4ec0-a83a-fb4265b9ad13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "550b66a9-1759-442d-b802-ab3208164350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a62dca3-87dd-4f9d-8864-0b375af15768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "550b66a9-1759-442d-b802-ab3208164350",
                    "LayerId": "3166d627-b09e-4fdd-8e06-7d0b988b4374"
                }
            ]
        },
        {
            "id": "23c15c41-7453-4211-8b4a-38357d85b5f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82873ac-3af1-4025-ae0a-de4f6a945183",
            "compositeImage": {
                "id": "e4268cd7-e943-40dc-b75b-3a9177c0ab7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c15c41-7453-4211-8b4a-38357d85b5f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9804442f-e6a3-4c45-8b66-50c6a6987956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c15c41-7453-4211-8b4a-38357d85b5f8",
                    "LayerId": "3166d627-b09e-4fdd-8e06-7d0b988b4374"
                }
            ]
        },
        {
            "id": "f1177d38-b6f1-43a5-b9bf-3617ba194404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82873ac-3af1-4025-ae0a-de4f6a945183",
            "compositeImage": {
                "id": "e775681b-95e2-4b68-8faa-d1842e0fbcdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1177d38-b6f1-43a5-b9bf-3617ba194404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed0ec84-780d-415e-9e55-78aa69e55867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1177d38-b6f1-43a5-b9bf-3617ba194404",
                    "LayerId": "3166d627-b09e-4fdd-8e06-7d0b988b4374"
                }
            ]
        },
        {
            "id": "30f3940c-ef5e-4ee8-bb20-a46b9388944d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82873ac-3af1-4025-ae0a-de4f6a945183",
            "compositeImage": {
                "id": "018a8b8e-9759-4923-a1f9-2a4048f1edab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f3940c-ef5e-4ee8-bb20-a46b9388944d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a202542e-1ad6-44c1-8b7c-ed3b2184bfb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f3940c-ef5e-4ee8-bb20-a46b9388944d",
                    "LayerId": "3166d627-b09e-4fdd-8e06-7d0b988b4374"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3166d627-b09e-4fdd-8e06-7d0b988b4374",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c82873ac-3af1-4025-ae0a-de4f6a945183",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}