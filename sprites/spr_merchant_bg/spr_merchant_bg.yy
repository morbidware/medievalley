{
    "id": "1b34dbcf-acda-47d4-8dcc-2f54aac0e64c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_merchant_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 176,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82a1ff94-33ce-4ce7-ad33-1e9f029bdcc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b34dbcf-acda-47d4-8dcc-2f54aac0e64c",
            "compositeImage": {
                "id": "d9b42176-1e08-4b5b-9b2f-8c2a156bc7e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a1ff94-33ce-4ce7-ad33-1e9f029bdcc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e779743-82ea-4c78-8464-0ce8fb0cbc00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a1ff94-33ce-4ce7-ad33-1e9f029bdcc7",
                    "LayerId": "bc7a2862-3576-41de-b969-2e0cab4a3ac2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 177,
    "layers": [
        {
            "id": "bc7a2862-3576-41de-b969-2e0cab4a3ac2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b34dbcf-acda-47d4-8dcc-2f54aac0e64c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 153,
    "yorig": 88
}