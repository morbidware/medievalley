{
    "id": "4e2ebfa4-3e48-4610-bd6e-94d307f681ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_scrollpotion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a814d6f-86c2-4387-b9bb-13f4f3fbd4a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e2ebfa4-3e48-4610-bd6e-94d307f681ac",
            "compositeImage": {
                "id": "a6a5f7de-4e22-4839-a5b9-26fad741e949",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a814d6f-86c2-4387-b9bb-13f4f3fbd4a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfc5fb21-ba0c-461f-8b90-7c4e09265e38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a814d6f-86c2-4387-b9bb-13f4f3fbd4a4",
                    "LayerId": "382a8efc-2b70-4c00-b343-548af93e952d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "382a8efc-2b70-4c00-b343-548af93e952d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e2ebfa4-3e48-4610-bd6e-94d307f681ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}