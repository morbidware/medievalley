{
    "id": "16610c66-88ed-4b2c-95ff-a796f6a5eadf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_idle_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8632f923-81ec-4fa3-be27-cf2881407890",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16610c66-88ed-4b2c-95ff-a796f6a5eadf",
            "compositeImage": {
                "id": "aa1d19d9-0455-4263-8e88-7c09c9f3dc98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8632f923-81ec-4fa3-be27-cf2881407890",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1e0266-1161-4fa7-b7c9-553668d2fc3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8632f923-81ec-4fa3-be27-cf2881407890",
                    "LayerId": "0265e52e-1a24-44b7-900d-70562fcc42fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "0265e52e-1a24-44b7-900d-70562fcc42fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16610c66-88ed-4b2c-95ff-a796f6a5eadf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}