{
    "id": "f056d849-7464-46a2-a589-213758dba357",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de16abe7-97ff-4406-92cb-9c29692913bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f056d849-7464-46a2-a589-213758dba357",
            "compositeImage": {
                "id": "a015c804-7e05-4c91-8ea0-6474afc2bdbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de16abe7-97ff-4406-92cb-9c29692913bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224c560b-03c2-489b-99b0-be89afb09eab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de16abe7-97ff-4406-92cb-9c29692913bb",
                    "LayerId": "9e57b229-04f4-465b-ad80-8fad4f77ea73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e57b229-04f4-465b-ad80-8fad4f77ea73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f056d849-7464-46a2-a589-213758dba357",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}