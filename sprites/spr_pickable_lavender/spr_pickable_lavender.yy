{
    "id": "486ef9d7-3c2a-48e4-8a9f-f9ab3d9c02c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_lavender",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5f7d72d-9fd7-4411-a32a-869642788ea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "486ef9d7-3c2a-48e4-8a9f-f9ab3d9c02c2",
            "compositeImage": {
                "id": "ebda7166-5abb-42b5-bf25-5d7ffbf0f59a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f7d72d-9fd7-4411-a32a-869642788ea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6da461-c38a-44da-85fa-137d1c77e48b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f7d72d-9fd7-4411-a32a-869642788ea9",
                    "LayerId": "918083f1-7255-4ed2-8271-aa2532a94714"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "918083f1-7255-4ed2-8271-aa2532a94714",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "486ef9d7-3c2a-48e4-8a9f-f9ab3d9c02c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}