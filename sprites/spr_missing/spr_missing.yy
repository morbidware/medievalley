{
    "id": "c81f47fe-510d-407d-a5d4-cb4a9ea34d0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_missing",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "195ed41e-554a-4c4b-b357-27d7f07999e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c81f47fe-510d-407d-a5d4-cb4a9ea34d0c",
            "compositeImage": {
                "id": "d7b44e7c-6887-449b-9d8a-89b8667e6342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195ed41e-554a-4c4b-b357-27d7f07999e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97fee2d4-e8dc-4f5e-a0be-506849db8ebd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195ed41e-554a-4c4b-b357-27d7f07999e2",
                    "LayerId": "69fe0042-94e7-4dda-99f2-337944ee2cae"
                },
                {
                    "id": "549bbcd7-366c-497f-b386-59d04bf216f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195ed41e-554a-4c4b-b357-27d7f07999e2",
                    "LayerId": "99edb54f-a10a-47e1-84b6-6346e0bd3b94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "99edb54f-a10a-47e1-84b6-6346e0bd3b94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c81f47fe-510d-407d-a5d4-cb4a9ea34d0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "69fe0042-94e7-4dda-99f2-337944ee2cae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c81f47fe-510d-407d-a5d4-cb4a9ea34d0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}