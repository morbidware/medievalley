{
    "id": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_watering_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c37449b3-dc0b-470f-a457-6470015a394f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "compositeImage": {
                "id": "978e4fc9-2126-4b2a-b461-08a7e485fc69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c37449b3-dc0b-470f-a457-6470015a394f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b9873b-0488-4491-b981-d1141ab62b95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c37449b3-dc0b-470f-a457-6470015a394f",
                    "LayerId": "2d39071c-d819-4718-ac58-e810b29d01b6"
                }
            ]
        },
        {
            "id": "eb0032dc-4a60-4942-86a9-b687d0b7684c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "compositeImage": {
                "id": "e428a507-8b8c-4481-bab4-f40a13f304d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0032dc-4a60-4942-86a9-b687d0b7684c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e254052-015f-4397-9e42-5583d2707583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0032dc-4a60-4942-86a9-b687d0b7684c",
                    "LayerId": "2d39071c-d819-4718-ac58-e810b29d01b6"
                }
            ]
        },
        {
            "id": "79ac17d8-a93d-46d5-9140-f34ccc20aa4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "compositeImage": {
                "id": "fb6017df-a0ee-44c0-9f3e-0e16a0697e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ac17d8-a93d-46d5-9140-f34ccc20aa4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39eca35a-ec71-4d7b-a653-34355f328584",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ac17d8-a93d-46d5-9140-f34ccc20aa4a",
                    "LayerId": "2d39071c-d819-4718-ac58-e810b29d01b6"
                }
            ]
        },
        {
            "id": "c75f112d-df42-4eeb-964f-a1813c8076f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "compositeImage": {
                "id": "edf305f0-a80f-4620-b78e-80daf3540547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c75f112d-df42-4eeb-964f-a1813c8076f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a2531c-7a26-442c-8d37-f1dad1044b97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c75f112d-df42-4eeb-964f-a1813c8076f5",
                    "LayerId": "2d39071c-d819-4718-ac58-e810b29d01b6"
                }
            ]
        },
        {
            "id": "fad8da2e-56a1-430e-9411-ef2adc07b40d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "compositeImage": {
                "id": "19171d54-3226-4b7f-93f7-de3cdbdccd7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fad8da2e-56a1-430e-9411-ef2adc07b40d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55aaa27-c998-4b95-9676-6ed22cdf3e24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fad8da2e-56a1-430e-9411-ef2adc07b40d",
                    "LayerId": "2d39071c-d819-4718-ac58-e810b29d01b6"
                }
            ]
        },
        {
            "id": "57f4f555-e22b-47f1-9857-f269273456d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "compositeImage": {
                "id": "c5556b8f-9a6b-4b04-b075-7ecfe05e9c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f4f555-e22b-47f1-9857-f269273456d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ddf467-a834-4588-a2f9-aabe56b2822d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f4f555-e22b-47f1-9857-f269273456d8",
                    "LayerId": "2d39071c-d819-4718-ac58-e810b29d01b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2d39071c-d819-4718-ac58-e810b29d01b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4d49e27-97fa-49b6-9a09-3ab54b9bd129",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}