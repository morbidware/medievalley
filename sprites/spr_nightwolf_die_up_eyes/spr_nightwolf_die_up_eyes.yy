{
    "id": "6a08c696-049d-436e-8ab5-dec0efc2735e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_die_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b954499b-95c1-4640-b760-9b73704e05a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a08c696-049d-436e-8ab5-dec0efc2735e",
            "compositeImage": {
                "id": "a2cf63bb-3074-4877-a543-351624ee3929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b954499b-95c1-4640-b760-9b73704e05a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2fe5ab1-76ad-4301-9fa8-38db34450070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b954499b-95c1-4640-b760-9b73704e05a3",
                    "LayerId": "fc12e1cf-f5c3-4531-8ee0-5845d4a0e865"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "fc12e1cf-f5c3-4531-8ee0-5845d4a0e865",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a08c696-049d-436e-8ab5-dec0efc2735e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}