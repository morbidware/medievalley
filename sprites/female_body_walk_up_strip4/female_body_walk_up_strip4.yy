{
    "id": "c257deeb-0e8c-4941-81b6-f7fc568c5aeb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91c107c3-2f51-4194-b1b8-98a35acb83d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c257deeb-0e8c-4941-81b6-f7fc568c5aeb",
            "compositeImage": {
                "id": "59e0c9b8-6214-47a6-86b5-1a920218c591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c107c3-2f51-4194-b1b8-98a35acb83d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ab7ba5-8342-4496-a56c-fdcb0914b193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c107c3-2f51-4194-b1b8-98a35acb83d1",
                    "LayerId": "2c95dd7a-423e-4cd8-b0b7-f8857af3fa7d"
                }
            ]
        },
        {
            "id": "75ebb59a-8b43-4ff1-964e-777834e60e41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c257deeb-0e8c-4941-81b6-f7fc568c5aeb",
            "compositeImage": {
                "id": "dbfaedaa-e71b-4472-9e13-8b060e996655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ebb59a-8b43-4ff1-964e-777834e60e41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99e5bcb9-3158-426d-b0ed-83561917c973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ebb59a-8b43-4ff1-964e-777834e60e41",
                    "LayerId": "2c95dd7a-423e-4cd8-b0b7-f8857af3fa7d"
                }
            ]
        },
        {
            "id": "f66a0fef-052e-4763-9149-35fee40a99b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c257deeb-0e8c-4941-81b6-f7fc568c5aeb",
            "compositeImage": {
                "id": "965fb746-cafb-475b-9496-5e6613c1681e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f66a0fef-052e-4763-9149-35fee40a99b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4b9b5d2-9716-468b-8619-61acd0887751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f66a0fef-052e-4763-9149-35fee40a99b1",
                    "LayerId": "2c95dd7a-423e-4cd8-b0b7-f8857af3fa7d"
                }
            ]
        },
        {
            "id": "456e3d7f-0c6b-4eb1-af9b-0a126a2c624d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c257deeb-0e8c-4941-81b6-f7fc568c5aeb",
            "compositeImage": {
                "id": "6a472112-e50e-415a-af95-0968e6dffb15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "456e3d7f-0c6b-4eb1-af9b-0a126a2c624d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "789f1de4-6de8-4bd9-9e06-ad9731ee93fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "456e3d7f-0c6b-4eb1-af9b-0a126a2c624d",
                    "LayerId": "2c95dd7a-423e-4cd8-b0b7-f8857af3fa7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c95dd7a-423e-4cd8-b0b7-f8857af3fa7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c257deeb-0e8c-4941-81b6-f7fc568c5aeb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}