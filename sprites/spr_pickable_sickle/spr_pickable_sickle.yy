{
    "id": "fe6a4e9c-4ef0-4f5a-9cb6-f0a157f170f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_sickle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "312b9f7a-523c-4f29-9a30-3f856a504a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6a4e9c-4ef0-4f5a-9cb6-f0a157f170f0",
            "compositeImage": {
                "id": "fa9b2df9-6537-4fa5-a1c6-c068a6228acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "312b9f7a-523c-4f29-9a30-3f856a504a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44702141-f476-4d6d-a2cd-cdd075e812ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "312b9f7a-523c-4f29-9a30-3f856a504a83",
                    "LayerId": "e61f2431-f4f3-482a-9e40-dbf745483520"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e61f2431-f4f3-482a-9e40-dbf745483520",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe6a4e9c-4ef0-4f5a-9cb6-f0a157f170f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}