{
    "id": "58abda7d-ca73-40ef-a221-9a546ed88b98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_spear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5601f9b4-f3de-48b6-b221-478810a887b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58abda7d-ca73-40ef-a221-9a546ed88b98",
            "compositeImage": {
                "id": "7fe6f78b-d33d-4044-a431-daa02024a07c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5601f9b4-f3de-48b6-b221-478810a887b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b37616-33e1-4890-97d8-9ffae288bc16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5601f9b4-f3de-48b6-b221-478810a887b5",
                    "LayerId": "698f9b43-56e9-499f-b93a-55ab3127c2a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "698f9b43-56e9-499f-b93a-55ab3127c2a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58abda7d-ca73-40ef-a221-9a546ed88b98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 12
}