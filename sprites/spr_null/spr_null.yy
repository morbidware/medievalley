{
    "id": "f0ed877a-a350-4c41-9872-8c6451c613d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_null",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b293d38-e1a9-4d6d-8650-35fe1329b3cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
            "compositeImage": {
                "id": "d3449ecb-9be1-4d1c-a362-57b212568590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b293d38-e1a9-4d6d-8650-35fe1329b3cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c370b8ec-8e08-4fa5-bc5c-cc8a2da93d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b293d38-e1a9-4d6d-8650-35fe1329b3cb",
                    "LayerId": "fbd4ad92-0a46-45b0-9e02-e6e2bc372902"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fbd4ad92-0a46-45b0-9e02-e6e2bc372902",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0ed877a-a350-4c41-9872-8c6451c613d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}