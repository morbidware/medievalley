{
    "id": "497924cc-cb2c-44a5-bc72-add049195675",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 6,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29358c64-76f5-4cb4-8bfb-9dba7e8ce818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "compositeImage": {
                "id": "7c746b26-7075-45b5-a9b9-16eb832c5f4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29358c64-76f5-4cb4-8bfb-9dba7e8ce818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "014fec61-214c-47f0-b6e7-11702d44b399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29358c64-76f5-4cb4-8bfb-9dba7e8ce818",
                    "LayerId": "f1c00411-06c8-4226-a54f-2c64510af978"
                }
            ]
        },
        {
            "id": "72c8e71a-ec7d-41a1-a8f1-91e954ed9763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "compositeImage": {
                "id": "9617bdec-a7bd-43f6-9b6e-a70765ff13de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c8e71a-ec7d-41a1-a8f1-91e954ed9763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a4c3ff-ba09-4f2c-a5ee-6b010df46b9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c8e71a-ec7d-41a1-a8f1-91e954ed9763",
                    "LayerId": "f1c00411-06c8-4226-a54f-2c64510af978"
                }
            ]
        },
        {
            "id": "e19bdca0-f88c-4e25-876f-ede0a2edc45f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "compositeImage": {
                "id": "d1d49c06-3b27-45c3-bae5-a770175665d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e19bdca0-f88c-4e25-876f-ede0a2edc45f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890e8356-facd-4c76-ae0b-318684eab853",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e19bdca0-f88c-4e25-876f-ede0a2edc45f",
                    "LayerId": "f1c00411-06c8-4226-a54f-2c64510af978"
                }
            ]
        },
        {
            "id": "d0c811fe-7605-4a07-8b47-5dd81c0711d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "compositeImage": {
                "id": "de7f9029-da19-4663-89db-e74c11c12503",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0c811fe-7605-4a07-8b47-5dd81c0711d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5d208e7-3b6a-45c1-abba-620600b10516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0c811fe-7605-4a07-8b47-5dd81c0711d9",
                    "LayerId": "f1c00411-06c8-4226-a54f-2c64510af978"
                }
            ]
        },
        {
            "id": "52c5354f-37cc-4252-a31d-6320d406d508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "compositeImage": {
                "id": "56d34041-1d36-4832-9af5-c508b9267deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c5354f-37cc-4252-a31d-6320d406d508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4260dc1-4b68-4319-bbc7-5fcd33d6340a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c5354f-37cc-4252-a31d-6320d406d508",
                    "LayerId": "f1c00411-06c8-4226-a54f-2c64510af978"
                }
            ]
        },
        {
            "id": "ad18ed48-c160-4382-a6ac-8c814bca70f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "compositeImage": {
                "id": "4f2ce35e-6fc1-4666-b2e4-ceaaa9967a70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad18ed48-c160-4382-a6ac-8c814bca70f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e746717a-d3d6-4718-bf0e-4437021f3634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad18ed48-c160-4382-a6ac-8c814bca70f9",
                    "LayerId": "f1c00411-06c8-4226-a54f-2c64510af978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "f1c00411-06c8-4226-a54f-2c64510af978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "497924cc-cb2c-44a5-bc72-add049195675",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}