{
    "id": "0e2a466d-b643-421e-a97f-60c698f49020",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 39,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4547599d-49a6-420c-9402-8ecf27dfad5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "compositeImage": {
                "id": "35dac068-6de6-4be3-80ec-af33d10a51c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4547599d-49a6-420c-9402-8ecf27dfad5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "230e50ee-d9fe-4b9e-8731-f5ad542ab490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4547599d-49a6-420c-9402-8ecf27dfad5b",
                    "LayerId": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f"
                }
            ]
        },
        {
            "id": "315ca00e-1fa9-4fc6-95de-a96e8bb21c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "compositeImage": {
                "id": "1e7f5133-6d97-427f-9176-3bb927df5e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "315ca00e-1fa9-4fc6-95de-a96e8bb21c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8f47c7-26af-4e84-8b70-03ba6302e9bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "315ca00e-1fa9-4fc6-95de-a96e8bb21c51",
                    "LayerId": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f"
                }
            ]
        },
        {
            "id": "1092e158-0108-416b-b6b8-a472570461f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "compositeImage": {
                "id": "7ccb7a92-80e7-4fd5-b9c1-dbb73d3658b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1092e158-0108-416b-b6b8-a472570461f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53782b43-6e21-4a31-962e-c4c645fc8f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1092e158-0108-416b-b6b8-a472570461f7",
                    "LayerId": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f"
                }
            ]
        },
        {
            "id": "7248e10e-0ca7-401a-8272-33bd0dd16751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "compositeImage": {
                "id": "f8b32b73-7948-4d32-8557-f18b215b7470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7248e10e-0ca7-401a-8272-33bd0dd16751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60f4366a-d4c6-437b-8816-02447e820b27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7248e10e-0ca7-401a-8272-33bd0dd16751",
                    "LayerId": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f"
                }
            ]
        },
        {
            "id": "d01cd199-01a6-4512-b265-93fea30fde9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "compositeImage": {
                "id": "889d8818-2c64-47f8-891f-5d4a2dd06af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01cd199-01a6-4512-b265-93fea30fde9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0acc7fc-186d-4e55-8f03-e6224afa2a25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01cd199-01a6-4512-b265-93fea30fde9e",
                    "LayerId": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f"
                }
            ]
        },
        {
            "id": "e546467d-79c4-47a4-9904-198554d2cdcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "compositeImage": {
                "id": "b3aef435-9ebb-4d79-90d3-af7875f72fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e546467d-79c4-47a4-9904-198554d2cdcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8adf356-aa86-4e8d-9c3d-f8977530bd99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e546467d-79c4-47a4-9904-198554d2cdcd",
                    "LayerId": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b35dfb3a-bd1e-47c9-bc5f-e65e5248aa8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e2a466d-b643-421e-a97f-60c698f49020",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}