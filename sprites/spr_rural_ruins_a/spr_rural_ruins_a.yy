{
    "id": "ae5bbdb5-ce39-4c7a-91fd-0bbae0757bda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_ruins_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccce6f4a-b869-46a2-aefe-dab90388f22a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae5bbdb5-ce39-4c7a-91fd-0bbae0757bda",
            "compositeImage": {
                "id": "705b1d61-5d89-4aca-b34c-2f87b5680fb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccce6f4a-b869-46a2-aefe-dab90388f22a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "024e3d26-b540-4976-8824-1a308aed4a7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccce6f4a-b869-46a2-aefe-dab90388f22a",
                    "LayerId": "64feff86-9b89-4e85-ac3d-bdace1c04e4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "64feff86-9b89-4e85-ac3d-bdace1c04e4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae5bbdb5-ce39-4c7a-91fd-0bbae0757bda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}