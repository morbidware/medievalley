{
    "id": "bffd9da7-373a-4e27-8cae-6df1a6f432fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f427200-fdaa-42f2-ac7f-27b52693240d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bffd9da7-373a-4e27-8cae-6df1a6f432fd",
            "compositeImage": {
                "id": "f47ab224-ee8e-45dc-8887-ba7b5c2601e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f427200-fdaa-42f2-ac7f-27b52693240d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ff238f-8028-4b16-9b62-85d04dad100b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f427200-fdaa-42f2-ac7f-27b52693240d",
                    "LayerId": "5a66cd02-ed7a-4f17-af74-b65aca48b037"
                }
            ]
        },
        {
            "id": "e057b0ba-5988-48dd-9751-10e65e2328a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bffd9da7-373a-4e27-8cae-6df1a6f432fd",
            "compositeImage": {
                "id": "ce1005dd-ae80-4888-a216-a2c83a6cb664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e057b0ba-5988-48dd-9751-10e65e2328a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "352efa16-6a43-4ae4-8c0a-c358bf281baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e057b0ba-5988-48dd-9751-10e65e2328a5",
                    "LayerId": "5a66cd02-ed7a-4f17-af74-b65aca48b037"
                }
            ]
        },
        {
            "id": "00c22088-8ef7-4d9d-8110-9b404d95090c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bffd9da7-373a-4e27-8cae-6df1a6f432fd",
            "compositeImage": {
                "id": "7ff2b691-52da-4b6e-8817-0716e90737d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c22088-8ef7-4d9d-8110-9b404d95090c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e64390-5ae7-4615-9dd4-c0e4f1e18274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c22088-8ef7-4d9d-8110-9b404d95090c",
                    "LayerId": "5a66cd02-ed7a-4f17-af74-b65aca48b037"
                }
            ]
        },
        {
            "id": "c1543cd9-60b2-4322-ac1d-ee9fede5c52a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bffd9da7-373a-4e27-8cae-6df1a6f432fd",
            "compositeImage": {
                "id": "e0df09f0-57ec-478a-af12-79968c2e8693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1543cd9-60b2-4322-ac1d-ee9fede5c52a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70637e2-cd59-4566-9332-07343933546f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1543cd9-60b2-4322-ac1d-ee9fede5c52a",
                    "LayerId": "5a66cd02-ed7a-4f17-af74-b65aca48b037"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5a66cd02-ed7a-4f17-af74-b65aca48b037",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bffd9da7-373a-4e27-8cae-6df1a6f432fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}