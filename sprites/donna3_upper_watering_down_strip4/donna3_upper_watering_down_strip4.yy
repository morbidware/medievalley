{
    "id": "bfd1496d-2d56-4b2b-b0b8-83e12020fd14",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa618e39-20e9-4114-9681-c911c910a6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd1496d-2d56-4b2b-b0b8-83e12020fd14",
            "compositeImage": {
                "id": "1fdf4c98-e5d2-49ea-bc9a-bec4fe767184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa618e39-20e9-4114-9681-c911c910a6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca2500b-e18a-41ee-abc1-44acd708944e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa618e39-20e9-4114-9681-c911c910a6e7",
                    "LayerId": "4246eb12-8686-43f9-ad08-26d25f9bb268"
                }
            ]
        },
        {
            "id": "2274e8d1-d95c-4d27-a773-6d1ff6093ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd1496d-2d56-4b2b-b0b8-83e12020fd14",
            "compositeImage": {
                "id": "68f9f635-e7cd-4cd5-902f-011a499c274a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2274e8d1-d95c-4d27-a773-6d1ff6093ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aab77ac4-898f-4e3c-9696-7bcb99270d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2274e8d1-d95c-4d27-a773-6d1ff6093ed1",
                    "LayerId": "4246eb12-8686-43f9-ad08-26d25f9bb268"
                }
            ]
        },
        {
            "id": "3a484cf6-a9a2-4e22-8fb9-97550ea1ee59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd1496d-2d56-4b2b-b0b8-83e12020fd14",
            "compositeImage": {
                "id": "21b7e10f-501a-48b9-8f0d-753330a06ade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a484cf6-a9a2-4e22-8fb9-97550ea1ee59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8782ce77-d7f9-43ff-b91e-a91b563d010d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a484cf6-a9a2-4e22-8fb9-97550ea1ee59",
                    "LayerId": "4246eb12-8686-43f9-ad08-26d25f9bb268"
                }
            ]
        },
        {
            "id": "b585685e-5487-4b8c-b9db-315723c51e4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfd1496d-2d56-4b2b-b0b8-83e12020fd14",
            "compositeImage": {
                "id": "d98da62d-c8be-4def-9024-eb27c634927f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b585685e-5487-4b8c-b9db-315723c51e4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb314ff-2b58-4a91-a0bb-13b367b35b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b585685e-5487-4b8c-b9db-315723c51e4a",
                    "LayerId": "4246eb12-8686-43f9-ad08-26d25f9bb268"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4246eb12-8686-43f9-ad08-26d25f9bb268",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfd1496d-2d56-4b2b-b0b8-83e12020fd14",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}