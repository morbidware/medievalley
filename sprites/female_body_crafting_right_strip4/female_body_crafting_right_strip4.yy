{
    "id": "03e75b2c-d9e5-4b9c-97e2-8d6d9ce3d442",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 4,
    "bbox_right": 15,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26f4121d-95a6-4526-a7b5-9560c9a9e11f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e75b2c-d9e5-4b9c-97e2-8d6d9ce3d442",
            "compositeImage": {
                "id": "6447c730-f910-4ace-b06b-f1e084ff4ad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f4121d-95a6-4526-a7b5-9560c9a9e11f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f16bd297-3015-4690-865d-acde05313ac3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f4121d-95a6-4526-a7b5-9560c9a9e11f",
                    "LayerId": "a1ad69e6-2a6b-4d49-bba2-a3c33e6a5269"
                }
            ]
        },
        {
            "id": "e72e1456-af47-4efd-8762-8386c08e0830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e75b2c-d9e5-4b9c-97e2-8d6d9ce3d442",
            "compositeImage": {
                "id": "5bb44364-e52a-4665-a22a-6ce70a7bc5bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72e1456-af47-4efd-8762-8386c08e0830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7542390e-8669-4e35-b2b7-df2f98ad78e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72e1456-af47-4efd-8762-8386c08e0830",
                    "LayerId": "a1ad69e6-2a6b-4d49-bba2-a3c33e6a5269"
                }
            ]
        },
        {
            "id": "5b6a0a83-4d3b-4e45-b3ce-0939464972f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e75b2c-d9e5-4b9c-97e2-8d6d9ce3d442",
            "compositeImage": {
                "id": "4d091a59-dfb0-4822-99db-1815317e5c6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b6a0a83-4d3b-4e45-b3ce-0939464972f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4905cf3-d778-48f1-ac72-1ebb15d172d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b6a0a83-4d3b-4e45-b3ce-0939464972f2",
                    "LayerId": "a1ad69e6-2a6b-4d49-bba2-a3c33e6a5269"
                }
            ]
        },
        {
            "id": "78026b5c-20ec-4f5a-8bd3-3ea24cbd147c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e75b2c-d9e5-4b9c-97e2-8d6d9ce3d442",
            "compositeImage": {
                "id": "1265c548-f3ce-4711-a36a-1689328a74b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78026b5c-20ec-4f5a-8bd3-3ea24cbd147c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa7e9164-20b2-4c34-b8ba-1a021bf01361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78026b5c-20ec-4f5a-8bd3-3ea24cbd147c",
                    "LayerId": "a1ad69e6-2a6b-4d49-bba2-a3c33e6a5269"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a1ad69e6-2a6b-4d49-bba2-a3c33e6a5269",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03e75b2c-d9e5-4b9c-97e2-8d6d9ce3d442",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}