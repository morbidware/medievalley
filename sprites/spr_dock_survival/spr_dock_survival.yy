{
    "id": "96b97dbb-ea54-4078-b3e3-ddf8da9b3168",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15c87c2d-c73c-4c66-9a79-338622cdc0ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96b97dbb-ea54-4078-b3e3-ddf8da9b3168",
            "compositeImage": {
                "id": "439ac916-4348-4295-8b1c-3dd5529e7c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c87c2d-c73c-4c66-9a79-338622cdc0ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f826738-571c-41d7-851a-9b51462a09df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c87c2d-c73c-4c66-9a79-338622cdc0ac",
                    "LayerId": "a96ff324-1e4d-4b87-bd55-c57df743ed55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 137,
    "layers": [
        {
            "id": "a96ff324-1e4d-4b87-bd55-c57df743ed55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96b97dbb-ea54-4078-b3e3-ddf8da9b3168",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}