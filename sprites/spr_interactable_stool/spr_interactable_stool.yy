{
    "id": "97840556-11cd-482b-b911-53179d43218e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_stool",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4c4fee7-3dfc-4f75-9597-c7cea083a0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97840556-11cd-482b-b911-53179d43218e",
            "compositeImage": {
                "id": "4b2cec87-7ac5-4d7d-996c-12e8c18fb3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4c4fee7-3dfc-4f75-9597-c7cea083a0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33c66f80-ae1f-40fe-8c34-3381c3b1ec3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4c4fee7-3dfc-4f75-9597-c7cea083a0bf",
                    "LayerId": "395747c3-e489-4edf-8cc6-3438bec5707c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "395747c3-e489-4edf-8cc6-3438bec5707c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97840556-11cd-482b-b911-53179d43218e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}