{
    "id": "8eea5484-a685-43ab-bdba-8e2abf76ab78",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35396ed8-8d13-4c14-a837-6e3177bbfe57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eea5484-a685-43ab-bdba-8e2abf76ab78",
            "compositeImage": {
                "id": "4fbd0dd3-70ea-4e8f-bf1e-c9a7143addc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35396ed8-8d13-4c14-a837-6e3177bbfe57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cda443d9-e17b-4c1f-b735-008553a18ab9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35396ed8-8d13-4c14-a837-6e3177bbfe57",
                    "LayerId": "fe5279f1-b61a-4088-b2d4-2c22312ebbf5"
                }
            ]
        },
        {
            "id": "55e09db1-88db-4930-a426-52555c0077dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eea5484-a685-43ab-bdba-8e2abf76ab78",
            "compositeImage": {
                "id": "25211504-2e61-4cf1-95df-e374492671c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e09db1-88db-4930-a426-52555c0077dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876036e5-da1a-4237-9a6e-a80c97bfdc68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e09db1-88db-4930-a426-52555c0077dd",
                    "LayerId": "fe5279f1-b61a-4088-b2d4-2c22312ebbf5"
                }
            ]
        },
        {
            "id": "b2265b0a-497f-420e-9dbf-f568504e4645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eea5484-a685-43ab-bdba-8e2abf76ab78",
            "compositeImage": {
                "id": "1dde0026-282e-4f0f-90a5-9a3cde3ef010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2265b0a-497f-420e-9dbf-f568504e4645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "405d4635-cce3-49c7-8b7f-2e5b60b4ed25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2265b0a-497f-420e-9dbf-f568504e4645",
                    "LayerId": "fe5279f1-b61a-4088-b2d4-2c22312ebbf5"
                }
            ]
        },
        {
            "id": "5deec91e-1d94-4bd4-aea8-b42be25d1fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8eea5484-a685-43ab-bdba-8e2abf76ab78",
            "compositeImage": {
                "id": "d95b7f19-05e7-4c68-a899-6386888d243a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5deec91e-1d94-4bd4-aea8-b42be25d1fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec3baf9f-1894-4c0d-8a91-c5a8b50dcfd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5deec91e-1d94-4bd4-aea8-b42be25d1fca",
                    "LayerId": "fe5279f1-b61a-4088-b2d4-2c22312ebbf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fe5279f1-b61a-4088-b2d4-2c22312ebbf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8eea5484-a685-43ab-bdba-8e2abf76ab78",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}