{
    "id": "75b4dfb1-a864-4993-8b7c-4897b312da4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d148bab0-7b51-4fef-9cec-dbee70ecf31c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b4dfb1-a864-4993-8b7c-4897b312da4b",
            "compositeImage": {
                "id": "df9d0793-79e7-45d1-b067-e6a36841df2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d148bab0-7b51-4fef-9cec-dbee70ecf31c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b96760-64cd-4f8d-85d7-54362a36c18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d148bab0-7b51-4fef-9cec-dbee70ecf31c",
                    "LayerId": "ec70a7b4-7fda-4156-aeee-1660c36606c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ec70a7b4-7fda-4156-aeee-1660c36606c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75b4dfb1-a864-4993-8b7c-4897b312da4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}