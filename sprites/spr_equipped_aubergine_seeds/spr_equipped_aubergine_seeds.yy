{
    "id": "e34150b9-5117-4572-b13c-c73a68923bbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_aubergine_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5fb8e93-554e-4260-8fa0-8f59846ec918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34150b9-5117-4572-b13c-c73a68923bbb",
            "compositeImage": {
                "id": "d9bf528a-a6ca-4fe4-a693-ff17dd59d24e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5fb8e93-554e-4260-8fa0-8f59846ec918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1988a10b-7325-4aa1-b1a3-a8e65bababee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5fb8e93-554e-4260-8fa0-8f59846ec918",
                    "LayerId": "c51323d8-8afd-4737-aa7d-85bb51c85843"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c51323d8-8afd-4737-aa7d-85bb51c85843",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e34150b9-5117-4572-b13c-c73a68923bbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}