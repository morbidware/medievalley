{
    "id": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_melee_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "611baa62-084b-4609-86b0-144eba0c9537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "compositeImage": {
                "id": "dcd05b62-debb-4ef5-826f-af46ba0258a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "611baa62-084b-4609-86b0-144eba0c9537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5e7899e-e9d9-440c-9aab-3bc850d07ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611baa62-084b-4609-86b0-144eba0c9537",
                    "LayerId": "2c92b893-ddf6-49a6-b941-64792306018b"
                }
            ]
        },
        {
            "id": "d8376a19-eb72-44d4-954a-06e6034648ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "compositeImage": {
                "id": "aede7467-8afe-4a9f-9486-c4234cf0d2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8376a19-eb72-44d4-954a-06e6034648ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e1704c5-af40-433e-85d1-783d1d79acec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8376a19-eb72-44d4-954a-06e6034648ad",
                    "LayerId": "2c92b893-ddf6-49a6-b941-64792306018b"
                }
            ]
        },
        {
            "id": "52863ab0-19f9-498a-b423-7decc7ce5799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "compositeImage": {
                "id": "ab10daee-689b-484f-bf13-480b8c5197be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52863ab0-19f9-498a-b423-7decc7ce5799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3296c49-eaa1-4b41-b3c8-352bb1f126f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52863ab0-19f9-498a-b423-7decc7ce5799",
                    "LayerId": "2c92b893-ddf6-49a6-b941-64792306018b"
                }
            ]
        },
        {
            "id": "517ce0e5-0ab6-43a1-8d0c-2e345648a2d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "compositeImage": {
                "id": "abad051e-9267-48e7-8dd2-91c230c5924f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517ce0e5-0ab6-43a1-8d0c-2e345648a2d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180b8a64-78d1-4a65-b7bf-6d358f934fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517ce0e5-0ab6-43a1-8d0c-2e345648a2d5",
                    "LayerId": "2c92b893-ddf6-49a6-b941-64792306018b"
                }
            ]
        },
        {
            "id": "6ec5be1d-14b1-45bb-a5a0-9378386a8cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "compositeImage": {
                "id": "a9133f76-81fa-412c-a569-8474ca8d8085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec5be1d-14b1-45bb-a5a0-9378386a8cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "260687cd-6a44-48aa-ab0c-e3ae8de9c4d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec5be1d-14b1-45bb-a5a0-9378386a8cbe",
                    "LayerId": "2c92b893-ddf6-49a6-b941-64792306018b"
                }
            ]
        },
        {
            "id": "5d5b2973-f62f-4690-b4e3-e38f645cdad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "compositeImage": {
                "id": "9dfd4b5d-c56e-4a1e-b205-f8b0a157097b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d5b2973-f62f-4690-b4e3-e38f645cdad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1243a002-24c6-4c0f-ac76-d38a13b9a384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d5b2973-f62f-4690-b4e3-e38f645cdad1",
                    "LayerId": "2c92b893-ddf6-49a6-b941-64792306018b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c92b893-ddf6-49a6-b941-64792306018b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d61d0982-4b7e-4d76-ba99-0f1d80f0d2e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}