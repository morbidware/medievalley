{
    "id": "6984ed72-7bbf-4edc-95ec-f6a7e5f24bc9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stash_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 133,
    "bbox_left": 0,
    "bbox_right": 229,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "606ba9ae-8981-4b5a-b0ed-2de7a473eef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6984ed72-7bbf-4edc-95ec-f6a7e5f24bc9",
            "compositeImage": {
                "id": "b493dcca-02c9-4d08-aca5-e10ac90a9686",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606ba9ae-8981-4b5a-b0ed-2de7a473eef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23d067d6-1938-49be-a841-339b48a371e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606ba9ae-8981-4b5a-b0ed-2de7a473eef2",
                    "LayerId": "1fc65706-91c5-4c17-9666-a932ac84b464"
                }
            ]
        }
    ],
    "gridX": 115,
    "gridY": 67,
    "height": 134,
    "layers": [
        {
            "id": "1fc65706-91c5-4c17-9666-a932ac84b464",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6984ed72-7bbf-4edc-95ec-f6a7e5f24bc9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 230,
    "xorig": 115,
    "yorig": 67
}