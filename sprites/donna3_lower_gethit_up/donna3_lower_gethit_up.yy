{
    "id": "711bdabc-5f80-43fb-93b4-5a6ad8390351",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2878f16e-f935-4bee-ac18-0ccd7e0a4a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "711bdabc-5f80-43fb-93b4-5a6ad8390351",
            "compositeImage": {
                "id": "98ab9dce-1c62-4578-a9fe-fe26251f5f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2878f16e-f935-4bee-ac18-0ccd7e0a4a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b03ebe-4ef3-4eec-a9ae-b96efdc9e4ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2878f16e-f935-4bee-ac18-0ccd7e0a4a6c",
                    "LayerId": "0fe10505-dcf0-49a5-aaa8-6a4eada07fa3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0fe10505-dcf0-49a5-aaa8-6a4eada07fa3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "711bdabc-5f80-43fb-93b4-5a6ad8390351",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}