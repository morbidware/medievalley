{
    "id": "17f09786-1ef8-46cd-a9e0-8587140710ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_corn_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c909031-3edb-496b-806c-9067aa8645b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
            "compositeImage": {
                "id": "b055f973-f19c-4d8d-af3a-7db1ba874014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c909031-3edb-496b-806c-9067aa8645b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7089ca3-a573-4d85-93ec-320cfd7fe89b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c909031-3edb-496b-806c-9067aa8645b7",
                    "LayerId": "5707a0cf-f16d-4e61-b70c-64ae9be6fcc2"
                }
            ]
        },
        {
            "id": "5b45904d-e8b8-4175-8824-39047705fbb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
            "compositeImage": {
                "id": "3a2995bb-ce3c-4290-8413-4168cf78033e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b45904d-e8b8-4175-8824-39047705fbb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "901621d4-9504-473b-ab7b-87b303a22947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b45904d-e8b8-4175-8824-39047705fbb1",
                    "LayerId": "5707a0cf-f16d-4e61-b70c-64ae9be6fcc2"
                }
            ]
        },
        {
            "id": "a6326f7d-d383-43d7-9481-e7d9e466ab61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
            "compositeImage": {
                "id": "7c5f2aa2-f47c-45e8-910d-55aebc09580e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6326f7d-d383-43d7-9481-e7d9e466ab61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "238e97c4-e6b5-4eac-8762-a557bd57b9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6326f7d-d383-43d7-9481-e7d9e466ab61",
                    "LayerId": "5707a0cf-f16d-4e61-b70c-64ae9be6fcc2"
                }
            ]
        },
        {
            "id": "e4d64283-c5c7-46ba-95ee-483f3faedcc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
            "compositeImage": {
                "id": "e02b397c-43e9-41a7-8088-e8e80b6062e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4d64283-c5c7-46ba-95ee-483f3faedcc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a2d637c-3480-4140-a88b-dfcae41d10a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4d64283-c5c7-46ba-95ee-483f3faedcc1",
                    "LayerId": "5707a0cf-f16d-4e61-b70c-64ae9be6fcc2"
                }
            ]
        },
        {
            "id": "7c0c6308-cf76-43c0-bca5-839fa59335fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
            "compositeImage": {
                "id": "09bb159d-eba8-48b7-82c3-aabfcae4d4bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0c6308-cf76-43c0-bca5-839fa59335fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0feeb430-aa78-4756-942c-251c9e972a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0c6308-cf76-43c0-bca5-839fa59335fb",
                    "LayerId": "5707a0cf-f16d-4e61-b70c-64ae9be6fcc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5707a0cf-f16d-4e61-b70c-64ae9be6fcc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17f09786-1ef8-46cd-a9e0-8587140710ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 28
}