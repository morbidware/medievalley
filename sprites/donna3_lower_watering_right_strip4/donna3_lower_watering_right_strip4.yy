{
    "id": "e6247a2e-fb82-42ea-b9bb-fa56f479177f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1533bff-79aa-4947-ab37-adf638874170",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6247a2e-fb82-42ea-b9bb-fa56f479177f",
            "compositeImage": {
                "id": "0bb139f9-310e-450b-b12c-bab5dfb4b569",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1533bff-79aa-4947-ab37-adf638874170",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cb7435-a87c-41be-8f1c-822e5d69a42c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1533bff-79aa-4947-ab37-adf638874170",
                    "LayerId": "62b4324c-e93d-4a16-a1d4-880486e88a59"
                }
            ]
        },
        {
            "id": "04014609-d6cc-4982-a36e-deb8dd8f3575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6247a2e-fb82-42ea-b9bb-fa56f479177f",
            "compositeImage": {
                "id": "4c2e6266-c5df-4cdd-a721-a270040e5ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04014609-d6cc-4982-a36e-deb8dd8f3575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3557c9fb-987b-445e-adfc-49a6f50cda31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04014609-d6cc-4982-a36e-deb8dd8f3575",
                    "LayerId": "62b4324c-e93d-4a16-a1d4-880486e88a59"
                }
            ]
        },
        {
            "id": "cf398465-bd83-4587-b5ae-114d498e2656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6247a2e-fb82-42ea-b9bb-fa56f479177f",
            "compositeImage": {
                "id": "e5f96ddd-f76e-4442-886d-4f98223729d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf398465-bd83-4587-b5ae-114d498e2656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d514bdd0-2f55-42c4-bf5d-dd9a19d4a689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf398465-bd83-4587-b5ae-114d498e2656",
                    "LayerId": "62b4324c-e93d-4a16-a1d4-880486e88a59"
                }
            ]
        },
        {
            "id": "17b359f8-fc7d-4cfb-9270-52c3d858e019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6247a2e-fb82-42ea-b9bb-fa56f479177f",
            "compositeImage": {
                "id": "eddd9931-3fbe-4ec2-b83c-699668f9edfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17b359f8-fc7d-4cfb-9270-52c3d858e019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2901d0b6-996f-421f-9d51-ecac6140c506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17b359f8-fc7d-4cfb-9270-52c3d858e019",
                    "LayerId": "62b4324c-e93d-4a16-a1d4-880486e88a59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "62b4324c-e93d-4a16-a1d4-880486e88a59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6247a2e-fb82-42ea-b9bb-fa56f479177f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}