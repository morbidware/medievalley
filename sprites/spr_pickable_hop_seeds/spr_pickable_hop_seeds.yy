{
    "id": "1ab1c7c4-c5af-4606-a497-bc02920105d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_hop_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7306ae37-72ec-41ae-aad6-e88ffc6de175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ab1c7c4-c5af-4606-a497-bc02920105d8",
            "compositeImage": {
                "id": "e26bf313-d983-4e29-8247-a83e46a3149c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7306ae37-72ec-41ae-aad6-e88ffc6de175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b719470a-6528-4d8b-ba7f-82180e1641ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7306ae37-72ec-41ae-aad6-e88ffc6de175",
                    "LayerId": "4f7cb027-ece4-46a2-8674-f698aa3c0177"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4f7cb027-ece4-46a2-8674-f698aa3c0177",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ab1c7c4-c5af-4606-a497-bc02920105d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}