{
    "id": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_crafting_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d434a3e-1727-41ef-b76d-1f65f87504dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "compositeImage": {
                "id": "c4da70de-5e98-4e52-bcd2-e9e50a2b28de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d434a3e-1727-41ef-b76d-1f65f87504dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd66cd3f-dcc7-49c9-9157-5792181357b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d434a3e-1727-41ef-b76d-1f65f87504dd",
                    "LayerId": "cc92033b-7008-4105-999b-659d772b231e"
                }
            ]
        },
        {
            "id": "3135e1c4-0b9e-44fb-b9d9-b547abfca962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "compositeImage": {
                "id": "ae6d71ec-f061-4a0c-bd75-fe044448df53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3135e1c4-0b9e-44fb-b9d9-b547abfca962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d050ffd-e009-4c49-9905-99e13c8292d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3135e1c4-0b9e-44fb-b9d9-b547abfca962",
                    "LayerId": "cc92033b-7008-4105-999b-659d772b231e"
                }
            ]
        },
        {
            "id": "5f334d57-b759-45c4-bf6f-2c550f40c388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "compositeImage": {
                "id": "be655469-0f2b-4b1e-a35f-89195f4a761c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f334d57-b759-45c4-bf6f-2c550f40c388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5603ba3-8e53-44d7-870e-d73d3fbdcb1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f334d57-b759-45c4-bf6f-2c550f40c388",
                    "LayerId": "cc92033b-7008-4105-999b-659d772b231e"
                }
            ]
        },
        {
            "id": "38c2ea12-9f21-4878-9dc8-d26077a9f8c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "compositeImage": {
                "id": "14b0851a-4ef1-4b66-95f0-0fd41079a4a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c2ea12-9f21-4878-9dc8-d26077a9f8c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9804bd0e-4601-46ea-8e0c-0af04b234768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c2ea12-9f21-4878-9dc8-d26077a9f8c6",
                    "LayerId": "cc92033b-7008-4105-999b-659d772b231e"
                }
            ]
        },
        {
            "id": "6b1ee2bf-a2c3-4465-a5be-4ba134876e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "compositeImage": {
                "id": "a5adca39-96d7-4bae-bb2c-82b5b66b663b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1ee2bf-a2c3-4465-a5be-4ba134876e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76bf8275-9da8-4b7c-b366-d444f380acaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1ee2bf-a2c3-4465-a5be-4ba134876e20",
                    "LayerId": "cc92033b-7008-4105-999b-659d772b231e"
                }
            ]
        },
        {
            "id": "ee2911ef-0ebe-406d-9aa6-69e87947acb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "compositeImage": {
                "id": "e14b6658-91a5-4f8e-ae26-5a84c8479e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2911ef-0ebe-406d-9aa6-69e87947acb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "775cf14d-b506-4056-9234-f2026b630a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2911ef-0ebe-406d-9aa6-69e87947acb4",
                    "LayerId": "cc92033b-7008-4105-999b-659d772b231e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc92033b-7008-4105-999b-659d772b231e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ebaa339-4c6f-4710-aa5d-e2c7f1f03e45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}