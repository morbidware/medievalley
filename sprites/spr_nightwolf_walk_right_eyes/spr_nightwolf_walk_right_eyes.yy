{
    "id": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_walk_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 41,
    "bbox_right": 41,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17ef7cfd-dfc9-459b-a903-0845ed8c5e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "compositeImage": {
                "id": "5d2da911-abf9-45d6-9fd6-deb2d2952397",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17ef7cfd-dfc9-459b-a903-0845ed8c5e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f973c779-94ed-4753-b1bd-15f68b2d660a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17ef7cfd-dfc9-459b-a903-0845ed8c5e67",
                    "LayerId": "9ad2eb7c-c585-4fb4-a78b-be76753d553e"
                }
            ]
        },
        {
            "id": "b4eb10f7-fdc2-4d86-a21a-ab40b5609f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "compositeImage": {
                "id": "0420ab72-e33b-488f-b870-394dfc6f592f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4eb10f7-fdc2-4d86-a21a-ab40b5609f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32514d1c-35ef-4c1c-92ba-f7aa5bcadbb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4eb10f7-fdc2-4d86-a21a-ab40b5609f39",
                    "LayerId": "9ad2eb7c-c585-4fb4-a78b-be76753d553e"
                }
            ]
        },
        {
            "id": "ffaf3023-a5e2-4f74-9be3-9c32db20ff1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "compositeImage": {
                "id": "de398e51-8242-4d77-8932-88e59e53c3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffaf3023-a5e2-4f74-9be3-9c32db20ff1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaf21fa6-ffa7-4003-afeb-4b026ce8ab26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffaf3023-a5e2-4f74-9be3-9c32db20ff1b",
                    "LayerId": "9ad2eb7c-c585-4fb4-a78b-be76753d553e"
                }
            ]
        },
        {
            "id": "b4026052-38d0-4678-a677-711144bfe717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "compositeImage": {
                "id": "fc28c3f7-43f3-42f1-8078-65f3d901e53f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4026052-38d0-4678-a677-711144bfe717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6c82e35-9472-4348-b4ba-534a841b3d7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4026052-38d0-4678-a677-711144bfe717",
                    "LayerId": "9ad2eb7c-c585-4fb4-a78b-be76753d553e"
                }
            ]
        },
        {
            "id": "68eae4a9-60ad-4b26-9984-6449c1816549",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "compositeImage": {
                "id": "5f0ef7a5-65fc-4a3e-9b6c-7a66f179e7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68eae4a9-60ad-4b26-9984-6449c1816549",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c36b545-4542-40f3-a3a3-86565ca372ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68eae4a9-60ad-4b26-9984-6449c1816549",
                    "LayerId": "9ad2eb7c-c585-4fb4-a78b-be76753d553e"
                }
            ]
        },
        {
            "id": "fee0d9dc-9dbc-499b-a9c0-944390874637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "compositeImage": {
                "id": "5a49cf4f-114f-4928-b34c-9ea09682f5ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fee0d9dc-9dbc-499b-a9c0-944390874637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491864cd-bc45-484e-9aa9-79916491b659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fee0d9dc-9dbc-499b-a9c0-944390874637",
                    "LayerId": "9ad2eb7c-c585-4fb4-a78b-be76753d553e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "9ad2eb7c-c585-4fb4-a78b-be76753d553e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "603a39df-d5b8-46b4-9277-079c9cd59e2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}