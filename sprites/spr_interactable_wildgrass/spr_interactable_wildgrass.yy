{
    "id": "cb762e9d-9ff2-458e-8f0a-784dcdabc941",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_wildgrass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7f4578b-9ac3-4552-8679-a0ab5903a402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb762e9d-9ff2-458e-8f0a-784dcdabc941",
            "compositeImage": {
                "id": "2279c944-c038-4049-bcb5-c0661abc7362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f4578b-9ac3-4552-8679-a0ab5903a402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e82b09d-a923-4d01-b77b-d6d59eaf94eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f4578b-9ac3-4552-8679-a0ab5903a402",
                    "LayerId": "58f3f526-00a1-438f-bd88-e3e974716587"
                }
            ]
        },
        {
            "id": "f3d50ddd-9224-468f-8da6-eb71a952bc97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb762e9d-9ff2-458e-8f0a-784dcdabc941",
            "compositeImage": {
                "id": "0658944b-51bd-46fb-a9ba-ffca2ee55148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d50ddd-9224-468f-8da6-eb71a952bc97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e22ff1ea-ecc8-4c3b-9ff0-657574d8d34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d50ddd-9224-468f-8da6-eb71a952bc97",
                    "LayerId": "58f3f526-00a1-438f-bd88-e3e974716587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "58f3f526-00a1-438f-bd88-e3e974716587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb762e9d-9ff2-458e-8f0a-784dcdabc941",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}