{
    "id": "eb5e4faf-79f4-495b-96a6-8936419e947f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_blob",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c43a27c4-8de6-4ddd-acf6-792f24606266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb5e4faf-79f4-495b-96a6-8936419e947f",
            "compositeImage": {
                "id": "d56e2720-17a3-47af-9eaa-96af1a8252ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c43a27c4-8de6-4ddd-acf6-792f24606266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02df6d0a-b570-4db9-a62d-494c509150b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c43a27c4-8de6-4ddd-acf6-792f24606266",
                    "LayerId": "11e8749b-a9f4-46b3-896b-3b91213ba1b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "11e8749b-a9f4-46b3-896b-3b91213ba1b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb5e4faf-79f4-495b-96a6-8936419e947f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}