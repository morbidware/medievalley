{
    "id": "ff58b064-7fb8-461b-837d-234dcff26ae8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_egg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5831ad5a-3e91-4740-a2a6-68cd7cbbdc86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff58b064-7fb8-461b-837d-234dcff26ae8",
            "compositeImage": {
                "id": "d2e84d98-b9b6-48c4-9fd3-9a14046f4904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5831ad5a-3e91-4740-a2a6-68cd7cbbdc86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb6fe9f1-5fbb-4e05-beb6-e46e7fe247fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5831ad5a-3e91-4740-a2a6-68cd7cbbdc86",
                    "LayerId": "b688b333-bf99-4c17-9e3d-2448c17ce3c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b688b333-bf99-4c17-9e3d-2448c17ce3c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff58b064-7fb8-461b-837d-234dcff26ae8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 13
}