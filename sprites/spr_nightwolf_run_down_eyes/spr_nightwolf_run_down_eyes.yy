{
    "id": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_run_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 21,
    "bbox_right": 26,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bd98634-b1fa-466c-97d9-0e50b45993a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "compositeImage": {
                "id": "df1218d5-2c69-4d76-ab0b-947c8f383b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd98634-b1fa-466c-97d9-0e50b45993a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c217c0d5-eebe-49ef-888f-cb086ae40ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd98634-b1fa-466c-97d9-0e50b45993a7",
                    "LayerId": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6"
                }
            ]
        },
        {
            "id": "87fbdf4c-170b-42aa-9364-c8d469f596fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "compositeImage": {
                "id": "8566c8ad-b745-4ee2-86f1-b7e67b1f2491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87fbdf4c-170b-42aa-9364-c8d469f596fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "436311f8-d4ee-4294-9aba-96f4fa626ae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87fbdf4c-170b-42aa-9364-c8d469f596fe",
                    "LayerId": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6"
                }
            ]
        },
        {
            "id": "a2e12d7e-004f-4d6a-bfd0-e23e1076a219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "compositeImage": {
                "id": "6990c72a-ba9f-4aad-9073-299d1e12a72e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e12d7e-004f-4d6a-bfd0-e23e1076a219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f04437-53d9-45fd-b543-60c47db8e49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e12d7e-004f-4d6a-bfd0-e23e1076a219",
                    "LayerId": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6"
                }
            ]
        },
        {
            "id": "ac9345d9-7907-4cb3-8d83-86751a2912a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "compositeImage": {
                "id": "c179b0c9-a243-41ff-b071-0d2924718bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac9345d9-7907-4cb3-8d83-86751a2912a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa40a86-759d-4c61-89c0-8044ae8deb62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac9345d9-7907-4cb3-8d83-86751a2912a7",
                    "LayerId": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6"
                }
            ]
        },
        {
            "id": "c8f62d1f-d716-4e41-be6a-9c6401620ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "compositeImage": {
                "id": "47c30080-09d5-4b0d-b9f6-714843e5772c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8f62d1f-d716-4e41-be6a-9c6401620ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66a474a-d88b-45ed-8535-780c29168769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8f62d1f-d716-4e41-be6a-9c6401620ddd",
                    "LayerId": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6"
                }
            ]
        },
        {
            "id": "b9f2f905-fb7b-4299-b3c7-fe66012cc30a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "compositeImage": {
                "id": "4c74a527-5c12-49e3-ac9b-16e1e6386147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9f2f905-fb7b-4299-b3c7-fe66012cc30a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f835bb-4cbf-4554-a5cf-dbf5c571f695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9f2f905-fb7b-4299-b3c7-fe66012cc30a",
                    "LayerId": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "dc2793d6-d777-4e5c-9fb1-c56281bb63a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca2604de-665e-4c3a-aa44-e30f6c114b85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}