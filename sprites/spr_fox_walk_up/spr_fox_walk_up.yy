{
    "id": "261102d3-63c3-4ab8-9a6c-749fe42a0869",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be25b0dc-50a0-43d5-9567-e173f1866d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "261102d3-63c3-4ab8-9a6c-749fe42a0869",
            "compositeImage": {
                "id": "8e766323-ac08-47eb-80dd-b0dab3e4fcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be25b0dc-50a0-43d5-9567-e173f1866d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e71b750b-7fee-4727-86c8-f47bf0a1439e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be25b0dc-50a0-43d5-9567-e173f1866d03",
                    "LayerId": "ac1e77ba-4d08-4dce-b42e-77aec811e465"
                }
            ]
        },
        {
            "id": "f5704f26-cdc7-4a09-8c8c-db4065c8bba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "261102d3-63c3-4ab8-9a6c-749fe42a0869",
            "compositeImage": {
                "id": "5eb627a3-090e-4441-8682-5edcc660dcb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5704f26-cdc7-4a09-8c8c-db4065c8bba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b453602-a5d5-4d35-80fd-36ddb6597d77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5704f26-cdc7-4a09-8c8c-db4065c8bba5",
                    "LayerId": "ac1e77ba-4d08-4dce-b42e-77aec811e465"
                }
            ]
        },
        {
            "id": "6cc4dcf8-ec92-4714-9768-794a35ae864c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "261102d3-63c3-4ab8-9a6c-749fe42a0869",
            "compositeImage": {
                "id": "cb05b82a-2ea9-4cc0-9cda-7f877592810d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cc4dcf8-ec92-4714-9768-794a35ae864c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4ba6f2-2f3d-43db-a265-5742512027a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cc4dcf8-ec92-4714-9768-794a35ae864c",
                    "LayerId": "ac1e77ba-4d08-4dce-b42e-77aec811e465"
                }
            ]
        },
        {
            "id": "2a0acdd5-c75f-4fa7-abbf-315d4d52cba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "261102d3-63c3-4ab8-9a6c-749fe42a0869",
            "compositeImage": {
                "id": "5d23dc32-1f59-4dd0-b34e-b941ba851faa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0acdd5-c75f-4fa7-abbf-315d4d52cba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd00f7e-ab90-4410-b3cb-5055e4aeb9e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0acdd5-c75f-4fa7-abbf-315d4d52cba3",
                    "LayerId": "ac1e77ba-4d08-4dce-b42e-77aec811e465"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "ac1e77ba-4d08-4dce-b42e-77aec811e465",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "261102d3-63c3-4ab8-9a6c-749fe42a0869",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}