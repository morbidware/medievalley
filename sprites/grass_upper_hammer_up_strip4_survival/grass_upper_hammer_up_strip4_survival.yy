{
    "id": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_hammer_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a882ee0-4cc0-46f4-bc0b-104a18aadf66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "compositeImage": {
                "id": "3b3cc4da-3196-4123-a10c-b9c3706ebcb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a882ee0-4cc0-46f4-bc0b-104a18aadf66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1debcde-3fca-4fc8-a0f5-614313296457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a882ee0-4cc0-46f4-bc0b-104a18aadf66",
                    "LayerId": "0d966ae7-57fa-468e-b5e5-82c503c3eb75"
                }
            ]
        },
        {
            "id": "30b5f1bf-d9a9-4c44-b221-93b953bf586c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "compositeImage": {
                "id": "b6f676d3-a548-4325-8765-388ee739cdf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b5f1bf-d9a9-4c44-b221-93b953bf586c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cd538fc-7314-4845-9bd8-ba8b43271ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b5f1bf-d9a9-4c44-b221-93b953bf586c",
                    "LayerId": "0d966ae7-57fa-468e-b5e5-82c503c3eb75"
                }
            ]
        },
        {
            "id": "05c9ff07-d38a-4c83-aa6b-1f93b42f60f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "compositeImage": {
                "id": "0b9461f9-a89f-4d47-9b9e-9bb034056bc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c9ff07-d38a-4c83-aa6b-1f93b42f60f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b645bc6-41d5-4277-9bd6-75bd23746ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c9ff07-d38a-4c83-aa6b-1f93b42f60f5",
                    "LayerId": "0d966ae7-57fa-468e-b5e5-82c503c3eb75"
                }
            ]
        },
        {
            "id": "b6cbd2eb-3925-44e2-b4f6-6e534d5820be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "compositeImage": {
                "id": "652be26f-be4c-4951-8ccb-ac32f72aa72e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6cbd2eb-3925-44e2-b4f6-6e534d5820be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b31165-e1a3-45cf-bf45-7a9dcfeaa42c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6cbd2eb-3925-44e2-b4f6-6e534d5820be",
                    "LayerId": "0d966ae7-57fa-468e-b5e5-82c503c3eb75"
                }
            ]
        },
        {
            "id": "926a5fef-39a1-426a-af1d-b19cdd1ea728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "compositeImage": {
                "id": "1cc4e191-221d-4b64-88f5-05896f56eac6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926a5fef-39a1-426a-af1d-b19cdd1ea728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd878ce3-0f34-44e4-8b7c-3123a23abaab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926a5fef-39a1-426a-af1d-b19cdd1ea728",
                    "LayerId": "0d966ae7-57fa-468e-b5e5-82c503c3eb75"
                }
            ]
        },
        {
            "id": "f5e86ade-477b-46a6-9b55-22119aba4c85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "compositeImage": {
                "id": "e8f45e1b-a902-44f1-9584-bc285f85059c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5e86ade-477b-46a6-9b55-22119aba4c85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8062ebca-ed78-4e25-975a-c11123cb1146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5e86ade-477b-46a6-9b55-22119aba4c85",
                    "LayerId": "0d966ae7-57fa-468e-b5e5-82c503c3eb75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d966ae7-57fa-468e-b5e5-82c503c3eb75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c8c108f-f273-4acf-8cde-15e9d771b9bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}