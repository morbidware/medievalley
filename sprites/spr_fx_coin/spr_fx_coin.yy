{
    "id": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c6ddb45-6784-4600-a334-89d1e01a5033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "compositeImage": {
                "id": "31515bb2-e954-4175-967d-329d3eb4c5c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c6ddb45-6784-4600-a334-89d1e01a5033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e9189a4-e6e9-4f9d-b3a2-5ec6fc9fef8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c6ddb45-6784-4600-a334-89d1e01a5033",
                    "LayerId": "95dc776c-954a-46d0-a1e9-6644129fb7ab"
                }
            ]
        },
        {
            "id": "991e11d2-2d84-40e2-bb23-bab39647a6f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "compositeImage": {
                "id": "6019c49f-6789-4ebd-a951-5f572a7f1780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "991e11d2-2d84-40e2-bb23-bab39647a6f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d77dee-3875-43b9-8b2b-d79471f4ec3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "991e11d2-2d84-40e2-bb23-bab39647a6f5",
                    "LayerId": "95dc776c-954a-46d0-a1e9-6644129fb7ab"
                }
            ]
        },
        {
            "id": "dcbfbcde-fc5e-41aa-9758-1f36d226def8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "compositeImage": {
                "id": "ef1fb6a4-3893-4706-87f6-48e5a8ba5fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcbfbcde-fc5e-41aa-9758-1f36d226def8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44997f45-a66c-462f-921a-d094c2856e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcbfbcde-fc5e-41aa-9758-1f36d226def8",
                    "LayerId": "95dc776c-954a-46d0-a1e9-6644129fb7ab"
                }
            ]
        },
        {
            "id": "a16376a6-732c-4fff-87f9-8171a5dec42e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "compositeImage": {
                "id": "e94705ec-cca7-4111-8531-b5ff064f8227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16376a6-732c-4fff-87f9-8171a5dec42e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2935f8a-6ab5-4a69-aebe-801ab3209751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16376a6-732c-4fff-87f9-8171a5dec42e",
                    "LayerId": "95dc776c-954a-46d0-a1e9-6644129fb7ab"
                }
            ]
        },
        {
            "id": "7e65dad4-69eb-43c1-b327-4cd5424b1b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "compositeImage": {
                "id": "417ceb94-1687-4e31-b667-1e66acd21ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e65dad4-69eb-43c1-b327-4cd5424b1b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd6b756b-399a-4880-b6d1-b3147320ef3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e65dad4-69eb-43c1-b327-4cd5424b1b4a",
                    "LayerId": "95dc776c-954a-46d0-a1e9-6644129fb7ab"
                }
            ]
        },
        {
            "id": "5d83830f-0408-4266-a382-e586f25ef41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "compositeImage": {
                "id": "8c912aac-e1d4-479b-8fa9-9a88b800c9ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d83830f-0408-4266-a382-e586f25ef41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7304609-a802-45ab-9e19-e6a19e6dfdb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d83830f-0408-4266-a382-e586f25ef41e",
                    "LayerId": "95dc776c-954a-46d0-a1e9-6644129fb7ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "95dc776c-954a-46d0-a1e9-6644129fb7ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b09ec766-0c84-41c4-ae26-289115d7b0bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}