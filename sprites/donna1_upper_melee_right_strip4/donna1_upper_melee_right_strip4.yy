{
    "id": "6977e224-fe40-4310-b50d-26fd96a524c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06595391-ec74-4051-a7bf-f781dc415e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6977e224-fe40-4310-b50d-26fd96a524c1",
            "compositeImage": {
                "id": "dfd2f86b-a055-4a3e-9fb3-3af61adbd7af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06595391-ec74-4051-a7bf-f781dc415e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e93a4a11-a174-4722-b279-5ae35c181dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06595391-ec74-4051-a7bf-f781dc415e74",
                    "LayerId": "045b2091-1afd-4cfb-ae48-e0c151800715"
                }
            ]
        },
        {
            "id": "fdd0a58e-c917-4b7b-8f96-30d14ef4d0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6977e224-fe40-4310-b50d-26fd96a524c1",
            "compositeImage": {
                "id": "0c37630a-b8af-464f-9268-51fff4de17e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd0a58e-c917-4b7b-8f96-30d14ef4d0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8580ecbf-529d-4065-85c6-433a276ceef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd0a58e-c917-4b7b-8f96-30d14ef4d0e7",
                    "LayerId": "045b2091-1afd-4cfb-ae48-e0c151800715"
                }
            ]
        },
        {
            "id": "e247478f-65bb-4b30-afcf-13c9b9519816",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6977e224-fe40-4310-b50d-26fd96a524c1",
            "compositeImage": {
                "id": "3509e5f7-765d-4896-a4f5-326127a27176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e247478f-65bb-4b30-afcf-13c9b9519816",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5edc96d-7184-425c-985e-7d161b411cf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e247478f-65bb-4b30-afcf-13c9b9519816",
                    "LayerId": "045b2091-1afd-4cfb-ae48-e0c151800715"
                }
            ]
        },
        {
            "id": "094131ef-0385-4ad5-aa39-1d8e2bead17f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6977e224-fe40-4310-b50d-26fd96a524c1",
            "compositeImage": {
                "id": "aab09ccc-7f02-4649-954e-8f8e8c829b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "094131ef-0385-4ad5-aa39-1d8e2bead17f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f303f8-d098-483b-b7a9-23576798eaa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "094131ef-0385-4ad5-aa39-1d8e2bead17f",
                    "LayerId": "045b2091-1afd-4cfb-ae48-e0c151800715"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "045b2091-1afd-4cfb-ae48-e0c151800715",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6977e224-fe40-4310-b50d-26fd96a524c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}