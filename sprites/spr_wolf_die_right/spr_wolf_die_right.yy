{
    "id": "3106f03d-c4a9-442c-b55c-7234ebbe71e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_die_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 7,
    "bbox_right": 40,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24c8a0af-5005-4138-a050-ce851f23c4e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3106f03d-c4a9-442c-b55c-7234ebbe71e2",
            "compositeImage": {
                "id": "e66a52e0-9328-4b73-9aac-66e353426602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24c8a0af-5005-4138-a050-ce851f23c4e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "067deed6-e246-4cff-a7c0-222a99920af9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24c8a0af-5005-4138-a050-ce851f23c4e1",
                    "LayerId": "d40a5fab-02e4-47df-bb73-1c4dbd187772"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "d40a5fab-02e4-47df-bb73-1c4dbd187772",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3106f03d-c4a9-442c-b55c-7234ebbe71e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}