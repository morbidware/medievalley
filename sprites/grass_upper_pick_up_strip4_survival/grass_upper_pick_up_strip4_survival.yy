{
    "id": "035e5a5f-e05f-4928-817a-09bbd2548000",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_pick_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a6fb7f2-672f-4610-811f-bc85d87f2e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "compositeImage": {
                "id": "d7d0579e-2654-4abb-8365-c72b5c956ca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a6fb7f2-672f-4610-811f-bc85d87f2e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7c9aa70-342a-4be9-b997-d7fe37d94fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a6fb7f2-672f-4610-811f-bc85d87f2e28",
                    "LayerId": "b401cb8d-3a27-4f7f-97ef-0061e9b70652"
                }
            ]
        },
        {
            "id": "1772085a-a8d8-4e0c-a46c-38fccbe603d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "compositeImage": {
                "id": "ff9bcdd8-4333-4c4c-aadb-c19c7592f1f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1772085a-a8d8-4e0c-a46c-38fccbe603d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abbb7b90-d9ab-4cf1-a68d-ff444039558a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1772085a-a8d8-4e0c-a46c-38fccbe603d9",
                    "LayerId": "b401cb8d-3a27-4f7f-97ef-0061e9b70652"
                }
            ]
        },
        {
            "id": "98ffa745-71d8-4346-90e8-9d371e7119cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "compositeImage": {
                "id": "f75ab09c-6257-499a-99fe-abe0dd11fe09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ffa745-71d8-4346-90e8-9d371e7119cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34f4099-801a-4857-942b-5190a444e974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ffa745-71d8-4346-90e8-9d371e7119cf",
                    "LayerId": "b401cb8d-3a27-4f7f-97ef-0061e9b70652"
                }
            ]
        },
        {
            "id": "1152705c-9a27-471b-aaf6-af36858ee39c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "compositeImage": {
                "id": "dc4b8cb9-244d-4e5b-bae6-728da400257b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1152705c-9a27-471b-aaf6-af36858ee39c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f575a268-953d-4ac9-ab26-013f149fca32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1152705c-9a27-471b-aaf6-af36858ee39c",
                    "LayerId": "b401cb8d-3a27-4f7f-97ef-0061e9b70652"
                }
            ]
        },
        {
            "id": "889b8af6-6e23-47b2-a7c4-ddd7237133d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "compositeImage": {
                "id": "074faca1-5701-4432-8353-5589dd7c60d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889b8af6-6e23-47b2-a7c4-ddd7237133d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d269c78-ce72-480e-b1df-3ee4f570cdd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889b8af6-6e23-47b2-a7c4-ddd7237133d3",
                    "LayerId": "b401cb8d-3a27-4f7f-97ef-0061e9b70652"
                }
            ]
        },
        {
            "id": "5ff2e640-b054-4e01-b568-f0f52cfef539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "compositeImage": {
                "id": "a5c662dd-2712-427e-832c-e8f969cf140f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ff2e640-b054-4e01-b568-f0f52cfef539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6888b2f1-47e9-402c-9448-710cd30043cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ff2e640-b054-4e01-b568-f0f52cfef539",
                    "LayerId": "b401cb8d-3a27-4f7f-97ef-0061e9b70652"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b401cb8d-3a27-4f7f-97ef-0061e9b70652",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "035e5a5f-e05f-4928-817a-09bbd2548000",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}