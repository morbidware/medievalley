{
    "id": "da42d98f-f259-4222-ae72-4a5251a86e55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_idle_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9de93fe4-5ee6-4e88-a99c-76ddb6f38a21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da42d98f-f259-4222-ae72-4a5251a86e55",
            "compositeImage": {
                "id": "7aa39ad3-0a6f-4e6d-929f-b301bc4be098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9de93fe4-5ee6-4e88-a99c-76ddb6f38a21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2b01fcd-9672-459e-b2d4-74032e416fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9de93fe4-5ee6-4e88-a99c-76ddb6f38a21",
                    "LayerId": "392912bb-00bc-4247-8ab8-36f35bb2544a"
                }
            ]
        },
        {
            "id": "5738de49-9fa5-4f27-9624-c3f4338bffb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da42d98f-f259-4222-ae72-4a5251a86e55",
            "compositeImage": {
                "id": "00d1a55a-976b-4fd4-9f73-3ce37fc8098d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5738de49-9fa5-4f27-9624-c3f4338bffb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec2e4f1-d28b-45c1-abe7-8d592f706f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5738de49-9fa5-4f27-9624-c3f4338bffb6",
                    "LayerId": "392912bb-00bc-4247-8ab8-36f35bb2544a"
                }
            ]
        },
        {
            "id": "b7f2c73f-3652-4c77-b68a-4b2ccfcda478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da42d98f-f259-4222-ae72-4a5251a86e55",
            "compositeImage": {
                "id": "0cc66352-2d2e-4edf-8282-290e5f4d4e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f2c73f-3652-4c77-b68a-4b2ccfcda478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ba98a9-139f-460f-a1bf-e6e41909733f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f2c73f-3652-4c77-b68a-4b2ccfcda478",
                    "LayerId": "392912bb-00bc-4247-8ab8-36f35bb2544a"
                }
            ]
        },
        {
            "id": "bdcf6aeb-b0c1-407e-afe9-e6a15d786fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da42d98f-f259-4222-ae72-4a5251a86e55",
            "compositeImage": {
                "id": "fad22b9b-6036-4de6-96e6-3879a442d8ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdcf6aeb-b0c1-407e-afe9-e6a15d786fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60aa98da-2686-45e9-856e-4f2f436d2ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdcf6aeb-b0c1-407e-afe9-e6a15d786fbf",
                    "LayerId": "392912bb-00bc-4247-8ab8-36f35bb2544a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "392912bb-00bc-4247-8ab8-36f35bb2544a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da42d98f-f259-4222-ae72-4a5251a86e55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}