{
    "id": "68333f48-eb9c-47cb-8739-940077a6c845",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_home",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81f13ba4-6a21-4ee4-b014-6303e2aa202c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68333f48-eb9c-47cb-8739-940077a6c845",
            "compositeImage": {
                "id": "91352b3c-4f0d-4409-ad4e-ff4f0d0afc71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f13ba4-6a21-4ee4-b014-6303e2aa202c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c8493c8-fc73-4e13-8af7-707c9f271da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f13ba4-6a21-4ee4-b014-6303e2aa202c",
                    "LayerId": "0d60827e-8df1-4ae9-8284-03f2676fcd40"
                }
            ]
        },
        {
            "id": "77d2c083-3751-454a-9cb4-0094a639999e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68333f48-eb9c-47cb-8739-940077a6c845",
            "compositeImage": {
                "id": "6fda2451-b601-491c-9d25-53e8e01cd261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d2c083-3751-454a-9cb4-0094a639999e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e59f592-763c-449a-bc6d-08afe7c7d788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d2c083-3751-454a-9cb4-0094a639999e",
                    "LayerId": "0d60827e-8df1-4ae9-8284-03f2676fcd40"
                }
            ]
        },
        {
            "id": "28d9ef91-212e-415c-8f43-d64dabebccfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68333f48-eb9c-47cb-8739-940077a6c845",
            "compositeImage": {
                "id": "ffe3dab5-bc00-4c79-b633-1124507b66b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d9ef91-212e-415c-8f43-d64dabebccfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0478ac5e-29f4-4bb0-bf0c-e9a19d4304ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d9ef91-212e-415c-8f43-d64dabebccfa",
                    "LayerId": "0d60827e-8df1-4ae9-8284-03f2676fcd40"
                }
            ]
        },
        {
            "id": "c8ee8c56-f7c1-444d-be7a-99083c71d4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68333f48-eb9c-47cb-8739-940077a6c845",
            "compositeImage": {
                "id": "8829de40-d08e-4d2d-a1b0-06d8567d6163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ee8c56-f7c1-444d-be7a-99083c71d4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bab2e98-3077-4eb4-911d-c4d35e77aa59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ee8c56-f7c1-444d-be7a-99083c71d4a3",
                    "LayerId": "0d60827e-8df1-4ae9-8284-03f2676fcd40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "0d60827e-8df1-4ae9-8284-03f2676fcd40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68333f48-eb9c-47cb-8739-940077a6c845",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}