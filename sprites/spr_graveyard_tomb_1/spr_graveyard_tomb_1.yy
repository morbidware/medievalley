{
    "id": "c9bc9c17-5125-4a80-93fe-8b2c4b68296c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90999642-ca35-4b40-b097-0ef16a1f9d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9bc9c17-5125-4a80-93fe-8b2c4b68296c",
            "compositeImage": {
                "id": "e478e898-1bbc-433b-8657-d58429b8faed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90999642-ca35-4b40-b097-0ef16a1f9d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17db204d-75fa-497d-8a51-103cd8b68bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90999642-ca35-4b40-b097-0ef16a1f9d35",
                    "LayerId": "2ea455c7-8e91-4fdb-bbfd-33c77237dc24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2ea455c7-8e91-4fdb-bbfd-33c77237dc24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9bc9c17-5125-4a80-93fe-8b2c4b68296c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 28
}