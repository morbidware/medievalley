{
    "id": "65a0dc18-6f99-492b-8722-47bd26f947de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e63c175-382b-4957-8f73-de606899a72a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a0dc18-6f99-492b-8722-47bd26f947de",
            "compositeImage": {
                "id": "20d40351-8cf2-4adc-b1e5-e35943a2e737",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e63c175-382b-4957-8f73-de606899a72a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd949edd-44ee-49d2-a4e2-56e92fd74322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e63c175-382b-4957-8f73-de606899a72a",
                    "LayerId": "af86c172-f923-441b-b202-b4a71eccd15a"
                }
            ]
        },
        {
            "id": "17770c22-9ef0-4a4c-87a0-e1cf1779a0bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a0dc18-6f99-492b-8722-47bd26f947de",
            "compositeImage": {
                "id": "0378ee1d-6ab7-4e70-9b4c-eb4e3c51bc6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17770c22-9ef0-4a4c-87a0-e1cf1779a0bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a6390e4-7415-45ec-b495-513dedabf9b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17770c22-9ef0-4a4c-87a0-e1cf1779a0bc",
                    "LayerId": "af86c172-f923-441b-b202-b4a71eccd15a"
                }
            ]
        },
        {
            "id": "ce303dc8-2e47-418a-af38-4dd252eb2cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a0dc18-6f99-492b-8722-47bd26f947de",
            "compositeImage": {
                "id": "c7beb351-b333-4b79-a9b3-86757dc39d49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce303dc8-2e47-418a-af38-4dd252eb2cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2463bf49-2db1-407d-a520-ece61ae23241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce303dc8-2e47-418a-af38-4dd252eb2cd0",
                    "LayerId": "af86c172-f923-441b-b202-b4a71eccd15a"
                }
            ]
        },
        {
            "id": "81fe46e8-b9f2-4ac8-9838-11335053dee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a0dc18-6f99-492b-8722-47bd26f947de",
            "compositeImage": {
                "id": "9f6dafd4-4114-4bf2-9edd-1507919cd0d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81fe46e8-b9f2-4ac8-9838-11335053dee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa1342d-7212-4b2f-8f1d-7902fde00f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81fe46e8-b9f2-4ac8-9838-11335053dee3",
                    "LayerId": "af86c172-f923-441b-b202-b4a71eccd15a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af86c172-f923-441b-b202-b4a71eccd15a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65a0dc18-6f99-492b-8722-47bd26f947de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}