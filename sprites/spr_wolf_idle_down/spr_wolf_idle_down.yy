{
    "id": "24cc4aa7-db91-48bf-9054-9b7a654e1420",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 17,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dacd71fb-7cf5-4f79-bd2f-044898c16591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24cc4aa7-db91-48bf-9054-9b7a654e1420",
            "compositeImage": {
                "id": "be36c14d-db17-491b-bdae-f347418fac6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dacd71fb-7cf5-4f79-bd2f-044898c16591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a3908e-0575-4b92-8278-a51f1018d844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dacd71fb-7cf5-4f79-bd2f-044898c16591",
                    "LayerId": "6784bf3b-d3ac-441e-818f-61780e3002c9"
                }
            ]
        },
        {
            "id": "ee2128ec-7024-4475-bef9-564b41cd617a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24cc4aa7-db91-48bf-9054-9b7a654e1420",
            "compositeImage": {
                "id": "ce52148d-3ae2-4d5b-ac66-de73bbacdb61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2128ec-7024-4475-bef9-564b41cd617a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ade9a9a-bae9-4033-8408-34316d6e0664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2128ec-7024-4475-bef9-564b41cd617a",
                    "LayerId": "6784bf3b-d3ac-441e-818f-61780e3002c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "6784bf3b-d3ac-441e-818f-61780e3002c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24cc4aa7-db91-48bf-9054-9b7a654e1420",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}