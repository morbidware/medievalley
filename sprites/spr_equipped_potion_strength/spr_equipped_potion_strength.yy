{
    "id": "4b81b865-9fa1-4d5d-a325-2bc57a27081d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potion_strength",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9db1547e-5ad4-4556-8059-9deca7bbbdb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b81b865-9fa1-4d5d-a325-2bc57a27081d",
            "compositeImage": {
                "id": "3f3ac1ee-1e6c-4b37-9445-693a1f605346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db1547e-5ad4-4556-8059-9deca7bbbdb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340ff69c-d4a6-42b2-b644-6e5f34f576d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db1547e-5ad4-4556-8059-9deca7bbbdb9",
                    "LayerId": "ed6a8131-1a7d-4580-8978-88f0152c1e9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ed6a8131-1a7d-4580-8978-88f0152c1e9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b81b865-9fa1-4d5d-a325-2bc57a27081d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}