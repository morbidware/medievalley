{
    "id": "f87a447c-0d89-4add-96eb-5505f49b4e10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 196,
    "bbox_left": 0,
    "bbox_right": 179,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4be7e1e1-2cfd-4362-bbbe-eb576b6b6a08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f87a447c-0d89-4add-96eb-5505f49b4e10",
            "compositeImage": {
                "id": "00b7c5ac-8e8e-4984-a93d-e7a255af105c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4be7e1e1-2cfd-4362-bbbe-eb576b6b6a08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29089e21-1aae-44b4-8057-11c1726cb969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4be7e1e1-2cfd-4362-bbbe-eb576b6b6a08",
                    "LayerId": "f739c759-f26a-433b-b084-1f99c5c276c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 197,
    "layers": [
        {
            "id": "f739c759-f26a-433b-b084-1f99c5c276c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f87a447c-0d89-4add-96eb-5505f49b4e10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 90,
    "yorig": 98
}