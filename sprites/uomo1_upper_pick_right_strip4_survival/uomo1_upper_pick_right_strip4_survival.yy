{
    "id": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_pick_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ab1149b-ce3e-484e-a91c-eb6c330bc99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "compositeImage": {
                "id": "e118fed2-4830-461d-93f1-3f2c77941c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ab1149b-ce3e-484e-a91c-eb6c330bc99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33414521-fc7c-468d-a4f5-63586ed9669d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ab1149b-ce3e-484e-a91c-eb6c330bc99a",
                    "LayerId": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51"
                }
            ]
        },
        {
            "id": "84d24435-1e14-43f9-9e9f-68ffe18bdf30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "compositeImage": {
                "id": "5b7fdc96-e356-4a48-87b0-315bb879068e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84d24435-1e14-43f9-9e9f-68ffe18bdf30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268c35fc-c951-4d34-a029-2d5023660bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84d24435-1e14-43f9-9e9f-68ffe18bdf30",
                    "LayerId": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51"
                }
            ]
        },
        {
            "id": "9764ab5d-21a8-4d7c-8fd4-2316406860f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "compositeImage": {
                "id": "58d0d5a2-9a09-4e03-a134-791cc3ab7c2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9764ab5d-21a8-4d7c-8fd4-2316406860f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da49fc1-c978-405d-8d76-e082616c290e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9764ab5d-21a8-4d7c-8fd4-2316406860f9",
                    "LayerId": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51"
                }
            ]
        },
        {
            "id": "6a8dde16-090b-4113-946a-0e4adcc22317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "compositeImage": {
                "id": "57fd0b92-600e-4e69-a497-da91ed304317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8dde16-090b-4113-946a-0e4adcc22317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7aa04c3-6de8-421a-b5f4-da0879ae285a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8dde16-090b-4113-946a-0e4adcc22317",
                    "LayerId": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51"
                }
            ]
        },
        {
            "id": "8e81c890-5491-4b97-a324-e75fce32abd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "compositeImage": {
                "id": "67e103f2-337b-4c2a-8892-7c7ff0504526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e81c890-5491-4b97-a324-e75fce32abd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35723632-a731-424a-88cb-6769babdcd21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e81c890-5491-4b97-a324-e75fce32abd3",
                    "LayerId": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51"
                }
            ]
        },
        {
            "id": "4e48da88-3ecf-4420-a2c5-57f83c597497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "compositeImage": {
                "id": "90169325-712a-44ca-98ac-c658401798b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e48da88-3ecf-4420-a2c5-57f83c597497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caef4514-1536-416b-961b-94c7c463b032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e48da88-3ecf-4420-a2c5-57f83c597497",
                    "LayerId": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ea2ff1f1-3c15-4beb-b62c-56ac8be5df51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b217e55d-70ab-4c7a-aa46-6370b5ab7d8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}