{
    "id": "da774c90-880d-4ad7-9254-a5eec14606a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_idle_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "427a8e9a-b342-4bbb-beff-bdf54c757ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da774c90-880d-4ad7-9254-a5eec14606a3",
            "compositeImage": {
                "id": "6127c116-af83-4003-a675-bf3903fa0008",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "427a8e9a-b342-4bbb-beff-bdf54c757ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3773393a-b040-4544-befe-436c8f6b23ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "427a8e9a-b342-4bbb-beff-bdf54c757ea6",
                    "LayerId": "e31d853c-c7ff-4da2-b6f8-b32774373b76"
                }
            ]
        },
        {
            "id": "15a70f76-1510-4a5e-acd7-48a31dd6292c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da774c90-880d-4ad7-9254-a5eec14606a3",
            "compositeImage": {
                "id": "02777f6d-5ab4-4a70-b124-11d6ff2a7b21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a70f76-1510-4a5e-acd7-48a31dd6292c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c7f335-799f-4fad-9a56-2183bc9c05a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a70f76-1510-4a5e-acd7-48a31dd6292c",
                    "LayerId": "e31d853c-c7ff-4da2-b6f8-b32774373b76"
                }
            ]
        },
        {
            "id": "7eeb85a2-3ba1-4437-a092-f38d603d9ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da774c90-880d-4ad7-9254-a5eec14606a3",
            "compositeImage": {
                "id": "53075626-4bd8-407e-96cb-4a8bb5f377c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7eeb85a2-3ba1-4437-a092-f38d603d9ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1643b2f6-11d2-404f-8d83-f6a5b24ec150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7eeb85a2-3ba1-4437-a092-f38d603d9ff1",
                    "LayerId": "e31d853c-c7ff-4da2-b6f8-b32774373b76"
                }
            ]
        },
        {
            "id": "af740e99-6d3c-42a6-8d15-1ffd95399a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da774c90-880d-4ad7-9254-a5eec14606a3",
            "compositeImage": {
                "id": "d60a62ba-af49-45ea-8d8b-80ee715b22c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af740e99-6d3c-42a6-8d15-1ffd95399a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5afd9371-23e3-46a2-81fd-0986a1e7d10b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af740e99-6d3c-42a6-8d15-1ffd95399a99",
                    "LayerId": "e31d853c-c7ff-4da2-b6f8-b32774373b76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e31d853c-c7ff-4da2-b6f8-b32774373b76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da774c90-880d-4ad7-9254-a5eec14606a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}