{
    "id": "5ffd5024-cd3e-4231-83e3-55ba068bf927",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_mint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff6625f1-2d15-4ea2-b738-c1be8c515fad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ffd5024-cd3e-4231-83e3-55ba068bf927",
            "compositeImage": {
                "id": "14d4954d-54e4-486c-9763-c772edc442de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6625f1-2d15-4ea2-b738-c1be8c515fad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3260d7bc-e94f-41c8-ad78-c0aae0edca7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6625f1-2d15-4ea2-b738-c1be8c515fad",
                    "LayerId": "771c2d1e-6249-484c-9716-3c8a62cdabff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "771c2d1e-6249-484c-9716-3c8a62cdabff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ffd5024-cd3e-4231-83e3-55ba068bf927",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}