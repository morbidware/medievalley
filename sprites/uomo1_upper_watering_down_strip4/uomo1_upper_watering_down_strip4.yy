{
    "id": "4471af52-140c-416b-9aa4-e4afd2041bf0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ecf51d9-c32b-4bc6-af45-25ab79ab5e96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4471af52-140c-416b-9aa4-e4afd2041bf0",
            "compositeImage": {
                "id": "6fee89b6-12d8-4936-9d97-7b050e91ddd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ecf51d9-c32b-4bc6-af45-25ab79ab5e96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "749fbb4d-e66b-4ce9-be51-bf7ca45b0437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ecf51d9-c32b-4bc6-af45-25ab79ab5e96",
                    "LayerId": "1f591b31-bdaf-470f-93a9-03dc91beb834"
                }
            ]
        },
        {
            "id": "790761af-1c71-4c39-be06-7bb6162ad57f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4471af52-140c-416b-9aa4-e4afd2041bf0",
            "compositeImage": {
                "id": "2597e505-69ac-4923-80c3-4fffe4b7ec7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "790761af-1c71-4c39-be06-7bb6162ad57f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccac84db-678b-4868-be40-f5c7fee8ff2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "790761af-1c71-4c39-be06-7bb6162ad57f",
                    "LayerId": "1f591b31-bdaf-470f-93a9-03dc91beb834"
                }
            ]
        },
        {
            "id": "7da5797b-2071-4285-b43c-ea27b4949840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4471af52-140c-416b-9aa4-e4afd2041bf0",
            "compositeImage": {
                "id": "e0179e53-983c-4f16-8cd9-d2e9558e86e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da5797b-2071-4285-b43c-ea27b4949840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1979a009-9f30-4d5e-a3f8-bd776315b468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da5797b-2071-4285-b43c-ea27b4949840",
                    "LayerId": "1f591b31-bdaf-470f-93a9-03dc91beb834"
                }
            ]
        },
        {
            "id": "a0561934-bc1a-44a2-b525-5107db5847ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4471af52-140c-416b-9aa4-e4afd2041bf0",
            "compositeImage": {
                "id": "c3079aa2-1526-4222-8534-bde2cc127d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0561934-bc1a-44a2-b525-5107db5847ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62529835-97dd-4e94-a0bd-d24ed493d8eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0561934-bc1a-44a2-b525-5107db5847ab",
                    "LayerId": "1f591b31-bdaf-470f-93a9-03dc91beb834"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1f591b31-bdaf-470f-93a9-03dc91beb834",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4471af52-140c-416b-9aa4-e4afd2041bf0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}