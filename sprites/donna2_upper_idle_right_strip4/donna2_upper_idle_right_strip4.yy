{
    "id": "4de1c6f8-8b1b-49c8-9712-637d46de135b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dee22abb-c802-47be-8002-e6ffc1be81f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4de1c6f8-8b1b-49c8-9712-637d46de135b",
            "compositeImage": {
                "id": "c27ef3c5-f51d-4e05-a4f2-8436affd3842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee22abb-c802-47be-8002-e6ffc1be81f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b36a0d-1c48-4b51-8d7f-e7d058778248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee22abb-c802-47be-8002-e6ffc1be81f1",
                    "LayerId": "fe20ec6e-4bf0-4e5f-a6ee-8b6d29814718"
                }
            ]
        },
        {
            "id": "cddaaf4e-d6de-4ed9-a286-c61bfde4c16e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4de1c6f8-8b1b-49c8-9712-637d46de135b",
            "compositeImage": {
                "id": "1204626e-7816-4328-91a3-078fba78cf8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cddaaf4e-d6de-4ed9-a286-c61bfde4c16e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faba2ed9-c86f-43fc-9fe1-27c96cba7977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cddaaf4e-d6de-4ed9-a286-c61bfde4c16e",
                    "LayerId": "fe20ec6e-4bf0-4e5f-a6ee-8b6d29814718"
                }
            ]
        },
        {
            "id": "63b731e8-742c-47d9-ba6f-9f54a289f555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4de1c6f8-8b1b-49c8-9712-637d46de135b",
            "compositeImage": {
                "id": "79dd5927-8390-474a-bed4-c1ca5b482674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b731e8-742c-47d9-ba6f-9f54a289f555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d5d10d3-19e3-4148-bb4f-9d362ae8813b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b731e8-742c-47d9-ba6f-9f54a289f555",
                    "LayerId": "fe20ec6e-4bf0-4e5f-a6ee-8b6d29814718"
                }
            ]
        },
        {
            "id": "c33eb4ca-296c-477b-a9d1-5f89c17b3fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4de1c6f8-8b1b-49c8-9712-637d46de135b",
            "compositeImage": {
                "id": "08dcd2c7-ed80-4902-93de-fc4a3920e63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33eb4ca-296c-477b-a9d1-5f89c17b3fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2bb8dc-5e60-4005-b8ca-333fa67d2e76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33eb4ca-296c-477b-a9d1-5f89c17b3fed",
                    "LayerId": "fe20ec6e-4bf0-4e5f-a6ee-8b6d29814718"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fe20ec6e-4bf0-4e5f-a6ee-8b6d29814718",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4de1c6f8-8b1b-49c8-9712-637d46de135b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}