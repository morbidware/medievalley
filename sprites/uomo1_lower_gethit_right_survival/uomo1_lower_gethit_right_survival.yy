{
    "id": "6d8c7cfe-0a1b-4f8e-a941-0d4d542e02eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_gethit_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 4,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e467cbf-9ff8-4ef7-b707-a31fad3331e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d8c7cfe-0a1b-4f8e-a941-0d4d542e02eb",
            "compositeImage": {
                "id": "c2fe4ceb-2a71-4865-bfa8-149bb8bd38cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e467cbf-9ff8-4ef7-b707-a31fad3331e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89da9ec0-d739-41b2-92d1-bd41bf48181d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e467cbf-9ff8-4ef7-b707-a31fad3331e8",
                    "LayerId": "3c854c9a-0a7f-43e7-830d-fe7419bb3d8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3c854c9a-0a7f-43e7-830d-fe7419bb3d8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d8c7cfe-0a1b-4f8e-a941-0d4d542e02eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}