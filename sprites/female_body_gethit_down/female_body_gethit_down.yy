{
    "id": "7d192d8f-526a-4fa6-b1d4-b5f9eebe2238",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5de1d7db-66cf-4459-aa04-6aa3d4e53cb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d192d8f-526a-4fa6-b1d4-b5f9eebe2238",
            "compositeImage": {
                "id": "95b068db-d3b4-4102-b899-40dc363220b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de1d7db-66cf-4459-aa04-6aa3d4e53cb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949c6c67-0936-4362-be53-61735e15dbe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de1d7db-66cf-4459-aa04-6aa3d4e53cb2",
                    "LayerId": "cd295f9e-2f12-47ee-a571-1eb39763f6d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cd295f9e-2f12-47ee-a571-1eb39763f6d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d192d8f-526a-4fa6-b1d4-b5f9eebe2238",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}