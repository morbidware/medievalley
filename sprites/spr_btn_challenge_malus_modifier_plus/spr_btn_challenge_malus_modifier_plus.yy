{
    "id": "4dc900e3-9370-4a3d-b5f5-1a89252634c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_challenge_malus_modifier_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "866dd0bf-7e84-44bc-abb3-afb2fa643087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dc900e3-9370-4a3d-b5f5-1a89252634c8",
            "compositeImage": {
                "id": "0d30e7eb-29af-4f34-9c41-df68e0766943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "866dd0bf-7e84-44bc-abb3-afb2fa643087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "499c3f58-74f1-4abc-b383-694cab9773cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866dd0bf-7e84-44bc-abb3-afb2fa643087",
                    "LayerId": "c20b85fa-b447-4b27-b226-bb09df245a42"
                },
                {
                    "id": "4d74fc12-2f8a-403e-8157-8f0caa5e057c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "866dd0bf-7e84-44bc-abb3-afb2fa643087",
                    "LayerId": "4d6b4602-c6bc-4c75-9439-e8f476836b18"
                }
            ]
        },
        {
            "id": "49169a02-6e7c-4e56-a9ae-89276668bacb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dc900e3-9370-4a3d-b5f5-1a89252634c8",
            "compositeImage": {
                "id": "2fb42950-4e5c-43a4-aa8a-2b96c51c9061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49169a02-6e7c-4e56-a9ae-89276668bacb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a55196b-74fe-4771-a4d3-53d0b4cd10d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49169a02-6e7c-4e56-a9ae-89276668bacb",
                    "LayerId": "c20b85fa-b447-4b27-b226-bb09df245a42"
                },
                {
                    "id": "e72d6279-4830-4ac1-9a8f-7a46ceffe653",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49169a02-6e7c-4e56-a9ae-89276668bacb",
                    "LayerId": "4d6b4602-c6bc-4c75-9439-e8f476836b18"
                }
            ]
        },
        {
            "id": "df2dd16d-d091-4779-bebe-829225dea7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4dc900e3-9370-4a3d-b5f5-1a89252634c8",
            "compositeImage": {
                "id": "f834419f-ad72-4cef-95e0-42126f7a5f12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df2dd16d-d091-4779-bebe-829225dea7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea3fbec-df5f-4d86-94e4-9fd1935fee78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df2dd16d-d091-4779-bebe-829225dea7d9",
                    "LayerId": "c20b85fa-b447-4b27-b226-bb09df245a42"
                },
                {
                    "id": "770051bd-8fab-407f-929b-fa45b537cb3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df2dd16d-d091-4779-bebe-829225dea7d9",
                    "LayerId": "4d6b4602-c6bc-4c75-9439-e8f476836b18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "4d6b4602-c6bc-4c75-9439-e8f476836b18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dc900e3-9370-4a3d-b5f5-1a89252634c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c20b85fa-b447-4b27-b226-bb09df245a42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4dc900e3-9370-4a3d-b5f5-1a89252634c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}