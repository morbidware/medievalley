{
    "id": "edff63e3-71cc-4e87-842c-2e9700e5085d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_suffer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 17,
    "bbox_right": 30,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "613bd38e-a6e8-4a0c-9880-53c3cd618e72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edff63e3-71cc-4e87-842c-2e9700e5085d",
            "compositeImage": {
                "id": "2e77ce63-25c3-4efb-b3c6-a2344dd59dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "613bd38e-a6e8-4a0c-9880-53c3cd618e72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4cf5c9-f214-4eb3-ae7b-00e5fa766776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "613bd38e-a6e8-4a0c-9880-53c3cd618e72",
                    "LayerId": "99dc1b08-5b5d-46dd-b11c-5688fb471646"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "99dc1b08-5b5d-46dd-b11c-5688fb471646",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edff63e3-71cc-4e87-842c-2e9700e5085d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}