{
    "id": "da4f8869-17bd-4b07-901e-65bd0d6929e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_die_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bce1abb-724f-45eb-8e85-550389c43dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da4f8869-17bd-4b07-901e-65bd0d6929e7",
            "compositeImage": {
                "id": "5f48da11-c81b-41b5-9205-269d7370564b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bce1abb-724f-45eb-8e85-550389c43dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ff21a2-e18c-4f6e-b855-9d8d051ada62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bce1abb-724f-45eb-8e85-550389c43dbd",
                    "LayerId": "dfc7f0b0-d44d-4a9b-a854-5ecd990358d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "dfc7f0b0-d44d-4a9b-a854-5ecd990358d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da4f8869-17bd-4b07-901e-65bd0d6929e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 44
}