{
    "id": "dab4590a-75c7-49f4-aba5-14140dc503f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_gethit_down_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2a65fd1-091e-46f1-922a-03c1758fbc5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dab4590a-75c7-49f4-aba5-14140dc503f2",
            "compositeImage": {
                "id": "179f976f-31aa-45a8-8d8c-5cb8e1831fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2a65fd1-091e-46f1-922a-03c1758fbc5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "387025c0-9182-4978-ac00-90cebd4e4d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2a65fd1-091e-46f1-922a-03c1758fbc5c",
                    "LayerId": "4c29e98c-c016-4b2d-975b-803fba76a6e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4c29e98c-c016-4b2d-975b-803fba76a6e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dab4590a-75c7-49f4-aba5-14140dc503f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}