{
    "id": "ca965a14-a5d3-47f3-a5e0-822e1c0abe92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13c12a3a-e2bd-481d-8b2a-982b6cabf482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca965a14-a5d3-47f3-a5e0-822e1c0abe92",
            "compositeImage": {
                "id": "31eaaed8-cd79-4418-8c9b-a0223d5f0ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c12a3a-e2bd-481d-8b2a-982b6cabf482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "579029a1-75e4-428a-a148-de6c4cae29fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c12a3a-e2bd-481d-8b2a-982b6cabf482",
                    "LayerId": "50e66769-b30c-4e0d-b42a-01c537333225"
                }
            ]
        },
        {
            "id": "a1255001-ce85-49c5-b68f-3c86bf8f3c55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca965a14-a5d3-47f3-a5e0-822e1c0abe92",
            "compositeImage": {
                "id": "eabebeba-a632-4bfd-957f-8dbef699e010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1255001-ce85-49c5-b68f-3c86bf8f3c55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8432de68-63f2-426b-aaa5-0def2a873074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1255001-ce85-49c5-b68f-3c86bf8f3c55",
                    "LayerId": "50e66769-b30c-4e0d-b42a-01c537333225"
                }
            ]
        },
        {
            "id": "1d197e65-6108-4ffa-b91e-7940dc0d9625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca965a14-a5d3-47f3-a5e0-822e1c0abe92",
            "compositeImage": {
                "id": "28a62ada-5a26-45cd-b5e8-5d9445f9ad32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d197e65-6108-4ffa-b91e-7940dc0d9625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16253102-4f26-4a9d-a159-b64877df13c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d197e65-6108-4ffa-b91e-7940dc0d9625",
                    "LayerId": "50e66769-b30c-4e0d-b42a-01c537333225"
                }
            ]
        },
        {
            "id": "357dc5e7-6d34-4856-9b43-2b03a0905616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca965a14-a5d3-47f3-a5e0-822e1c0abe92",
            "compositeImage": {
                "id": "45ccd0d3-1d46-43f4-9b25-f529dff40c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "357dc5e7-6d34-4856-9b43-2b03a0905616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6811732-7b36-4d4c-8202-bddb976f89ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "357dc5e7-6d34-4856-9b43-2b03a0905616",
                    "LayerId": "50e66769-b30c-4e0d-b42a-01c537333225"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "50e66769-b30c-4e0d-b42a-01c537333225",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca965a14-a5d3-47f3-a5e0-822e1c0abe92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}