{
    "id": "dc10969b-d98d-4ee3-ac24-3cdf0e68b069",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_echinacea",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29fecfba-4812-4693-9178-21780ceed3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc10969b-d98d-4ee3-ac24-3cdf0e68b069",
            "compositeImage": {
                "id": "7a63c74a-f4f8-43ee-b9e4-fa369a3380b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29fecfba-4812-4693-9178-21780ceed3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5efcc51e-bb55-45a8-9f97-4ac090605df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29fecfba-4812-4693-9178-21780ceed3ff",
                    "LayerId": "958e1c2f-c30a-4785-a55a-b993372aa5ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "958e1c2f-c30a-4785-a55a-b993372aa5ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc10969b-d98d-4ee3-ac24-3cdf0e68b069",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}