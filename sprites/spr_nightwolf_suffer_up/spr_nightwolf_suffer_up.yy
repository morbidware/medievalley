{
    "id": "16b750f6-3cbd-4fb4-9480-52529d5f6305",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_suffer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 17,
    "bbox_right": 30,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22f2e655-f5fb-4bfa-993e-beeb5e20f91c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16b750f6-3cbd-4fb4-9480-52529d5f6305",
            "compositeImage": {
                "id": "bc997d56-7c8f-4b1d-8d8e-052dcdc44135",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22f2e655-f5fb-4bfa-993e-beeb5e20f91c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a60acbd2-8319-4cb3-a8c9-4fd3c86f0829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22f2e655-f5fb-4bfa-993e-beeb5e20f91c",
                    "LayerId": "49250909-2b0b-4513-8869-a4e6912ddc71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "49250909-2b0b-4513-8869-a4e6912ddc71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16b750f6-3cbd-4fb4-9480-52529d5f6305",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}