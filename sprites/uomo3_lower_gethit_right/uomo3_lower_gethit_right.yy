{
    "id": "23b96e70-5bdd-43e5-bffd-352a8ef4164a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf06b6d4-b8f1-4c19-b7cf-42ecf29812da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23b96e70-5bdd-43e5-bffd-352a8ef4164a",
            "compositeImage": {
                "id": "0f0fcfea-3890-4e2d-8290-8c9cc4004412",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf06b6d4-b8f1-4c19-b7cf-42ecf29812da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7755b8-b07e-47fc-9160-bc564f920da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf06b6d4-b8f1-4c19-b7cf-42ecf29812da",
                    "LayerId": "9313b855-4692-443f-b1ac-7c2febccce48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9313b855-4692-443f-b1ac-7c2febccce48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23b96e70-5bdd-43e5-bffd-352a8ef4164a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}