{
    "id": "0dad3403-00df-4d6e-a39a-e76ad86ec237",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_die_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 10,
    "bbox_right": 31,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2198e91-b53e-430a-83c3-24ff15187d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dad3403-00df-4d6e-a39a-e76ad86ec237",
            "compositeImage": {
                "id": "5b670464-4924-4296-9f22-8107694d7ae2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2198e91-b53e-430a-83c3-24ff15187d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5547b2d-7a0a-462a-90cc-255f3dcc4aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2198e91-b53e-430a-83c3-24ff15187d83",
                    "LayerId": "4e7e9048-d501-43d7-af83-476d11fb7a97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "4e7e9048-d501-43d7-af83-476d11fb7a97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dad3403-00df-4d6e-a39a-e76ad86ec237",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}