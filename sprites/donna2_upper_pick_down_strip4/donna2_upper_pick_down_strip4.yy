{
    "id": "785aafa1-a7a5-4612-b0c1-216b40fd3975",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be9cbfce-a9b2-4894-82e3-ef17864f4eda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "785aafa1-a7a5-4612-b0c1-216b40fd3975",
            "compositeImage": {
                "id": "d9e56c12-29d0-46aa-88cc-f56bcada8339",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be9cbfce-a9b2-4894-82e3-ef17864f4eda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce6bc14f-accd-4c74-927b-4ff93d4a3728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be9cbfce-a9b2-4894-82e3-ef17864f4eda",
                    "LayerId": "3b064ea5-5633-4cb6-9905-3aef2e81a314"
                }
            ]
        },
        {
            "id": "36505227-dd29-42b9-9848-d3e0bb00be78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "785aafa1-a7a5-4612-b0c1-216b40fd3975",
            "compositeImage": {
                "id": "8cf92374-7899-4fef-bb74-9fa7700d3a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36505227-dd29-42b9-9848-d3e0bb00be78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3f11216-e955-484f-8497-503882e9a8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36505227-dd29-42b9-9848-d3e0bb00be78",
                    "LayerId": "3b064ea5-5633-4cb6-9905-3aef2e81a314"
                }
            ]
        },
        {
            "id": "7b9f8242-e37d-4cf7-a05e-320cca159136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "785aafa1-a7a5-4612-b0c1-216b40fd3975",
            "compositeImage": {
                "id": "2b4e4c7b-2b00-441c-9eb4-ec07249ebb57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b9f8242-e37d-4cf7-a05e-320cca159136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e451d810-c845-4f39-b1dc-dee82a2a480b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b9f8242-e37d-4cf7-a05e-320cca159136",
                    "LayerId": "3b064ea5-5633-4cb6-9905-3aef2e81a314"
                }
            ]
        },
        {
            "id": "cce517b4-c4b9-49b2-bda9-1c2856997417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "785aafa1-a7a5-4612-b0c1-216b40fd3975",
            "compositeImage": {
                "id": "c9ef0c5e-7a97-4e44-ae6e-86c989b1d4db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cce517b4-c4b9-49b2-bda9-1c2856997417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04dd9682-4a04-4670-a6b4-17ad7bacae84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cce517b4-c4b9-49b2-bda9-1c2856997417",
                    "LayerId": "3b064ea5-5633-4cb6-9905-3aef2e81a314"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b064ea5-5633-4cb6-9905-3aef2e81a314",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "785aafa1-a7a5-4612-b0c1-216b40fd3975",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}