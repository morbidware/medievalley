{
    "id": "59bfd122-73a8-43f8-829b-25719c080075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_watering_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fffec92-d135-4a3f-b1f5-f2559bab99b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "compositeImage": {
                "id": "fb137d18-bbf8-4160-870b-8cabe4abede8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fffec92-d135-4a3f-b1f5-f2559bab99b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b47084-abfa-4d1f-8720-cc8e0ef93d86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fffec92-d135-4a3f-b1f5-f2559bab99b5",
                    "LayerId": "dee3072d-8068-4578-9db8-e04ccc69ecbf"
                }
            ]
        },
        {
            "id": "b7e8805b-4c9f-4b64-8292-3eb1ab443f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "compositeImage": {
                "id": "a746220f-f1e4-43cc-a68e-02018f1edf84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e8805b-4c9f-4b64-8292-3eb1ab443f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89a2500-d876-4a64-a9bf-79cefa2c9cc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e8805b-4c9f-4b64-8292-3eb1ab443f36",
                    "LayerId": "dee3072d-8068-4578-9db8-e04ccc69ecbf"
                }
            ]
        },
        {
            "id": "ee2539da-4ddd-4fb8-a4ff-853ae2bcf529",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "compositeImage": {
                "id": "ca46192c-75e8-401f-a22e-e66e2a8b737c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2539da-4ddd-4fb8-a4ff-853ae2bcf529",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66dea8a8-84ca-4ad6-9bea-ba32d51ec29f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2539da-4ddd-4fb8-a4ff-853ae2bcf529",
                    "LayerId": "dee3072d-8068-4578-9db8-e04ccc69ecbf"
                }
            ]
        },
        {
            "id": "d21f08fb-9543-49fe-84e4-45f3c422457f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "compositeImage": {
                "id": "f9bb1669-7563-4417-8912-2b62647b08af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d21f08fb-9543-49fe-84e4-45f3c422457f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "290ca6f6-e5ef-433d-9364-9920b814d33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d21f08fb-9543-49fe-84e4-45f3c422457f",
                    "LayerId": "dee3072d-8068-4578-9db8-e04ccc69ecbf"
                }
            ]
        },
        {
            "id": "70ab3ee5-ad01-43f9-a504-4b6eb4baaa2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "compositeImage": {
                "id": "8cd3a040-3ffc-4b37-a3d8-3642d760ebc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ab3ee5-ad01-43f9-a504-4b6eb4baaa2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e0bfde2-2976-448d-a08e-ed8823f8f329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ab3ee5-ad01-43f9-a504-4b6eb4baaa2c",
                    "LayerId": "dee3072d-8068-4578-9db8-e04ccc69ecbf"
                }
            ]
        },
        {
            "id": "7e8414f0-d691-4156-9ed3-543d93c5e2c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "compositeImage": {
                "id": "2af66147-6cf1-4f73-a59f-c62f17c129ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8414f0-d691-4156-9ed3-543d93c5e2c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "529fbe1e-02d1-4f3e-8691-adad3087ed62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8414f0-d691-4156-9ed3-543d93c5e2c8",
                    "LayerId": "dee3072d-8068-4578-9db8-e04ccc69ecbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dee3072d-8068-4578-9db8-e04ccc69ecbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59bfd122-73a8-43f8-829b-25719c080075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}