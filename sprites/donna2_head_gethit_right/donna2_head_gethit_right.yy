{
    "id": "c70ce741-20b7-425e-bcb0-cfc59599d10b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a7fd287-ad6e-4ff2-959d-e385a4a6796d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c70ce741-20b7-425e-bcb0-cfc59599d10b",
            "compositeImage": {
                "id": "70c9138d-f350-4286-b78e-25932fed875c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a7fd287-ad6e-4ff2-959d-e385a4a6796d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39fbd812-881e-408a-b4f0-4f1bec31b792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7fd287-ad6e-4ff2-959d-e385a4a6796d",
                    "LayerId": "5cc4edce-d246-4b35-b302-172de15c33fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5cc4edce-d246-4b35-b302-172de15c33fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c70ce741-20b7-425e-bcb0-cfc59599d10b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}