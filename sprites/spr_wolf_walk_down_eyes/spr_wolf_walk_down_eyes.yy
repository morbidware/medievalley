{
    "id": "7e1cdf3a-f202-45b3-9954-706ede9f4487",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_walk_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 23,
    "bbox_right": 28,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69998335-5eaa-460a-ad58-f35620919939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e1cdf3a-f202-45b3-9954-706ede9f4487",
            "compositeImage": {
                "id": "7340c288-c7ca-4b52-b4bf-856f9570664e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69998335-5eaa-460a-ad58-f35620919939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18a80259-1a0e-4b1d-890e-29f3a1e60dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69998335-5eaa-460a-ad58-f35620919939",
                    "LayerId": "917b457d-defe-4b45-8c7a-afe9fd641354"
                }
            ]
        },
        {
            "id": "d6ad785c-246b-4bac-b338-0e7ec25b6b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e1cdf3a-f202-45b3-9954-706ede9f4487",
            "compositeImage": {
                "id": "ad1b5d0f-08aa-4ebb-8d14-1893e1b4617d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ad785c-246b-4bac-b338-0e7ec25b6b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bea670de-118a-48d5-bbd6-baa18f184350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ad785c-246b-4bac-b338-0e7ec25b6b6c",
                    "LayerId": "917b457d-defe-4b45-8c7a-afe9fd641354"
                }
            ]
        },
        {
            "id": "305c99b3-48a9-4cb8-8a48-46bdc6e7117f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e1cdf3a-f202-45b3-9954-706ede9f4487",
            "compositeImage": {
                "id": "3abb7b1f-f21a-4689-806b-74cb2ba990a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305c99b3-48a9-4cb8-8a48-46bdc6e7117f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8daf1445-878c-4c64-8fb0-e8e4330e2566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305c99b3-48a9-4cb8-8a48-46bdc6e7117f",
                    "LayerId": "917b457d-defe-4b45-8c7a-afe9fd641354"
                }
            ]
        },
        {
            "id": "1adecdc4-9202-433b-87d5-880c67c9f5f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7e1cdf3a-f202-45b3-9954-706ede9f4487",
            "compositeImage": {
                "id": "12474119-43a7-42ad-b2f7-42ad2eb19875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1adecdc4-9202-433b-87d5-880c67c9f5f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46969347-7c22-46e6-b76a-23edd753d80b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1adecdc4-9202-433b-87d5-880c67c9f5f4",
                    "LayerId": "917b457d-defe-4b45-8c7a-afe9fd641354"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "917b457d-defe-4b45-8c7a-afe9fd641354",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7e1cdf3a-f202-45b3-9954-706ede9f4487",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}