{
    "id": "cb07a0b8-3408-4f93-b2af-51f244f69ead",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 31,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbf6f080-2d17-43d4-a52c-37f87af8ff59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb07a0b8-3408-4f93-b2af-51f244f69ead",
            "compositeImage": {
                "id": "8eaf3414-47ce-406d-a417-42202e4e8c1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf6f080-2d17-43d4-a52c-37f87af8ff59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbdc8a18-0356-494c-bbe0-83e4131b2bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf6f080-2d17-43d4-a52c-37f87af8ff59",
                    "LayerId": "7ca5a80a-fb03-4f81-ad81-ff1730892751"
                }
            ]
        },
        {
            "id": "ee0d8710-e1b9-48f2-b8a0-c4e032451a64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb07a0b8-3408-4f93-b2af-51f244f69ead",
            "compositeImage": {
                "id": "6627f188-46dc-45f1-a476-516ee2e573e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0d8710-e1b9-48f2-b8a0-c4e032451a64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46027e87-c51c-4f91-b585-7a29e2add43b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0d8710-e1b9-48f2-b8a0-c4e032451a64",
                    "LayerId": "7ca5a80a-fb03-4f81-ad81-ff1730892751"
                }
            ]
        },
        {
            "id": "d6253dd2-df3a-4d06-8a07-6edce02c863b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb07a0b8-3408-4f93-b2af-51f244f69ead",
            "compositeImage": {
                "id": "1eff4574-b669-487c-8ddd-a9a0568cb085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6253dd2-df3a-4d06-8a07-6edce02c863b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd937df-1f60-4847-9a3a-fae0e8118dcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6253dd2-df3a-4d06-8a07-6edce02c863b",
                    "LayerId": "7ca5a80a-fb03-4f81-ad81-ff1730892751"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "7ca5a80a-fb03-4f81-ad81-ff1730892751",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb07a0b8-3408-4f93-b2af-51f244f69ead",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}