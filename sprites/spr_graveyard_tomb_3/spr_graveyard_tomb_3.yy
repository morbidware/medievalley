{
    "id": "17e47095-5762-40e9-9808-6884e312e147",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_tomb_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8a28237-96cc-4c58-b025-0231f353a87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "17e47095-5762-40e9-9808-6884e312e147",
            "compositeImage": {
                "id": "0bb9d1af-5a23-498b-a382-4e1aad3dcdf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a28237-96cc-4c58-b025-0231f353a87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b21e4b-cc53-4207-8711-07d7a7d8ff5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a28237-96cc-4c58-b025-0231f353a87f",
                    "LayerId": "6ede3a7b-6126-4fcf-ab89-80aec7304896"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ede3a7b-6126-4fcf-ab89-80aec7304896",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "17e47095-5762-40e9-9808-6884e312e147",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 27
}