{
    "id": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_garlic_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bbabd3d-39f8-4938-a4e3-702a2af7ea6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "compositeImage": {
                "id": "464e84b6-65b0-4f25-ab08-10f3440cea4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bbabd3d-39f8-4938-a4e3-702a2af7ea6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1b6844-30f6-4852-a2b8-bbeae5bc16e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bbabd3d-39f8-4938-a4e3-702a2af7ea6c",
                    "LayerId": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8"
                }
            ]
        },
        {
            "id": "e263d017-fc0b-4168-a458-97b3ed14261f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "compositeImage": {
                "id": "682087be-cc02-4f01-8c4a-a17587cffc3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e263d017-fc0b-4168-a458-97b3ed14261f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b76a4d7-7dc9-4d06-bf96-cc68a539a5f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e263d017-fc0b-4168-a458-97b3ed14261f",
                    "LayerId": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8"
                }
            ]
        },
        {
            "id": "2e35e3d6-1cc1-446e-81ca-7a8d691a8389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "compositeImage": {
                "id": "581483ae-eab8-43ea-9377-3f618207e8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e35e3d6-1cc1-446e-81ca-7a8d691a8389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da7c8360-8bcc-4aec-9d17-9e50efe58bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e35e3d6-1cc1-446e-81ca-7a8d691a8389",
                    "LayerId": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8"
                }
            ]
        },
        {
            "id": "1e3f3e29-987c-4126-b32e-8b07a5a0d0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "compositeImage": {
                "id": "6e7af0c7-a7a4-4023-81b2-5f1778504d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e3f3e29-987c-4126-b32e-8b07a5a0d0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e5eec5b-b620-4163-8cff-dd671a5c98fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e3f3e29-987c-4126-b32e-8b07a5a0d0aa",
                    "LayerId": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8"
                }
            ]
        },
        {
            "id": "014ec4c3-0ef9-4fe4-818c-0fbfd112cec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "compositeImage": {
                "id": "76902a7e-7540-438d-bfbe-49267c6d14b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "014ec4c3-0ef9-4fe4-818c-0fbfd112cec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09dbf3e2-72dd-439d-a6db-6147a1f15f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "014ec4c3-0ef9-4fe4-818c-0fbfd112cec6",
                    "LayerId": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8"
                }
            ]
        },
        {
            "id": "414f4f59-c4ae-4ab9-9c05-faadaf54c524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "compositeImage": {
                "id": "7bd55051-b494-41a4-9360-92302dc9786e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414f4f59-c4ae-4ab9-9c05-faadaf54c524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45b18b1-1c7f-4dda-91de-4512d9c7c7a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414f4f59-c4ae-4ab9-9c05-faadaf54c524",
                    "LayerId": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e31eeae0-17b1-4bf4-8154-0c2a886f67a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2da441e-95d4-4dee-9692-cf7ae7cf088d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 26
}