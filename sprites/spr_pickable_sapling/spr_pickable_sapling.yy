{
    "id": "415377bd-445c-4ccd-b011-145dabf35585",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_sapling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be7f799b-0eb6-4fc1-83fe-343af6032883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "415377bd-445c-4ccd-b011-145dabf35585",
            "compositeImage": {
                "id": "7a80562c-32ff-425a-8fe9-adf829c5b04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be7f799b-0eb6-4fc1-83fe-343af6032883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "412cbad5-a141-4acd-9658-6ab34d474c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be7f799b-0eb6-4fc1-83fe-343af6032883",
                    "LayerId": "7171db94-e0cd-433c-bef6-d8735b7975c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7171db94-e0cd-433c-bef6-d8735b7975c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "415377bd-445c-4ccd-b011-145dabf35585",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}