{
    "id": "8a2c0f62-8ff5-4ac3-bd13-c79291d3ebfe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f4a67df-e21f-4d77-8738-e47905d2684f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c0f62-8ff5-4ac3-bd13-c79291d3ebfe",
            "compositeImage": {
                "id": "d3b27a70-4abb-48f3-ba6b-0a67d7679814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4a67df-e21f-4d77-8738-e47905d2684f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa1db9d-fe59-4cb9-b891-cd6bf661cb3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4a67df-e21f-4d77-8738-e47905d2684f",
                    "LayerId": "fba446fc-6b96-4472-86b0-c5c5be1b74c2"
                }
            ]
        },
        {
            "id": "902dc72c-0a66-42c6-a3e1-879fd7b6fa2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c0f62-8ff5-4ac3-bd13-c79291d3ebfe",
            "compositeImage": {
                "id": "23219ac5-4f10-4a36-961b-5395222f56c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "902dc72c-0a66-42c6-a3e1-879fd7b6fa2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb4ae1cd-2f25-42de-ac73-528cd5b9ff21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "902dc72c-0a66-42c6-a3e1-879fd7b6fa2c",
                    "LayerId": "fba446fc-6b96-4472-86b0-c5c5be1b74c2"
                }
            ]
        },
        {
            "id": "e62c2c53-859c-487e-866c-ba0d6f5bc5f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c0f62-8ff5-4ac3-bd13-c79291d3ebfe",
            "compositeImage": {
                "id": "e26fd123-1d49-419f-b07a-09970bc1f747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62c2c53-859c-487e-866c-ba0d6f5bc5f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c62269c0-af71-4bd1-a9e5-2e78c8154bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62c2c53-859c-487e-866c-ba0d6f5bc5f1",
                    "LayerId": "fba446fc-6b96-4472-86b0-c5c5be1b74c2"
                }
            ]
        },
        {
            "id": "30f2b71b-1c70-4704-92f8-e821835aa07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a2c0f62-8ff5-4ac3-bd13-c79291d3ebfe",
            "compositeImage": {
                "id": "8b8f4ac2-fd77-4213-a03e-f10343bc79e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30f2b71b-1c70-4704-92f8-e821835aa07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b52c1d35-fef1-4b1d-8796-42d72a247c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30f2b71b-1c70-4704-92f8-e821835aa07b",
                    "LayerId": "fba446fc-6b96-4472-86b0-c5c5be1b74c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fba446fc-6b96-4472-86b0-c5c5be1b74c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a2c0f62-8ff5-4ac3-bd13-c79291d3ebfe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}