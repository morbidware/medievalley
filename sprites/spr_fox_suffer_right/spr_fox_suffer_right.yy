{
    "id": "bb6dc4b7-d6df-4e26-ad85-724f6fd0e8f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_suffer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e55896b6-4694-4b6c-b8c1-ab15b01ae29c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb6dc4b7-d6df-4e26-ad85-724f6fd0e8f7",
            "compositeImage": {
                "id": "f7cfc64e-0638-4ed7-8adf-4a130275cad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e55896b6-4694-4b6c-b8c1-ab15b01ae29c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5373b0b0-7812-42ec-8901-a87905d9baea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e55896b6-4694-4b6c-b8c1-ab15b01ae29c",
                    "LayerId": "c0c8395d-66b5-4293-b2ba-5e7644cd54cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c0c8395d-66b5-4293-b2ba-5e7644cd54cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb6dc4b7-d6df-4e26-ad85-724f6fd0e8f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}