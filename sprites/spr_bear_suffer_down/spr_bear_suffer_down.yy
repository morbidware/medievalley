{
    "id": "18754d09-9ac0-4f10-ac19-dc5e40ce177d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_suffer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5545093-e8d0-4144-a556-33c89f6a2482",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18754d09-9ac0-4f10-ac19-dc5e40ce177d",
            "compositeImage": {
                "id": "ec24fa10-32e1-4699-9d4c-f418f902a1f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5545093-e8d0-4144-a556-33c89f6a2482",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20701eb-0c66-40bf-81c3-479c115c492c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5545093-e8d0-4144-a556-33c89f6a2482",
                    "LayerId": "b0707da3-9ee0-4699-b319-81f6c60b2d41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b0707da3-9ee0-4699-b319-81f6c60b2d41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18754d09-9ac0-4f10-ac19-dc5e40ce177d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}