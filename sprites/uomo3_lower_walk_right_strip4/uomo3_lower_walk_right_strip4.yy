{
    "id": "1899f975-bee3-43eb-a1ef-a6e36840c6b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4adb9d34-6852-49bd-be74-37c96548711c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1899f975-bee3-43eb-a1ef-a6e36840c6b5",
            "compositeImage": {
                "id": "4eaa3718-b64c-43c3-be86-ac0c12fc9d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4adb9d34-6852-49bd-be74-37c96548711c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ba5e71-4f59-4189-bd75-ef9dd0763a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4adb9d34-6852-49bd-be74-37c96548711c",
                    "LayerId": "827f0f98-f2b9-4fb8-96e9-ed44cb287ac6"
                }
            ]
        },
        {
            "id": "3c1c9003-da31-4b6d-8d7f-b30fc0bcf6ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1899f975-bee3-43eb-a1ef-a6e36840c6b5",
            "compositeImage": {
                "id": "ae1851e8-2c4b-4da3-b871-04e4b360a67d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c1c9003-da31-4b6d-8d7f-b30fc0bcf6ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95cd7c5d-f63f-4a62-9393-39923cb7663f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c1c9003-da31-4b6d-8d7f-b30fc0bcf6ab",
                    "LayerId": "827f0f98-f2b9-4fb8-96e9-ed44cb287ac6"
                }
            ]
        },
        {
            "id": "a35dcb3a-49f9-4dcf-bc4e-a6cdec6a00b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1899f975-bee3-43eb-a1ef-a6e36840c6b5",
            "compositeImage": {
                "id": "e7da50ac-2397-4fe9-a3ff-506caa74ef89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a35dcb3a-49f9-4dcf-bc4e-a6cdec6a00b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3db23a-9fc8-4680-ab22-4637ea1fe893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a35dcb3a-49f9-4dcf-bc4e-a6cdec6a00b2",
                    "LayerId": "827f0f98-f2b9-4fb8-96e9-ed44cb287ac6"
                }
            ]
        },
        {
            "id": "e9118a07-0b53-4788-bedc-8bdb9de37ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1899f975-bee3-43eb-a1ef-a6e36840c6b5",
            "compositeImage": {
                "id": "f5408702-67f2-47e3-b13c-59e66b0fd00f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9118a07-0b53-4788-bedc-8bdb9de37ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2304ef5a-7e67-4c4b-b105-df5e5b08f69f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9118a07-0b53-4788-bedc-8bdb9de37ee5",
                    "LayerId": "827f0f98-f2b9-4fb8-96e9-ed44cb287ac6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "827f0f98-f2b9-4fb8-96e9-ed44cb287ac6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1899f975-bee3-43eb-a1ef-a6e36840c6b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}