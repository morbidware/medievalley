{
    "id": "70a04711-8b64-42f7-b372-c6e9288f364c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3e5eccd-ccdd-4e85-972b-0025854f9643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70a04711-8b64-42f7-b372-c6e9288f364c",
            "compositeImage": {
                "id": "d02deb93-19de-4fb9-89e8-6369ddf8dea5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3e5eccd-ccdd-4e85-972b-0025854f9643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0237a91e-6fd4-4083-bbe5-1047a41c8cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3e5eccd-ccdd-4e85-972b-0025854f9643",
                    "LayerId": "6998f9d1-fa6a-4511-a553-bec3ae675c59"
                }
            ]
        },
        {
            "id": "a5186563-c5ab-412b-94cc-af538c64d57a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70a04711-8b64-42f7-b372-c6e9288f364c",
            "compositeImage": {
                "id": "3cd07c50-70df-4b48-9842-40bb2f38300d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5186563-c5ab-412b-94cc-af538c64d57a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838c4f9e-46ea-4b49-9e69-7dee1c3994d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5186563-c5ab-412b-94cc-af538c64d57a",
                    "LayerId": "6998f9d1-fa6a-4511-a553-bec3ae675c59"
                }
            ]
        },
        {
            "id": "64c227ab-5649-4bc0-99cb-a921889cc92b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70a04711-8b64-42f7-b372-c6e9288f364c",
            "compositeImage": {
                "id": "0828eebf-df7f-42d2-a3f0-cea4a986ac47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c227ab-5649-4bc0-99cb-a921889cc92b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8776aed4-3dea-4821-9b31-275c7f53d335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c227ab-5649-4bc0-99cb-a921889cc92b",
                    "LayerId": "6998f9d1-fa6a-4511-a553-bec3ae675c59"
                }
            ]
        },
        {
            "id": "0feec02d-6456-4769-b56d-44a7c3f7b80a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70a04711-8b64-42f7-b372-c6e9288f364c",
            "compositeImage": {
                "id": "fafb134f-2041-449c-9d75-3db8a8ec0c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0feec02d-6456-4769-b56d-44a7c3f7b80a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d3ad6c-a4e5-4bf3-b217-8d50ed877d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0feec02d-6456-4769-b56d-44a7c3f7b80a",
                    "LayerId": "6998f9d1-fa6a-4511-a553-bec3ae675c59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6998f9d1-fa6a-4511-a553-bec3ae675c59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70a04711-8b64-42f7-b372-c6e9288f364c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}