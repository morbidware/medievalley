{
    "id": "c3cea906-4fd4-4482-9770-84de0a0dd92d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a74b43c0-e193-4e88-ba4f-3ae8e9919c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cea906-4fd4-4482-9770-84de0a0dd92d",
            "compositeImage": {
                "id": "cc68c90c-e84f-4672-8d5f-a1d7ef64a3fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74b43c0-e193-4e88-ba4f-3ae8e9919c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "295aa3fb-7e63-4b0d-97d3-ac4538b17bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74b43c0-e193-4e88-ba4f-3ae8e9919c8e",
                    "LayerId": "b959f132-bcd3-4105-8db7-7aff0c584004"
                }
            ]
        },
        {
            "id": "f360c9b5-8e41-4b18-ae57-35ef1bb07c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cea906-4fd4-4482-9770-84de0a0dd92d",
            "compositeImage": {
                "id": "8fa8e3be-a809-492b-a64a-f9300c147541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f360c9b5-8e41-4b18-ae57-35ef1bb07c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a49223f-bb4a-4673-888c-daf4c1b31c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f360c9b5-8e41-4b18-ae57-35ef1bb07c97",
                    "LayerId": "b959f132-bcd3-4105-8db7-7aff0c584004"
                }
            ]
        },
        {
            "id": "18862886-75e3-4697-853d-973dd8774e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cea906-4fd4-4482-9770-84de0a0dd92d",
            "compositeImage": {
                "id": "ea1353f1-df6a-421f-89ef-e7fd7c02f3ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18862886-75e3-4697-853d-973dd8774e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3cd79ea-c26c-4e8c-aef0-2364b75b73c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18862886-75e3-4697-853d-973dd8774e00",
                    "LayerId": "b959f132-bcd3-4105-8db7-7aff0c584004"
                }
            ]
        },
        {
            "id": "7016b5d0-9ac5-434b-a7a9-25a6683eb908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cea906-4fd4-4482-9770-84de0a0dd92d",
            "compositeImage": {
                "id": "830792d8-f77a-470d-be64-2a93ec922a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7016b5d0-9ac5-434b-a7a9-25a6683eb908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be89d0ff-fbac-4304-b5fa-0f941cdd71b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7016b5d0-9ac5-434b-a7a9-25a6683eb908",
                    "LayerId": "b959f132-bcd3-4105-8db7-7aff0c584004"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b959f132-bcd3-4105-8db7-7aff0c584004",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3cea906-4fd4-4482-9770-84de0a0dd92d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}