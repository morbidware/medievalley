{
    "id": "746cb87e-6b8e-4006-bca3-e09913ca4a99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1431e659-4b55-4da8-99c6-15df6ace75d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746cb87e-6b8e-4006-bca3-e09913ca4a99",
            "compositeImage": {
                "id": "9ea0f605-ee99-4934-bf36-148effe869a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1431e659-4b55-4da8-99c6-15df6ace75d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0445aa78-2c1a-4135-9f5e-7361e7603872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1431e659-4b55-4da8-99c6-15df6ace75d8",
                    "LayerId": "d85f9c73-d925-4af7-93e7-47171af4173b"
                }
            ]
        },
        {
            "id": "aa1cf2b5-9680-4255-8961-129fce251652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746cb87e-6b8e-4006-bca3-e09913ca4a99",
            "compositeImage": {
                "id": "8da904ec-b202-4772-941f-adc90bab7b11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa1cf2b5-9680-4255-8961-129fce251652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f914a7b7-34b1-4cc6-82c8-cf31146874b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa1cf2b5-9680-4255-8961-129fce251652",
                    "LayerId": "d85f9c73-d925-4af7-93e7-47171af4173b"
                }
            ]
        },
        {
            "id": "4ed78abf-547a-490b-8f70-426e2f0be8ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746cb87e-6b8e-4006-bca3-e09913ca4a99",
            "compositeImage": {
                "id": "c66487bc-05d7-4d6c-b391-bca8bde2f5c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ed78abf-547a-490b-8f70-426e2f0be8ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a4d99d-c948-4bd4-83f9-309c2cd4a082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ed78abf-547a-490b-8f70-426e2f0be8ad",
                    "LayerId": "d85f9c73-d925-4af7-93e7-47171af4173b"
                }
            ]
        },
        {
            "id": "f8bed3eb-471e-48df-a0b4-f206272d476d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746cb87e-6b8e-4006-bca3-e09913ca4a99",
            "compositeImage": {
                "id": "114a096a-0f31-491f-8db3-913b60f6788b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8bed3eb-471e-48df-a0b4-f206272d476d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d4a142-c98b-4fa3-86da-23702bec91ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8bed3eb-471e-48df-a0b4-f206272d476d",
                    "LayerId": "d85f9c73-d925-4af7-93e7-47171af4173b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d85f9c73-d925-4af7-93e7-47171af4173b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746cb87e-6b8e-4006-bca3-e09913ca4a99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}