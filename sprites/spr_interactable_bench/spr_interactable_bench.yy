{
    "id": "ddf2b2f7-0696-4e11-9ce0-a4e2ec3cd0d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_bench",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 38,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "565d2842-ea7e-4721-8a55-1cd136d796bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddf2b2f7-0696-4e11-9ce0-a4e2ec3cd0d3",
            "compositeImage": {
                "id": "8c19aeec-6292-48d2-a05e-513be5c87663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565d2842-ea7e-4721-8a55-1cd136d796bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "328673e8-c5ac-48e1-8cb6-cda1411ac973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565d2842-ea7e-4721-8a55-1cd136d796bc",
                    "LayerId": "dfc7d8eb-ba2e-460b-8084-29b1a6f19b60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "dfc7d8eb-ba2e-460b-8084-29b1a6f19b60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddf2b2f7-0696-4e11-9ce0-a4e2ec3cd0d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 8
}