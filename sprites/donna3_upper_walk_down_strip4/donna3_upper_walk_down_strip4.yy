{
    "id": "902b433a-afbb-4288-9b49-03b684ded96f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f453ee79-d173-4a6b-974a-341114a39436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "902b433a-afbb-4288-9b49-03b684ded96f",
            "compositeImage": {
                "id": "d0254f30-876a-42f8-a7f5-6a324ec0a229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f453ee79-d173-4a6b-974a-341114a39436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77325944-ff85-4150-8bb8-1866b6157036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f453ee79-d173-4a6b-974a-341114a39436",
                    "LayerId": "b0b39a81-200f-4950-8403-206346d57635"
                }
            ]
        },
        {
            "id": "083d18d9-82b4-4fd8-9a35-42fd913cfe2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "902b433a-afbb-4288-9b49-03b684ded96f",
            "compositeImage": {
                "id": "b861f9d9-e523-40db-8194-7197dcbb64e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083d18d9-82b4-4fd8-9a35-42fd913cfe2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60781b9a-46a7-4a15-959c-700561602e7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083d18d9-82b4-4fd8-9a35-42fd913cfe2c",
                    "LayerId": "b0b39a81-200f-4950-8403-206346d57635"
                }
            ]
        },
        {
            "id": "c60fb6ba-7a74-48d1-b696-986e32d3377d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "902b433a-afbb-4288-9b49-03b684ded96f",
            "compositeImage": {
                "id": "44ccb792-a98b-4915-a67c-59a2fcca2eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c60fb6ba-7a74-48d1-b696-986e32d3377d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bddad296-042c-4473-86d6-bf36a54d6acf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c60fb6ba-7a74-48d1-b696-986e32d3377d",
                    "LayerId": "b0b39a81-200f-4950-8403-206346d57635"
                }
            ]
        },
        {
            "id": "8e4c6c08-313f-4af0-b368-195d50faae77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "902b433a-afbb-4288-9b49-03b684ded96f",
            "compositeImage": {
                "id": "48f0daf1-baea-4126-a415-cead197e8798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e4c6c08-313f-4af0-b368-195d50faae77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d9a0c2-9ec6-43a2-82dc-a10825af178a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e4c6c08-313f-4af0-b368-195d50faae77",
                    "LayerId": "b0b39a81-200f-4950-8403-206346d57635"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b0b39a81-200f-4950-8403-206346d57635",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "902b433a-afbb-4288-9b49-03b684ded96f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}