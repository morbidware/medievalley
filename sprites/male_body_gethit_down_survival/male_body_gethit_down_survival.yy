{
    "id": "b39b004c-3354-4061-8fa5-5671b65356b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_gethit_down_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f928b1b4-84de-4edb-9cae-beafa010a321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b39b004c-3354-4061-8fa5-5671b65356b6",
            "compositeImage": {
                "id": "37a2a9f0-a196-463e-945b-4855fae852d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f928b1b4-84de-4edb-9cae-beafa010a321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2091b5-8b5f-475c-8433-ad321c46ce84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f928b1b4-84de-4edb-9cae-beafa010a321",
                    "LayerId": "dbfef40b-4e28-4930-b179-b0f3f70a1cfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dbfef40b-4e28-4930-b179-b0f3f70a1cfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b39b004c-3354-4061-8fa5-5671b65356b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}