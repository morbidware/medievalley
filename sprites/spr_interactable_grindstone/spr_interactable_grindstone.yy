{
    "id": "5f926423-2718-45ac-925b-e656357be9ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_grindstone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bf8c60d-e455-484d-ab4c-0a290d961c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f926423-2718-45ac-925b-e656357be9ae",
            "compositeImage": {
                "id": "bf687d26-f20b-4dfb-87ba-3bd4a9e74f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bf8c60d-e455-484d-ab4c-0a290d961c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93215762-8562-4324-bab9-68794675d0fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bf8c60d-e455-484d-ab4c-0a290d961c68",
                    "LayerId": "e14249d2-876e-4e81-8fad-ada11b18e5a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e14249d2-876e-4e81-8fad-ada11b18e5a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f926423-2718-45ac-925b-e656357be9ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}