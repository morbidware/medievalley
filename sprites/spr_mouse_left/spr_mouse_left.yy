{
    "id": "2e77d1d4-3651-4d86-b80c-9ed82a3a59be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c46c683b-ad0c-477f-a30e-79e3b4bacb65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e77d1d4-3651-4d86-b80c-9ed82a3a59be",
            "compositeImage": {
                "id": "ec37a3fd-7e5f-46be-a77a-c5a6b4081bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46c683b-ad0c-477f-a30e-79e3b4bacb65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4542272f-0b6c-4463-824f-fe91d5604f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46c683b-ad0c-477f-a30e-79e3b4bacb65",
                    "LayerId": "83c07c05-1ab7-4f59-8df5-8c6a80d46eac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "83c07c05-1ab7-4f59-8df5-8c6a80d46eac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e77d1d4-3651-4d86-b80c-9ed82a3a59be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 6
}