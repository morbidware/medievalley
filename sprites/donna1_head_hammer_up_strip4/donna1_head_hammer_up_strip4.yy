{
    "id": "74d8ebde-671b-437c-abc1-db5fb4936513",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24f279f8-ae26-4913-9da5-2c6860715148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d8ebde-671b-437c-abc1-db5fb4936513",
            "compositeImage": {
                "id": "5bbe634a-f032-43e7-ad80-874cc7bd38bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f279f8-ae26-4913-9da5-2c6860715148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff7bebb-e93a-4e73-b5bf-344b59ddb05b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f279f8-ae26-4913-9da5-2c6860715148",
                    "LayerId": "ebfac2b5-8b98-435c-807e-f0e6ef1f9e04"
                }
            ]
        },
        {
            "id": "e81cb68a-1b3d-4870-9d22-31582911b31b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d8ebde-671b-437c-abc1-db5fb4936513",
            "compositeImage": {
                "id": "611e14f7-75c3-41ff-a52c-32f5016cfad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81cb68a-1b3d-4870-9d22-31582911b31b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5632219a-476b-463d-81c4-1c2aaac4f00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81cb68a-1b3d-4870-9d22-31582911b31b",
                    "LayerId": "ebfac2b5-8b98-435c-807e-f0e6ef1f9e04"
                }
            ]
        },
        {
            "id": "03f53670-c41f-40cc-bd48-1bf47d6756df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d8ebde-671b-437c-abc1-db5fb4936513",
            "compositeImage": {
                "id": "f8a5a32d-3f05-43e3-b359-7d2e6f11a683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f53670-c41f-40cc-bd48-1bf47d6756df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0d8c5ff-70f8-482d-b3ab-720ea6260020",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f53670-c41f-40cc-bd48-1bf47d6756df",
                    "LayerId": "ebfac2b5-8b98-435c-807e-f0e6ef1f9e04"
                }
            ]
        },
        {
            "id": "e9fc4fd2-81f6-488b-84df-071d69c1ae56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d8ebde-671b-437c-abc1-db5fb4936513",
            "compositeImage": {
                "id": "a86d5d18-6f17-4968-9172-30c40e456061",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fc4fd2-81f6-488b-84df-071d69c1ae56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c68adb-d943-418a-9d86-57d23e3bf602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fc4fd2-81f6-488b-84df-071d69c1ae56",
                    "LayerId": "ebfac2b5-8b98-435c-807e-f0e6ef1f9e04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ebfac2b5-8b98-435c-807e-f0e6ef1f9e04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74d8ebde-671b-437c-abc1-db5fb4936513",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}