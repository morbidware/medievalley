{
    "id": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_pumpkin_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a937f3ad-02d0-4a43-98df-9d651c8a6897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
            "compositeImage": {
                "id": "ecc1eb80-1580-403c-84ad-af7771de59bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a937f3ad-02d0-4a43-98df-9d651c8a6897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "420e1901-450c-4dfc-97ea-9b097f46ff4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a937f3ad-02d0-4a43-98df-9d651c8a6897",
                    "LayerId": "567cf44a-9095-4681-8351-db1c34d0bd76"
                }
            ]
        },
        {
            "id": "f095f242-41d8-4120-b9d5-a4cab0ac56c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
            "compositeImage": {
                "id": "b54d86c3-10c8-48af-91a4-2e909af7937b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f095f242-41d8-4120-b9d5-a4cab0ac56c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159be4eb-0ae3-4db2-8485-2cbd4cdc8bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f095f242-41d8-4120-b9d5-a4cab0ac56c0",
                    "LayerId": "567cf44a-9095-4681-8351-db1c34d0bd76"
                }
            ]
        },
        {
            "id": "5de2e8ba-459c-438b-9b2c-bbb63d0198b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
            "compositeImage": {
                "id": "d8afd74f-8650-4ef6-b0f8-0e8abf3b69ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de2e8ba-459c-438b-9b2c-bbb63d0198b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac1c0e6-eff3-4627-ab62-f6beba494901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de2e8ba-459c-438b-9b2c-bbb63d0198b1",
                    "LayerId": "567cf44a-9095-4681-8351-db1c34d0bd76"
                }
            ]
        },
        {
            "id": "94dd7143-8813-4190-83ae-47531f9a4416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
            "compositeImage": {
                "id": "9d6ee1e0-2ace-48be-b7d9-570b6fe9f9ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94dd7143-8813-4190-83ae-47531f9a4416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "734ccecb-65f1-4c53-92be-c009225cae27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94dd7143-8813-4190-83ae-47531f9a4416",
                    "LayerId": "567cf44a-9095-4681-8351-db1c34d0bd76"
                }
            ]
        },
        {
            "id": "ace7abfe-1a68-464c-93eb-1ee20bd82ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
            "compositeImage": {
                "id": "5c5d2807-ed74-41e1-9a28-8c733e678c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace7abfe-1a68-464c-93eb-1ee20bd82ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4ad5d9d-a431-44a9-8d0f-977064626dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace7abfe-1a68-464c-93eb-1ee20bd82ae4",
                    "LayerId": "567cf44a-9095-4681-8351-db1c34d0bd76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "567cf44a-9095-4681-8351-db1c34d0bd76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a27cc6b-c213-4e82-add7-bf2dc80421ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 24
}