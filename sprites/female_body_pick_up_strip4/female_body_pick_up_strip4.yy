{
    "id": "01881556-35cc-4821-a9da-c8c541384ac3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 6,
    "bbox_right": 9,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "731d4f67-01c5-4a70-be56-2a2c98461149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01881556-35cc-4821-a9da-c8c541384ac3",
            "compositeImage": {
                "id": "12dacdf2-6213-464b-af8c-b16ed974e7e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "731d4f67-01c5-4a70-be56-2a2c98461149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b8bd9c-81cf-4cc9-a302-31f3d14fe3da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "731d4f67-01c5-4a70-be56-2a2c98461149",
                    "LayerId": "cf00616f-f349-47fa-b084-333c149941c2"
                }
            ]
        },
        {
            "id": "e2c0a57e-683d-4d82-b3b6-12ee1ca53739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01881556-35cc-4821-a9da-c8c541384ac3",
            "compositeImage": {
                "id": "a2ffca87-48c0-40b3-86f6-d486955e6cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2c0a57e-683d-4d82-b3b6-12ee1ca53739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2a7df56-ce11-4afe-b6c3-93e8278f0dc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2c0a57e-683d-4d82-b3b6-12ee1ca53739",
                    "LayerId": "cf00616f-f349-47fa-b084-333c149941c2"
                }
            ]
        },
        {
            "id": "d0ef87e0-b64c-4f66-9c0d-373cbb78a0d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01881556-35cc-4821-a9da-c8c541384ac3",
            "compositeImage": {
                "id": "2a364a21-25e0-48d0-a30e-78dff8bb5f97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ef87e0-b64c-4f66-9c0d-373cbb78a0d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797d66bc-60ba-406b-b15d-525ec9118506",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ef87e0-b64c-4f66-9c0d-373cbb78a0d0",
                    "LayerId": "cf00616f-f349-47fa-b084-333c149941c2"
                }
            ]
        },
        {
            "id": "4014888a-9d23-4dcb-929f-6061e2611c19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01881556-35cc-4821-a9da-c8c541384ac3",
            "compositeImage": {
                "id": "1bc7e852-5c07-472a-9028-e4a61d4ffa7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4014888a-9d23-4dcb-929f-6061e2611c19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b60b8f1f-e675-4fcf-910d-936f9bc22448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4014888a-9d23-4dcb-929f-6061e2611c19",
                    "LayerId": "cf00616f-f349-47fa-b084-333c149941c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cf00616f-f349-47fa-b084-333c149941c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01881556-35cc-4821-a9da-c8c541384ac3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}