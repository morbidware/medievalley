{
    "id": "c0607f40-2a48-4ee6-9888-2cf12b90d3ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_die_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f728f41f-d39b-4404-83fa-97c5b4d76e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0607f40-2a48-4ee6-9888-2cf12b90d3ff",
            "compositeImage": {
                "id": "643ff313-873a-4fab-bb09-f51d24a9821e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f728f41f-d39b-4404-83fa-97c5b4d76e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7973f759-2680-4bc7-bef2-8ec9e97c2082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f728f41f-d39b-4404-83fa-97c5b4d76e81",
                    "LayerId": "ff239558-0758-45f7-a3c3-0b89ddc7246f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ff239558-0758-45f7-a3c3-0b89ddc7246f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0607f40-2a48-4ee6-9888-2cf12b90d3ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}