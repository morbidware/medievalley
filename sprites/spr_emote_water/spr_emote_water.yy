{
    "id": "2958bf5a-a8dc-4d60-a5df-2170283181fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 3,
    "bbox_right": 17,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99b8a272-257d-4806-a94b-3f40075f920f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2958bf5a-a8dc-4d60-a5df-2170283181fa",
            "compositeImage": {
                "id": "37200f37-92b2-40d5-8dc0-517e64d8a230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b8a272-257d-4806-a94b-3f40075f920f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6526f0cb-b9a3-48d7-ae84-b0072deb8180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b8a272-257d-4806-a94b-3f40075f920f",
                    "LayerId": "29149580-0c76-406b-a6a7-07f42d7072e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "29149580-0c76-406b-a6a7-07f42d7072e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2958bf5a-a8dc-4d60-a5df-2170283181fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}