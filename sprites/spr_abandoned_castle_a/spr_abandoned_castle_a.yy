{
    "id": "4c21a8d6-92ef-4e49-9ebe-08d7e4a92fc6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abandoned_castle_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 0,
    "bbox_right": 279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5695ecbc-5e08-4cd5-ae16-f35cab858f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c21a8d6-92ef-4e49-9ebe-08d7e4a92fc6",
            "compositeImage": {
                "id": "c473391b-f108-42df-82ab-b249495fbde3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5695ecbc-5e08-4cd5-ae16-f35cab858f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5bbf2a1-43dd-4314-92d4-4c5b1fed33a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5695ecbc-5e08-4cd5-ae16-f35cab858f5a",
                    "LayerId": "44b13883-c3e6-45eb-b703-74e87be300c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 237,
    "layers": [
        {
            "id": "44b13883-c3e6-45eb-b703-74e87be300c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c21a8d6-92ef-4e49-9ebe-08d7e4a92fc6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 280,
    "xorig": 145,
    "yorig": 118
}