{
    "id": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_numbers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61a3035c-45f5-4a1c-831e-9d1776a5a07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "85bdb642-94e7-48bb-a290-0057c57d8ea0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61a3035c-45f5-4a1c-831e-9d1776a5a07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08cb46a9-1926-49cd-a7c3-6e5a57d3d24d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61a3035c-45f5-4a1c-831e-9d1776a5a07b",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "4288eccd-2581-4215-8616-dd77ba52e856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "b7e888d8-7d6b-4a35-bc81-46f6c5198a5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4288eccd-2581-4215-8616-dd77ba52e856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6d8cffe-35da-40f5-902f-4081a8cd7581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4288eccd-2581-4215-8616-dd77ba52e856",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "9cda8ce6-60c0-4b4e-bd62-a8851c6f0f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "7a9f4348-c339-4ddf-9700-7a1ac5f751d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cda8ce6-60c0-4b4e-bd62-a8851c6f0f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6a83ba7-7926-4874-a8f3-5b8035d7a017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cda8ce6-60c0-4b4e-bd62-a8851c6f0f4b",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "d5542352-fd2e-4931-b0c5-72424d280c9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "7cee3429-20b0-4419-aa9e-37693089e35c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5542352-fd2e-4931-b0c5-72424d280c9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc9e3e0c-b631-4420-b634-f3a7eef323a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5542352-fd2e-4931-b0c5-72424d280c9d",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "171a2b27-7b0d-43b2-8965-ce9ae718beda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "84bb73f8-a3b0-4745-b787-1788848208a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171a2b27-7b0d-43b2-8965-ce9ae718beda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "245cd3e3-b222-48a0-ab14-a647ed84b72e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171a2b27-7b0d-43b2-8965-ce9ae718beda",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "f247e394-baca-4653-9d26-712040b5b46d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "720a1cb8-057d-49c5-aaba-67e7546d7160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f247e394-baca-4653-9d26-712040b5b46d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ae4facf-5e96-4dcb-bb73-be83fb3b6be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f247e394-baca-4653-9d26-712040b5b46d",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "957446e0-886b-4ca3-b3bd-941616047cc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "4d9a0a0d-d304-4b4c-8317-7853e42ae46a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "957446e0-886b-4ca3-b3bd-941616047cc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfde0cf0-d5bd-4322-9b9a-801fe6193b5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "957446e0-886b-4ca3-b3bd-941616047cc2",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "a7a2733f-9ac2-4ba8-8ea9-83eb717813f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "7eec62e2-0c1f-4cc9-bc2c-3288d5599905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a2733f-9ac2-4ba8-8ea9-83eb717813f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd13bc1e-5757-46d8-836b-33e4871f6878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a2733f-9ac2-4ba8-8ea9-83eb717813f3",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "954ace90-e10f-43bd-ac72-0011082d684d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "adf5f622-b662-48f2-8664-aa87572efb9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "954ace90-e10f-43bd-ac72-0011082d684d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab64631-7bb4-4d8f-8067-0a5efffd6317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "954ace90-e10f-43bd-ac72-0011082d684d",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "582538dd-1d4e-43ec-8d3c-6ffaa3ce1592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "8d812e2c-1e3b-461b-8a62-f4ba2c34f802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582538dd-1d4e-43ec-8d3c-6ffaa3ce1592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faaac803-35c8-421b-b612-ed65c68e66ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582538dd-1d4e-43ec-8d3c-6ffaa3ce1592",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "614c0ded-4e35-4cc5-8172-d754edd7ed47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "df7ed4bf-f4ee-4e0c-8d07-47921be5808c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614c0ded-4e35-4cc5-8172-d754edd7ed47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f41941d-2193-4cee-8346-5f3d52740250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614c0ded-4e35-4cc5-8172-d754edd7ed47",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        },
        {
            "id": "30884919-a381-4b94-a421-bfe79b55d5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "compositeImage": {
                "id": "c2ebe3fd-255a-4ea7-ba92-9def492ec5b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30884919-a381-4b94-a421-bfe79b55d5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db446820-fb38-432e-a22e-d3a55fe77db0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30884919-a381-4b94-a421-bfe79b55d5b5",
                    "LayerId": "654a2f43-ae59-4d9b-87e4-c2ac158e3554"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "654a2f43-ae59-4d9b-87e4-c2ac158e3554",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "016e08a7-6e24-4a47-a515-dede3aa6cddd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3,
    "xorig": 0,
    "yorig": 0
}