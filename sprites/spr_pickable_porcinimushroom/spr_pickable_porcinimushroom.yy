{
    "id": "1e9e78b3-0225-412a-9c04-772c2a67f730",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_porcinimushroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8af76a2-de43-4bdb-8517-1f830dec29ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e9e78b3-0225-412a-9c04-772c2a67f730",
            "compositeImage": {
                "id": "07a81c16-dca0-4ee9-a185-6f97363c3c48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8af76a2-de43-4bdb-8517-1f830dec29ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f66f435-f1eb-4c54-a86e-31abce7f9f67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8af76a2-de43-4bdb-8517-1f830dec29ad",
                    "LayerId": "5ac67288-f2c9-4fb5-aa58-34d5a158eddb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5ac67288-f2c9-4fb5-aa58-34d5a158eddb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e9e78b3-0225-412a-9c04-772c2a67f730",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}