{
    "id": "7569919e-5928-4ac9-8520-32a566c083bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potato",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26e72821-1859-466a-8aed-b2bfc0c42977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7569919e-5928-4ac9-8520-32a566c083bd",
            "compositeImage": {
                "id": "151b550b-d89a-4a0f-960b-3fb9976e2f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26e72821-1859-466a-8aed-b2bfc0c42977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "987d3c26-7e9e-4ad9-bbb8-3fe0fc699e22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26e72821-1859-466a-8aed-b2bfc0c42977",
                    "LayerId": "f0cb10a4-c090-4888-a50b-894d2699f485"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f0cb10a4-c090-4888-a50b-894d2699f485",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7569919e-5928-4ac9-8520-32a566c083bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}