{
    "id": "dadbd4bf-7090-4b45-b80d-2d5ec4a7b438",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cf3a9dd-ff49-4156-9e14-efcc72920768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dadbd4bf-7090-4b45-b80d-2d5ec4a7b438",
            "compositeImage": {
                "id": "e480ac2f-3612-468a-b017-4025b7bcac27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf3a9dd-ff49-4156-9e14-efcc72920768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d98459-662b-407f-ab4f-90d0e80c85ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf3a9dd-ff49-4156-9e14-efcc72920768",
                    "LayerId": "a303fca6-4c22-4bcc-b120-eaf123074ca4"
                }
            ]
        },
        {
            "id": "e2e434c0-da74-4ddf-bbe2-5c105b188f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dadbd4bf-7090-4b45-b80d-2d5ec4a7b438",
            "compositeImage": {
                "id": "1ad842b5-8b00-45fc-b72e-db97474dd533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e434c0-da74-4ddf-bbe2-5c105b188f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b1000b-6c7b-48d9-a1ac-867123fab63e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e434c0-da74-4ddf-bbe2-5c105b188f37",
                    "LayerId": "a303fca6-4c22-4bcc-b120-eaf123074ca4"
                }
            ]
        },
        {
            "id": "ffcadd23-ca2f-4868-ae0e-a764301ba736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dadbd4bf-7090-4b45-b80d-2d5ec4a7b438",
            "compositeImage": {
                "id": "ee20013d-6ac8-4cb0-a32a-6660ce79b972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffcadd23-ca2f-4868-ae0e-a764301ba736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e87ced3-38fb-4ea4-9ff9-3f7f3a9e658b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffcadd23-ca2f-4868-ae0e-a764301ba736",
                    "LayerId": "a303fca6-4c22-4bcc-b120-eaf123074ca4"
                }
            ]
        },
        {
            "id": "89bfbd98-12d7-4089-9d9b-ebbae3403234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dadbd4bf-7090-4b45-b80d-2d5ec4a7b438",
            "compositeImage": {
                "id": "3cdcefe3-bb5d-4eb3-9bb9-2ee1b7fadf9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89bfbd98-12d7-4089-9d9b-ebbae3403234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f27d2a50-e95a-413d-bb0c-02b133560a68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89bfbd98-12d7-4089-9d9b-ebbae3403234",
                    "LayerId": "a303fca6-4c22-4bcc-b120-eaf123074ca4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a303fca6-4c22-4bcc-b120-eaf123074ca4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dadbd4bf-7090-4b45-b80d-2d5ec4a7b438",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}