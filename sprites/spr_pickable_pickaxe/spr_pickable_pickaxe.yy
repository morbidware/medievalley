{
    "id": "2ad946df-33b9-4b9e-b8ee-d0f731ca6ac3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_pickaxe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14466ef9-7562-48d0-8f86-e7c21b40eb95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ad946df-33b9-4b9e-b8ee-d0f731ca6ac3",
            "compositeImage": {
                "id": "0366edda-2f71-426d-b24f-fbdcdb238f59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14466ef9-7562-48d0-8f86-e7c21b40eb95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9448162-5f3e-4d2c-9f4b-4a535089e549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14466ef9-7562-48d0-8f86-e7c21b40eb95",
                    "LayerId": "4b823026-91c0-445c-be1f-b6df845fbada"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4b823026-91c0-445c-be1f-b6df845fbada",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ad946df-33b9-4b9e-b8ee-d0f731ca6ac3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 6,
    "yorig": 11
}