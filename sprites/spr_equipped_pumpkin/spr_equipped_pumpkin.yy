{
    "id": "ecd6005c-039a-4fe0-8eb1-99068edddf68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_pumpkin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5efd488-d268-49fb-80e7-37940cd150ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecd6005c-039a-4fe0-8eb1-99068edddf68",
            "compositeImage": {
                "id": "328b6862-44d3-4d3b-8190-e2adfc838dce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5efd488-d268-49fb-80e7-37940cd150ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1102df21-c46a-4bf0-a5b4-31cebf328825",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5efd488-d268-49fb-80e7-37940cd150ad",
                    "LayerId": "22e15532-d2be-4473-8947-5ec65d552e8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22e15532-d2be-4473-8947-5ec65d552e8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecd6005c-039a-4fe0-8eb1-99068edddf68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}