{
    "id": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_watering_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5fc8135b-26d9-42f4-b874-53879048a337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "compositeImage": {
                "id": "92f16550-0761-47bd-88bd-da44716bab3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fc8135b-26d9-42f4-b874-53879048a337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fc5a566-e556-4ebb-95f0-f47a58e0c666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fc8135b-26d9-42f4-b874-53879048a337",
                    "LayerId": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1"
                }
            ]
        },
        {
            "id": "1182d6b4-d801-4554-9e0a-6aa29adbe427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "compositeImage": {
                "id": "7dfb680b-5b83-4d26-b8d4-06026d3d4a76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1182d6b4-d801-4554-9e0a-6aa29adbe427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60346304-0f62-4c48-8dd6-86aa5f422099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1182d6b4-d801-4554-9e0a-6aa29adbe427",
                    "LayerId": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1"
                }
            ]
        },
        {
            "id": "47150fbc-f18c-4ae7-8cd2-b489ee88b807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "compositeImage": {
                "id": "ff7f7e60-b295-4376-bb5a-383edb6be244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47150fbc-f18c-4ae7-8cd2-b489ee88b807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a11a68-2547-47b6-8a2d-2c06ec5b61f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47150fbc-f18c-4ae7-8cd2-b489ee88b807",
                    "LayerId": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1"
                }
            ]
        },
        {
            "id": "a90b9bb0-5bbe-41f7-a88a-351e533c078f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "compositeImage": {
                "id": "6017bd5a-2e25-40ff-b269-7a440ae953c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90b9bb0-5bbe-41f7-a88a-351e533c078f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e7fd51c-9155-473e-9c75-63a65a9aaaaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90b9bb0-5bbe-41f7-a88a-351e533c078f",
                    "LayerId": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1"
                }
            ]
        },
        {
            "id": "9054580f-46a8-4bc7-980e-7635ee1b56b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "compositeImage": {
                "id": "18ce7f2f-e0cf-4caf-b6e3-085ab1e241cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9054580f-46a8-4bc7-980e-7635ee1b56b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c27f60f-bb7f-40f9-ab27-ff04999f3d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9054580f-46a8-4bc7-980e-7635ee1b56b2",
                    "LayerId": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1"
                }
            ]
        },
        {
            "id": "f1c2a78c-41ad-4a72-bb86-6e4a604005a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "compositeImage": {
                "id": "9e8a388e-1560-4fb0-b8a7-12c27e6461be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c2a78c-41ad-4a72-bb86-6e4a604005a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddec7799-209a-487b-8f4a-4c67bf73c559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c2a78c-41ad-4a72-bb86-6e4a604005a0",
                    "LayerId": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4072dc22-8bd7-44a6-9bdd-f6c5cfd8f3e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5143443-ec1d-498f-99b0-8626e3ad2a02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}