{
    "id": "66b16eaf-4b3f-46bb-852e-150c9a3c2c11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_particle_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6044036-c968-499a-86ff-75e3ed07ae66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66b16eaf-4b3f-46bb-852e-150c9a3c2c11",
            "compositeImage": {
                "id": "ba30a8fc-a02a-41ae-9d08-2c76ed0d84f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6044036-c968-499a-86ff-75e3ed07ae66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d852558-9585-4703-995a-eadbaa17c6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6044036-c968-499a-86ff-75e3ed07ae66",
                    "LayerId": "0648dd0a-42a7-4fc7-8197-164e1113d5e0"
                }
            ]
        },
        {
            "id": "01fd76c0-9abd-498c-ad7a-8ef449b27f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66b16eaf-4b3f-46bb-852e-150c9a3c2c11",
            "compositeImage": {
                "id": "cefe1733-c7fc-4c10-9498-75365a339240",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01fd76c0-9abd-498c-ad7a-8ef449b27f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4d1b47-49d6-460c-9245-4d8339009a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01fd76c0-9abd-498c-ad7a-8ef449b27f6d",
                    "LayerId": "0648dd0a-42a7-4fc7-8197-164e1113d5e0"
                }
            ]
        },
        {
            "id": "30d1c91d-4141-44c6-b28d-d7645de476aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66b16eaf-4b3f-46bb-852e-150c9a3c2c11",
            "compositeImage": {
                "id": "bd4d81cb-7e68-422c-9cbb-fccca2d9f9c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30d1c91d-4141-44c6-b28d-d7645de476aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a13f0f2c-9115-48da-81ab-394d466fbf57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30d1c91d-4141-44c6-b28d-d7645de476aa",
                    "LayerId": "0648dd0a-42a7-4fc7-8197-164e1113d5e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "0648dd0a-42a7-4fc7-8197-164e1113d5e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66b16eaf-4b3f-46bb-852e-150c9a3c2c11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4,
    "xorig": 2,
    "yorig": 2
}