{
    "id": "459bd311-91b6-465f-9cbc-f9b67b8d65db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_meat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af6921f4-bd0c-44b0-b4da-1ad5c88c2f39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "459bd311-91b6-465f-9cbc-f9b67b8d65db",
            "compositeImage": {
                "id": "31a70e07-48b4-4cf0-a13f-3b9e8febaf1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af6921f4-bd0c-44b0-b4da-1ad5c88c2f39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c546d5-a81e-4605-9aee-e699abd1ae96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af6921f4-bd0c-44b0-b4da-1ad5c88c2f39",
                    "LayerId": "2562dfad-aa6d-4700-b282-63fddbfeedfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2562dfad-aa6d-4700-b282-63fddbfeedfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "459bd311-91b6-465f-9cbc-f9b67b8d65db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}