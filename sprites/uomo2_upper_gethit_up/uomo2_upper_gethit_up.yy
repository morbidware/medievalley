{
    "id": "8b25aedf-63ae-4629-a04e-c608235ca6ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd1dbb77-22b2-40c1-bad0-222c24cd7f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b25aedf-63ae-4629-a04e-c608235ca6ff",
            "compositeImage": {
                "id": "88bf162d-caf2-4a9c-b32d-f44396e63b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd1dbb77-22b2-40c1-bad0-222c24cd7f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c10e69eb-504c-497f-b34f-cece6eba0229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd1dbb77-22b2-40c1-bad0-222c24cd7f3b",
                    "LayerId": "dbf2b5e1-ba92-4589-906a-80b167fa4489"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dbf2b5e1-ba92-4589-906a-80b167fa4489",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b25aedf-63ae-4629-a04e-c608235ca6ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}