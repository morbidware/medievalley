{
    "id": "f9680ac1-107c-4557-a534-a0c1740ede73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_pick_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fbeb756-ca82-43e8-a1aa-acdb38c2ded4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "compositeImage": {
                "id": "7af1ac01-74de-457f-a103-ce0f8cc9e26a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbeb756-ca82-43e8-a1aa-acdb38c2ded4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e10777c7-099d-4870-b257-9e584eff8e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbeb756-ca82-43e8-a1aa-acdb38c2ded4",
                    "LayerId": "86184d85-f025-4a6c-8706-32ec7c3dce5e"
                }
            ]
        },
        {
            "id": "2a466e54-f1c0-4955-997d-35bff6d04843",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "compositeImage": {
                "id": "6dffb6c0-54d0-480e-a177-64d7a95eaec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a466e54-f1c0-4955-997d-35bff6d04843",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693c88cc-c4fd-46a4-841e-e6b355c23f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a466e54-f1c0-4955-997d-35bff6d04843",
                    "LayerId": "86184d85-f025-4a6c-8706-32ec7c3dce5e"
                }
            ]
        },
        {
            "id": "fb5ae98f-1d43-46bb-a130-b2acfa3b3f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "compositeImage": {
                "id": "82a4cb60-be98-4948-b178-77192e760f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb5ae98f-1d43-46bb-a130-b2acfa3b3f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6598a8e4-e4ad-47de-8782-3caef72c1b0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb5ae98f-1d43-46bb-a130-b2acfa3b3f0d",
                    "LayerId": "86184d85-f025-4a6c-8706-32ec7c3dce5e"
                }
            ]
        },
        {
            "id": "4696731c-e267-468b-a8d4-8dd06361b9c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "compositeImage": {
                "id": "22674b83-2174-47b4-989e-eb8e2583bbcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4696731c-e267-468b-a8d4-8dd06361b9c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0a82ea6-2b96-4af7-a005-ac19891d4e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4696731c-e267-468b-a8d4-8dd06361b9c6",
                    "LayerId": "86184d85-f025-4a6c-8706-32ec7c3dce5e"
                }
            ]
        },
        {
            "id": "1a913dfe-aeb2-4656-b938-e5361641a7b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "compositeImage": {
                "id": "36088fe7-8b76-4614-ae8d-1839966130dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a913dfe-aeb2-4656-b938-e5361641a7b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd6e7d6d-290c-465a-a37b-f1482c4ea7c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a913dfe-aeb2-4656-b938-e5361641a7b6",
                    "LayerId": "86184d85-f025-4a6c-8706-32ec7c3dce5e"
                }
            ]
        },
        {
            "id": "0cb4dce4-2c44-4535-9767-b2cb016d7ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "compositeImage": {
                "id": "2adc3b7b-eb50-4e91-be81-f59dd55f0cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb4dce4-2c44-4535-9767-b2cb016d7ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a03411-a576-444f-83be-cded3ceff836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb4dce4-2c44-4535-9767-b2cb016d7ce3",
                    "LayerId": "86184d85-f025-4a6c-8706-32ec7c3dce5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "86184d85-f025-4a6c-8706-32ec7c3dce5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9680ac1-107c-4557-a534-a0c1740ede73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}