{
    "id": "849852c1-dbbb-466a-933b-f738546726f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_coast_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07845080-62ed-4e53-8637-09ddcccb664d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "8e7bbeac-fc45-4404-a620-24edb89bbc2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07845080-62ed-4e53-8637-09ddcccb664d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5402f78-49f7-4940-bb99-893eb0ddc742",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07845080-62ed-4e53-8637-09ddcccb664d",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "d855175a-8c7f-4e4f-acb2-cf46ceed0aa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "d3d88294-0231-4e33-ae95-543db1dd8a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d855175a-8c7f-4e4f-acb2-cf46ceed0aa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "283a5626-67c7-4524-bf06-04cd2c0064f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d855175a-8c7f-4e4f-acb2-cf46ceed0aa7",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "424913c5-d68d-4f2d-a4fb-0fe119008e58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "c0c59b74-b6ff-4e30-99b9-b8f649efdfa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "424913c5-d68d-4f2d-a4fb-0fe119008e58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d5a848-aef3-45a5-b9cc-848da0acd88c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "424913c5-d68d-4f2d-a4fb-0fe119008e58",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "afcb1097-1cc5-4a47-a444-f77c27b34893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "6d8449f4-38bd-4a43-aa2c-9515131b51b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afcb1097-1cc5-4a47-a444-f77c27b34893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f95c959-b111-4ffc-a2b5-d6cd2d190293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afcb1097-1cc5-4a47-a444-f77c27b34893",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "d36446bc-7ebd-4aeb-bf85-8ba2101c6d9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "b1c6d406-e4e4-4607-ab82-db6db8541c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d36446bc-7ebd-4aeb-bf85-8ba2101c6d9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0080285b-ca84-470f-ac94-44897a7d4ec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d36446bc-7ebd-4aeb-bf85-8ba2101c6d9c",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "6ff715af-5a3f-4f2b-a31e-7b9edc4aac23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "5e8d020f-b112-4cee-a770-c3f5455aa265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff715af-5a3f-4f2b-a31e-7b9edc4aac23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46199c31-e967-4847-8320-e08e176a195f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff715af-5a3f-4f2b-a31e-7b9edc4aac23",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "a7f25af7-3473-43f2-8d5c-e4bc1ac61925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "10eb705d-51c0-4a65-8557-52e41c4f074d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f25af7-3473-43f2-8d5c-e4bc1ac61925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fb3accd-70da-42bc-bfd0-ccb453d65b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f25af7-3473-43f2-8d5c-e4bc1ac61925",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "1ea9aa81-2447-42c3-89f2-85fe7351a42b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "f60a4cf4-547f-4dac-96c1-b50c8430a796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ea9aa81-2447-42c3-89f2-85fe7351a42b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8230474-9953-474f-b1c5-9ebca9266933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ea9aa81-2447-42c3-89f2-85fe7351a42b",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "e4ebbf76-6b1d-410b-95fe-74c651086808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "90a98abc-104d-4c80-a062-8a4b0a6f2c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ebbf76-6b1d-410b-95fe-74c651086808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24b786a8-1bfc-415c-aa1f-f88891aa2b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ebbf76-6b1d-410b-95fe-74c651086808",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "4cba4041-16be-41a0-873f-07a11cb10684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "47408af9-6085-4c11-a158-94020864d6ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cba4041-16be-41a0-873f-07a11cb10684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bbcaafb-87b1-4b02-898d-3056e921315b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cba4041-16be-41a0-873f-07a11cb10684",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "bffd92a9-7bcf-45c9-b7e2-e90ece130d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "7e094326-9491-49bc-a118-cc3f6d09c1bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bffd92a9-7bcf-45c9-b7e2-e90ece130d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcc8e352-ef96-46f9-859d-32d736cbc2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bffd92a9-7bcf-45c9-b7e2-e90ece130d61",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "1b751822-eea2-4574-83a3-b79c6149eef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "e4681d6b-0d88-414d-93a3-3da28c8db79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b751822-eea2-4574-83a3-b79c6149eef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5114b2a8-70c4-4d35-95f8-20af70bc5be9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b751822-eea2-4574-83a3-b79c6149eef5",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "64efc872-8a6d-4887-9c9c-de9b0d33c0d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "f2100f30-b29e-4bbd-84bb-af490b4836db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64efc872-8a6d-4887-9c9c-de9b0d33c0d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f2f141-f658-487b-b8a1-7e8fef8125d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64efc872-8a6d-4887-9c9c-de9b0d33c0d9",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "d69f9efb-1f39-444b-ab3a-fdfb897b96b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "bb7553b2-d74c-4cc5-9aca-2c8b992c98c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d69f9efb-1f39-444b-ab3a-fdfb897b96b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130eacf7-d868-447d-aa51-cef60cb816ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d69f9efb-1f39-444b-ab3a-fdfb897b96b2",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "f1f7e09b-ce6c-43e6-a0a0-bc0047a086b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "cad2734e-e1de-43ab-bb6d-a7bd6c489b71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f7e09b-ce6c-43e6-a0a0-bc0047a086b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6b4a0e1-6ef3-4868-b370-af2b61f1c4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f7e09b-ce6c-43e6-a0a0-bc0047a086b7",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "0b8a48b8-6853-4dbe-a318-b95c3fab3c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "e948b55b-edca-4ce7-a0ac-4e88997fa893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8a48b8-6853-4dbe-a318-b95c3fab3c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c760e2-c365-469c-8a71-1bde4904cd6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8a48b8-6853-4dbe-a318-b95c3fab3c56",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "e825ab7b-5eaa-4132-9c4f-7d751dd119a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "3dc9234c-020d-481f-b05c-b5fdc8984b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e825ab7b-5eaa-4132-9c4f-7d751dd119a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9b98608-ec98-4e43-a844-0dbf8ccf4118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e825ab7b-5eaa-4132-9c4f-7d751dd119a8",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "d46cee42-f615-402f-8f2f-c825cee95a76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "27c2b630-83ef-4fdf-a132-b13c947de08b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46cee42-f615-402f-8f2f-c825cee95a76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc7e9c5-b00e-4951-902d-476f579c120f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46cee42-f615-402f-8f2f-c825cee95a76",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "4b6c8016-4435-4ae7-8e5c-bfc7478183c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "c05c69f0-0f54-4770-8997-34571b0ddc44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6c8016-4435-4ae7-8e5c-bfc7478183c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b70fe4-ae2a-410d-9fa5-3dea9b8ed139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6c8016-4435-4ae7-8e5c-bfc7478183c1",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "b761d4ba-4887-463a-87d2-a8b2b86d52ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "7fd3ce7e-26c5-44e5-815b-f50e0112df48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b761d4ba-4887-463a-87d2-a8b2b86d52ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0dfcaa-831f-4846-827d-278d7fd3acaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b761d4ba-4887-463a-87d2-a8b2b86d52ab",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "3786e1bf-d554-4413-8340-93f20c1159c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "4ee893eb-fda9-4193-8ccc-1dbe18524b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3786e1bf-d554-4413-8340-93f20c1159c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16e3c47-17b8-41ec-8d3b-b5537d473b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3786e1bf-d554-4413-8340-93f20c1159c7",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "c7dd8ee6-4c90-4efa-881e-7f5811360f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "86fd467b-da1e-4bf4-9547-6bcebd564721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7dd8ee6-4c90-4efa-881e-7f5811360f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71267e54-bb57-4069-a41b-fd0dcfb52890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7dd8ee6-4c90-4efa-881e-7f5811360f9b",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "9d670822-aba8-4835-a697-7682a80a4bd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "10cc2b26-ae37-460c-93e8-4b2720570411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d670822-aba8-4835-a697-7682a80a4bd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cfdb7b0-31cf-4557-ba17-33f41d160c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d670822-aba8-4835-a697-7682a80a4bd9",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "5c9069d3-4b39-4287-81dd-775770a33f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "e4c29e59-afff-406b-b499-81694a56e4d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9069d3-4b39-4287-81dd-775770a33f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "873b687c-095f-437d-b05f-78ff417774b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9069d3-4b39-4287-81dd-775770a33f6a",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "a350d899-c8b6-4a64-8ed1-00cb4f9c8918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "820fd8b9-c6ba-4ab3-a669-f8cf0251ae3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a350d899-c8b6-4a64-8ed1-00cb4f9c8918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d94ea322-f831-4b6d-9f1f-c1a22dab268b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a350d899-c8b6-4a64-8ed1-00cb4f9c8918",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "4270590e-9e8d-424d-808b-de613ae3f3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "af8859fb-7a45-4e9e-b9e4-6089e57c51c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4270590e-9e8d-424d-808b-de613ae3f3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60573e33-9ae3-4ec3-a684-4accb09772a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4270590e-9e8d-424d-808b-de613ae3f3c6",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "71defb82-4a22-45d6-b445-4ff548d55b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "c2d778a8-8d0d-4a51-b6f5-c1f86dc11d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71defb82-4a22-45d6-b445-4ff548d55b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88844774-93f5-443c-83c2-8516b686eb02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71defb82-4a22-45d6-b445-4ff548d55b5a",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "eb270812-6887-4aec-a65c-8340c44d0488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "312a7632-c162-4e30-8d9b-b01b3ce29347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb270812-6887-4aec-a65c-8340c44d0488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a079ae7-1e21-4b70-aa14-6b8cdf944d30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb270812-6887-4aec-a65c-8340c44d0488",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "8286f356-3f0c-42db-8645-a50ae50bcbbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "2c864480-52b6-4951-a1d1-beb17002317b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8286f356-3f0c-42db-8645-a50ae50bcbbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8516432f-3da9-49f3-82cc-79f317151205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8286f356-3f0c-42db-8645-a50ae50bcbbb",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "cffa029d-f8d1-4f5c-ba1b-07f5d6cd4c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "7ade310a-5d5e-4f4d-b6b3-7c3a88cfd8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffa029d-f8d1-4f5c-ba1b-07f5d6cd4c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea318e9-7381-4d29-97c7-99e53d09d306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffa029d-f8d1-4f5c-ba1b-07f5d6cd4c6e",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "810f2589-56c1-4f29-a863-7964a11bc1e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "80fc7cb5-478b-496d-890b-b60a7f61da78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "810f2589-56c1-4f29-a863-7964a11bc1e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc35a99-06f4-4311-8892-6f7a3fb1fefa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "810f2589-56c1-4f29-a863-7964a11bc1e6",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "20754756-d629-478e-a3ac-c24a7c665723",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "b82612b1-0fe7-41a9-a716-db85ca03fd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20754756-d629-478e-a3ac-c24a7c665723",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9e716f-ef21-4cdb-8244-baa315528e1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20754756-d629-478e-a3ac-c24a7c665723",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "3c5acaa8-6ba3-4c00-8a00-79f1288a45f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "8c9125ad-481a-46bd-870c-fa3ade003cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c5acaa8-6ba3-4c00-8a00-79f1288a45f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35a9cb8-807f-4bbb-8674-5e86fdedf0d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5acaa8-6ba3-4c00-8a00-79f1288a45f8",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "1ef8e181-c3d5-450f-8fee-d9ffd2246d3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "23ee8350-0f3c-425c-b008-964045e81335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ef8e181-c3d5-450f-8fee-d9ffd2246d3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c80c747-73a4-4bbf-9cf0-fe624a081b48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ef8e181-c3d5-450f-8fee-d9ffd2246d3d",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "da4aefb0-c00f-481f-8a08-8301d2085041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "5cc4f408-8353-4f6b-9f0b-cb4fcf85a94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da4aefb0-c00f-481f-8a08-8301d2085041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "905e3821-0051-4999-b081-8e0feb718c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da4aefb0-c00f-481f-8a08-8301d2085041",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "7251c73b-56bf-4536-adb6-72d25ab2e30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "0e2243ce-4672-46d2-a77c-2e96bc8d6d22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7251c73b-56bf-4536-adb6-72d25ab2e30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eddb21b7-a1ba-4983-a410-32b1270ead7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7251c73b-56bf-4536-adb6-72d25ab2e30d",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "c2c21703-4070-4da5-aabd-fcded5cf04b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "405d501e-622c-4ec2-96fd-706c6b20ae89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c21703-4070-4da5-aabd-fcded5cf04b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f57f67f-67a1-4ba8-93cf-6f0da325bf81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c21703-4070-4da5-aabd-fcded5cf04b9",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "ba96a67e-2143-420b-853e-29cc51fef6bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "bd0b2517-a2b4-47c8-a10f-c5a237f85ad4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba96a67e-2143-420b-853e-29cc51fef6bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52db10ff-7989-4630-a586-c96ab711f29c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba96a67e-2143-420b-853e-29cc51fef6bd",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "c87ee8a0-817b-4e70-989b-78d7c8bad6eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "5a9afe12-4108-42e4-8b12-8ed389b94e5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87ee8a0-817b-4e70-989b-78d7c8bad6eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c66403-0a46-459e-8790-50553f63e81a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87ee8a0-817b-4e70-989b-78d7c8bad6eb",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "95a035a4-fd2b-4cc3-b743-dfc157e474ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "842a9a2b-4a8e-487a-8586-b6d5f3a4899b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a035a4-fd2b-4cc3-b743-dfc157e474ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a880ee3f-2bf8-4a56-a313-c688d342eeda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a035a4-fd2b-4cc3-b743-dfc157e474ec",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "4bc64c61-72a6-460d-96ec-f5d991ed45d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "a1e4238e-c0be-4cd9-b0d5-018ec17aa934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bc64c61-72a6-460d-96ec-f5d991ed45d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10f2113d-f1f2-4a0d-a035-629daa77783f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bc64c61-72a6-460d-96ec-f5d991ed45d5",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "33d6b15f-561f-48de-abf8-495f7f8a3bec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "05ce1573-85c4-466d-9255-b0519aeb7812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d6b15f-561f-48de-abf8-495f7f8a3bec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65739260-ef59-4ff8-80d9-a21f20a3fc4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d6b15f-561f-48de-abf8-495f7f8a3bec",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "4e01666e-1bdd-44d1-a480-404062b312de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "7fedaa6f-46a3-46a8-b99f-587d823656e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e01666e-1bdd-44d1-a480-404062b312de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e48fd1d-ae2d-4d8a-ab3f-cf3a966ed6b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e01666e-1bdd-44d1-a480-404062b312de",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "b83c255e-ab6a-40a2-81f5-361e1156a8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "d25f5cf6-c520-49e1-a3ca-2597fad910ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83c255e-ab6a-40a2-81f5-361e1156a8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c1d8e0-fecf-4329-8149-16bdd7cf44b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83c255e-ab6a-40a2-81f5-361e1156a8e9",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "400d2f57-4a61-4d5e-b628-28775f4c700c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "680a2125-5e88-4516-9901-4b915ad59d2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "400d2f57-4a61-4d5e-b628-28775f4c700c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a91ce5d8-e8c4-4292-8851-86680a2bc4e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "400d2f57-4a61-4d5e-b628-28775f4c700c",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "55a63be8-a0b7-47bf-9f22-1c1335c8a0cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "e1f298e8-087c-4538-a348-231f87bd8cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55a63be8-a0b7-47bf-9f22-1c1335c8a0cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4340c6d7-192d-4ab2-8268-a9fcdce73bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55a63be8-a0b7-47bf-9f22-1c1335c8a0cb",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "b93c2aac-19b4-4a64-a094-30f2846fbf98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "ed0b7f6b-d8c5-412c-bdea-1fd84d84c81c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b93c2aac-19b4-4a64-a094-30f2846fbf98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95adfb37-16c2-4d20-b4f2-8d044e26d0b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b93c2aac-19b4-4a64-a094-30f2846fbf98",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "f7993fcc-ed35-4524-ad9c-811534d03061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "8ac0e30d-412b-4a19-9cf2-6e0166bfdfd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7993fcc-ed35-4524-ad9c-811534d03061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81fe33ed-dda0-498e-8f47-383b20e3ff96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7993fcc-ed35-4524-ad9c-811534d03061",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        },
        {
            "id": "b8aeaaff-71e3-43c9-bc60-8020bfbe41c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "compositeImage": {
                "id": "6898248e-b5d8-4bc4-9850-986cd20e2592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8aeaaff-71e3-43c9-bc60-8020bfbe41c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b94a0840-7398-46c2-b237-8a643b9dadb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8aeaaff-71e3-43c9-bc60-8020bfbe41c7",
                    "LayerId": "54b40c5c-821a-4d27-a513-4b4384ce1267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "54b40c5c-821a-4d27-a513-4b4384ce1267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "849852c1-dbbb-466a-933b-f738546726f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}