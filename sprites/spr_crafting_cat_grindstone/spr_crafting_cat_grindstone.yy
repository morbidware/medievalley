{
    "id": "764e32e5-abf6-4578-957d-07fa840180e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_grindstone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8728b5c-6282-49f9-878a-99334ab701f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764e32e5-abf6-4578-957d-07fa840180e1",
            "compositeImage": {
                "id": "a0ef9aab-5d59-4474-a672-1dcf005b6e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8728b5c-6282-49f9-878a-99334ab701f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb5fd9e1-0d00-4911-940e-73b610a19332",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8728b5c-6282-49f9-878a-99334ab701f2",
                    "LayerId": "9800e35f-86cf-43f3-86cd-bd31481c7782"
                }
            ]
        },
        {
            "id": "643cdffb-d7d9-401f-8a3b-ee835d60220b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "764e32e5-abf6-4578-957d-07fa840180e1",
            "compositeImage": {
                "id": "6498fd38-f0ac-4198-8453-4ac21022832d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643cdffb-d7d9-401f-8a3b-ee835d60220b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea060d5-c1d8-42e5-a0a3-1546ff011809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643cdffb-d7d9-401f-8a3b-ee835d60220b",
                    "LayerId": "9800e35f-86cf-43f3-86cd-bd31481c7782"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "9800e35f-86cf-43f3-86cd-bd31481c7782",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "764e32e5-abf6-4578-957d-07fa840180e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}