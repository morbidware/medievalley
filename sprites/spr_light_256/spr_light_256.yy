{
    "id": "e79507b4-8799-4434-a669-ddf07bc4b815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light_256",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c79242ef-4c47-43ed-9d28-b7fc77b87298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e79507b4-8799-4434-a669-ddf07bc4b815",
            "compositeImage": {
                "id": "1d35b716-3aa9-49e0-8b4e-3662127924ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79242ef-4c47-43ed-9d28-b7fc77b87298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6bd516-6fc6-40cb-bf93-fc6b97f039f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79242ef-4c47-43ed-9d28-b7fc77b87298",
                    "LayerId": "4f8e5a6a-7817-4881-8bd3-0c9bf7931dad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "4f8e5a6a-7817-4881-8bd3-0c9bf7931dad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e79507b4-8799-4434-a669-ddf07bc4b815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}