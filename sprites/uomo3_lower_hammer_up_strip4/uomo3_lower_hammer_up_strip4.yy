{
    "id": "113839ec-ac2f-4e78-9952-13720a4dcc59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "478a2baa-9c2e-44c5-9ef9-e95036badf31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "113839ec-ac2f-4e78-9952-13720a4dcc59",
            "compositeImage": {
                "id": "0d26c53c-3b4e-4a25-b38f-ebcfd4a0d3ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "478a2baa-9c2e-44c5-9ef9-e95036badf31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb8a820-dc98-4bc2-8e63-c11a22af55e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "478a2baa-9c2e-44c5-9ef9-e95036badf31",
                    "LayerId": "42d39d6a-a0c2-49dc-a5f5-cb1607f2c3f6"
                }
            ]
        },
        {
            "id": "fcf94ce5-da1e-4456-9a26-a88e1a8ea1bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "113839ec-ac2f-4e78-9952-13720a4dcc59",
            "compositeImage": {
                "id": "38d0395f-f04d-43a8-8755-d097d2af7a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf94ce5-da1e-4456-9a26-a88e1a8ea1bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0f0082b-bbaf-4b25-977b-a3ef6b7626be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf94ce5-da1e-4456-9a26-a88e1a8ea1bb",
                    "LayerId": "42d39d6a-a0c2-49dc-a5f5-cb1607f2c3f6"
                }
            ]
        },
        {
            "id": "ece33229-ce40-4174-8cb8-e21c2ec2035a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "113839ec-ac2f-4e78-9952-13720a4dcc59",
            "compositeImage": {
                "id": "7b8f428e-7b99-4207-af2a-069c6ba70159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ece33229-ce40-4174-8cb8-e21c2ec2035a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb4d368-0a5d-4f00-a1f2-e62325020c3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ece33229-ce40-4174-8cb8-e21c2ec2035a",
                    "LayerId": "42d39d6a-a0c2-49dc-a5f5-cb1607f2c3f6"
                }
            ]
        },
        {
            "id": "0b82eaa7-1f21-41fe-a8af-4e949b39aaf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "113839ec-ac2f-4e78-9952-13720a4dcc59",
            "compositeImage": {
                "id": "719ba078-db54-4569-a9d6-8423b726c947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b82eaa7-1f21-41fe-a8af-4e949b39aaf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a30d660-8b95-4528-88c6-3ef767cd9089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b82eaa7-1f21-41fe-a8af-4e949b39aaf7",
                    "LayerId": "42d39d6a-a0c2-49dc-a5f5-cb1607f2c3f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "42d39d6a-a0c2-49dc-a5f5-cb1607f2c3f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "113839ec-ac2f-4e78-9952-13720a4dcc59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}