{
    "id": "6f2c2627-6f2d-486d-9c02-401b174ab289",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light_128",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5469695d-40b8-4f6d-b6d6-4710530e1d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f2c2627-6f2d-486d-9c02-401b174ab289",
            "compositeImage": {
                "id": "bfc800bd-766d-4256-b367-074eabb91a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5469695d-40b8-4f6d-b6d6-4710530e1d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d26686-ee82-459b-8697-b26644699c9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5469695d-40b8-4f6d-b6d6-4710530e1d75",
                    "LayerId": "a3dd4d4f-bf38-47a0-bbec-f2c800e9489c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a3dd4d4f-bf38-47a0-bbec-f2c800e9489c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f2c2627-6f2d-486d-9c02-401b174ab289",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}