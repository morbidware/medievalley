{
    "id": "1d8f342f-059f-4529-bfa2-4b40998d76b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b678ec7c-10f5-4ef2-af57-da2b32e420f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8f342f-059f-4529-bfa2-4b40998d76b4",
            "compositeImage": {
                "id": "928d824f-99e6-4764-a1ef-c73fbc9a7d0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b678ec7c-10f5-4ef2-af57-da2b32e420f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a51b621-b3b1-4491-9733-ed7c3d753e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b678ec7c-10f5-4ef2-af57-da2b32e420f3",
                    "LayerId": "55af8bbe-2bd5-40d8-8441-11302d100f10"
                }
            ]
        },
        {
            "id": "cb0f2f46-62bb-4fda-99ec-079de0126b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8f342f-059f-4529-bfa2-4b40998d76b4",
            "compositeImage": {
                "id": "8248757e-4a5d-43a8-bc91-81446b71e2ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb0f2f46-62bb-4fda-99ec-079de0126b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fae64b-f6d3-4bb2-9952-13b45830fe8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb0f2f46-62bb-4fda-99ec-079de0126b60",
                    "LayerId": "55af8bbe-2bd5-40d8-8441-11302d100f10"
                }
            ]
        },
        {
            "id": "0fe895cd-edaa-4c89-994e-e2d4f9696aff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8f342f-059f-4529-bfa2-4b40998d76b4",
            "compositeImage": {
                "id": "11e31c59-6a17-4057-ac57-2bbac82cc8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe895cd-edaa-4c89-994e-e2d4f9696aff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45bda126-1ae7-42c6-bd85-db41ce32267b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe895cd-edaa-4c89-994e-e2d4f9696aff",
                    "LayerId": "55af8bbe-2bd5-40d8-8441-11302d100f10"
                }
            ]
        },
        {
            "id": "3dec6326-82eb-4cec-929c-0ef95db223ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8f342f-059f-4529-bfa2-4b40998d76b4",
            "compositeImage": {
                "id": "04f48b00-3d23-4dd1-9564-9d7385bbd3c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dec6326-82eb-4cec-929c-0ef95db223ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0342157-2acf-4825-9068-70a184b63606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dec6326-82eb-4cec-929c-0ef95db223ec",
                    "LayerId": "55af8bbe-2bd5-40d8-8441-11302d100f10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55af8bbe-2bd5-40d8-8441-11302d100f10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d8f342f-059f-4529-bfa2-4b40998d76b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}