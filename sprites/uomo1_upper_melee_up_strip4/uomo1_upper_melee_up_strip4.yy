{
    "id": "b74980d7-e7bc-4969-939a-120f6fb515ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a3f3b81-ec6f-4ce6-a48a-c6b68e1d4aa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74980d7-e7bc-4969-939a-120f6fb515ae",
            "compositeImage": {
                "id": "d8d2d9e1-cd22-4c3f-a030-621702c90487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3f3b81-ec6f-4ce6-a48a-c6b68e1d4aa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24a834a3-ea82-41f4-9346-5b1285b725ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3f3b81-ec6f-4ce6-a48a-c6b68e1d4aa1",
                    "LayerId": "61570edb-8786-4c21-b2d2-bda6b0807b54"
                }
            ]
        },
        {
            "id": "105fadd8-59df-4050-8128-b2425d2a1d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74980d7-e7bc-4969-939a-120f6fb515ae",
            "compositeImage": {
                "id": "35fae3b3-275d-42d0-955d-b542d182111e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "105fadd8-59df-4050-8128-b2425d2a1d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a1a2c38-f449-4645-bd33-abae01c12909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "105fadd8-59df-4050-8128-b2425d2a1d56",
                    "LayerId": "61570edb-8786-4c21-b2d2-bda6b0807b54"
                }
            ]
        },
        {
            "id": "76a02a71-dc20-45c0-b02a-4f51fc1f2ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74980d7-e7bc-4969-939a-120f6fb515ae",
            "compositeImage": {
                "id": "7e26ef48-51e7-403c-bb89-170fd1999b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a02a71-dc20-45c0-b02a-4f51fc1f2ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9674330-e09d-4422-baa8-ade1c813da28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a02a71-dc20-45c0-b02a-4f51fc1f2ed6",
                    "LayerId": "61570edb-8786-4c21-b2d2-bda6b0807b54"
                }
            ]
        },
        {
            "id": "d46642d6-f850-43d9-b17f-66c1390325a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74980d7-e7bc-4969-939a-120f6fb515ae",
            "compositeImage": {
                "id": "132eaa06-9ac4-4acf-9e1f-e10ecadc74e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46642d6-f850-43d9-b17f-66c1390325a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d40a7ded-bf7c-4864-806b-e1ed16e878a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46642d6-f850-43d9-b17f-66c1390325a2",
                    "LayerId": "61570edb-8786-4c21-b2d2-bda6b0807b54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "61570edb-8786-4c21-b2d2-bda6b0807b54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b74980d7-e7bc-4969-939a-120f6fb515ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}