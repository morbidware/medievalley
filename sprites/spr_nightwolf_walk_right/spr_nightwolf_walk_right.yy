{
    "id": "c32d223d-6799-426b-b57c-27ad45e7b24f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 8,
    "bbox_right": 47,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f598be5-ba71-42a5-95a7-8fe29fe406b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "compositeImage": {
                "id": "6723fa30-ae30-486a-9316-0d08cefec8ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f598be5-ba71-42a5-95a7-8fe29fe406b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac884fb-5ee1-4c66-9049-bdfaff8d48d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f598be5-ba71-42a5-95a7-8fe29fe406b0",
                    "LayerId": "8a9df173-430d-4914-9f93-cca2dd543b6c"
                }
            ]
        },
        {
            "id": "3b7bd464-5e86-47c1-b49a-c83d2c0f44e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "compositeImage": {
                "id": "b5b9f03f-0e49-4298-a27e-d414057a873f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7bd464-5e86-47c1-b49a-c83d2c0f44e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2056deb3-8eac-4557-8050-dd09915de9e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7bd464-5e86-47c1-b49a-c83d2c0f44e4",
                    "LayerId": "8a9df173-430d-4914-9f93-cca2dd543b6c"
                }
            ]
        },
        {
            "id": "f944d24f-33d0-46d3-8a89-8027492f2855",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "compositeImage": {
                "id": "ba2adcfd-dc48-4607-bfb0-b0707864a150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f944d24f-33d0-46d3-8a89-8027492f2855",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29057cfb-b6c2-4797-a4b8-0bd5fa7ab0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f944d24f-33d0-46d3-8a89-8027492f2855",
                    "LayerId": "8a9df173-430d-4914-9f93-cca2dd543b6c"
                }
            ]
        },
        {
            "id": "bbc527a5-69b7-4c7e-b87b-5b099c09ba5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "compositeImage": {
                "id": "e9e8b426-5c3a-4f81-b0ad-6e5a501d4c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc527a5-69b7-4c7e-b87b-5b099c09ba5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "558114b6-ff44-4161-839a-b2e07771f764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc527a5-69b7-4c7e-b87b-5b099c09ba5a",
                    "LayerId": "8a9df173-430d-4914-9f93-cca2dd543b6c"
                }
            ]
        },
        {
            "id": "b512499d-9eb0-4570-a877-ab69be39706f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "compositeImage": {
                "id": "51cb90d0-2e2d-403f-826e-ca2e0f08ce3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b512499d-9eb0-4570-a877-ab69be39706f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63ff3cad-59e8-4489-bd54-eed122c8641a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b512499d-9eb0-4570-a877-ab69be39706f",
                    "LayerId": "8a9df173-430d-4914-9f93-cca2dd543b6c"
                }
            ]
        },
        {
            "id": "21865852-c39b-48c6-b10f-6d80a9633510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "compositeImage": {
                "id": "1d014f5b-5096-4495-ac79-a7379355c0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21865852-c39b-48c6-b10f-6d80a9633510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86926cc2-5a3d-4fa7-a1ea-b957c5f88220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21865852-c39b-48c6-b10f-6d80a9633510",
                    "LayerId": "8a9df173-430d-4914-9f93-cca2dd543b6c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "8a9df173-430d-4914-9f93-cca2dd543b6c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c32d223d-6799-426b-b57c-27ad45e7b24f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}