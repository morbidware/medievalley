{
    "id": "0b0a4b7c-ddd0-4a86-bccf-d022c03b7482",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_blueberry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c04a1f6c-51c7-4b53-9868-a790d5b07f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0a4b7c-ddd0-4a86-bccf-d022c03b7482",
            "compositeImage": {
                "id": "6a1898ae-b2bc-4af8-99b7-973a2b157578",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c04a1f6c-51c7-4b53-9868-a790d5b07f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "402b9808-2dd8-47bf-bfaa-a30ce91302bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c04a1f6c-51c7-4b53-9868-a790d5b07f7e",
                    "LayerId": "13eaeb2f-f1d3-4a00-97a0-bf365287e196"
                }
            ]
        },
        {
            "id": "549e4744-be61-4a96-ac50-9f2ac830dd9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b0a4b7c-ddd0-4a86-bccf-d022c03b7482",
            "compositeImage": {
                "id": "9142e865-f789-4b96-903f-dd1f0c85286c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "549e4744-be61-4a96-ac50-9f2ac830dd9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fb731f-33e7-43a6-a421-c0135b0868dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "549e4744-be61-4a96-ac50-9f2ac830dd9e",
                    "LayerId": "13eaeb2f-f1d3-4a00-97a0-bf365287e196"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13eaeb2f-f1d3-4a00-97a0-bf365287e196",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b0a4b7c-ddd0-4a86-bccf-d022c03b7482",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}