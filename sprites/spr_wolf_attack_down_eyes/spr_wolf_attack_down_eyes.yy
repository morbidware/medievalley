{
    "id": "10e6162e-c904-45ab-95c2-861154c558c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_attack_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 22,
    "bbox_right": 27,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b817b51-ada5-47ff-a689-c94f191d8a64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e6162e-c904-45ab-95c2-861154c558c1",
            "compositeImage": {
                "id": "36cb4f3d-2351-4184-bbd8-b20349010245",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b817b51-ada5-47ff-a689-c94f191d8a64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d044cc-000f-46d2-a5cc-e4e83605d141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b817b51-ada5-47ff-a689-c94f191d8a64",
                    "LayerId": "50c8516a-bbf3-41e6-84ce-4362faa5b005"
                }
            ]
        },
        {
            "id": "0c5cdec9-156f-497d-9bbf-42f5a6dd1357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e6162e-c904-45ab-95c2-861154c558c1",
            "compositeImage": {
                "id": "df866b08-50c7-4869-ad26-c792d324275b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c5cdec9-156f-497d-9bbf-42f5a6dd1357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f5de39c-c5b3-4473-a139-1571a66cd5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c5cdec9-156f-497d-9bbf-42f5a6dd1357",
                    "LayerId": "50c8516a-bbf3-41e6-84ce-4362faa5b005"
                }
            ]
        },
        {
            "id": "48d6e6bb-e452-42ad-ad6b-99ae04646875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e6162e-c904-45ab-95c2-861154c558c1",
            "compositeImage": {
                "id": "dd2fde81-c4dc-4012-ae87-b1dd44ccd4c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d6e6bb-e452-42ad-ad6b-99ae04646875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c8b1a1-c924-4d1b-ab54-7a99b051ba23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d6e6bb-e452-42ad-ad6b-99ae04646875",
                    "LayerId": "50c8516a-bbf3-41e6-84ce-4362faa5b005"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "50c8516a-bbf3-41e6-84ce-4362faa5b005",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e6162e-c904-45ab-95c2-861154c558c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}