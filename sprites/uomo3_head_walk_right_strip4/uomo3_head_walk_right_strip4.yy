{
    "id": "05aeed48-bdb3-4639-b620-f1770412b0df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f547a1d-6a82-4dc8-a146-d2320178309f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05aeed48-bdb3-4639-b620-f1770412b0df",
            "compositeImage": {
                "id": "bdf0cbe1-eb2e-4267-ac44-8ff935d664a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f547a1d-6a82-4dc8-a146-d2320178309f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ceb558-9693-4207-b4ea-d9b5e2d8f18b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f547a1d-6a82-4dc8-a146-d2320178309f",
                    "LayerId": "a901cc56-7faf-45ec-ada8-2a16fd78d2fe"
                }
            ]
        },
        {
            "id": "fcd5ae0e-3b1f-4c27-9618-c3bfdc91c439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05aeed48-bdb3-4639-b620-f1770412b0df",
            "compositeImage": {
                "id": "965f4a17-5e0a-4d77-8ed8-0fb97e74af68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd5ae0e-3b1f-4c27-9618-c3bfdc91c439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e80edabc-bf0c-47df-8a2d-1999baa5a0af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd5ae0e-3b1f-4c27-9618-c3bfdc91c439",
                    "LayerId": "a901cc56-7faf-45ec-ada8-2a16fd78d2fe"
                }
            ]
        },
        {
            "id": "d64e1bbb-2644-4f12-b4a6-b8f09c79a291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05aeed48-bdb3-4639-b620-f1770412b0df",
            "compositeImage": {
                "id": "85a22f30-4c2d-4593-a9a6-a3159f546956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d64e1bbb-2644-4f12-b4a6-b8f09c79a291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1e942f-3c08-46f3-a5d9-248b04ddd9f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d64e1bbb-2644-4f12-b4a6-b8f09c79a291",
                    "LayerId": "a901cc56-7faf-45ec-ada8-2a16fd78d2fe"
                }
            ]
        },
        {
            "id": "cfdd3555-e038-4f0a-83bd-a5b3453b2ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05aeed48-bdb3-4639-b620-f1770412b0df",
            "compositeImage": {
                "id": "cf255bf0-113e-4a9f-8f6d-b8d1e5c1a426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfdd3555-e038-4f0a-83bd-a5b3453b2ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f242027e-5ecb-4457-ab9b-c95cb928eb21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfdd3555-e038-4f0a-83bd-a5b3453b2ca7",
                    "LayerId": "a901cc56-7faf-45ec-ada8-2a16fd78d2fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a901cc56-7faf-45ec-ada8-2a16fd78d2fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05aeed48-bdb3-4639-b620-f1770412b0df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}