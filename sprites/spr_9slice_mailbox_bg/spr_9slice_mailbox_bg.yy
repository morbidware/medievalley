{
    "id": "d7344057-f1a5-4d11-8903-d23e51557d6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9slice_mailbox_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e0e11c2-daa5-4816-92c7-1a3890867e21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "d8bc2434-cc97-4c7e-9351-7500b70043a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e0e11c2-daa5-4816-92c7-1a3890867e21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41ccb54-948f-4505-896a-3bd97c179352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0e11c2-daa5-4816-92c7-1a3890867e21",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "095712ec-939a-4382-8c73-e1eeb452a6ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "deb056a0-555e-414f-bdc6-49572b2815fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095712ec-939a-4382-8c73-e1eeb452a6ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcc16a2b-897a-4aae-9db8-5ab81bbdac7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095712ec-939a-4382-8c73-e1eeb452a6ce",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "ab92ea07-f98e-4b80-ad7e-759b34f72844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "fc8e4117-7ffd-4979-8e91-56c98db4490e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab92ea07-f98e-4b80-ad7e-759b34f72844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3a95f3-0c37-430c-90a1-0c8e937d924c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab92ea07-f98e-4b80-ad7e-759b34f72844",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "c9023a75-6677-4b74-bf66-596e22d0a28b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "34d91c8c-b577-4b58-87e2-65b807d41626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9023a75-6677-4b74-bf66-596e22d0a28b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d854d5f-81f4-48db-9ac1-4b44b2ddf4ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9023a75-6677-4b74-bf66-596e22d0a28b",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "d19614aa-59e1-4695-ad06-bdf5defc51c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "5ce94829-5a5e-4e9d-a926-bd44e7384b94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d19614aa-59e1-4695-ad06-bdf5defc51c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aae54ee-a92d-4832-9d53-3c23652b5fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d19614aa-59e1-4695-ad06-bdf5defc51c4",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "c50ec908-0c92-4c73-b636-40a7bfa831ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "8d213c94-933b-47fa-a0a0-1197875effdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50ec908-0c92-4c73-b636-40a7bfa831ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80d19f7c-4a3c-41d2-8236-a96a044bf365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50ec908-0c92-4c73-b636-40a7bfa831ae",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "3b4bbc9c-efc7-4a50-99b0-cec666aafb2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "18ee1f31-0f14-4298-b275-a2313bc92cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4bbc9c-efc7-4a50-99b0-cec666aafb2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4f57815-dc9d-4146-81ca-6c8b5e30780b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4bbc9c-efc7-4a50-99b0-cec666aafb2b",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "bffaa733-0a17-4864-b77f-d31d97ea1615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "d8bb74a0-981e-494c-be7f-a24bc2da0015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bffaa733-0a17-4864-b77f-d31d97ea1615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09f34cb-a719-40a8-8de1-bea70ed8cf3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bffaa733-0a17-4864-b77f-d31d97ea1615",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        },
        {
            "id": "1599f3e4-08ef-4259-b0ab-0b193cf47107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "compositeImage": {
                "id": "9e7eacda-8847-4774-9c49-5dbe91d6ac6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1599f3e4-08ef-4259-b0ab-0b193cf47107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e609070c-b077-42a5-a9b5-5209332a9293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1599f3e4-08ef-4259-b0ab-0b193cf47107",
                    "LayerId": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5f59fdee-76d9-48af-a1ee-66e0e3af8d8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7344057-f1a5-4d11-8903-d23e51557d6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}