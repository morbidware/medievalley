{
    "id": "3afb914e-9364-47e4-9647-ef36dad41777",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26140836-8134-40c1-9723-9bf7669be356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3afb914e-9364-47e4-9647-ef36dad41777",
            "compositeImage": {
                "id": "5578df36-cb56-42fe-9a7b-619641e6b464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26140836-8134-40c1-9723-9bf7669be356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d589710-9d97-4f9b-8c81-6ee9caa5b4a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26140836-8134-40c1-9723-9bf7669be356",
                    "LayerId": "5c5be863-373f-4b23-94b0-e94066cea6e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5c5be863-373f-4b23-94b0-e94066cea6e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3afb914e-9364-47e4-9647-ef36dad41777",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}