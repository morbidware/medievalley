{
    "id": "3889565e-2e6b-42cc-8bda-7a9f94004946",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_redberry_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28219462-1e3d-4f5a-8abf-6fd25cbe91e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3889565e-2e6b-42cc-8bda-7a9f94004946",
            "compositeImage": {
                "id": "a75dfa8d-7718-4a9d-b182-a68e8d1bec6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28219462-1e3d-4f5a-8abf-6fd25cbe91e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9fbd3d8-1209-4426-801d-166678d25a30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28219462-1e3d-4f5a-8abf-6fd25cbe91e5",
                    "LayerId": "b2d942ee-44c3-4ada-88de-5e0754348dc7"
                }
            ]
        },
        {
            "id": "f531699e-cf2f-4610-89c3-d6d1dfa404e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3889565e-2e6b-42cc-8bda-7a9f94004946",
            "compositeImage": {
                "id": "ca42dc0c-a19b-4313-b85f-e0c9fa87fed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f531699e-cf2f-4610-89c3-d6d1dfa404e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed8df5c-5b56-49f7-a23b-090c268871b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f531699e-cf2f-4610-89c3-d6d1dfa404e3",
                    "LayerId": "b2d942ee-44c3-4ada-88de-5e0754348dc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2d942ee-44c3-4ada-88de-5e0754348dc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3889565e-2e6b-42cc-8bda-7a9f94004946",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 14,
    "yorig": 29
}