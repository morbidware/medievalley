{
    "id": "3a6acbe8-0f7d-49f2-8b69-924d6a7079e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7049941c-07ce-45b1-88c0-759f33c48cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a6acbe8-0f7d-49f2-8b69-924d6a7079e9",
            "compositeImage": {
                "id": "f32e1df1-d123-402c-8686-8a5b3a162fed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7049941c-07ce-45b1-88c0-759f33c48cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b206b3d8-6fae-4caf-9047-21c9f4065a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7049941c-07ce-45b1-88c0-759f33c48cb0",
                    "LayerId": "1fe4b94c-57b0-471d-a54d-a73125fd24cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1fe4b94c-57b0-471d-a54d-a73125fd24cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a6acbe8-0f7d-49f2-8b69-924d6a7079e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}