{
    "id": "b0ee6bfd-9648-4043-9bae-c6f35ac8ffcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "593a42bd-66d7-4745-8d2f-52c41b9b4b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0ee6bfd-9648-4043-9bae-c6f35ac8ffcf",
            "compositeImage": {
                "id": "7e2eaadb-ef91-4226-80de-50caddf2b71d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "593a42bd-66d7-4745-8d2f-52c41b9b4b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa5fc7b-18cd-4248-af82-09ebb6be80a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "593a42bd-66d7-4745-8d2f-52c41b9b4b48",
                    "LayerId": "49ce545a-7c50-4da8-a930-89192802c714"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "49ce545a-7c50-4da8-a930-89192802c714",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0ee6bfd-9648-4043-9bae-c6f35ac8ffcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}