{
    "id": "852e4460-c7f3-442c-8478-961d6d42a1dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe8f59ee-6047-4fc6-8b70-b66a2dc09a1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852e4460-c7f3-442c-8478-961d6d42a1dd",
            "compositeImage": {
                "id": "33f5d83d-f405-4e74-a190-54f539eab2a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8f59ee-6047-4fc6-8b70-b66a2dc09a1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0f29fc1-e93d-4128-99ff-b4f6a45f6d58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8f59ee-6047-4fc6-8b70-b66a2dc09a1b",
                    "LayerId": "fcaed00b-405e-4986-9bef-59096cc4e324"
                }
            ]
        },
        {
            "id": "2343c597-3ce8-489f-b0d4-4d93f9890b7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852e4460-c7f3-442c-8478-961d6d42a1dd",
            "compositeImage": {
                "id": "6aebedc7-23e1-4219-b682-13a85cae049d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2343c597-3ce8-489f-b0d4-4d93f9890b7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a12e07f-d40c-48d3-ac31-2a07753b050e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2343c597-3ce8-489f-b0d4-4d93f9890b7a",
                    "LayerId": "fcaed00b-405e-4986-9bef-59096cc4e324"
                }
            ]
        },
        {
            "id": "5808466b-d6d3-40b7-858d-f194992d97ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852e4460-c7f3-442c-8478-961d6d42a1dd",
            "compositeImage": {
                "id": "98a4ee28-4788-4e0c-b812-2e761dde2ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5808466b-d6d3-40b7-858d-f194992d97ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d4acb5c-d8c8-47a3-97fb-1796278f800a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5808466b-d6d3-40b7-858d-f194992d97ce",
                    "LayerId": "fcaed00b-405e-4986-9bef-59096cc4e324"
                }
            ]
        },
        {
            "id": "548f23b1-9f89-4e5f-a40b-7449565a9053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "852e4460-c7f3-442c-8478-961d6d42a1dd",
            "compositeImage": {
                "id": "cd9f35df-44fb-4d67-84ed-e29d0c781ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "548f23b1-9f89-4e5f-a40b-7449565a9053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241de587-511f-41bc-89a5-dd69a1b5b5d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "548f23b1-9f89-4e5f-a40b-7449565a9053",
                    "LayerId": "fcaed00b-405e-4986-9bef-59096cc4e324"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fcaed00b-405e-4986-9bef-59096cc4e324",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "852e4460-c7f3-442c-8478-961d6d42a1dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}