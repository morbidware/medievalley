{
    "id": "9c00105f-30c3-41e0-940b-11fd31b47506",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28d1cb87-36cc-484b-9d01-7896ae946d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c00105f-30c3-41e0-940b-11fd31b47506",
            "compositeImage": {
                "id": "4e99c939-eaea-44b2-925e-d65b94d7e189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28d1cb87-36cc-484b-9d01-7896ae946d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d25ebbed-4832-4951-83cc-37361c0bc275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28d1cb87-36cc-484b-9d01-7896ae946d78",
                    "LayerId": "d6a92453-0b7e-4d1a-a481-61ccc1d4441f"
                }
            ]
        },
        {
            "id": "a7f25a30-773d-4a11-8a41-3bac75f10461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c00105f-30c3-41e0-940b-11fd31b47506",
            "compositeImage": {
                "id": "d0502847-9397-4535-acb7-0d24783f4a59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7f25a30-773d-4a11-8a41-3bac75f10461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049ad4fa-c5f0-413d-b952-ac56f30a4d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7f25a30-773d-4a11-8a41-3bac75f10461",
                    "LayerId": "d6a92453-0b7e-4d1a-a481-61ccc1d4441f"
                }
            ]
        },
        {
            "id": "e86d18e7-5b84-43d4-876d-bcf68e9ea76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c00105f-30c3-41e0-940b-11fd31b47506",
            "compositeImage": {
                "id": "41dcbf99-82b8-4e6a-979e-65dea25d8961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e86d18e7-5b84-43d4-876d-bcf68e9ea76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2fce97a-8d3d-49d4-83be-dd4a46dbfb46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e86d18e7-5b84-43d4-876d-bcf68e9ea76f",
                    "LayerId": "d6a92453-0b7e-4d1a-a481-61ccc1d4441f"
                }
            ]
        },
        {
            "id": "398ebd43-cf92-4be5-aab1-76086df396f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c00105f-30c3-41e0-940b-11fd31b47506",
            "compositeImage": {
                "id": "eaeab947-a931-45ed-9009-6f06c5472d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "398ebd43-cf92-4be5-aab1-76086df396f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22cfdb1e-79bf-4f42-a573-116d69f0427c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "398ebd43-cf92-4be5-aab1-76086df396f8",
                    "LayerId": "d6a92453-0b7e-4d1a-a481-61ccc1d4441f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6a92453-0b7e-4d1a-a481-61ccc1d4441f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c00105f-30c3-41e0-940b-11fd31b47506",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}