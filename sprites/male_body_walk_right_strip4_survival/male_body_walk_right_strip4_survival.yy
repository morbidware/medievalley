{
    "id": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_walk_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6af1f5f-2817-43eb-b457-db76c4a8553b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "compositeImage": {
                "id": "74848096-8233-45de-bcca-9e8a559a8b89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6af1f5f-2817-43eb-b457-db76c4a8553b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90038649-c9a8-4fb3-9964-b122a374761a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6af1f5f-2817-43eb-b457-db76c4a8553b",
                    "LayerId": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4"
                }
            ]
        },
        {
            "id": "93acfea1-3213-4944-a772-bddbc4b67f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "compositeImage": {
                "id": "9328e192-7592-4c5a-b3f4-9bddf403841f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93acfea1-3213-4944-a772-bddbc4b67f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c473d9e9-2f7e-4694-97d4-b3600d6367fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93acfea1-3213-4944-a772-bddbc4b67f8c",
                    "LayerId": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4"
                }
            ]
        },
        {
            "id": "bda2df93-122c-444d-a4dd-f8c0419fee10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "compositeImage": {
                "id": "e8ed5bc7-0420-42e2-9d79-6682e82d522f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bda2df93-122c-444d-a4dd-f8c0419fee10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8401aef0-71a1-4d70-9bf1-98f7b447344c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bda2df93-122c-444d-a4dd-f8c0419fee10",
                    "LayerId": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4"
                }
            ]
        },
        {
            "id": "1aa8bd01-cb2c-47b1-8a08-e0a493e487f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "compositeImage": {
                "id": "d4fde858-fe45-46ff-a67c-bd232f74d2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa8bd01-cb2c-47b1-8a08-e0a493e487f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5efc337d-a52f-4ceb-a4be-63590f6c4eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa8bd01-cb2c-47b1-8a08-e0a493e487f2",
                    "LayerId": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4"
                }
            ]
        },
        {
            "id": "93385b97-a5cc-46f1-adaa-af971c63edf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "compositeImage": {
                "id": "cc77f79b-acce-4864-831d-acee3c2ab639",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93385b97-a5cc-46f1-adaa-af971c63edf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9fad17-379d-4b22-9043-8ceaf124f11d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93385b97-a5cc-46f1-adaa-af971c63edf8",
                    "LayerId": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4"
                }
            ]
        },
        {
            "id": "823ad6e5-edd2-4d9a-a235-67667205b928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "compositeImage": {
                "id": "3fe5b9f6-abf4-4e9d-8438-6e7a358defce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823ad6e5-edd2-4d9a-a235-67667205b928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7e82c8-7127-49ae-a553-befd09d0a98f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823ad6e5-edd2-4d9a-a235-67667205b928",
                    "LayerId": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f54e4bc2-3e4c-4582-8d47-a96ff7dd66a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35c5138a-7a96-4ef2-a0aa-3088a7432005",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}