{
    "id": "1d903e43-6f3c-44f5-83d7-21db52983f06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_melee_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f355398a-dd4c-4c1b-8215-359b4971ca0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "compositeImage": {
                "id": "f0b3ace6-35ce-442b-8099-b913df06c8a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f355398a-dd4c-4c1b-8215-359b4971ca0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb6c290-f093-4867-9feb-bffd1b7c69b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f355398a-dd4c-4c1b-8215-359b4971ca0c",
                    "LayerId": "31623e8c-4862-4ae4-8120-e270b3efa1ad"
                }
            ]
        },
        {
            "id": "39c97aca-904d-4fcd-9607-353898df34ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "compositeImage": {
                "id": "710527af-2cff-4bc1-b05a-93bb4aa03d45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c97aca-904d-4fcd-9607-353898df34ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "007a600f-38ee-446c-94a6-b8db5d7c308c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c97aca-904d-4fcd-9607-353898df34ae",
                    "LayerId": "31623e8c-4862-4ae4-8120-e270b3efa1ad"
                }
            ]
        },
        {
            "id": "1463ce8c-3bf6-4b41-b256-94a646358a12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "compositeImage": {
                "id": "a1a48925-6f86-4f1b-b677-75baa6c9126a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1463ce8c-3bf6-4b41-b256-94a646358a12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec7870ac-8acc-4c6a-b3c7-5bc1c397ae52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1463ce8c-3bf6-4b41-b256-94a646358a12",
                    "LayerId": "31623e8c-4862-4ae4-8120-e270b3efa1ad"
                }
            ]
        },
        {
            "id": "cbafa064-8538-4424-a1d4-85109ee5b4f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "compositeImage": {
                "id": "2ce53dbe-bfd2-4ca1-ace9-0fdc130eaa70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbafa064-8538-4424-a1d4-85109ee5b4f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfbc7024-e41d-4b47-8be8-2920ddb571e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbafa064-8538-4424-a1d4-85109ee5b4f2",
                    "LayerId": "31623e8c-4862-4ae4-8120-e270b3efa1ad"
                }
            ]
        },
        {
            "id": "b1a08200-d7ce-493c-9aa6-83e3ba54f0bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "compositeImage": {
                "id": "d32f13f3-5fa1-4720-a53a-512d0fc7569d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1a08200-d7ce-493c-9aa6-83e3ba54f0bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e577b994-68d4-4be5-8f17-fab9df322607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1a08200-d7ce-493c-9aa6-83e3ba54f0bb",
                    "LayerId": "31623e8c-4862-4ae4-8120-e270b3efa1ad"
                }
            ]
        },
        {
            "id": "7e470839-716e-4bde-b38a-4e3f3e73b65e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "compositeImage": {
                "id": "58c3730b-11fc-464e-8532-39b6ce4dc0f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e470839-716e-4bde-b38a-4e3f3e73b65e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1379fe4b-6e9e-4a06-a5a4-50d52affe937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e470839-716e-4bde-b38a-4e3f3e73b65e",
                    "LayerId": "31623e8c-4862-4ae4-8120-e270b3efa1ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "31623e8c-4862-4ae4-8120-e270b3efa1ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d903e43-6f3c-44f5-83d7-21db52983f06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}