{
    "id": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_melee_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eef3c604-798a-4c8c-b62f-25dedd41863f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "compositeImage": {
                "id": "4f3da6a0-6bae-4c96-8bf9-9eb83051163c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef3c604-798a-4c8c-b62f-25dedd41863f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7673b069-8ae6-4d7e-8918-e0b367cf186d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef3c604-798a-4c8c-b62f-25dedd41863f",
                    "LayerId": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb"
                }
            ]
        },
        {
            "id": "48ac0818-25c3-47b1-b742-1102b91820cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "compositeImage": {
                "id": "3e91aa60-671c-4b8c-89c6-a5d16f0f8776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ac0818-25c3-47b1-b742-1102b91820cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e3e0058-940b-4dcd-9236-d29845e08e4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ac0818-25c3-47b1-b742-1102b91820cc",
                    "LayerId": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb"
                }
            ]
        },
        {
            "id": "8e50dd1d-dd87-4884-9ca3-a9ce46393574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "compositeImage": {
                "id": "2f30982d-4f47-41fc-b48c-8fcc38933070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e50dd1d-dd87-4884-9ca3-a9ce46393574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88b6d022-9430-42ba-bcea-a3e7c559879d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e50dd1d-dd87-4884-9ca3-a9ce46393574",
                    "LayerId": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb"
                }
            ]
        },
        {
            "id": "1ffa9daf-f534-48da-bcb9-58246f9bc13b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "compositeImage": {
                "id": "16e424e2-0157-41c4-9cd2-8b2e13999562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ffa9daf-f534-48da-bcb9-58246f9bc13b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c9d189-a584-4dbb-bbf6-f4c4947bc4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ffa9daf-f534-48da-bcb9-58246f9bc13b",
                    "LayerId": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb"
                }
            ]
        },
        {
            "id": "46194acc-e818-4465-ac0e-688848524d16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "compositeImage": {
                "id": "f4413643-0888-4186-a4b6-5181bb739053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46194acc-e818-4465-ac0e-688848524d16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce7385d-5002-4a6f-b564-26f855c02886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46194acc-e818-4465-ac0e-688848524d16",
                    "LayerId": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb"
                }
            ]
        },
        {
            "id": "492d33e9-4972-402a-a852-c870de2d8231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "compositeImage": {
                "id": "23b56be0-8ada-4ba6-aa9a-0680bbc503cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492d33e9-4972-402a-a852-c870de2d8231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49da65b-0c64-432a-ba18-cfb1d9f2ce6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492d33e9-4972-402a-a852-c870de2d8231",
                    "LayerId": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a5d982a1-9c49-4287-aa8c-d55d5d6ff2fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e3bbb9f-90ed-4734-a138-04ce5f4062c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}