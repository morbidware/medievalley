{
    "id": "b6d2d212-9112-46ab-ac96-0a284339bebe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_watering_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "619ae1d7-561d-47fd-98e8-b783ad982c2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "compositeImage": {
                "id": "06254ca1-2872-46cd-92cb-774bb25d6956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "619ae1d7-561d-47fd-98e8-b783ad982c2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbcb6a3-1285-4d8a-b5d4-452450e94b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "619ae1d7-561d-47fd-98e8-b783ad982c2d",
                    "LayerId": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b"
                }
            ]
        },
        {
            "id": "63c7aa66-2e19-49fd-8df0-8ab45059c2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "compositeImage": {
                "id": "6ce1c82e-cb9d-41be-b642-f89fe14eb8ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c7aa66-2e19-49fd-8df0-8ab45059c2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "212c7bf9-aae7-4699-abac-144bf400ecac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c7aa66-2e19-49fd-8df0-8ab45059c2bd",
                    "LayerId": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b"
                }
            ]
        },
        {
            "id": "f58b1c7d-0746-444c-8f1d-2d6d70d6c523",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "compositeImage": {
                "id": "95bda377-3259-449e-97bf-ebe774201314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f58b1c7d-0746-444c-8f1d-2d6d70d6c523",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1df20be2-7cd2-4102-853a-25a1f53a06fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f58b1c7d-0746-444c-8f1d-2d6d70d6c523",
                    "LayerId": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b"
                }
            ]
        },
        {
            "id": "e8ca5345-127c-4a15-8826-54eb6311a1ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "compositeImage": {
                "id": "21a70155-9bd9-4bea-9daf-ff60646090c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ca5345-127c-4a15-8826-54eb6311a1ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb24d375-2c3d-44a0-b851-8dac09a8cf8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ca5345-127c-4a15-8826-54eb6311a1ed",
                    "LayerId": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b"
                }
            ]
        },
        {
            "id": "2f3a5384-02f1-4742-816d-8f4c3843e637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "compositeImage": {
                "id": "47e92784-7216-4612-81eb-7fd8679ada78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3a5384-02f1-4742-816d-8f4c3843e637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8876b018-dd7a-4329-9a2b-55ac0f3a227c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3a5384-02f1-4742-816d-8f4c3843e637",
                    "LayerId": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b"
                }
            ]
        },
        {
            "id": "73f76f66-7b60-4ba8-8d80-f09d6d385755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "compositeImage": {
                "id": "dd4cbcb2-4395-41f0-9616-5d4bc7ff79ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f76f66-7b60-4ba8-8d80-f09d6d385755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef696be-2dad-4fb3-8c8e-2b535dd788a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f76f66-7b60-4ba8-8d80-f09d6d385755",
                    "LayerId": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d3a8ae10-23c1-4f4a-b0eb-0187470cfa3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6d2d212-9112-46ab-ac96-0a284339bebe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}