{
    "id": "3b4d3bb6-b5e3-4f23-9109-768165b25e74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f7e4dbf-0ed4-4ff3-a269-e2dc735650cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4d3bb6-b5e3-4f23-9109-768165b25e74",
            "compositeImage": {
                "id": "ef81fd67-c162-495c-8385-ae105c1dd4d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7e4dbf-0ed4-4ff3-a269-e2dc735650cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944b90c8-2653-4254-a599-1cad03670c12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7e4dbf-0ed4-4ff3-a269-e2dc735650cd",
                    "LayerId": "6f175fe4-5858-455d-bf70-399c8e1a3bd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "6f175fe4-5858-455d-bf70-399c8e1a3bd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b4d3bb6-b5e3-4f23-9109-768165b25e74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 9
}