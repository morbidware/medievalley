{
    "id": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_furniture",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "082283e8-6f55-4a73-989b-b306bcc6359c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
            "compositeImage": {
                "id": "cb11fe65-9dbd-4e56-9f16-f84730bc0642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082283e8-6f55-4a73-989b-b306bcc6359c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83598186-b6c0-4902-b8ed-90b11d3e5385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082283e8-6f55-4a73-989b-b306bcc6359c",
                    "LayerId": "8c532bde-0a61-49d2-8552-4c62f87543f5"
                }
            ]
        },
        {
            "id": "1ffec75a-53b1-4a32-86af-ee67f7c4e8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
            "compositeImage": {
                "id": "07b13b24-6e0a-4f42-a554-8dad24972f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ffec75a-53b1-4a32-86af-ee67f7c4e8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd52cbf0-af4d-4ed0-89d0-e3905d7684e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ffec75a-53b1-4a32-86af-ee67f7c4e8b3",
                    "LayerId": "8c532bde-0a61-49d2-8552-4c62f87543f5"
                }
            ]
        },
        {
            "id": "f522b463-53d1-4c0e-acd2-9294e3a00da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
            "compositeImage": {
                "id": "d51d9443-a492-400d-b21a-f5b31d8fdfd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f522b463-53d1-4c0e-acd2-9294e3a00da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4804bef6-34ea-4619-846f-d3002f7708c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f522b463-53d1-4c0e-acd2-9294e3a00da7",
                    "LayerId": "8c532bde-0a61-49d2-8552-4c62f87543f5"
                }
            ]
        },
        {
            "id": "ae8e79f8-1ec7-4e2d-8e45-996cb0eeb277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
            "compositeImage": {
                "id": "39829e26-ba61-4239-a915-e3310cfd12ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8e79f8-1ec7-4e2d-8e45-996cb0eeb277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc4b129-8207-4855-827c-f16c15b7e96d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8e79f8-1ec7-4e2d-8e45-996cb0eeb277",
                    "LayerId": "8c532bde-0a61-49d2-8552-4c62f87543f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "8c532bde-0a61-49d2-8552-4c62f87543f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8b28e82-656e-4af0-be9a-cacd9e1668ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}