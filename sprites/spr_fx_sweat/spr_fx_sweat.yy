{
    "id": "089ab697-0357-4617-98ca-f15626ce2649",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_sweat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea743130-e1af-4494-b68e-8edb1ee30317",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "089ab697-0357-4617-98ca-f15626ce2649",
            "compositeImage": {
                "id": "03f46129-5612-41f6-bfab-45ab3adf3b77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea743130-e1af-4494-b68e-8edb1ee30317",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe5639f6-3163-4637-9529-1680d96e0eba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea743130-e1af-4494-b68e-8edb1ee30317",
                    "LayerId": "6c54788f-389c-4a11-8402-95d0da3a2391"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "6c54788f-389c-4a11-8402-95d0da3a2391",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "089ab697-0357-4617-98ca-f15626ce2649",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}