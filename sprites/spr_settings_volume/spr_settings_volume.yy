{
    "id": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_volume",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 119,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f418c5f-8f88-4180-9253-b02a74369c6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "495ea935-af91-4878-9d80-f8ac85ff1250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f418c5f-8f88-4180-9253-b02a74369c6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c703d76-ce83-4d4e-82de-9a6f64f130bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f418c5f-8f88-4180-9253-b02a74369c6c",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "586b46fc-1a73-420e-854b-7ef4211a1ccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "9aa2d09b-a4aa-4148-ac2f-793b19e5ca2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586b46fc-1a73-420e-854b-7ef4211a1ccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75ec3950-c447-4eed-89a4-c2e277808b94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586b46fc-1a73-420e-854b-7ef4211a1ccc",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "3d6c585a-b531-4b0a-8e77-b2468a5a2ad6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "f2f17934-e7c1-41cb-bd74-d6aaae76acf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d6c585a-b531-4b0a-8e77-b2468a5a2ad6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c9f1cc-ed7d-4a7a-adcc-592ae307687a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d6c585a-b531-4b0a-8e77-b2468a5a2ad6",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "f0537f09-b2a1-426d-9fc6-8d47ebf469cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "5e61e84e-b6cf-472d-a04f-84dd9950770d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0537f09-b2a1-426d-9fc6-8d47ebf469cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2e7073f-f5d1-473e-a4e1-6d0cbc86209a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0537f09-b2a1-426d-9fc6-8d47ebf469cd",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "ca59a51c-baf0-4bcf-b3a3-7e0fef62f961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "d75969f8-2da9-4d85-b770-78b0719da58f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca59a51c-baf0-4bcf-b3a3-7e0fef62f961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bf51128-a7d4-4fd2-a542-3c9fdfc5c306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca59a51c-baf0-4bcf-b3a3-7e0fef62f961",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "c9310c41-fc31-4fb0-babc-acfc28dc45ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "d0dc2a1a-746f-424c-9882-8b0d43d8a421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9310c41-fc31-4fb0-babc-acfc28dc45ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc17e0b-ba68-4155-8db9-370e8fd6b6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9310c41-fc31-4fb0-babc-acfc28dc45ed",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "15ddc3b1-002d-490e-9718-a60c0df0a80f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "33f82212-feb6-4ba6-9a34-0f3f28731cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15ddc3b1-002d-490e-9718-a60c0df0a80f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f18d48-5b5c-4f77-923b-bdc4ab641388",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15ddc3b1-002d-490e-9718-a60c0df0a80f",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "0c680cfa-5ea2-4cee-8c57-081fc92e706d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "62a13c7c-81d9-496a-9e1e-4fb1beea5024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c680cfa-5ea2-4cee-8c57-081fc92e706d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd76abb3-fde1-4e8d-84ef-12c9161d360f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c680cfa-5ea2-4cee-8c57-081fc92e706d",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "03ae212d-6502-4c33-8cff-3ccecbede430",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "a2ab62c7-5663-4827-9eb6-27716b2f255b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ae212d-6502-4c33-8cff-3ccecbede430",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6fb74ec-ea4e-4505-9b34-89b2f65a8046",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ae212d-6502-4c33-8cff-3ccecbede430",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "f0419e5e-8288-4040-accc-ee59de71961e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "ca8dbe16-db6c-4830-a30d-7feca5061ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0419e5e-8288-4040-accc-ee59de71961e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa42bbf-6c9a-4b65-a477-52f5104993c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0419e5e-8288-4040-accc-ee59de71961e",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        },
        {
            "id": "e99922f0-ca55-4746-b452-bd40f96c4089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "compositeImage": {
                "id": "7bafa288-97a5-42e6-92bf-f68a0621011c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99922f0-ca55-4746-b452-bd40f96c4089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57417074-42d7-464a-b690-253c2b518a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99922f0-ca55-4746-b452-bd40f96c4089",
                    "LayerId": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a1bcabc6-5636-4c59-b7a6-ccec64d046f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83d4c4fe-04ab-483a-8718-2c4dc4264911",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 60,
    "yorig": 12
}