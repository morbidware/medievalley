{
    "id": "53da4ab6-b2dc-4fa8-9aff-d2fd99ca4e2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_alchemytable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89f88326-f78f-4d8d-97a1-77ff8fd3abeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53da4ab6-b2dc-4fa8-9aff-d2fd99ca4e2b",
            "compositeImage": {
                "id": "dbc3ef1f-1513-423b-80e9-274f00ae1e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f88326-f78f-4d8d-97a1-77ff8fd3abeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82777832-e445-4b05-ae95-39e7292e9bd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f88326-f78f-4d8d-97a1-77ff8fd3abeb",
                    "LayerId": "4f7af3b2-6f56-4855-a477-80028a70af59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4f7af3b2-6f56-4855-a477-80028a70af59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53da4ab6-b2dc-4fa8-9aff-d2fd99ca4e2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 13
}