{
    "id": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_xpdr_plain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea841a4b-fc23-4e75-bb3d-ed39babd4da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "fad23ae7-4cab-4a51-a677-45214cdf32c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea841a4b-fc23-4e75-bb3d-ed39babd4da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c20fbd-a643-44e9-85ac-81b919027276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea841a4b-fc23-4e75-bb3d-ed39babd4da0",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "4d79fde3-31a8-4748-84bb-3e76b176738c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "753d4861-70f9-44fe-b653-7bf5d8db4f94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d79fde3-31a8-4748-84bb-3e76b176738c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0961c18d-92c5-4341-a36c-82f86dc7a4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d79fde3-31a8-4748-84bb-3e76b176738c",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "4a6074fd-a6aa-42ba-9722-8cee37e5cd0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "e51552c4-1199-4e17-b81d-3bfd359408f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a6074fd-a6aa-42ba-9722-8cee37e5cd0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e73cf1-57fa-4dda-a1fb-d39207c22f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a6074fd-a6aa-42ba-9722-8cee37e5cd0b",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "f1c4ed93-d604-452e-b596-8530c1a6221e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "35449451-5def-40ee-8ffd-41b2a16dca3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c4ed93-d604-452e-b596-8530c1a6221e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22aeeae-bc54-400a-a4c3-c1b4b6aeb7b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c4ed93-d604-452e-b596-8530c1a6221e",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "4ef9b6ed-04fe-45f9-a6db-59f5f160d20c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "ef0bc05b-6b85-4009-9f10-05791793c0f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ef9b6ed-04fe-45f9-a6db-59f5f160d20c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2803d509-43ca-460e-b616-065ea95180c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ef9b6ed-04fe-45f9-a6db-59f5f160d20c",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "14d3fe76-a799-46bd-a032-a80152a1f799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "177176ca-5ff2-4cef-8788-aa03923c91d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d3fe76-a799-46bd-a032-a80152a1f799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1611bd45-9079-4ba4-9887-061d1f7b899b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d3fe76-a799-46bd-a032-a80152a1f799",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "110a226b-f3fc-4bc5-a001-0b8de6621a7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "467c8668-af7a-4f92-871f-581f9118cafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "110a226b-f3fc-4bc5-a001-0b8de6621a7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc323e8-e7d6-4b5a-a54e-265a2f38f29c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "110a226b-f3fc-4bc5-a001-0b8de6621a7a",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ed6cfa02-1016-49f8-871d-6f386102649a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "e2f2cab9-58a0-43d6-ac77-11b05ce7da71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6cfa02-1016-49f8-871d-6f386102649a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0f1b33-2792-469b-9cc1-e6292a0f92a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6cfa02-1016-49f8-871d-6f386102649a",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ef003f6c-e9be-4c37-a71e-aeb96d543ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "c65e72a9-1dd3-4bd8-a524-fc028f681703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef003f6c-e9be-4c37-a71e-aeb96d543ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d329610-4d7a-4979-86ce-0b1b80e0754a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef003f6c-e9be-4c37-a71e-aeb96d543ae6",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "55638f9d-4f41-4b04-924a-ef19a578632e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "edf4324d-acd2-4640-a73d-bb2e5362327d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55638f9d-4f41-4b04-924a-ef19a578632e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec92947c-98ff-4725-a249-66a67df7900a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55638f9d-4f41-4b04-924a-ef19a578632e",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "2e7d3726-aa15-4264-8fda-01df138992f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "30831591-8b80-42a6-a5b1-660936ac278a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e7d3726-aa15-4264-8fda-01df138992f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3db1cb4-d47c-4a98-b43c-9f94fcd663ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e7d3726-aa15-4264-8fda-01df138992f8",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "63d9a4e8-f98c-4aed-ae3c-c77d8996e650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "384dd062-ded8-449e-aa9b-970ca3792312",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63d9a4e8-f98c-4aed-ae3c-c77d8996e650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d85fd86-3b55-4edf-a03f-5a0ce6b3fba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63d9a4e8-f98c-4aed-ae3c-c77d8996e650",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "54fb9cf2-6a38-4fe5-b99f-8d24b944251b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "5e5db9f7-6477-40e0-a795-1a81c043c3b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54fb9cf2-6a38-4fe5-b99f-8d24b944251b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a750c594-4a6f-4009-8b5a-7ea11b50513e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54fb9cf2-6a38-4fe5-b99f-8d24b944251b",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "089828a5-1555-4272-baa3-0b37fc0dc49c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "a42bc8f7-9b11-49c7-b4c1-70d2660232e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "089828a5-1555-4272-baa3-0b37fc0dc49c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89b4d2b6-67b2-4703-b1f4-4b1dc3a33067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "089828a5-1555-4272-baa3-0b37fc0dc49c",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "e6677fda-867e-4d98-9750-4226f017a8ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "3f49cd58-dd94-4222-8a29-a8dff9419c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6677fda-867e-4d98-9750-4226f017a8ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "182decbc-e031-4896-ad17-b9e417fc60ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6677fda-867e-4d98-9750-4226f017a8ef",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ba207f93-6619-47a3-b4b9-1d9405ec802b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "808ffc98-39e3-464f-918f-86adcacf9508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba207f93-6619-47a3-b4b9-1d9405ec802b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2044aac8-aade-41af-a03f-d5790c1a5967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba207f93-6619-47a3-b4b9-1d9405ec802b",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "dabc47c8-3e70-426e-b452-c49b1249b60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "b12fba9a-6331-4308-ac17-1efd1543604e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dabc47c8-3e70-426e-b452-c49b1249b60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7503770-7e92-4f93-87f2-70ad0cdc9bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dabc47c8-3e70-426e-b452-c49b1249b60f",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ace4b4fd-d479-492b-874c-2964b22746cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "ad216456-fd69-42ab-a96a-185670b81420",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace4b4fd-d479-492b-874c-2964b22746cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14feff2d-b2be-4b67-8194-e9982aae8a9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace4b4fd-d479-492b-874c-2964b22746cc",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "d2759531-2cd2-4bb6-852b-325c9d2a5cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "04d9c62b-ecae-4a25-ac6e-132153dd37e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2759531-2cd2-4bb6-852b-325c9d2a5cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0913f78-a5ff-436a-a390-62cca6bf03c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2759531-2cd2-4bb6-852b-325c9d2a5cfd",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "89b44b23-0ec8-4658-9438-a0ae1f2c0230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "4a3fec90-8b86-4a7c-867c-7faa9b856037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b44b23-0ec8-4658-9438-a0ae1f2c0230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782be578-3cfe-4d29-bdea-897396ee22bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b44b23-0ec8-4658-9438-a0ae1f2c0230",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "627b1722-ea4d-4839-864d-a7d49f1b45a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "3a7da872-8e0d-4e81-bd76-d553eec2ef01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "627b1722-ea4d-4839-864d-a7d49f1b45a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e14161d-0abf-4c97-a25b-a2a295fc3099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "627b1722-ea4d-4839-864d-a7d49f1b45a3",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ca1e4c0d-9ac0-41f0-89d3-3348222a32f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "5db987bb-bbc9-4fcd-af85-fe3ff5204099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca1e4c0d-9ac0-41f0-89d3-3348222a32f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bceed96a-28eb-4cdf-85b5-18df688be116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca1e4c0d-9ac0-41f0-89d3-3348222a32f6",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "cc07250c-578b-4ed3-bbd7-5dc233cdcb67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "9b08986a-3247-48a3-9197-271ab19cf2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc07250c-578b-4ed3-bbd7-5dc233cdcb67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b4098b-e006-4b32-8ac3-25d0d863a3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc07250c-578b-4ed3-bbd7-5dc233cdcb67",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "b964328b-1460-4bbf-a371-3aa568ab06c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "db2786a0-3bb2-456a-bfdc-1cdda83a3cae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b964328b-1460-4bbf-a371-3aa568ab06c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb31c5b-8997-4be4-8cad-ced7252c266f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b964328b-1460-4bbf-a371-3aa568ab06c0",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "546c1a70-6325-41f8-a93d-81e708715b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "6e449670-4c14-44a9-b23d-747fa3bbad0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "546c1a70-6325-41f8-a93d-81e708715b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f26fc4d-47b8-4001-a79c-0118b3aa3efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "546c1a70-6325-41f8-a93d-81e708715b46",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "533cf7fd-e265-465a-8cc2-d03cc85d4810",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "aff32007-2b5d-43bd-b51d-92c7fbb7ac7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533cf7fd-e265-465a-8cc2-d03cc85d4810",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c1fdb7-947f-4bf3-8166-91122b3c9e03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533cf7fd-e265-465a-8cc2-d03cc85d4810",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "2cded71b-c04d-48c2-ac00-7bd8a2d2ba4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "581f8f96-bc93-4cc3-8996-f6c061aba585",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cded71b-c04d-48c2-ac00-7bd8a2d2ba4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94f0381-e18e-444c-bfdf-d24d69c8b66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cded71b-c04d-48c2-ac00-7bd8a2d2ba4e",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "a442a842-80b1-4ce1-90db-980152b0fe7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "61fd4e93-6b54-4acb-90da-4bec742696ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a442a842-80b1-4ce1-90db-980152b0fe7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f89eda0a-7cdc-4ace-995a-d37907543241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a442a842-80b1-4ce1-90db-980152b0fe7c",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "e5477447-d8eb-4853-9808-7f36e8180ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "6e930b13-6b4b-43f8-b5c8-834eff1045f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5477447-d8eb-4853-9808-7f36e8180ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32a36de7-cfe2-4f7b-9f66-ff5d5fe7437d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5477447-d8eb-4853-9808-7f36e8180ea4",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "f48d6df1-b1b3-4d19-8e32-abe0dc5204a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "9b475dc3-01e4-4f14-a083-c59e170d96bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f48d6df1-b1b3-4d19-8e32-abe0dc5204a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6acb37c-fc72-430a-ba25-2550e0770840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f48d6df1-b1b3-4d19-8e32-abe0dc5204a6",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "813cdc49-9339-4474-be8a-f81528faf24a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "cdabf18d-2113-49dc-8629-1782b5dae26f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "813cdc49-9339-4474-be8a-f81528faf24a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b655575-65d6-4e4b-acf4-960dcf1cafce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "813cdc49-9339-4474-be8a-f81528faf24a",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "6fd5dc21-8a67-4b78-99b3-81a21070b509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "70f1b821-d06a-49a7-9daf-d1edf1790d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fd5dc21-8a67-4b78-99b3-81a21070b509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6961fe-e677-4859-adf4-44f8511ed157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fd5dc21-8a67-4b78-99b3-81a21070b509",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "7ad9373e-18ba-4cbf-b426-4ffd64c1acd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "0b3007b8-6007-45b3-874b-f1dbdeaf6067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad9373e-18ba-4cbf-b426-4ffd64c1acd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e76e4de-7855-4c0b-9503-7f74e196f2af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad9373e-18ba-4cbf-b426-4ffd64c1acd6",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "9ee6ab7a-05a3-4771-bc78-550d3def6ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "ef220998-3673-46cc-ab89-ca0898cdc154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee6ab7a-05a3-4771-bc78-550d3def6ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b409387-5046-480a-b0b1-6495e9a6bace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee6ab7a-05a3-4771-bc78-550d3def6ea4",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "2b6f1e94-dba4-44bc-8402-0309625e7945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "60a1f9cb-f641-4cbf-8ef4-0a8516945318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b6f1e94-dba4-44bc-8402-0309625e7945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4387be97-49a4-44d7-b823-51d6e6dda9a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b6f1e94-dba4-44bc-8402-0309625e7945",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "f80e5ffb-91fc-4bdb-8d21-818a3812c112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "5038541b-266c-48f2-97e9-610ac786d8f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f80e5ffb-91fc-4bdb-8d21-818a3812c112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac2c9c3-47d7-478f-b55f-eca865ac0466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f80e5ffb-91fc-4bdb-8d21-818a3812c112",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "b6931ae6-8b10-457d-b9b6-8cb9d6b0d8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "bce098e3-bfaa-4f18-b5fe-d6cebe90446c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6931ae6-8b10-457d-b9b6-8cb9d6b0d8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bc942a8-2478-4a63-91e7-46c8bd3dfe61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6931ae6-8b10-457d-b9b6-8cb9d6b0d8ca",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ee5c8771-a727-42bf-83eb-d72ca1a1db12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "e8fed565-66e4-4a76-8628-26a70fb1e956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee5c8771-a727-42bf-83eb-d72ca1a1db12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58849636-64e2-426e-b2dc-dd9a645a9ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee5c8771-a727-42bf-83eb-d72ca1a1db12",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "25d4072d-0796-4ab0-96c9-8e6e4d7e71c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "f533344c-d349-429e-945b-303193186675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d4072d-0796-4ab0-96c9-8e6e4d7e71c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e9f68d3-f49c-4ca0-a979-ee7771046265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d4072d-0796-4ab0-96c9-8e6e4d7e71c7",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "d615da08-ebcd-4102-8f6b-bd1781fcf337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "904c287f-f34a-42ae-bc30-7f14ebb280dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d615da08-ebcd-4102-8f6b-bd1781fcf337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3041121b-fb93-48b3-89ea-c6f2be43dfd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d615da08-ebcd-4102-8f6b-bd1781fcf337",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "3f08995b-b6f0-46cf-a59e-4be45e67bbe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "348d657c-5aba-4c56-bed9-c40fd37d4679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f08995b-b6f0-46cf-a59e-4be45e67bbe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04214c5b-8ad5-46bc-81f0-e9401ad91406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f08995b-b6f0-46cf-a59e-4be45e67bbe4",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "357f7374-8e79-478e-892c-c11a88454c24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "4ed76da9-5d04-4d4b-9a5b-364e744639d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "357f7374-8e79-478e-892c-c11a88454c24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d739e12f-efcd-4bcb-a09a-66b5579918ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "357f7374-8e79-478e-892c-c11a88454c24",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "779d6e31-c053-45e8-9f60-9459005723dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "a9823b97-283e-4e80-a120-1c040b142082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "779d6e31-c053-45e8-9f60-9459005723dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc60596-7435-45bf-afd3-e2be6ad46109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "779d6e31-c053-45e8-9f60-9459005723dc",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ca4e6935-0828-4a85-9520-660121774e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "6e69999c-d02b-40b6-bc49-20300a841ba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca4e6935-0828-4a85-9520-660121774e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3fd2fde-287d-4283-a573-64d68c293502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca4e6935-0828-4a85-9520-660121774e69",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "9f4e35ce-c388-4a2d-b60b-c1c3fa007eba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "ceb30840-2f56-4b7c-b5b3-3c715220e028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4e35ce-c388-4a2d-b60b-c1c3fa007eba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b9b4ea-b728-44dc-84b4-cc36aca91982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4e35ce-c388-4a2d-b60b-c1c3fa007eba",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "535e4cc0-07aa-4cde-a0e6-9c7e5d472f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "92d1b267-1f14-4cb2-8f21-e805fb83bf31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "535e4cc0-07aa-4cde-a0e6-9c7e5d472f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69332e0e-007a-449d-b099-6211e08da45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "535e4cc0-07aa-4cde-a0e6-9c7e5d472f88",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "019c91e3-5538-48d6-9343-fb5fd3c9be7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "229b2e7a-b2ea-4d11-aa15-a920e0a2cb9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019c91e3-5538-48d6-9343-fb5fd3c9be7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e078f4-c202-4272-9b53-7b020b50a7d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019c91e3-5538-48d6-9343-fb5fd3c9be7e",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "2cfddabd-7b13-4c12-9043-82103a76f1d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "0d74b828-699e-44ad-b42e-c1baa6ef7ce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cfddabd-7b13-4c12-9043-82103a76f1d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84c72938-6e44-427d-ad2f-16ec6a1b6f46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cfddabd-7b13-4c12-9043-82103a76f1d2",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "414289df-a485-4b07-825a-140776d45596",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "8a2e9f99-f0b5-48d5-9d04-3e9999674415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "414289df-a485-4b07-825a-140776d45596",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f25c584-a860-4653-9e22-3e0d6f08d313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "414289df-a485-4b07-825a-140776d45596",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "193b5cc2-1634-4d23-9e1f-bea951d8f2bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "e1e7173d-55bb-43bd-9c1a-e3dd968026a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193b5cc2-1634-4d23-9e1f-bea951d8f2bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1fa9e1-0f80-4b7b-aaa5-a186d73a4379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193b5cc2-1634-4d23-9e1f-bea951d8f2bb",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "48d973d6-035b-42fd-964d-9106403de7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "6bfc6042-338a-4120-a4c6-4c0efb8d50ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d973d6-035b-42fd-964d-9106403de7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24a9dd52-0122-48fe-a5b1-7a0ceee678ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d973d6-035b-42fd-964d-9106403de7d2",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ce147455-ec94-4734-b1d8-432da1efd221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "20bdc0ae-be8d-4f08-961e-446f198dfc16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce147455-ec94-4734-b1d8-432da1efd221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "624f9854-877b-46df-8d0e-6d0975492b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce147455-ec94-4734-b1d8-432da1efd221",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "29198a6f-8188-4d11-b857-c823c8a2ec7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "f715c825-a2f1-4670-b399-36d459ca02e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29198a6f-8188-4d11-b857-c823c8a2ec7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff70089-f47f-4753-8e94-67dec1e58e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29198a6f-8188-4d11-b857-c823c8a2ec7b",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "d0b00282-0f0d-471a-b3e7-9b2b6798a311",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "10f615b7-1db9-44f6-a88f-6cfe0fa4ccce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b00282-0f0d-471a-b3e7-9b2b6798a311",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4af8658e-7a2d-49f3-af31-8a4233e3c4eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b00282-0f0d-471a-b3e7-9b2b6798a311",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "51b4bee4-6beb-4fa0-b794-525237a4fa56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "0d97b760-d29e-4696-9029-47deb1419171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b4bee4-6beb-4fa0-b794-525237a4fa56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ab645b-2290-4dde-b30b-764f18ab43c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b4bee4-6beb-4fa0-b794-525237a4fa56",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "b3cdc8fd-12ec-4d76-8717-f55b0abb26a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "7ccc3b8d-2e7b-4480-a1cd-67bf1f02a43d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3cdc8fd-12ec-4d76-8717-f55b0abb26a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a8d0527-9943-4d57-a757-2ed74ba911d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3cdc8fd-12ec-4d76-8717-f55b0abb26a2",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "75123cea-3fb3-436a-9e26-c602adb64b96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "157f1c75-0741-4280-b70c-8d38790d0685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75123cea-3fb3-436a-9e26-c602adb64b96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9d8980c-b545-4536-a644-dd13e09ba3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75123cea-3fb3-436a-9e26-c602adb64b96",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "eb0a332f-643f-483b-9990-a65e5338482e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "e1b7436f-02d4-4737-8218-19a0a6ba84c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb0a332f-643f-483b-9990-a65e5338482e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26822a4b-33a7-434c-bce2-ceacf0d8951c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb0a332f-643f-483b-9990-a65e5338482e",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "d9bfecd6-33c4-4374-a3cf-8d0ccf1403a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "9c79a734-6c23-4265-af3b-6b245910c9bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9bfecd6-33c4-4374-a3cf-8d0ccf1403a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2834d9b0-7cf7-4662-85e4-a7d36b63eea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9bfecd6-33c4-4374-a3cf-8d0ccf1403a3",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "0557f620-55d8-4718-86d8-bfe42688c9c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "e16d0c4f-a078-4ac5-a392-1fee58d6abc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0557f620-55d8-4718-86d8-bfe42688c9c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e10666e5-3a9e-4087-9039-74ce09ce6b0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0557f620-55d8-4718-86d8-bfe42688c9c5",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "674f635d-59e9-4bab-9369-9f4a05d8566f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "50bd417b-55b2-4037-bcfd-483028c34428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "674f635d-59e9-4bab-9369-9f4a05d8566f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a70337c4-3fda-4b95-8bdf-1d803ad1eee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "674f635d-59e9-4bab-9369-9f4a05d8566f",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "9cce4a4b-4aa7-4308-85a9-d2395850c02c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "0ba3909e-6fe5-4f4b-8b44-1ecc1a8a3bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cce4a4b-4aa7-4308-85a9-d2395850c02c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f66c5bcc-76aa-43da-80c4-72ffab464e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cce4a4b-4aa7-4308-85a9-d2395850c02c",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "fa1dd9e9-2a80-4a54-8278-aacbfc79ddda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "054267aa-abcf-43ed-b9f5-a628ccebd414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1dd9e9-2a80-4a54-8278-aacbfc79ddda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512fa709-01cd-4e64-b0aa-f0a81ed74a2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1dd9e9-2a80-4a54-8278-aacbfc79ddda",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "5ebce773-354b-40cd-b9b6-bb6e65095da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "160cd83f-512b-4841-a8af-d0202bc3744c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ebce773-354b-40cd-b9b6-bb6e65095da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fa939a7-1911-4fbe-9d55-a7e075358c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ebce773-354b-40cd-b9b6-bb6e65095da2",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "ba8fccf2-32e3-405f-8a69-56328d9a779b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "989565f1-16b2-4487-ba29-ee1e61b261b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba8fccf2-32e3-405f-8a69-56328d9a779b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6879a583-d14d-47ad-87c6-993876735bba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba8fccf2-32e3-405f-8a69-56328d9a779b",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "d982bf4a-fc3d-4424-8ebb-d1cbc4f02fcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "09b555de-e8dd-4178-a7f1-ff9dbc45a624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d982bf4a-fc3d-4424-8ebb-d1cbc4f02fcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4cd4724-e79a-46f5-8fe8-a0ee82714a7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d982bf4a-fc3d-4424-8ebb-d1cbc4f02fcf",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "57d2482d-817a-4dd8-8997-a8e44bda2127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "fc1e80c1-0a45-43d3-bcd9-fc6578d553c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57d2482d-817a-4dd8-8997-a8e44bda2127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94a0b9f-96b9-4874-a777-7f38e49757b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d2482d-817a-4dd8-8997-a8e44bda2127",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "917d4c29-8618-49e0-b5db-f3c9f2272494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "9afef1d9-f277-4a9c-80c4-3896977d772e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "917d4c29-8618-49e0-b5db-f3c9f2272494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5675f9-e985-4623-99f9-a0ab1374c3cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "917d4c29-8618-49e0-b5db-f3c9f2272494",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "4c5110cb-faf7-47ef-b749-3b4b41ff4d13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "ce722b79-a8d2-4a03-b676-878577f8d43c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5110cb-faf7-47ef-b749-3b4b41ff4d13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21d46096-2394-4615-b834-ebbaad88ccc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5110cb-faf7-47ef-b749-3b4b41ff4d13",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "c94e5914-08b8-4b54-8466-9faab162fd0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "1c78f6da-35ec-499f-b247-96e3f0c09f50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c94e5914-08b8-4b54-8466-9faab162fd0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bc47aa-7384-41d2-8f67-637fca90ea5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c94e5914-08b8-4b54-8466-9faab162fd0a",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "6b11a963-3036-4c98-b014-58f316b8242f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "505eb81f-4896-4da1-a30c-03a80ad5f5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b11a963-3036-4c98-b014-58f316b8242f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016148f3-f00f-4b1a-821e-a216193d78e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b11a963-3036-4c98-b014-58f316b8242f",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "33c798e0-c8f3-48be-9fa7-ec2532ddbaa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "488ca741-e4a8-4bd7-8612-43b90348165f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c798e0-c8f3-48be-9fa7-ec2532ddbaa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c70e98e0-7071-45e0-ae53-77e266847992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c798e0-c8f3-48be-9fa7-ec2532ddbaa0",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "345e547b-7067-4814-a397-d723e8b4adae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "474e5a5a-2ce2-4c11-be62-ebcf0a108863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345e547b-7067-4814-a397-d723e8b4adae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8da1f13-d097-4aa1-8423-33e670774233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345e547b-7067-4814-a397-d723e8b4adae",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "70c79134-cf59-4e1a-9216-9e862be0d575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "0a7c5473-5f9f-49d4-aca1-27b072f0379e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70c79134-cf59-4e1a-9216-9e862be0d575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9923ad52-997c-4642-ae92-adc53517a5fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70c79134-cf59-4e1a-9216-9e862be0d575",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "3de9bded-8c7a-4280-97a4-9ae197d6ae2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "7dc527e4-ec80-47af-b5ea-dcc020ee568f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3de9bded-8c7a-4280-97a4-9ae197d6ae2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "740f6d54-c198-4603-9af5-502230182274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3de9bded-8c7a-4280-97a4-9ae197d6ae2b",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "e9a49633-0af3-4708-ad25-81b704eb8a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "81c38062-3097-4c7d-b453-1208e73dd41b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a49633-0af3-4708-ad25-81b704eb8a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc63aaf7-731d-44c0-a1c1-4de12ca21ec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a49633-0af3-4708-ad25-81b704eb8a92",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "1def8f71-8581-414c-9015-b55cffcaa6db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "a5b39e78-d38c-4a3d-9cc1-7660ce235e1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1def8f71-8581-414c-9015-b55cffcaa6db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f51600f-3238-4e1d-97f3-5c6f46252563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1def8f71-8581-414c-9015-b55cffcaa6db",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        },
        {
            "id": "dcad69b5-8653-441d-bb3d-8cd6073d5879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "compositeImage": {
                "id": "66c19e17-10c4-48f4-ba9e-5a372075e9de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcad69b5-8653-441d-bb3d-8cd6073d5879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d690e5e5-20ad-4894-92d5-5be622f86635",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcad69b5-8653-441d-bb3d-8cd6073d5879",
                    "LayerId": "498b47e0-0f1a-4584-b7e4-931d6a033eab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "498b47e0-0f1a-4584-b7e4-931d6a033eab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b9b2a8b-3625-4139-91fc-eb4d9c3c485c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}