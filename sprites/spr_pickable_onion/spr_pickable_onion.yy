{
    "id": "929e4161-241b-46f6-84a5-5b86293d71e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_onion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "002519c2-8613-4539-add3-359decf3f7cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "929e4161-241b-46f6-84a5-5b86293d71e6",
            "compositeImage": {
                "id": "7889b574-9f1d-49a5-8af2-39908464db37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "002519c2-8613-4539-add3-359decf3f7cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05301a6c-d111-40c8-b22c-7ef814ae10f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "002519c2-8613-4539-add3-359decf3f7cb",
                    "LayerId": "f2a57c42-d120-432b-96ec-5e7bfe457284"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f2a57c42-d120-432b-96ec-5e7bfe457284",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "929e4161-241b-46f6-84a5-5b86293d71e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 12
}