{
    "id": "3ea020fd-1fb8-4bbd-bc50-631db143205c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6de2333e-1b5e-4612-b4c7-23fbc93ad397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ea020fd-1fb8-4bbd-bc50-631db143205c",
            "compositeImage": {
                "id": "79714a91-67ae-4cc5-88e1-45ddbd564556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de2333e-1b5e-4612-b4c7-23fbc93ad397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee2851bc-4e5f-4859-bb4e-e8f0a1057c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de2333e-1b5e-4612-b4c7-23fbc93ad397",
                    "LayerId": "55ed8a17-4481-44b1-b64c-f272c1281227"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55ed8a17-4481-44b1-b64c-f272c1281227",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ea020fd-1fb8-4bbd-bc50-631db143205c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}