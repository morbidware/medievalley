{
    "id": "a6682015-826f-491c-86a8-b667e47e15ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_charcr_charbase",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3980eba1-280b-4a29-96a6-ae797c3dd835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6682015-826f-491c-86a8-b667e47e15ee",
            "compositeImage": {
                "id": "faeaa2fe-0c16-40c2-bbb5-285a8e00af4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3980eba1-280b-4a29-96a6-ae797c3dd835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d09b5617-b8bf-4736-973e-c36d9ffeb54f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3980eba1-280b-4a29-96a6-ae797c3dd835",
                    "LayerId": "8ed4e286-81f8-452b-b689-26c56cd260e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "8ed4e286-81f8-452b-b689-26c56cd260e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6682015-826f-491c-86a8-b667e47e15ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 27,
    "yorig": 25
}