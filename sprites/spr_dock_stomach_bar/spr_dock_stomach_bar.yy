{
    "id": "8d65de4c-421b-44e7-abbe-ba300e90834e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_stomach_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4af935a2-c0f2-470d-b818-63d7922d92bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d65de4c-421b-44e7-abbe-ba300e90834e",
            "compositeImage": {
                "id": "0d210962-fc8c-4bca-a8b7-8c58b22fad90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4af935a2-c0f2-470d-b818-63d7922d92bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68dc81c3-6000-406f-b637-82ec5543836d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4af935a2-c0f2-470d-b818-63d7922d92bd",
                    "LayerId": "57c9dd9f-5ff1-4cf4-893c-91eb7bae707b"
                }
            ]
        },
        {
            "id": "c20bf32f-92cb-4faf-a7c1-a59d260942e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d65de4c-421b-44e7-abbe-ba300e90834e",
            "compositeImage": {
                "id": "6ccc4f85-b0f6-4d16-b871-63b8b3cf13e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c20bf32f-92cb-4faf-a7c1-a59d260942e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26a78c8-3779-4413-9b52-e88f6a9377be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c20bf32f-92cb-4faf-a7c1-a59d260942e2",
                    "LayerId": "57c9dd9f-5ff1-4cf4-893c-91eb7bae707b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "57c9dd9f-5ff1-4cf4-893c-91eb7bae707b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d65de4c-421b-44e7-abbe-ba300e90834e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 68
}