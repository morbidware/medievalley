{
    "id": "99292144-8b39-4739-bc40-d04dec04e1fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_life_stamina_bg_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e156713-ae01-444c-a153-dd837f940d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99292144-8b39-4739-bc40-d04dec04e1fb",
            "compositeImage": {
                "id": "cab98d85-238d-4c94-8adb-7d7725d2c05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e156713-ae01-444c-a153-dd837f940d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505fac11-9a30-4a53-ad88-aa6777fca8b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e156713-ae01-444c-a153-dd837f940d7c",
                    "LayerId": "b792b2cf-bd04-4bfd-83bc-8440b34aeb77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "b792b2cf-bd04-4bfd-83bc-8440b34aeb77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99292144-8b39-4739-bc40-d04dec04e1fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}