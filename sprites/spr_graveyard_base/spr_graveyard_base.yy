{
    "id": "0aeefc49-46cc-4852-ba3a-6ce6735b84f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 271,
    "bbox_left": 0,
    "bbox_right": 271,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33f351ad-0cbb-42a1-b819-401b6287429e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aeefc49-46cc-4852-ba3a-6ce6735b84f9",
            "compositeImage": {
                "id": "0c6242df-6196-4461-8c0d-0ec968c0e383",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f351ad-0cbb-42a1-b819-401b6287429e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac1b84e-3bce-4af3-878d-c55116fc97a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f351ad-0cbb-42a1-b819-401b6287429e",
                    "LayerId": "daf13fb8-2748-4b5d-a654-5cf0dbc12248"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 272,
    "layers": [
        {
            "id": "daf13fb8-2748-4b5d-a654-5cf0dbc12248",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aeefc49-46cc-4852-ba3a-6ce6735b84f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 272,
    "xorig": 136,
    "yorig": 136
}