{
    "id": "3b0817b7-e73c-4e03-8b1d-15ef9227c4b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f82bf704-183b-4f60-8884-793efdb0a61b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b0817b7-e73c-4e03-8b1d-15ef9227c4b8",
            "compositeImage": {
                "id": "6f3f77bd-f3b8-40bc-96cc-84e741390a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f82bf704-183b-4f60-8884-793efdb0a61b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eac4492-15a9-4255-b4e0-d6b5ac3c1caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f82bf704-183b-4f60-8884-793efdb0a61b",
                    "LayerId": "cf940dac-fe10-4208-9d6a-fa7e26970313"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cf940dac-fe10-4208-9d6a-fa7e26970313",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b0817b7-e73c-4e03-8b1d-15ef9227c4b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}