{
    "id": "87aa8bdc-45a9-4c40-8ee0-b0779ab4a99b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89ae0ad6-e670-497b-a799-6ecafe4b3f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aa8bdc-45a9-4c40-8ee0-b0779ab4a99b",
            "compositeImage": {
                "id": "cddd48da-3452-46e4-84e2-6d536d9dcde5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ae0ad6-e670-497b-a799-6ecafe4b3f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36700cdd-fc32-489c-aa64-c71a7e3e9ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ae0ad6-e670-497b-a799-6ecafe4b3f55",
                    "LayerId": "e30f3757-e160-46ba-a96e-379279905879"
                }
            ]
        },
        {
            "id": "084e15e5-2b7c-484b-9c70-6a8be66ce9b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aa8bdc-45a9-4c40-8ee0-b0779ab4a99b",
            "compositeImage": {
                "id": "0f9a43e2-2ce4-4196-8839-5d56cee531e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084e15e5-2b7c-484b-9c70-6a8be66ce9b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66cefc0d-96ea-43a0-9079-f1be118ce679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084e15e5-2b7c-484b-9c70-6a8be66ce9b0",
                    "LayerId": "e30f3757-e160-46ba-a96e-379279905879"
                }
            ]
        },
        {
            "id": "f9435314-9f82-44ba-89bf-743c8afd1d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aa8bdc-45a9-4c40-8ee0-b0779ab4a99b",
            "compositeImage": {
                "id": "c8ff26b6-3e99-4c9b-99be-046eb2b3f300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9435314-9f82-44ba-89bf-743c8afd1d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9071a05a-1759-422d-a810-0ab548139e01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9435314-9f82-44ba-89bf-743c8afd1d5e",
                    "LayerId": "e30f3757-e160-46ba-a96e-379279905879"
                }
            ]
        },
        {
            "id": "4916d8e2-de16-473d-9c52-51e93adb6bc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aa8bdc-45a9-4c40-8ee0-b0779ab4a99b",
            "compositeImage": {
                "id": "68a44b2f-ce62-40fa-8c05-6376996eb3ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4916d8e2-de16-473d-9c52-51e93adb6bc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12f6c70a-babb-4afc-8059-40131b060bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4916d8e2-de16-473d-9c52-51e93adb6bc0",
                    "LayerId": "e30f3757-e160-46ba-a96e-379279905879"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e30f3757-e160-46ba-a96e-379279905879",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87aa8bdc-45a9-4c40-8ee0-b0779ab4a99b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}