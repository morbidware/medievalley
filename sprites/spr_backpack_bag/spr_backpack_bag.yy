{
    "id": "ddd794ac-9cf8-49f5-a5d7-c281aad8d39c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backpack_bag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "466f1112-2731-4aa4-b4a9-5e090c574cf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddd794ac-9cf8-49f5-a5d7-c281aad8d39c",
            "compositeImage": {
                "id": "0110a7c5-f57f-4681-9ecd-ea84c8a6b450",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "466f1112-2731-4aa4-b4a9-5e090c574cf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eac1d42-3aef-45cd-af58-860cd5786678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "466f1112-2731-4aa4-b4a9-5e090c574cf8",
                    "LayerId": "97eb2880-09a6-4040-a0d7-17be5f901459"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "97eb2880-09a6-4040-a0d7-17be5f901459",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddd794ac-9cf8-49f5-a5d7-c281aad8d39c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 7
}