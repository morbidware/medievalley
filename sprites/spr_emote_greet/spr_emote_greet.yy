{
    "id": "a8b02362-47cb-44f2-bd0b-00cb0543d4c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_greet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e0696b37-588d-45b1-b98a-522fa69890d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b02362-47cb-44f2-bd0b-00cb0543d4c3",
            "compositeImage": {
                "id": "15f29537-38d2-4b3a-a20c-bf50041638ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0696b37-588d-45b1-b98a-522fa69890d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25af4d9f-6ab6-42e3-b8a2-5188fa1a7f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0696b37-588d-45b1-b98a-522fa69890d3",
                    "LayerId": "37dbc563-e9ea-4013-ac25-5bbea8a4d4f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "37dbc563-e9ea-4013-ac25-5bbea8a4d4f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b02362-47cb-44f2-bd0b-00cb0543d4c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}