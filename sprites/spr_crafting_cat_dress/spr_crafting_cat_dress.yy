{
    "id": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_dress",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f0f7843-0985-43df-b282-114818f6b843",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
            "compositeImage": {
                "id": "24bb859f-5a8e-4f01-950e-9e78cdbb268c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f0f7843-0985-43df-b282-114818f6b843",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce2fee8-12a6-4a35-a35a-8f2ab3e16031",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f0f7843-0985-43df-b282-114818f6b843",
                    "LayerId": "b145758d-7da5-4f0b-8f00-bbab57a3f094"
                },
                {
                    "id": "1b2f2b96-583e-4539-9344-27a6bf202799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f0f7843-0985-43df-b282-114818f6b843",
                    "LayerId": "1773709b-6143-4385-a723-8d69471f2146"
                }
            ]
        },
        {
            "id": "4bf8b20c-b12c-40c3-96fa-ec2da90dbb1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
            "compositeImage": {
                "id": "e9794dbf-611f-4cac-b01f-cb6f0ca20658",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf8b20c-b12c-40c3-96fa-ec2da90dbb1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2f9325-a62a-446e-a4b8-7b3fa739f1db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf8b20c-b12c-40c3-96fa-ec2da90dbb1a",
                    "LayerId": "b145758d-7da5-4f0b-8f00-bbab57a3f094"
                },
                {
                    "id": "a079bb7c-4dc0-45cd-8d8d-c31e09b5a4a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf8b20c-b12c-40c3-96fa-ec2da90dbb1a",
                    "LayerId": "1773709b-6143-4385-a723-8d69471f2146"
                }
            ]
        },
        {
            "id": "2bfdd266-ab53-4cd6-a08b-ca3f1b717e26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
            "compositeImage": {
                "id": "bdede8f7-1394-4479-a76b-40da2e6175fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bfdd266-ab53-4cd6-a08b-ca3f1b717e26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d14bf3ab-77bc-4915-a355-f11b86242e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bfdd266-ab53-4cd6-a08b-ca3f1b717e26",
                    "LayerId": "b145758d-7da5-4f0b-8f00-bbab57a3f094"
                },
                {
                    "id": "7fddddd3-d09c-490f-89b8-7d01c1f63078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bfdd266-ab53-4cd6-a08b-ca3f1b717e26",
                    "LayerId": "1773709b-6143-4385-a723-8d69471f2146"
                }
            ]
        },
        {
            "id": "e15427ca-593d-4eba-945d-e39cabaefe0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
            "compositeImage": {
                "id": "8f1cbb5a-5271-404c-92f1-0e61d8e2e98c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15427ca-593d-4eba-945d-e39cabaefe0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d0ad70d-b7bf-49fc-b59a-b2153b4dcc0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15427ca-593d-4eba-945d-e39cabaefe0f",
                    "LayerId": "b145758d-7da5-4f0b-8f00-bbab57a3f094"
                },
                {
                    "id": "0777cfcb-e6b0-4639-a798-f3cd435d6a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15427ca-593d-4eba-945d-e39cabaefe0f",
                    "LayerId": "1773709b-6143-4385-a723-8d69471f2146"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 28,
    "layers": [
        {
            "id": "b145758d-7da5-4f0b-8f00-bbab57a3f094",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1773709b-6143-4385-a723-8d69471f2146",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c90f764a-2cf4-43bf-88ff-7e6165d5bd03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}