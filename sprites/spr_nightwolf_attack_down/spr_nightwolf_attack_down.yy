{
    "id": "362b1750-9f1f-46fc-892a-23bc071ee158",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 16,
    "bbox_right": 33,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13d37ff3-287c-432d-bae7-02b8ab65c908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "362b1750-9f1f-46fc-892a-23bc071ee158",
            "compositeImage": {
                "id": "89363d3e-1bc6-445f-8b22-56d57d4c6740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d37ff3-287c-432d-bae7-02b8ab65c908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a252806d-2289-4a61-a449-574c4dd06389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d37ff3-287c-432d-bae7-02b8ab65c908",
                    "LayerId": "7236d939-3a6a-49fc-9e6e-20247dc02006"
                }
            ]
        },
        {
            "id": "46f5c41e-748e-4841-97f6-b097cbf8c182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "362b1750-9f1f-46fc-892a-23bc071ee158",
            "compositeImage": {
                "id": "7e3b86fa-3ac3-4905-ae90-0d11bfb10577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46f5c41e-748e-4841-97f6-b097cbf8c182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e419ea6-c5de-4db1-b703-348dea7dc7bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46f5c41e-748e-4841-97f6-b097cbf8c182",
                    "LayerId": "7236d939-3a6a-49fc-9e6e-20247dc02006"
                }
            ]
        },
        {
            "id": "eb3a859d-188d-4868-beed-3b4dbbbfaf5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "362b1750-9f1f-46fc-892a-23bc071ee158",
            "compositeImage": {
                "id": "fcb133dc-f1e2-4e1e-ad2b-f6bc9427b556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3a859d-188d-4868-beed-3b4dbbbfaf5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b8055a3-88db-46e8-b780-5c560d71acfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3a859d-188d-4868-beed-3b4dbbbfaf5d",
                    "LayerId": "7236d939-3a6a-49fc-9e6e-20247dc02006"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "7236d939-3a6a-49fc-9e6e-20247dc02006",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "362b1750-9f1f-46fc-892a-23bc071ee158",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}