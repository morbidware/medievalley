{
    "id": "07f2493b-3eb1-47d6-8f9f-619b3fde4665",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipeinfo_bg_separator",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 155,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e74ee35-9638-4374-8041-b5b070eda312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07f2493b-3eb1-47d6-8f9f-619b3fde4665",
            "compositeImage": {
                "id": "6d2213aa-106a-4c0a-ae2d-d1a30c25d65a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e74ee35-9638-4374-8041-b5b070eda312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99be6a29-a65f-4737-b088-4afc032f9247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e74ee35-9638-4374-8041-b5b070eda312",
                    "LayerId": "d4ebe10e-71cc-4ccd-b206-4de4c3741732"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "d4ebe10e-71cc-4ccd-b206-4de4c3741732",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07f2493b-3eb1-47d6-8f9f-619b3fde4665",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 156,
    "xorig": 78,
    "yorig": 7
}