{
    "id": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_aubergine_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b81e907-5049-4a78-814b-619f02d202fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
            "compositeImage": {
                "id": "a3f1b01b-3bb4-4d5b-8963-b34aa161750b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b81e907-5049-4a78-814b-619f02d202fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5cdddf8-4a39-4317-ae43-8d2124ba918c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b81e907-5049-4a78-814b-619f02d202fa",
                    "LayerId": "930829bb-4054-4ee6-b067-fbc9552ca07c"
                }
            ]
        },
        {
            "id": "a0554ca1-46da-4d47-a86e-75f9d9ea5cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
            "compositeImage": {
                "id": "830d1b08-67d8-4207-85de-7eb9ad629059",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0554ca1-46da-4d47-a86e-75f9d9ea5cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd0900a5-bc8d-4824-934c-4bc60a11785d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0554ca1-46da-4d47-a86e-75f9d9ea5cfd",
                    "LayerId": "930829bb-4054-4ee6-b067-fbc9552ca07c"
                }
            ]
        },
        {
            "id": "069b62e1-9653-4a5d-a5f1-570c5cce8381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
            "compositeImage": {
                "id": "9a08d6fe-7b7a-4685-8ee1-97b1dc842c0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "069b62e1-9653-4a5d-a5f1-570c5cce8381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e45a3457-132a-4b41-a408-534bf04f166b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "069b62e1-9653-4a5d-a5f1-570c5cce8381",
                    "LayerId": "930829bb-4054-4ee6-b067-fbc9552ca07c"
                }
            ]
        },
        {
            "id": "9d549645-8bf7-4424-82a6-719604f6890a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
            "compositeImage": {
                "id": "56f4335a-a111-42d2-8777-61756ea161b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d549645-8bf7-4424-82a6-719604f6890a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e73fda9d-ce00-4ba8-995b-42e615188d47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d549645-8bf7-4424-82a6-719604f6890a",
                    "LayerId": "930829bb-4054-4ee6-b067-fbc9552ca07c"
                }
            ]
        },
        {
            "id": "dedbdad3-799d-495c-9660-0dadd0ee469d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
            "compositeImage": {
                "id": "a7c472e6-1df5-4e98-b21b-6a70f34db5c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dedbdad3-799d-495c-9660-0dadd0ee469d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e9895a7-5adb-48ba-b4fa-6e00bcf719f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dedbdad3-799d-495c-9660-0dadd0ee469d",
                    "LayerId": "930829bb-4054-4ee6-b067-fbc9552ca07c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "930829bb-4054-4ee6-b067-fbc9552ca07c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "074a238c-a9d7-4252-b3b3-2f96cf8c8d93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 26
}