{
    "id": "1ec51017-5f63-4017-8bcf-8fb6a1dc0573",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cff78b6d-af0c-435a-a670-1d53edafbe8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ec51017-5f63-4017-8bcf-8fb6a1dc0573",
            "compositeImage": {
                "id": "3aeadb76-0982-402c-9d5a-8ac0761c29cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cff78b6d-af0c-435a-a670-1d53edafbe8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb53b64c-6409-4fd7-8d4c-6d36f2a0e60b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cff78b6d-af0c-435a-a670-1d53edafbe8c",
                    "LayerId": "aa1f8431-1dab-44cc-841f-ed67497209f9"
                }
            ]
        },
        {
            "id": "11c984d4-28a2-4ecc-9722-52f5514a95ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ec51017-5f63-4017-8bcf-8fb6a1dc0573",
            "compositeImage": {
                "id": "80619f07-5462-450c-918c-9024e068de81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11c984d4-28a2-4ecc-9722-52f5514a95ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54703e0b-9a0c-45a9-b643-88a033643837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11c984d4-28a2-4ecc-9722-52f5514a95ee",
                    "LayerId": "aa1f8431-1dab-44cc-841f-ed67497209f9"
                }
            ]
        },
        {
            "id": "f4f4c09b-b01d-4067-9a81-5394cb12d65b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ec51017-5f63-4017-8bcf-8fb6a1dc0573",
            "compositeImage": {
                "id": "102d6850-68df-4875-8b1e-c46d4ee52d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4f4c09b-b01d-4067-9a81-5394cb12d65b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "139920bf-8892-41bf-afbe-3aa8dfffb113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4f4c09b-b01d-4067-9a81-5394cb12d65b",
                    "LayerId": "aa1f8431-1dab-44cc-841f-ed67497209f9"
                }
            ]
        },
        {
            "id": "f0db8766-fd92-4f57-8b1e-82e11b4ce7bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ec51017-5f63-4017-8bcf-8fb6a1dc0573",
            "compositeImage": {
                "id": "c65399c3-4d50-49f0-b265-e812d5a72532",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0db8766-fd92-4f57-8b1e-82e11b4ce7bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52b6b34c-5aa3-4f92-ba29-0ea31656ee30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0db8766-fd92-4f57-8b1e-82e11b4ce7bc",
                    "LayerId": "aa1f8431-1dab-44cc-841f-ed67497209f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "aa1f8431-1dab-44cc-841f-ed67497209f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ec51017-5f63-4017-8bcf-8fb6a1dc0573",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}