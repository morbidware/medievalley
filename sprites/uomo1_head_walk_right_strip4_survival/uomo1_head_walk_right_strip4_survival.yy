{
    "id": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_walk_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59bef933-5204-4df5-aeb7-b48ba6143421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "compositeImage": {
                "id": "df3d6f8b-68e0-4448-80c3-de27f85b26ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59bef933-5204-4df5-aeb7-b48ba6143421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b14c144c-97b0-4013-9148-de2e119e584a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59bef933-5204-4df5-aeb7-b48ba6143421",
                    "LayerId": "f6604473-c4a9-43ba-afda-b57b2b8d9142"
                }
            ]
        },
        {
            "id": "3d2ce782-e39a-44b6-a07f-437da8783a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "compositeImage": {
                "id": "cb8ae252-bd37-40e0-997a-9b6814d690bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2ce782-e39a-44b6-a07f-437da8783a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20636eab-ddbe-40f8-a171-80e48f02c71b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2ce782-e39a-44b6-a07f-437da8783a65",
                    "LayerId": "f6604473-c4a9-43ba-afda-b57b2b8d9142"
                }
            ]
        },
        {
            "id": "fe140f26-8f17-49f1-bd10-dfc0687f5dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "compositeImage": {
                "id": "b86d3006-e67d-4436-bad5-c73c5884fbf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe140f26-8f17-49f1-bd10-dfc0687f5dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe6ec94-d7cf-4fcc-a17a-365148cfef98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe140f26-8f17-49f1-bd10-dfc0687f5dbc",
                    "LayerId": "f6604473-c4a9-43ba-afda-b57b2b8d9142"
                }
            ]
        },
        {
            "id": "3ce45171-ef65-4609-a0d0-0ca24d34f659",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "compositeImage": {
                "id": "8e8217ce-5c3c-4090-b25e-281d83065da6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce45171-ef65-4609-a0d0-0ca24d34f659",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc02768-f7ba-4ae8-8ea6-b194b50cfed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce45171-ef65-4609-a0d0-0ca24d34f659",
                    "LayerId": "f6604473-c4a9-43ba-afda-b57b2b8d9142"
                }
            ]
        },
        {
            "id": "504ab4cb-55d9-4459-b5fd-2c866c091f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "compositeImage": {
                "id": "e37144e9-b1fa-4c9e-a928-c700a4bc4feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "504ab4cb-55d9-4459-b5fd-2c866c091f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0434295c-f8e1-40b0-8898-9ac4cc9f268b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "504ab4cb-55d9-4459-b5fd-2c866c091f3a",
                    "LayerId": "f6604473-c4a9-43ba-afda-b57b2b8d9142"
                }
            ]
        },
        {
            "id": "45efbed9-157b-48ad-b64a-cf94ee4c136c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "compositeImage": {
                "id": "3819768f-b6d9-4812-a57b-225bab3f71cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45efbed9-157b-48ad-b64a-cf94ee4c136c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43af2acc-f704-465b-8f6b-8dc311ac8b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45efbed9-157b-48ad-b64a-cf94ee4c136c",
                    "LayerId": "f6604473-c4a9-43ba-afda-b57b2b8d9142"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f6604473-c4a9-43ba-afda-b57b2b8d9142",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bad1d9be-7cf8-4b6e-be8b-7a42422b5601",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}