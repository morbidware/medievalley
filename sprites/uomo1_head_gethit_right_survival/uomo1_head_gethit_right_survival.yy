{
    "id": "1dfcdb35-12d0-407f-b272-e73fdfdaab3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_gethit_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97273720-37f2-4183-aeb5-9ea969f57dad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dfcdb35-12d0-407f-b272-e73fdfdaab3f",
            "compositeImage": {
                "id": "e51491db-fdef-4da4-bd58-9795c7b9926a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97273720-37f2-4183-aeb5-9ea969f57dad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ff8d31-2e2e-44cb-9e87-30f72af25874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97273720-37f2-4183-aeb5-9ea969f57dad",
                    "LayerId": "5ea8cf38-eae7-4f25-8c5f-f3c5baaaccab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ea8cf38-eae7-4f25-8c5f-f3c5baaaccab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dfcdb35-12d0-407f-b272-e73fdfdaab3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}