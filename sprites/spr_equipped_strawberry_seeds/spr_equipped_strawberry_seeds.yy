{
    "id": "8d48ce75-a8f1-45a9-afab-71d3b987b5ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_strawberry_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0c81835-532a-472a-bac0-7472e38db6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d48ce75-a8f1-45a9-afab-71d3b987b5ba",
            "compositeImage": {
                "id": "3f6310fb-4a41-4afd-bc8c-4906737e0ba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c81835-532a-472a-bac0-7472e38db6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a3aabf-6265-417b-8d8a-7e9e46ab7306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c81835-532a-472a-bac0-7472e38db6a1",
                    "LayerId": "d1f3ec17-478b-412b-855a-b3204294a790"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d1f3ec17-478b-412b-855a-b3204294a790",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d48ce75-a8f1-45a9-afab-71d3b987b5ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}