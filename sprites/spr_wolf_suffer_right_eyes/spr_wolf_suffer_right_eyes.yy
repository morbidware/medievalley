{
    "id": "a336842e-9a6f-4636-97a4-12dc3604a2f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_suffer_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f664b24-6724-4358-80a5-6ac9c41dc6d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a336842e-9a6f-4636-97a4-12dc3604a2f9",
            "compositeImage": {
                "id": "1e73b788-25fa-4c54-a896-580b0dbea920",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f664b24-6724-4358-80a5-6ac9c41dc6d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52923e58-1f47-4012-96f3-ac564616b91b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f664b24-6724-4358-80a5-6ac9c41dc6d8",
                    "LayerId": "c26879f2-9163-435a-be5d-babb0bcf6439"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "c26879f2-9163-435a-be5d-babb0bcf6439",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a336842e-9a6f-4636-97a4-12dc3604a2f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}