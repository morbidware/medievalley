{
    "id": "560844b6-6a9c-4ae8-a087-841e40efc013",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf5d097b-e10b-498e-a4d2-e0c5c60dee38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "560844b6-6a9c-4ae8-a087-841e40efc013",
            "compositeImage": {
                "id": "849e53cd-6a49-4d0a-ac46-8d50b0a5c6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf5d097b-e10b-498e-a4d2-e0c5c60dee38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f0e8c2d-1867-42e4-9282-a44fcda99662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf5d097b-e10b-498e-a4d2-e0c5c60dee38",
                    "LayerId": "932734e3-3786-44ba-832a-81a803761797"
                }
            ]
        },
        {
            "id": "221832b2-a304-48ee-8239-77dad085073e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "560844b6-6a9c-4ae8-a087-841e40efc013",
            "compositeImage": {
                "id": "ba24e799-b504-48bb-aa70-35234d87a7d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "221832b2-a304-48ee-8239-77dad085073e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b1d72bf-7db3-4ac1-9eb0-84c680a6076e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "221832b2-a304-48ee-8239-77dad085073e",
                    "LayerId": "932734e3-3786-44ba-832a-81a803761797"
                }
            ]
        },
        {
            "id": "2f6e9762-d9fe-4161-825e-348c9b6c46de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "560844b6-6a9c-4ae8-a087-841e40efc013",
            "compositeImage": {
                "id": "4c277957-2fb2-45ba-87f6-cd75ccbdf3f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6e9762-d9fe-4161-825e-348c9b6c46de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c826d9ff-4d32-4886-b8b0-95ac5c132aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6e9762-d9fe-4161-825e-348c9b6c46de",
                    "LayerId": "932734e3-3786-44ba-832a-81a803761797"
                }
            ]
        },
        {
            "id": "b94e2277-8ce4-4629-bad0-a69fe98b6ae0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "560844b6-6a9c-4ae8-a087-841e40efc013",
            "compositeImage": {
                "id": "6047f1ec-dd07-4fc1-9704-23317f2dbbdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b94e2277-8ce4-4629-bad0-a69fe98b6ae0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ebcce92-68ae-4abc-8775-c653e989d733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b94e2277-8ce4-4629-bad0-a69fe98b6ae0",
                    "LayerId": "932734e3-3786-44ba-832a-81a803761797"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "932734e3-3786-44ba-832a-81a803761797",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "560844b6-6a9c-4ae8-a087-841e40efc013",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}