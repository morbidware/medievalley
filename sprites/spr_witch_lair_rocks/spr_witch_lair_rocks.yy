{
    "id": "5e17ef47-27e0-4967-808f-9c2345513582",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_witch_lair_rocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7295c506-981c-40c6-920d-2e284b546741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e17ef47-27e0-4967-808f-9c2345513582",
            "compositeImage": {
                "id": "4023e7c9-ffb7-46a2-9486-4e14cd6dbae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7295c506-981c-40c6-920d-2e284b546741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9b1048-e736-42c0-a93a-1ecb2a3a3937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7295c506-981c-40c6-920d-2e284b546741",
                    "LayerId": "c3f1c1f9-6034-4ed4-9764-b2f0e1a1831c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c3f1c1f9-6034-4ed4-9764-b2f0e1a1831c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e17ef47-27e0-4967-808f-9c2345513582",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 25
}