{
    "id": "13b465f8-1d58-4b3c-ad01-815ba5cab347",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a529904a-ed2d-4906-9319-11967035676a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13b465f8-1d58-4b3c-ad01-815ba5cab347",
            "compositeImage": {
                "id": "bf2416d0-8b5b-4546-8349-7d79e71a02af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a529904a-ed2d-4906-9319-11967035676a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e746f233-1012-457b-9d4c-22e362a61c78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a529904a-ed2d-4906-9319-11967035676a",
                    "LayerId": "2f5e866d-b48b-4b49-adc1-50fd33f3641a"
                }
            ]
        },
        {
            "id": "d8e5cab0-2ef4-4842-84b8-1f0a556a412a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13b465f8-1d58-4b3c-ad01-815ba5cab347",
            "compositeImage": {
                "id": "57282849-98a7-480a-8d21-2cf7b47804e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e5cab0-2ef4-4842-84b8-1f0a556a412a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "752cc98e-15a7-467a-b900-38f81a197852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e5cab0-2ef4-4842-84b8-1f0a556a412a",
                    "LayerId": "2f5e866d-b48b-4b49-adc1-50fd33f3641a"
                }
            ]
        },
        {
            "id": "0e0f2b71-15d1-4eb4-989f-4ef0a6148e3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13b465f8-1d58-4b3c-ad01-815ba5cab347",
            "compositeImage": {
                "id": "15bf693b-e7dc-44ed-ad22-d9806e348a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e0f2b71-15d1-4eb4-989f-4ef0a6148e3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "667376cd-18ec-4c19-b5b9-d2e7b4a625f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e0f2b71-15d1-4eb4-989f-4ef0a6148e3c",
                    "LayerId": "2f5e866d-b48b-4b49-adc1-50fd33f3641a"
                }
            ]
        },
        {
            "id": "aba0aa44-726c-45a4-9d25-3b40880a91c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13b465f8-1d58-4b3c-ad01-815ba5cab347",
            "compositeImage": {
                "id": "93b68839-0a04-400e-ae96-02e714d5469d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba0aa44-726c-45a4-9d25-3b40880a91c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1fada11-73d5-44b6-8eb2-ba2050d1f256",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba0aa44-726c-45a4-9d25-3b40880a91c7",
                    "LayerId": "2f5e866d-b48b-4b49-adc1-50fd33f3641a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f5e866d-b48b-4b49-adc1-50fd33f3641a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13b465f8-1d58-4b3c-ad01-815ba5cab347",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}