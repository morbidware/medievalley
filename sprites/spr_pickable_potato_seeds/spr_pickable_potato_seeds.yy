{
    "id": "b50feac1-5939-45d7-8ab7-27d986134d07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potato_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a5307da-2b04-4a9a-9b78-5cd59af88e3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b50feac1-5939-45d7-8ab7-27d986134d07",
            "compositeImage": {
                "id": "91f10905-dff5-4018-b4bb-8d9794b1d092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5307da-2b04-4a9a-9b78-5cd59af88e3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34fed52-4531-4707-a07c-933f89227391",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5307da-2b04-4a9a-9b78-5cd59af88e3d",
                    "LayerId": "2fe1317c-b8ed-4d04-8503-eae3c3c2553f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2fe1317c-b8ed-4d04-8503-eae3c3c2553f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b50feac1-5939-45d7-8ab7-27d986134d07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}