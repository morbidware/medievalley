{
    "id": "002f13d9-854e-423d-9d55-b841e5308682",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_numbers_64",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 56,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "913a5711-246c-424e-952c-da3a407031fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "002f13d9-854e-423d-9d55-b841e5308682",
            "compositeImage": {
                "id": "8c397b4a-c585-44ca-b202-6d5d9383a733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913a5711-246c-424e-952c-da3a407031fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdad0b00-2fdf-42bb-81af-1f5b3ae745e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913a5711-246c-424e-952c-da3a407031fd",
                    "LayerId": "51b601e0-4fb2-43fc-9fa1-3077bcff5485"
                }
            ]
        },
        {
            "id": "56437bf3-0f81-4e0f-a7a9-e468d99d941f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "002f13d9-854e-423d-9d55-b841e5308682",
            "compositeImage": {
                "id": "47d8443a-e8de-4bdf-85d7-f75f40a769f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56437bf3-0f81-4e0f-a7a9-e468d99d941f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d59665-f7d1-4b53-b40d-f07289a75a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56437bf3-0f81-4e0f-a7a9-e468d99d941f",
                    "LayerId": "51b601e0-4fb2-43fc-9fa1-3077bcff5485"
                }
            ]
        },
        {
            "id": "5d35205a-dec0-4a8b-bab7-439c15e3e3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "002f13d9-854e-423d-9d55-b841e5308682",
            "compositeImage": {
                "id": "9cae5cbd-6705-4031-a484-1d44aa53062f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d35205a-dec0-4a8b-bab7-439c15e3e3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c84e5f2-2e45-4a2a-bf7e-19a024233e6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d35205a-dec0-4a8b-bab7-439c15e3e3ff",
                    "LayerId": "51b601e0-4fb2-43fc-9fa1-3077bcff5485"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "51b601e0-4fb2-43fc-9fa1-3077bcff5485",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "002f13d9-854e-423d-9d55-b841e5308682",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}