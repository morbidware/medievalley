{
    "id": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_pick_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff25778c-bfc7-4084-8251-8501b46f02d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "compositeImage": {
                "id": "061c8633-451d-4f0e-8317-6daca51269bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff25778c-bfc7-4084-8251-8501b46f02d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca93c26c-7e22-4e97-8df6-97146d5618d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff25778c-bfc7-4084-8251-8501b46f02d5",
                    "LayerId": "d139d7e9-dd8a-4805-99fc-ef9f12236731"
                }
            ]
        },
        {
            "id": "10a427c4-fa78-4ae6-a02d-0ca4b7182751",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "compositeImage": {
                "id": "58804052-9d0e-476d-b6a7-bb8abaffb63a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10a427c4-fa78-4ae6-a02d-0ca4b7182751",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a66d1e7-cf54-4372-9664-1fc7ff4f31ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a427c4-fa78-4ae6-a02d-0ca4b7182751",
                    "LayerId": "d139d7e9-dd8a-4805-99fc-ef9f12236731"
                }
            ]
        },
        {
            "id": "67334496-e176-450a-8de6-f85781ba5423",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "compositeImage": {
                "id": "5cf5ef43-f1c0-428d-bf7a-2fa467bd5f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67334496-e176-450a-8de6-f85781ba5423",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c4784a-9977-4fd7-af50-5f212addf171",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67334496-e176-450a-8de6-f85781ba5423",
                    "LayerId": "d139d7e9-dd8a-4805-99fc-ef9f12236731"
                }
            ]
        },
        {
            "id": "acb17fc5-b3ee-49e7-b861-34ff1240afab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "compositeImage": {
                "id": "aa682e87-3f2f-4145-ad44-9c9cb0610e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acb17fc5-b3ee-49e7-b861-34ff1240afab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6bf2892-cd02-426c-9785-4c0cc1bda78f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acb17fc5-b3ee-49e7-b861-34ff1240afab",
                    "LayerId": "d139d7e9-dd8a-4805-99fc-ef9f12236731"
                }
            ]
        },
        {
            "id": "47b5f50d-69de-4e77-ba62-ecfa3d4d7320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "compositeImage": {
                "id": "cd660853-fc36-4809-8dd7-9b5701d2b0be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b5f50d-69de-4e77-ba62-ecfa3d4d7320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fe36ad6-22e3-4683-8196-e9ba8a262e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b5f50d-69de-4e77-ba62-ecfa3d4d7320",
                    "LayerId": "d139d7e9-dd8a-4805-99fc-ef9f12236731"
                }
            ]
        },
        {
            "id": "9120cb25-fffb-42df-ac2b-d4c3c6dd49b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "compositeImage": {
                "id": "42ac2b83-4fa4-46df-a9ee-d7ec7fa730a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9120cb25-fffb-42df-ac2b-d4c3c6dd49b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd19cdb-d479-45ab-9f1e-3d53d1176d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9120cb25-fffb-42df-ac2b-d4c3c6dd49b6",
                    "LayerId": "d139d7e9-dd8a-4805-99fc-ef9f12236731"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d139d7e9-dd8a-4805-99fc-ef9f12236731",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3378527e-c1d0-4cbe-8478-8a1644f1a2fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}