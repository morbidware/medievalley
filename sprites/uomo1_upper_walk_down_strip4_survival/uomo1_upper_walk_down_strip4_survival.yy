{
    "id": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_walk_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4d878fc-ae4b-472a-8aaa-3721be28620b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "compositeImage": {
                "id": "a5cd5732-0877-4b76-b1dc-4779f5ce61aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d878fc-ae4b-472a-8aaa-3721be28620b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "075c0142-dc7a-49c5-81cd-c148ccedc511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d878fc-ae4b-472a-8aaa-3721be28620b",
                    "LayerId": "16c1b37a-4e20-4e0d-b8c3-b280128eba65"
                }
            ]
        },
        {
            "id": "5a4c0ee6-4fa4-4ed9-9977-c3bc32828f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "compositeImage": {
                "id": "4fb333b1-16df-4f75-8a5c-991f28de95f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4c0ee6-4fa4-4ed9-9977-c3bc32828f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71cd1707-f8dd-49ae-8c0b-4418a6eca1c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4c0ee6-4fa4-4ed9-9977-c3bc32828f09",
                    "LayerId": "16c1b37a-4e20-4e0d-b8c3-b280128eba65"
                }
            ]
        },
        {
            "id": "e567cb16-8fb5-4afe-a8e7-6c523c14d779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "compositeImage": {
                "id": "532194ab-11f1-4981-9eb8-8348458bdb36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e567cb16-8fb5-4afe-a8e7-6c523c14d779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f641f1f6-5f14-485d-9668-be2f93b4a4e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e567cb16-8fb5-4afe-a8e7-6c523c14d779",
                    "LayerId": "16c1b37a-4e20-4e0d-b8c3-b280128eba65"
                }
            ]
        },
        {
            "id": "bf030487-1382-4738-96fd-79cc9261820d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "compositeImage": {
                "id": "ccb7208c-2149-4fc6-9a25-7b3be812bf2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf030487-1382-4738-96fd-79cc9261820d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b215ac-4f43-402b-8243-f00ef2323ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf030487-1382-4738-96fd-79cc9261820d",
                    "LayerId": "16c1b37a-4e20-4e0d-b8c3-b280128eba65"
                }
            ]
        },
        {
            "id": "9eb6f1d3-d908-4457-a09b-2310e4d83792",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "compositeImage": {
                "id": "05aa5990-d105-4572-86cb-cc4356033723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eb6f1d3-d908-4457-a09b-2310e4d83792",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85dabf57-9446-4b18-bf0b-144d9fdabb76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eb6f1d3-d908-4457-a09b-2310e4d83792",
                    "LayerId": "16c1b37a-4e20-4e0d-b8c3-b280128eba65"
                }
            ]
        },
        {
            "id": "429c02e2-f9f0-4a4c-90fe-bc7b30ca972f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "compositeImage": {
                "id": "0aecb399-5620-4439-bcec-fd9be3ca2e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "429c02e2-f9f0-4a4c-90fe-bc7b30ca972f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1323f32d-aa86-48c9-b5ee-93a2fd536b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "429c02e2-f9f0-4a4c-90fe-bc7b30ca972f",
                    "LayerId": "16c1b37a-4e20-4e0d-b8c3-b280128eba65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "16c1b37a-4e20-4e0d-b8c3-b280128eba65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94485e18-2bb6-43ff-9127-4cd0ac8274c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}