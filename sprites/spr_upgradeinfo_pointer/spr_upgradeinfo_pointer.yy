{
    "id": "e6648967-9a63-4d64-8d8f-b6e8bf2d1f6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_upgradeinfo_pointer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d97ac993-819a-4fc7-8146-7aca5c6ea93f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6648967-9a63-4d64-8d8f-b6e8bf2d1f6f",
            "compositeImage": {
                "id": "711e8f1e-6cd7-4e05-ae9b-e671e782a94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d97ac993-819a-4fc7-8146-7aca5c6ea93f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b993381e-0cc1-4c32-8773-3b16461d8271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d97ac993-819a-4fc7-8146-7aca5c6ea93f",
                    "LayerId": "7c57ecf7-ed2b-42d8-a4bc-25ca5d6981de"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 8,
    "height": 16,
    "layers": [
        {
            "id": "7c57ecf7-ed2b-42d8-a4bc-25ca5d6981de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6648967-9a63-4d64-8d8f-b6e8bf2d1f6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 0
}