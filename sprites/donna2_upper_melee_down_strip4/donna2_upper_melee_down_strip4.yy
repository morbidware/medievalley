{
    "id": "72073634-e488-4045-989f-89621c5ccc76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a622749d-c22f-4561-a9ff-edf05ba3094f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72073634-e488-4045-989f-89621c5ccc76",
            "compositeImage": {
                "id": "df8f5737-ba7d-4cd0-8de6-de2fdf0a16ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a622749d-c22f-4561-a9ff-edf05ba3094f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2d6e5a-3a5d-4467-8435-bf317daaeff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a622749d-c22f-4561-a9ff-edf05ba3094f",
                    "LayerId": "2f84711e-1e16-493c-8a59-da68c4569eee"
                }
            ]
        },
        {
            "id": "b9ca3db3-e21e-44fe-8332-6a8ff1e461d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72073634-e488-4045-989f-89621c5ccc76",
            "compositeImage": {
                "id": "086db63a-389f-4fd6-ac08-cff6d57f0ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9ca3db3-e21e-44fe-8332-6a8ff1e461d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f813cbf2-a8b9-45b4-9946-48f2a9050fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9ca3db3-e21e-44fe-8332-6a8ff1e461d6",
                    "LayerId": "2f84711e-1e16-493c-8a59-da68c4569eee"
                }
            ]
        },
        {
            "id": "e6914e34-dd5f-456c-a4e1-d93d1ce31385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72073634-e488-4045-989f-89621c5ccc76",
            "compositeImage": {
                "id": "3ad49496-9ac2-45cc-a5cb-46307cb3097e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6914e34-dd5f-456c-a4e1-d93d1ce31385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d31790af-60b1-4001-8e50-b843e173336b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6914e34-dd5f-456c-a4e1-d93d1ce31385",
                    "LayerId": "2f84711e-1e16-493c-8a59-da68c4569eee"
                }
            ]
        },
        {
            "id": "5c7a6f8d-a021-40cd-91ad-3dc5b83010fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72073634-e488-4045-989f-89621c5ccc76",
            "compositeImage": {
                "id": "b4eef9ef-79e3-412b-a57a-66cbed9bc272",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7a6f8d-a021-40cd-91ad-3dc5b83010fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9722d36a-1f8f-4c0b-a640-7826d830451a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7a6f8d-a021-40cd-91ad-3dc5b83010fc",
                    "LayerId": "2f84711e-1e16-493c-8a59-da68c4569eee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f84711e-1e16-493c-8a59-da68c4569eee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72073634-e488-4045-989f-89621c5ccc76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}