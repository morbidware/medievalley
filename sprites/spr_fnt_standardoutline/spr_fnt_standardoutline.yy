{
    "id": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_standardoutline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8fc5773-8e49-4abe-a30d-75b585a00da3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "6e23d7a5-f7ec-45ed-bf67-1598209e51f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8fc5773-8e49-4abe-a30d-75b585a00da3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74952967-c628-407c-9860-7f65eee65433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8fc5773-8e49-4abe-a30d-75b585a00da3",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "8682d2d6-fb0d-411f-b4d0-56edf3e7e443",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "fc36f86b-4ffb-47dd-9e98-44f1b197903a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8682d2d6-fb0d-411f-b4d0-56edf3e7e443",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf357ff3-1ef7-42ff-894f-2a0e09c5bf7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8682d2d6-fb0d-411f-b4d0-56edf3e7e443",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "ad27672f-f9de-494b-87de-d4ddd03cea46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "38dcab63-7868-4755-85d7-424e6fef450a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad27672f-f9de-494b-87de-d4ddd03cea46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4940d5c-97c4-4377-815a-4d212fed9d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad27672f-f9de-494b-87de-d4ddd03cea46",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "73bfa767-d559-4d65-8cf7-fc429c04f266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "4487f258-c5b8-49ae-b52e-57a8a37f53dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73bfa767-d559-4d65-8cf7-fc429c04f266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84932aa8-a2c3-46a1-b94b-44eeca41d4bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73bfa767-d559-4d65-8cf7-fc429c04f266",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "2e2b7fa7-c84d-45a7-8633-24c9ea6d6ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "ac42a236-f888-4071-99aa-3d1a1655a1da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e2b7fa7-c84d-45a7-8633-24c9ea6d6ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6215a9af-174d-4bc6-bafe-efeb61a914c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e2b7fa7-c84d-45a7-8633-24c9ea6d6ecf",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "b7a2dc98-17ae-4a1c-8c94-daf95ccf35ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "5fcc1429-707a-4729-aa7f-4f82177fad35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a2dc98-17ae-4a1c-8c94-daf95ccf35ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "511a004c-2c00-4f4c-b4df-f3c54880d7e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a2dc98-17ae-4a1c-8c94-daf95ccf35ad",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "7697fb71-a5e2-471b-a3d3-b7eb7c895860",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "a5743263-241f-4539-9099-643e0f574fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7697fb71-a5e2-471b-a3d3-b7eb7c895860",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4986c4-4964-46a9-9ed4-5d8c9a239809",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7697fb71-a5e2-471b-a3d3-b7eb7c895860",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "bad8bba9-885e-495c-8338-7ecd6e8f7cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "dc397bae-260b-4d4d-8d10-73c8cca15a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bad8bba9-885e-495c-8338-7ecd6e8f7cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d939164-13e2-4360-a738-a795ef4010f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bad8bba9-885e-495c-8338-7ecd6e8f7cf6",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "960c17d2-08e9-4d5c-a684-5292313a0b8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "af01d3c1-b460-4232-aceb-93df603a9bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960c17d2-08e9-4d5c-a684-5292313a0b8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3be4a33-a1bf-49cf-ac07-244ee58b864f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960c17d2-08e9-4d5c-a684-5292313a0b8e",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "32bb74b2-8696-45d8-95f4-2cd7f3ffff5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "869bf39b-ef46-420c-a231-29b38f9b007b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32bb74b2-8696-45d8-95f4-2cd7f3ffff5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "403e4b53-e1a3-4b03-bdfc-92ae7c817067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32bb74b2-8696-45d8-95f4-2cd7f3ffff5b",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "afe6b7fb-45f3-40e0-8eb7-c0dfd73fdb38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "344d5829-f821-4355-8498-6befbf92d86d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe6b7fb-45f3-40e0-8eb7-c0dfd73fdb38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3447ae65-daf1-45d6-b847-f247267f5f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe6b7fb-45f3-40e0-8eb7-c0dfd73fdb38",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "99fcc2a1-3c15-40a1-9fad-2ee0f5172d7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "549c2d37-0969-48d3-8d2a-9add660f2080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99fcc2a1-3c15-40a1-9fad-2ee0f5172d7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "354076c0-482f-4cf7-a65b-0a605baf8e87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99fcc2a1-3c15-40a1-9fad-2ee0f5172d7e",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "864478f1-6da2-48b9-9216-bf294c474bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "e023c9d0-bbda-488a-bb6c-b76f1ad1a23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "864478f1-6da2-48b9-9216-bf294c474bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27220846-cbde-4f9d-a59b-569f19c0c0e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "864478f1-6da2-48b9-9216-bf294c474bd8",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "4780a641-cd9b-4e68-9bfd-93e9ee415e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "4eb44c80-e795-4aa3-93a8-d4e1b156827f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4780a641-cd9b-4e68-9bfd-93e9ee415e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769fbf26-306c-4477-9c63-7a8782cf71e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4780a641-cd9b-4e68-9bfd-93e9ee415e4e",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "877ec115-3728-469c-a7a7-8df4f65c6805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "ffa2723f-0ba5-42ad-9c54-93e0054c492c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877ec115-3728-469c-a7a7-8df4f65c6805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bed1db20-6586-49b4-ad32-405aea4d6403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877ec115-3728-469c-a7a7-8df4f65c6805",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "8d31c86c-1ce0-40da-bd97-0fe6e7ff43e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d5ae46c0-912b-4fde-b1e7-24d3629c616d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d31c86c-1ce0-40da-bd97-0fe6e7ff43e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94caab1e-fd75-4397-8fd0-fead4de6398b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d31c86c-1ce0-40da-bd97-0fe6e7ff43e7",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "777df32c-1fe2-47b8-ae58-f8fa28e382a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "9f09abb6-429c-4c8d-b0ad-b4dd55b32670",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777df32c-1fe2-47b8-ae58-f8fa28e382a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826a0951-a378-4472-a1d2-590c159db9e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777df32c-1fe2-47b8-ae58-f8fa28e382a8",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "51f96a2b-7263-431a-98b4-4cea67d07851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "e0504aca-dfe6-468b-97a2-bf7cd9d0d05f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f96a2b-7263-431a-98b4-4cea67d07851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0405a5a5-82d3-4c44-a91c-389a65ea2e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f96a2b-7263-431a-98b4-4cea67d07851",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "f325a31d-f517-426b-a5d8-a0b958e241a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "e18634d8-b5cb-46e1-83df-4901d540c629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f325a31d-f517-426b-a5d8-a0b958e241a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce562878-5150-4e99-b584-109fb3d14e83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f325a31d-f517-426b-a5d8-a0b958e241a0",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "768b1db6-6dc9-46dd-b63e-99d90ef7fb76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "5ed66a4c-ac67-493a-9681-1d4c064c4137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "768b1db6-6dc9-46dd-b63e-99d90ef7fb76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2158f87d-85d2-4db0-b2df-2a555f70db83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "768b1db6-6dc9-46dd-b63e-99d90ef7fb76",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "85b564a6-cabe-4593-81f2-28431ec2cc0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "a971e1a5-de25-44d4-9e20-e5be58bf0de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b564a6-cabe-4593-81f2-28431ec2cc0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96f899cd-d5c9-482e-9917-581c91f8b675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b564a6-cabe-4593-81f2-28431ec2cc0d",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "00e3df59-4653-43df-8e29-f4ba84ff39e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "4933a627-35d8-4df5-b9b1-85614fd629f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00e3df59-4653-43df-8e29-f4ba84ff39e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26b5784f-2b05-4144-8fd8-10649c838b51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00e3df59-4653-43df-8e29-f4ba84ff39e4",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "e5d371e8-4243-4dc5-80b6-86252274e9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "97a1dd3d-b746-4e65-b5cc-1db8a2a32636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5d371e8-4243-4dc5-80b6-86252274e9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badccbf3-9570-43bb-b229-fa6d572be655",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d371e8-4243-4dc5-80b6-86252274e9d0",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "dbcb5eca-2aee-4245-a2a9-5bbac5dc77eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "146bf66d-00d5-4b8f-b97b-25d73d97b9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbcb5eca-2aee-4245-a2a9-5bbac5dc77eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b809aa9-95b6-4c4d-84be-d10f5b69f2b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbcb5eca-2aee-4245-a2a9-5bbac5dc77eb",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "30e576a8-7a33-48ea-9c5a-f47ed858e60d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "c77b650a-e3fc-4534-a101-38fdbc7f387e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e576a8-7a33-48ea-9c5a-f47ed858e60d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44aa8fa1-0608-4ba2-b91a-20433d314600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e576a8-7a33-48ea-9c5a-f47ed858e60d",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "5361f1f4-acc5-420f-bd7a-11ee7c1240c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "baa9989f-b3d6-4fa2-a6f3-604265a1293d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5361f1f4-acc5-420f-bd7a-11ee7c1240c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a756fd86-1d4c-4db9-8fae-b3e08291359a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5361f1f4-acc5-420f-bd7a-11ee7c1240c9",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "6fefaa03-c4e1-48c1-9a58-9585a53c4b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "fa5db7e5-7af6-4451-8426-fd3fe9597efb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fefaa03-c4e1-48c1-9a58-9585a53c4b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0403c119-1ea1-409e-a8d2-1ed59f7547f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fefaa03-c4e1-48c1-9a58-9585a53c4b89",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "fb976782-72b2-4bff-8848-25f6a34a0aa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "464a39e8-5c8e-4130-a046-91077113e599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb976782-72b2-4bff-8848-25f6a34a0aa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ffd9640-d4ec-41bf-b291-6ebf79969493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb976782-72b2-4bff-8848-25f6a34a0aa3",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "306e36c3-8c6a-417c-a218-bd76366b0f8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "a52d03a6-e3af-41b3-82bf-05cbe95ddc99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "306e36c3-8c6a-417c-a218-bd76366b0f8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e78edc4-6499-491f-ad24-0ae35624df7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "306e36c3-8c6a-417c-a218-bd76366b0f8a",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "c399d231-7361-44f2-952c-88da0b324af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "85dcbf04-7474-4e3d-abf2-47027ad92b4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c399d231-7361-44f2-952c-88da0b324af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63a5869-203d-4035-acb8-776e11078008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c399d231-7361-44f2-952c-88da0b324af2",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "7f3f2373-7fde-46e8-837f-4335529e8454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "9e1cff15-1129-4d9e-bc92-eb72ab3eb146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f3f2373-7fde-46e8-837f-4335529e8454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e30677b2-493f-4822-8e21-1ac44083047d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f3f2373-7fde-46e8-837f-4335529e8454",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "af0d6d64-9435-478f-ad53-711c5ca9e844",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "1d32e9ac-aade-49ff-b585-00e6b05814fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af0d6d64-9435-478f-ad53-711c5ca9e844",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e53abc19-a1dc-48bf-916b-00295591191b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af0d6d64-9435-478f-ad53-711c5ca9e844",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "9cd55f32-bbc1-4b9d-b0e4-184d6cfcc2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "0fe9c2c9-8749-4c45-abeb-8deac832a1f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd55f32-bbc1-4b9d-b0e4-184d6cfcc2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9067ec63-a368-4666-b38d-f0641010d835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd55f32-bbc1-4b9d-b0e4-184d6cfcc2c3",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "09e2fc1a-221b-43db-b90f-0566c4da2801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "19f68590-f3d8-40e1-ab45-75644eace9f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09e2fc1a-221b-43db-b90f-0566c4da2801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c59209d-ac4b-4897-a992-9a1e32b6046b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09e2fc1a-221b-43db-b90f-0566c4da2801",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "03afe0be-f358-4089-b769-cedc4efb4ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "c2feb8f8-c915-42cb-87b6-e60ed58d179a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03afe0be-f358-4089-b769-cedc4efb4ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8689c84-97c8-4403-8dc7-5ce2ce08c1a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03afe0be-f358-4089-b769-cedc4efb4ce3",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "88ba1c4b-11b0-4db5-b63f-dbe4d4f53a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "dd343506-75ba-4116-ac1f-3c025d7ae3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ba1c4b-11b0-4db5-b63f-dbe4d4f53a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e981c0d-5ca4-448d-b4bf-830e7a4f04db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ba1c4b-11b0-4db5-b63f-dbe4d4f53a1e",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "ffdda271-cfab-4738-9f56-7fc18748a8a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "cb39b33b-76ff-42b9-89fb-f04695f1b037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffdda271-cfab-4738-9f56-7fc18748a8a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf808305-e181-44c5-8c03-126adde9a829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffdda271-cfab-4738-9f56-7fc18748a8a0",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "e521242e-a554-44c6-9cf7-0140a9c46775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "84d3f4e5-79c5-4558-b043-d5b78561450a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e521242e-a554-44c6-9cf7-0140a9c46775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74527ea0-62fd-4cd2-9421-972354caf4ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e521242e-a554-44c6-9cf7-0140a9c46775",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "0a66920f-50bd-4258-9e38-8c5c8ce3f43d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "8d5ad0b5-9fdc-4b7d-8cee-79a3d3a7a70d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a66920f-50bd-4258-9e38-8c5c8ce3f43d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "549e81ba-b0e3-4794-bc48-f48011cec5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a66920f-50bd-4258-9e38-8c5c8ce3f43d",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "c05c9c57-b1cc-4fcb-befd-a774d5fdd8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "9c19ac8d-780b-4779-aa5f-9bf05aec15dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c05c9c57-b1cc-4fcb-befd-a774d5fdd8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f41283ad-1d29-40b7-a502-029289a7efb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c05c9c57-b1cc-4fcb-befd-a774d5fdd8ea",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "7f057a00-2ea0-4ede-b8b5-9b6bb9d9ff13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "f0aabf5a-2c19-4984-913c-b38b37787dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f057a00-2ea0-4ede-b8b5-9b6bb9d9ff13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "486387bd-25e2-4294-9539-5786011d3781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f057a00-2ea0-4ede-b8b5-9b6bb9d9ff13",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "d41be2f9-c34b-4dd2-a5e8-b629b29f2966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "03116e85-3d3e-4547-b195-c88a5f9b5f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d41be2f9-c34b-4dd2-a5e8-b629b29f2966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67eee328-27ad-4e26-9ad4-2d6acbce7cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d41be2f9-c34b-4dd2-a5e8-b629b29f2966",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "a2dc869c-1cd3-43e8-b1eb-71271081a43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "5d581725-469d-42ca-af96-1dc203fe5ea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2dc869c-1cd3-43e8-b1eb-71271081a43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1428ea55-0164-4a5f-bd74-d586b134f779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2dc869c-1cd3-43e8-b1eb-71271081a43c",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "0ffc6560-4726-4807-a65e-a8429207762c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "82d3c6b4-4bd4-442f-a3ed-a3f70141b416",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ffc6560-4726-4807-a65e-a8429207762c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e4dbac-96f9-401d-b5f3-ff4af49c4273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ffc6560-4726-4807-a65e-a8429207762c",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "9bcfc842-909f-498e-bd7d-488ec04339ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "e3a14114-4481-4fff-a1e6-38d70f66b1f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bcfc842-909f-498e-bd7d-488ec04339ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b59c71d-c3e8-4e8e-84d6-bcec6ccb7965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bcfc842-909f-498e-bd7d-488ec04339ed",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "d972ada5-7759-445f-a5a7-8145daf0ac3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "71de7e3f-711f-43bc-b86c-bc32121c0ce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d972ada5-7759-445f-a5a7-8145daf0ac3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6143f37-8a88-43c4-96af-1f336271e988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d972ada5-7759-445f-a5a7-8145daf0ac3c",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "5f7b6a08-0f28-4e8f-91a1-11884dc349d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "108fe3d6-3a22-4048-a0c2-a42d455f9fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7b6a08-0f28-4e8f-91a1-11884dc349d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb20b48b-fa17-4616-b066-f13c887beb77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7b6a08-0f28-4e8f-91a1-11884dc349d6",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "4854a54c-4220-4bb5-b7e6-3ffae2e6ea82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "4a71e5ab-d41e-4a07-b2c3-84a14c995bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4854a54c-4220-4bb5-b7e6-3ffae2e6ea82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b336710-1fca-4e54-9386-4dfa1362455a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4854a54c-4220-4bb5-b7e6-3ffae2e6ea82",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "8dcefadc-bb13-4c55-8366-6e7dd528d210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "240f95f1-ed78-4944-aa1d-5e85b626f92c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dcefadc-bb13-4c55-8366-6e7dd528d210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8d6fe1b-f8a2-489b-9756-cabfe5d15f7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dcefadc-bb13-4c55-8366-6e7dd528d210",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "fa98709c-e584-40fa-8ca0-44d85fa6a8d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d6063988-4f04-4f1f-aee7-41c89fa63d68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa98709c-e584-40fa-8ca0-44d85fa6a8d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9f1c2e5-11af-4025-9e4b-cc77137c534f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa98709c-e584-40fa-8ca0-44d85fa6a8d0",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "5d47827f-7b3f-4a65-b105-c92686e8577a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "a5c47973-f2e0-416c-8e9b-ea7cf92e8326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d47827f-7b3f-4a65-b105-c92686e8577a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "771d4caf-f868-493e-b88f-3e0fd0459fe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d47827f-7b3f-4a65-b105-c92686e8577a",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "a19fb6c8-27eb-42e7-92af-3c2f0c6dfdef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "e50569da-5361-4fa9-b63f-4ad698b1f80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a19fb6c8-27eb-42e7-92af-3c2f0c6dfdef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ace2c4-2da6-4a2e-b9cd-f3c71d5cf51f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a19fb6c8-27eb-42e7-92af-3c2f0c6dfdef",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "7e54be75-0d6c-4c6b-a0fe-24d873c9ade0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "c44ca5d3-c90a-4944-b018-9d44e0967f0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e54be75-0d6c-4c6b-a0fe-24d873c9ade0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7e896a-d521-4b72-a292-9ec109ffa01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e54be75-0d6c-4c6b-a0fe-24d873c9ade0",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "fb073bbd-4493-47ca-9e4d-5a0be17b0c49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "6a47a842-211b-45fa-8845-f0703b9cce06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb073bbd-4493-47ca-9e4d-5a0be17b0c49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11165202-a084-4877-9cd9-c193edf8f39f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb073bbd-4493-47ca-9e4d-5a0be17b0c49",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "450a4b15-03e4-4f68-9ffd-4403d8db5331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d4da710e-988b-41ed-8693-cf6422b88b0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "450a4b15-03e4-4f68-9ffd-4403d8db5331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7fedcec-c385-40aa-a2d0-385540c1069b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "450a4b15-03e4-4f68-9ffd-4403d8db5331",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "9618a8ff-e864-4889-a00a-21917e08549b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d9ce0234-ff19-4013-9fbd-92bb871a05c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9618a8ff-e864-4889-a00a-21917e08549b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feefd911-662d-4cd8-9b8b-cca1ac225381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9618a8ff-e864-4889-a00a-21917e08549b",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "83c27af6-6a43-498a-8a43-cbafe4962d73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "22539da9-595b-4de4-876f-131440b7fa73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c27af6-6a43-498a-8a43-cbafe4962d73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89bfc3f6-6aab-4ca7-931f-c220d3dafb3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c27af6-6a43-498a-8a43-cbafe4962d73",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "eb860f02-d575-4a16-aa22-416b41f751bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "4aa3978f-7c33-4bb6-91cf-c11a4e9461da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb860f02-d575-4a16-aa22-416b41f751bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13360b22-89e4-49c9-866a-ccaa28a8d08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb860f02-d575-4a16-aa22-416b41f751bf",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "e8466ff6-c5b7-4415-831b-82c6f6e91d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "ce96a9b4-9fe0-4d9f-87f1-eb86e51a1cf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8466ff6-c5b7-4415-831b-82c6f6e91d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686ddb32-33ab-4d74-bc57-9b7c8fea7d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8466ff6-c5b7-4415-831b-82c6f6e91d9a",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "9aea980c-0f10-4928-ba5c-ad6f982a427a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "90f13a9f-a1f4-4fbd-b890-5003b1b76a6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9aea980c-0f10-4928-ba5c-ad6f982a427a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72095a1c-9050-4132-8ecb-1f7166be849b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9aea980c-0f10-4928-ba5c-ad6f982a427a",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "81ae7b7a-ffa9-4059-921a-0222fbf128d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "5e64ffa1-e764-41f1-ab44-259a8aecc7d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ae7b7a-ffa9-4059-921a-0222fbf128d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e4ee40a-242a-4e75-95b1-3f942ee5212a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ae7b7a-ffa9-4059-921a-0222fbf128d1",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "b6c5610f-5c54-4b72-8280-4668edf1c04a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "c532c3de-08af-4f4c-a81b-ceec83c3b9d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c5610f-5c54-4b72-8280-4668edf1c04a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76113d4d-e498-422e-a4dc-450646e37a84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c5610f-5c54-4b72-8280-4668edf1c04a",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "6129bcaa-67f7-416a-93e3-c5ca78cb3b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "1038df3b-b408-4e3f-8ffd-7c79595f2476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6129bcaa-67f7-416a-93e3-c5ca78cb3b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266655b9-8acb-4a68-94cb-af8195bb4032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6129bcaa-67f7-416a-93e3-c5ca78cb3b31",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "80010521-e366-4df3-9ad1-e9ebe7f23add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "352a2942-7d6f-4279-b390-6b3226e1336d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80010521-e366-4df3-9ad1-e9ebe7f23add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65659d2e-4966-466c-8be2-24c94f1afac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80010521-e366-4df3-9ad1-e9ebe7f23add",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "6fe56c85-bd19-48c4-aa9f-e7d5e806d30a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d6c3862d-6ec7-44bb-a439-5aefc8a5082a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe56c85-bd19-48c4-aa9f-e7d5e806d30a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1312aec0-2968-4448-975f-490ef8938231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe56c85-bd19-48c4-aa9f-e7d5e806d30a",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "46403b51-9264-4cf3-b50c-1ab1dd589b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "ad223138-e3c0-4a53-977d-ad395981107d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46403b51-9264-4cf3-b50c-1ab1dd589b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2b7926-7ad5-408e-be49-7ac8a3012362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46403b51-9264-4cf3-b50c-1ab1dd589b2b",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "a07c0f4c-9d31-4241-8488-c549aedcbb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d6ea78ef-8aef-45e7-923f-9537a6065c2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07c0f4c-9d31-4241-8488-c549aedcbb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016c486c-9899-4291-a28d-65a4d489fbda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07c0f4c-9d31-4241-8488-c549aedcbb87",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "f198330f-f388-43b9-aacb-c014e29e3815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "22e69671-7a7c-4c63-a05c-44cc1d74fa87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f198330f-f388-43b9-aacb-c014e29e3815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5124b299-6fe9-4915-824e-227af58147b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f198330f-f388-43b9-aacb-c014e29e3815",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "5591ade3-799b-4a92-a19b-f92bab7f2fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "50838741-10b5-4995-8849-c87672481c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5591ade3-799b-4a92-a19b-f92bab7f2fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c6f376-89f5-43e3-b978-c8879e9078ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5591ade3-799b-4a92-a19b-f92bab7f2fed",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "e173278b-c2e8-4260-a0e6-4389cf53003e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "297256f5-6a13-424e-b6d7-5621a9c6efb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e173278b-c2e8-4260-a0e6-4389cf53003e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c8d302-1fa0-4513-a1d7-b5350dcaba91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e173278b-c2e8-4260-a0e6-4389cf53003e",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "bdf69b0e-1c6e-4c1c-9c84-0e4039f4a967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "0731aab8-34b5-42a9-a701-476d2feaa3be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf69b0e-1c6e-4c1c-9c84-0e4039f4a967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa8f27b2-e683-4b4e-af5a-d10a2b94befe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf69b0e-1c6e-4c1c-9c84-0e4039f4a967",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "807de6a0-e9b6-4b6c-bf5e-bb68d49603b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "1c7286aa-a37d-42f3-9917-e11410a46fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807de6a0-e9b6-4b6c-bf5e-bb68d49603b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bc24997-3c0d-4f0d-baef-e498b54b39c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807de6a0-e9b6-4b6c-bf5e-bb68d49603b2",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "81373f76-e69d-4e0c-aab6-86dd827ee0de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "2653e8a8-67c3-4400-abdc-04ee771ab206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81373f76-e69d-4e0c-aab6-86dd827ee0de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f48360-a285-439f-8500-46f3ae02faf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81373f76-e69d-4e0c-aab6-86dd827ee0de",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "93e9e5eb-ddbd-4487-a0e2-47a25c394823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "dabfe289-e95a-42da-a6dd-dc5d39ae567b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e9e5eb-ddbd-4487-a0e2-47a25c394823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f214e98e-4747-4c86-9c03-56a66fbeeac1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e9e5eb-ddbd-4487-a0e2-47a25c394823",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "6e08a4b2-d693-48d9-b5f7-8ad9a37b93bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "2ed784b3-9f2a-4395-91cb-bfc9f28833bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e08a4b2-d693-48d9-b5f7-8ad9a37b93bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d397a7d-5fcf-4e0d-8cda-8de75397595a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e08a4b2-d693-48d9-b5f7-8ad9a37b93bd",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "a6780c4c-cbc5-49b9-9274-6968edeecc46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "dccccbfc-b588-40f6-a73a-9c26c9b5d67f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6780c4c-cbc5-49b9-9274-6968edeecc46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0669871f-bdff-4cf5-8142-4ca3edb321a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6780c4c-cbc5-49b9-9274-6968edeecc46",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "c7ff5fac-5534-4248-886e-ea3962786485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "262e6261-6f18-4993-a46d-e215aaad4f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7ff5fac-5534-4248-886e-ea3962786485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e7ce661-4756-4e26-b94d-f77a51a498ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7ff5fac-5534-4248-886e-ea3962786485",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        },
        {
            "id": "fd919bc0-479f-42ba-b60b-b04d33afdb41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "compositeImage": {
                "id": "d3f4aa1c-b8ba-4f4a-ae4a-6dcc5951aa9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd919bc0-479f-42ba-b60b-b04d33afdb41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bec6494-dffa-402f-ab35-45000b1dcb36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd919bc0-479f-42ba-b60b-b04d33afdb41",
                    "LayerId": "3641965a-5606-43f6-8db4-0a6389135943"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "3641965a-5606-43f6-8db4-0a6389135943",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "320c4b2d-e9d2-48c2-9baa-863ff1ae925c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}