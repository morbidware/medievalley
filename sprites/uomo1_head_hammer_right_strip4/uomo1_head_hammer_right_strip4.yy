{
    "id": "0908a4e0-ca96-4492-b1bf-8a4a00e61506",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d097730-1eb6-4809-95b7-1ec52d585b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0908a4e0-ca96-4492-b1bf-8a4a00e61506",
            "compositeImage": {
                "id": "975bc9d3-0e33-4ee3-95e6-346b1b7b1bc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d097730-1eb6-4809-95b7-1ec52d585b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb391355-2be9-493e-8ba5-72683ac41d0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d097730-1eb6-4809-95b7-1ec52d585b24",
                    "LayerId": "6a68d90a-0f96-4f85-8689-35d5be7d7071"
                }
            ]
        },
        {
            "id": "bd01dee3-de81-4a17-8f60-b9dee89cec90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0908a4e0-ca96-4492-b1bf-8a4a00e61506",
            "compositeImage": {
                "id": "d3f942fe-1efd-4e05-8751-976341b28fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd01dee3-de81-4a17-8f60-b9dee89cec90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2287b223-5476-4804-ad52-7cd8f5ac3897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd01dee3-de81-4a17-8f60-b9dee89cec90",
                    "LayerId": "6a68d90a-0f96-4f85-8689-35d5be7d7071"
                }
            ]
        },
        {
            "id": "33603598-ae7e-4378-a5ae-ba5b64804821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0908a4e0-ca96-4492-b1bf-8a4a00e61506",
            "compositeImage": {
                "id": "570ec1e4-8c6c-47cd-966c-04b2c3855931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33603598-ae7e-4378-a5ae-ba5b64804821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772a9ea6-3adc-4536-92ea-3fbb0f81fc68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33603598-ae7e-4378-a5ae-ba5b64804821",
                    "LayerId": "6a68d90a-0f96-4f85-8689-35d5be7d7071"
                }
            ]
        },
        {
            "id": "869b8bbd-8950-4156-9106-53a6605beb00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0908a4e0-ca96-4492-b1bf-8a4a00e61506",
            "compositeImage": {
                "id": "e74c4caa-aece-4202-bd77-1dbd04038543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869b8bbd-8950-4156-9106-53a6605beb00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2feea3-4d42-4683-a05b-fac313b58d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869b8bbd-8950-4156-9106-53a6605beb00",
                    "LayerId": "6a68d90a-0f96-4f85-8689-35d5be7d7071"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6a68d90a-0f96-4f85-8689-35d5be7d7071",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0908a4e0-ca96-4492-b1bf-8a4a00e61506",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}