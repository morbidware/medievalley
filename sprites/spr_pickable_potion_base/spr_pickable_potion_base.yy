{
    "id": "94ed4311-6665-48b8-960e-4dfc3b065c17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potion_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e14f473-a04d-406e-93ed-7a5e375f3d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94ed4311-6665-48b8-960e-4dfc3b065c17",
            "compositeImage": {
                "id": "0d37543f-93e8-443d-8f9c-4aac43775c90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e14f473-a04d-406e-93ed-7a5e375f3d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6de185e-f303-4f45-836b-0f60f329f725",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e14f473-a04d-406e-93ed-7a5e375f3d3b",
                    "LayerId": "763c12f9-dcad-4452-a5b2-c7e7f3115379"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "763c12f9-dcad-4452-a5b2-c7e7f3115379",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94ed4311-6665-48b8-960e-4dfc3b065c17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}