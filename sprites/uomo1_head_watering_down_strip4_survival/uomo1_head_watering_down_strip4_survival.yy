{
    "id": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_watering_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01c0f736-7746-4a2a-a51a-33aa60e96123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "compositeImage": {
                "id": "9b9b4fc6-1525-4af7-a06f-099fa9fb0c68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c0f736-7746-4a2a-a51a-33aa60e96123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "166e3692-6c13-4894-9068-293e6ec1f4e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c0f736-7746-4a2a-a51a-33aa60e96123",
                    "LayerId": "9681522f-d2d9-47f8-87ac-9e72ad593e81"
                }
            ]
        },
        {
            "id": "f5db1ed1-f393-42d5-a40f-e51f9ee13e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "compositeImage": {
                "id": "97a0bf93-cc35-4c46-9639-dff7863c4ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5db1ed1-f393-42d5-a40f-e51f9ee13e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5c0fc8a-4ee7-4b41-94f3-95b29a47b482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5db1ed1-f393-42d5-a40f-e51f9ee13e5a",
                    "LayerId": "9681522f-d2d9-47f8-87ac-9e72ad593e81"
                }
            ]
        },
        {
            "id": "bf3fb248-e9b0-4b7f-8b98-a29c100cdec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "compositeImage": {
                "id": "61c44c44-f876-428d-8d11-5e13108c7fc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf3fb248-e9b0-4b7f-8b98-a29c100cdec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d15cbe-6005-4787-a6f1-7178bf17ad9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf3fb248-e9b0-4b7f-8b98-a29c100cdec6",
                    "LayerId": "9681522f-d2d9-47f8-87ac-9e72ad593e81"
                }
            ]
        },
        {
            "id": "16c489d4-8d51-4191-b621-3e69d6e25a54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "compositeImage": {
                "id": "32de132d-4a2c-4b52-9747-20a3cf4558c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16c489d4-8d51-4191-b621-3e69d6e25a54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec290929-fbde-4c0d-afbf-13f05a74c057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16c489d4-8d51-4191-b621-3e69d6e25a54",
                    "LayerId": "9681522f-d2d9-47f8-87ac-9e72ad593e81"
                }
            ]
        },
        {
            "id": "9c2bacb9-1991-4ebd-a4eb-f44e5f8ff626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "compositeImage": {
                "id": "be496074-1fe6-49fe-82a5-df5df54ecc35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2bacb9-1991-4ebd-a4eb-f44e5f8ff626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2da91588-70ef-429a-9a27-1b45fbbc6ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2bacb9-1991-4ebd-a4eb-f44e5f8ff626",
                    "LayerId": "9681522f-d2d9-47f8-87ac-9e72ad593e81"
                }
            ]
        },
        {
            "id": "21292012-b894-4d0f-adec-fad27e5bde0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "compositeImage": {
                "id": "a0f4111a-fd9d-42dc-b549-78266738e6d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21292012-b894-4d0f-adec-fad27e5bde0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53cf2b00-c29e-4c1f-a1f0-e1b2a8f9fd40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21292012-b894-4d0f-adec-fad27e5bde0f",
                    "LayerId": "9681522f-d2d9-47f8-87ac-9e72ad593e81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9681522f-d2d9-47f8-87ac-9e72ad593e81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "773960e4-29ac-4ad6-9f83-44d0d139cd54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}