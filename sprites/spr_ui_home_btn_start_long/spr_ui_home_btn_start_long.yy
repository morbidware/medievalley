{
    "id": "5f0a581a-4dfd-4e98-92ff-5358d9ad523e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_home_btn_start_long",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 174,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d629322-7b9e-4682-be43-78aa3977b0d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f0a581a-4dfd-4e98-92ff-5358d9ad523e",
            "compositeImage": {
                "id": "4cb90e76-01cc-4907-812d-50cafa7724a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d629322-7b9e-4682-be43-78aa3977b0d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35d3d3a-8c5d-4346-b323-90b6b8c856fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d629322-7b9e-4682-be43-78aa3977b0d1",
                    "LayerId": "ec87fe81-f72f-4c24-883a-e69803a3de04"
                }
            ]
        },
        {
            "id": "e6974f3d-0714-4425-9882-ebe3287e14e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f0a581a-4dfd-4e98-92ff-5358d9ad523e",
            "compositeImage": {
                "id": "bbef7703-9896-4ba2-819c-25fef949a72b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6974f3d-0714-4425-9882-ebe3287e14e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d97ce187-cc22-49b1-b6b2-cc3d63366b72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6974f3d-0714-4425-9882-ebe3287e14e4",
                    "LayerId": "ec87fe81-f72f-4c24-883a-e69803a3de04"
                }
            ]
        },
        {
            "id": "ba38e86a-8b0b-483d-b711-b1723c5d9e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f0a581a-4dfd-4e98-92ff-5358d9ad523e",
            "compositeImage": {
                "id": "a3f408ef-877e-4239-a312-acd231bcc666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba38e86a-8b0b-483d-b711-b1723c5d9e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56b77bf-3dfc-49e4-a9e7-d9d9c6ac6872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba38e86a-8b0b-483d-b711-b1723c5d9e6b",
                    "LayerId": "ec87fe81-f72f-4c24-883a-e69803a3de04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ec87fe81-f72f-4c24-883a-e69803a3de04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f0a581a-4dfd-4e98-92ff-5358d9ad523e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 175,
    "xorig": 87,
    "yorig": 9
}