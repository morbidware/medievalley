{
    "id": "89b1e019-318a-4668-b4c5-ae8b37e72b67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_tent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c7467ee-2238-4716-aecc-5e1c616ce7c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89b1e019-318a-4668-b4c5-ae8b37e72b67",
            "compositeImage": {
                "id": "70e89e66-ec00-4357-a1b0-2fb2f0bcf986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7467ee-2238-4716-aecc-5e1c616ce7c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843bbc43-9332-4158-baf0-f2d140d22e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7467ee-2238-4716-aecc-5e1c616ce7c0",
                    "LayerId": "259bcb62-dac2-450a-837b-3b8cbce203aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "259bcb62-dac2-450a-837b-3b8cbce203aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89b1e019-318a-4668-b4c5-ae8b37e72b67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}