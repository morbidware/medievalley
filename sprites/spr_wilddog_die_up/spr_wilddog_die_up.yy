{
    "id": "dc7d67c0-157b-4381-aed4-cd803e4799b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_die_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f653554e-86cb-40d8-81bc-6acbc11d1088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc7d67c0-157b-4381-aed4-cd803e4799b5",
            "compositeImage": {
                "id": "9b8ff897-22bb-4e59-924a-2140f2345544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f653554e-86cb-40d8-81bc-6acbc11d1088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3f980d-1bf9-4360-af1c-34fe7df8aa54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f653554e-86cb-40d8-81bc-6acbc11d1088",
                    "LayerId": "6eff4e6b-6fff-4572-ab50-ac0d6c887212"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6eff4e6b-6fff-4572-ab50-ac0d6c887212",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc7d67c0-157b-4381-aed4-cd803e4799b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}