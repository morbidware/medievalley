{
    "id": "a0781a3c-9d89-4967-bcc1-d48262d7a5dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_refined",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78d90617-eac3-433e-8556-06cb1e993544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0781a3c-9d89-4967-bcc1-d48262d7a5dd",
            "compositeImage": {
                "id": "de263c75-f37a-4212-b6f0-07b875dc2813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d90617-eac3-433e-8556-06cb1e993544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8ae43f-af8f-4917-be76-d4c5b1e8edcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d90617-eac3-433e-8556-06cb1e993544",
                    "LayerId": "96639fc9-fd6a-4196-8ada-87ffccbd7a3c"
                }
            ]
        },
        {
            "id": "3fd6864f-762c-4c63-a603-d0cfbf968a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0781a3c-9d89-4967-bcc1-d48262d7a5dd",
            "compositeImage": {
                "id": "1db83bda-b7c1-4e48-a16e-2967bcca4c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fd6864f-762c-4c63-a603-d0cfbf968a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bae67f2-eb4e-4461-b9ca-779a214c6ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fd6864f-762c-4c63-a603-d0cfbf968a40",
                    "LayerId": "96639fc9-fd6a-4196-8ada-87ffccbd7a3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "96639fc9-fd6a-4196-8ada-87ffccbd7a3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0781a3c-9d89-4967-bcc1-d48262d7a5dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}