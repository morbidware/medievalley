{
    "id": "4808b84f-d3e9-4622-8a9d-e1fc6b487971",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_joystickActionButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76a28a7d-4e8c-4b08-9589-d937d0292e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4808b84f-d3e9-4622-8a9d-e1fc6b487971",
            "compositeImage": {
                "id": "e3542b0a-63ca-4a53-82ba-67b70421b638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a28a7d-4e8c-4b08-9589-d937d0292e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e8c3ae2-3c18-49ad-a860-09f423c96012",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a28a7d-4e8c-4b08-9589-d937d0292e89",
                    "LayerId": "bae00adc-20f3-4258-8bb9-5486ad0b9f9d"
                },
                {
                    "id": "bca179fc-b2b8-4f7c-9537-57692223ccf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a28a7d-4e8c-4b08-9589-d937d0292e89",
                    "LayerId": "85fde532-3ead-4b72-8e0f-5652f3a01541"
                },
                {
                    "id": "017c4a01-f38d-4ec8-8aa7-1151136d520f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a28a7d-4e8c-4b08-9589-d937d0292e89",
                    "LayerId": "ba6daeea-a6be-471f-ac1f-bcd3fcc5b8b0"
                }
            ]
        },
        {
            "id": "240c851b-c333-4448-96af-cbed53628d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4808b84f-d3e9-4622-8a9d-e1fc6b487971",
            "compositeImage": {
                "id": "f5d6e6ef-2b70-437c-8930-8db39fa297f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "240c851b-c333-4448-96af-cbed53628d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14e21e17-8cc6-40ad-ba14-16e5298b31a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240c851b-c333-4448-96af-cbed53628d80",
                    "LayerId": "85fde532-3ead-4b72-8e0f-5652f3a01541"
                },
                {
                    "id": "b9902273-05e3-44a4-ac68-cb5cbdf106b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240c851b-c333-4448-96af-cbed53628d80",
                    "LayerId": "ba6daeea-a6be-471f-ac1f-bcd3fcc5b8b0"
                },
                {
                    "id": "272a1207-151b-4469-b2a5-d2f9a71e11b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "240c851b-c333-4448-96af-cbed53628d80",
                    "LayerId": "bae00adc-20f3-4258-8bb9-5486ad0b9f9d"
                }
            ]
        }
    ],
    "gridX": 36,
    "gridY": 36,
    "height": 80,
    "layers": [
        {
            "id": "85fde532-3ead-4b72-8e0f-5652f3a01541",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4808b84f-d3e9-4622-8a9d-e1fc6b487971",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ba6daeea-a6be-471f-ac1f-bcd3fcc5b8b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4808b84f-d3e9-4622-8a9d-e1fc6b487971",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bae00adc-20f3-4258-8bb9-5486ad0b9f9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4808b84f-d3e9-4622-8a9d-e1fc6b487971",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 36
}