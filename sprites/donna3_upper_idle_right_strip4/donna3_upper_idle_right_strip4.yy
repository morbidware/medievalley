{
    "id": "584f9bad-79b1-45d5-a0cc-6c5c8e7d8a2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6870afa0-f572-41ff-b774-cdadc150bb9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "584f9bad-79b1-45d5-a0cc-6c5c8e7d8a2c",
            "compositeImage": {
                "id": "fd8a71d4-7489-4a87-817b-e21850568214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6870afa0-f572-41ff-b774-cdadc150bb9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef20f19c-14f8-4971-b7dd-8a4bd0d5571a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6870afa0-f572-41ff-b774-cdadc150bb9b",
                    "LayerId": "518d0f06-8111-4a62-8f45-27718b8d0be2"
                }
            ]
        },
        {
            "id": "943d4322-a174-42b4-9c38-ba113f98bbc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "584f9bad-79b1-45d5-a0cc-6c5c8e7d8a2c",
            "compositeImage": {
                "id": "9a168931-96b3-4fdf-8886-fe3c6ba8915d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943d4322-a174-42b4-9c38-ba113f98bbc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "245148f3-5c14-463f-9f49-12cac7aeec23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943d4322-a174-42b4-9c38-ba113f98bbc5",
                    "LayerId": "518d0f06-8111-4a62-8f45-27718b8d0be2"
                }
            ]
        },
        {
            "id": "ac59261e-abe1-49a4-936a-00612c674111",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "584f9bad-79b1-45d5-a0cc-6c5c8e7d8a2c",
            "compositeImage": {
                "id": "4ac287b7-882f-4658-b713-cbf2c69d084e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac59261e-abe1-49a4-936a-00612c674111",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ffdcd9-c929-4f32-b9db-806d56348422",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac59261e-abe1-49a4-936a-00612c674111",
                    "LayerId": "518d0f06-8111-4a62-8f45-27718b8d0be2"
                }
            ]
        },
        {
            "id": "7144fc5f-5564-4995-a3ab-aee450f161bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "584f9bad-79b1-45d5-a0cc-6c5c8e7d8a2c",
            "compositeImage": {
                "id": "d6d68971-ee8c-467d-9158-0490b554cbe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7144fc5f-5564-4995-a3ab-aee450f161bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6a39bb-9bdc-4b8b-b62f-eb5c7b42a41f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7144fc5f-5564-4995-a3ab-aee450f161bc",
                    "LayerId": "518d0f06-8111-4a62-8f45-27718b8d0be2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "518d0f06-8111-4a62-8f45-27718b8d0be2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "584f9bad-79b1-45d5-a0cc-6c5c8e7d8a2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}