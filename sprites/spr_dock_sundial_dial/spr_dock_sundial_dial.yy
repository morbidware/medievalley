{
    "id": "4bd1e9d1-d688-4ce8-8423-cded5d2ed7f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_sundial_dial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4099ab1b-e8a5-45c0-a8e1-00c7ff60e524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bd1e9d1-d688-4ce8-8423-cded5d2ed7f5",
            "compositeImage": {
                "id": "199dcbeb-d044-4509-92d9-3dda50314abb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4099ab1b-e8a5-45c0-a8e1-00c7ff60e524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78638b02-fc95-44d1-a409-666f9760f42e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4099ab1b-e8a5-45c0-a8e1-00c7ff60e524",
                    "LayerId": "ea39c1dd-1ef0-4727-9cf7-c3a4c55f1e18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "ea39c1dd-1ef0-4727-9cf7-c3a4c55f1e18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bd1e9d1-d688-4ce8-8423-cded5d2ed7f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}