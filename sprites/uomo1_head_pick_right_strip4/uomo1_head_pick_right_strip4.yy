{
    "id": "f1486a04-e097-4a69-be3e-9fd3ed497b01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "364f0616-2d7f-439e-a4e8-de5be6684e84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1486a04-e097-4a69-be3e-9fd3ed497b01",
            "compositeImage": {
                "id": "1370495d-c809-4e7a-97f2-c0be0019c2c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "364f0616-2d7f-439e-a4e8-de5be6684e84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cbdf9db-fa8a-4259-a778-673eb5814ebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "364f0616-2d7f-439e-a4e8-de5be6684e84",
                    "LayerId": "3f968876-b967-4c15-a829-4007811abaf1"
                }
            ]
        },
        {
            "id": "aea1acd0-7ec7-4f5e-b99f-3e3112d4f2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1486a04-e097-4a69-be3e-9fd3ed497b01",
            "compositeImage": {
                "id": "0754b451-2085-49bd-9d80-eb48c9bed5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea1acd0-7ec7-4f5e-b99f-3e3112d4f2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13386c01-3b60-41c8-a3e8-723096a08df8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea1acd0-7ec7-4f5e-b99f-3e3112d4f2e3",
                    "LayerId": "3f968876-b967-4c15-a829-4007811abaf1"
                }
            ]
        },
        {
            "id": "e7916b86-55af-4232-ad0c-d86c4efbf6fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1486a04-e097-4a69-be3e-9fd3ed497b01",
            "compositeImage": {
                "id": "e5ccdb55-d335-4cf1-a994-869c2fd5826c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7916b86-55af-4232-ad0c-d86c4efbf6fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "802e965c-5ad9-435e-836a-30252ab1e9e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7916b86-55af-4232-ad0c-d86c4efbf6fe",
                    "LayerId": "3f968876-b967-4c15-a829-4007811abaf1"
                }
            ]
        },
        {
            "id": "a7fba83b-418b-4339-818f-c02af4865f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1486a04-e097-4a69-be3e-9fd3ed497b01",
            "compositeImage": {
                "id": "340c3104-6e6a-459d-adaf-d46e174e633d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fba83b-418b-4339-818f-c02af4865f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd08e9b8-253c-4b90-bf58-0e7388e03d41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fba83b-418b-4339-818f-c02af4865f24",
                    "LayerId": "3f968876-b967-4c15-a829-4007811abaf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3f968876-b967-4c15-a829-4007811abaf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1486a04-e097-4a69-be3e-9fd3ed497b01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}