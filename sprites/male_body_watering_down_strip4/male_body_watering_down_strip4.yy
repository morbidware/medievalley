{
    "id": "2f7b3f0c-6309-4739-a926-4bfb0aeecb7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2568133-a1ee-480f-9ff2-80d0fe570357",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7b3f0c-6309-4739-a926-4bfb0aeecb7e",
            "compositeImage": {
                "id": "c0bb6592-9523-4a4c-bc47-9cd89c97d5c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2568133-a1ee-480f-9ff2-80d0fe570357",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18646f1-4eec-436e-82b5-0710b4b76af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2568133-a1ee-480f-9ff2-80d0fe570357",
                    "LayerId": "ca1c33b5-ddd3-45f0-b068-c7c3de61c00a"
                }
            ]
        },
        {
            "id": "39b0968f-404e-49dd-ad0e-f7f749785282",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7b3f0c-6309-4739-a926-4bfb0aeecb7e",
            "compositeImage": {
                "id": "d6ef6425-2d5b-4638-b307-cee64944f333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39b0968f-404e-49dd-ad0e-f7f749785282",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4abd26fe-976b-4cda-97de-9929b36a6cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39b0968f-404e-49dd-ad0e-f7f749785282",
                    "LayerId": "ca1c33b5-ddd3-45f0-b068-c7c3de61c00a"
                }
            ]
        },
        {
            "id": "41352ad7-1ed8-442e-9710-15c3c5ff7840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7b3f0c-6309-4739-a926-4bfb0aeecb7e",
            "compositeImage": {
                "id": "e4ed0413-6285-4d38-85fe-bd04d071ed1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41352ad7-1ed8-442e-9710-15c3c5ff7840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6bd563d-fbfd-4a01-a60f-d5973eb0cf02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41352ad7-1ed8-442e-9710-15c3c5ff7840",
                    "LayerId": "ca1c33b5-ddd3-45f0-b068-c7c3de61c00a"
                }
            ]
        },
        {
            "id": "64a282b4-8722-45d8-a2ad-3d08889be0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f7b3f0c-6309-4739-a926-4bfb0aeecb7e",
            "compositeImage": {
                "id": "34f48e05-5363-4d2c-a0cc-2d9dc3af8424",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a282b4-8722-45d8-a2ad-3d08889be0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc573ef3-74a3-4b3a-a590-edc578d44264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a282b4-8722-45d8-a2ad-3d08889be0aa",
                    "LayerId": "ca1c33b5-ddd3-45f0-b068-c7c3de61c00a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ca1c33b5-ddd3-45f0-b068-c7c3de61c00a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f7b3f0c-6309-4739-a926-4bfb0aeecb7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}