{
    "id": "31341b64-9eeb-43f3-b6ce-52548698f5a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_die_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a93f49c-79ad-4b51-beda-3b0404a3c8f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31341b64-9eeb-43f3-b6ce-52548698f5a1",
            "compositeImage": {
                "id": "f1c3d31a-d366-4c1b-9dd1-c1e5ace0153b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a93f49c-79ad-4b51-beda-3b0404a3c8f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff46cef-450d-4f22-8933-be86800c86f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a93f49c-79ad-4b51-beda-3b0404a3c8f5",
                    "LayerId": "4bd49529-15c9-4e99-bbdb-3a07c336047e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4bd49529-15c9-4e99-bbdb-3a07c336047e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31341b64-9eeb-43f3-b6ce-52548698f5a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}