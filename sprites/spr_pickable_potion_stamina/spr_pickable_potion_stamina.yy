{
    "id": "8d889956-f017-4bb7-bd9b-dfcb46a11700",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potion_stamina",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c32f918-1079-4a72-95b7-5d45cc0f3819",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d889956-f017-4bb7-bd9b-dfcb46a11700",
            "compositeImage": {
                "id": "94d1b58d-594d-424e-bfbc-951cb7f33671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c32f918-1079-4a72-95b7-5d45cc0f3819",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a821d2b-3d8c-4142-b279-719a18c9bc46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c32f918-1079-4a72-95b7-5d45cc0f3819",
                    "LayerId": "633eaaf4-ff4f-45e5-8d4a-c623475cc854"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "633eaaf4-ff4f-45e5-8d4a-c623475cc854",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d889956-f017-4bb7-bd9b-dfcb46a11700",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}