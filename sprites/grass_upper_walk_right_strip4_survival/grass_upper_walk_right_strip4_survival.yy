{
    "id": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_walk_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a4f0299-9685-4005-b781-4c83eb188338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "compositeImage": {
                "id": "2a320dbe-544e-4eed-85b2-6b3a378d912d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a4f0299-9685-4005-b781-4c83eb188338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91606cd2-b3a3-4dbc-9edb-6d181c71a499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a4f0299-9685-4005-b781-4c83eb188338",
                    "LayerId": "9d2459f1-6539-4d46-84eb-368c36dfbba0"
                }
            ]
        },
        {
            "id": "07764699-35dc-41ae-b42e-f4a674b494b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "compositeImage": {
                "id": "18f9f5b6-d66a-4b5d-9cac-5d4d673c7ba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07764699-35dc-41ae-b42e-f4a674b494b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2469d99-91f9-431a-9ae8-56d478ee8c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07764699-35dc-41ae-b42e-f4a674b494b2",
                    "LayerId": "9d2459f1-6539-4d46-84eb-368c36dfbba0"
                }
            ]
        },
        {
            "id": "328c29cd-18ae-4a0d-9dfb-e390f73a5a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "compositeImage": {
                "id": "0d63f658-7862-4c66-a702-f5c71ab01c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "328c29cd-18ae-4a0d-9dfb-e390f73a5a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d7db131-7f5f-470b-b773-e76137bdddfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "328c29cd-18ae-4a0d-9dfb-e390f73a5a78",
                    "LayerId": "9d2459f1-6539-4d46-84eb-368c36dfbba0"
                }
            ]
        },
        {
            "id": "98ba6032-b34a-468a-8bd8-02f3e5a5582f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "compositeImage": {
                "id": "770ceb6d-4a22-45bd-a038-1b32731ee644",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ba6032-b34a-468a-8bd8-02f3e5a5582f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02a46a55-246b-4c78-8bcd-603f67747c4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ba6032-b34a-468a-8bd8-02f3e5a5582f",
                    "LayerId": "9d2459f1-6539-4d46-84eb-368c36dfbba0"
                }
            ]
        },
        {
            "id": "90f3a7bb-581c-4594-8e6c-3481ffea28ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "compositeImage": {
                "id": "7c90e52a-59ec-4748-8781-9f85cddff691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f3a7bb-581c-4594-8e6c-3481ffea28ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3601285d-c5f2-4d70-a95d-8d2573aad053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f3a7bb-581c-4594-8e6c-3481ffea28ac",
                    "LayerId": "9d2459f1-6539-4d46-84eb-368c36dfbba0"
                }
            ]
        },
        {
            "id": "7ba54ea8-aaae-4ecd-8b2a-fd7ca5621275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "compositeImage": {
                "id": "92a59c0c-57ef-47ab-8252-b65339424e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ba54ea8-aaae-4ecd-8b2a-fd7ca5621275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe63637-3b28-4ebe-9fba-ec30eaabe7c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ba54ea8-aaae-4ecd-8b2a-fd7ca5621275",
                    "LayerId": "9d2459f1-6539-4d46-84eb-368c36dfbba0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9d2459f1-6539-4d46-84eb-368c36dfbba0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50932bbb-de36-4e75-91dd-f9d6508bfcf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}