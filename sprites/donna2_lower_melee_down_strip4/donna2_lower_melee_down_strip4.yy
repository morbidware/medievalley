{
    "id": "5dd41cfe-4b27-4c43-b201-a8c882e9cdf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9080b479-36bc-4ec4-b60e-60b73b378b95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd41cfe-4b27-4c43-b201-a8c882e9cdf1",
            "compositeImage": {
                "id": "0331d5ba-4a82-4145-998b-d32a857993fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9080b479-36bc-4ec4-b60e-60b73b378b95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6b8b776-f0b4-4aeb-9e4a-9c6bfc5aeddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9080b479-36bc-4ec4-b60e-60b73b378b95",
                    "LayerId": "c426eb7d-8508-4e74-85c0-aa343577716e"
                }
            ]
        },
        {
            "id": "6d5a0b47-1f39-40bb-9c3e-53223aa76a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd41cfe-4b27-4c43-b201-a8c882e9cdf1",
            "compositeImage": {
                "id": "1b22c556-6883-4165-b24e-929020b89d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d5a0b47-1f39-40bb-9c3e-53223aa76a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bdc0cfd-8c7c-431d-bfef-2f8d44a0c5c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d5a0b47-1f39-40bb-9c3e-53223aa76a2a",
                    "LayerId": "c426eb7d-8508-4e74-85c0-aa343577716e"
                }
            ]
        },
        {
            "id": "036ba3aa-f79b-4597-8701-e0181b6aeb96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd41cfe-4b27-4c43-b201-a8c882e9cdf1",
            "compositeImage": {
                "id": "92101b91-ca34-4ad2-8a93-7819444fbf8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "036ba3aa-f79b-4597-8701-e0181b6aeb96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a17407-0675-4475-a715-45b4c8174181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "036ba3aa-f79b-4597-8701-e0181b6aeb96",
                    "LayerId": "c426eb7d-8508-4e74-85c0-aa343577716e"
                }
            ]
        },
        {
            "id": "a9d6081c-d68e-43eb-a20e-0b8b41b4bdfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd41cfe-4b27-4c43-b201-a8c882e9cdf1",
            "compositeImage": {
                "id": "5e29d45d-14f9-4b27-93cb-cfc99256cdc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9d6081c-d68e-43eb-a20e-0b8b41b4bdfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7badd2-bd4e-4e5d-a100-dbd1bbf754d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9d6081c-d68e-43eb-a20e-0b8b41b4bdfb",
                    "LayerId": "c426eb7d-8508-4e74-85c0-aa343577716e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c426eb7d-8508-4e74-85c0-aa343577716e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5dd41cfe-4b27-4c43-b201-a8c882e9cdf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}