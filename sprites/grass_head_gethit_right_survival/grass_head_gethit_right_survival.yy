{
    "id": "845ef21f-8721-4903-9e2b-a5968cc47a79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_gethit_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aa45baf-9e28-4598-9e15-93d0d931563a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845ef21f-8721-4903-9e2b-a5968cc47a79",
            "compositeImage": {
                "id": "6b148516-358e-4500-9b28-e2010e53b573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aa45baf-9e28-4598-9e15-93d0d931563a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c03a25-4af2-499f-80e0-1365dd4b095d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aa45baf-9e28-4598-9e15-93d0d931563a",
                    "LayerId": "0132bf05-ee9d-4534-b6ff-d32cf88d3156"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0132bf05-ee9d-4534-b6ff-d32cf88d3156",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "845ef21f-8721-4903-9e2b-a5968cc47a79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}