{
    "id": "5f3f5cfa-d81f-413c-a89d-fe363d975e63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_challenge_malus_modifier_minus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80caba8f-a09c-42cc-82d6-f86481c5b28a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f3f5cfa-d81f-413c-a89d-fe363d975e63",
            "compositeImage": {
                "id": "086bd82f-f971-4c45-b727-62d44c1a54fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80caba8f-a09c-42cc-82d6-f86481c5b28a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f278a69a-af90-4d88-b9fe-f488d38875b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80caba8f-a09c-42cc-82d6-f86481c5b28a",
                    "LayerId": "914d3a65-fd99-4603-8b92-cfbd4f43e015"
                },
                {
                    "id": "af00dcdf-d96c-4f20-8ce5-e95118a181e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80caba8f-a09c-42cc-82d6-f86481c5b28a",
                    "LayerId": "a2625c6d-1b1b-4322-b031-9cf17074eb24"
                }
            ]
        },
        {
            "id": "b11a05c4-e03c-4249-bb00-6e3a1c9bb1d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f3f5cfa-d81f-413c-a89d-fe363d975e63",
            "compositeImage": {
                "id": "b9727432-0bf8-416c-aad9-e418904944a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11a05c4-e03c-4249-bb00-6e3a1c9bb1d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6adf9565-c94f-42be-8bf4-ab9077313230",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11a05c4-e03c-4249-bb00-6e3a1c9bb1d0",
                    "LayerId": "914d3a65-fd99-4603-8b92-cfbd4f43e015"
                },
                {
                    "id": "82731fd6-1cc5-40ae-bef1-801abbfd86e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11a05c4-e03c-4249-bb00-6e3a1c9bb1d0",
                    "LayerId": "a2625c6d-1b1b-4322-b031-9cf17074eb24"
                }
            ]
        },
        {
            "id": "9bc0afbf-5bc5-4b21-9e76-9182364a49b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f3f5cfa-d81f-413c-a89d-fe363d975e63",
            "compositeImage": {
                "id": "9d69ca71-9f32-4ff2-92c2-53287f610612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc0afbf-5bc5-4b21-9e76-9182364a49b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5398743-afdc-4574-9d18-41d75ae27cf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc0afbf-5bc5-4b21-9e76-9182364a49b6",
                    "LayerId": "914d3a65-fd99-4603-8b92-cfbd4f43e015"
                },
                {
                    "id": "d166cbab-ee04-48d0-bea1-806cccc449af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc0afbf-5bc5-4b21-9e76-9182364a49b6",
                    "LayerId": "a2625c6d-1b1b-4322-b031-9cf17074eb24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a2625c6d-1b1b-4322-b031-9cf17074eb24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f3f5cfa-d81f-413c-a89d-fe363d975e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "914d3a65-fd99-4603-8b92-cfbd4f43e015",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f3f5cfa-d81f-413c-a89d-fe363d975e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}