{
    "id": "882db6b6-bd32-496a-ad39-e846e55001c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "749862fb-9d56-43e0-8bf5-0a047fb13137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882db6b6-bd32-496a-ad39-e846e55001c4",
            "compositeImage": {
                "id": "a43a265d-1bbf-44d1-bd23-0b06ad821af6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749862fb-9d56-43e0-8bf5-0a047fb13137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f24a66a5-99e7-4df8-8781-83a3f66265a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749862fb-9d56-43e0-8bf5-0a047fb13137",
                    "LayerId": "a9a3920d-0bf2-4386-98f7-cfebefb44535"
                }
            ]
        },
        {
            "id": "5c00c73d-b6d1-4d26-a282-609122772d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882db6b6-bd32-496a-ad39-e846e55001c4",
            "compositeImage": {
                "id": "14462400-9608-4263-aa1c-5ee7f0014ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c00c73d-b6d1-4d26-a282-609122772d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9859264-f39e-4d8b-976e-24db300f7ed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c00c73d-b6d1-4d26-a282-609122772d4b",
                    "LayerId": "a9a3920d-0bf2-4386-98f7-cfebefb44535"
                }
            ]
        },
        {
            "id": "fe4b9c22-1b77-43b3-8b7b-0002a71dbf76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882db6b6-bd32-496a-ad39-e846e55001c4",
            "compositeImage": {
                "id": "9ca02048-e933-4573-ac53-fd555cd3479b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4b9c22-1b77-43b3-8b7b-0002a71dbf76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "295568f6-9c03-4c5c-8a20-5c0e5f54de13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4b9c22-1b77-43b3-8b7b-0002a71dbf76",
                    "LayerId": "a9a3920d-0bf2-4386-98f7-cfebefb44535"
                }
            ]
        },
        {
            "id": "b20b9115-1f79-4fc3-a8d1-748df76b2efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "882db6b6-bd32-496a-ad39-e846e55001c4",
            "compositeImage": {
                "id": "d7ae9b4e-0c3c-4e31-9929-ced4a0a541f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20b9115-1f79-4fc3-a8d1-748df76b2efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6fcff3f-2e5f-44b8-9890-f43b9ac2c1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20b9115-1f79-4fc3-a8d1-748df76b2efd",
                    "LayerId": "a9a3920d-0bf2-4386-98f7-cfebefb44535"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9a3920d-0bf2-4386-98f7-cfebefb44535",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "882db6b6-bd32-496a-ad39-e846e55001c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}