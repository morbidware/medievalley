{
    "id": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_beechtree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 6,
    "bbox_right": 53,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8bad79e-3d27-47e7-acf2-6c6af985a17d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
            "compositeImage": {
                "id": "88c92947-ec69-4164-83a2-37dfdff7b81c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8bad79e-3d27-47e7-acf2-6c6af985a17d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "142707e5-b473-40d6-8339-dd489c9933d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8bad79e-3d27-47e7-acf2-6c6af985a17d",
                    "LayerId": "4dc91877-4030-4b88-9a91-1a171ec6a30a"
                }
            ]
        },
        {
            "id": "60a401c6-6511-4d78-ab48-95a7f30d5809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
            "compositeImage": {
                "id": "57fd0fc0-edb9-4048-ac1e-bda7539ad157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a401c6-6511-4d78-ab48-95a7f30d5809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04eba9e0-22fb-483b-b5be-c1c16fe17f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a401c6-6511-4d78-ab48-95a7f30d5809",
                    "LayerId": "4dc91877-4030-4b88-9a91-1a171ec6a30a"
                }
            ]
        },
        {
            "id": "e0dbe285-ac09-4ac4-a6e9-49eef74a492f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
            "compositeImage": {
                "id": "a7349f7a-f1f6-4dde-934e-89e9264e669b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0dbe285-ac09-4ac4-a6e9-49eef74a492f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcb3f023-8f80-41b8-9285-15b181acd912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0dbe285-ac09-4ac4-a6e9-49eef74a492f",
                    "LayerId": "4dc91877-4030-4b88-9a91-1a171ec6a30a"
                }
            ]
        },
        {
            "id": "0f6d8f5f-0116-4e50-8a5e-597c862f2478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
            "compositeImage": {
                "id": "cfe1cc53-e246-4623-8ef7-b59d96b4fffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f6d8f5f-0116-4e50-8a5e-597c862f2478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c71a168-14c1-452b-bcf6-d6233ee6168b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f6d8f5f-0116-4e50-8a5e-597c862f2478",
                    "LayerId": "4dc91877-4030-4b88-9a91-1a171ec6a30a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "4dc91877-4030-4b88-9a91-1a171ec6a30a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fa01577-eb06-454a-ba74-1e9cd70f79d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 29,
    "yorig": 76
}