{
    "id": "af9b9790-cc18-465d-b246-bddc41fea546",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_idle_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12ab2eaf-0904-4a08-9d6e-87ca42cce367",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af9b9790-cc18-465d-b246-bddc41fea546",
            "compositeImage": {
                "id": "df25b98f-5802-4216-a44a-c77670771f5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ab2eaf-0904-4a08-9d6e-87ca42cce367",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a292df91-fa3d-492c-b95e-9b5d129a6b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ab2eaf-0904-4a08-9d6e-87ca42cce367",
                    "LayerId": "0bcd31cb-8137-4135-b2ca-e418013b7766"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "0bcd31cb-8137-4135-b2ca-e418013b7766",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af9b9790-cc18-465d-b246-bddc41fea546",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}