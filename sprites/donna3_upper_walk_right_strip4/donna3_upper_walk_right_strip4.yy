{
    "id": "92c9704e-2740-459d-8484-9057114a6f4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa9e654c-73b2-4314-a4d6-30d3021bfa8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c9704e-2740-459d-8484-9057114a6f4f",
            "compositeImage": {
                "id": "4406387f-57a5-4f81-9d20-d1800cc6ef30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa9e654c-73b2-4314-a4d6-30d3021bfa8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f32185c0-f427-4753-a60f-4ea9798e1572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa9e654c-73b2-4314-a4d6-30d3021bfa8e",
                    "LayerId": "7a7c3a17-da29-4f33-98ba-ac5d7ac72b7f"
                }
            ]
        },
        {
            "id": "514bbd35-9100-4213-a702-92a5cb5939bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c9704e-2740-459d-8484-9057114a6f4f",
            "compositeImage": {
                "id": "5b2bf4d3-9e52-4a1d-a35a-8723196f2ef5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "514bbd35-9100-4213-a702-92a5cb5939bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19d016a8-c796-4f3e-a4dd-caca08dd6d2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "514bbd35-9100-4213-a702-92a5cb5939bc",
                    "LayerId": "7a7c3a17-da29-4f33-98ba-ac5d7ac72b7f"
                }
            ]
        },
        {
            "id": "14b466ae-7b40-46ad-852d-e37061e3d3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c9704e-2740-459d-8484-9057114a6f4f",
            "compositeImage": {
                "id": "1d578fb2-d8cc-41b5-86e4-724d5a8b48a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b466ae-7b40-46ad-852d-e37061e3d3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e714b927-afc9-4bdb-b823-d4174ff5abf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b466ae-7b40-46ad-852d-e37061e3d3c6",
                    "LayerId": "7a7c3a17-da29-4f33-98ba-ac5d7ac72b7f"
                }
            ]
        },
        {
            "id": "f3a243a4-7da4-4264-8ef4-e6f092c48c91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92c9704e-2740-459d-8484-9057114a6f4f",
            "compositeImage": {
                "id": "854c18c3-4ed0-42f1-9719-e3b0b13aecc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3a243a4-7da4-4264-8ef4-e6f092c48c91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b7d837-709a-4842-a51d-8d1d76751984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3a243a4-7da4-4264-8ef4-e6f092c48c91",
                    "LayerId": "7a7c3a17-da29-4f33-98ba-ac5d7ac72b7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a7c3a17-da29-4f33-98ba-ac5d7ac72b7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92c9704e-2740-459d-8484-9057114a6f4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}