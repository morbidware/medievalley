{
    "id": "f045ea20-fcaa-40a4-bc2c-e2f4d846fc8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33e9b190-3392-4ff4-adcd-3ae5763c96e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f045ea20-fcaa-40a4-bc2c-e2f4d846fc8c",
            "compositeImage": {
                "id": "55e95139-b38e-4224-835c-af9563b486e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33e9b190-3392-4ff4-adcd-3ae5763c96e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e79aa6d-0b09-4ea0-b086-cb90f22d38a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33e9b190-3392-4ff4-adcd-3ae5763c96e6",
                    "LayerId": "018c40e6-62b5-46d2-8ee7-51dc9544522a"
                }
            ]
        },
        {
            "id": "a95ece7c-28af-4249-8f4d-151a6c61d59a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f045ea20-fcaa-40a4-bc2c-e2f4d846fc8c",
            "compositeImage": {
                "id": "b545910a-4313-4e3b-82c6-249206a3093c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a95ece7c-28af-4249-8f4d-151a6c61d59a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab5efa7-847d-4e58-adc9-16aedae75226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a95ece7c-28af-4249-8f4d-151a6c61d59a",
                    "LayerId": "018c40e6-62b5-46d2-8ee7-51dc9544522a"
                }
            ]
        },
        {
            "id": "034fe637-5060-4d81-ab0e-e4cd53534ba2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f045ea20-fcaa-40a4-bc2c-e2f4d846fc8c",
            "compositeImage": {
                "id": "fc881d04-8997-40f8-9b5f-7c6db643649a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "034fe637-5060-4d81-ab0e-e4cd53534ba2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec24aad0-1b23-4612-adae-95aa4cc22b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "034fe637-5060-4d81-ab0e-e4cd53534ba2",
                    "LayerId": "018c40e6-62b5-46d2-8ee7-51dc9544522a"
                }
            ]
        },
        {
            "id": "19fd5b62-9204-422d-aa43-2a23f012deca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f045ea20-fcaa-40a4-bc2c-e2f4d846fc8c",
            "compositeImage": {
                "id": "bbe6a3aa-4e44-4159-836d-271f4d1c5d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fd5b62-9204-422d-aa43-2a23f012deca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b325373d-6b3f-4a49-bb7b-d88685c52aad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fd5b62-9204-422d-aa43-2a23f012deca",
                    "LayerId": "018c40e6-62b5-46d2-8ee7-51dc9544522a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "018c40e6-62b5-46d2-8ee7-51dc9544522a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f045ea20-fcaa-40a4-bc2c-e2f4d846fc8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}