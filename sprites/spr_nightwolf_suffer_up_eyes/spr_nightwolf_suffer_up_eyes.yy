{
    "id": "f05927e5-c061-4f9b-b1a2-ff78d482dae1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_suffer_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "925ef77e-683f-4b47-9877-fbcdab2a1adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f05927e5-c061-4f9b-b1a2-ff78d482dae1",
            "compositeImage": {
                "id": "e7f4f489-d8ef-4f64-9cc4-30ba08bbcfd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "925ef77e-683f-4b47-9877-fbcdab2a1adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e903e3af-d47d-4622-8557-ce1d7e686100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "925ef77e-683f-4b47-9877-fbcdab2a1adf",
                    "LayerId": "71b5e14d-ad95-4556-93c2-d4ba23a231d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "71b5e14d-ad95-4556-93c2-d4ba23a231d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f05927e5-c061-4f9b-b1a2-ff78d482dae1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}