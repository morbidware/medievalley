{
    "id": "3b157d9e-0044-4ea5-870b-d0d0d84c8a06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_selection",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd0ba963-aa0b-4516-99d5-f47507707162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b157d9e-0044-4ea5-870b-d0d0d84c8a06",
            "compositeImage": {
                "id": "f51e9267-0715-48ad-a857-0549f3afb5de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd0ba963-aa0b-4516-99d5-f47507707162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41872499-e5a5-496b-9de4-b67e661f9dde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd0ba963-aa0b-4516-99d5-f47507707162",
                    "LayerId": "373d6945-d66f-46b3-b08e-336c7f499c15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "373d6945-d66f-46b3-b08e-336c7f499c15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b157d9e-0044-4ea5-870b-d0d0d84c8a06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}