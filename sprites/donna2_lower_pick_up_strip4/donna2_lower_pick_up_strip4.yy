{
    "id": "0c74aaba-8010-434a-a0e9-dd88c8349a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26785b7f-fc58-476b-925b-3bdbb7ea6315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c74aaba-8010-434a-a0e9-dd88c8349a41",
            "compositeImage": {
                "id": "3fac8087-6490-4789-a49d-6c224f56bafe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26785b7f-fc58-476b-925b-3bdbb7ea6315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e20b967-749d-4b03-8b26-d6c4d0af476d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26785b7f-fc58-476b-925b-3bdbb7ea6315",
                    "LayerId": "33d2fc11-dc50-4392-af50-015b31e7364c"
                }
            ]
        },
        {
            "id": "a632e33c-faa9-4337-8cf4-d190b8474cf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c74aaba-8010-434a-a0e9-dd88c8349a41",
            "compositeImage": {
                "id": "99a76b3b-bdf1-4b60-aa4a-5e36e7632ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a632e33c-faa9-4337-8cf4-d190b8474cf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24689da5-b527-4634-81df-7cf782379ab4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a632e33c-faa9-4337-8cf4-d190b8474cf3",
                    "LayerId": "33d2fc11-dc50-4392-af50-015b31e7364c"
                }
            ]
        },
        {
            "id": "6234a22e-36a8-4d2c-aea8-5092a95b2922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c74aaba-8010-434a-a0e9-dd88c8349a41",
            "compositeImage": {
                "id": "60867a82-10b5-46bb-b5f8-72b991627e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6234a22e-36a8-4d2c-aea8-5092a95b2922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4421b092-d392-4c15-b9fd-9d119957210d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6234a22e-36a8-4d2c-aea8-5092a95b2922",
                    "LayerId": "33d2fc11-dc50-4392-af50-015b31e7364c"
                }
            ]
        },
        {
            "id": "dbaecb59-2795-4c96-bc3a-0b59aa34f2fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c74aaba-8010-434a-a0e9-dd88c8349a41",
            "compositeImage": {
                "id": "daf327de-616a-4552-a5a8-07d3356d109d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbaecb59-2795-4c96-bc3a-0b59aa34f2fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b34aa767-080f-4b78-b993-b073edcda3df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbaecb59-2795-4c96-bc3a-0b59aa34f2fc",
                    "LayerId": "33d2fc11-dc50-4392-af50-015b31e7364c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33d2fc11-dc50-4392-af50-015b31e7364c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c74aaba-8010-434a-a0e9-dd88c8349a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}