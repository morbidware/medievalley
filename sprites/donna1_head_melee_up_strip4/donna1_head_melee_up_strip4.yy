{
    "id": "f575c849-dffb-4bb6-b2fe-95521efb98f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "199e1e73-ec30-49b0-a75d-7d6860e78dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f575c849-dffb-4bb6-b2fe-95521efb98f8",
            "compositeImage": {
                "id": "377e5743-9b04-43ff-b706-f8b55f4055c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199e1e73-ec30-49b0-a75d-7d6860e78dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee1b0865-de7a-4274-a08f-c70087763657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199e1e73-ec30-49b0-a75d-7d6860e78dfc",
                    "LayerId": "5f295e37-c52d-415c-93f8-f89c5a0e77d5"
                }
            ]
        },
        {
            "id": "af332eb5-22b2-4a16-a3cf-db8d884ae4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f575c849-dffb-4bb6-b2fe-95521efb98f8",
            "compositeImage": {
                "id": "da89b456-3bcc-470a-982f-e486faa1ee69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af332eb5-22b2-4a16-a3cf-db8d884ae4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a99ee55-2a25-4f6e-ba6b-e7da29aa5ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af332eb5-22b2-4a16-a3cf-db8d884ae4c2",
                    "LayerId": "5f295e37-c52d-415c-93f8-f89c5a0e77d5"
                }
            ]
        },
        {
            "id": "4a28cf38-e2ef-4013-8471-9f9e379fcfe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f575c849-dffb-4bb6-b2fe-95521efb98f8",
            "compositeImage": {
                "id": "9328a34a-9f0c-4354-9be6-051fa1eb9b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a28cf38-e2ef-4013-8471-9f9e379fcfe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41f4b217-80c7-4079-8be7-dbe8397b8a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a28cf38-e2ef-4013-8471-9f9e379fcfe0",
                    "LayerId": "5f295e37-c52d-415c-93f8-f89c5a0e77d5"
                }
            ]
        },
        {
            "id": "8dc9448c-d181-4e7a-8904-4f0f183400c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f575c849-dffb-4bb6-b2fe-95521efb98f8",
            "compositeImage": {
                "id": "e45cd776-4b29-411e-ab27-54a9adea7c93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dc9448c-d181-4e7a-8904-4f0f183400c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac4a289-4de5-4c05-b7fa-d20d76419780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dc9448c-d181-4e7a-8904-4f0f183400c0",
                    "LayerId": "5f295e37-c52d-415c-93f8-f89c5a0e77d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5f295e37-c52d-415c-93f8-f89c5a0e77d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f575c849-dffb-4bb6-b2fe-95521efb98f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}