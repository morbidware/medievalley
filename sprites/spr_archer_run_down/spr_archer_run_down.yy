{
    "id": "2bbcbc34-a2bb-43b4-a504-e7a3bf300c3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "332415e1-50b5-4223-83c1-83e744186417",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bbcbc34-a2bb-43b4-a504-e7a3bf300c3c",
            "compositeImage": {
                "id": "76e89400-2a13-496a-bf37-1c1e08a02680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "332415e1-50b5-4223-83c1-83e744186417",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eedaa2d7-e2f3-4ba5-a8c1-aceb31cf6fd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "332415e1-50b5-4223-83c1-83e744186417",
                    "LayerId": "eea0e505-07bf-4c7e-be83-efcba42ef534"
                }
            ]
        },
        {
            "id": "190bfe9f-90e8-465e-adae-8e338ddd2dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bbcbc34-a2bb-43b4-a504-e7a3bf300c3c",
            "compositeImage": {
                "id": "05d86728-0e81-4029-b062-cde488712566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190bfe9f-90e8-465e-adae-8e338ddd2dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65efad2b-0df6-46d8-b05f-6a37bec55c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190bfe9f-90e8-465e-adae-8e338ddd2dc8",
                    "LayerId": "eea0e505-07bf-4c7e-be83-efcba42ef534"
                }
            ]
        },
        {
            "id": "9f09dd02-d37c-4ed7-b694-58f2a3431053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bbcbc34-a2bb-43b4-a504-e7a3bf300c3c",
            "compositeImage": {
                "id": "615aa3d8-e2e3-47e6-b478-b196607df766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f09dd02-d37c-4ed7-b694-58f2a3431053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a86a092-0a17-4bff-86ea-170d9c2195f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f09dd02-d37c-4ed7-b694-58f2a3431053",
                    "LayerId": "eea0e505-07bf-4c7e-be83-efcba42ef534"
                }
            ]
        },
        {
            "id": "9f7e9c13-2f31-4a15-8add-7fdc54613b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bbcbc34-a2bb-43b4-a504-e7a3bf300c3c",
            "compositeImage": {
                "id": "fd22be01-860c-4ab1-b339-2df726a68b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f7e9c13-2f31-4a15-8add-7fdc54613b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c81eafbb-47a8-4847-856b-89903328a26f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f7e9c13-2f31-4a15-8add-7fdc54613b75",
                    "LayerId": "eea0e505-07bf-4c7e-be83-efcba42ef534"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eea0e505-07bf-4c7e-be83-efcba42ef534",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bbcbc34-a2bb-43b4-a504-e7a3bf300c3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}