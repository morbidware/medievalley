{
    "id": "dcbc2285-80ff-4076-aece-707d854754f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8de7847e-70f6-4eed-b47d-fa574b1a513c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcbc2285-80ff-4076-aece-707d854754f8",
            "compositeImage": {
                "id": "220432b1-b0bc-4cb4-8d0c-d904f116092c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de7847e-70f6-4eed-b47d-fa574b1a513c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e969118-4ffb-4224-b237-7a2a0bca720a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de7847e-70f6-4eed-b47d-fa574b1a513c",
                    "LayerId": "de627ef4-d4c3-4e4a-b9b1-ad943cc272e3"
                }
            ]
        },
        {
            "id": "3a942ad8-181e-4237-9cdb-dd46d94ae1d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcbc2285-80ff-4076-aece-707d854754f8",
            "compositeImage": {
                "id": "7a07a922-8fee-49bf-bb02-3533e6f2c291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a942ad8-181e-4237-9cdb-dd46d94ae1d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d146a7ad-838b-4116-b502-309a4aca6ef6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a942ad8-181e-4237-9cdb-dd46d94ae1d8",
                    "LayerId": "de627ef4-d4c3-4e4a-b9b1-ad943cc272e3"
                }
            ]
        },
        {
            "id": "b4d85a79-3464-41e9-a325-4a7547f6b8be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcbc2285-80ff-4076-aece-707d854754f8",
            "compositeImage": {
                "id": "46fc75f2-b13a-4852-bf54-a39550f789e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4d85a79-3464-41e9-a325-4a7547f6b8be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb44286d-9bb5-4189-acc4-748062c15e8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4d85a79-3464-41e9-a325-4a7547f6b8be",
                    "LayerId": "de627ef4-d4c3-4e4a-b9b1-ad943cc272e3"
                }
            ]
        },
        {
            "id": "4595ba49-1ee5-494d-951f-447adafeb928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcbc2285-80ff-4076-aece-707d854754f8",
            "compositeImage": {
                "id": "7c8c53ef-d09a-4ddf-9954-e6b93cb0eff7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4595ba49-1ee5-494d-951f-447adafeb928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b74dfe-20e7-4ddc-baa3-bbdcb84c0cbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4595ba49-1ee5-494d-951f-447adafeb928",
                    "LayerId": "de627ef4-d4c3-4e4a-b9b1-ad943cc272e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "de627ef4-d4c3-4e4a-b9b1-ad943cc272e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcbc2285-80ff-4076-aece-707d854754f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}