{
    "id": "b7871f4f-bfee-489d-b39e-71a255c01995",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_galacredit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8b57de7-a7b8-40a9-a5b5-2ecac45cd236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7871f4f-bfee-489d-b39e-71a255c01995",
            "compositeImage": {
                "id": "8266e646-5fa3-412d-bf81-b586a2f9aad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b57de7-a7b8-40a9-a5b5-2ecac45cd236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f79348e-fc00-4cba-a05a-ef6f8a8e9964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b57de7-a7b8-40a9-a5b5-2ecac45cd236",
                    "LayerId": "5181017e-9dd7-4c38-a1c2-764a796e389f"
                },
                {
                    "id": "2a2ecb9c-8133-4ab0-b3a2-483b10e0e993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b57de7-a7b8-40a9-a5b5-2ecac45cd236",
                    "LayerId": "2b87742b-5b8c-4424-858c-ba1d32ff4cbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2b87742b-5b8c-4424-858c-ba1d32ff4cbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7871f4f-bfee-489d-b39e-71a255c01995",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 5",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5181017e-9dd7-4c38-a1c2-764a796e389f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7871f4f-bfee-489d-b39e-71a255c01995",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 4",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}