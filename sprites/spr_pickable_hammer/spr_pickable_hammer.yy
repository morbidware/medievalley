{
    "id": "dca007eb-2c98-426e-876f-02bda3990d10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_hammer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1deac076-3b32-493e-b5c5-c19b8fa65663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dca007eb-2c98-426e-876f-02bda3990d10",
            "compositeImage": {
                "id": "67256899-80c9-4d1b-8c4b-55187e0b337e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1deac076-3b32-493e-b5c5-c19b8fa65663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30964479-5525-4afc-8b30-d9aec9e90668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1deac076-3b32-493e-b5c5-c19b8fa65663",
                    "LayerId": "e74bcd42-c50d-4bf8-a089-c1855e6926b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e74bcd42-c50d-4bf8-a089-c1855e6926b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dca007eb-2c98-426e-876f-02bda3990d10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}