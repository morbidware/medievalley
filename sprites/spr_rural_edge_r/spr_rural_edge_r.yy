{
    "id": "e946ea3f-d151-4525-aca8-c872a4c734c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_edge_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c180faa1-1a33-4510-9112-25f25d5a87ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e946ea3f-d151-4525-aca8-c872a4c734c7",
            "compositeImage": {
                "id": "e2013573-d1fb-45fe-a129-b3a8254c0d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c180faa1-1a33-4510-9112-25f25d5a87ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79648ae-70fe-471b-be88-b66786e0bd4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c180faa1-1a33-4510-9112-25f25d5a87ba",
                    "LayerId": "ce278847-31f8-441d-93c2-2b16910e97e5"
                }
            ]
        },
        {
            "id": "6e047ea0-f327-456e-a5c1-eddd57fcfed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e946ea3f-d151-4525-aca8-c872a4c734c7",
            "compositeImage": {
                "id": "29c33dde-9c6a-4c38-acbc-7a43b614b096",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e047ea0-f327-456e-a5c1-eddd57fcfed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "838572c1-fc3d-4d05-aeb6-1ce6ec246f3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e047ea0-f327-456e-a5c1-eddd57fcfed5",
                    "LayerId": "ce278847-31f8-441d-93c2-2b16910e97e5"
                }
            ]
        },
        {
            "id": "a70db07f-5fdc-42df-93be-6d2dc76bb883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e946ea3f-d151-4525-aca8-c872a4c734c7",
            "compositeImage": {
                "id": "f202d802-087b-4340-ac4b-c86d397c0d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a70db07f-5fdc-42df-93be-6d2dc76bb883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68ec7ec8-e9af-4515-946c-44d7b008762b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a70db07f-5fdc-42df-93be-6d2dc76bb883",
                    "LayerId": "ce278847-31f8-441d-93c2-2b16910e97e5"
                }
            ]
        },
        {
            "id": "0e8de27b-76e9-4d6e-a250-ba1b2fef4c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e946ea3f-d151-4525-aca8-c872a4c734c7",
            "compositeImage": {
                "id": "722debdb-e2c7-4a01-a3f4-29d9f6e659dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e8de27b-76e9-4d6e-a250-ba1b2fef4c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd123791-a7a2-4271-95e4-c876596d9d39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e8de27b-76e9-4d6e-a250-ba1b2fef4c1c",
                    "LayerId": "ce278847-31f8-441d-93c2-2b16910e97e5"
                }
            ]
        },
        {
            "id": "24f7aa76-fe5f-49d1-a2f0-9e395d27324c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e946ea3f-d151-4525-aca8-c872a4c734c7",
            "compositeImage": {
                "id": "90ab750b-9eec-47ca-b749-067e14a48a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24f7aa76-fe5f-49d1-a2f0-9e395d27324c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "803d12fc-62bf-467e-990a-271d2577b5e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24f7aa76-fe5f-49d1-a2f0-9e395d27324c",
                    "LayerId": "ce278847-31f8-441d-93c2-2b16910e97e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce278847-31f8-441d-93c2-2b16910e97e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e946ea3f-d151-4525-aca8-c872a4c734c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}