{
    "id": "5be7eaea-1d19-4d1d-8ce8-63c66744e1c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_scrollblueprinthouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e68cab0a-2d72-4805-a61d-ebff1972b684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5be7eaea-1d19-4d1d-8ce8-63c66744e1c4",
            "compositeImage": {
                "id": "8d6e65af-d0f7-4f87-921e-d72bfe150ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e68cab0a-2d72-4805-a61d-ebff1972b684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5f9129-9b59-49bb-a548-1f18a75a4295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e68cab0a-2d72-4805-a61d-ebff1972b684",
                    "LayerId": "5376083b-9ce6-4ca9-9de4-16994f4ba2bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5376083b-9ce6-4ca9-9de4-16994f4ba2bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5be7eaea-1d19-4d1d-8ce8-63c66744e1c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}