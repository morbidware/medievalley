{
    "id": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_pick_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36fb2796-3d76-4ef7-aa6e-ed7a1f9cdaec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "compositeImage": {
                "id": "d5ab8585-d9ef-4f81-a7a8-1965d78cc278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36fb2796-3d76-4ef7-aa6e-ed7a1f9cdaec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "265af651-4fc1-4fa1-a7b2-ec66677e72ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36fb2796-3d76-4ef7-aa6e-ed7a1f9cdaec",
                    "LayerId": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd"
                }
            ]
        },
        {
            "id": "b3d2cc2a-57fe-4134-891f-3eadb60d17bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "compositeImage": {
                "id": "c94ce522-fbec-4ea3-b37e-79afb297cb49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d2cc2a-57fe-4134-891f-3eadb60d17bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a950f7-d8ab-447a-a0cf-0cd53e3d22c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d2cc2a-57fe-4134-891f-3eadb60d17bb",
                    "LayerId": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd"
                }
            ]
        },
        {
            "id": "6abec375-4095-4cba-889d-601a3e55fe30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "compositeImage": {
                "id": "8c450f6b-c59b-40c1-83bf-9a45a3ee53e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abec375-4095-4cba-889d-601a3e55fe30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe77cf77-3328-4b95-bd3e-728202f8a96a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abec375-4095-4cba-889d-601a3e55fe30",
                    "LayerId": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd"
                }
            ]
        },
        {
            "id": "1d303c38-4454-4cb5-90c1-3c28d03e5211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "compositeImage": {
                "id": "50f6d6f0-ecdb-4772-b14e-33bf53e60794",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d303c38-4454-4cb5-90c1-3c28d03e5211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12e1454-0015-4ec0-8bee-8254dfbc4ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d303c38-4454-4cb5-90c1-3c28d03e5211",
                    "LayerId": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd"
                }
            ]
        },
        {
            "id": "313c0c44-0ba7-4b50-affb-0fcb1a00f0d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "compositeImage": {
                "id": "4b32fd8e-9b9d-4890-abb3-4f0e16d231f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313c0c44-0ba7-4b50-affb-0fcb1a00f0d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4516bfc-2a79-4524-9a81-9f510f5268c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313c0c44-0ba7-4b50-affb-0fcb1a00f0d3",
                    "LayerId": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd"
                }
            ]
        },
        {
            "id": "d3d737f5-a5d1-4c12-a421-1320c67cd776",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "compositeImage": {
                "id": "8ba480c6-9d51-42ba-8df5-2a280f6cef25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3d737f5-a5d1-4c12-a421-1320c67cd776",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0e1f106-f668-4e37-85e7-95aec4c93065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3d737f5-a5d1-4c12-a421-1320c67cd776",
                    "LayerId": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5b45d2b0-d096-40fe-bf3a-460596cb2cfd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ef2e347-9217-4f5b-ac75-91f173c859aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}