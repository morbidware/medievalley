{
    "id": "c7ab80ce-a6c1-4d20-bbae-be5d8a95634c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "061d5c32-0234-4cf1-8dd4-a7a0dc57500b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7ab80ce-a6c1-4d20-bbae-be5d8a95634c",
            "compositeImage": {
                "id": "8db465c0-0550-4ce4-bb83-d711ab78a267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061d5c32-0234-4cf1-8dd4-a7a0dc57500b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332e2a74-1abd-4005-8d29-90cfc03ee7bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061d5c32-0234-4cf1-8dd4-a7a0dc57500b",
                    "LayerId": "04978032-3ee9-4a20-a539-1509ba3bdd13"
                }
            ]
        },
        {
            "id": "556e3a85-5f44-4f04-b376-e01172a488d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7ab80ce-a6c1-4d20-bbae-be5d8a95634c",
            "compositeImage": {
                "id": "398eb3c3-3d86-4678-af18-fc136f70114e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "556e3a85-5f44-4f04-b376-e01172a488d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dea49eab-41d1-4656-8422-b97daaf0d872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "556e3a85-5f44-4f04-b376-e01172a488d6",
                    "LayerId": "04978032-3ee9-4a20-a539-1509ba3bdd13"
                }
            ]
        },
        {
            "id": "5220cf68-7a98-4bb8-9d0f-2a0d955dcd6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7ab80ce-a6c1-4d20-bbae-be5d8a95634c",
            "compositeImage": {
                "id": "1ff66f9b-506e-4275-b287-c3d7d0b452ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5220cf68-7a98-4bb8-9d0f-2a0d955dcd6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff9719aa-6d51-4101-9200-5e219c46e8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5220cf68-7a98-4bb8-9d0f-2a0d955dcd6d",
                    "LayerId": "04978032-3ee9-4a20-a539-1509ba3bdd13"
                }
            ]
        },
        {
            "id": "52a8670b-2945-43c7-b81f-a3c52117f721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7ab80ce-a6c1-4d20-bbae-be5d8a95634c",
            "compositeImage": {
                "id": "532a3b47-c41f-480f-9560-1e46e1b14aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52a8670b-2945-43c7-b81f-a3c52117f721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feaa3ef3-9735-4e63-9831-e4fe2b2d4fd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52a8670b-2945-43c7-b81f-a3c52117f721",
                    "LayerId": "04978032-3ee9-4a20-a539-1509ba3bdd13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04978032-3ee9-4a20-a539-1509ba3bdd13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7ab80ce-a6c1-4d20-bbae-be5d8a95634c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}