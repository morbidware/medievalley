{
    "id": "57577609-af96-4bf5-85aa-0a3a2af0c2c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_die_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 14,
    "bbox_right": 29,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dab81b1-dd81-46c8-81bc-e7a52bf74da7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57577609-af96-4bf5-85aa-0a3a2af0c2c0",
            "compositeImage": {
                "id": "e0928dc4-89b9-47b9-bc27-78143469fb28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dab81b1-dd81-46c8-81bc-e7a52bf74da7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf230ea-faf3-4157-a6b7-9ebb849d349e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dab81b1-dd81-46c8-81bc-e7a52bf74da7",
                    "LayerId": "eaae96be-7820-44df-8d8c-ee320500200d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "eaae96be-7820-44df-8d8c-ee320500200d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57577609-af96-4bf5-85aa-0a3a2af0c2c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}