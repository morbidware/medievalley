{
    "id": "56270561-d619-4f36-9b22-d94aaa05a1b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81ec26fd-ba07-4c95-ad25-d96e924f2bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56270561-d619-4f36-9b22-d94aaa05a1b8",
            "compositeImage": {
                "id": "c5932708-5e98-4b57-a16b-bc8da32db27d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81ec26fd-ba07-4c95-ad25-d96e924f2bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b85b72-05f9-4635-a985-8fc39c3af022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81ec26fd-ba07-4c95-ad25-d96e924f2bd6",
                    "LayerId": "060fc55a-b453-4aaf-a01e-4b9d72a1da2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "060fc55a-b453-4aaf-a01e-4b9d72a1da2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56270561-d619-4f36-9b22-d94aaa05a1b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}