{
    "id": "6fd374e1-8e12-4a50-9bee-1e41cb4e6b40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c79b9c45-a4c4-4d7e-abb4-c96fe52d883e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fd374e1-8e12-4a50-9bee-1e41cb4e6b40",
            "compositeImage": {
                "id": "17ffe8fd-2c8b-45e5-91b5-c4c0ddaec20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c79b9c45-a4c4-4d7e-abb4-c96fe52d883e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688dff73-055a-4d86-affc-585deb636839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c79b9c45-a4c4-4d7e-abb4-c96fe52d883e",
                    "LayerId": "0802a8bd-8671-49b1-bc68-cf6db704fc23"
                }
            ]
        },
        {
            "id": "32c0f1b9-bd25-409b-98aa-ef303f768344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fd374e1-8e12-4a50-9bee-1e41cb4e6b40",
            "compositeImage": {
                "id": "311a322b-4e72-47a4-94ef-79c4a0b66efa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c0f1b9-bd25-409b-98aa-ef303f768344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d0f493-cd60-4824-b1d6-cb6eb5dc006d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c0f1b9-bd25-409b-98aa-ef303f768344",
                    "LayerId": "0802a8bd-8671-49b1-bc68-cf6db704fc23"
                }
            ]
        },
        {
            "id": "68b21bc4-18b9-4f1e-8011-db33ede44fa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fd374e1-8e12-4a50-9bee-1e41cb4e6b40",
            "compositeImage": {
                "id": "369168d3-15c5-45c5-9e06-0cf286ed3386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68b21bc4-18b9-4f1e-8011-db33ede44fa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b06829f-bc76-4bac-abdd-eddeea35fcbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68b21bc4-18b9-4f1e-8011-db33ede44fa8",
                    "LayerId": "0802a8bd-8671-49b1-bc68-cf6db704fc23"
                }
            ]
        },
        {
            "id": "39fa1a29-e94d-4dfa-bb44-696960c30dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fd374e1-8e12-4a50-9bee-1e41cb4e6b40",
            "compositeImage": {
                "id": "1cc035a0-20e4-4e84-861a-33510f5dec75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39fa1a29-e94d-4dfa-bb44-696960c30dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b5a842a-8b40-4ca4-b9ef-4bb6aa34ac9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39fa1a29-e94d-4dfa-bb44-696960c30dac",
                    "LayerId": "0802a8bd-8671-49b1-bc68-cf6db704fc23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0802a8bd-8671-49b1-bc68-cf6db704fc23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fd374e1-8e12-4a50-9bee-1e41cb4e6b40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}