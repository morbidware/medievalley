{
    "id": "c1b3ed3e-02d8-4332-bd97-8cca189034fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_suffer_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 15,
    "bbox_right": 30,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7109f0f5-240d-47d3-a248-fba2c598995d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1b3ed3e-02d8-4332-bd97-8cca189034fc",
            "compositeImage": {
                "id": "239c8638-799a-44d9-8e8b-952d7cdd11f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7109f0f5-240d-47d3-a248-fba2c598995d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f592585-8a46-435c-a53b-2869c4d4121b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7109f0f5-240d-47d3-a248-fba2c598995d",
                    "LayerId": "3bf3c23c-fd0e-4174-9ca5-cff9a7abfbe1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "3bf3c23c-fd0e-4174-9ca5-cff9a7abfbe1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1b3ed3e-02d8-4332-bd97-8cca189034fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}