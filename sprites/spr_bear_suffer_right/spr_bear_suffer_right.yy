{
    "id": "37026a6d-bae8-4d51-a680-a1c047105008",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_suffer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 1,
    "bbox_right": 46,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6f94080-c279-4574-8bc5-789c6ad354e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37026a6d-bae8-4d51-a680-a1c047105008",
            "compositeImage": {
                "id": "fc0df7c5-18e9-4c47-b2df-6332d8d0831f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f94080-c279-4574-8bc5-789c6ad354e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f78dd7c-93a7-47c4-955a-51eb446fea03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f94080-c279-4574-8bc5-789c6ad354e2",
                    "LayerId": "88e75109-c5a8-4469-b8c6-085cf92badc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "88e75109-c5a8-4469-b8c6-085cf92badc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37026a6d-bae8-4d51-a680-a1c047105008",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 22,
    "yorig": 45
}