{
    "id": "09c92ab8-14f1-4a5f-be9a-4432634a7a5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_life_stamina_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c09c93e-3e4e-4e45-8037-0c6ae3eaa720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09c92ab8-14f1-4a5f-be9a-4432634a7a5d",
            "compositeImage": {
                "id": "a75b6144-0e36-4d3b-9ef0-7d8a469aebdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c09c93e-3e4e-4e45-8037-0c6ae3eaa720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93c13b93-d8d6-4b9c-b190-1e53dea558dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c09c93e-3e4e-4e45-8037-0c6ae3eaa720",
                    "LayerId": "2f2a6bcd-bb4b-4c8d-8018-426c1cffce33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "2f2a6bcd-bb4b-4c8d-8018-426c1cffce33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09c92ab8-14f1-4a5f-be9a-4432634a7a5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}