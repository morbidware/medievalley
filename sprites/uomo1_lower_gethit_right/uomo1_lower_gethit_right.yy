{
    "id": "87dce869-543e-449d-90d9-99a7a651ebee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a242ea32-d5ef-457f-947c-eed6db8f4e57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87dce869-543e-449d-90d9-99a7a651ebee",
            "compositeImage": {
                "id": "8fc83d7c-83c9-4c73-a54b-9e7a9121cccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a242ea32-d5ef-457f-947c-eed6db8f4e57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72ef3339-1170-4f99-b730-c513a8e27c79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a242ea32-d5ef-457f-947c-eed6db8f4e57",
                    "LayerId": "0074b57e-24f1-4590-b3fe-d1ad0eada026"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0074b57e-24f1-4590-b3fe-d1ad0eada026",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87dce869-543e-449d-90d9-99a7a651ebee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}