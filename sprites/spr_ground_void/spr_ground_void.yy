{
    "id": "e7ee4156-4d25-4ec6-9f6c-6ece3c17085b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_void",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9905bfe5-d732-43dd-b418-34ee1c40b28c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7ee4156-4d25-4ec6-9f6c-6ece3c17085b",
            "compositeImage": {
                "id": "f8ff2e9e-d1e7-4218-a36d-6b504e677f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9905bfe5-d732-43dd-b418-34ee1c40b28c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dd0f7c-60db-4694-b054-fd9e0e2f1c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9905bfe5-d732-43dd-b418-34ee1c40b28c",
                    "LayerId": "889bcdc4-e06e-4df9-a77f-c38865bfc7c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "889bcdc4-e06e-4df9-a77f-c38865bfc7c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7ee4156-4d25-4ec6-9f6c-6ece3c17085b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}