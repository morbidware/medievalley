{
    "id": "20602bce-31a5-4a3d-a5cb-7d4cfdf40cd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6a4ffe7-aa98-49ac-9c22-c55c7b13ffb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20602bce-31a5-4a3d-a5cb-7d4cfdf40cd5",
            "compositeImage": {
                "id": "d501ac7a-5860-44d7-933f-3caeccc7ebe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a4ffe7-aa98-49ac-9c22-c55c7b13ffb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6eaa8a6-23ae-4708-8332-097ea8e849d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a4ffe7-aa98-49ac-9c22-c55c7b13ffb4",
                    "LayerId": "fb383ca4-6144-4044-9fb9-2b66b2a2c4a5"
                }
            ]
        },
        {
            "id": "5bdbf2fd-3c6f-4b4f-994e-250af9ac3bcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20602bce-31a5-4a3d-a5cb-7d4cfdf40cd5",
            "compositeImage": {
                "id": "59c2820f-ca75-40de-b753-395628a34dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bdbf2fd-3c6f-4b4f-994e-250af9ac3bcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b622653-c075-4a44-aed8-9071d2d387d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bdbf2fd-3c6f-4b4f-994e-250af9ac3bcf",
                    "LayerId": "fb383ca4-6144-4044-9fb9-2b66b2a2c4a5"
                }
            ]
        },
        {
            "id": "93d2bf71-84e5-4130-916a-db3f42e746fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20602bce-31a5-4a3d-a5cb-7d4cfdf40cd5",
            "compositeImage": {
                "id": "d2de60e2-6bfb-4a89-99aa-fccbf29fbee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93d2bf71-84e5-4130-916a-db3f42e746fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae68d667-14e9-4e1f-bb95-fbac2b2abfcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93d2bf71-84e5-4130-916a-db3f42e746fc",
                    "LayerId": "fb383ca4-6144-4044-9fb9-2b66b2a2c4a5"
                }
            ]
        },
        {
            "id": "dca76554-678e-49e8-afd0-60207be79678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20602bce-31a5-4a3d-a5cb-7d4cfdf40cd5",
            "compositeImage": {
                "id": "fcaefdcc-ec72-4200-a411-75ebe5584d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca76554-678e-49e8-afd0-60207be79678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d68441b-d2c0-4a8b-840b-aab8d974692f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca76554-678e-49e8-afd0-60207be79678",
                    "LayerId": "fb383ca4-6144-4044-9fb9-2b66b2a2c4a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb383ca4-6144-4044-9fb9-2b66b2a2c4a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20602bce-31a5-4a3d-a5cb-7d4cfdf40cd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}