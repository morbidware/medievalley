{
    "id": "af100c2a-125c-4606-88c4-876f1df2c6ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 5,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8be4ef43-ced6-4194-bf88-ac86ada93bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af100c2a-125c-4606-88c4-876f1df2c6ae",
            "compositeImage": {
                "id": "b6f01cb5-ba61-4d31-a829-f0a53d6bf864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be4ef43-ced6-4194-bf88-ac86ada93bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eaea84c-d2b0-4e82-98ec-0bea231a1bba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be4ef43-ced6-4194-bf88-ac86ada93bb2",
                    "LayerId": "ff0dc1f3-4e46-4244-8d49-4dddce6cda7e"
                }
            ]
        },
        {
            "id": "7b82c2bb-dd14-4499-86cb-99f0755231ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af100c2a-125c-4606-88c4-876f1df2c6ae",
            "compositeImage": {
                "id": "78ac673a-f465-45ab-9f01-c34b317e1069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b82c2bb-dd14-4499-86cb-99f0755231ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb2f82b-5aac-45e9-8b8b-26a67d0c28c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b82c2bb-dd14-4499-86cb-99f0755231ee",
                    "LayerId": "ff0dc1f3-4e46-4244-8d49-4dddce6cda7e"
                }
            ]
        },
        {
            "id": "ca22c0fb-31a7-426e-b080-5bfd6cbe3f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af100c2a-125c-4606-88c4-876f1df2c6ae",
            "compositeImage": {
                "id": "b3a31a4f-c3ad-4fc0-ac51-b894f486cabd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca22c0fb-31a7-426e-b080-5bfd6cbe3f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b42eda9-f381-4e78-81bd-ed2b8687371b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca22c0fb-31a7-426e-b080-5bfd6cbe3f92",
                    "LayerId": "ff0dc1f3-4e46-4244-8d49-4dddce6cda7e"
                }
            ]
        },
        {
            "id": "5ab9044e-3a05-4df3-a0a2-75be6ad8193e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af100c2a-125c-4606-88c4-876f1df2c6ae",
            "compositeImage": {
                "id": "acad6f53-f822-440e-b069-8f6b340ab2f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab9044e-3a05-4df3-a0a2-75be6ad8193e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce31031-4e81-4cbe-aa52-133f2ea30323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab9044e-3a05-4df3-a0a2-75be6ad8193e",
                    "LayerId": "ff0dc1f3-4e46-4244-8d49-4dddce6cda7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ff0dc1f3-4e46-4244-8d49-4dddce6cda7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af100c2a-125c-4606-88c4-876f1df2c6ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}