{
    "id": "9ffdc4c4-0473-447b-aa5c-72e79d6f114f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 11,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b3c2d87-eed3-424b-8c47-481a71942a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ffdc4c4-0473-447b-aa5c-72e79d6f114f",
            "compositeImage": {
                "id": "5057b5c4-c263-418b-a745-aa445cf3ed05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b3c2d87-eed3-424b-8c47-481a71942a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e02bb201-88a7-4f7d-81b4-92cb2e287731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b3c2d87-eed3-424b-8c47-481a71942a2e",
                    "LayerId": "f2b72665-7a96-4d61-abe9-9d128acdbe30"
                }
            ]
        },
        {
            "id": "d56a19c1-958a-43f1-9667-9e371e5b0839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ffdc4c4-0473-447b-aa5c-72e79d6f114f",
            "compositeImage": {
                "id": "c1f222e7-fae1-498b-8862-32f5e3772d67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56a19c1-958a-43f1-9667-9e371e5b0839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "799fa8db-d04e-4769-a942-e8f547fed89f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56a19c1-958a-43f1-9667-9e371e5b0839",
                    "LayerId": "f2b72665-7a96-4d61-abe9-9d128acdbe30"
                }
            ]
        },
        {
            "id": "7c9f4c6a-c702-4693-affb-b3f1f715091e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ffdc4c4-0473-447b-aa5c-72e79d6f114f",
            "compositeImage": {
                "id": "078459e4-9874-4d11-a910-ab7f6e388592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c9f4c6a-c702-4693-affb-b3f1f715091e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a19ca7cf-3038-417e-bb5c-4ddf0af7b56d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c9f4c6a-c702-4693-affb-b3f1f715091e",
                    "LayerId": "f2b72665-7a96-4d61-abe9-9d128acdbe30"
                }
            ]
        },
        {
            "id": "c675f032-a3d4-4fdc-b709-c5788a299910",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ffdc4c4-0473-447b-aa5c-72e79d6f114f",
            "compositeImage": {
                "id": "5241f83c-4fdc-452b-ae00-6f1a6e917f85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c675f032-a3d4-4fdc-b709-c5788a299910",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecd726ee-e149-43c4-8d02-dbd230dbd501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c675f032-a3d4-4fdc-b709-c5788a299910",
                    "LayerId": "f2b72665-7a96-4d61-abe9-9d128acdbe30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f2b72665-7a96-4d61-abe9-9d128acdbe30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ffdc4c4-0473-447b-aa5c-72e79d6f114f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}