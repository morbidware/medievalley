{
    "id": "7ee093ae-f968-4e78-bf2b-53ad36dfc7d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee42c5eb-ffb3-4a88-836c-30737b33470b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee093ae-f968-4e78-bf2b-53ad36dfc7d5",
            "compositeImage": {
                "id": "cefc0701-a951-4e45-9e83-592b483b1426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee42c5eb-ffb3-4a88-836c-30737b33470b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9cee9e0-149b-4a03-991e-89a43c1c8375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee42c5eb-ffb3-4a88-836c-30737b33470b",
                    "LayerId": "f468c774-a15d-4e95-823e-e1901f67c6b6"
                }
            ]
        },
        {
            "id": "fc42e944-114a-4d7c-b3e5-a4d93ef576e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee093ae-f968-4e78-bf2b-53ad36dfc7d5",
            "compositeImage": {
                "id": "b8f6cd7d-cceb-44c1-9ed1-bb39204c43fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc42e944-114a-4d7c-b3e5-a4d93ef576e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "326cc9fd-7fbd-4ba5-b3e6-52ade2ed186d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc42e944-114a-4d7c-b3e5-a4d93ef576e6",
                    "LayerId": "f468c774-a15d-4e95-823e-e1901f67c6b6"
                }
            ]
        },
        {
            "id": "83dbcd58-ab1e-4c87-b570-c0af62d90e0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee093ae-f968-4e78-bf2b-53ad36dfc7d5",
            "compositeImage": {
                "id": "05e28ebd-e62c-475f-a18c-234758d4bad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83dbcd58-ab1e-4c87-b570-c0af62d90e0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26d8bc25-15a9-4210-a0c5-d32d1dfe719a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83dbcd58-ab1e-4c87-b570-c0af62d90e0b",
                    "LayerId": "f468c774-a15d-4e95-823e-e1901f67c6b6"
                }
            ]
        },
        {
            "id": "5728361e-df0d-4f15-888c-545ac46eabae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee093ae-f968-4e78-bf2b-53ad36dfc7d5",
            "compositeImage": {
                "id": "1193379b-192a-4186-a063-6f9540e4bf42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5728361e-df0d-4f15-888c-545ac46eabae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "629027cd-ff4b-49a8-af35-1791d4a3aa7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5728361e-df0d-4f15-888c-545ac46eabae",
                    "LayerId": "f468c774-a15d-4e95-823e-e1901f67c6b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f468c774-a15d-4e95-823e-e1901f67c6b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ee093ae-f968-4e78-bf2b-53ad36dfc7d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}