{
    "id": "eac9c750-e3ea-440e-8dcb-aa3a5c61ed7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_idle_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 22,
    "bbox_right": 27,
    "bbox_top": 26,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70b006a8-c48c-48d2-a374-5146b65856ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eac9c750-e3ea-440e-8dcb-aa3a5c61ed7b",
            "compositeImage": {
                "id": "0b236adc-a66d-4fb6-a9db-e14e79d3f59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70b006a8-c48c-48d2-a374-5146b65856ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a0e87d-0d3a-421d-a7d3-0e567b412191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70b006a8-c48c-48d2-a374-5146b65856ce",
                    "LayerId": "13c62252-ac75-4354-acb1-2abdc69ba8d9"
                }
            ]
        },
        {
            "id": "a30ddec5-7aeb-4538-aa8e-c8beb8211c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eac9c750-e3ea-440e-8dcb-aa3a5c61ed7b",
            "compositeImage": {
                "id": "2964e392-5ddb-4590-9255-a6cabed79e21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a30ddec5-7aeb-4538-aa8e-c8beb8211c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3c74da-b6a6-4046-a26a-adc9b02f64cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a30ddec5-7aeb-4538-aa8e-c8beb8211c22",
                    "LayerId": "13c62252-ac75-4354-acb1-2abdc69ba8d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "13c62252-ac75-4354-acb1-2abdc69ba8d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eac9c750-e3ea-440e-8dcb-aa3a5c61ed7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}