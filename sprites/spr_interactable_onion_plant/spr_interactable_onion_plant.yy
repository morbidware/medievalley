{
    "id": "da540995-a3c9-45c5-9c96-533243d684f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_onion_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "629b161c-0461-408e-9504-a36ad4f3e1e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "compositeImage": {
                "id": "a9bab7a0-d0b9-414a-b5c5-0c2fbe719a55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "629b161c-0461-408e-9504-a36ad4f3e1e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5170054-0fee-4c6b-8a92-a4442ab4289f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "629b161c-0461-408e-9504-a36ad4f3e1e8",
                    "LayerId": "07257814-0380-4fa7-a3da-1dc8c92b562a"
                }
            ]
        },
        {
            "id": "b0662cfb-8163-4171-aed8-7bbcd07c8c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "compositeImage": {
                "id": "b5de1ecd-6236-4d21-938a-3b2ab1069b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0662cfb-8163-4171-aed8-7bbcd07c8c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "305119f0-ab6c-44d4-b76e-2acd20a256e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0662cfb-8163-4171-aed8-7bbcd07c8c72",
                    "LayerId": "07257814-0380-4fa7-a3da-1dc8c92b562a"
                }
            ]
        },
        {
            "id": "460a76e0-b003-421b-a04b-6723d9e63675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "compositeImage": {
                "id": "3f612845-3e4a-4b3c-afdc-107b79d9ddf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "460a76e0-b003-421b-a04b-6723d9e63675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f9e101f-eefc-4f72-99c5-09641c744185",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "460a76e0-b003-421b-a04b-6723d9e63675",
                    "LayerId": "07257814-0380-4fa7-a3da-1dc8c92b562a"
                }
            ]
        },
        {
            "id": "5b225e1c-fec2-410d-a4c9-975abd4f46cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "compositeImage": {
                "id": "e0b8745c-dcf3-4368-9937-a26955bece86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b225e1c-fec2-410d-a4c9-975abd4f46cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3e18504-bfc4-47fe-a71a-27a600fa7f03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b225e1c-fec2-410d-a4c9-975abd4f46cb",
                    "LayerId": "07257814-0380-4fa7-a3da-1dc8c92b562a"
                }
            ]
        },
        {
            "id": "0a157770-849c-4211-a0db-2adb91b25f80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "compositeImage": {
                "id": "1e0f9470-6f68-415d-a0cd-776f73d62a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a157770-849c-4211-a0db-2adb91b25f80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15d56efb-dabb-4ce1-8914-292cef25ab6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a157770-849c-4211-a0db-2adb91b25f80",
                    "LayerId": "07257814-0380-4fa7-a3da-1dc8c92b562a"
                }
            ]
        },
        {
            "id": "1ed2864e-26c4-4141-8b3c-407640a7984e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "compositeImage": {
                "id": "8e9b34ac-80a8-401c-9a45-fc4f1d7cfe31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed2864e-26c4-4141-8b3c-407640a7984e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117baf9e-3acf-4d0f-8f1c-e87d81bc2c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed2864e-26c4-4141-8b3c-407640a7984e",
                    "LayerId": "07257814-0380-4fa7-a3da-1dc8c92b562a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "07257814-0380-4fa7-a3da-1dc8c92b562a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da540995-a3c9-45c5-9c96-533243d684f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 27
}