{
    "id": "8b3572f0-4035-4844-9ca3-8082a9e3d45b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16cf21ac-670b-4480-b0c2-f9abaa939925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b3572f0-4035-4844-9ca3-8082a9e3d45b",
            "compositeImage": {
                "id": "cec596ed-9247-47f2-a1e7-3f147fc8b611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16cf21ac-670b-4480-b0c2-f9abaa939925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99f4350-5aa2-42d0-9286-37cec2893d49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16cf21ac-670b-4480-b0c2-f9abaa939925",
                    "LayerId": "95395853-4b4a-4bc5-b543-b3cf9b15f678"
                }
            ]
        },
        {
            "id": "bdc12012-6efc-4e66-85dd-245ed3e6fd44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b3572f0-4035-4844-9ca3-8082a9e3d45b",
            "compositeImage": {
                "id": "46581aa6-cf61-4c59-bd50-3b6101bd3223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdc12012-6efc-4e66-85dd-245ed3e6fd44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9eac983-a38c-49a5-a77a-8cc3f079004b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdc12012-6efc-4e66-85dd-245ed3e6fd44",
                    "LayerId": "95395853-4b4a-4bc5-b543-b3cf9b15f678"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "95395853-4b4a-4bc5-b543-b3cf9b15f678",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b3572f0-4035-4844-9ca3-8082a9e3d45b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}