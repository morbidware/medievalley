{
    "id": "cfebf1ee-41e3-45aa-b30d-3a66a95f365e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0eeb0a97-b2a1-4bb1-a335-118d54ffc882",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfebf1ee-41e3-45aa-b30d-3a66a95f365e",
            "compositeImage": {
                "id": "5a180c57-839e-484f-ad7a-b67401d9b5a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eeb0a97-b2a1-4bb1-a335-118d54ffc882",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ad6fefb-8913-478e-8a45-e687ca6024dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eeb0a97-b2a1-4bb1-a335-118d54ffc882",
                    "LayerId": "5c97ccae-0170-4c40-a607-0593aa6d5e72"
                }
            ]
        },
        {
            "id": "7020b6b4-abec-4159-aa74-e24f50257c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfebf1ee-41e3-45aa-b30d-3a66a95f365e",
            "compositeImage": {
                "id": "d32e9b2f-7ed7-4151-a660-bf10e5505a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7020b6b4-abec-4159-aa74-e24f50257c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb80ce3f-5574-40cb-940a-104c547056cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7020b6b4-abec-4159-aa74-e24f50257c7a",
                    "LayerId": "5c97ccae-0170-4c40-a607-0593aa6d5e72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "5c97ccae-0170-4c40-a607-0593aa6d5e72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfebf1ee-41e3-45aa-b30d-3a66a95f365e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}