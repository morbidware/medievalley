{
    "id": "39392d95-480d-4b27-ab12-93eba590c976",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_bow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 6,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41787ed3-dbd6-402a-a264-082561a218fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39392d95-480d-4b27-ab12-93eba590c976",
            "compositeImage": {
                "id": "ffd08d66-9067-4553-9471-ff7555b2d072",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41787ed3-dbd6-402a-a264-082561a218fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2a98b5e-1f50-4539-a24f-e2d51c9295b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41787ed3-dbd6-402a-a264-082561a218fc",
                    "LayerId": "21612f63-653f-49a0-b1fa-b1f06c91865b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "21612f63-653f-49a0-b1fa-b1f06c91865b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39392d95-480d-4b27-ab12-93eba590c976",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}