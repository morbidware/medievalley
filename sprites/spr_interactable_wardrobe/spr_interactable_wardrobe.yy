{
    "id": "61102b52-09c2-4bd0-a048-f4a30ec1b55a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_wardrobe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a3626ed-8864-492f-b4f8-851bcecabd7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61102b52-09c2-4bd0-a048-f4a30ec1b55a",
            "compositeImage": {
                "id": "613cc018-f817-4bea-bf7c-dfe9a00466a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a3626ed-8864-492f-b4f8-851bcecabd7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ac157f-8192-41e8-b25a-5cd8a7f219a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a3626ed-8864-492f-b4f8-851bcecabd7e",
                    "LayerId": "b7aa0b71-77ce-4c1c-b117-33e26f39b083"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b7aa0b71-77ce-4c1c-b117-33e26f39b083",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61102b52-09c2-4bd0-a048-f4a30ec1b55a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 40
}