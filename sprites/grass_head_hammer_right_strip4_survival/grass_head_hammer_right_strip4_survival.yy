{
    "id": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_hammer_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b823a384-7c26-47e3-8354-11516ebaf86e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "compositeImage": {
                "id": "2e18e628-0dbc-4729-ae9e-6e1dfe40c908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b823a384-7c26-47e3-8354-11516ebaf86e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a662ecd-20ac-4454-8682-20cb2181e9f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b823a384-7c26-47e3-8354-11516ebaf86e",
                    "LayerId": "ce027f52-beb6-4f87-96c7-2f212e0b44c3"
                }
            ]
        },
        {
            "id": "2313ef0e-e337-480b-aca0-73de383c853d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "compositeImage": {
                "id": "cc14b259-8a8f-41df-99e8-df4cdeb50dc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2313ef0e-e337-480b-aca0-73de383c853d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4b674c3-c8e8-4db0-9385-01f25c9fdd2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2313ef0e-e337-480b-aca0-73de383c853d",
                    "LayerId": "ce027f52-beb6-4f87-96c7-2f212e0b44c3"
                }
            ]
        },
        {
            "id": "a8636d84-1a6f-4834-a897-f29d935eee40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "compositeImage": {
                "id": "c2f010d2-1af5-47e1-b0c2-384c3d0444c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8636d84-1a6f-4834-a897-f29d935eee40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb096a0a-ca60-462d-89ed-dfb6808c7f5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8636d84-1a6f-4834-a897-f29d935eee40",
                    "LayerId": "ce027f52-beb6-4f87-96c7-2f212e0b44c3"
                }
            ]
        },
        {
            "id": "3d88c456-1866-4fb0-b413-b319a289868c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "compositeImage": {
                "id": "e31b3fec-9892-4ebd-8e4c-4a070b1bcef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d88c456-1866-4fb0-b413-b319a289868c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "324c9e98-c8e6-4196-b4bf-df2ed491c25b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d88c456-1866-4fb0-b413-b319a289868c",
                    "LayerId": "ce027f52-beb6-4f87-96c7-2f212e0b44c3"
                }
            ]
        },
        {
            "id": "d7ea9ae8-e94b-4644-acaf-929a61b55621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "compositeImage": {
                "id": "70a7cdc0-2746-4d25-9ed0-084a2f4e5538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ea9ae8-e94b-4644-acaf-929a61b55621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30611e22-6772-4f9d-a8e0-c2304cef5f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ea9ae8-e94b-4644-acaf-929a61b55621",
                    "LayerId": "ce027f52-beb6-4f87-96c7-2f212e0b44c3"
                }
            ]
        },
        {
            "id": "0b4bb809-db17-4bc8-8acb-efb281a63fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "compositeImage": {
                "id": "760f73be-b431-4eb8-b9ca-05fa1bcff52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b4bb809-db17-4bc8-8acb-efb281a63fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7c314fa-2c90-46ce-85d0-2f12479b9500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b4bb809-db17-4bc8-8acb-efb281a63fcc",
                    "LayerId": "ce027f52-beb6-4f87-96c7-2f212e0b44c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce027f52-beb6-4f87-96c7-2f212e0b44c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1863fd31-1822-40c9-b9ba-134ef9c3be7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}