{
    "id": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 38,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "751fff11-0c0a-48d3-bfb0-93ff09fc8563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "compositeImage": {
                "id": "4d296e42-e9ee-4dcc-834b-0d7a95716f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "751fff11-0c0a-48d3-bfb0-93ff09fc8563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "114168f2-7d3b-4314-999c-89a145148ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "751fff11-0c0a-48d3-bfb0-93ff09fc8563",
                    "LayerId": "fee7de17-0952-4aac-bd67-7cd4f183460b"
                }
            ]
        },
        {
            "id": "fe415206-519d-4a78-beff-9c7711a103cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "compositeImage": {
                "id": "a4e791c0-3620-45ba-9565-bb3403746446",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe415206-519d-4a78-beff-9c7711a103cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a11b9d-a6c5-4cac-b356-b4b2f912d88a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe415206-519d-4a78-beff-9c7711a103cd",
                    "LayerId": "fee7de17-0952-4aac-bd67-7cd4f183460b"
                }
            ]
        },
        {
            "id": "81a49074-199b-4a42-b393-d7bc915cbe68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "compositeImage": {
                "id": "a8b13033-910c-4bb7-afea-6f869755fa57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a49074-199b-4a42-b393-d7bc915cbe68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba7cd83b-bf88-4781-9c5a-3cdb7049d250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a49074-199b-4a42-b393-d7bc915cbe68",
                    "LayerId": "fee7de17-0952-4aac-bd67-7cd4f183460b"
                }
            ]
        },
        {
            "id": "a422774d-e41d-4c5c-99c4-585a001a42d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "compositeImage": {
                "id": "d7117959-50ff-42ab-afff-db22d6f564f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a422774d-e41d-4c5c-99c4-585a001a42d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcfca47b-2d31-4e92-ad32-ee86486c9175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a422774d-e41d-4c5c-99c4-585a001a42d7",
                    "LayerId": "fee7de17-0952-4aac-bd67-7cd4f183460b"
                }
            ]
        },
        {
            "id": "554bab7b-e7a0-46fd-b554-60074cfb1b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "compositeImage": {
                "id": "33ae568f-2734-4a40-bd5f-c4c319ce7815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "554bab7b-e7a0-46fd-b554-60074cfb1b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e538e906-41d4-47e8-acff-9f5cda827a85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "554bab7b-e7a0-46fd-b554-60074cfb1b58",
                    "LayerId": "fee7de17-0952-4aac-bd67-7cd4f183460b"
                }
            ]
        },
        {
            "id": "9a0ec0ed-6649-4f41-a3b8-f5c45f46eb08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "compositeImage": {
                "id": "1319bc0d-be4e-4b5e-85e2-4d6d4c56214c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0ec0ed-6649-4f41-a3b8-f5c45f46eb08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f505df36-1cc2-4ef6-92c4-1d0d413987e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0ec0ed-6649-4f41-a3b8-f5c45f46eb08",
                    "LayerId": "fee7de17-0952-4aac-bd67-7cd4f183460b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fee7de17-0952-4aac-bd67-7cd4f183460b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97fd5f97-af3c-4be7-8b9e-e16703c702b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}