{
    "id": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_edge_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7961f879-d778-4e11-b3c9-93fd0407004c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
            "compositeImage": {
                "id": "54a0bb6c-c61b-4684-a236-f342fb80fa09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7961f879-d778-4e11-b3c9-93fd0407004c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0f32a8-e344-4cca-a976-4318744fc465",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7961f879-d778-4e11-b3c9-93fd0407004c",
                    "LayerId": "2046345f-df0d-46df-a9d7-70fc4d42dbc0"
                }
            ]
        },
        {
            "id": "6944cc5f-dbc1-492d-99f3-879611df8a1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
            "compositeImage": {
                "id": "a1e2dba5-78e2-46fa-8071-fad8abf54e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6944cc5f-dbc1-492d-99f3-879611df8a1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33afe341-fc7d-4241-8d99-2286a7fcd8eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6944cc5f-dbc1-492d-99f3-879611df8a1e",
                    "LayerId": "2046345f-df0d-46df-a9d7-70fc4d42dbc0"
                }
            ]
        },
        {
            "id": "30e0f30e-585c-4a2d-b80b-8da1a1ac20bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
            "compositeImage": {
                "id": "25328322-79ac-487f-ac0c-a473a2e3fde9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30e0f30e-585c-4a2d-b80b-8da1a1ac20bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db567b69-dcc8-4b41-b319-a53c7257beae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30e0f30e-585c-4a2d-b80b-8da1a1ac20bf",
                    "LayerId": "2046345f-df0d-46df-a9d7-70fc4d42dbc0"
                }
            ]
        },
        {
            "id": "2a9f1a71-6d9a-4d73-9fbe-14c40870f4ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
            "compositeImage": {
                "id": "a34ace16-f7b5-4db7-bfd6-4d2135d8d6c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a9f1a71-6d9a-4d73-9fbe-14c40870f4ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9759ae52-750d-4fd5-82d0-96c4e5a40cde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9f1a71-6d9a-4d73-9fbe-14c40870f4ba",
                    "LayerId": "2046345f-df0d-46df-a9d7-70fc4d42dbc0"
                }
            ]
        },
        {
            "id": "8859dcdc-80dd-47b5-b0b8-19921bdfbfa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
            "compositeImage": {
                "id": "b44dd875-9153-434d-89ab-2f933a2dcde4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8859dcdc-80dd-47b5-b0b8-19921bdfbfa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5580cf50-4c69-4557-bc3d-cb4f53acb5c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8859dcdc-80dd-47b5-b0b8-19921bdfbfa5",
                    "LayerId": "2046345f-df0d-46df-a9d7-70fc4d42dbc0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2046345f-df0d-46df-a9d7-70fc4d42dbc0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74682bfb-8d9a-48c5-b033-ae2bd56033dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}