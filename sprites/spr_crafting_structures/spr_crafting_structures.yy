{
    "id": "888f0a00-b3e8-4594-9be3-fe0a7b7bf591",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_structures",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da77d99e-6587-4550-ae81-d24db378b44e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888f0a00-b3e8-4594-9be3-fe0a7b7bf591",
            "compositeImage": {
                "id": "2f23c081-f34c-4a00-8ca4-9b82cd91b665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da77d99e-6587-4550-ae81-d24db378b44e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f8081a7-37c8-4dd2-910c-16b21971efce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da77d99e-6587-4550-ae81-d24db378b44e",
                    "LayerId": "d5c83d6c-cf92-4944-917e-1aa8161d0724"
                }
            ]
        },
        {
            "id": "36c8afe1-b829-4e8b-a3f5-0320285c69a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888f0a00-b3e8-4594-9be3-fe0a7b7bf591",
            "compositeImage": {
                "id": "31b52010-5eaf-4761-8cd3-57917e271c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c8afe1-b829-4e8b-a3f5-0320285c69a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b3e909-a222-42ff-baa3-6eabd422dcbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c8afe1-b829-4e8b-a3f5-0320285c69a8",
                    "LayerId": "d5c83d6c-cf92-4944-917e-1aa8161d0724"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "d5c83d6c-cf92-4944-917e-1aa8161d0724",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "888f0a00-b3e8-4594-9be3-fe0a7b7bf591",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}