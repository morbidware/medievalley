{
    "id": "e34e1a06-d12c-44c0-8f59-5a14a66b5e52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86ed6133-66f6-45c6-964e-b4303a14477a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34e1a06-d12c-44c0-8f59-5a14a66b5e52",
            "compositeImage": {
                "id": "a658f01c-c1d3-49fc-a9dd-42b8fc8deb2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86ed6133-66f6-45c6-964e-b4303a14477a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc79798b-7b2c-4859-813c-33d4d920d2f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86ed6133-66f6-45c6-964e-b4303a14477a",
                    "LayerId": "66e36fea-575d-4695-8aff-3ce6cfd62bc9"
                }
            ]
        },
        {
            "id": "8d2da899-95f3-4712-a897-c0ccf7c88269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34e1a06-d12c-44c0-8f59-5a14a66b5e52",
            "compositeImage": {
                "id": "e8c5af39-c2c9-457b-a573-9ef8fa56f36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d2da899-95f3-4712-a897-c0ccf7c88269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc68901-1bb9-404c-a91b-f681329f6622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d2da899-95f3-4712-a897-c0ccf7c88269",
                    "LayerId": "66e36fea-575d-4695-8aff-3ce6cfd62bc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "66e36fea-575d-4695-8aff-3ce6cfd62bc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e34e1a06-d12c-44c0-8f59-5a14a66b5e52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}