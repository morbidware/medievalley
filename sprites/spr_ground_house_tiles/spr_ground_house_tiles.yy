{
    "id": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_house_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9896049b-daf1-43c6-97aa-8a83ccce8a54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "36cd3f9f-5990-42b8-8065-97c1b7055ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9896049b-daf1-43c6-97aa-8a83ccce8a54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1128a6-c82b-48a5-a439-8e6909b51179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9896049b-daf1-43c6-97aa-8a83ccce8a54",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "029318aa-07bb-4950-b114-1d07bb17ea06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "f733fa89-7953-4ca5-a496-e4693c25654a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029318aa-07bb-4950-b114-1d07bb17ea06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f983d83-6067-4672-b28b-528cffae31dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029318aa-07bb-4950-b114-1d07bb17ea06",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "a62e18e3-c599-4adc-bad1-b3fbcb5aa535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "570d0284-51a8-48e9-adf8-13c4fd017706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62e18e3-c599-4adc-bad1-b3fbcb5aa535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e75ff7b-1ca1-4661-9c97-1a99f19b74a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62e18e3-c599-4adc-bad1-b3fbcb5aa535",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "935f40ec-2a22-4f73-bc6e-4b4f625c9f6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "c0ed556a-4d34-49af-bbb1-7d61b2dc2089",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "935f40ec-2a22-4f73-bc6e-4b4f625c9f6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76628903-c4ed-4272-a5a3-1b355b297455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "935f40ec-2a22-4f73-bc6e-4b4f625c9f6b",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "e81ebdba-5080-4b20-b4f1-d65bb96d41a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "e9b944b8-b1f4-43f7-99b4-15eb8cd79c0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81ebdba-5080-4b20-b4f1-d65bb96d41a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d701de13-0e5c-4bb0-9ca0-b714c7213694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81ebdba-5080-4b20-b4f1-d65bb96d41a0",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "e5c781e3-ce16-4a94-9390-a2c1ec3c88f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "75ca0045-302e-4cd0-8d71-62ae5e20b7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c781e3-ce16-4a94-9390-a2c1ec3c88f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec83f78-07f0-44c0-b769-227f84314139",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c781e3-ce16-4a94-9390-a2c1ec3c88f0",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "9128f481-4004-4b22-a2a8-d9c1ee480081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "2c1746c6-3fb8-4443-8274-70115c50ab05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9128f481-4004-4b22-a2a8-d9c1ee480081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15945fee-9f16-4e17-b8cb-97a66e81ba58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9128f481-4004-4b22-a2a8-d9c1ee480081",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "0f06b577-71b4-4833-b8d1-519d615e609b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "49c3b3b2-92ae-49e0-b1cb-c4098775f747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f06b577-71b4-4833-b8d1-519d615e609b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d6dc0d-6416-479b-9ced-0c487d1aefff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f06b577-71b4-4833-b8d1-519d615e609b",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "0d427456-82d5-4abc-aed0-a29d7614b993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "1aa33241-f52a-4df3-8240-9357759860f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d427456-82d5-4abc-aed0-a29d7614b993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6babb680-112e-406e-91bf-90c595a3573c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d427456-82d5-4abc-aed0-a29d7614b993",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "f27bcd42-752a-4cd0-a08e-79566d46e9e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "0911fa47-be7e-4fd6-95d6-254d1eab2d6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f27bcd42-752a-4cd0-a08e-79566d46e9e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c6ec50c-7451-41c6-8c30-164fbc0f3e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f27bcd42-752a-4cd0-a08e-79566d46e9e3",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "22443105-cb32-43d5-accf-7aabe049a8a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "133e6d7b-27fd-41e4-ac46-49f21e1d73f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22443105-cb32-43d5-accf-7aabe049a8a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "574e5afc-cfa1-492c-996f-4865c390994f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22443105-cb32-43d5-accf-7aabe049a8a9",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "3790e26e-66a5-474c-8675-b738ea7da0b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "19b896d7-b3db-47b0-888c-84bcaee0b083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3790e26e-66a5-474c-8675-b738ea7da0b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a56061-8cec-4720-91d0-3bd5b21f090b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3790e26e-66a5-474c-8675-b738ea7da0b8",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "9e5109d9-38a1-41f2-9e8c-499e7af636d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "feb50c8c-b82b-4da8-a181-b8b4950f2b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e5109d9-38a1-41f2-9e8c-499e7af636d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1dafaf-1553-48e0-bb90-d0666e1cda17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e5109d9-38a1-41f2-9e8c-499e7af636d3",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "73fd7e63-d547-435d-8a24-bdd203bb9acd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "45342cb6-73ff-4d00-8e6e-33760baa20cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fd7e63-d547-435d-8a24-bdd203bb9acd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "538cd34a-1212-4f7a-842d-ec04442721b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fd7e63-d547-435d-8a24-bdd203bb9acd",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "19fe27d4-dd08-49eb-9345-c98d1e63dc9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "cf94327e-b157-41d1-becd-124d02a07768",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19fe27d4-dd08-49eb-9345-c98d1e63dc9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb144b23-6925-4d54-9fd4-2a0cc7bc4b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19fe27d4-dd08-49eb-9345-c98d1e63dc9d",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "348a7ce6-1c33-4dce-8134-99698e03eb42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "a69b1281-4e4e-43da-8603-61f8f477caeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "348a7ce6-1c33-4dce-8134-99698e03eb42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50d5922-ccf9-4c13-86d2-cdf982e262e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "348a7ce6-1c33-4dce-8134-99698e03eb42",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "3d4bb026-0ef3-443d-92a2-6e2785f0f6dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "9ad344b3-9b1e-4905-a213-0c75c3da0672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d4bb026-0ef3-443d-92a2-6e2785f0f6dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2730b1-e0a9-4112-b958-fa2598682f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d4bb026-0ef3-443d-92a2-6e2785f0f6dd",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "8581919b-03c1-414e-bbba-b70228ce4734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "fba297fa-0eea-46c7-973d-25c15fe27ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8581919b-03c1-414e-bbba-b70228ce4734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23f5857c-e46a-4baf-8e3a-ac04a1acb7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8581919b-03c1-414e-bbba-b70228ce4734",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "c4ec68a6-25ba-4494-b9f5-231cc116c418",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "39ae58ad-8bc3-4932-b6b5-6ecef15fc7d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ec68a6-25ba-4494-b9f5-231cc116c418",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b2b703-4629-425a-8f36-6f35ef2327c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ec68a6-25ba-4494-b9f5-231cc116c418",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "d1abb819-e1b9-4c4d-8ef5-87ce57f5be73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "4e48ab7a-eede-443b-87c3-141c39bac188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1abb819-e1b9-4c4d-8ef5-87ce57f5be73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb0b2b3-5583-4259-a5fc-62bd335e04c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1abb819-e1b9-4c4d-8ef5-87ce57f5be73",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "6b06f62e-cdb1-432e-bdbc-a7f67c4b198f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "da41790c-0213-40ca-b4e6-e04a12837c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b06f62e-cdb1-432e-bdbc-a7f67c4b198f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb288216-7fa2-4e6c-a2b6-75d8b6d9fbf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b06f62e-cdb1-432e-bdbc-a7f67c4b198f",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "7f103b05-edb6-4adb-8eee-0cf844048a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "2984eb59-7f14-44d9-8668-934123fb7039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f103b05-edb6-4adb-8eee-0cf844048a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb12bb8-bbc4-4081-a042-30db6a900158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f103b05-edb6-4adb-8eee-0cf844048a87",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "b81e457f-56b1-4068-b4ef-31480b91eb1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "4f696463-78f7-4e1c-803d-d7e4a6ea9b78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b81e457f-56b1-4068-b4ef-31480b91eb1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d312e99-5184-44d5-af4a-8ead69eb2259",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b81e457f-56b1-4068-b4ef-31480b91eb1a",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "3d8b87d8-c17e-445c-8080-7381228294cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "f68a6467-d679-4f37-9f93-98fea3b1f7f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d8b87d8-c17e-445c-8080-7381228294cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e27c7e96-b587-49a9-9fe0-bf6fcb845e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d8b87d8-c17e-445c-8080-7381228294cd",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "fc4f3641-af05-4886-9bf6-cfed6a7182e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "f250995a-0756-4a9c-b032-4fa92a9486cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc4f3641-af05-4886-9bf6-cfed6a7182e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e684a53-fb95-44f2-bd34-a950d2b32ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc4f3641-af05-4886-9bf6-cfed6a7182e4",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "99e88778-d766-4a59-913f-245d72865a22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "1f57448c-b059-4146-8ed1-915c56018a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e88778-d766-4a59-913f-245d72865a22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3955681a-03c2-443d-9eca-a846352b8e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e88778-d766-4a59-913f-245d72865a22",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "54a69ac1-fd39-4f55-af18-833c75965a6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "93ecc363-816e-42d6-9b0a-b72a72463fec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a69ac1-fd39-4f55-af18-833c75965a6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c226b768-cbe0-4279-900d-75cb11aa7336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a69ac1-fd39-4f55-af18-833c75965a6f",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "2a677d16-2b91-4d71-8aee-cf80bb95de63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "fdaaacd3-9522-4b66-b481-7c3d1d2195ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a677d16-2b91-4d71-8aee-cf80bb95de63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569f7a44-ddc2-48b3-9e7d-3a09c075bda0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a677d16-2b91-4d71-8aee-cf80bb95de63",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "02ff5617-0c39-4eda-953e-b07a8733aab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "18109473-e2e0-41dc-861c-e3441057012c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ff5617-0c39-4eda-953e-b07a8733aab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02313c47-96a5-4b1a-a277-0afe5013927d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ff5617-0c39-4eda-953e-b07a8733aab1",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "5c1b3e6e-6942-449f-a4a5-2a963197d272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "dcca7b68-2bb9-4a6c-92ac-4b6163e77d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1b3e6e-6942-449f-a4a5-2a963197d272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42ef17c-2674-4ddb-953a-4f73cdb074d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1b3e6e-6942-449f-a4a5-2a963197d272",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "eea7ec62-a4dc-4804-b3bf-8fb7c469a1eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "7f7ee4ea-a985-456b-b16d-e8ab667cb93b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eea7ec62-a4dc-4804-b3bf-8fb7c469a1eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eccfb2ec-1dad-47d1-8c4c-bed20706b9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eea7ec62-a4dc-4804-b3bf-8fb7c469a1eb",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "2bae8362-8042-413d-bbc5-68d69d7ba42f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "6a7049c6-9563-4513-89c4-277225fe0164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bae8362-8042-413d-bbc5-68d69d7ba42f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eaa2a89-1f4f-4d4e-a81e-5c064d2ed110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bae8362-8042-413d-bbc5-68d69d7ba42f",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "65f02969-9cdf-4794-bb6e-e6b2c71a423c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "971a67fb-5fa8-486b-b530-f6ebf153d157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f02969-9cdf-4794-bb6e-e6b2c71a423c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5978a4e0-e9e6-4611-9f03-f562b44a18d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f02969-9cdf-4794-bb6e-e6b2c71a423c",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "5ef6edc1-b0e3-49c0-ad50-a2ca250c6475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "0884dec9-34cf-4557-b509-d13315442c75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef6edc1-b0e3-49c0-ad50-a2ca250c6475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1a14a2f-9bc1-4755-b95f-9eed4d07a7ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef6edc1-b0e3-49c0-ad50-a2ca250c6475",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "c257ebae-d9fb-41d6-bcd9-86e6f11fb316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "d2f1d140-1fac-4e13-beaf-5ed581dc89b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c257ebae-d9fb-41d6-bcd9-86e6f11fb316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "429b5796-da4c-4373-836c-91f7daf0e6d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c257ebae-d9fb-41d6-bcd9-86e6f11fb316",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "eb4642f3-99b9-43c1-96a5-834074c5c50a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "874a25c2-ab62-4307-b4dc-7272682241a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb4642f3-99b9-43c1-96a5-834074c5c50a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9d91d1b-46ac-43ca-b74c-c1cf54a426db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb4642f3-99b9-43c1-96a5-834074c5c50a",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "60e1c716-d792-400e-a3cb-fda41083b228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "e342ca24-1abe-4749-a8ea-f8e2f9b71aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60e1c716-d792-400e-a3cb-fda41083b228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74dc0559-8044-4a6e-a473-961c3f8bf8e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60e1c716-d792-400e-a3cb-fda41083b228",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "b3b6f99c-e9c0-454c-8535-400d340397ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "c3e96d2c-64ca-452a-9fd7-c5834c794558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b6f99c-e9c0-454c-8535-400d340397ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27fda292-735a-4ce4-9838-4da1ea7246d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b6f99c-e9c0-454c-8535-400d340397ac",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "57c4affc-1a4a-4652-9df6-18afb018039a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "a1346d97-4cf8-44a3-913f-e5ad5143dc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c4affc-1a4a-4652-9df6-18afb018039a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db9ec943-97fa-402d-a2f1-aced0bd5bf3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c4affc-1a4a-4652-9df6-18afb018039a",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "02f330fd-a6dc-4bd8-a8e3-84b96c2f81c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "4da66c71-f468-45f0-b3c7-4fe525b1b7d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f330fd-a6dc-4bd8-a8e3-84b96c2f81c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a40f7b9-42ab-43e1-b6dc-41996d090f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f330fd-a6dc-4bd8-a8e3-84b96c2f81c9",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "7017e4a6-378c-4eaf-a9c1-ed900c62874c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "2c0c27fa-a9ab-49ad-8bfe-4b079f691225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7017e4a6-378c-4eaf-a9c1-ed900c62874c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998b0a4f-47da-4f34-b742-b351c8e54b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7017e4a6-378c-4eaf-a9c1-ed900c62874c",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "36862035-3ac7-4049-99c1-1d58d26baea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "2e8c60d7-0450-4f73-822e-26961815b564",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36862035-3ac7-4049-99c1-1d58d26baea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66f1be8d-6d26-4876-b62e-6fc9bb03cb09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36862035-3ac7-4049-99c1-1d58d26baea8",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "4033a639-ff8c-428f-b64d-1014957ffe8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "334ef994-a7db-41e4-b58d-c258b489a43f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4033a639-ff8c-428f-b64d-1014957ffe8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "530ef824-c8e8-43e3-b339-fec635940330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4033a639-ff8c-428f-b64d-1014957ffe8f",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "321685e4-cb90-43e6-a71b-c58e8ae371a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "4cc98f4c-a6ac-4500-bbb8-52c75c618da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321685e4-cb90-43e6-a71b-c58e8ae371a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80e2de37-6fe0-4959-9230-fabdb9831f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321685e4-cb90-43e6-a71b-c58e8ae371a3",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "66fef568-956c-48ec-94ba-abfdb6cb036f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "67411b96-da26-40f0-95c8-d86089e40bb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fef568-956c-48ec-94ba-abfdb6cb036f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12889f9a-62ef-4896-a234-46c916e61246",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fef568-956c-48ec-94ba-abfdb6cb036f",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "c870431b-777f-4153-9db0-c471e01f8ae1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "0dc7578e-176e-45ac-8370-b97c923d0dc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c870431b-777f-4153-9db0-c471e01f8ae1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8302654-e13a-4f82-a462-10e9d13f3c5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c870431b-777f-4153-9db0-c471e01f8ae1",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "6a4322fb-25ac-4037-a604-93ea01ac8077",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "46ec0034-acee-4395-9f3a-a5a1aa0bfe8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a4322fb-25ac-4037-a604-93ea01ac8077",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "832256bb-a4cd-4b88-acff-3eb3085fdcfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a4322fb-25ac-4037-a604-93ea01ac8077",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "f706f993-b62c-41a1-a734-4c8a088965cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "b3ae0378-025a-4f8c-936a-b568013cd41b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f706f993-b62c-41a1-a734-4c8a088965cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5662d33c-482f-47d4-b730-4d5aef957e00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f706f993-b62c-41a1-a734-4c8a088965cc",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "a72694dc-11a4-4ea8-82f6-84e1c7964dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "2b080e91-0ec5-4ea3-afe0-af541eec578b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72694dc-11a4-4ea8-82f6-84e1c7964dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb390a8-69ce-47db-9940-a4a642a569db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72694dc-11a4-4ea8-82f6-84e1c7964dd6",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "7cfdcfb3-0e10-4546-8abb-2a31ae0e8ec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "1ac3d2f8-fee9-4ab7-9ba9-c459bcd758f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cfdcfb3-0e10-4546-8abb-2a31ae0e8ec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc71104b-31ee-4033-bedb-4c77fc3b34ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cfdcfb3-0e10-4546-8abb-2a31ae0e8ec7",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "0972b293-8e9e-4de8-99ec-dd3aab4df8fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "54ead6c4-e305-4f69-a56e-b3728e93c93c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0972b293-8e9e-4de8-99ec-dd3aab4df8fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5b5d86-ca6c-4590-a7c9-8e7c867cd239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0972b293-8e9e-4de8-99ec-dd3aab4df8fe",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "337170cb-24e1-4efc-b8f9-25b1ed3f717a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "55b117e5-bcc4-4abd-8d3f-cbce7e45cfc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "337170cb-24e1-4efc-b8f9-25b1ed3f717a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46eaf467-c294-4ea9-85a4-a45155bb764a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "337170cb-24e1-4efc-b8f9-25b1ed3f717a",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "250a8bf4-8de1-45da-a0ef-92d9e1877478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "9a7407ce-7b1e-4f64-bd09-4eabe2bf43d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "250a8bf4-8de1-45da-a0ef-92d9e1877478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "027b9a21-f10d-409b-a0de-5cc64ab79a3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250a8bf4-8de1-45da-a0ef-92d9e1877478",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "52e6b84b-0ec4-4377-a783-9c4947fa4bbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "6feb371c-60ef-4c30-b031-a5bcda0fbe36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e6b84b-0ec4-4377-a783-9c4947fa4bbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f73aed5-bf0a-495d-9478-9e7db622bfef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e6b84b-0ec4-4377-a783-9c4947fa4bbb",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "cfb8cba5-985e-4fb9-a906-bc4fa486804a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "b16a8d88-a334-4ac9-b833-f257262bf183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb8cba5-985e-4fb9-a906-bc4fa486804a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "298b87f3-5273-4c9a-9016-95fd287719b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb8cba5-985e-4fb9-a906-bc4fa486804a",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "3e02e0bc-4771-47e4-b808-932a35b0039e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "a08a382b-7427-4a08-89f7-4be6f05d6221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e02e0bc-4771-47e4-b808-932a35b0039e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae59983c-ab6d-4b35-bfa9-15618fb1a9cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e02e0bc-4771-47e4-b808-932a35b0039e",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        },
        {
            "id": "0c1b8a82-93a8-4620-a154-1a1bef03070b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "compositeImage": {
                "id": "87ae9cc4-87d2-4e55-bc7f-049d44a09b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c1b8a82-93a8-4620-a154-1a1bef03070b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5e924af-ea48-4af0-ac1e-2e959da15212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c1b8a82-93a8-4620-a154-1a1bef03070b",
                    "LayerId": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "036af8cd-8121-4c95-96f4-6e65a2a3f2fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "331e38bd-1c00-4147-a4de-904ad5cd4b7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 55
}