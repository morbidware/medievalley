{
    "id": "fee788c2-46a9-44fc-b51f-487b9fe8f9b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_garlic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a34c1c9e-93ba-495c-aadd-0eaa1a8d56ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fee788c2-46a9-44fc-b51f-487b9fe8f9b8",
            "compositeImage": {
                "id": "29f05afd-d436-4171-bcaf-5fd2d43dc028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a34c1c9e-93ba-495c-aadd-0eaa1a8d56ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942d9351-6ce8-485e-bdd9-c24a121cbddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a34c1c9e-93ba-495c-aadd-0eaa1a8d56ef",
                    "LayerId": "506301aa-7d25-4ffa-b87b-f392dc457c02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "506301aa-7d25-4ffa-b87b-f392dc457c02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fee788c2-46a9-44fc-b51f-487b9fe8f9b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}