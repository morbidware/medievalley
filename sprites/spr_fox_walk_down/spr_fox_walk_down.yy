{
    "id": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6c6a215-ad1f-425e-b67b-d75d16805973",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "compositeImage": {
                "id": "b45f3a87-9891-4e70-b118-eca95816a855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c6a215-ad1f-425e-b67b-d75d16805973",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6a540d-e968-4ea1-b693-b4c6dc5b1d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c6a215-ad1f-425e-b67b-d75d16805973",
                    "LayerId": "b546f6f1-971b-4534-9143-068e8972504d"
                }
            ]
        },
        {
            "id": "b01723a1-d8a2-4263-8203-721ccb419794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "compositeImage": {
                "id": "15499bb9-2408-47c7-a3b3-de37f4df1411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b01723a1-d8a2-4263-8203-721ccb419794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ea2cf81-c6e0-468b-908f-03e4ca588371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b01723a1-d8a2-4263-8203-721ccb419794",
                    "LayerId": "b546f6f1-971b-4534-9143-068e8972504d"
                }
            ]
        },
        {
            "id": "89db0eaa-1b42-4f97-8112-3901baddb584",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "compositeImage": {
                "id": "b2cd8448-ec9a-4b83-8f9c-b4f1291230db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89db0eaa-1b42-4f97-8112-3901baddb584",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967d7c47-5b00-4326-a142-386238bd23eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89db0eaa-1b42-4f97-8112-3901baddb584",
                    "LayerId": "b546f6f1-971b-4534-9143-068e8972504d"
                }
            ]
        },
        {
            "id": "f91cbb0f-6aa2-4aeb-acc3-dd168490ca26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "compositeImage": {
                "id": "2f850b6c-d870-421c-881f-add431253451",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f91cbb0f-6aa2-4aeb-acc3-dd168490ca26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4390a2d2-07e8-48ae-b610-6d1990aa7060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f91cbb0f-6aa2-4aeb-acc3-dd168490ca26",
                    "LayerId": "b546f6f1-971b-4534-9143-068e8972504d"
                }
            ]
        },
        {
            "id": "b7b3abd6-645e-4e19-b61e-5788879a0340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "compositeImage": {
                "id": "5352cfe9-8436-4a1d-bb52-c5fe171e4814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7b3abd6-645e-4e19-b61e-5788879a0340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c3cd9a1-f201-4177-8ab7-d9109d1a593a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7b3abd6-645e-4e19-b61e-5788879a0340",
                    "LayerId": "b546f6f1-971b-4534-9143-068e8972504d"
                }
            ]
        },
        {
            "id": "d804bcd9-e3e0-49b3-a3b6-0932aac54df2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "compositeImage": {
                "id": "e5ba5b5f-efaa-4f45-b297-976946ce8dec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d804bcd9-e3e0-49b3-a3b6-0932aac54df2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4857985-b4bc-4fe6-a1be-04ae63bf7f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d804bcd9-e3e0-49b3-a3b6-0932aac54df2",
                    "LayerId": "b546f6f1-971b-4534-9143-068e8972504d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "b546f6f1-971b-4534-9143-068e8972504d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67e0dba0-4a7a-4e87-9f67-091e7f2f3374",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}