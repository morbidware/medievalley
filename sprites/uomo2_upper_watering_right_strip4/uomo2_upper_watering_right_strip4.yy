{
    "id": "df215c17-a222-4deb-a383-7c78e993d24e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fa06ad1-ff24-4f86-9206-63ac9ede58e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df215c17-a222-4deb-a383-7c78e993d24e",
            "compositeImage": {
                "id": "f234f9e5-6c7d-4a4d-8315-18381672836a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fa06ad1-ff24-4f86-9206-63ac9ede58e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77e5e0c6-f421-4a7d-b354-e64787b71dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fa06ad1-ff24-4f86-9206-63ac9ede58e3",
                    "LayerId": "309b0153-e11f-459e-98cc-783b7ddc8bc3"
                }
            ]
        },
        {
            "id": "a3ef6d59-c269-4ff3-a8a8-12d8fa1a9ed5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df215c17-a222-4deb-a383-7c78e993d24e",
            "compositeImage": {
                "id": "5804f5a5-06df-475d-8bf9-b63ceb1e0619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3ef6d59-c269-4ff3-a8a8-12d8fa1a9ed5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21a843fe-e454-4645-afba-7ff815110124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3ef6d59-c269-4ff3-a8a8-12d8fa1a9ed5",
                    "LayerId": "309b0153-e11f-459e-98cc-783b7ddc8bc3"
                }
            ]
        },
        {
            "id": "989bd6da-0e7c-4171-a587-dd8ee3129f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df215c17-a222-4deb-a383-7c78e993d24e",
            "compositeImage": {
                "id": "c7e2fbe5-a7ef-439c-8373-e970fe16a085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989bd6da-0e7c-4171-a587-dd8ee3129f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f75136b-16c7-421d-be50-19dd1d2a0910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989bd6da-0e7c-4171-a587-dd8ee3129f62",
                    "LayerId": "309b0153-e11f-459e-98cc-783b7ddc8bc3"
                }
            ]
        },
        {
            "id": "0d37d113-a3bf-4b9a-8eba-abce50fbef04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df215c17-a222-4deb-a383-7c78e993d24e",
            "compositeImage": {
                "id": "6b7d7f60-d1a5-4705-b094-147c9939527c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d37d113-a3bf-4b9a-8eba-abce50fbef04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9830d06-21a4-4981-b5e8-38a5510ec80f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d37d113-a3bf-4b9a-8eba-abce50fbef04",
                    "LayerId": "309b0153-e11f-459e-98cc-783b7ddc8bc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "309b0153-e11f-459e-98cc-783b7ddc8bc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df215c17-a222-4deb-a383-7c78e993d24e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}