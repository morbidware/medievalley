{
    "id": "cd56d538-7a6f-4d27-b123-219a06b0e86b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_attack_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "611540b1-cdd8-4933-8b49-e6bcd4b5ea48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd56d538-7a6f-4d27-b123-219a06b0e86b",
            "compositeImage": {
                "id": "4f1da2a1-4409-4220-b1c2-106f5b447287",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "611540b1-cdd8-4933-8b49-e6bcd4b5ea48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3613396a-0515-4e54-bf5b-77387d01bb07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611540b1-cdd8-4933-8b49-e6bcd4b5ea48",
                    "LayerId": "27675839-2381-46d3-b66f-1eb595e5345c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "27675839-2381-46d3-b66f-1eb595e5345c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd56d538-7a6f-4d27-b123-219a06b0e86b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}