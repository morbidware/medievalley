{
    "id": "a82ead9f-6def-4575-89b6-b3ee7debee80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_axe",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68d0710b-4765-4849-9ac2-03526dcc1c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a82ead9f-6def-4575-89b6-b3ee7debee80",
            "compositeImage": {
                "id": "b21ea64c-dd80-47a2-a4c0-e0349d8a76ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d0710b-4765-4849-9ac2-03526dcc1c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528eda1b-0514-4806-8112-a16d21c65b2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d0710b-4765-4849-9ac2-03526dcc1c7b",
                    "LayerId": "c80895f5-4ab7-4b66-9afc-d53df6a0055f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c80895f5-4ab7-4b66-9afc-d53df6a0055f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a82ead9f-6def-4575-89b6-b3ee7debee80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}