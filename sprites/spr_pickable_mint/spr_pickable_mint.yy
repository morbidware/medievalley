{
    "id": "d16ba99a-0bf0-4c4c-8c3d-8bb61c7c7437",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_mint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fae4ea21-4100-45b7-af6b-402fb3f903b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16ba99a-0bf0-4c4c-8c3d-8bb61c7c7437",
            "compositeImage": {
                "id": "5a534102-355a-413c-8eb3-89b440d81e9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae4ea21-4100-45b7-af6b-402fb3f903b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9f99a0-b77c-4a26-a86a-86dc2370e1f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae4ea21-4100-45b7-af6b-402fb3f903b4",
                    "LayerId": "9a3adca0-1f26-4392-ad83-38cb976e02ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9a3adca0-1f26-4392-ad83-38cb976e02ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d16ba99a-0bf0-4c4c-8c3d-8bb61c7c7437",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}