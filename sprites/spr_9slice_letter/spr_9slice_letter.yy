{
    "id": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9slice_letter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "553e7b15-d810-4fbf-b8fa-f1bcaf7fec34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "1f2ab5e1-709c-4588-821e-eb13ac8bf779",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553e7b15-d810-4fbf-b8fa-f1bcaf7fec34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b7e2df-90fe-4111-8561-54b4229befdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553e7b15-d810-4fbf-b8fa-f1bcaf7fec34",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "e629a15b-34c1-4d1b-a95b-1b7ff9741f1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553e7b15-d810-4fbf-b8fa-f1bcaf7fec34",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "e265709a-e09a-4d71-bce9-fffb0aa9fcb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "0a0dcc89-ea5d-499a-a724-f46ca8c51b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e265709a-e09a-4d71-bce9-fffb0aa9fcb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5caab0-af8a-43fb-97cd-ef405859e5b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e265709a-e09a-4d71-bce9-fffb0aa9fcb9",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "a381068f-44e9-47e6-a519-61978f5c7eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e265709a-e09a-4d71-bce9-fffb0aa9fcb9",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "e1389f8c-17be-49e2-8f7b-933cfbaa7cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "18f63d5f-d99b-4174-8228-4b7ef2e11381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1389f8c-17be-49e2-8f7b-933cfbaa7cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a246c857-4204-4abb-aa98-722a6a0b440b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1389f8c-17be-49e2-8f7b-933cfbaa7cad",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "5d163cbc-0e40-4ff8-8ef8-faedac4cf166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1389f8c-17be-49e2-8f7b-933cfbaa7cad",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "744c5473-ee30-4150-ae05-602460a333d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "ff07eb2c-2bbc-427e-a19f-9cfb822d2010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "744c5473-ee30-4150-ae05-602460a333d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ea2521-6fa3-4c73-ae76-126e7f62584d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "744c5473-ee30-4150-ae05-602460a333d7",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "9731ebe9-4b42-4dc2-96d1-2d26ccd66fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "744c5473-ee30-4150-ae05-602460a333d7",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "0d2c3a14-3aa5-415a-a6b9-e97800451421",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "6536203a-bfb3-4a96-ac49-81fe2ee29780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d2c3a14-3aa5-415a-a6b9-e97800451421",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ed5504f-255b-45e7-8c50-222b010d0efa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d2c3a14-3aa5-415a-a6b9-e97800451421",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "74c1a304-e268-4355-a580-5b82120944eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d2c3a14-3aa5-415a-a6b9-e97800451421",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "ae7cb29c-ac59-415f-8ab6-f1530417e299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "1f2391b1-b661-4504-8838-986fc252d002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae7cb29c-ac59-415f-8ab6-f1530417e299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6266fdd6-16ef-4e88-9d3e-8e03d5ed53b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae7cb29c-ac59-415f-8ab6-f1530417e299",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "1cf5219c-d056-43ea-800b-e1c694995351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae7cb29c-ac59-415f-8ab6-f1530417e299",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "c17a4678-7aaa-4d93-a834-34ea748707be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "a13de562-5176-4aa3-bb4c-f5fb97c37910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c17a4678-7aaa-4d93-a834-34ea748707be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a87fdc11-56c4-4cc9-be1a-d49ec0c08df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17a4678-7aaa-4d93-a834-34ea748707be",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "de536cb4-c6ab-4ab7-8a5b-6b10667b0a40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17a4678-7aaa-4d93-a834-34ea748707be",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "0deffae5-b1c2-4f97-aeaa-033f1183c350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "4993ba3b-babf-4e23-bb75-c3becf245601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0deffae5-b1c2-4f97-aeaa-033f1183c350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4588f860-4c4c-4087-b60a-b01c87c808ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0deffae5-b1c2-4f97-aeaa-033f1183c350",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "17b09bf3-3020-48fa-9621-1335840fba41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0deffae5-b1c2-4f97-aeaa-033f1183c350",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        },
        {
            "id": "fb44e071-8dcf-49cd-a402-4d365c637ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "compositeImage": {
                "id": "24fe9edc-d77a-4982-bc6c-978ad9fd550c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb44e071-8dcf-49cd-a402-4d365c637ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd20a1a-f6fa-4738-9031-d5d2e3dfa6c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb44e071-8dcf-49cd-a402-4d365c637ace",
                    "LayerId": "47424fbf-0dff-4a98-a142-9e457425d130"
                },
                {
                    "id": "460b84d4-9c30-4297-bb1b-ae0b3ac4d3d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb44e071-8dcf-49cd-a402-4d365c637ace",
                    "LayerId": "bc25df9e-d680-46c3-89e2-47cd42dc473e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "47424fbf-0dff-4a98-a142-9e457425d130",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "bc25df9e-d680-46c3-89e2-47cd42dc473e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04fcb01c-bace-432d-9bbb-0747df37b1eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}