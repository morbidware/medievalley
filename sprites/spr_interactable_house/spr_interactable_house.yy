{
    "id": "1dc1e004-3bcc-4aac-82f1-f683c02212d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 86,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73fb5e7c-fc24-4b5e-8852-4841174bb298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dc1e004-3bcc-4aac-82f1-f683c02212d9",
            "compositeImage": {
                "id": "1d360511-59cc-4539-aa04-29700ed291aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fb5e7c-fc24-4b5e-8852-4841174bb298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c59bc6-bd41-42e9-ba35-53952fe2bed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fb5e7c-fc24-4b5e-8852-4841174bb298",
                    "LayerId": "5beb1b30-5923-43e0-8d05-73ed7bc9fa4f"
                }
            ]
        },
        {
            "id": "29cd3a94-43ec-4d1c-8d69-39d828b9748a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dc1e004-3bcc-4aac-82f1-f683c02212d9",
            "compositeImage": {
                "id": "257847a6-2fef-4da1-98a5-33ef4a7986a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29cd3a94-43ec-4d1c-8d69-39d828b9748a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23586add-8e0a-441c-b1ae-0a68166e3f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29cd3a94-43ec-4d1c-8d69-39d828b9748a",
                    "LayerId": "5beb1b30-5923-43e0-8d05-73ed7bc9fa4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "5beb1b30-5923-43e0-8d05-73ed7bc9fa4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dc1e004-3bcc-4aac-82f1-f683c02212d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 80
}