{
    "id": "69ab0fbe-4956-482b-8b20-7c681c67db3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d061f348-e429-4a1a-b2f5-827459ea76f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69ab0fbe-4956-482b-8b20-7c681c67db3f",
            "compositeImage": {
                "id": "90acccfe-200a-449d-9a36-da28b2b59032",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d061f348-e429-4a1a-b2f5-827459ea76f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d7becb-49d0-4633-9ed3-fb68c872549e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d061f348-e429-4a1a-b2f5-827459ea76f1",
                    "LayerId": "84878cfc-0447-4b24-b3ab-1a932e624cee"
                }
            ]
        },
        {
            "id": "518a819d-aaca-4a67-81a2-ea32e2593884",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69ab0fbe-4956-482b-8b20-7c681c67db3f",
            "compositeImage": {
                "id": "9175a6dd-b50c-4df4-babf-42dd17afdcac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "518a819d-aaca-4a67-81a2-ea32e2593884",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35408972-0e48-499a-a241-cf0cc196377c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "518a819d-aaca-4a67-81a2-ea32e2593884",
                    "LayerId": "84878cfc-0447-4b24-b3ab-1a932e624cee"
                }
            ]
        },
        {
            "id": "82becbde-c217-4715-8dbd-006ae281492d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69ab0fbe-4956-482b-8b20-7c681c67db3f",
            "compositeImage": {
                "id": "9ee38664-044e-4e10-9afd-bd4f2042b091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82becbde-c217-4715-8dbd-006ae281492d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d978daa-8561-48aa-9a66-7ad87e531d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82becbde-c217-4715-8dbd-006ae281492d",
                    "LayerId": "84878cfc-0447-4b24-b3ab-1a932e624cee"
                }
            ]
        },
        {
            "id": "1007ddb2-a76f-4d33-85c8-593ec2129c7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69ab0fbe-4956-482b-8b20-7c681c67db3f",
            "compositeImage": {
                "id": "cbb83998-5706-4d9d-82b4-5594f245f120",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1007ddb2-a76f-4d33-85c8-593ec2129c7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e56f9d5a-1d95-4cc4-8bbf-4a95cfb613f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1007ddb2-a76f-4d33-85c8-593ec2129c7d",
                    "LayerId": "84878cfc-0447-4b24-b3ab-1a932e624cee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84878cfc-0447-4b24-b3ab-1a932e624cee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69ab0fbe-4956-482b-8b20-7c681c67db3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}