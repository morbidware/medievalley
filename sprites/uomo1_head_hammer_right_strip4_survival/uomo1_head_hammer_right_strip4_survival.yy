{
    "id": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_hammer_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2754d79-9571-44e6-83b8-022b68b8010e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "compositeImage": {
                "id": "42a95ccd-00b7-45f0-b7e6-8bcb270b6050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2754d79-9571-44e6-83b8-022b68b8010e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25dcc1be-3984-41e0-be0f-c85e960d00f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2754d79-9571-44e6-83b8-022b68b8010e",
                    "LayerId": "30df18c2-4569-4165-9768-9de019a73c30"
                }
            ]
        },
        {
            "id": "759c54e8-fe48-43ae-b720-c6209b7caf9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "compositeImage": {
                "id": "329eac94-4474-4fdc-b21d-7722e0d5b45a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "759c54e8-fe48-43ae-b720-c6209b7caf9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8bd1ee-7b5b-4a69-84c1-a5a361c79410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "759c54e8-fe48-43ae-b720-c6209b7caf9f",
                    "LayerId": "30df18c2-4569-4165-9768-9de019a73c30"
                }
            ]
        },
        {
            "id": "faeee35c-07f7-466a-89b2-a7cb5ae8edb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "compositeImage": {
                "id": "267017fa-d863-4b20-bc51-311622a8705e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faeee35c-07f7-466a-89b2-a7cb5ae8edb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a5d7d7-b156-4d3e-a70a-ac72c22b7eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faeee35c-07f7-466a-89b2-a7cb5ae8edb0",
                    "LayerId": "30df18c2-4569-4165-9768-9de019a73c30"
                }
            ]
        },
        {
            "id": "5f100bc1-33b2-40cd-b45c-fbe7870718cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "compositeImage": {
                "id": "e4790c1d-2bc0-4108-8093-63b72b8b28f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f100bc1-33b2-40cd-b45c-fbe7870718cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27078237-b4ce-4611-a6d8-83d71ed5e87a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f100bc1-33b2-40cd-b45c-fbe7870718cf",
                    "LayerId": "30df18c2-4569-4165-9768-9de019a73c30"
                }
            ]
        },
        {
            "id": "1b8f6e90-2f2c-4edf-afdb-1c35b7a4539b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "compositeImage": {
                "id": "d8a5598f-01eb-4224-8597-10f2cf38e835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8f6e90-2f2c-4edf-afdb-1c35b7a4539b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a75690d4-a637-4eba-ac50-56f59d9f70cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8f6e90-2f2c-4edf-afdb-1c35b7a4539b",
                    "LayerId": "30df18c2-4569-4165-9768-9de019a73c30"
                }
            ]
        },
        {
            "id": "20645f8d-c82b-43e4-a50a-176dc745da73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "compositeImage": {
                "id": "3dfa2c77-a99c-4098-8cc6-e4656665165c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20645f8d-c82b-43e4-a50a-176dc745da73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "800ae174-acd7-4b7e-9f78-15e6d2c4366c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20645f8d-c82b-43e4-a50a-176dc745da73",
                    "LayerId": "30df18c2-4569-4165-9768-9de019a73c30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "30df18c2-4569-4165-9768-9de019a73c30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d406490-b1a8-4674-a5fc-3ecf7cf54f6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}