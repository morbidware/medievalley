{
    "id": "5629df0f-7c86-4573-bbc7-e0f87d8648fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_plains_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91555c60-5164-47cf-a7a8-19ecb99d146f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5629df0f-7c86-4573-bbc7-e0f87d8648fe",
            "compositeImage": {
                "id": "309ac216-0fa8-4a38-ae07-97a7735909a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91555c60-5164-47cf-a7a8-19ecb99d146f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee4e6b05-72f0-4174-9890-25471dbc6d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91555c60-5164-47cf-a7a8-19ecb99d146f",
                    "LayerId": "167f9571-fc58-43e9-bdcf-f5ce51d5792d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "167f9571-fc58-43e9-bdcf-f5ce51d5792d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5629df0f-7c86-4573-bbc7-e0f87d8648fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}