{
    "id": "b0abf0aa-e788-4ff5-a8fe-9b962d47df41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pp_light_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 2,
    "bbox_right": 127,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "8c60b0e2-67b1-4f7d-b998-1c3ca90e1617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0abf0aa-e788-4ff5-a8fe-9b962d47df41",
            "compositeImage": {
                "id": "63009cd4-9eea-4e18-8e16-83b697d384d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c60b0e2-67b1-4f7d-b998-1c3ca90e1617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8a7ecc-8a17-41e0-8dc3-c4eeac235632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c60b0e2-67b1-4f7d-b998-1c3ca90e1617",
                    "LayerId": "8f91bf44-e31e-4bbd-b023-81d7b6348679"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8f91bf44-e31e-4bbd-b023-81d7b6348679",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0abf0aa-e788-4ff5-a8fe-9b962d47df41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}