{
    "id": "1a942ee1-728f-46b9-85b8-7205537dcd17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_hop_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ba50445-fee1-45a5-9926-398fd9a3ea78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "compositeImage": {
                "id": "f3c70e57-3bcf-4d4b-a01d-124bba734a13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba50445-fee1-45a5-9926-398fd9a3ea78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a918671-649b-412b-a2aa-2091bd9ae453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba50445-fee1-45a5-9926-398fd9a3ea78",
                    "LayerId": "6e2bf0f3-0925-4a07-bb76-800aee5df11b"
                }
            ]
        },
        {
            "id": "43537053-231e-42db-87be-059478dadeaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "compositeImage": {
                "id": "d25b8ccf-6203-44d9-8a5d-74c538a25e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43537053-231e-42db-87be-059478dadeaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe342a50-e512-43a0-ae11-9b2873c388e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43537053-231e-42db-87be-059478dadeaf",
                    "LayerId": "6e2bf0f3-0925-4a07-bb76-800aee5df11b"
                }
            ]
        },
        {
            "id": "6b8949f8-95b2-46c6-b58b-90944d2b6b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "compositeImage": {
                "id": "af4f4e54-9e9f-446c-b56d-78f77109de22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8949f8-95b2-46c6-b58b-90944d2b6b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c95ca4bb-3a21-408f-ae2e-03eb2db23321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8949f8-95b2-46c6-b58b-90944d2b6b09",
                    "LayerId": "6e2bf0f3-0925-4a07-bb76-800aee5df11b"
                }
            ]
        },
        {
            "id": "83b3bba3-5245-4675-a21b-fe0a9df01ede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "compositeImage": {
                "id": "3980884d-ca80-49c9-b4ca-9c62e65b8604",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83b3bba3-5245-4675-a21b-fe0a9df01ede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efaf0631-97cd-4403-a348-1ab1173a9727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83b3bba3-5245-4675-a21b-fe0a9df01ede",
                    "LayerId": "6e2bf0f3-0925-4a07-bb76-800aee5df11b"
                }
            ]
        },
        {
            "id": "a72a7487-6394-4421-b051-8b8694457836",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "compositeImage": {
                "id": "f8cb81ea-e6ff-4a87-baed-e79f85013fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72a7487-6394-4421-b051-8b8694457836",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65c8f72-cce8-463a-82ff-62d796ccecb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72a7487-6394-4421-b051-8b8694457836",
                    "LayerId": "6e2bf0f3-0925-4a07-bb76-800aee5df11b"
                }
            ]
        },
        {
            "id": "ecb5d675-f7c8-402d-9054-ff78d2ff66e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "compositeImage": {
                "id": "99daf7f5-9314-4c5b-be7e-db5971cc5f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecb5d675-f7c8-402d-9054-ff78d2ff66e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e09c6310-5ac9-47cc-9a2c-3a9cf34e078c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecb5d675-f7c8-402d-9054-ff78d2ff66e9",
                    "LayerId": "6e2bf0f3-0925-4a07-bb76-800aee5df11b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6e2bf0f3-0925-4a07-bb76-800aee5df11b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a942ee1-728f-46b9-85b8-7205537dcd17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 28
}