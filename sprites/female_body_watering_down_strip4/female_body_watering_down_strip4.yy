{
    "id": "de7b4537-2213-4031-b9b8-e71c3d68c7b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78d9a303-eaed-4ac0-94a2-d6f6095f94f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7b4537-2213-4031-b9b8-e71c3d68c7b3",
            "compositeImage": {
                "id": "f9785f9b-0921-49ba-b61d-7053d87d3579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d9a303-eaed-4ac0-94a2-d6f6095f94f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cfeb7d3-0c08-4132-b114-2ac8f48d147a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d9a303-eaed-4ac0-94a2-d6f6095f94f5",
                    "LayerId": "3d7ea1e7-a1bb-496b-aabd-529e64a02f6e"
                }
            ]
        },
        {
            "id": "a5adc567-2cda-46fb-9384-9392e0dab85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7b4537-2213-4031-b9b8-e71c3d68c7b3",
            "compositeImage": {
                "id": "ad9e174b-925b-4e44-a316-aba7b8941456",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5adc567-2cda-46fb-9384-9392e0dab85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbd19157-d848-4610-aa06-db399c67defc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5adc567-2cda-46fb-9384-9392e0dab85f",
                    "LayerId": "3d7ea1e7-a1bb-496b-aabd-529e64a02f6e"
                }
            ]
        },
        {
            "id": "e96c9adb-5822-4b78-b9be-ed0e3a381f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7b4537-2213-4031-b9b8-e71c3d68c7b3",
            "compositeImage": {
                "id": "5772b82a-772b-4b2c-9023-d71ab260d29a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96c9adb-5822-4b78-b9be-ed0e3a381f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76011ad5-1ecd-47ef-a9dc-df1c81db63c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96c9adb-5822-4b78-b9be-ed0e3a381f88",
                    "LayerId": "3d7ea1e7-a1bb-496b-aabd-529e64a02f6e"
                }
            ]
        },
        {
            "id": "5d9f9b17-a084-442c-a19e-5674897928a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de7b4537-2213-4031-b9b8-e71c3d68c7b3",
            "compositeImage": {
                "id": "6d697560-eb8b-4b91-9735-67b893df2c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d9f9b17-a084-442c-a19e-5674897928a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4d654e-622a-4f6a-b8a3-e3ad981da513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d9f9b17-a084-442c-a19e-5674897928a5",
                    "LayerId": "3d7ea1e7-a1bb-496b-aabd-529e64a02f6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3d7ea1e7-a1bb-496b-aabd-529e64a02f6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de7b4537-2213-4031-b9b8-e71c3d68c7b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}