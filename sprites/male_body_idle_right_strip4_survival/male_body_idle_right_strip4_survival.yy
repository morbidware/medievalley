{
    "id": "c6b1daee-26e6-4ae8-89eb-f780811646c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_idle_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1d54a88-5e4e-419f-a3eb-360522ecba53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b1daee-26e6-4ae8-89eb-f780811646c5",
            "compositeImage": {
                "id": "755ad9cc-1902-4bc1-b7bf-f30959b807e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1d54a88-5e4e-419f-a3eb-360522ecba53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f4102a-cfdd-45f5-a70a-52caead2abb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1d54a88-5e4e-419f-a3eb-360522ecba53",
                    "LayerId": "3a29d987-ea3d-4a8e-b966-ff3ec42e0634"
                }
            ]
        },
        {
            "id": "02d84421-bd01-4cab-803d-40571411383d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b1daee-26e6-4ae8-89eb-f780811646c5",
            "compositeImage": {
                "id": "3750ed5e-54c3-4578-a91f-3b7ee1d44dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d84421-bd01-4cab-803d-40571411383d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2168a009-1a6e-45a5-a660-d59c8c62aba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d84421-bd01-4cab-803d-40571411383d",
                    "LayerId": "3a29d987-ea3d-4a8e-b966-ff3ec42e0634"
                }
            ]
        },
        {
            "id": "0ebb5aea-ea03-423e-a796-5f85d68723fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b1daee-26e6-4ae8-89eb-f780811646c5",
            "compositeImage": {
                "id": "7a754a80-70db-421d-9c6d-9bb537e0a274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ebb5aea-ea03-423e-a796-5f85d68723fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed66bd6-bc08-49e5-81a4-4971ef026c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ebb5aea-ea03-423e-a796-5f85d68723fc",
                    "LayerId": "3a29d987-ea3d-4a8e-b966-ff3ec42e0634"
                }
            ]
        },
        {
            "id": "d3c37a48-b1c2-4079-aca0-f8c6028a82d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6b1daee-26e6-4ae8-89eb-f780811646c5",
            "compositeImage": {
                "id": "d1955c92-d839-4c36-b70e-d81e19cd9cfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3c37a48-b1c2-4079-aca0-f8c6028a82d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8900b80f-4c96-4971-bb3e-c4dada18f542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3c37a48-b1c2-4079-aca0-f8c6028a82d7",
                    "LayerId": "3a29d987-ea3d-4a8e-b966-ff3ec42e0634"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3a29d987-ea3d-4a8e-b966-ff3ec42e0634",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6b1daee-26e6-4ae8-89eb-f780811646c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}