{
    "id": "a593cba0-87c0-4bc1-ba1f-8b6ffb6687c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81dd4743-0b32-4ed8-8d0e-dcf45c9d16c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a593cba0-87c0-4bc1-ba1f-8b6ffb6687c9",
            "compositeImage": {
                "id": "24e022d1-2f79-4ba2-b922-efdee131da82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81dd4743-0b32-4ed8-8d0e-dcf45c9d16c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c82eeae9-4316-43b3-baff-21ff9873825f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81dd4743-0b32-4ed8-8d0e-dcf45c9d16c7",
                    "LayerId": "0543f341-282c-4ba8-bcdc-e6782e18544e"
                }
            ]
        },
        {
            "id": "a90331f2-8b65-48fc-ac95-8be68d71264f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a593cba0-87c0-4bc1-ba1f-8b6ffb6687c9",
            "compositeImage": {
                "id": "7c4856bb-ca04-4429-95d9-61f95a5d0b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90331f2-8b65-48fc-ac95-8be68d71264f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38b79c10-9768-4494-9cec-dc7ec6157593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90331f2-8b65-48fc-ac95-8be68d71264f",
                    "LayerId": "0543f341-282c-4ba8-bcdc-e6782e18544e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "0543f341-282c-4ba8-bcdc-e6782e18544e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a593cba0-87c0-4bc1-ba1f-8b6ffb6687c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}