{
    "id": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_watering_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cf74ceb-816f-4602-9df3-2ad64dea289b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "compositeImage": {
                "id": "778ec5ad-fbab-4b3f-b259-fb0283c74d4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf74ceb-816f-4602-9df3-2ad64dea289b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a98a4a3-9a81-48d3-aaf3-f4af965023f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf74ceb-816f-4602-9df3-2ad64dea289b",
                    "LayerId": "4146faa6-d7c1-4fcb-824d-137415471daa"
                }
            ]
        },
        {
            "id": "b2462637-989b-4b6c-bc69-3dc5e89db590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "compositeImage": {
                "id": "aaf03ca9-edc5-4053-af4c-b17a2ea7c347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2462637-989b-4b6c-bc69-3dc5e89db590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dc4e95-15f6-42d0-bf25-7095efb95bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2462637-989b-4b6c-bc69-3dc5e89db590",
                    "LayerId": "4146faa6-d7c1-4fcb-824d-137415471daa"
                }
            ]
        },
        {
            "id": "236bfa72-350a-4dcc-87b5-e71a08eb95a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "compositeImage": {
                "id": "b626ba6f-8305-43b4-9a56-75728494ed04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "236bfa72-350a-4dcc-87b5-e71a08eb95a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a7d4bd-b227-44d9-9cd3-82b5cc0b48f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "236bfa72-350a-4dcc-87b5-e71a08eb95a4",
                    "LayerId": "4146faa6-d7c1-4fcb-824d-137415471daa"
                }
            ]
        },
        {
            "id": "e8a0454d-f12e-4306-a780-fae551d7bd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "compositeImage": {
                "id": "ab604754-b8fe-45d5-a811-ecf3d23e26b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8a0454d-f12e-4306-a780-fae551d7bd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9641a37-9c84-44a9-b811-b3832d2c2fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8a0454d-f12e-4306-a780-fae551d7bd2c",
                    "LayerId": "4146faa6-d7c1-4fcb-824d-137415471daa"
                }
            ]
        },
        {
            "id": "85c00987-5e2e-4b04-aa88-0739616a3340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "compositeImage": {
                "id": "2edf8326-d55f-4364-b271-2e8ad62d590a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85c00987-5e2e-4b04-aa88-0739616a3340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36fc1e8d-f673-4d45-babe-395527cee4b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85c00987-5e2e-4b04-aa88-0739616a3340",
                    "LayerId": "4146faa6-d7c1-4fcb-824d-137415471daa"
                }
            ]
        },
        {
            "id": "e4cfb994-e973-4d8f-9c2d-ee8f8c98d228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "compositeImage": {
                "id": "9a5b505b-22f6-445b-8800-957e4e4952cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4cfb994-e973-4d8f-9c2d-ee8f8c98d228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6619518b-a5a6-4b6b-b5e7-920f52c2e2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4cfb994-e973-4d8f-9c2d-ee8f8c98d228",
                    "LayerId": "4146faa6-d7c1-4fcb-824d-137415471daa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4146faa6-d7c1-4fcb-824d-137415471daa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40456cc4-e7d6-44d4-b93a-2e575bc2b12a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}