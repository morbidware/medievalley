{
    "id": "1dd3d867-9bad-47ad-8148-df474e8cde5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "235e70d8-a329-4165-b16c-648fb1e63373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd3d867-9bad-47ad-8148-df474e8cde5e",
            "compositeImage": {
                "id": "72002aa2-b5b1-42f4-964e-d091e2aa84bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235e70d8-a329-4165-b16c-648fb1e63373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09954094-30f1-4e82-932a-779f48b3a001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235e70d8-a329-4165-b16c-648fb1e63373",
                    "LayerId": "4df4dff9-7572-4c51-aa74-1b4b801d8288"
                }
            ]
        },
        {
            "id": "2cae3d14-20d2-4fab-9a6b-c2fc8c1a6d14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd3d867-9bad-47ad-8148-df474e8cde5e",
            "compositeImage": {
                "id": "cc671f6d-8a79-46a2-b20a-26d276a01b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cae3d14-20d2-4fab-9a6b-c2fc8c1a6d14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b43f9f-d05e-4bf2-ae7d-faa691919e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cae3d14-20d2-4fab-9a6b-c2fc8c1a6d14",
                    "LayerId": "4df4dff9-7572-4c51-aa74-1b4b801d8288"
                }
            ]
        },
        {
            "id": "f8dd5096-d6a2-4f97-a64f-74333eab9133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd3d867-9bad-47ad-8148-df474e8cde5e",
            "compositeImage": {
                "id": "875143f2-ee3e-42ab-98b2-9453cf438081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8dd5096-d6a2-4f97-a64f-74333eab9133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecc8165a-ccab-446b-a2d4-ccfb6461d24f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8dd5096-d6a2-4f97-a64f-74333eab9133",
                    "LayerId": "4df4dff9-7572-4c51-aa74-1b4b801d8288"
                }
            ]
        },
        {
            "id": "7ff25975-ca3c-40fa-8e9d-2a25a1c4f85d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dd3d867-9bad-47ad-8148-df474e8cde5e",
            "compositeImage": {
                "id": "ca2bfd06-0e02-4217-b6e6-537d8fc12efa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ff25975-ca3c-40fa-8e9d-2a25a1c4f85d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8db55c-b0fe-4782-b3c3-0f54e34f2b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ff25975-ca3c-40fa-8e9d-2a25a1c4f85d",
                    "LayerId": "4df4dff9-7572-4c51-aa74-1b4b801d8288"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4df4dff9-7572-4c51-aa74-1b4b801d8288",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dd3d867-9bad-47ad-8148-df474e8cde5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}