{
    "id": "05f480b0-a9a5-4e59-89dc-4b4560381c86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64721b4b-8188-4264-8b33-caa65c2de11e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f480b0-a9a5-4e59-89dc-4b4560381c86",
            "compositeImage": {
                "id": "ffa00fe3-d881-403c-a444-675da4fa2c11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64721b4b-8188-4264-8b33-caa65c2de11e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b34865bc-496e-4ff7-b834-ca8a0287723a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64721b4b-8188-4264-8b33-caa65c2de11e",
                    "LayerId": "31ce1569-4631-4ae5-94c9-21fa5d2d0ede"
                }
            ]
        },
        {
            "id": "e7bce421-bc36-450e-badc-d010cae0579b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f480b0-a9a5-4e59-89dc-4b4560381c86",
            "compositeImage": {
                "id": "bef55e09-71f3-4d66-9fe7-718f8852b96b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7bce421-bc36-450e-badc-d010cae0579b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4f244d8-d558-4b20-a7bb-ab3488f0822d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7bce421-bc36-450e-badc-d010cae0579b",
                    "LayerId": "31ce1569-4631-4ae5-94c9-21fa5d2d0ede"
                }
            ]
        },
        {
            "id": "13e32a5f-1ac9-4c95-a16f-71bb08379c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f480b0-a9a5-4e59-89dc-4b4560381c86",
            "compositeImage": {
                "id": "7ec9fca1-288f-44ed-b80f-4363e565fee5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e32a5f-1ac9-4c95-a16f-71bb08379c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70d2fa9f-d68e-43d4-8770-73340412f542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e32a5f-1ac9-4c95-a16f-71bb08379c43",
                    "LayerId": "31ce1569-4631-4ae5-94c9-21fa5d2d0ede"
                }
            ]
        },
        {
            "id": "3f52e9ff-b0a7-4e16-8e4a-530faa33ed66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05f480b0-a9a5-4e59-89dc-4b4560381c86",
            "compositeImage": {
                "id": "34d8a3a6-32e8-40f6-b9f6-bc925709494b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f52e9ff-b0a7-4e16-8e4a-530faa33ed66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b748a4c-302e-47ef-9d0b-4df88fad5487",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f52e9ff-b0a7-4e16-8e4a-530faa33ed66",
                    "LayerId": "31ce1569-4631-4ae5-94c9-21fa5d2d0ede"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "31ce1569-4631-4ae5-94c9-21fa5d2d0ede",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05f480b0-a9a5-4e59-89dc-4b4560381c86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}