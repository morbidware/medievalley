{
    "id": "6edaab19-2e5f-4526-a13f-168e4fced113",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_gold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de868212-1a59-4d77-90fb-6f09c9a58b75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6edaab19-2e5f-4526-a13f-168e4fced113",
            "compositeImage": {
                "id": "fafba220-c1cf-4b05-a27d-0f018f4b0fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de868212-1a59-4d77-90fb-6f09c9a58b75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72d4aeed-1e83-4d9c-a54e-61521b179339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de868212-1a59-4d77-90fb-6f09c9a58b75",
                    "LayerId": "d2c1ae7e-1813-41d8-9597-cfbc4d96e091"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d2c1ae7e-1813-41d8-9597-cfbc4d96e091",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6edaab19-2e5f-4526-a13f-168e4fced113",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}