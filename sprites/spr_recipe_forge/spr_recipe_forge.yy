{
    "id": "b1cbcba9-1784-4e70-9f3c-f9d53ca6bdef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipe_forge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "452a6d2c-fa86-4047-a3d8-2fc11559d33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1cbcba9-1784-4e70-9f3c-f9d53ca6bdef",
            "compositeImage": {
                "id": "c202073f-29e2-4e35-9d09-585895e0d19d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "452a6d2c-fa86-4047-a3d8-2fc11559d33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67b7c97-078b-4ceb-99b3-88b1e621f74a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "452a6d2c-fa86-4047-a3d8-2fc11559d33c",
                    "LayerId": "e9ab5c05-7f0a-40a3-8aec-27f2b51ea8a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "e9ab5c05-7f0a-40a3-8aec-27f2b51ea8a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1cbcba9-1784-4e70-9f3c-f9d53ca6bdef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}