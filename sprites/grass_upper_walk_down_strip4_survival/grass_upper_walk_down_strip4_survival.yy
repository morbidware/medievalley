{
    "id": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_walk_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c0f5791-0f0d-41f8-9f49-60be04c0f935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "compositeImage": {
                "id": "f8cb6c83-9d17-4001-82ae-e074ec8f75d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c0f5791-0f0d-41f8-9f49-60be04c0f935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efac6b07-0dc8-446b-b081-a36661bc9276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c0f5791-0f0d-41f8-9f49-60be04c0f935",
                    "LayerId": "5d608a71-0375-4523-a182-5b65f7a8ca22"
                }
            ]
        },
        {
            "id": "7f35ba5b-0c32-4fd0-8c1c-aa1f4681ad12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "compositeImage": {
                "id": "234e615e-a3e5-4f6f-b671-e325854d2e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f35ba5b-0c32-4fd0-8c1c-aa1f4681ad12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008f8adf-894c-41de-9f1f-89f5e5009a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f35ba5b-0c32-4fd0-8c1c-aa1f4681ad12",
                    "LayerId": "5d608a71-0375-4523-a182-5b65f7a8ca22"
                }
            ]
        },
        {
            "id": "3596bd04-60f6-40cf-ba66-f74cc8fe4dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "compositeImage": {
                "id": "82ab160c-265f-4a8f-9794-ec67c9245fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3596bd04-60f6-40cf-ba66-f74cc8fe4dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed81ac49-5ebe-4131-bcae-ca9a1b18eed6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3596bd04-60f6-40cf-ba66-f74cc8fe4dc8",
                    "LayerId": "5d608a71-0375-4523-a182-5b65f7a8ca22"
                }
            ]
        },
        {
            "id": "0da3c985-73e4-4cb1-9637-c8f61d8055b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "compositeImage": {
                "id": "64790896-07b5-4ab2-8f07-bd7afe04e880",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0da3c985-73e4-4cb1-9637-c8f61d8055b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c62911-959f-40c0-af48-1591160a3f93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0da3c985-73e4-4cb1-9637-c8f61d8055b4",
                    "LayerId": "5d608a71-0375-4523-a182-5b65f7a8ca22"
                }
            ]
        },
        {
            "id": "a413677c-7a61-4cc6-bc7a-18313f56f8d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "compositeImage": {
                "id": "6974cf8f-429c-4e2a-8376-73db6f368a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a413677c-7a61-4cc6-bc7a-18313f56f8d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fabeca85-9072-45b9-aff3-9ec63504b841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a413677c-7a61-4cc6-bc7a-18313f56f8d5",
                    "LayerId": "5d608a71-0375-4523-a182-5b65f7a8ca22"
                }
            ]
        },
        {
            "id": "62d9ffc9-b901-4e70-adcf-e88a23ccf97a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "compositeImage": {
                "id": "1bba70aa-a81b-4adc-afd7-56559a1b2f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d9ffc9-b901-4e70-adcf-e88a23ccf97a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1be97a-6bb6-4b2c-9aa7-c4e24d064bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d9ffc9-b901-4e70-adcf-e88a23ccf97a",
                    "LayerId": "5d608a71-0375-4523-a182-5b65f7a8ca22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5d608a71-0375-4523-a182-5b65f7a8ca22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3363a5f6-b81c-4cf9-a7e8-df38d8f6d57e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}