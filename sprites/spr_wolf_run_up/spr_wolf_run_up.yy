{
    "id": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ea0727a-1beb-47a6-ac3b-51f9f7e2da39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "compositeImage": {
                "id": "f13eeb2f-e36a-4a44-9ce5-80c2d4764221",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ea0727a-1beb-47a6-ac3b-51f9f7e2da39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a90e86c2-d576-4ef0-9878-73ced637ffdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ea0727a-1beb-47a6-ac3b-51f9f7e2da39",
                    "LayerId": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4"
                }
            ]
        },
        {
            "id": "b323ba36-d073-4a94-b453-5b7f4ab74d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "compositeImage": {
                "id": "af0c7980-9793-4f31-b37f-5f91a88d59ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b323ba36-d073-4a94-b453-5b7f4ab74d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ec6f08a-dbe6-45f1-b977-d2d897b7d968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b323ba36-d073-4a94-b453-5b7f4ab74d9b",
                    "LayerId": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4"
                }
            ]
        },
        {
            "id": "0c1a2558-81d1-4451-a50d-d7df1bcd8a23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "compositeImage": {
                "id": "dbacde1a-59ef-44b0-955b-f5498709161c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c1a2558-81d1-4451-a50d-d7df1bcd8a23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b26ba080-e375-4f39-86e6-bae9e465fb59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c1a2558-81d1-4451-a50d-d7df1bcd8a23",
                    "LayerId": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4"
                }
            ]
        },
        {
            "id": "d1db76d7-2796-4753-97a7-3ccfa410cca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "compositeImage": {
                "id": "9223e6c2-e98d-4e87-a147-f21852185a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1db76d7-2796-4753-97a7-3ccfa410cca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdadabd1-f486-46fa-91fa-2b9746f3d663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1db76d7-2796-4753-97a7-3ccfa410cca7",
                    "LayerId": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4"
                }
            ]
        },
        {
            "id": "95d264f4-c51a-431e-ae3a-4cdf6f279446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "compositeImage": {
                "id": "5552a3b9-c492-44ce-a95d-a76ce6b7d51f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d264f4-c51a-431e-ae3a-4cdf6f279446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132f76ac-585d-49b1-b662-419fe94b54f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d264f4-c51a-431e-ae3a-4cdf6f279446",
                    "LayerId": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4"
                }
            ]
        },
        {
            "id": "690fe770-51f7-4425-a560-08ab940cedd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "compositeImage": {
                "id": "1dbb959e-467e-4480-928e-7406c11bd997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "690fe770-51f7-4425-a560-08ab940cedd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cea8b227-647f-4c86-9c48-7904f006707d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "690fe770-51f7-4425-a560-08ab940cedd9",
                    "LayerId": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "c03dea17-2f42-4faa-afe9-fed45d7ab0d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c08b0af7-742a-47bd-ac92-929ce2c0cbc8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}