{
    "id": "e3fd3645-bcd0-45a4-ad51-8346698444f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_die_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 15,
    "bbox_right": 30,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ccdf32f-474f-4b9d-b223-7e755590717d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3fd3645-bcd0-45a4-ad51-8346698444f6",
            "compositeImage": {
                "id": "489fe11a-f4ae-4a95-823f-fd2b71a7e502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ccdf32f-474f-4b9d-b223-7e755590717d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a49175f-2efa-42c3-8aa3-5a628c473cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ccdf32f-474f-4b9d-b223-7e755590717d",
                    "LayerId": "edbfbd99-4804-4269-a1dd-d940e62086cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "edbfbd99-4804-4269-a1dd-d940e62086cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3fd3645-bcd0-45a4-ad51-8346698444f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}