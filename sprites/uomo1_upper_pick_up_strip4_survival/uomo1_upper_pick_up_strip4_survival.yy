{
    "id": "29c3babe-806d-40d4-9545-58227151011e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_pick_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e72f8324-1875-4fc0-aa57-830b4cc442fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "compositeImage": {
                "id": "06b832ef-e991-40d6-959e-074e4ae2f2fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e72f8324-1875-4fc0-aa57-830b4cc442fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e716367-b8ff-49d9-adb5-05144726f7be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e72f8324-1875-4fc0-aa57-830b4cc442fd",
                    "LayerId": "9eff30cd-26b1-4e15-876b-f9f4d5237358"
                }
            ]
        },
        {
            "id": "0f827528-09e7-47a3-80b6-d1daaa90c4a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "compositeImage": {
                "id": "bcf49169-f9f7-4b86-a048-4ff330c4a0b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f827528-09e7-47a3-80b6-d1daaa90c4a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f563d6-cbcc-469b-a545-cdc3125893a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f827528-09e7-47a3-80b6-d1daaa90c4a0",
                    "LayerId": "9eff30cd-26b1-4e15-876b-f9f4d5237358"
                }
            ]
        },
        {
            "id": "3d56a8e8-9b28-4416-b95b-9f106e9b2634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "compositeImage": {
                "id": "33b0adc0-c936-477e-8712-1ea13e0d9a1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d56a8e8-9b28-4416-b95b-9f106e9b2634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8475039b-452a-4f3f-b034-affda88a2e78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d56a8e8-9b28-4416-b95b-9f106e9b2634",
                    "LayerId": "9eff30cd-26b1-4e15-876b-f9f4d5237358"
                }
            ]
        },
        {
            "id": "b46988db-96c2-460f-bcd0-be4a66425b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "compositeImage": {
                "id": "43897361-e203-44f1-8cea-76d0ef17b4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46988db-96c2-460f-bcd0-be4a66425b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75b32bf-1dcb-4d06-8f31-ce480aa33c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46988db-96c2-460f-bcd0-be4a66425b4c",
                    "LayerId": "9eff30cd-26b1-4e15-876b-f9f4d5237358"
                }
            ]
        },
        {
            "id": "53a520ca-ecc5-4847-b650-aed21c5d206d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "compositeImage": {
                "id": "1bdb0ee6-785d-470b-a0c1-094642d21f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53a520ca-ecc5-4847-b650-aed21c5d206d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bacd7692-ca88-45a5-8e51-c4afc140de8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53a520ca-ecc5-4847-b650-aed21c5d206d",
                    "LayerId": "9eff30cd-26b1-4e15-876b-f9f4d5237358"
                }
            ]
        },
        {
            "id": "ca149e48-c881-402a-8cfd-87c4e5117bee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "compositeImage": {
                "id": "53bbee2d-c63c-49bb-a0ab-de9bfacb8b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca149e48-c881-402a-8cfd-87c4e5117bee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfff34e3-5a64-4b77-8531-702490671f12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca149e48-c881-402a-8cfd-87c4e5117bee",
                    "LayerId": "9eff30cd-26b1-4e15-876b-f9f4d5237358"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9eff30cd-26b1-4e15-876b-f9f4d5237358",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29c3babe-806d-40d4-9545-58227151011e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}