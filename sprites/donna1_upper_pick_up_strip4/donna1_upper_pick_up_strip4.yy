{
    "id": "5989faa3-090d-4644-a129-32709199d652",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2217a5b4-64d2-4dbb-9d87-46353cf8da7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5989faa3-090d-4644-a129-32709199d652",
            "compositeImage": {
                "id": "37662e21-f46e-4051-9fb7-60a7f6f1998c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2217a5b4-64d2-4dbb-9d87-46353cf8da7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e3fc27e-e8c0-473e-ac1b-198f32526a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2217a5b4-64d2-4dbb-9d87-46353cf8da7e",
                    "LayerId": "54df254f-39c9-485b-a00b-8c43288103b8"
                }
            ]
        },
        {
            "id": "dda51c4c-285a-4825-975c-ca967e2d84cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5989faa3-090d-4644-a129-32709199d652",
            "compositeImage": {
                "id": "09165bbd-8210-457f-8775-b6ac74e8b3b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda51c4c-285a-4825-975c-ca967e2d84cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "212bb726-8be9-4a16-b161-034e79c03409",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda51c4c-285a-4825-975c-ca967e2d84cf",
                    "LayerId": "54df254f-39c9-485b-a00b-8c43288103b8"
                }
            ]
        },
        {
            "id": "979c286c-4f16-45ba-8c2a-1698c04c1898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5989faa3-090d-4644-a129-32709199d652",
            "compositeImage": {
                "id": "6e021880-7e77-4a5f-a81e-cc0765c792dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "979c286c-4f16-45ba-8c2a-1698c04c1898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d45d2a3e-4b96-4120-9f4d-d7274279d871",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "979c286c-4f16-45ba-8c2a-1698c04c1898",
                    "LayerId": "54df254f-39c9-485b-a00b-8c43288103b8"
                }
            ]
        },
        {
            "id": "b47ecfb8-99d4-49e7-bb2b-70c20dcbbaf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5989faa3-090d-4644-a129-32709199d652",
            "compositeImage": {
                "id": "1cd2f05c-2265-45e0-930a-2342eba65021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b47ecfb8-99d4-49e7-bb2b-70c20dcbbaf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26240c7a-783e-4d30-b441-cdc8a152b65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b47ecfb8-99d4-49e7-bb2b-70c20dcbbaf7",
                    "LayerId": "54df254f-39c9-485b-a00b-8c43288103b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54df254f-39c9-485b-a00b-8c43288103b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5989faa3-090d-4644-a129-32709199d652",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}