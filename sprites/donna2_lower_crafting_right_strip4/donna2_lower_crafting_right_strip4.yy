{
    "id": "d9a97b1b-533c-48ae-b925-c6c633d58984",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a8dc3089-7a12-41da-9271-0b387d6e91e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9a97b1b-533c-48ae-b925-c6c633d58984",
            "compositeImage": {
                "id": "9c8900ca-c427-4901-ae51-b07ea2e9c4fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8dc3089-7a12-41da-9271-0b387d6e91e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f742b9c3-e9da-4640-936c-37ad2ad9f674",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8dc3089-7a12-41da-9271-0b387d6e91e2",
                    "LayerId": "527a1197-2f09-4b34-aabc-42ae9ee0162e"
                }
            ]
        },
        {
            "id": "fff0aa74-0156-4d7a-9544-45cce838c6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9a97b1b-533c-48ae-b925-c6c633d58984",
            "compositeImage": {
                "id": "d69a1423-1c50-47a7-a3b0-c3f073ef45b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff0aa74-0156-4d7a-9544-45cce838c6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60e9d9e-a2aa-4c60-8eaa-4df86c1fd779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff0aa74-0156-4d7a-9544-45cce838c6e4",
                    "LayerId": "527a1197-2f09-4b34-aabc-42ae9ee0162e"
                }
            ]
        },
        {
            "id": "2b24ae2f-bf8c-4a55-80b4-f34e43feb242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9a97b1b-533c-48ae-b925-c6c633d58984",
            "compositeImage": {
                "id": "65fa1eae-b0e6-4008-8166-8ca51b23acd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b24ae2f-bf8c-4a55-80b4-f34e43feb242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43b5834c-606c-4f47-80cb-131ae4062e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b24ae2f-bf8c-4a55-80b4-f34e43feb242",
                    "LayerId": "527a1197-2f09-4b34-aabc-42ae9ee0162e"
                }
            ]
        },
        {
            "id": "6ff58835-79e0-4206-8f1c-c115ba38a39d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9a97b1b-533c-48ae-b925-c6c633d58984",
            "compositeImage": {
                "id": "718be67e-cb2b-4a3f-a1cf-c0618ebb3ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff58835-79e0-4206-8f1c-c115ba38a39d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5579c50f-2fd2-455a-bbf6-4307cf3e3f97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff58835-79e0-4206-8f1c-c115ba38a39d",
                    "LayerId": "527a1197-2f09-4b34-aabc-42ae9ee0162e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "527a1197-2f09-4b34-aabc-42ae9ee0162e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9a97b1b-533c-48ae-b925-c6c633d58984",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}