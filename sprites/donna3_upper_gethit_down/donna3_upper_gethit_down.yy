{
    "id": "599106fe-5802-42e4-9423-045f3b62468a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f478cf35-da50-41ac-a641-102adb6b8b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "599106fe-5802-42e4-9423-045f3b62468a",
            "compositeImage": {
                "id": "15f483c0-2263-403c-a26d-ffcaaa7fc51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f478cf35-da50-41ac-a641-102adb6b8b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02bc013b-fa72-4d50-801a-2dfe41d813ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f478cf35-da50-41ac-a641-102adb6b8b5f",
                    "LayerId": "890a3ae0-42d3-46ba-8498-f5657c7b768f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "890a3ae0-42d3-46ba-8498-f5657c7b768f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "599106fe-5802-42e4-9423-045f3b62468a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}