{
    "id": "00d8d2d8-4d79-4431-a60b-6d63e1af85dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_settings_btn_plus_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d53a1912-4e19-47c9-b106-fde9dffeee8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00d8d2d8-4d79-4431-a60b-6d63e1af85dc",
            "compositeImage": {
                "id": "7df01fca-3090-4c76-ba4f-6b489d05e1d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d53a1912-4e19-47c9-b106-fde9dffeee8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36f4f296-0819-4c08-b1eb-2e5e4eb9493d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d53a1912-4e19-47c9-b106-fde9dffeee8b",
                    "LayerId": "370c90a8-b624-4923-b6fd-32fd49e154b1"
                }
            ]
        },
        {
            "id": "65c15dbf-2ed9-4111-97bc-3adb3771890e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00d8d2d8-4d79-4431-a60b-6d63e1af85dc",
            "compositeImage": {
                "id": "dac77af0-af53-4c92-b424-46794cc34d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65c15dbf-2ed9-4111-97bc-3adb3771890e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645f1cdf-f358-446b-b452-173f31a90c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65c15dbf-2ed9-4111-97bc-3adb3771890e",
                    "LayerId": "370c90a8-b624-4923-b6fd-32fd49e154b1"
                }
            ]
        },
        {
            "id": "d991f983-2dd9-410e-bb77-202a4edf4d4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00d8d2d8-4d79-4431-a60b-6d63e1af85dc",
            "compositeImage": {
                "id": "d841e693-55c1-416a-bd3f-cbd950848b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d991f983-2dd9-410e-bb77-202a4edf4d4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2875f5b5-67cb-4fa3-8147-0e07a6184271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d991f983-2dd9-410e-bb77-202a4edf4d4a",
                    "LayerId": "370c90a8-b624-4923-b6fd-32fd49e154b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "370c90a8-b624-4923-b6fd-32fd49e154b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00d8d2d8-4d79-4431-a60b-6d63e1af85dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}