{
    "id": "2a24afe2-dd1a-4bce-8a94-09322720c6a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_hypericum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42dbf2b6-d5e5-4be7-bdcf-3f5688dc4621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a24afe2-dd1a-4bce-8a94-09322720c6a3",
            "compositeImage": {
                "id": "5ea1d862-b549-4c50-a69c-d220e9deda15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42dbf2b6-d5e5-4be7-bdcf-3f5688dc4621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "124919db-6110-4b45-be90-eb801a828e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42dbf2b6-d5e5-4be7-bdcf-3f5688dc4621",
                    "LayerId": "cfdf3270-67ca-4efb-9c1f-a54e7471c28f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cfdf3270-67ca-4efb-9c1f-a54e7471c28f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a24afe2-dd1a-4bce-8a94-09322720c6a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 13
}