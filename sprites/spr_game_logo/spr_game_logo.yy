{
    "id": "f3d04a22-c9fe-4ce3-af48-f17c663bdce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 91,
    "bbox_left": 9,
    "bbox_right": 214,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff43a004-c767-4138-bf20-4a8b6dea53cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3d04a22-c9fe-4ce3-af48-f17c663bdce8",
            "compositeImage": {
                "id": "e51e4597-7fe6-4676-8013-89ed175274f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff43a004-c767-4138-bf20-4a8b6dea53cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7430e0cf-a365-47fe-84e8-d87ea5a71f85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff43a004-c767-4138-bf20-4a8b6dea53cb",
                    "LayerId": "0729cadd-2d43-43ad-a0be-bae51576710e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "0729cadd-2d43-43ad-a0be-bae51576710e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3d04a22-c9fe-4ce3-af48-f17c663bdce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 224,
    "xorig": 112,
    "yorig": 48
}