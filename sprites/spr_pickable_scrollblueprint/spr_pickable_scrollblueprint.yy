{
    "id": "8da46bbb-b5de-4e81-b10e-bb23b3aa8e6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_scrollblueprint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03e63845-ee0b-4bfe-9462-a4fdf9db69aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8da46bbb-b5de-4e81-b10e-bb23b3aa8e6c",
            "compositeImage": {
                "id": "8c6566b1-a241-443a-a2b9-800263e1643f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e63845-ee0b-4bfe-9462-a4fdf9db69aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16a9b43-a47c-40c5-b02f-67b563ee158d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e63845-ee0b-4bfe-9462-a4fdf9db69aa",
                    "LayerId": "123d46a1-5db8-4fd7-8392-9bf32a58fcd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "123d46a1-5db8-4fd7-8392-9bf32a58fcd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8da46bbb-b5de-4e81-b10e-bb23b3aa8e6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}