{
    "id": "ed8edc33-b18e-4ab1-8eea-2507e5d03f1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_porcinimushroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a88c1927-6306-48ff-8030-2f83b1de1d4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed8edc33-b18e-4ab1-8eea-2507e5d03f1f",
            "compositeImage": {
                "id": "6bc9eeb4-bf66-46c0-b95a-5fef51c07bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a88c1927-6306-48ff-8030-2f83b1de1d4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee81ee0c-e99e-4bc8-845e-e35c67bba66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a88c1927-6306-48ff-8030-2f83b1de1d4b",
                    "LayerId": "add3c053-c017-49c8-8d29-89fc32170006"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "add3c053-c017-49c8-8d29-89fc32170006",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed8edc33-b18e-4ab1-8eea-2507e5d03f1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 14
}