{
    "id": "3062237f-2b10-4f38-bc04-1d658eb60d9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09bd3494-5a77-428e-883c-a510e6e281ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3062237f-2b10-4f38-bc04-1d658eb60d9b",
            "compositeImage": {
                "id": "a969c265-c603-48c8-966e-e6288c69ba22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09bd3494-5a77-428e-883c-a510e6e281ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8376c4ae-8799-444a-a67b-8dc8458edaac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09bd3494-5a77-428e-883c-a510e6e281ed",
                    "LayerId": "ef893160-c288-41f6-ab05-8ead8d21bad6"
                }
            ]
        },
        {
            "id": "b77e5ada-6163-4c9b-95bd-ad028e0ec7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3062237f-2b10-4f38-bc04-1d658eb60d9b",
            "compositeImage": {
                "id": "2da449e0-4a4a-499f-a305-27fd32e5957a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77e5ada-6163-4c9b-95bd-ad028e0ec7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "549376a9-424d-4e7e-9aa8-dfef0d3f5b1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77e5ada-6163-4c9b-95bd-ad028e0ec7c4",
                    "LayerId": "ef893160-c288-41f6-ab05-8ead8d21bad6"
                }
            ]
        },
        {
            "id": "ba1ca793-613d-4227-8af1-41627f52f4f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3062237f-2b10-4f38-bc04-1d658eb60d9b",
            "compositeImage": {
                "id": "570c5fe1-81c5-4edb-a6b2-a20769eee274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba1ca793-613d-4227-8af1-41627f52f4f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67c41a02-9225-4d5d-b5ff-88ebc661c93f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba1ca793-613d-4227-8af1-41627f52f4f8",
                    "LayerId": "ef893160-c288-41f6-ab05-8ead8d21bad6"
                }
            ]
        },
        {
            "id": "252b6a09-719b-448f-8a81-77fd3f159d95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3062237f-2b10-4f38-bc04-1d658eb60d9b",
            "compositeImage": {
                "id": "02f730ef-cf33-49c8-b75b-c3142862efbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252b6a09-719b-448f-8a81-77fd3f159d95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0225628-1d69-4314-9c5c-1ee7c253234c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252b6a09-719b-448f-8a81-77fd3f159d95",
                    "LayerId": "ef893160-c288-41f6-ab05-8ead8d21bad6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef893160-c288-41f6-ab05-8ead8d21bad6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3062237f-2b10-4f38-bc04-1d658eb60d9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}