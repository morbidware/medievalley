{
    "id": "08713f60-aa11-45dd-860e-879098a6af27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_carrot_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4936681c-5a50-41af-b762-7555531a23a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "compositeImage": {
                "id": "76b7d8b2-0f37-470c-8dd8-0da7cd3a63df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4936681c-5a50-41af-b762-7555531a23a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2497afa-8bc3-4ba6-b765-0e09ac143641",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4936681c-5a50-41af-b762-7555531a23a8",
                    "LayerId": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2"
                }
            ]
        },
        {
            "id": "7e6cfdbe-a447-4c74-8f33-35632e51f59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "compositeImage": {
                "id": "261d3a64-c499-477f-b006-10cddbb8b00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6cfdbe-a447-4c74-8f33-35632e51f59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7def7b3e-005b-4dc5-80ef-c66d40651679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6cfdbe-a447-4c74-8f33-35632e51f59d",
                    "LayerId": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2"
                }
            ]
        },
        {
            "id": "57b8202b-7ea1-428b-8878-f22557f9da37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "compositeImage": {
                "id": "bbefa4f4-3d4d-4d37-8a04-5c11d15dbdd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b8202b-7ea1-428b-8878-f22557f9da37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13cd9d3f-e5b1-4db6-b6ff-4456086bf0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b8202b-7ea1-428b-8878-f22557f9da37",
                    "LayerId": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2"
                }
            ]
        },
        {
            "id": "eb38d892-907a-425c-b09b-3b0713f4d72e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "compositeImage": {
                "id": "a6c146b0-a3b1-4b65-be04-1e7132f2e225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb38d892-907a-425c-b09b-3b0713f4d72e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06f5a60d-3ff6-4b29-98b7-db8227173f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb38d892-907a-425c-b09b-3b0713f4d72e",
                    "LayerId": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2"
                }
            ]
        },
        {
            "id": "1c64ab7b-f189-4760-8b25-23dfcb706665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "compositeImage": {
                "id": "c593d8e7-e4a7-470d-9f86-ad864cedf808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c64ab7b-f189-4760-8b25-23dfcb706665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cbe1aba-517c-436d-be39-93d0b4ca5d43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c64ab7b-f189-4760-8b25-23dfcb706665",
                    "LayerId": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2"
                }
            ]
        },
        {
            "id": "df601901-9e4d-433c-a47f-c80766bafaff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "compositeImage": {
                "id": "c16616ce-9ab8-4ca1-b61b-78b845da0d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df601901-9e4d-433c-a47f-c80766bafaff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aca27d9-3726-4c6f-9abb-e05299f570da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df601901-9e4d-433c-a47f-c80766bafaff",
                    "LayerId": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0b5d135e-38bf-4d0c-9fa4-84e88aa5cbf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "08713f60-aa11-45dd-860e-879098a6af27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 25
}