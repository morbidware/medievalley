{
    "id": "f5a00e1c-b105-4384-8811-22e7ab07d788",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_tallgrass_variation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a091c9e-b853-49c8-89df-12f1dc58d64a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5a00e1c-b105-4384-8811-22e7ab07d788",
            "compositeImage": {
                "id": "e6d114ef-e0bf-4c27-95da-ebf886de43b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a091c9e-b853-49c8-89df-12f1dc58d64a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbb95b1a-734a-4b06-af55-15bd6af058a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a091c9e-b853-49c8-89df-12f1dc58d64a",
                    "LayerId": "58c41ad8-8503-4a35-b9b5-5e4a5efef2cd"
                }
            ]
        },
        {
            "id": "02a90819-43f9-4cb0-89e3-258d7bbfa12a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5a00e1c-b105-4384-8811-22e7ab07d788",
            "compositeImage": {
                "id": "a1ec6550-4197-481c-8180-0d0bbc6a4014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02a90819-43f9-4cb0-89e3-258d7bbfa12a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe802a7-d473-4902-8f59-247e2745f048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02a90819-43f9-4cb0-89e3-258d7bbfa12a",
                    "LayerId": "58c41ad8-8503-4a35-b9b5-5e4a5efef2cd"
                }
            ]
        },
        {
            "id": "38ccfc77-67ed-4ad6-a409-e85d05d394fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5a00e1c-b105-4384-8811-22e7ab07d788",
            "compositeImage": {
                "id": "0c228ad6-71ca-4c10-89a7-b8257b72bc4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38ccfc77-67ed-4ad6-a409-e85d05d394fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c156cea-834e-459a-b83b-1284066e811a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38ccfc77-67ed-4ad6-a409-e85d05d394fd",
                    "LayerId": "58c41ad8-8503-4a35-b9b5-5e4a5efef2cd"
                }
            ]
        },
        {
            "id": "49a77956-ec44-4df1-ae92-4ed61b34d17f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5a00e1c-b105-4384-8811-22e7ab07d788",
            "compositeImage": {
                "id": "c094be72-b7c0-47a4-a4c2-77c182fadbe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a77956-ec44-4df1-ae92-4ed61b34d17f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31fc7b56-a369-49cb-b65a-c17c3042c00e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a77956-ec44-4df1-ae92-4ed61b34d17f",
                    "LayerId": "58c41ad8-8503-4a35-b9b5-5e4a5efef2cd"
                }
            ]
        },
        {
            "id": "ad9c9a14-0b4f-43b1-b14b-9b1cfb887d06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5a00e1c-b105-4384-8811-22e7ab07d788",
            "compositeImage": {
                "id": "8fb56f6b-24ed-4e97-81f0-e1f153b0235e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad9c9a14-0b4f-43b1-b14b-9b1cfb887d06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec592561-5407-440e-8366-28c8ba2fa3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad9c9a14-0b4f-43b1-b14b-9b1cfb887d06",
                    "LayerId": "58c41ad8-8503-4a35-b9b5-5e4a5efef2cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "58c41ad8-8503-4a35-b9b5-5e4a5efef2cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5a00e1c-b105-4384-8811-22e7ab07d788",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}