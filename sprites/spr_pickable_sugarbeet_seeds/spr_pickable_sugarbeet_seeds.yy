{
    "id": "403ecbeb-eed4-48fa-969b-1cd5134ca1f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_sugarbeet_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "340788be-56ff-47e1-88fb-c9faf97105d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "403ecbeb-eed4-48fa-969b-1cd5134ca1f5",
            "compositeImage": {
                "id": "8ef6dd22-0ffa-4c0e-bc47-bb9ad82eb7c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "340788be-56ff-47e1-88fb-c9faf97105d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "870655bf-a110-42f5-bd82-8921279366e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "340788be-56ff-47e1-88fb-c9faf97105d8",
                    "LayerId": "4fa2b042-bb15-4b03-8c4c-e8fe228d0e6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4fa2b042-bb15-4b03-8c4c-e8fe228d0e6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "403ecbeb-eed4-48fa-969b-1cd5134ca1f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}