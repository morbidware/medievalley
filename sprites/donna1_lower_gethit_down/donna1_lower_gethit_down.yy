{
    "id": "e9987f48-4c62-45f1-bee9-eb47db868ff9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9846ae93-ada5-413d-92fe-3a7df6be664f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9987f48-4c62-45f1-bee9-eb47db868ff9",
            "compositeImage": {
                "id": "1de29d63-0fe0-41d1-93a3-3d50a374d5d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9846ae93-ada5-413d-92fe-3a7df6be664f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c838ca5-6561-4aaa-85d5-0d00dc274690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9846ae93-ada5-413d-92fe-3a7df6be664f",
                    "LayerId": "8094ca0c-2ea6-427c-baa9-0454e6ad456a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8094ca0c-2ea6-427c-baa9-0454e6ad456a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9987f48-4c62-45f1-bee9-eb47db868ff9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}