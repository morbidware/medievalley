{
    "id": "c2931321-671c-4f46-a131-98be2c8db24f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b2f6ad1-f4aa-4257-8093-7b0c81a0ca55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2931321-671c-4f46-a131-98be2c8db24f",
            "compositeImage": {
                "id": "607c153c-9cd5-403b-b5fe-a55a2722212a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2f6ad1-f4aa-4257-8093-7b0c81a0ca55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16ef29e-c5e6-4e64-9205-c8831073f77a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2f6ad1-f4aa-4257-8093-7b0c81a0ca55",
                    "LayerId": "1bb5b2af-63ef-4f27-bd81-57639a5c4a53"
                }
            ]
        },
        {
            "id": "bc39f052-86b7-4ed4-8852-7d34f56deea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2931321-671c-4f46-a131-98be2c8db24f",
            "compositeImage": {
                "id": "1d5b3e28-8bac-4e5f-a811-f300663c517b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc39f052-86b7-4ed4-8852-7d34f56deea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34190ce0-494e-4c72-8807-686bdba56d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc39f052-86b7-4ed4-8852-7d34f56deea4",
                    "LayerId": "1bb5b2af-63ef-4f27-bd81-57639a5c4a53"
                }
            ]
        },
        {
            "id": "f146eb6a-4f3d-425b-a90b-69156b17a896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2931321-671c-4f46-a131-98be2c8db24f",
            "compositeImage": {
                "id": "6d72c11a-a2e5-4789-b5b8-22416e80be1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f146eb6a-4f3d-425b-a90b-69156b17a896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4728c8be-b7e9-41a3-a340-25adaa8249cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f146eb6a-4f3d-425b-a90b-69156b17a896",
                    "LayerId": "1bb5b2af-63ef-4f27-bd81-57639a5c4a53"
                }
            ]
        },
        {
            "id": "750872c4-3d02-49c6-9131-b812b779fb76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2931321-671c-4f46-a131-98be2c8db24f",
            "compositeImage": {
                "id": "f10af157-86f8-409b-a162-84821415c589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750872c4-3d02-49c6-9131-b812b779fb76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "777160a7-9b81-464c-9821-7c9453d2562a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750872c4-3d02-49c6-9131-b812b779fb76",
                    "LayerId": "1bb5b2af-63ef-4f27-bd81-57639a5c4a53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1bb5b2af-63ef-4f27-bd81-57639a5c4a53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2931321-671c-4f46-a131-98be2c8db24f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}