{
    "id": "5d449c6a-3857-4210-aa67-06f14ee5b261",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_tent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 2,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b359d1c-2a54-4f67-8467-0fffe6131599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d449c6a-3857-4210-aa67-06f14ee5b261",
            "compositeImage": {
                "id": "c5f7427d-a758-4b48-807e-abf436936fdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b359d1c-2a54-4f67-8467-0fffe6131599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1e2cf63-0be1-4922-9663-5e1ffe7d29d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b359d1c-2a54-4f67-8467-0fffe6131599",
                    "LayerId": "0b53d11e-c5d9-4bba-a47f-8c23c4bf9fe7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0b53d11e-c5d9-4bba-a47f-8c23c4bf9fe7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d449c6a-3857-4210-aa67-06f14ee5b261",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 56
}