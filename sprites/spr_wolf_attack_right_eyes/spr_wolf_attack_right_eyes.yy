{
    "id": "cfe7ffdb-2e18-4849-b906-aba5c1ccadda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_attack_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 38,
    "bbox_right": 41,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2dd2542-da86-438b-b90a-c3e73207d861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfe7ffdb-2e18-4849-b906-aba5c1ccadda",
            "compositeImage": {
                "id": "6836ea0f-4fc7-4155-aff5-b8d7537981f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2dd2542-da86-438b-b90a-c3e73207d861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d5b64e-09c2-4e65-aedd-3994a978cbb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2dd2542-da86-438b-b90a-c3e73207d861",
                    "LayerId": "19d82cc9-b267-408a-a887-7d3db43eb813"
                }
            ]
        },
        {
            "id": "4517ef28-2e95-483a-a162-ce40b95be5a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfe7ffdb-2e18-4849-b906-aba5c1ccadda",
            "compositeImage": {
                "id": "fc3524e9-d6b2-4d5c-9295-0b2f74b86e6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4517ef28-2e95-483a-a162-ce40b95be5a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f53f30-e576-485d-bde0-ab594ea1dd81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4517ef28-2e95-483a-a162-ce40b95be5a9",
                    "LayerId": "19d82cc9-b267-408a-a887-7d3db43eb813"
                }
            ]
        },
        {
            "id": "4b8104a0-3999-4b98-a6dd-6e352652fbe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfe7ffdb-2e18-4849-b906-aba5c1ccadda",
            "compositeImage": {
                "id": "2ad901eb-8691-4029-a8d7-407bf1da8d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b8104a0-3999-4b98-a6dd-6e352652fbe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a5d3a16-0f8c-42cd-b06f-19bb803ddaf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b8104a0-3999-4b98-a6dd-6e352652fbe0",
                    "LayerId": "19d82cc9-b267-408a-a887-7d3db43eb813"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "19d82cc9-b267-408a-a887-7d3db43eb813",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfe7ffdb-2e18-4849-b906-aba5c1ccadda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}