{
    "id": "42344c7a-fe24-4da5-b6e9-fc79fd33ae62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16014a96-ea9f-4111-bc9a-a07119e7ea48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42344c7a-fe24-4da5-b6e9-fc79fd33ae62",
            "compositeImage": {
                "id": "1adb23f4-8753-4d60-8e57-5111468a0b09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16014a96-ea9f-4111-bc9a-a07119e7ea48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9689bf1-768c-4874-8ec6-f15da7a7c240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16014a96-ea9f-4111-bc9a-a07119e7ea48",
                    "LayerId": "fc0f3e20-9c36-46ec-8640-6d0f6e6dd0e9"
                }
            ]
        },
        {
            "id": "60ca4e0a-872c-4535-aa51-cc934bb91333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42344c7a-fe24-4da5-b6e9-fc79fd33ae62",
            "compositeImage": {
                "id": "4695c9f1-1997-4f7e-86cb-6b4f3552278d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60ca4e0a-872c-4535-aa51-cc934bb91333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a81c9927-e20c-4942-86d9-8b95f6e02eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60ca4e0a-872c-4535-aa51-cc934bb91333",
                    "LayerId": "fc0f3e20-9c36-46ec-8640-6d0f6e6dd0e9"
                }
            ]
        },
        {
            "id": "6373842f-2267-4c3b-a754-4bcd977e84b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42344c7a-fe24-4da5-b6e9-fc79fd33ae62",
            "compositeImage": {
                "id": "64b7ca3f-c42b-46b8-9f25-0ec873035847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6373842f-2267-4c3b-a754-4bcd977e84b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5857d47-a881-4656-84c1-2933c601d555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6373842f-2267-4c3b-a754-4bcd977e84b9",
                    "LayerId": "fc0f3e20-9c36-46ec-8640-6d0f6e6dd0e9"
                }
            ]
        },
        {
            "id": "db4702f5-e6bf-4ff9-bb27-d6435858f3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42344c7a-fe24-4da5-b6e9-fc79fd33ae62",
            "compositeImage": {
                "id": "6f41a22f-9e04-44d4-ac64-b7762b02171c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db4702f5-e6bf-4ff9-bb27-d6435858f3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135dd1a9-651b-4c1c-ab82-9e6c5db12f7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db4702f5-e6bf-4ff9-bb27-d6435858f3f1",
                    "LayerId": "fc0f3e20-9c36-46ec-8640-6d0f6e6dd0e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc0f3e20-9c36-46ec-8640-6d0f6e6dd0e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42344c7a-fe24-4da5-b6e9-fc79fd33ae62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}