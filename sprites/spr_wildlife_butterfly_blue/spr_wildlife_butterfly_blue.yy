{
    "id": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wildlife_butterfly_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "453784c9-20c9-44a6-9949-4b97d27a69cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "63965a90-940c-45da-a840-60864a3c0317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453784c9-20c9-44a6-9949-4b97d27a69cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57685aca-9260-4d7a-b78f-d857d113b076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453784c9-20c9-44a6-9949-4b97d27a69cb",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "1e886a48-cc17-47b2-9058-fbbef6de64f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "05791c7d-b931-40ae-9911-b1d1a9039683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e886a48-cc17-47b2-9058-fbbef6de64f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6119cb65-36c9-4d46-ba20-016007c0d2ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e886a48-cc17-47b2-9058-fbbef6de64f5",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "4cfda4f8-638f-49e9-aaa9-c898dcdb38e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "414b1358-8049-4d87-a385-9513880d2a4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cfda4f8-638f-49e9-aaa9-c898dcdb38e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "385d9b4e-5ed0-434d-89c8-8ee12c29cbc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cfda4f8-638f-49e9-aaa9-c898dcdb38e7",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "72f5cab9-db87-487e-b747-bb9db0534d68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "b56068e6-be76-4999-a1d3-0999c111e547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f5cab9-db87-487e-b747-bb9db0534d68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a6edbc-9aea-44fe-9250-8ce433d06452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f5cab9-db87-487e-b747-bb9db0534d68",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "78d9a9e1-6866-4195-8e80-6822343eb1c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "9c593674-be06-4441-8016-feae99de98ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d9a9e1-6866-4195-8e80-6822343eb1c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a93a52-b611-42dd-a65c-57c975b18efd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d9a9e1-6866-4195-8e80-6822343eb1c3",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "31a8c21a-0137-42af-847a-6d0421d8dbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "b3a9bfad-7dee-4c41-9a75-292c7a14867e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a8c21a-0137-42af-847a-6d0421d8dbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7dba57-befa-48a0-8e55-cbfcac6ad118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a8c21a-0137-42af-847a-6d0421d8dbef",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "0e8276f9-28bf-4719-a099-a4dc2d808ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "4bd1564b-0357-4d3e-8579-35530c12ab7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e8276f9-28bf-4719-a099-a4dc2d808ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67fe1ff2-dd64-4466-80b4-abcfd94a4791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e8276f9-28bf-4719-a099-a4dc2d808ec5",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "2545be5e-8243-4170-a79d-1010491c65bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "26e15c44-aa4c-46d3-865c-95fdb12c02bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2545be5e-8243-4170-a79d-1010491c65bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1266a38-13ed-4c51-9fe3-546727bd2447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2545be5e-8243-4170-a79d-1010491c65bc",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "bf821e99-917e-4124-bf27-d64fc18c751a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "5f71cade-108b-4151-b41e-b24594bbeaa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf821e99-917e-4124-bf27-d64fc18c751a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b68a15dc-e09c-4843-b19e-17325ed41f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf821e99-917e-4124-bf27-d64fc18c751a",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "3fe7b69e-da8a-4e5f-bbee-ac340a0b3ceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "c1e599a6-37a0-4c28-a2f8-0e3c37dbe40d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fe7b69e-da8a-4e5f-bbee-ac340a0b3ceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87347587-b5f7-422d-90ea-5505ab0ecebc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fe7b69e-da8a-4e5f-bbee-ac340a0b3ceb",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "6e6c27dc-6427-4c65-93db-15f9c2401fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "496b61fb-a1b5-41e4-8a4b-39362c12701a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e6c27dc-6427-4c65-93db-15f9c2401fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56edc30-761a-4e38-967e-823045593dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e6c27dc-6427-4c65-93db-15f9c2401fc6",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        },
        {
            "id": "f00c9605-fc5a-40cf-a10d-1c0b4585d14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "compositeImage": {
                "id": "8ebf2481-38d7-437e-8050-c76954c6054b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f00c9605-fc5a-40cf-a10d-1c0b4585d14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d67c693d-f510-486b-8ee2-8c23ad3fb35d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f00c9605-fc5a-40cf-a10d-1c0b4585d14a",
                    "LayerId": "f06f985c-85b3-4242-aaa5-34eecc7e689b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f06f985c-85b3-4242-aaa5-34eecc7e689b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fa44a45-618f-4e64-82a6-ef86bd3e15a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}