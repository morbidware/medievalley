{
    "id": "a6e3d903-344f-49fe-b482-6f9e13120008",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_list_survival_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 321,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d52a476b-d447-431a-a5f6-30b73c636513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
            "compositeImage": {
                "id": "7c9d2143-7a62-4213-98e0-0f1b04e7fd5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d52a476b-d447-431a-a5f6-30b73c636513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d44f7307-811c-42b4-b7fc-438a2e33b07b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d52a476b-d447-431a-a5f6-30b73c636513",
                    "LayerId": "9f2507fa-cd99-46c7-a79b-e2dde371769c"
                }
            ]
        },
        {
            "id": "e8e93743-2936-4a7f-826e-762beac8becc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
            "compositeImage": {
                "id": "7ba63f46-c909-4f11-9b1f-9cdd6fd0dda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e93743-2936-4a7f-826e-762beac8becc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c5e53d-f1ca-46f1-bbcf-0f4caa0ecea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e93743-2936-4a7f-826e-762beac8becc",
                    "LayerId": "9f2507fa-cd99-46c7-a79b-e2dde371769c"
                }
            ]
        },
        {
            "id": "4092d322-00cb-479a-87d4-e6ad484f264f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
            "compositeImage": {
                "id": "fd33304c-df53-45d1-8e93-3a425205a2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4092d322-00cb-479a-87d4-e6ad484f264f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67b7a9ea-4732-41dc-8827-da15b7c4b0e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4092d322-00cb-479a-87d4-e6ad484f264f",
                    "LayerId": "9f2507fa-cd99-46c7-a79b-e2dde371769c"
                }
            ]
        },
        {
            "id": "85bf843a-230e-4200-8b20-5b9ebd52af77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
            "compositeImage": {
                "id": "be63a53b-eac9-47c3-ac03-dccf6154431c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bf843a-230e-4200-8b20-5b9ebd52af77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e876f3-486d-43a6-b8c9-af85ec329ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bf843a-230e-4200-8b20-5b9ebd52af77",
                    "LayerId": "9f2507fa-cd99-46c7-a79b-e2dde371769c"
                }
            ]
        },
        {
            "id": "3290940e-cb5b-4e4e-9a52-3ec68e958dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
            "compositeImage": {
                "id": "85cbe0f7-19cf-46b7-a6a9-2d5a15b4bbb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3290940e-cb5b-4e4e-9a52-3ec68e958dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450489c2-df47-4834-a8d1-0cecc1d48bc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3290940e-cb5b-4e4e-9a52-3ec68e958dd5",
                    "LayerId": "9f2507fa-cd99-46c7-a79b-e2dde371769c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 322,
    "layers": [
        {
            "id": "9f2507fa-cd99-46c7-a79b-e2dde371769c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6e3d903-344f-49fe-b482-6f9e13120008",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}