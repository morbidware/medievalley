{
    "id": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_walk_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a5970a7-2c3c-40ac-a6ac-72a3ff81cd00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "compositeImage": {
                "id": "72cc11c5-5a64-40d2-b49b-179066d056fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a5970a7-2c3c-40ac-a6ac-72a3ff81cd00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6914ba38-9048-4f60-ba0b-bbdaa187eeb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a5970a7-2c3c-40ac-a6ac-72a3ff81cd00",
                    "LayerId": "1f2e4e10-0f18-4919-b12a-5141dc02508b"
                }
            ]
        },
        {
            "id": "44b06c11-0b9d-468a-abb3-1fbf727d4f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "compositeImage": {
                "id": "7429cc5e-0be8-4c75-818b-98409c5e4ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b06c11-0b9d-468a-abb3-1fbf727d4f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4d78f3-c9d0-42c7-a9f4-9c91bf170497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b06c11-0b9d-468a-abb3-1fbf727d4f88",
                    "LayerId": "1f2e4e10-0f18-4919-b12a-5141dc02508b"
                }
            ]
        },
        {
            "id": "e049a7e2-8525-4801-ab93-984371fe3688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "compositeImage": {
                "id": "67e537f5-cec4-4d91-bea3-22eb1630a98c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e049a7e2-8525-4801-ab93-984371fe3688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "104ba772-a748-4446-849a-ffe886e46f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e049a7e2-8525-4801-ab93-984371fe3688",
                    "LayerId": "1f2e4e10-0f18-4919-b12a-5141dc02508b"
                }
            ]
        },
        {
            "id": "58fc211e-3f67-4558-a724-ebbbc1c6c722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "compositeImage": {
                "id": "502bf203-c4ed-4bd0-845c-2b5921972f46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58fc211e-3f67-4558-a724-ebbbc1c6c722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "345b65b3-f467-4b8f-bfde-05193bea4ff4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58fc211e-3f67-4558-a724-ebbbc1c6c722",
                    "LayerId": "1f2e4e10-0f18-4919-b12a-5141dc02508b"
                }
            ]
        },
        {
            "id": "8d9cd4cb-3580-4d69-9080-c25c623fd410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "compositeImage": {
                "id": "82cc8f2f-216b-4e09-9d83-f7755572b7c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9cd4cb-3580-4d69-9080-c25c623fd410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45595019-975d-48a7-afad-53f4ca0f75d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9cd4cb-3580-4d69-9080-c25c623fd410",
                    "LayerId": "1f2e4e10-0f18-4919-b12a-5141dc02508b"
                }
            ]
        },
        {
            "id": "83c8cd5f-84c2-4f29-a463-0869fb6d440a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "compositeImage": {
                "id": "15e378c7-6b70-47cc-9b50-9717fc140c28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c8cd5f-84c2-4f29-a463-0869fb6d440a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4775488-d7b2-4d95-94af-196dc65e604e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c8cd5f-84c2-4f29-a463-0869fb6d440a",
                    "LayerId": "1f2e4e10-0f18-4919-b12a-5141dc02508b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1f2e4e10-0f18-4919-b12a-5141dc02508b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e77ab28-8336-4b6f-8fc4-9bfa43dc024d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}