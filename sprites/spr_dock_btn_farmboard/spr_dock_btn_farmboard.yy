{
    "id": "ee75c45f-af6e-4932-a8ce-35ebd6915a48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_farmboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f01a787b-89ca-44fd-9024-48e79ae3bbc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee75c45f-af6e-4932-a8ce-35ebd6915a48",
            "compositeImage": {
                "id": "c4065300-7b45-4208-aa69-8f09a5a56ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f01a787b-89ca-44fd-9024-48e79ae3bbc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb8446de-0877-4270-af3f-7b3953865c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f01a787b-89ca-44fd-9024-48e79ae3bbc1",
                    "LayerId": "69da017e-1ff5-431d-abdc-f596ce091d82"
                }
            ]
        },
        {
            "id": "e06eecd5-cd2c-4005-bdd5-918c5e28c01b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee75c45f-af6e-4932-a8ce-35ebd6915a48",
            "compositeImage": {
                "id": "27b6b022-c581-44b1-bad7-b05c154634d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e06eecd5-cd2c-4005-bdd5-918c5e28c01b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8211eba2-1ee7-4ce0-b34b-600728b8cf58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e06eecd5-cd2c-4005-bdd5-918c5e28c01b",
                    "LayerId": "69da017e-1ff5-431d-abdc-f596ce091d82"
                }
            ]
        },
        {
            "id": "8cea7357-d59a-46de-9d9b-cc94de6a5b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee75c45f-af6e-4932-a8ce-35ebd6915a48",
            "compositeImage": {
                "id": "344f11c7-3c7a-49e9-b80d-dac36812fcd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cea7357-d59a-46de-9d9b-cc94de6a5b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d788f8-c34d-4646-aac4-c742d99fa86c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cea7357-d59a-46de-9d9b-cc94de6a5b5d",
                    "LayerId": "69da017e-1ff5-431d-abdc-f596ce091d82"
                }
            ]
        },
        {
            "id": "6009f764-82e6-4010-888e-4afe72436ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee75c45f-af6e-4932-a8ce-35ebd6915a48",
            "compositeImage": {
                "id": "7134c5c5-1868-430d-9d0d-e9971938c40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6009f764-82e6-4010-888e-4afe72436ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b5a479d-c7fe-4c39-9e87-5354858509f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6009f764-82e6-4010-888e-4afe72436ded",
                    "LayerId": "69da017e-1ff5-431d-abdc-f596ce091d82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "69da017e-1ff5-431d-abdc-f596ce091d82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee75c45f-af6e-4932-a8ce-35ebd6915a48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}