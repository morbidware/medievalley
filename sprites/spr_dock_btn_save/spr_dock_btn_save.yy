{
    "id": "e6f6f68c-698e-4c88-85eb-fa89d089e379",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_save",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f718405-1f57-4c6a-bdb1-4f0b4bb833a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6f6f68c-698e-4c88-85eb-fa89d089e379",
            "compositeImage": {
                "id": "b78ddd21-6a3b-4c8b-9968-ffda0f069b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f718405-1f57-4c6a-bdb1-4f0b4bb833a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c97805-b7be-4f71-8441-dfe0ba5e8a6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f718405-1f57-4c6a-bdb1-4f0b4bb833a9",
                    "LayerId": "bda99a8d-c051-48f4-bfe6-dd87fd997456"
                }
            ]
        },
        {
            "id": "08364e5d-4dc7-4387-859d-b007ebeb62e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6f6f68c-698e-4c88-85eb-fa89d089e379",
            "compositeImage": {
                "id": "98d20024-d20c-4f48-8d97-fde4f8d383fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08364e5d-4dc7-4387-859d-b007ebeb62e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ce003c-9cee-4834-aabf-c29630c1e4c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08364e5d-4dc7-4387-859d-b007ebeb62e8",
                    "LayerId": "bda99a8d-c051-48f4-bfe6-dd87fd997456"
                }
            ]
        },
        {
            "id": "a8e56319-a5d2-4565-9206-f6cc2c612bd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6f6f68c-698e-4c88-85eb-fa89d089e379",
            "compositeImage": {
                "id": "245d92c5-7d6e-4429-a832-ad453dc27334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e56319-a5d2-4565-9206-f6cc2c612bd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68bcf83c-5972-42ad-8334-a0feca402ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e56319-a5d2-4565-9206-f6cc2c612bd2",
                    "LayerId": "bda99a8d-c051-48f4-bfe6-dd87fd997456"
                }
            ]
        },
        {
            "id": "900baba2-95b1-4bbf-ba38-5757caee79d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6f6f68c-698e-4c88-85eb-fa89d089e379",
            "compositeImage": {
                "id": "847ea48c-e13e-47a0-a077-af1eeeb131cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "900baba2-95b1-4bbf-ba38-5757caee79d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c530fdc-23a3-4581-a6f2-96e406a4e5da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "900baba2-95b1-4bbf-ba38-5757caee79d2",
                    "LayerId": "bda99a8d-c051-48f4-bfe6-dd87fd997456"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "bda99a8d-c051-48f4-bfe6-dd87fd997456",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6f6f68c-698e-4c88-85eb-fa89d089e379",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}