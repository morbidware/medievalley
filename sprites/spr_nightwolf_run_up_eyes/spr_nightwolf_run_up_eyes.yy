{
    "id": "affaf27b-8e0e-4cde-b040-61b30ec147a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_run_up_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "715b1ef4-f261-4205-b5d1-9ba393cc0bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "affaf27b-8e0e-4cde-b040-61b30ec147a6",
            "compositeImage": {
                "id": "ecfb1b09-61f1-4929-8316-31ddecf43173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "715b1ef4-f261-4205-b5d1-9ba393cc0bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1197214e-d3d3-4449-bb16-6296a8de54f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "715b1ef4-f261-4205-b5d1-9ba393cc0bcc",
                    "LayerId": "792585fc-5fe8-4fd2-8f36-2bc81450d9f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "792585fc-5fe8-4fd2-8f36-2bc81450d9f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "affaf27b-8e0e-4cde-b040-61b30ec147a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}