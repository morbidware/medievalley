{
    "id": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_woods_bottom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d54270d3-5eb8-480a-8ddf-e7214b6ed1b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
            "compositeImage": {
                "id": "4795e874-d1bc-435a-acc9-ce3098714a1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d54270d3-5eb8-480a-8ddf-e7214b6ed1b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99c0a8e4-80cc-4464-830f-232ef75b2604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d54270d3-5eb8-480a-8ddf-e7214b6ed1b5",
                    "LayerId": "648147af-6624-48a6-9b01-9c01098c9194"
                }
            ]
        },
        {
            "id": "e3a20378-69e2-4385-b1d1-2bc2a25b9636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
            "compositeImage": {
                "id": "48938e9b-b1d0-44bd-932f-98156f163791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3a20378-69e2-4385-b1d1-2bc2a25b9636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c26769f-a94e-45f9-b99b-498a09e8451d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3a20378-69e2-4385-b1d1-2bc2a25b9636",
                    "LayerId": "648147af-6624-48a6-9b01-9c01098c9194"
                }
            ]
        },
        {
            "id": "1faf6156-9807-44a5-87c5-7fb18d431226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
            "compositeImage": {
                "id": "8388de95-5bad-48f6-b69b-0e1addf11073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1faf6156-9807-44a5-87c5-7fb18d431226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e51e35-3a50-45f6-b3d9-8bd0464a8c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1faf6156-9807-44a5-87c5-7fb18d431226",
                    "LayerId": "648147af-6624-48a6-9b01-9c01098c9194"
                }
            ]
        },
        {
            "id": "60a5cb89-49f8-4444-aa89-f6020fa5e1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
            "compositeImage": {
                "id": "8c5b5246-bb27-4f1a-a655-7b316875a271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a5cb89-49f8-4444-aa89-f6020fa5e1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dce9d2ef-3fa1-4834-832e-f56ffd57c5c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a5cb89-49f8-4444-aa89-f6020fa5e1c0",
                    "LayerId": "648147af-6624-48a6-9b01-9c01098c9194"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "648147af-6624-48a6-9b01-9c01098c9194",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6baf592-264a-41bd-bdfc-3ec6e36f9eb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}