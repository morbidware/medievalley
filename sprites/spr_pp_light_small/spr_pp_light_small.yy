{
    "id": "9ec7ca94-7108-498c-9cf3-836b0da1fc5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pp_light_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85d6caa9-f1a1-484d-9cb5-7d9343409b7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ec7ca94-7108-498c-9cf3-836b0da1fc5f",
            "compositeImage": {
                "id": "6863debf-8df6-4108-8e0f-0b5ec861bfb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d6caa9-f1a1-484d-9cb5-7d9343409b7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b568ef-b2a6-47f5-b8bb-c4a78aa6f385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d6caa9-f1a1-484d-9cb5-7d9343409b7d",
                    "LayerId": "1970f1bd-1b47-45ce-9c6a-a26659a0fc5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1970f1bd-1b47-45ce-9c6a-a26659a0fc5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ec7ca94-7108-498c-9cf3-836b0da1fc5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}