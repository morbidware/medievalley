{
    "id": "5bfd4916-8cf7-4cd7-aaa5-500fe9695073",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f4e58ad-5109-4e79-815b-f0a90d1a9f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bfd4916-8cf7-4cd7-aaa5-500fe9695073",
            "compositeImage": {
                "id": "d1bd87aa-0202-4909-b78a-ff7c1e51714e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f4e58ad-5109-4e79-815b-f0a90d1a9f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14ab08f8-a680-4902-8417-8214fc5f6225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f4e58ad-5109-4e79-815b-f0a90d1a9f8b",
                    "LayerId": "57bd43f7-0f4c-4d1e-8bca-97f8b6ed918f"
                }
            ]
        },
        {
            "id": "a5126c30-60a0-47f4-8aa1-0aaa57ac7b47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bfd4916-8cf7-4cd7-aaa5-500fe9695073",
            "compositeImage": {
                "id": "527fabe1-7f6f-4d75-997d-752fd8552a6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5126c30-60a0-47f4-8aa1-0aaa57ac7b47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83a5288-a1d8-46e0-9ad1-a66de350cb99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5126c30-60a0-47f4-8aa1-0aaa57ac7b47",
                    "LayerId": "57bd43f7-0f4c-4d1e-8bca-97f8b6ed918f"
                }
            ]
        },
        {
            "id": "b2ce27cd-b023-4573-9c56-ff0c8224e1c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bfd4916-8cf7-4cd7-aaa5-500fe9695073",
            "compositeImage": {
                "id": "3a756a76-9fd4-4833-b9db-69c7dc4a6a59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2ce27cd-b023-4573-9c56-ff0c8224e1c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcfee396-f547-43e0-a5ac-effc9f376c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2ce27cd-b023-4573-9c56-ff0c8224e1c6",
                    "LayerId": "57bd43f7-0f4c-4d1e-8bca-97f8b6ed918f"
                }
            ]
        },
        {
            "id": "28da0407-01d3-413d-a209-dca2f805d6fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bfd4916-8cf7-4cd7-aaa5-500fe9695073",
            "compositeImage": {
                "id": "d7e191e1-cda2-4f53-b3f1-322e12303f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28da0407-01d3-413d-a209-dca2f805d6fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27b2b74f-677b-41a6-bc1a-67942fd6ab7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28da0407-01d3-413d-a209-dca2f805d6fe",
                    "LayerId": "57bd43f7-0f4c-4d1e-8bca-97f8b6ed918f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "57bd43f7-0f4c-4d1e-8bca-97f8b6ed918f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bfd4916-8cf7-4cd7-aaa5-500fe9695073",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}