{
    "id": "416d57d8-0e43-49da-a8b0-48f47f513796",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_pumpkin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8af044d4-7ca1-4320-b6c9-e9e08946298f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "416d57d8-0e43-49da-a8b0-48f47f513796",
            "compositeImage": {
                "id": "041b724d-4e40-46d2-839f-1c8512e02913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8af044d4-7ca1-4320-b6c9-e9e08946298f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a290b1-8de0-4ee5-8608-2add529e86de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8af044d4-7ca1-4320-b6c9-e9e08946298f",
                    "LayerId": "afee0898-c6c4-459c-bea7-c2e5e337a671"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "afee0898-c6c4-459c-bea7-c2e5e337a671",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "416d57d8-0e43-49da-a8b0-48f47f513796",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}