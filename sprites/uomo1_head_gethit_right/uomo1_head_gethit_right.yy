{
    "id": "6a5cccdc-397e-493e-94ce-14d3d14329b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "846d32f4-3cdd-4a5c-9318-65c98161206a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a5cccdc-397e-493e-94ce-14d3d14329b0",
            "compositeImage": {
                "id": "59a51ae8-621d-440a-a409-ce400934eebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "846d32f4-3cdd-4a5c-9318-65c98161206a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f661ab6a-5052-4a34-964e-0edc5b7780d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "846d32f4-3cdd-4a5c-9318-65c98161206a",
                    "LayerId": "fec9f041-6186-4bdf-9c34-5cc019e73729"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fec9f041-6186-4bdf-9c34-5cc019e73729",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a5cccdc-397e-493e-94ce-14d3d14329b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}