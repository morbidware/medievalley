{
    "id": "a5978362-53f4-4e93-b15e-e0db2b93ed50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_ruins_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f53d2133-3db6-4f02-98f7-2de81fad1e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5978362-53f4-4e93-b15e-e0db2b93ed50",
            "compositeImage": {
                "id": "99c2e053-2bc6-498a-b24f-807f55b3f4eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f53d2133-3db6-4f02-98f7-2de81fad1e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2037cccd-cd85-4bab-b4aa-d6bcdded7ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f53d2133-3db6-4f02-98f7-2de81fad1e14",
                    "LayerId": "ffa35569-84b6-4e25-a704-33894f01027f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ffa35569-84b6-4e25-a704-33894f01027f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5978362-53f4-4e93-b15e-e0db2b93ed50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}