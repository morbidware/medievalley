{
    "id": "73572215-9360-4b59-a8bc-00aa94b3ce12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_hop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcf4f1cb-6ad7-467e-91bb-adcf56e770af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73572215-9360-4b59-a8bc-00aa94b3ce12",
            "compositeImage": {
                "id": "6d6367b1-b30e-4be9-b6eb-61aca55f2fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf4f1cb-6ad7-467e-91bb-adcf56e770af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "924b8ca6-3211-486c-b1eb-9d69c2a6bada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf4f1cb-6ad7-467e-91bb-adcf56e770af",
                    "LayerId": "afd63357-40ea-41a7-a74a-b1588a669e12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "afd63357-40ea-41a7-a74a-b1588a669e12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73572215-9360-4b59-a8bc-00aa94b3ce12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}