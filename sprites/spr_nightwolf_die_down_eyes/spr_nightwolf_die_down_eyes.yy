{
    "id": "25dee92b-a678-4409-ac45-fa9511a37a64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_die_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5b5652b-15f5-4980-a823-656d0b2a834e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25dee92b-a678-4409-ac45-fa9511a37a64",
            "compositeImage": {
                "id": "0375ffe9-6f71-4f68-a329-0a642bf1df1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b5652b-15f5-4980-a823-656d0b2a834e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "297a5dd9-b5b7-44c7-8c18-ee6f06b5ac59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b5652b-15f5-4980-a823-656d0b2a834e",
                    "LayerId": "63701ead-a6f6-4f66-9492-13162de8687f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "63701ead-a6f6-4f66-9492-13162de8687f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25dee92b-a678-4409-ac45-fa9511a37a64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}