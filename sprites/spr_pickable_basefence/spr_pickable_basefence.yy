{
    "id": "6735ae17-0c1c-490c-9795-21a15f77279d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_basefence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1663aaa8-5d22-47ad-8838-4ded42284f33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6735ae17-0c1c-490c-9795-21a15f77279d",
            "compositeImage": {
                "id": "4d8cc245-d858-41f1-b14e-79cf0bc93870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1663aaa8-5d22-47ad-8838-4ded42284f33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ed44c7-1ddd-4c1e-ac40-9bbc2f8468d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1663aaa8-5d22-47ad-8838-4ded42284f33",
                    "LayerId": "bb337dec-3dd3-410a-8d5d-adc52c69d810"
                }
            ]
        },
        {
            "id": "b5f7bd0c-1587-40b6-bad6-9b4a0f20ab03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6735ae17-0c1c-490c-9795-21a15f77279d",
            "compositeImage": {
                "id": "b80ceb45-bcb2-4615-8dda-58cc6ef25368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f7bd0c-1587-40b6-bad6-9b4a0f20ab03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e49c3360-0a80-4713-b86e-7edbe410e576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f7bd0c-1587-40b6-bad6-9b4a0f20ab03",
                    "LayerId": "bb337dec-3dd3-410a-8d5d-adc52c69d810"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bb337dec-3dd3-410a-8d5d-adc52c69d810",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6735ae17-0c1c-490c-9795-21a15f77279d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}