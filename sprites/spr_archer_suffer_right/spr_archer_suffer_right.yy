{
    "id": "eb3385d8-c2bb-43d5-b830-fdd83fee67aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_suffer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "45803fb0-5e69-4a48-b286-a3f919f401a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb3385d8-c2bb-43d5-b830-fdd83fee67aa",
            "compositeImage": {
                "id": "b6075ea7-ba3f-4fcd-b4a2-1eb38b1a0022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45803fb0-5e69-4a48-b286-a3f919f401a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd154b15-6100-4553-afc8-dfcff3ee95ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45803fb0-5e69-4a48-b286-a3f919f401a1",
                    "LayerId": "cb8107c7-ce94-44d6-908a-1f64f68162fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cb8107c7-ce94-44d6-908a-1f64f68162fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb3385d8-c2bb-43d5-b830-fdd83fee67aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}