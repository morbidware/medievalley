{
    "id": "7bbf04d6-8549-47f9-a477-b0f67d36d899",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_shovel_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02d7fe8b-5ad4-4c35-9567-e3fb285a9e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bbf04d6-8549-47f9-a477-b0f67d36d899",
            "compositeImage": {
                "id": "87744d55-adaf-468c-b129-d71c05344852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d7fe8b-5ad4-4c35-9567-e3fb285a9e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43da3d27-865c-484c-9f5c-0d3a92baec8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d7fe8b-5ad4-4c35-9567-e3fb285a9e5b",
                    "LayerId": "8178e089-529f-4e5d-a44e-3948738f13ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8178e089-529f-4e5d-a44e-3948738f13ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bbf04d6-8549-47f9-a477-b0f67d36d899",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 13
}