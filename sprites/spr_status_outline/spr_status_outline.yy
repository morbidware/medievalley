{
    "id": "dd7f0587-82ef-4284-b086-fd963c1e3729",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_status_outline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "521fb3dc-e980-4b75-8fdf-13baa5c07c55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd7f0587-82ef-4284-b086-fd963c1e3729",
            "compositeImage": {
                "id": "ef31083b-b248-41da-b366-3a477c803d34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "521fb3dc-e980-4b75-8fdf-13baa5c07c55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "639414d6-ae5b-457d-b355-e296447b50fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "521fb3dc-e980-4b75-8fdf-13baa5c07c55",
                    "LayerId": "01f0e848-6d99-4da0-bfcb-6f661750b0b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "01f0e848-6d99-4da0-bfcb-6f661750b0b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd7f0587-82ef-4284-b086-fd963c1e3729",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}