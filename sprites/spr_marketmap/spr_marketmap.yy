{
    "id": "33842bee-d6af-4dae-bd77-ce653445e408",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marketmap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa4088c6-aa2d-4ca5-a440-3e9d4a1716cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33842bee-d6af-4dae-bd77-ce653445e408",
            "compositeImage": {
                "id": "6247a07f-0231-4aeb-a97a-4bb4333d0207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa4088c6-aa2d-4ca5-a440-3e9d4a1716cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4cb70b-0b28-4729-8ad3-5370f892910e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa4088c6-aa2d-4ca5-a440-3e9d4a1716cb",
                    "LayerId": "5407575f-aad3-4708-b784-703f224caebb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5407575f-aad3-4708-b784-703f224caebb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33842bee-d6af-4dae-bd77-ce653445e408",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}