{
    "id": "9ad9c66f-ca39-43e4-bebe-5be32ff98c02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2747f54d-109c-4775-b9cf-16b989cf02e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ad9c66f-ca39-43e4-bebe-5be32ff98c02",
            "compositeImage": {
                "id": "a56281cf-cb7e-4380-a464-4166893edae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2747f54d-109c-4775-b9cf-16b989cf02e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99ada32-44ce-45e3-89ff-0b17ce046d1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2747f54d-109c-4775-b9cf-16b989cf02e1",
                    "LayerId": "a2719c6c-ce38-4e71-a391-a689e98c54d9"
                }
            ]
        },
        {
            "id": "8e8c4774-9d4e-4e9b-b798-6e5af208c32f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ad9c66f-ca39-43e4-bebe-5be32ff98c02",
            "compositeImage": {
                "id": "b4b0f335-e545-4ae9-b6b9-9a93a72ddfff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8c4774-9d4e-4e9b-b798-6e5af208c32f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffdf86ee-4a68-42bc-a698-409d815e233e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8c4774-9d4e-4e9b-b798-6e5af208c32f",
                    "LayerId": "a2719c6c-ce38-4e71-a391-a689e98c54d9"
                }
            ]
        },
        {
            "id": "a1953d72-e237-4b70-b8c1-9b2fce38f5f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ad9c66f-ca39-43e4-bebe-5be32ff98c02",
            "compositeImage": {
                "id": "f11c4c9b-2fa5-4100-9dee-e5e529114299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1953d72-e237-4b70-b8c1-9b2fce38f5f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b42883-d227-45c0-bb61-0407de35ee88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1953d72-e237-4b70-b8c1-9b2fce38f5f4",
                    "LayerId": "a2719c6c-ce38-4e71-a391-a689e98c54d9"
                }
            ]
        },
        {
            "id": "3dedf14b-0b22-4161-9f2f-6eaae67d205c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ad9c66f-ca39-43e4-bebe-5be32ff98c02",
            "compositeImage": {
                "id": "d9295a89-3dad-479a-89fe-aea3d1a6ef54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dedf14b-0b22-4161-9f2f-6eaae67d205c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581df948-a1ed-437b-bb58-55df3a0c70c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dedf14b-0b22-4161-9f2f-6eaae67d205c",
                    "LayerId": "a2719c6c-ce38-4e71-a391-a689e98c54d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2719c6c-ce38-4e71-a391-a689e98c54d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ad9c66f-ca39-43e4-bebe-5be32ff98c02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}