{
    "id": "aebff56e-045d-4e3b-988e-8d20ad89baa3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nav_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "671f0721-ab3a-4dee-88d1-e7cf76904f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aebff56e-045d-4e3b-988e-8d20ad89baa3",
            "compositeImage": {
                "id": "42cb6c46-9709-4307-b441-98c0a3f1263a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "671f0721-ab3a-4dee-88d1-e7cf76904f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4441504-5e6b-4b80-b3a7-c850736977da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "671f0721-ab3a-4dee-88d1-e7cf76904f5b",
                    "LayerId": "e72ffbe6-36ef-45fe-9015-ba46f8357ea5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e72ffbe6-36ef-45fe-9015-ba46f8357ea5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aebff56e-045d-4e3b-988e-8d20ad89baa3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}