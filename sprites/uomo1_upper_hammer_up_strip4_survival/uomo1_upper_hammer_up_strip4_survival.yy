{
    "id": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_hammer_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04b2e7d4-0e73-4661-841c-981edc17bfa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "compositeImage": {
                "id": "34061b71-b99d-4900-8d4d-92f8df98df05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b2e7d4-0e73-4661-841c-981edc17bfa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e38c878c-0970-4a13-8930-580bbb19c4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b2e7d4-0e73-4661-841c-981edc17bfa0",
                    "LayerId": "a507ff7a-5a45-4467-911d-941cf3740545"
                }
            ]
        },
        {
            "id": "9cd9636b-9274-45b6-b226-ae26c433cd92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "compositeImage": {
                "id": "aac1b45b-bc61-4b61-928b-cf3cd601d4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd9636b-9274-45b6-b226-ae26c433cd92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "007a77f7-e3e0-4c97-b642-d3a83d8c9c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd9636b-9274-45b6-b226-ae26c433cd92",
                    "LayerId": "a507ff7a-5a45-4467-911d-941cf3740545"
                }
            ]
        },
        {
            "id": "0a5cfed6-2c97-457e-8784-21030bcc4e04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "compositeImage": {
                "id": "b0798266-9bbf-49b6-ac21-d69be66f50dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a5cfed6-2c97-457e-8784-21030bcc4e04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea07b61a-f272-461d-8ed5-609c125a10cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5cfed6-2c97-457e-8784-21030bcc4e04",
                    "LayerId": "a507ff7a-5a45-4467-911d-941cf3740545"
                }
            ]
        },
        {
            "id": "a75a3a5f-eb7c-46f6-8e63-c1d2a183fc0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "compositeImage": {
                "id": "d7eff8a0-5a7a-4a4f-ac57-f0c1149c5d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a75a3a5f-eb7c-46f6-8e63-c1d2a183fc0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "277c9449-e48d-4348-8b62-c2a422539a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a75a3a5f-eb7c-46f6-8e63-c1d2a183fc0f",
                    "LayerId": "a507ff7a-5a45-4467-911d-941cf3740545"
                }
            ]
        },
        {
            "id": "24472f07-539d-4e5a-bda8-eab2f45065e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "compositeImage": {
                "id": "32e92161-b91c-4809-b2c8-f57234b643fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24472f07-539d-4e5a-bda8-eab2f45065e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f649b9ce-4dc6-458f-a690-1ead99b255c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24472f07-539d-4e5a-bda8-eab2f45065e6",
                    "LayerId": "a507ff7a-5a45-4467-911d-941cf3740545"
                }
            ]
        },
        {
            "id": "f37fada8-c49d-4870-b533-31f4e697bc9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "compositeImage": {
                "id": "d5e9421b-3b18-43fc-a797-e3be48fb5ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f37fada8-c49d-4870-b533-31f4e697bc9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4341a47-5f24-4473-8bc2-72cdff6db46e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f37fada8-c49d-4870-b533-31f4e697bc9e",
                    "LayerId": "a507ff7a-5a45-4467-911d-941cf3740545"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a507ff7a-5a45-4467-911d-941cf3740545",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "105b9224-8d7d-4ec3-89fe-89b7f54c7af1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}