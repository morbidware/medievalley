{
    "id": "2a2122d8-1d20-48c0-bb0b-ca6b892f118e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_lavender",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2a3d213-365c-4079-be20-c8ccdfdc6dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a2122d8-1d20-48c0-bb0b-ca6b892f118e",
            "compositeImage": {
                "id": "426e84e4-dbe4-43cd-a387-a38d3312a4c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a3d213-365c-4079-be20-c8ccdfdc6dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf50e91-b7a0-4535-a404-cfb979acf7dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a3d213-365c-4079-be20-c8ccdfdc6dee",
                    "LayerId": "237d83e9-98e9-4b4c-89b2-848f3257d76a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "237d83e9-98e9-4b4c-89b2-848f3257d76a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a2122d8-1d20-48c0-bb0b-ca6b892f118e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}