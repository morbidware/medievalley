{
    "id": "75b48f1e-7bbe-444b-8885-d68b674aee31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8bb58c11-891c-4989-b1d4-b8d838cc0d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b48f1e-7bbe-444b-8885-d68b674aee31",
            "compositeImage": {
                "id": "939a7e52-73cb-4c23-8fce-febc7efa9d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb58c11-891c-4989-b1d4-b8d838cc0d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6082fb3-be71-4439-a644-cbaa644b5c2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb58c11-891c-4989-b1d4-b8d838cc0d93",
                    "LayerId": "47f429d7-898d-4d9b-88a7-47cc8619219b"
                }
            ]
        },
        {
            "id": "7d844984-9f9f-45be-b19b-c36d18c8b83c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b48f1e-7bbe-444b-8885-d68b674aee31",
            "compositeImage": {
                "id": "19d78246-1271-4ce7-9bb5-0ecac57ba192",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d844984-9f9f-45be-b19b-c36d18c8b83c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "088acb1b-ff27-4079-a33c-635cc25b1f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d844984-9f9f-45be-b19b-c36d18c8b83c",
                    "LayerId": "47f429d7-898d-4d9b-88a7-47cc8619219b"
                }
            ]
        },
        {
            "id": "b3feefb2-f47c-4474-a1e8-27309ee34c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b48f1e-7bbe-444b-8885-d68b674aee31",
            "compositeImage": {
                "id": "761d5506-4681-4833-af30-7f30e2f36614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3feefb2-f47c-4474-a1e8-27309ee34c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e22e7c4-84ca-4bd5-a267-d5cf89a55cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3feefb2-f47c-4474-a1e8-27309ee34c72",
                    "LayerId": "47f429d7-898d-4d9b-88a7-47cc8619219b"
                }
            ]
        },
        {
            "id": "82185a42-84bc-4647-82bf-c3f0c0c3cb7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b48f1e-7bbe-444b-8885-d68b674aee31",
            "compositeImage": {
                "id": "ca2166ea-fbce-4ba3-a329-f0636313669c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82185a42-84bc-4647-82bf-c3f0c0c3cb7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff475ab-12c3-40a9-abc0-39bd28292ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82185a42-84bc-4647-82bf-c3f0c0c3cb7e",
                    "LayerId": "47f429d7-898d-4d9b-88a7-47cc8619219b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47f429d7-898d-4d9b-88a7-47cc8619219b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75b48f1e-7bbe-444b-8885-d68b674aee31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}