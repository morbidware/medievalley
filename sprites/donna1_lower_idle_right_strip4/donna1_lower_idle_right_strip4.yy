{
    "id": "ad8d3af1-8450-4da3-a037-b51ac0aedc06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "939589a6-3ec8-4bdb-96ee-d7694c7b8b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8d3af1-8450-4da3-a037-b51ac0aedc06",
            "compositeImage": {
                "id": "d39afaa4-7de0-4dba-a155-b5b555e0dd95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "939589a6-3ec8-4bdb-96ee-d7694c7b8b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "837ef524-f00d-4588-9362-9c689204419e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "939589a6-3ec8-4bdb-96ee-d7694c7b8b63",
                    "LayerId": "ccee0d66-1cff-45dd-8332-80530e1e573e"
                }
            ]
        },
        {
            "id": "7e246c14-ca67-4618-a0d5-b2567028c056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8d3af1-8450-4da3-a037-b51ac0aedc06",
            "compositeImage": {
                "id": "f86570db-ebe1-4a0d-b3bf-516a8cf3c859",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e246c14-ca67-4618-a0d5-b2567028c056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c1bd25a-b643-4bc2-b5ce-4e37a2a04d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e246c14-ca67-4618-a0d5-b2567028c056",
                    "LayerId": "ccee0d66-1cff-45dd-8332-80530e1e573e"
                }
            ]
        },
        {
            "id": "260700b2-6d62-4c08-9b01-b8ff7f7bfce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8d3af1-8450-4da3-a037-b51ac0aedc06",
            "compositeImage": {
                "id": "ca58dc66-5b3c-4c93-b92b-c4e91918fdc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "260700b2-6d62-4c08-9b01-b8ff7f7bfce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "353e4a20-c825-4387-8878-f6ef80335bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "260700b2-6d62-4c08-9b01-b8ff7f7bfce8",
                    "LayerId": "ccee0d66-1cff-45dd-8332-80530e1e573e"
                }
            ]
        },
        {
            "id": "88d88c40-7679-49f8-9163-9eb96866d451",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad8d3af1-8450-4da3-a037-b51ac0aedc06",
            "compositeImage": {
                "id": "9297379a-1e4a-4c2f-a970-441a5e948e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d88c40-7679-49f8-9163-9eb96866d451",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fe4c77f-efa8-4d28-b8b2-b0f04ef99665",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d88c40-7679-49f8-9163-9eb96866d451",
                    "LayerId": "ccee0d66-1cff-45dd-8332-80530e1e573e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ccee0d66-1cff-45dd-8332-80530e1e573e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad8d3af1-8450-4da3-a037-b51ac0aedc06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}