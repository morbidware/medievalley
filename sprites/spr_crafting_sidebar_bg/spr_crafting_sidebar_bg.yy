{
    "id": "da65ad71-52fc-483c-8544-12794da102cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_sidebar_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 321,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2364f0e-b751-4e66-905f-d505198f9e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da65ad71-52fc-483c-8544-12794da102cd",
            "compositeImage": {
                "id": "e4cb28d7-e87d-4812-b652-6ec9150d085b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2364f0e-b751-4e66-905f-d505198f9e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46be3571-7126-4453-8645-e48e40aab1f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2364f0e-b751-4e66-905f-d505198f9e5c",
                    "LayerId": "5308a8fb-2dfd-4cd6-8d3d-f90a81d24107"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 322,
    "layers": [
        {
            "id": "5308a8fb-2dfd-4cd6-8d3d-f90a81d24107",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da65ad71-52fc-483c-8544-12794da102cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}