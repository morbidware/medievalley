{
    "id": "910a451f-c133-4256-89fb-66174156de00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e629de3-30c9-41fa-9768-ec4cabf96b6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "910a451f-c133-4256-89fb-66174156de00",
            "compositeImage": {
                "id": "dd38352e-35b9-49dc-9cfa-dc45d5fc3b5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e629de3-30c9-41fa-9768-ec4cabf96b6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2833527-3769-4dbe-b295-43fc68fd3f45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e629de3-30c9-41fa-9768-ec4cabf96b6b",
                    "LayerId": "d7d9735e-b09d-4b31-9e38-945293d92ba7"
                }
            ]
        },
        {
            "id": "3769c805-051f-4b60-b8a2-6689487f3be7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "910a451f-c133-4256-89fb-66174156de00",
            "compositeImage": {
                "id": "cd32b85f-f890-4085-80d8-827133cea847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3769c805-051f-4b60-b8a2-6689487f3be7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "183f5504-8ce0-434c-8af8-0f5f4fdde346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3769c805-051f-4b60-b8a2-6689487f3be7",
                    "LayerId": "d7d9735e-b09d-4b31-9e38-945293d92ba7"
                }
            ]
        },
        {
            "id": "e6aad09d-bd2c-42de-bd67-20447f37a539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "910a451f-c133-4256-89fb-66174156de00",
            "compositeImage": {
                "id": "99e70273-7c89-45ac-bd45-7712fa5a6ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6aad09d-bd2c-42de-bd67-20447f37a539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f92434-1ce2-4f44-8742-83564fde8910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6aad09d-bd2c-42de-bd67-20447f37a539",
                    "LayerId": "d7d9735e-b09d-4b31-9e38-945293d92ba7"
                }
            ]
        },
        {
            "id": "25a05e36-2a6e-4642-a9fb-091eca030492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "910a451f-c133-4256-89fb-66174156de00",
            "compositeImage": {
                "id": "313b6848-ccb6-4781-a4db-abed30967b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a05e36-2a6e-4642-a9fb-091eca030492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e58b60ed-3995-473c-affa-c62c5fe33f33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a05e36-2a6e-4642-a9fb-091eca030492",
                    "LayerId": "d7d9735e-b09d-4b31-9e38-945293d92ba7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d7d9735e-b09d-4b31-9e38-945293d92ba7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "910a451f-c133-4256-89fb-66174156de00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}