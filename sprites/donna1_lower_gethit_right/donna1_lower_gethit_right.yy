{
    "id": "f1043f5d-5105-4f6c-838e-560ccb6f380d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "742a3935-7c88-4669-9751-ce3a801fd990",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1043f5d-5105-4f6c-838e-560ccb6f380d",
            "compositeImage": {
                "id": "0686f005-10eb-49a9-8c87-4a6fd857d430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742a3935-7c88-4669-9751-ce3a801fd990",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a44879-ba56-4e65-8966-a366c0b72da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742a3935-7c88-4669-9751-ce3a801fd990",
                    "LayerId": "f7f75e96-9398-46a4-850b-bef567bce73c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f7f75e96-9398-46a4-850b-bef567bce73c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1043f5d-5105-4f6c-838e-560ccb6f380d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}