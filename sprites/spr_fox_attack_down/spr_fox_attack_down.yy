{
    "id": "f4bcd850-8c24-4471-83d8-4fcb1ee2a5bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 17,
    "bbox_right": 30,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "269058b3-9678-496e-9384-4742c6f1b125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4bcd850-8c24-4471-83d8-4fcb1ee2a5bb",
            "compositeImage": {
                "id": "753fc221-71ec-4c45-ba81-c80204360ce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269058b3-9678-496e-9384-4742c6f1b125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53ce0213-b312-4edd-be9c-a050ab64b8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269058b3-9678-496e-9384-4742c6f1b125",
                    "LayerId": "4b0e8503-8d26-4cf8-a869-93accc0589e3"
                }
            ]
        },
        {
            "id": "70e480ff-4fda-4c3a-9b79-1fb5961a91e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4bcd850-8c24-4471-83d8-4fcb1ee2a5bb",
            "compositeImage": {
                "id": "57ef4cff-673c-4fb6-908b-1d837d195732",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e480ff-4fda-4c3a-9b79-1fb5961a91e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2efa2f7-1cf8-46f6-b525-a005c9c148c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e480ff-4fda-4c3a-9b79-1fb5961a91e3",
                    "LayerId": "4b0e8503-8d26-4cf8-a869-93accc0589e3"
                }
            ]
        },
        {
            "id": "1782256c-300f-4757-a9b0-30811fcf33d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4bcd850-8c24-4471-83d8-4fcb1ee2a5bb",
            "compositeImage": {
                "id": "8ddf66b8-82b4-4b01-a236-b66cfcda87e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1782256c-300f-4757-a9b0-30811fcf33d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72324d33-d1f6-43d0-874a-42dd9720b8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1782256c-300f-4757-a9b0-30811fcf33d6",
                    "LayerId": "4b0e8503-8d26-4cf8-a869-93accc0589e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4b0e8503-8d26-4cf8-a869-93accc0589e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4bcd850-8c24-4471-83d8-4fcb1ee2a5bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 22
}