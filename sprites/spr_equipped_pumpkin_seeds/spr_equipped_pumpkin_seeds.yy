{
    "id": "23a07dda-22e5-4214-8c8f-f9cf6f42e03a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_pumpkin_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01814a94-dd70-42cd-bbcf-4b7a547f5d28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23a07dda-22e5-4214-8c8f-f9cf6f42e03a",
            "compositeImage": {
                "id": "8a9f1d93-a7fd-48cc-a8d0-885f68423a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01814a94-dd70-42cd-bbcf-4b7a547f5d28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca047e06-e71a-424d-bab4-0751a129bc66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01814a94-dd70-42cd-bbcf-4b7a547f5d28",
                    "LayerId": "aba8c28a-7a51-4795-8ae8-958260f5c5ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "aba8c28a-7a51-4795-8ae8-958260f5c5ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23a07dda-22e5-4214-8c8f-f9cf6f42e03a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}