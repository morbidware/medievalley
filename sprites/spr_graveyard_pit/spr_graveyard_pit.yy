{
    "id": "fd393bc4-ccc5-4224-a6e0-8509fea470de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_pit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff3207db-43d4-4bd0-9849-f841f637c41c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd393bc4-ccc5-4224-a6e0-8509fea470de",
            "compositeImage": {
                "id": "e14040ba-2f10-48a7-b28b-6dad34aebcb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff3207db-43d4-4bd0-9849-f841f637c41c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e52d923b-7776-449f-9935-1612558a672d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff3207db-43d4-4bd0-9849-f841f637c41c",
                    "LayerId": "f45bc7ee-2c8a-486f-ba15-698ca6119c02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f45bc7ee-2c8a-486f-ba15-698ca6119c02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd393bc4-ccc5-4224-a6e0-8509fea470de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}