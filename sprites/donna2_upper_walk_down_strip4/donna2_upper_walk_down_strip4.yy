{
    "id": "daefc603-5310-4594-95db-254882c555fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56038be8-69ad-4416-8ff0-10b127e60f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daefc603-5310-4594-95db-254882c555fe",
            "compositeImage": {
                "id": "43e8d56f-9d30-4d86-9f99-0c7c0443bd20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56038be8-69ad-4416-8ff0-10b127e60f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567d98a1-52f9-405e-aed4-beef2e3a0c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56038be8-69ad-4416-8ff0-10b127e60f35",
                    "LayerId": "ac44bc21-38d9-4520-a870-2ff369f47a9c"
                }
            ]
        },
        {
            "id": "81e9fe70-5bc8-4c0d-99ac-8db430bdd8c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daefc603-5310-4594-95db-254882c555fe",
            "compositeImage": {
                "id": "5e60c0d7-ee7c-4b53-a8fc-90a8a3b35a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e9fe70-5bc8-4c0d-99ac-8db430bdd8c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2106c113-36a3-4053-b286-ce8708a4ba85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e9fe70-5bc8-4c0d-99ac-8db430bdd8c1",
                    "LayerId": "ac44bc21-38d9-4520-a870-2ff369f47a9c"
                }
            ]
        },
        {
            "id": "d31ab32f-3d5f-4ef9-9006-e614cc7c675f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daefc603-5310-4594-95db-254882c555fe",
            "compositeImage": {
                "id": "fbfbd979-bace-428f-8504-86146e6682a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d31ab32f-3d5f-4ef9-9006-e614cc7c675f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f68127d3-6881-44ab-939a-7fe8a2571a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d31ab32f-3d5f-4ef9-9006-e614cc7c675f",
                    "LayerId": "ac44bc21-38d9-4520-a870-2ff369f47a9c"
                }
            ]
        },
        {
            "id": "31da2aa2-55b5-4f94-bb20-e0c41a10e4d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daefc603-5310-4594-95db-254882c555fe",
            "compositeImage": {
                "id": "53592ce7-4bed-484e-9e10-257c254cfe3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31da2aa2-55b5-4f94-bb20-e0c41a10e4d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27d80e8-170e-4d8e-a018-d89c20850c41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31da2aa2-55b5-4f94-bb20-e0c41a10e4d8",
                    "LayerId": "ac44bc21-38d9-4520-a870-2ff369f47a9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac44bc21-38d9-4520-a870-2ff369f47a9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daefc603-5310-4594-95db-254882c555fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}