{
    "id": "808368e5-26f7-4945-a657-c3926cc58ab6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_strawberry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34070b5b-697b-40da-bb77-deb49a90fb2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "808368e5-26f7-4945-a657-c3926cc58ab6",
            "compositeImage": {
                "id": "ddeda6be-9847-4a59-a3bb-9bfffc894092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34070b5b-697b-40da-bb77-deb49a90fb2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a27ebc17-e843-4e03-8d8e-6021600a9db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34070b5b-697b-40da-bb77-deb49a90fb2f",
                    "LayerId": "abf90980-6740-49c4-be82-0b09626db042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "abf90980-6740-49c4-be82-0b09626db042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "808368e5-26f7-4945-a657-c3926cc58ab6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}