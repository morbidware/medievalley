{
    "id": "1eb3aa35-d22d-43fa-b989-618066d7850c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipe_quotation_mark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4601b2fa-6c6a-455e-a226-a6195733e9ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eb3aa35-d22d-43fa-b989-618066d7850c",
            "compositeImage": {
                "id": "9e86a7cf-55a5-471e-8808-c72ed7335568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4601b2fa-6c6a-455e-a226-a6195733e9ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70645cbd-aae6-4370-baa3-bcdca55cbd2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4601b2fa-6c6a-455e-a226-a6195733e9ce",
                    "LayerId": "2cc3524d-480d-4fa0-b86f-b32fb969472b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2cc3524d-480d-4fa0-b86f-b32fb969472b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1eb3aa35-d22d-43fa-b989-618066d7850c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}