{
    "id": "4c54c797-8e00-458a-a8a1-c7a1f7171ec7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_stone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82408a00-e4f1-4840-963b-89fea3a58569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c54c797-8e00-458a-a8a1-c7a1f7171ec7",
            "compositeImage": {
                "id": "2a283750-0148-4ee6-87b6-03ca3f7ed838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82408a00-e4f1-4840-963b-89fea3a58569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc03ac36-e10d-4c39-a3ef-9b1bc6d41121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82408a00-e4f1-4840-963b-89fea3a58569",
                    "LayerId": "8249e6a0-e0b7-4608-9b3d-c14029a98e12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8249e6a0-e0b7-4608-9b3d-c14029a98e12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c54c797-8e00-458a-a8a1-c7a1f7171ec7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}