{
    "id": "3f6a104c-a45e-4324-9485-ca36f230ed76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dd6534c-af1e-486a-ba65-5a8bc519d09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6a104c-a45e-4324-9485-ca36f230ed76",
            "compositeImage": {
                "id": "78c8d07e-d254-4758-84c2-a30c8a0ec5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dd6534c-af1e-486a-ba65-5a8bc519d09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "027ee141-cf94-4d13-94ff-053db49ea884",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dd6534c-af1e-486a-ba65-5a8bc519d09d",
                    "LayerId": "61b5b880-2f0a-4f2c-8686-850bb3f170fe"
                }
            ]
        },
        {
            "id": "9abbfe95-cecf-47fa-867d-c0b820cb33d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6a104c-a45e-4324-9485-ca36f230ed76",
            "compositeImage": {
                "id": "03619992-d250-4822-b359-fe1f6ea2eaa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9abbfe95-cecf-47fa-867d-c0b820cb33d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533efdf4-feee-458b-9f9f-92f68facee10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9abbfe95-cecf-47fa-867d-c0b820cb33d4",
                    "LayerId": "61b5b880-2f0a-4f2c-8686-850bb3f170fe"
                }
            ]
        },
        {
            "id": "e25b8755-4cec-4c47-8863-eb35acd68048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6a104c-a45e-4324-9485-ca36f230ed76",
            "compositeImage": {
                "id": "c9ef8d90-75d5-4aca-8eb6-139e57281d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25b8755-4cec-4c47-8863-eb35acd68048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9892c1-c4a0-4200-943f-b65db5ec4e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25b8755-4cec-4c47-8863-eb35acd68048",
                    "LayerId": "61b5b880-2f0a-4f2c-8686-850bb3f170fe"
                }
            ]
        },
        {
            "id": "1a5d523a-f6ff-4c27-8e2e-0067701429ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f6a104c-a45e-4324-9485-ca36f230ed76",
            "compositeImage": {
                "id": "9d0d4a9d-bb18-44ba-9c67-5960b97695ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a5d523a-f6ff-4c27-8e2e-0067701429ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af36ba12-140d-42bd-8fe5-29559e3b8879",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a5d523a-f6ff-4c27-8e2e-0067701429ab",
                    "LayerId": "61b5b880-2f0a-4f2c-8686-850bb3f170fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "61b5b880-2f0a-4f2c-8686-850bb3f170fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f6a104c-a45e-4324-9485-ca36f230ed76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}