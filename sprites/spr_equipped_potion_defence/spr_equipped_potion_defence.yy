{
    "id": "d43a69b4-3148-470d-986d-6f6a112fe9bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potion_defence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67247f0f-81c4-4428-873b-1fd1cb35f0f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d43a69b4-3148-470d-986d-6f6a112fe9bb",
            "compositeImage": {
                "id": "5405fe88-dcaa-4899-93ee-0a83c5cbd8ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67247f0f-81c4-4428-873b-1fd1cb35f0f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3f8f69c-f988-40e8-9ab8-9e27968aa372",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67247f0f-81c4-4428-873b-1fd1cb35f0f2",
                    "LayerId": "2c2bd95f-511a-4b71-a719-d7270c4ca188"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2c2bd95f-511a-4b71-a719-d7270c4ca188",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d43a69b4-3148-470d-986d-6f6a112fe9bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}