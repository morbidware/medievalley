{
    "id": "e3ccec01-a896-46fd-8231-389d6353d158",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebab6442-3f1c-4b41-bdde-29384000ece3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccec01-a896-46fd-8231-389d6353d158",
            "compositeImage": {
                "id": "6c613536-4c3a-498c-a499-27edd2373581",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebab6442-3f1c-4b41-bdde-29384000ece3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b790da8c-727e-458e-8792-a81f0c32efc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebab6442-3f1c-4b41-bdde-29384000ece3",
                    "LayerId": "56daac60-e2d6-4210-8073-437b1359c019"
                }
            ]
        },
        {
            "id": "9bbbbae7-df95-4635-ae9c-531be1d95e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccec01-a896-46fd-8231-389d6353d158",
            "compositeImage": {
                "id": "6d37da7a-85ac-4bd4-98fd-21d2fd5d80fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bbbbae7-df95-4635-ae9c-531be1d95e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f49cfc9a-86bb-492c-b33a-17bfec5056f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bbbbae7-df95-4635-ae9c-531be1d95e18",
                    "LayerId": "56daac60-e2d6-4210-8073-437b1359c019"
                }
            ]
        },
        {
            "id": "bd2813aa-583d-41a9-8522-8660eb488ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccec01-a896-46fd-8231-389d6353d158",
            "compositeImage": {
                "id": "5bd11aca-f62e-4830-96b5-de40ba4da841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd2813aa-583d-41a9-8522-8660eb488ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cef53fb-5cab-4625-93fe-660d3da52f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd2813aa-583d-41a9-8522-8660eb488ec9",
                    "LayerId": "56daac60-e2d6-4210-8073-437b1359c019"
                }
            ]
        },
        {
            "id": "f7b3260b-ceb1-494b-bf5f-0bd5c364fe75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3ccec01-a896-46fd-8231-389d6353d158",
            "compositeImage": {
                "id": "bc527d0d-77de-48ca-b810-b23a7254ff25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b3260b-ceb1-494b-bf5f-0bd5c364fe75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "054c0a0d-fdb2-4ca7-978d-174925ed4e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b3260b-ceb1-494b-bf5f-0bd5c364fe75",
                    "LayerId": "56daac60-e2d6-4210-8073-437b1359c019"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56daac60-e2d6-4210-8073-437b1359c019",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3ccec01-a896-46fd-8231-389d6353d158",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}