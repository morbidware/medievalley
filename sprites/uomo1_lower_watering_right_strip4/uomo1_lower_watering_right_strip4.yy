{
    "id": "3a7a336e-f9ee-4b10-8fd3-12ac9a3b9e22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17e11c0b-ac7a-4ae1-b18a-5bd08d3e5b1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7a336e-f9ee-4b10-8fd3-12ac9a3b9e22",
            "compositeImage": {
                "id": "160881f8-81f1-43ac-aec7-6a037edad440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e11c0b-ac7a-4ae1-b18a-5bd08d3e5b1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbcde064-2559-4f6f-b93e-11423bd1be26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e11c0b-ac7a-4ae1-b18a-5bd08d3e5b1c",
                    "LayerId": "47cc104e-7f95-46c8-a9f9-d268953a9de7"
                }
            ]
        },
        {
            "id": "5cbb912e-20fb-4c70-aba9-a07e6a5cec97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7a336e-f9ee-4b10-8fd3-12ac9a3b9e22",
            "compositeImage": {
                "id": "5e20babb-c93a-4cce-91da-10bda75c5b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cbb912e-20fb-4c70-aba9-a07e6a5cec97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4135387-3b00-40ca-b322-d5563dbb1683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cbb912e-20fb-4c70-aba9-a07e6a5cec97",
                    "LayerId": "47cc104e-7f95-46c8-a9f9-d268953a9de7"
                }
            ]
        },
        {
            "id": "cdb27da2-5b4e-4f63-bda9-b0daf8f5e589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7a336e-f9ee-4b10-8fd3-12ac9a3b9e22",
            "compositeImage": {
                "id": "bf00a052-e5b1-41d9-8647-f98f0e360f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdb27da2-5b4e-4f63-bda9-b0daf8f5e589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a7d5f8-b1bd-452d-86a0-e1b25536f0fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdb27da2-5b4e-4f63-bda9-b0daf8f5e589",
                    "LayerId": "47cc104e-7f95-46c8-a9f9-d268953a9de7"
                }
            ]
        },
        {
            "id": "a1e3c030-04d3-465d-8be4-0c8a4a058ebc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a7a336e-f9ee-4b10-8fd3-12ac9a3b9e22",
            "compositeImage": {
                "id": "2d012932-72d3-4c89-b135-4be93e73bb6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e3c030-04d3-465d-8be4-0c8a4a058ebc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8b6711-04d3-4cf7-9482-ad9761fba9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e3c030-04d3-465d-8be4-0c8a4a058ebc",
                    "LayerId": "47cc104e-7f95-46c8-a9f9-d268953a9de7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47cc104e-7f95-46c8-a9f9-d268953a9de7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a7a336e-f9ee-4b10-8fd3-12ac9a3b9e22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}