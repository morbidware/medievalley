{
    "id": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_audio_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cef43039-19c8-411c-b29f-5c44f563013f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
            "compositeImage": {
                "id": "d2b031b4-add0-432b-b53a-767eb507c086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cef43039-19c8-411c-b29f-5c44f563013f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbd0e237-6fb0-407f-a47d-216987273893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cef43039-19c8-411c-b29f-5c44f563013f",
                    "LayerId": "daa02717-efd7-43ff-a7e0-e440491de63b"
                },
                {
                    "id": "93d9652c-ba91-43d1-9daf-12fd2e07a000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cef43039-19c8-411c-b29f-5c44f563013f",
                    "LayerId": "06a6fa20-c622-4c9c-8fd7-211d94b9611e"
                }
            ]
        },
        {
            "id": "972fdb47-23ce-42fd-927a-5f803bdd7046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
            "compositeImage": {
                "id": "048d6ae5-bce2-42bc-a0f0-a57c40fc4fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972fdb47-23ce-42fd-927a-5f803bdd7046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a8e8cf7-d686-47cc-bcdf-8e9d321f9595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972fdb47-23ce-42fd-927a-5f803bdd7046",
                    "LayerId": "daa02717-efd7-43ff-a7e0-e440491de63b"
                },
                {
                    "id": "2a1dcdb4-4d4c-478d-b9d7-4cf114656daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972fdb47-23ce-42fd-927a-5f803bdd7046",
                    "LayerId": "06a6fa20-c622-4c9c-8fd7-211d94b9611e"
                }
            ]
        },
        {
            "id": "9d482739-e152-4fef-b263-fc91162562a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
            "compositeImage": {
                "id": "1696cfaf-7437-4386-8563-622a519cbbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d482739-e152-4fef-b263-fc91162562a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f65e20c1-b0e2-4e89-8479-e07ac41526b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d482739-e152-4fef-b263-fc91162562a5",
                    "LayerId": "daa02717-efd7-43ff-a7e0-e440491de63b"
                },
                {
                    "id": "908a9690-326e-435b-8076-e7c53a337ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d482739-e152-4fef-b263-fc91162562a5",
                    "LayerId": "06a6fa20-c622-4c9c-8fd7-211d94b9611e"
                }
            ]
        },
        {
            "id": "5a8bee7b-a354-4ae7-8a84-5aa31fa12ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
            "compositeImage": {
                "id": "2978e82c-ada0-47ff-b4a3-d3297c78cbea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a8bee7b-a354-4ae7-8a84-5aa31fa12ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237069a7-babd-4adb-ad3d-39ec4e49bbe6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a8bee7b-a354-4ae7-8a84-5aa31fa12ca3",
                    "LayerId": "daa02717-efd7-43ff-a7e0-e440491de63b"
                },
                {
                    "id": "52420d3f-7091-44ba-823f-68b523c71f1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a8bee7b-a354-4ae7-8a84-5aa31fa12ca3",
                    "LayerId": "06a6fa20-c622-4c9c-8fd7-211d94b9611e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "daa02717-efd7-43ff-a7e0-e440491de63b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "06a6fa20-c622-4c9c-8fd7-211d94b9611e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e8e67ae-e6b4-4f64-86ea-562c7e8b3c26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}