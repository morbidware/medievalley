{
    "id": "863e5eb8-d33f-4672-8ed0-4b8d87b1d5dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a987a0a-58ac-4a51-b4e9-5c8ff3fb1398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863e5eb8-d33f-4672-8ed0-4b8d87b1d5dc",
            "compositeImage": {
                "id": "e204d450-cd64-402a-b3de-6d0b383e6f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a987a0a-58ac-4a51-b4e9-5c8ff3fb1398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c8932c0-6f08-4d16-963c-2692b551ce83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a987a0a-58ac-4a51-b4e9-5c8ff3fb1398",
                    "LayerId": "cf49290a-4678-433e-9313-83f695ba7587"
                }
            ]
        },
        {
            "id": "0b1ca1b8-187f-4511-92af-d660b25a739c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863e5eb8-d33f-4672-8ed0-4b8d87b1d5dc",
            "compositeImage": {
                "id": "4bd90101-8978-4510-a093-518a27eea5b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b1ca1b8-187f-4511-92af-d660b25a739c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0db62ce1-956c-4d79-b86c-184f2aad4145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b1ca1b8-187f-4511-92af-d660b25a739c",
                    "LayerId": "cf49290a-4678-433e-9313-83f695ba7587"
                }
            ]
        },
        {
            "id": "90043068-604e-4d32-a3e9-72f6ab85e4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863e5eb8-d33f-4672-8ed0-4b8d87b1d5dc",
            "compositeImage": {
                "id": "e7f27ce0-f8eb-46f1-a7fb-9a186d9087d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90043068-604e-4d32-a3e9-72f6ab85e4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "322d5df2-4a60-4559-b048-a3cf00e76777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90043068-604e-4d32-a3e9-72f6ab85e4cc",
                    "LayerId": "cf49290a-4678-433e-9313-83f695ba7587"
                }
            ]
        },
        {
            "id": "e031e8dc-c4a4-4dd3-8549-2ca6f5369236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863e5eb8-d33f-4672-8ed0-4b8d87b1d5dc",
            "compositeImage": {
                "id": "117228b1-ead9-41af-8255-b6b1b142f207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e031e8dc-c4a4-4dd3-8549-2ca6f5369236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3029a10a-f303-4018-bc45-852508a9c516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e031e8dc-c4a4-4dd3-8549-2ca6f5369236",
                    "LayerId": "cf49290a-4678-433e-9313-83f695ba7587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cf49290a-4678-433e-9313-83f695ba7587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "863e5eb8-d33f-4672-8ed0-4b8d87b1d5dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}