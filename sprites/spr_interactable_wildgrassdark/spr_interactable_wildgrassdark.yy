{
    "id": "d29d5bb7-d55d-4317-a422-ba85be653d36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_wildgrassdark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d5042f6-f326-4785-8ce1-187fd0a85962",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d29d5bb7-d55d-4317-a422-ba85be653d36",
            "compositeImage": {
                "id": "81c33126-774d-4000-b748-3b18f6688b03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5042f6-f326-4785-8ce1-187fd0a85962",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfca1ea4-0820-470c-892a-de29230ceb7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5042f6-f326-4785-8ce1-187fd0a85962",
                    "LayerId": "311b95d6-a0a4-4fc2-aef6-fc3f51e30ea5"
                }
            ]
        },
        {
            "id": "36f4cc2a-7d99-4cdd-9859-6a6d7982a76c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d29d5bb7-d55d-4317-a422-ba85be653d36",
            "compositeImage": {
                "id": "9c25f0f3-9017-4bec-a754-fe271c3152f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f4cc2a-7d99-4cdd-9859-6a6d7982a76c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfa84e6-bd0d-4181-a943-dd8935927a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f4cc2a-7d99-4cdd-9859-6a6d7982a76c",
                    "LayerId": "311b95d6-a0a4-4fc2-aef6-fc3f51e30ea5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "311b95d6-a0a4-4fc2-aef6-fc3f51e30ea5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d29d5bb7-d55d-4317-a422-ba85be653d36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}