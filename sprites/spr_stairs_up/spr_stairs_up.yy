{
    "id": "dfc59b85-de54-49b9-834b-134bb56a9d3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stairs_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdfc851d-5b4a-49c1-a36c-616657265047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc59b85-de54-49b9-834b-134bb56a9d3b",
            "compositeImage": {
                "id": "dc10adf9-ad06-4b6f-8fc2-68dfef4356c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdfc851d-5b4a-49c1-a36c-616657265047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501f0c12-cbba-459f-bc77-dc3ae01e7685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdfc851d-5b4a-49c1-a36c-616657265047",
                    "LayerId": "e71a8fa7-11cc-4bbc-abfa-178bd3283ce8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e71a8fa7-11cc-4bbc-abfa-178bd3283ce8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfc59b85-de54-49b9-834b-134bb56a9d3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 8,
    "yorig": 8
}