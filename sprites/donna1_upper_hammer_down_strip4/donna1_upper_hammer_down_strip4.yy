{
    "id": "f12684d4-e453-4281-a85f-0bc192ea9ca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76ad7ca9-3e54-41bb-8102-8dc7626a032f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f12684d4-e453-4281-a85f-0bc192ea9ca3",
            "compositeImage": {
                "id": "f8ae4bfb-5020-4c19-b101-1b52603ddd22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ad7ca9-3e54-41bb-8102-8dc7626a032f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fae738a-7022-4ea0-8426-041b2629da36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ad7ca9-3e54-41bb-8102-8dc7626a032f",
                    "LayerId": "9588b80d-9aa4-49f5-a245-02da7ccea9d5"
                }
            ]
        },
        {
            "id": "b77899bc-7a82-468e-bffb-de72dbc691f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f12684d4-e453-4281-a85f-0bc192ea9ca3",
            "compositeImage": {
                "id": "6cb4b0dd-0ca7-47e4-a5b8-9307951bab55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77899bc-7a82-468e-bffb-de72dbc691f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2847bbad-39c7-4f44-87f1-ee7609319776",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77899bc-7a82-468e-bffb-de72dbc691f4",
                    "LayerId": "9588b80d-9aa4-49f5-a245-02da7ccea9d5"
                }
            ]
        },
        {
            "id": "9c4213fb-4a79-478d-9248-6e0e5437ddb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f12684d4-e453-4281-a85f-0bc192ea9ca3",
            "compositeImage": {
                "id": "9bf61a6e-31ad-45d6-a649-0183884cfaa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c4213fb-4a79-478d-9248-6e0e5437ddb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ece7bdf-be55-4858-8751-09c2111b078d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c4213fb-4a79-478d-9248-6e0e5437ddb5",
                    "LayerId": "9588b80d-9aa4-49f5-a245-02da7ccea9d5"
                }
            ]
        },
        {
            "id": "60e24bb7-cf36-4b96-8b4b-7854fd329cac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f12684d4-e453-4281-a85f-0bc192ea9ca3",
            "compositeImage": {
                "id": "953db2a5-6f43-48f8-a239-06f2679b7cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60e24bb7-cf36-4b96-8b4b-7854fd329cac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce625b50-7fb9-4f50-b7d7-33bb0757c29e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60e24bb7-cf36-4b96-8b4b-7854fd329cac",
                    "LayerId": "9588b80d-9aa4-49f5-a245-02da7ccea9d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9588b80d-9aa4-49f5-a245-02da7ccea9d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f12684d4-e453-4281-a85f-0bc192ea9ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}