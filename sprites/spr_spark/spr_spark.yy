{
    "id": "5f2a19cb-cf45-423d-8b1a-8e5e5145766c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aee98cd5-37ad-4b98-95a5-180a7e2b7cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f2a19cb-cf45-423d-8b1a-8e5e5145766c",
            "compositeImage": {
                "id": "fba26284-2e60-4af8-a131-4951f03fc0d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aee98cd5-37ad-4b98-95a5-180a7e2b7cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed582134-eee0-455d-9041-a2ddabb505c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aee98cd5-37ad-4b98-95a5-180a7e2b7cbe",
                    "LayerId": "3cfa4117-05b9-42f8-98ee-e03b8654ccb7"
                }
            ]
        },
        {
            "id": "9e30aa71-b10e-4aa0-a3ab-78d144409b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f2a19cb-cf45-423d-8b1a-8e5e5145766c",
            "compositeImage": {
                "id": "cb07b265-b383-477e-b2c0-42c66e9175df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e30aa71-b10e-4aa0-a3ab-78d144409b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5901bf36-8ba2-4b9e-bace-aa047cc508f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e30aa71-b10e-4aa0-a3ab-78d144409b83",
                    "LayerId": "3cfa4117-05b9-42f8-98ee-e03b8654ccb7"
                }
            ]
        },
        {
            "id": "f54f4eba-e17e-4242-b522-b39b1b80b87b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f2a19cb-cf45-423d-8b1a-8e5e5145766c",
            "compositeImage": {
                "id": "8143067d-a280-469e-bf54-0eb5a64212e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54f4eba-e17e-4242-b522-b39b1b80b87b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0dc2567-2877-4748-8df1-fdf4ab93df6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54f4eba-e17e-4242-b522-b39b1b80b87b",
                    "LayerId": "3cfa4117-05b9-42f8-98ee-e03b8654ccb7"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 1,
    "layers": [
        {
            "id": "3cfa4117-05b9-42f8-98ee-e03b8654ccb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f2a19cb-cf45-423d-8b1a-8e5e5145766c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}