{
    "id": "f69e2095-b067-4b02-bdac-c0c347b7aefd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a266cc7d-a3ce-46bb-bc73-5a785a104859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f69e2095-b067-4b02-bdac-c0c347b7aefd",
            "compositeImage": {
                "id": "47a2b120-6051-4d6b-92b3-b46f41d0a99c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a266cc7d-a3ce-46bb-bc73-5a785a104859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38665e27-134d-4d57-9589-d3e88a9ca984",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a266cc7d-a3ce-46bb-bc73-5a785a104859",
                    "LayerId": "36500e11-6885-4c0d-b78e-94cb2a9d565c"
                }
            ]
        },
        {
            "id": "4e2c1d2a-efb9-4a9a-ac33-22950f162115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f69e2095-b067-4b02-bdac-c0c347b7aefd",
            "compositeImage": {
                "id": "c605d1ba-9fff-4ab8-b3c3-c7ed65c5f93e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2c1d2a-efb9-4a9a-ac33-22950f162115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b1be3cc-09bd-49eb-bda4-0bf53f81f600",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2c1d2a-efb9-4a9a-ac33-22950f162115",
                    "LayerId": "36500e11-6885-4c0d-b78e-94cb2a9d565c"
                }
            ]
        },
        {
            "id": "9045b978-8c7b-47d6-b278-c87cefbca7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f69e2095-b067-4b02-bdac-c0c347b7aefd",
            "compositeImage": {
                "id": "7408ec49-e586-4413-a2ab-946d344bd6a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9045b978-8c7b-47d6-b278-c87cefbca7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b626f0-3f9d-498a-8507-de73761b47b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9045b978-8c7b-47d6-b278-c87cefbca7ff",
                    "LayerId": "36500e11-6885-4c0d-b78e-94cb2a9d565c"
                }
            ]
        },
        {
            "id": "3dae0333-6645-4284-a71d-2b499bb3ef50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f69e2095-b067-4b02-bdac-c0c347b7aefd",
            "compositeImage": {
                "id": "4e06a43d-73e6-4264-b92a-5de98616d8e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dae0333-6645-4284-a71d-2b499bb3ef50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8399d0af-6d55-4cd5-b10c-a09327e5346b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dae0333-6645-4284-a71d-2b499bb3ef50",
                    "LayerId": "36500e11-6885-4c0d-b78e-94cb2a9d565c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "36500e11-6885-4c0d-b78e-94cb2a9d565c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f69e2095-b067-4b02-bdac-c0c347b7aefd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}