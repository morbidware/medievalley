{
    "id": "142e2cb9-58f7-4685-82a5-f8a226c80e5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4755f1a1-68db-4365-872d-70353e0e0abe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142e2cb9-58f7-4685-82a5-f8a226c80e5a",
            "compositeImage": {
                "id": "f4f4c551-4a80-41fa-94b4-b62aa573fbad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4755f1a1-68db-4365-872d-70353e0e0abe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88ab5d4b-45c4-4eb4-a588-aab99c0b62b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4755f1a1-68db-4365-872d-70353e0e0abe",
                    "LayerId": "2f9de773-d658-4847-89e7-b9d28c002880"
                }
            ]
        },
        {
            "id": "6c3ca760-0b37-4427-b15d-4181a9a09f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142e2cb9-58f7-4685-82a5-f8a226c80e5a",
            "compositeImage": {
                "id": "cd341967-9b7c-490d-be4d-90c4e3dd3da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3ca760-0b37-4427-b15d-4181a9a09f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767c75c0-b17f-4ab1-952c-31c4101af2ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3ca760-0b37-4427-b15d-4181a9a09f42",
                    "LayerId": "2f9de773-d658-4847-89e7-b9d28c002880"
                }
            ]
        },
        {
            "id": "42077277-6ca1-41fa-acb0-0f0b3807b0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142e2cb9-58f7-4685-82a5-f8a226c80e5a",
            "compositeImage": {
                "id": "5ca5898e-756c-44bb-b9c6-19f4ef15533f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42077277-6ca1-41fa-acb0-0f0b3807b0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc673254-cf8f-4e01-bdda-b97ff186cf57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42077277-6ca1-41fa-acb0-0f0b3807b0e6",
                    "LayerId": "2f9de773-d658-4847-89e7-b9d28c002880"
                }
            ]
        },
        {
            "id": "c8d69489-4dcd-43e1-a8c8-82e554a3e90e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "142e2cb9-58f7-4685-82a5-f8a226c80e5a",
            "compositeImage": {
                "id": "577a83c8-7202-4756-9184-ec147ce19421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8d69489-4dcd-43e1-a8c8-82e554a3e90e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c4cbfb-aa98-41b5-8e2a-e64938d07ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8d69489-4dcd-43e1-a8c8-82e554a3e90e",
                    "LayerId": "2f9de773-d658-4847-89e7-b9d28c002880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f9de773-d658-4847-89e7-b9d28c002880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "142e2cb9-58f7-4685-82a5-f8a226c80e5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}