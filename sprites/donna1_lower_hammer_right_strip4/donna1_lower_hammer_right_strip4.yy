{
    "id": "53f7828d-2577-4751-b9d1-6f7279627843",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2825d0ab-c85b-476b-b8e3-54dedb351813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f7828d-2577-4751-b9d1-6f7279627843",
            "compositeImage": {
                "id": "aab228b0-399e-4933-9c6f-579e9bbb374f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2825d0ab-c85b-476b-b8e3-54dedb351813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90af62e4-0dca-41e9-86c8-9e9f28a58ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2825d0ab-c85b-476b-b8e3-54dedb351813",
                    "LayerId": "6b808d22-5981-4aaf-bf8b-9c63c97c15a5"
                }
            ]
        },
        {
            "id": "ccd9782e-f109-4ed8-b076-f6c1afd323bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f7828d-2577-4751-b9d1-6f7279627843",
            "compositeImage": {
                "id": "ff4882b7-c795-4235-b161-884d05101478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd9782e-f109-4ed8-b076-f6c1afd323bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a2f6e25-31f0-442c-8d37-61d7f8416a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd9782e-f109-4ed8-b076-f6c1afd323bf",
                    "LayerId": "6b808d22-5981-4aaf-bf8b-9c63c97c15a5"
                }
            ]
        },
        {
            "id": "0dc6a5be-f7ba-48e6-9db8-2572e8fa95ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f7828d-2577-4751-b9d1-6f7279627843",
            "compositeImage": {
                "id": "852a01c6-2d81-4bbe-b854-18bb08ed9d7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc6a5be-f7ba-48e6-9db8-2572e8fa95ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394992fa-ec7b-47fa-8c6f-3cb2951e22b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc6a5be-f7ba-48e6-9db8-2572e8fa95ab",
                    "LayerId": "6b808d22-5981-4aaf-bf8b-9c63c97c15a5"
                }
            ]
        },
        {
            "id": "1c3fc2ed-cbb8-4f44-8a06-24ce5a1619aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53f7828d-2577-4751-b9d1-6f7279627843",
            "compositeImage": {
                "id": "b3b58b68-02ae-4f08-9a9a-f730db67a99c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3fc2ed-cbb8-4f44-8a06-24ce5a1619aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d00d5f-dc08-4218-9639-9b03f6d7c53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3fc2ed-cbb8-4f44-8a06-24ce5a1619aa",
                    "LayerId": "6b808d22-5981-4aaf-bf8b-9c63c97c15a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b808d22-5981-4aaf-bf8b-9c63c97c15a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53f7828d-2577-4751-b9d1-6f7279627843",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}