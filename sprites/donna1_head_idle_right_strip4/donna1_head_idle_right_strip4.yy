{
    "id": "6cbf6d1b-fb3d-4f65-bee0-4eee6a4f6db0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0020dbe-7a62-4dc1-b2b8-547544fa7622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cbf6d1b-fb3d-4f65-bee0-4eee6a4f6db0",
            "compositeImage": {
                "id": "45316727-7750-4f8b-b272-21164483e622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0020dbe-7a62-4dc1-b2b8-547544fa7622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61261b0a-d56f-4c90-a53e-7533cba9cbed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0020dbe-7a62-4dc1-b2b8-547544fa7622",
                    "LayerId": "39d15b82-0177-46d8-b19c-7746d24a881f"
                }
            ]
        },
        {
            "id": "33f00ba3-4a6c-48c5-87b8-2152c4a75460",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cbf6d1b-fb3d-4f65-bee0-4eee6a4f6db0",
            "compositeImage": {
                "id": "b0194c68-ece7-4ac9-b3a7-3ff63a81063b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f00ba3-4a6c-48c5-87b8-2152c4a75460",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b678b196-790d-4940-8377-052de9ff3692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f00ba3-4a6c-48c5-87b8-2152c4a75460",
                    "LayerId": "39d15b82-0177-46d8-b19c-7746d24a881f"
                }
            ]
        },
        {
            "id": "75a05e59-2b44-4419-92cc-12e230a815c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cbf6d1b-fb3d-4f65-bee0-4eee6a4f6db0",
            "compositeImage": {
                "id": "2e12dcbc-e462-4e8c-91b1-7afc0de4b1ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a05e59-2b44-4419-92cc-12e230a815c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66fb248-07b9-4167-92f4-2025a8444e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a05e59-2b44-4419-92cc-12e230a815c5",
                    "LayerId": "39d15b82-0177-46d8-b19c-7746d24a881f"
                }
            ]
        },
        {
            "id": "c36bf4f6-886b-4a9f-9280-205cb41c3d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cbf6d1b-fb3d-4f65-bee0-4eee6a4f6db0",
            "compositeImage": {
                "id": "f749b0b4-5057-4f36-ac96-4c987a3b5ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c36bf4f6-886b-4a9f-9280-205cb41c3d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a156bd1e-7a82-4d02-9d8a-a310dcb23eb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c36bf4f6-886b-4a9f-9280-205cb41c3d26",
                    "LayerId": "39d15b82-0177-46d8-b19c-7746d24a881f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "39d15b82-0177-46d8-b19c-7746d24a881f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cbf6d1b-fb3d-4f65-bee0-4eee6a4f6db0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}