{
    "id": "471f1869-de4e-4465-80e9-dff185e01d69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_selling_npc_blueprints_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4702ed04-8ccc-4fe8-b98b-c2b8ce8b7cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "471f1869-de4e-4465-80e9-dff185e01d69",
            "compositeImage": {
                "id": "1eef4790-8e30-42c7-8359-8b9062b5e6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4702ed04-8ccc-4fe8-b98b-c2b8ce8b7cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f905aca2-dda7-4284-9f02-0673ecaefc82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4702ed04-8ccc-4fe8-b98b-c2b8ce8b7cc7",
                    "LayerId": "ad4a034c-065b-402d-a6d8-4da6eeffe0ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ad4a034c-065b-402d-a6d8-4da6eeffe0ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "471f1869-de4e-4465-80e9-dff185e01d69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}