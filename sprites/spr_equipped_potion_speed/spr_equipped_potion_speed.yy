{
    "id": "eec30989-c5e7-46c5-9fb1-f152277b041c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potion_speed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5be0057f-c605-49e2-a944-d8edc79be3f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eec30989-c5e7-46c5-9fb1-f152277b041c",
            "compositeImage": {
                "id": "fe663bb6-a05a-4960-848e-02ef915562f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be0057f-c605-49e2-a944-d8edc79be3f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e503654-0200-4aa9-9cd6-976900e25a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be0057f-c605-49e2-a944-d8edc79be3f0",
                    "LayerId": "3837b35e-69ab-4dbe-845b-e416975a5a83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3837b35e-69ab-4dbe-845b-e416975a5a83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eec30989-c5e7-46c5-9fb1-f152277b041c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}