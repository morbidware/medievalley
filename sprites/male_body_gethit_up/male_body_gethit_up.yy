{
    "id": "227676a5-7b15-4167-93c6-fb02c9c13e9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0afb2ab5-01b9-4d55-b067-b2a2f772c99b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "227676a5-7b15-4167-93c6-fb02c9c13e9f",
            "compositeImage": {
                "id": "1016ad0c-c8d0-4b9e-aede-41115990d558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0afb2ab5-01b9-4d55-b067-b2a2f772c99b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79cbe2a-1cef-4060-bf2e-88d5bf831ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0afb2ab5-01b9-4d55-b067-b2a2f772c99b",
                    "LayerId": "03e9ebc4-2aea-43df-b8d6-5124263866b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "03e9ebc4-2aea-43df-b8d6-5124263866b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "227676a5-7b15-4167-93c6-fb02c9c13e9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}