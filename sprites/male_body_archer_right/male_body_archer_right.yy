{
    "id": "b1c1e86e-2f58-401a-9ee4-17e4ec0b1734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_archer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67f7eb2b-eaf7-451a-afa4-ab090be15b36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1c1e86e-2f58-401a-9ee4-17e4ec0b1734",
            "compositeImage": {
                "id": "69389efa-f6fa-42bb-b5d6-de5ad2615bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67f7eb2b-eaf7-451a-afa4-ab090be15b36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff21204-516e-4fa2-8e91-ff84a1d23a0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67f7eb2b-eaf7-451a-afa4-ab090be15b36",
                    "LayerId": "07da58a1-43b6-471f-ab48-42dab46b9222"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "07da58a1-43b6-471f-ab48-42dab46b9222",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1c1e86e-2f58-401a-9ee4-17e4ec0b1734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}