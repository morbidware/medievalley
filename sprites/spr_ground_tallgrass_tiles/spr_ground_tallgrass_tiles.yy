{
    "id": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_tallgrass_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5da6a5d-7763-4718-be82-e72331906131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "befb051e-8cd6-48f0-ba20-91e19e5fe088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5da6a5d-7763-4718-be82-e72331906131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a84b3844-65e6-45fe-b636-db4cf36ed70e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5da6a5d-7763-4718-be82-e72331906131",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "ea6c0676-2b1f-4014-b5c6-5edbedefcce1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "dfc38f57-3c5a-4c21-a8b9-805e481ca07f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6c0676-2b1f-4014-b5c6-5edbedefcce1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "364c4fec-dc8d-4ef5-a417-b246211d724f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6c0676-2b1f-4014-b5c6-5edbedefcce1",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "4723feda-827f-4465-9f44-5049d83a5b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "b8ad6bab-8d77-4ec3-b3a5-03a535be5a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4723feda-827f-4465-9f44-5049d83a5b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699d8fb1-0353-418d-90c4-9c2dbcb0e55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4723feda-827f-4465-9f44-5049d83a5b5a",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "999a6198-9a01-440d-a069-b3785c820a83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "f6f317e2-5d8d-4836-92e6-d9827164ad76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "999a6198-9a01-440d-a069-b3785c820a83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c9b43f-35f6-48ea-8e2b-fc047a533a70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "999a6198-9a01-440d-a069-b3785c820a83",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "e5137657-97c6-4df5-8418-8a093258f905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "ea2a0951-87ff-4ed9-9068-2d3a4c0f9a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5137657-97c6-4df5-8418-8a093258f905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40642d53-62a6-477c-8087-e5aebff73d1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5137657-97c6-4df5-8418-8a093258f905",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "94d7681b-3680-44d2-978c-0dd26d97b73d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "e5c6a011-9c02-45a5-8b76-cee36158d263",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94d7681b-3680-44d2-978c-0dd26d97b73d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "421e8ea0-7437-4997-a942-1b47cdd278f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94d7681b-3680-44d2-978c-0dd26d97b73d",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "ee632697-8f5b-4636-aa41-f09ae46a92f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "f3c43944-b95b-43eb-ad0c-8859bb58430e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee632697-8f5b-4636-aa41-f09ae46a92f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4488f823-6c1a-4c99-8ea2-8294e3fe5cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee632697-8f5b-4636-aa41-f09ae46a92f5",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "8dd156e5-d9a8-402e-9736-476abe2aac94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "9781942f-45fc-43b4-a47c-cec841ae9878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8dd156e5-d9a8-402e-9736-476abe2aac94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "120d8a20-a7ee-42bd-af4b-05b4562eafc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8dd156e5-d9a8-402e-9736-476abe2aac94",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "a0d6a757-7ca6-4a74-82d0-7909d344b3dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "7c27145a-f986-4957-b8df-2f67440998e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0d6a757-7ca6-4a74-82d0-7909d344b3dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40f6018d-6671-4f39-ad51-08af2b447c85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0d6a757-7ca6-4a74-82d0-7909d344b3dd",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "435901c4-2ff2-4d53-9d98-82c36c9d755c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "228feda5-0789-4b65-8dee-4778e4e51256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435901c4-2ff2-4d53-9d98-82c36c9d755c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51bec691-b933-453c-bf0c-5365304ab745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435901c4-2ff2-4d53-9d98-82c36c9d755c",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "c5f26f90-834a-4194-81b2-9f5323994700",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "75d2a30b-401b-499c-9cae-8aeba885a7d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f26f90-834a-4194-81b2-9f5323994700",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cba2e74-9103-4d52-b1e2-db5908ede80c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f26f90-834a-4194-81b2-9f5323994700",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "51558e96-613d-4bc3-86bb-b61fd43b7472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "ae47ec90-dbb3-494e-9a0e-1ce61f88fbb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51558e96-613d-4bc3-86bb-b61fd43b7472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299745ac-e019-4d80-b07e-b5c32b66572b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51558e96-613d-4bc3-86bb-b61fd43b7472",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "aeeb63db-6e7e-47f3-bd14-b707859e0229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "d483fb0c-488b-4afb-876c-8d9433c54712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeeb63db-6e7e-47f3-bd14-b707859e0229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab9e75b-31c1-4610-aeab-40cc28db3575",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeeb63db-6e7e-47f3-bd14-b707859e0229",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "7ba641a3-c7e9-4b7c-a050-a0b5d5398023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "103a5781-18aa-4af5-a501-9ff8671fddb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ba641a3-c7e9-4b7c-a050-a0b5d5398023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a708d646-4405-41aa-814d-c816a93f615f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ba641a3-c7e9-4b7c-a050-a0b5d5398023",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "143b321f-dc3b-4770-a25a-c8a7965b20ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "9e2a64a3-9ac6-4333-9a90-d5bb103fde46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143b321f-dc3b-4770-a25a-c8a7965b20ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c99cb3-7179-4237-b1de-c9908477cc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143b321f-dc3b-4770-a25a-c8a7965b20ce",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "22d14630-34d8-4f21-a6c6-ba069c3bd4d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "cac83928-7e75-46d1-9517-7d5940493b7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d14630-34d8-4f21-a6c6-ba069c3bd4d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a21a41c2-cadb-47eb-94e3-ec71b7063438",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d14630-34d8-4f21-a6c6-ba069c3bd4d6",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "caf34ebb-86df-4f7e-be68-ea76519c5f65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "f904eb95-718d-476d-8355-da85359406db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf34ebb-86df-4f7e-be68-ea76519c5f65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caec9db4-a1c8-418e-af65-c1d0502ef296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf34ebb-86df-4f7e-be68-ea76519c5f65",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "1ce2b78f-1d87-42db-8ca8-30d678ff7d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "b0547edc-109b-4379-a394-7522aff7bbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ce2b78f-1d87-42db-8ca8-30d678ff7d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea8bdec-6189-4543-8655-fb69ae81d84a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ce2b78f-1d87-42db-8ca8-30d678ff7d80",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "7e61108d-041f-4609-97da-8fe1dac417d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "06f69f98-817d-44b2-b6f0-44c28bf06ece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e61108d-041f-4609-97da-8fe1dac417d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b6abb7-93c9-4d6b-95e1-00f2081b0a2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e61108d-041f-4609-97da-8fe1dac417d5",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "f9b714a9-dd3a-43c6-b5c3-a289f718d4f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "682e86af-25eb-48f9-8936-b321e465929d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b714a9-dd3a-43c6-b5c3-a289f718d4f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "080745c3-b713-4488-971e-9ec61947936f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b714a9-dd3a-43c6-b5c3-a289f718d4f3",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "4daf13cd-3977-433c-aa14-fc2007c95567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "237ebc1f-6f12-456e-bda1-91591e1d55be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4daf13cd-3977-433c-aa14-fc2007c95567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049931db-0537-42ba-98a1-197cae8d5d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4daf13cd-3977-433c-aa14-fc2007c95567",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "f64ad97c-d809-46c2-8ff0-5aada912be72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "d98e08f0-7f97-4b97-b2cb-fa6763d04cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f64ad97c-d809-46c2-8ff0-5aada912be72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d230b60-eae7-48bf-bf08-2b1a3c88c38f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f64ad97c-d809-46c2-8ff0-5aada912be72",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "ea68fae3-46e1-4b78-bc0c-de687d097779",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "d66af17d-5f4a-4c03-8435-c9664937b65b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea68fae3-46e1-4b78-bc0c-de687d097779",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e189962-7123-4f76-8b84-5167da715f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea68fae3-46e1-4b78-bc0c-de687d097779",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "8621089e-78d4-4395-9ca1-7c5b2a9fc3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "53579169-3dcf-4fac-aeee-ea8ae15fa88f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8621089e-78d4-4395-9ca1-7c5b2a9fc3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51bdeb1a-bbef-43b2-bc53-46656e70da14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8621089e-78d4-4395-9ca1-7c5b2a9fc3be",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "f12aef26-5102-426b-b233-a951e29e10ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "de904d91-605d-4905-a4e0-11870cc5941c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12aef26-5102-426b-b233-a951e29e10ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b3e971-5566-40f5-831d-d5a463225ab1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12aef26-5102-426b-b233-a951e29e10ec",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "2f907f52-243f-4b4b-a21c-9e6db88712da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "39fc85e0-0aa9-4776-9142-90302dff5018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f907f52-243f-4b4b-a21c-9e6db88712da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6bddb43-9311-48b1-ada5-5b787e840c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f907f52-243f-4b4b-a21c-9e6db88712da",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "e1537da1-fa79-4785-aebf-ced6d928a295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "293d6809-8d53-4619-8c4e-dbb2ff4e6d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1537da1-fa79-4785-aebf-ced6d928a295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b023686-91e1-41b5-8e65-7d4a3c072ff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1537da1-fa79-4785-aebf-ced6d928a295",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "a3cdcdbd-e631-40b3-aa24-8ff0e8e2eb48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "014c3fb8-0561-44e8-bac2-72201c9ef25a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3cdcdbd-e631-40b3-aa24-8ff0e8e2eb48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c2a0452-1d31-4d22-b85c-c665584ce48e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3cdcdbd-e631-40b3-aa24-8ff0e8e2eb48",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "3f38c68d-65cc-41f0-bffc-0271eafa14a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "9b3a91b6-b915-4726-ab33-ba8ffbf049e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f38c68d-65cc-41f0-bffc-0271eafa14a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48cc75ef-7c78-4dfa-88cd-0575de384768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f38c68d-65cc-41f0-bffc-0271eafa14a1",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "5489b16b-eeb8-45c1-923b-ca3c845f6b56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "8dc87446-a757-4b0c-9a79-46bd1313ac5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5489b16b-eeb8-45c1-923b-ca3c845f6b56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3736962-7bdd-4b7f-b301-431eacb8237d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5489b16b-eeb8-45c1-923b-ca3c845f6b56",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "0f052238-5a1a-4a4a-9ce1-55ffa7e6717d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "33572374-1f15-46c5-941a-218bc40ad209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f052238-5a1a-4a4a-9ce1-55ffa7e6717d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95dead62-9cda-47ab-acb6-3989cd517cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f052238-5a1a-4a4a-9ce1-55ffa7e6717d",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "10b31652-9cec-4912-a388-d21f1ee25644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "caba3452-d36f-4996-98ce-8fc9ffd08bbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b31652-9cec-4912-a388-d21f1ee25644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee449802-7fca-41d5-8a49-c46ecf86f662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b31652-9cec-4912-a388-d21f1ee25644",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "926c3bed-6866-4031-a35a-3a9308ac4af5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "a2d43bf9-5804-4861-b601-0aed882b3fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926c3bed-6866-4031-a35a-3a9308ac4af5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f27b79-cd95-43d6-8cb8-63400e868ba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926c3bed-6866-4031-a35a-3a9308ac4af5",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "47dd3198-43b4-4b9f-960f-223098a500ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "08ab568c-e968-4567-b15b-7563eb344e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47dd3198-43b4-4b9f-960f-223098a500ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52bbe8e3-9e8d-443c-9b3c-3ceb77b53049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47dd3198-43b4-4b9f-960f-223098a500ee",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "6c01123b-ffa1-43c3-ab82-8b990e4a2dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "959a202c-e851-41bd-99cc-af8e25eccf61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c01123b-ffa1-43c3-ab82-8b990e4a2dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0f319ed-5b4c-417e-a7f6-db40b4c1cd4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c01123b-ffa1-43c3-ab82-8b990e4a2dbd",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "00e5a470-68bb-4dc9-b249-9d5a869b52d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "4dd6b111-6c48-40b9-9da2-937fb009af11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00e5a470-68bb-4dc9-b249-9d5a869b52d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6244897d-8969-401c-a5ea-697aa0e6178e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00e5a470-68bb-4dc9-b249-9d5a869b52d0",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "77154e9c-804f-4e7f-a5f9-b7e2f8c0c718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "dd182870-8cdd-494f-806a-a90ab0ac0a90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77154e9c-804f-4e7f-a5f9-b7e2f8c0c718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b2eee0a-1d38-4abe-8ae6-cb358073b444",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77154e9c-804f-4e7f-a5f9-b7e2f8c0c718",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "203ad15a-5b50-4bc1-b2f8-18d447e3d3b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "2b47cbe7-1fb2-4c0a-8f3a-895f1bd68795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203ad15a-5b50-4bc1-b2f8-18d447e3d3b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb4e7ae-fd47-4367-bd57-e20c642c912c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203ad15a-5b50-4bc1-b2f8-18d447e3d3b7",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "2c71945d-eb83-406e-b8b6-361e58feed58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "d750b3b9-a339-47bb-ba6f-2a900d6fe6ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c71945d-eb83-406e-b8b6-361e58feed58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fc58876-133a-444a-ace9-245e248663fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c71945d-eb83-406e-b8b6-361e58feed58",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "5afe8a15-8be2-4939-b283-fc00c937cdc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "49547d8c-2a6e-43aa-860f-f7deb05bc01b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afe8a15-8be2-4939-b283-fc00c937cdc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e640bf38-e084-41f0-a6e7-7ca333f104c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afe8a15-8be2-4939-b283-fc00c937cdc6",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "9fea5ac7-ac3b-408c-909c-bc45c52edad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "4e612869-431f-4f8a-800f-36f123c9f743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fea5ac7-ac3b-408c-909c-bc45c52edad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e694294a-d8c4-4bdc-ab1d-f6bc64a1b61f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fea5ac7-ac3b-408c-909c-bc45c52edad7",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "327167b1-c174-4daf-8130-89f1f128eae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "2c067881-8fa0-4104-86d6-a8438a48e941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "327167b1-c174-4daf-8130-89f1f128eae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc05db5b-f839-4799-bc8e-2257f9351080",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "327167b1-c174-4daf-8130-89f1f128eae8",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "e20a0dc6-afe7-49de-b66f-d5cf4bc4b93b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "646841a2-bb53-4123-9d1b-0ca409cdd570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e20a0dc6-afe7-49de-b66f-d5cf4bc4b93b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a22ae267-12de-4a07-a92a-35540e6ba1da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e20a0dc6-afe7-49de-b66f-d5cf4bc4b93b",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "0a24f725-c4ed-4aea-8389-837786d4213d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "8f86ab4f-97d2-43e2-95df-d07851d7fbe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a24f725-c4ed-4aea-8389-837786d4213d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d5df2a4-ba2a-4ec2-848a-68623b76eafb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a24f725-c4ed-4aea-8389-837786d4213d",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "9bc47656-f53a-44ce-bb63-20ea9d8dcc90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "9bf08b2b-9b62-40e0-a6c5-5a8b4e32e32f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc47656-f53a-44ce-bb63-20ea9d8dcc90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c60720-f2ad-4cc8-a643-df0661455023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc47656-f53a-44ce-bb63-20ea9d8dcc90",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "aa419400-ec9e-4eb8-a5a7-30e7f6c7d9a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "bc2fa9f9-7af6-4a61-b918-e4b183b5e64c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa419400-ec9e-4eb8-a5a7-30e7f6c7d9a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3070a1d5-26f1-42ea-842b-2246dcbf03d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa419400-ec9e-4eb8-a5a7-30e7f6c7d9a0",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "57c71c00-943c-44b0-9e90-e3b5a0306935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "ddba7845-70e1-4b0f-8d7c-c45e2bd56fd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c71c00-943c-44b0-9e90-e3b5a0306935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c7525b-2c54-4cf0-95c9-edab93e17a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c71c00-943c-44b0-9e90-e3b5a0306935",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "f235b16d-711e-48e6-b694-357cff7a7309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "321647a6-7f5b-457b-b3fa-e6ebe21d0975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f235b16d-711e-48e6-b694-357cff7a7309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa33023-ddee-48ef-b88f-7889a956a45e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f235b16d-711e-48e6-b694-357cff7a7309",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        },
        {
            "id": "e93aa547-5cd8-4891-95e4-094368cf754b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "compositeImage": {
                "id": "08031db7-cfc0-4afc-9e33-7b54f5d4c04a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e93aa547-5cd8-4891-95e4-094368cf754b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ca5904-a1b2-47e8-8d74-dddbff5f4fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e93aa547-5cd8-4891-95e4-094368cf754b",
                    "LayerId": "13c36941-5adc-4c24-9dc3-e6cb5b749182"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13c36941-5adc-4c24-9dc3-e6cb5b749182",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20b62b92-ca37-41bf-8805-62c92c58cd7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}