{
    "id": "8f04a744-e0bf-4d4f-be2f-d7e2c157996c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a937239a-10fa-4730-a3dd-912327b49440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f04a744-e0bf-4d4f-be2f-d7e2c157996c",
            "compositeImage": {
                "id": "415905c2-9b4e-42aa-996d-73ec74799b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a937239a-10fa-4730-a3dd-912327b49440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e38642-37dc-433e-bfde-fd6bcbfdce52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a937239a-10fa-4730-a3dd-912327b49440",
                    "LayerId": "3cbe0413-3a9c-40eb-8f8b-8dc35f1509a7"
                }
            ]
        },
        {
            "id": "b3f76d9c-046e-4ff8-b21a-6690b789a58d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f04a744-e0bf-4d4f-be2f-d7e2c157996c",
            "compositeImage": {
                "id": "afba6671-e808-46cc-a278-4adfd146bfed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3f76d9c-046e-4ff8-b21a-6690b789a58d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df7a7b95-415c-47eb-a858-57d0b32485a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3f76d9c-046e-4ff8-b21a-6690b789a58d",
                    "LayerId": "3cbe0413-3a9c-40eb-8f8b-8dc35f1509a7"
                }
            ]
        },
        {
            "id": "8ec9f43a-c9d3-4663-8a21-4a5418dc0a49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f04a744-e0bf-4d4f-be2f-d7e2c157996c",
            "compositeImage": {
                "id": "30b7dbc3-6676-45f0-a802-6b55ca257ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec9f43a-c9d3-4663-8a21-4a5418dc0a49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cae5bd2-63e8-4ec8-9913-c25fe5675b0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec9f43a-c9d3-4663-8a21-4a5418dc0a49",
                    "LayerId": "3cbe0413-3a9c-40eb-8f8b-8dc35f1509a7"
                }
            ]
        },
        {
            "id": "a8b5c84d-8023-4136-9eb7-c26c5e20ef0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f04a744-e0bf-4d4f-be2f-d7e2c157996c",
            "compositeImage": {
                "id": "dc7a0ac9-cc6e-4e0e-9282-ed46966fe017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8b5c84d-8023-4136-9eb7-c26c5e20ef0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1f9bfe-fb39-4bcb-a16e-fac9b4554150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8b5c84d-8023-4136-9eb7-c26c5e20ef0f",
                    "LayerId": "3cbe0413-3a9c-40eb-8f8b-8dc35f1509a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3cbe0413-3a9c-40eb-8f8b-8dc35f1509a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f04a744-e0bf-4d4f-be2f-d7e2c157996c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}