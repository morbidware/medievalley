{
    "id": "851f19b1-8340-4fd8-976d-ea89f05c4a7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "907de5ff-136a-4a30-ab0c-35a15aa3c88b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "851f19b1-8340-4fd8-976d-ea89f05c4a7b",
            "compositeImage": {
                "id": "5e700b27-83bb-4eb2-bb3a-9963d2258393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "907de5ff-136a-4a30-ab0c-35a15aa3c88b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc6c2cca-b30a-499b-8815-875ac95189c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "907de5ff-136a-4a30-ab0c-35a15aa3c88b",
                    "LayerId": "b5ff57ba-3a11-45b4-bda8-f80eb9a9ead9"
                }
            ]
        },
        {
            "id": "509f1245-0aa0-4acf-a80e-d9eb08ae2753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "851f19b1-8340-4fd8-976d-ea89f05c4a7b",
            "compositeImage": {
                "id": "7a860266-2640-41a2-b621-99b6246b6482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "509f1245-0aa0-4acf-a80e-d9eb08ae2753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a4c5903-b9c4-4c3a-b9ae-64f43d4c5e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509f1245-0aa0-4acf-a80e-d9eb08ae2753",
                    "LayerId": "b5ff57ba-3a11-45b4-bda8-f80eb9a9ead9"
                }
            ]
        },
        {
            "id": "a79b1433-6651-4551-83d9-fa56de9594bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "851f19b1-8340-4fd8-976d-ea89f05c4a7b",
            "compositeImage": {
                "id": "8a13fbad-8bdc-47be-9041-930d3b89c499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a79b1433-6651-4551-83d9-fa56de9594bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc731e5f-9d20-4539-94a7-fc91e0861c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a79b1433-6651-4551-83d9-fa56de9594bb",
                    "LayerId": "b5ff57ba-3a11-45b4-bda8-f80eb9a9ead9"
                }
            ]
        },
        {
            "id": "f6ad46d5-1c3a-4c01-afd9-6b62515d0ce4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "851f19b1-8340-4fd8-976d-ea89f05c4a7b",
            "compositeImage": {
                "id": "2e648191-4100-4405-b5b9-706d17b9a930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6ad46d5-1c3a-4c01-afd9-6b62515d0ce4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2229a47d-4b43-466d-b89a-d08d1352ce97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6ad46d5-1c3a-4c01-afd9-6b62515d0ce4",
                    "LayerId": "b5ff57ba-3a11-45b4-bda8-f80eb9a9ead9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5ff57ba-3a11-45b4-bda8-f80eb9a9ead9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "851f19b1-8340-4fd8-976d-ea89f05c4a7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}