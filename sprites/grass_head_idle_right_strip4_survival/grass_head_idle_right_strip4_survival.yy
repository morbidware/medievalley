{
    "id": "a25546bc-7838-4897-bad2-1ad73196930f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_idle_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e82bf565-4742-419a-b85e-bd93e40a795c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25546bc-7838-4897-bad2-1ad73196930f",
            "compositeImage": {
                "id": "e87f4855-c754-45b6-a3c2-0a75099e3c6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e82bf565-4742-419a-b85e-bd93e40a795c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d18b80-cd96-4cb6-8f6d-8f960f7dd117",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e82bf565-4742-419a-b85e-bd93e40a795c",
                    "LayerId": "8f45ef40-ee44-4670-9d28-5a00d7433502"
                }
            ]
        },
        {
            "id": "ba2eaaa9-1918-4f6a-bf4f-c28f6c2c4b69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25546bc-7838-4897-bad2-1ad73196930f",
            "compositeImage": {
                "id": "3ed2cade-0252-415e-aba8-4728ea2ffd1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba2eaaa9-1918-4f6a-bf4f-c28f6c2c4b69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e5aa123-7187-4b0a-b1da-2a2bc227ff50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba2eaaa9-1918-4f6a-bf4f-c28f6c2c4b69",
                    "LayerId": "8f45ef40-ee44-4670-9d28-5a00d7433502"
                }
            ]
        },
        {
            "id": "0de71c99-4ccf-4e88-b62a-4ce9283ef1e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25546bc-7838-4897-bad2-1ad73196930f",
            "compositeImage": {
                "id": "a20bf3f9-54b5-42d9-a56f-b135f4956675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0de71c99-4ccf-4e88-b62a-4ce9283ef1e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a785e2a0-582f-480c-8784-483461431285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0de71c99-4ccf-4e88-b62a-4ce9283ef1e1",
                    "LayerId": "8f45ef40-ee44-4670-9d28-5a00d7433502"
                }
            ]
        },
        {
            "id": "b0f8378b-d6e5-46f3-8516-8074f6dbc327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a25546bc-7838-4897-bad2-1ad73196930f",
            "compositeImage": {
                "id": "2940c750-256c-40d4-ae63-3a40b2c2722e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f8378b-d6e5-46f3-8516-8074f6dbc327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44622c0d-2e44-4ecb-bdd4-d4b7f87de480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f8378b-d6e5-46f3-8516-8074f6dbc327",
                    "LayerId": "8f45ef40-ee44-4670-9d28-5a00d7433502"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8f45ef40-ee44-4670-9d28-5a00d7433502",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a25546bc-7838-4897-bad2-1ad73196930f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}