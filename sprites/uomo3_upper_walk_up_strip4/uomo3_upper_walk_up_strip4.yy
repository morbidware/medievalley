{
    "id": "0a6f5c10-eb26-4746-af76-0704841eecd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81b49e73-784f-4693-8bac-cd3094c507ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a6f5c10-eb26-4746-af76-0704841eecd9",
            "compositeImage": {
                "id": "90fe52bb-943e-49fe-8358-1eb2a05d5dd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b49e73-784f-4693-8bac-cd3094c507ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "167a2579-760e-43f3-bd77-a1bce675d7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b49e73-784f-4693-8bac-cd3094c507ab",
                    "LayerId": "c087e0fa-3ef8-4db1-963c-87b9da6c9651"
                }
            ]
        },
        {
            "id": "e30a5051-1689-4a0d-be16-2d266056c889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a6f5c10-eb26-4746-af76-0704841eecd9",
            "compositeImage": {
                "id": "30358e82-e5f5-41ff-9c1b-00177509dcc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e30a5051-1689-4a0d-be16-2d266056c889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a92bc05-edb5-4dea-9e74-b34bff2d5bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e30a5051-1689-4a0d-be16-2d266056c889",
                    "LayerId": "c087e0fa-3ef8-4db1-963c-87b9da6c9651"
                }
            ]
        },
        {
            "id": "7f083eca-b7a6-4818-97ae-4581e400d539",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a6f5c10-eb26-4746-af76-0704841eecd9",
            "compositeImage": {
                "id": "832753c5-954b-49b5-9531-0a42f45241d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f083eca-b7a6-4818-97ae-4581e400d539",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20312992-124f-4068-9a09-28780aa171ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f083eca-b7a6-4818-97ae-4581e400d539",
                    "LayerId": "c087e0fa-3ef8-4db1-963c-87b9da6c9651"
                }
            ]
        },
        {
            "id": "db599970-e235-4829-b306-320d52e99265",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a6f5c10-eb26-4746-af76-0704841eecd9",
            "compositeImage": {
                "id": "eed89fed-fb09-4351-a34d-d10c0c154278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db599970-e235-4829-b306-320d52e99265",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "327b4a42-1d77-48b4-98ce-330d20f74dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db599970-e235-4829-b306-320d52e99265",
                    "LayerId": "c087e0fa-3ef8-4db1-963c-87b9da6c9651"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c087e0fa-3ef8-4db1-963c-87b9da6c9651",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a6f5c10-eb26-4746-af76-0704841eecd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}