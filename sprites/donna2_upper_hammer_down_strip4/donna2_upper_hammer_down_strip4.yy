{
    "id": "dc99a4da-4f09-4225-95f6-855abef40a80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "336c7749-bb3a-4a8a-9606-c22c705bef8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc99a4da-4f09-4225-95f6-855abef40a80",
            "compositeImage": {
                "id": "c93dde0d-edb1-4fb2-8699-1398842e32e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "336c7749-bb3a-4a8a-9606-c22c705bef8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101b8c1b-0fb8-4530-961f-ff0577d333f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "336c7749-bb3a-4a8a-9606-c22c705bef8a",
                    "LayerId": "c7528c08-ae3f-478b-83eb-15120522e048"
                }
            ]
        },
        {
            "id": "72a94f50-d011-4696-9e3d-52ab81bcb3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc99a4da-4f09-4225-95f6-855abef40a80",
            "compositeImage": {
                "id": "a14464ed-9524-40d7-b625-7d3dfd634012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a94f50-d011-4696-9e3d-52ab81bcb3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e808c5ce-138b-4e91-b131-bb72a6b765ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a94f50-d011-4696-9e3d-52ab81bcb3cf",
                    "LayerId": "c7528c08-ae3f-478b-83eb-15120522e048"
                }
            ]
        },
        {
            "id": "69d187db-50e1-4fd4-bf78-7ec36e75b002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc99a4da-4f09-4225-95f6-855abef40a80",
            "compositeImage": {
                "id": "c174c0fe-5c8b-468c-975f-e36a57ba9443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d187db-50e1-4fd4-bf78-7ec36e75b002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8dafb77-9baf-4a69-bdfc-fe15731f2824",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d187db-50e1-4fd4-bf78-7ec36e75b002",
                    "LayerId": "c7528c08-ae3f-478b-83eb-15120522e048"
                }
            ]
        },
        {
            "id": "a31527c1-1120-41bc-a832-de868f166a0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc99a4da-4f09-4225-95f6-855abef40a80",
            "compositeImage": {
                "id": "4a2d667a-8170-49b1-9ed3-0acb3f26548e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31527c1-1120-41bc-a832-de868f166a0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8390ee8f-f19b-4883-b35e-80054c925b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31527c1-1120-41bc-a832-de868f166a0e",
                    "LayerId": "c7528c08-ae3f-478b-83eb-15120522e048"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7528c08-ae3f-478b-83eb-15120522e048",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc99a4da-4f09-4225-95f6-855abef40a80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}