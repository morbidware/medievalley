{
    "id": "56a741b6-9a70-4631-a22b-98047bf6f8ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_close_bronze",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24673701-b12c-4992-af2d-7b40d68f9798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56a741b6-9a70-4631-a22b-98047bf6f8ed",
            "compositeImage": {
                "id": "570c311c-0e0d-4e35-9bf0-c09b7fd648c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24673701-b12c-4992-af2d-7b40d68f9798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f07a69d9-08bd-41cc-9e0c-a5e5e489c0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24673701-b12c-4992-af2d-7b40d68f9798",
                    "LayerId": "19ead93c-7fcb-4a86-86d6-9d48e6d28838"
                }
            ]
        },
        {
            "id": "e31a2485-320e-4531-9c0e-b99493c62699",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56a741b6-9a70-4631-a22b-98047bf6f8ed",
            "compositeImage": {
                "id": "92088581-4d41-405a-b8c0-2b1bc618e0d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e31a2485-320e-4531-9c0e-b99493c62699",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa8199c0-3f6b-41e6-a413-83a02ef9897f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e31a2485-320e-4531-9c0e-b99493c62699",
                    "LayerId": "19ead93c-7fcb-4a86-86d6-9d48e6d28838"
                }
            ]
        },
        {
            "id": "598a308d-7608-4c89-b16a-888043f2d6eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56a741b6-9a70-4631-a22b-98047bf6f8ed",
            "compositeImage": {
                "id": "ac917ace-caea-408b-8003-4900336710ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "598a308d-7608-4c89-b16a-888043f2d6eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79f77e9b-cb83-4e71-96aa-c7f3b4272cd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "598a308d-7608-4c89-b16a-888043f2d6eb",
                    "LayerId": "19ead93c-7fcb-4a86-86d6-9d48e6d28838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "19ead93c-7fcb-4a86-86d6-9d48e6d28838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56a741b6-9a70-4631-a22b-98047bf6f8ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}