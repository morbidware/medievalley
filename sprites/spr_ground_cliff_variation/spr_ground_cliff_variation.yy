{
    "id": "f10473a6-de40-439a-81b1-5502f24c931a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_cliff_variation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48ff08af-9227-4cd9-92fe-61bb83b05275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "b2e52271-23f7-4eba-a8db-4e148f9b18ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48ff08af-9227-4cd9-92fe-61bb83b05275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507ac943-54dc-4fd3-be04-babae04a5b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48ff08af-9227-4cd9-92fe-61bb83b05275",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "88a795a1-c241-4ff3-bc58-c27287c74bfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "5969f185-72b2-4a32-9694-4a06cefbb37c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a795a1-c241-4ff3-bc58-c27287c74bfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b81c9b-39e8-4360-a977-6a03780285f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a795a1-c241-4ff3-bc58-c27287c74bfd",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "e8996112-f5bb-4fb6-996e-55d67a28959f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "5eaea71e-63ca-450a-abf2-850d70dd2267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8996112-f5bb-4fb6-996e-55d67a28959f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9e6ae49-0733-4650-9964-11c5178fdbb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8996112-f5bb-4fb6-996e-55d67a28959f",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "6e98a4dc-46c3-4ff3-8251-b2fe72735f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "0daff36a-3a22-44d3-badc-1e3c235a4e17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e98a4dc-46c3-4ff3-8251-b2fe72735f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863e9816-59c4-42aa-b7ba-ddf9e000b58b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e98a4dc-46c3-4ff3-8251-b2fe72735f82",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "0f4e4a85-bcc0-4a47-85d8-690c04904d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "0a940839-2055-48ec-b629-ee9c63344066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f4e4a85-bcc0-4a47-85d8-690c04904d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c277b3f8-bbed-4885-ab5a-453c402598c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f4e4a85-bcc0-4a47-85d8-690c04904d63",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "dca70a2d-73b9-4d8d-bcab-1f7ff4d55acc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "c9506bee-d77e-43ef-9b07-5a98a3eb0bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca70a2d-73b9-4d8d-bcab-1f7ff4d55acc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "111d036d-2702-427d-b704-91e5f0bd210a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca70a2d-73b9-4d8d-bcab-1f7ff4d55acc",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "b0cc39e4-7fd8-4a85-9dfc-e4aee19fba74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "f21a8d6b-f0f1-4f82-bf35-9530679551bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cc39e4-7fd8-4a85-9dfc-e4aee19fba74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a5f12f-6d17-4a66-bc86-1782d033067d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cc39e4-7fd8-4a85-9dfc-e4aee19fba74",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "be88427b-2a73-4807-aa96-4859618b478e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "b91ed4a9-93dc-4a49-9bd0-513dce9c1c7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be88427b-2a73-4807-aa96-4859618b478e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15ec548d-caa3-4426-baa2-32d7fc6c05b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be88427b-2a73-4807-aa96-4859618b478e",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "6e42056c-23c1-4343-9ee7-b04f3548b81c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "bf099531-ae07-4453-b403-f657f8425c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e42056c-23c1-4343-9ee7-b04f3548b81c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be3f638-3f35-45a3-969d-9aa265815b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e42056c-23c1-4343-9ee7-b04f3548b81c",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "fbcdcd39-c1e1-40c7-9f0a-76b8207557e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "a7a8ee4a-0659-4ec3-b5f7-e017e000c81b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbcdcd39-c1e1-40c7-9f0a-76b8207557e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc6ea5a-1133-4991-80e2-7f453d775b4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbcdcd39-c1e1-40c7-9f0a-76b8207557e4",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "da672f33-8be3-4114-aff2-75104a312599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "24366feb-bc66-4128-87be-e979b7cb5b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da672f33-8be3-4114-aff2-75104a312599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d169902-365c-4454-9e6d-2c5424a21796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da672f33-8be3-4114-aff2-75104a312599",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "32e85a99-08ca-4fa1-9318-8f43260b0987",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "454fbd5b-7718-46c5-ad67-5da99bd916e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e85a99-08ca-4fa1-9318-8f43260b0987",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c63acd-1dc7-4b01-9451-a02bc8014573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e85a99-08ca-4fa1-9318-8f43260b0987",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "916a0866-83d0-4681-ba49-ef13ac50745f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "c9c80e3e-f8a4-44ef-95f3-96e01c13a940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916a0866-83d0-4681-ba49-ef13ac50745f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96dde48-5a25-4b06-977e-ad9d609b69c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916a0866-83d0-4681-ba49-ef13ac50745f",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "305d13ea-7f40-42c4-8910-72d4a8804758",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "df9e3bf2-6e3d-42bf-ad9a-be0623d240d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305d13ea-7f40-42c4-8910-72d4a8804758",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a3d812-76cd-4bae-9eaf-37b7d834440e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305d13ea-7f40-42c4-8910-72d4a8804758",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "75b6ed82-7e2d-4f0f-a216-78ba8bfc33cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "47bf9d3b-f900-470e-ae98-dcaef2496588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b6ed82-7e2d-4f0f-a216-78ba8bfc33cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32f5298c-7ac4-46c4-b431-a3f655411267",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b6ed82-7e2d-4f0f-a216-78ba8bfc33cb",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "075ab6c3-9c5a-4e59-8bda-20d05470d0e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "f3a9f9a5-f4b5-41b2-b804-dd60526e73cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "075ab6c3-9c5a-4e59-8bda-20d05470d0e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7ada22-0fa7-40b0-986a-e25e295c0410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "075ab6c3-9c5a-4e59-8bda-20d05470d0e3",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "8cfc4611-3d44-4baa-9ef7-41e7cafc50d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "994b49b2-76eb-4c0e-b1ba-baade79ac3da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cfc4611-3d44-4baa-9ef7-41e7cafc50d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff7cfac1-b826-4e82-bc95-75d94575ab5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cfc4611-3d44-4baa-9ef7-41e7cafc50d7",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "c347ae65-68f4-4bfb-be39-ae291bb8f824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "e373999e-d62c-4d95-a8b8-0174c47c15b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c347ae65-68f4-4bfb-be39-ae291bb8f824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b51053-ceaa-405c-955f-212fd3d3151d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c347ae65-68f4-4bfb-be39-ae291bb8f824",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "3f059990-4496-4b85-a012-ab95363dd58a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "8a3d222b-6dc8-4477-b893-c5ac074456d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f059990-4496-4b85-a012-ab95363dd58a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4278d6ab-de16-45f9-9077-c2a455339480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f059990-4496-4b85-a012-ab95363dd58a",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "2345e7dc-0b57-4f0e-a1bd-aad3433a7203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "02ffa0e8-22b4-4e89-988b-de1d8b958e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2345e7dc-0b57-4f0e-a1bd-aad3433a7203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "934d0359-cfe7-4649-81f1-eba82df2bc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2345e7dc-0b57-4f0e-a1bd-aad3433a7203",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "0b9c774c-2404-4d0d-807a-cf589065214a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "f48dafbf-40dd-4049-98b2-39663668e90b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b9c774c-2404-4d0d-807a-cf589065214a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fae3191-7cd8-4823-a412-4d1fea7b1453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b9c774c-2404-4d0d-807a-cf589065214a",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "24afa5d6-5b43-49f5-bb01-b44c000f16ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "74a1197b-5b41-419f-b12a-fddcca4dd6dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24afa5d6-5b43-49f5-bb01-b44c000f16ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dac08a1-db91-48e3-92d5-d5407666bd0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24afa5d6-5b43-49f5-bb01-b44c000f16ec",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "97fef729-fa39-4db0-8850-5c94028fd70e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "20fcbaa6-c2db-46ae-9606-a61031f8de3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97fef729-fa39-4db0-8850-5c94028fd70e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f55bbb3-ea88-4d3e-80f6-5af76b890c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97fef729-fa39-4db0-8850-5c94028fd70e",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "825e3828-bc5c-467c-a3e2-edbf1e34ddfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "83e89222-0fe3-4a13-908b-a9a49f105bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825e3828-bc5c-467c-a3e2-edbf1e34ddfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15cde7c4-6ae2-4ad4-bf10-959d9dda88c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825e3828-bc5c-467c-a3e2-edbf1e34ddfc",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "05473f50-3505-4ade-96ef-8266d6e0a112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "2fdbdf2a-0229-42ba-90c5-693108b4ce5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05473f50-3505-4ade-96ef-8266d6e0a112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eb1e308-b7fc-4a90-8411-800d57255d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05473f50-3505-4ade-96ef-8266d6e0a112",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "445dc2fc-b2d6-4ef8-8bce-6cd8ab7bd419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "d21d6c27-1437-4038-943f-9fe2499063e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "445dc2fc-b2d6-4ef8-8bce-6cd8ab7bd419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9825649-dafb-4c88-8913-93a146f472f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "445dc2fc-b2d6-4ef8-8bce-6cd8ab7bd419",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "3e8b6f26-bcdf-4ff7-9202-3289e6e3b43d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "85ce2218-be8c-4b87-b940-e8293ec9af25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e8b6f26-bcdf-4ff7-9202-3289e6e3b43d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eccee649-ee3c-4bf6-9450-4e693d3bc329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e8b6f26-bcdf-4ff7-9202-3289e6e3b43d",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "a730ff8e-7402-4c8b-b76c-0f4cbcd04de8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "86e52eac-5714-4e8e-8dec-1cc23696f350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a730ff8e-7402-4c8b-b76c-0f4cbcd04de8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f34aad-33ee-4210-a151-522ed6fd4cf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a730ff8e-7402-4c8b-b76c-0f4cbcd04de8",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "b158e70a-5bd0-4b1f-854a-b8b990d52eb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "f07cb2c5-b0d3-4402-83b7-f7404ff013b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b158e70a-5bd0-4b1f-854a-b8b990d52eb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f045d2db-a47f-493f-85d5-6704029aca31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b158e70a-5bd0-4b1f-854a-b8b990d52eb5",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "7d1b14d3-56da-4c6d-9451-1ad4b928c8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "a1496f14-62da-4f5f-9b73-ed95df821302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d1b14d3-56da-4c6d-9451-1ad4b928c8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30345c3-f5e4-4bf8-8a4d-7ab957240cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d1b14d3-56da-4c6d-9451-1ad4b928c8c7",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "67a6191e-b5e3-4e9d-b44e-d1d8be9d1062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "915642e8-236d-4c2d-ad5e-43eb40b2aab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a6191e-b5e3-4e9d-b44e-d1d8be9d1062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "633c758b-810e-45ed-ab14-8f1c7d73232b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a6191e-b5e3-4e9d-b44e-d1d8be9d1062",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "5fe9a4b7-32ac-4897-974a-89709f723188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "2ee7590a-2ebd-4286-bda5-e309ce067cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe9a4b7-32ac-4897-974a-89709f723188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a3226db-6f26-402b-bb3d-b767f62bbece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe9a4b7-32ac-4897-974a-89709f723188",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "43c67d46-8126-405f-bb5b-e17d6f73dfaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "4fe5d3e0-2312-4d5c-a1a2-ae27d275a3e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c67d46-8126-405f-bb5b-e17d6f73dfaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d74658a1-3d13-49ae-8bf9-3ded5986188f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c67d46-8126-405f-bb5b-e17d6f73dfaf",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "07fdb149-d833-4d1f-b51b-a37f43f839e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "3c373ab6-d51a-4a7a-bed0-c47579adaea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07fdb149-d833-4d1f-b51b-a37f43f839e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86b8eb95-1f49-4f92-bfbb-abdebc0e4281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07fdb149-d833-4d1f-b51b-a37f43f839e7",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "a3eaae49-f3cc-44f6-abc9-30914df5cb27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "dcbf3753-9961-40fd-b78d-3ddb838ca159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3eaae49-f3cc-44f6-abc9-30914df5cb27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1db57d4-f570-447d-86ed-e1f3311d03d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3eaae49-f3cc-44f6-abc9-30914df5cb27",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "6cf7101c-7c0a-447c-bf43-3ebb4ef289d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "52e54df6-4586-414b-942c-75519137beec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf7101c-7c0a-447c-bf43-3ebb4ef289d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9611aca2-1217-424e-97ff-5689e88bf956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf7101c-7c0a-447c-bf43-3ebb4ef289d7",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "40c3c50d-d213-4ea8-86cb-bc63e2eeb5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "111c745c-0cb2-448a-9d2d-be79578ddfc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c3c50d-d213-4ea8-86cb-bc63e2eeb5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9af86c7c-c029-4b7b-b89a-32d9c289305b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c3c50d-d213-4ea8-86cb-bc63e2eeb5eb",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "f2cd9f03-1d48-402f-a7f5-98ed527eded9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "9519d168-c016-49b8-8bbf-1bc6beaa1c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2cd9f03-1d48-402f-a7f5-98ed527eded9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ec17bb7-a901-49db-9b5f-eb626dc076ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2cd9f03-1d48-402f-a7f5-98ed527eded9",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "efc4403b-40e2-4b29-9760-9f86c6f05d2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "11281e93-5f31-441a-803c-35ebb2e2be9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efc4403b-40e2-4b29-9760-9f86c6f05d2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44ce71a0-e9c1-4be9-bd38-2c3d6cf3597f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efc4403b-40e2-4b29-9760-9f86c6f05d2e",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "02d4eee4-9a89-46ce-9adf-d3856b0a2a62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "990e1a89-e3c6-4762-a4d8-6852687d40d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02d4eee4-9a89-46ce-9adf-d3856b0a2a62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f09f69d2-ef49-4700-a15e-05309c621c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02d4eee4-9a89-46ce-9adf-d3856b0a2a62",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "89825fa8-eaeb-46eb-9c08-6093b42e2e34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "429d373d-b390-479f-affe-34b0c3f64e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89825fa8-eaeb-46eb-9c08-6093b42e2e34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d211f5-54ec-4b76-b417-1cabf5b84cec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89825fa8-eaeb-46eb-9c08-6093b42e2e34",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "8823a292-1aaa-443d-8d2e-ba75859ed4e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "64931a71-0dc0-4e20-9711-d83c7ebc36b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8823a292-1aaa-443d-8d2e-ba75859ed4e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc16d0ff-ac39-486d-b2a0-17fd98253e48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8823a292-1aaa-443d-8d2e-ba75859ed4e8",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "fc58f066-fe20-485c-bf3e-21187a1b4b42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "73c9d755-eccb-41be-8d22-dbadb83b7a1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc58f066-fe20-485c-bf3e-21187a1b4b42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a61e9527-b720-4c04-b0e4-d72f0b5d2581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc58f066-fe20-485c-bf3e-21187a1b4b42",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "417a5ff7-c26a-4e8e-a2ac-3155328ee340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "a26a537c-d766-4d72-8f0c-4f094cab8757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417a5ff7-c26a-4e8e-a2ac-3155328ee340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4fecfa2-1a9d-42e9-ba08-69f0d185b5ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417a5ff7-c26a-4e8e-a2ac-3155328ee340",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "d48fdc70-2121-4aa2-893f-7b9ce59e7c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "c97e2191-3d58-450c-9fbc-4e6a57bac5fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d48fdc70-2121-4aa2-893f-7b9ce59e7c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96b68ea-f01c-4667-bf24-7fb971785ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d48fdc70-2121-4aa2-893f-7b9ce59e7c48",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "062daea8-2651-4ce4-9b47-429b62e30286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "080178b8-de15-4df6-8c4a-3eb4288c56ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062daea8-2651-4ce4-9b47-429b62e30286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8690e697-dba5-4489-adec-b5a33a2a427a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062daea8-2651-4ce4-9b47-429b62e30286",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "49c18e14-aad2-4724-8435-c1c4b26a6667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "1a49ca35-3c8c-43f6-ae10-71c630577ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c18e14-aad2-4724-8435-c1c4b26a6667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa299c61-8c51-4199-9010-c36cf28f9de8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c18e14-aad2-4724-8435-c1c4b26a6667",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "b4b5d11b-e771-4556-8fdf-18157e65c409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "843133aa-e6b5-4fbf-945e-c32ef8d7bf2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b5d11b-e771-4556-8fdf-18157e65c409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea566fad-c130-479f-b1b1-3548b3688b70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b5d11b-e771-4556-8fdf-18157e65c409",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "2bee1ee4-7c9b-4148-8d20-14173dfdf23d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "9a2ac8b8-8319-4296-962d-4544b811e408",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bee1ee4-7c9b-4148-8d20-14173dfdf23d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5855331-f8d3-4e8c-b806-dd89cb1b3a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bee1ee4-7c9b-4148-8d20-14173dfdf23d",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "cd1b469a-a9da-4024-802d-58119572f807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "4c55f4bf-899b-41ac-be82-257c1083ecf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1b469a-a9da-4024-802d-58119572f807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c30a9dd5-d8cd-43e2-abd4-8ee76f4acb2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1b469a-a9da-4024-802d-58119572f807",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "71d144c2-897d-417c-b3cd-ae35ee88b583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "33026c5c-f73a-45b8-a070-4178827184a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71d144c2-897d-417c-b3cd-ae35ee88b583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f01dac0-e5ec-4eda-92ae-2dc029770485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71d144c2-897d-417c-b3cd-ae35ee88b583",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "a3a92a45-61b5-4e67-9663-91597e935652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "ebd6b8e6-64c7-43e0-9072-13cd0027ae88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a92a45-61b5-4e67-9663-91597e935652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c694a9c-11f1-4262-8cf8-662455319ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a92a45-61b5-4e67-9663-91597e935652",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "1359cd35-abd1-4ac5-80d0-4d9dc52646ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "28b1760d-fb50-435b-884c-5c2f7e8074df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1359cd35-abd1-4ac5-80d0-4d9dc52646ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "789de731-6ceb-4252-a3f4-684f17643a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1359cd35-abd1-4ac5-80d0-4d9dc52646ca",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "ab80f797-abfe-4320-b4f7-44571974cf8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "5df1d67a-e314-41fc-80c5-3979d24544ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab80f797-abfe-4320-b4f7-44571974cf8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca8dbd1-a0ff-456c-b56a-b0d68b4fde35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab80f797-abfe-4320-b4f7-44571974cf8c",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "795c5ae7-d26e-4375-9cf5-bf8a5883b2b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "1b2b512d-f7af-4a6b-add8-abc621e3640a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "795c5ae7-d26e-4375-9cf5-bf8a5883b2b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "008109dd-346e-43c5-8ee0-2f66e6dc2e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "795c5ae7-d26e-4375-9cf5-bf8a5883b2b3",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "ee80a0d9-b4f3-4216-86ac-f51404b6e4e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "2a92b70f-10e2-4660-bc4d-12244afcfab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee80a0d9-b4f3-4216-86ac-f51404b6e4e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5f23b5-177a-4358-b032-8d163880fd28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee80a0d9-b4f3-4216-86ac-f51404b6e4e4",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        },
        {
            "id": "772a8e55-07c0-4932-a800-9a0387a92a1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "compositeImage": {
                "id": "3a67c18b-2e09-4ea0-9448-134075c7c622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772a8e55-07c0-4932-a800-9a0387a92a1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "447d5f0e-652c-48d5-81fb-c36062f19647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772a8e55-07c0-4932-a800-9a0387a92a1d",
                    "LayerId": "7d35ed18-3815-440c-9c71-854c7ecc6651"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7d35ed18-3815-440c-9c71-854c7ecc6651",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f10473a6-de40-439a-81b1-5502f24c931a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}