{
    "id": "5fc54534-4562-4a48-87bb-6d7248dd72ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_alchemytable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42cf4fea-85bb-4d9a-85b1-d868cbdfc505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fc54534-4562-4a48-87bb-6d7248dd72ad",
            "compositeImage": {
                "id": "c5799321-2c26-4a1e-8245-33c0f5d07138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42cf4fea-85bb-4d9a-85b1-d868cbdfc505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b02131-0a59-4120-8e62-1db760fe2d67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42cf4fea-85bb-4d9a-85b1-d868cbdfc505",
                    "LayerId": "f04ae65d-562b-49a4-b60e-c77b4b944e36"
                }
            ]
        }
    ],
    "gridX": 40,
    "gridY": 72,
    "height": 64,
    "layers": [
        {
            "id": "f04ae65d-562b-49a4-b60e-c77b4b944e36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fc54534-4562-4a48-87bb-6d7248dd72ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 56
}