{
    "id": "fe36a10d-ba9b-4f66-b83e-cfe4b3f1faa2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27d6b3a1-0b99-47b1-b41a-8f4bcd6d1ee1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe36a10d-ba9b-4f66-b83e-cfe4b3f1faa2",
            "compositeImage": {
                "id": "d705d235-3dd2-42b5-a11b-ea11b17cd388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d6b3a1-0b99-47b1-b41a-8f4bcd6d1ee1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0993b220-de9a-4778-867d-784da0b8572d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d6b3a1-0b99-47b1-b41a-8f4bcd6d1ee1",
                    "LayerId": "7a50c874-9b49-4516-a991-36542d6a3441"
                }
            ]
        },
        {
            "id": "01ffcb34-a16e-4ec2-b173-df666730054f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe36a10d-ba9b-4f66-b83e-cfe4b3f1faa2",
            "compositeImage": {
                "id": "1290784f-0fef-4c78-a62d-8f912bfb46b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ffcb34-a16e-4ec2-b173-df666730054f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "559d6e55-29a3-4603-b673-d93c0e5355ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ffcb34-a16e-4ec2-b173-df666730054f",
                    "LayerId": "7a50c874-9b49-4516-a991-36542d6a3441"
                }
            ]
        },
        {
            "id": "2b8ff98c-f1e2-48d9-bdde-da9a72945d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe36a10d-ba9b-4f66-b83e-cfe4b3f1faa2",
            "compositeImage": {
                "id": "82825795-6a8b-443f-a961-a96a73d56477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b8ff98c-f1e2-48d9-bdde-da9a72945d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1758211-0fd0-4513-a8d1-4b3782f71354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b8ff98c-f1e2-48d9-bdde-da9a72945d6b",
                    "LayerId": "7a50c874-9b49-4516-a991-36542d6a3441"
                }
            ]
        },
        {
            "id": "140768b9-3ff8-4dbc-a827-ec00dfdf8acb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe36a10d-ba9b-4f66-b83e-cfe4b3f1faa2",
            "compositeImage": {
                "id": "f381d9f8-b16f-4e83-a10b-97f9c6e354e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "140768b9-3ff8-4dbc-a827-ec00dfdf8acb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08475046-e777-45a7-b6fe-08a7b5fc554a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "140768b9-3ff8-4dbc-a827-ec00dfdf8acb",
                    "LayerId": "7a50c874-9b49-4516-a991-36542d6a3441"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7a50c874-9b49-4516-a991-36542d6a3441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe36a10d-ba9b-4f66-b83e-cfe4b3f1faa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}