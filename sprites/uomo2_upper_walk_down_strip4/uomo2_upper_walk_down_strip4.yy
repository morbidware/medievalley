{
    "id": "3a1147a8-3bfa-47be-90f5-dbb77b78dcb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73ee3cb5-c71a-43a5-a090-0cae5846ab6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1147a8-3bfa-47be-90f5-dbb77b78dcb2",
            "compositeImage": {
                "id": "051be6af-d36a-499b-8553-77fb9aa6317a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73ee3cb5-c71a-43a5-a090-0cae5846ab6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b42ce4a-ee06-4d87-9578-89532718de75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73ee3cb5-c71a-43a5-a090-0cae5846ab6b",
                    "LayerId": "045de2a9-5064-4461-b801-0d5ed8e8d849"
                }
            ]
        },
        {
            "id": "f17b335a-219b-497c-a014-30b996194435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1147a8-3bfa-47be-90f5-dbb77b78dcb2",
            "compositeImage": {
                "id": "c0e77cff-b390-4ebd-b968-6aa31896115a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f17b335a-219b-497c-a014-30b996194435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a229d063-0556-4371-bd41-d216353f568b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f17b335a-219b-497c-a014-30b996194435",
                    "LayerId": "045de2a9-5064-4461-b801-0d5ed8e8d849"
                }
            ]
        },
        {
            "id": "7fb93350-690c-42e5-962d-2ee6eb8785d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1147a8-3bfa-47be-90f5-dbb77b78dcb2",
            "compositeImage": {
                "id": "be999e71-9515-4648-873a-a9a1bc0efd73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb93350-690c-42e5-962d-2ee6eb8785d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ca2b8a4-c350-4af0-8eaf-7e4f9987116c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb93350-690c-42e5-962d-2ee6eb8785d7",
                    "LayerId": "045de2a9-5064-4461-b801-0d5ed8e8d849"
                }
            ]
        },
        {
            "id": "cb7afefa-52bb-4520-9a41-9bff9cb5b274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1147a8-3bfa-47be-90f5-dbb77b78dcb2",
            "compositeImage": {
                "id": "c0423c32-d0d8-4084-8e61-9254b52d5f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7afefa-52bb-4520-9a41-9bff9cb5b274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c694fb-629b-4300-9df3-cc3267a1c8cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7afefa-52bb-4520-9a41-9bff9cb5b274",
                    "LayerId": "045de2a9-5064-4461-b801-0d5ed8e8d849"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "045de2a9-5064-4461-b801-0d5ed8e8d849",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a1147a8-3bfa-47be-90f5-dbb77b78dcb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}