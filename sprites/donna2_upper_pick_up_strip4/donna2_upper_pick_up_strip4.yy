{
    "id": "d687d8ea-3f19-433d-acb0-a67516f57979",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65e2fbe1-ad5a-4e04-8975-e9f5c2aa1988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d687d8ea-3f19-433d-acb0-a67516f57979",
            "compositeImage": {
                "id": "31857216-bf80-4353-af3b-2e52e51bd437",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e2fbe1-ad5a-4e04-8975-e9f5c2aa1988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e9c56a-739e-49e2-b819-b3fee14539fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e2fbe1-ad5a-4e04-8975-e9f5c2aa1988",
                    "LayerId": "f29e1a90-85cc-4ca4-887d-1dfb1f8ce536"
                }
            ]
        },
        {
            "id": "ded7904d-f04c-4606-a24b-7fd27e2b8aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d687d8ea-3f19-433d-acb0-a67516f57979",
            "compositeImage": {
                "id": "03306bd0-8f35-4998-98c0-99713fd4fc8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded7904d-f04c-4606-a24b-7fd27e2b8aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff30e900-276b-4cd0-88f4-2d6179fc0297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded7904d-f04c-4606-a24b-7fd27e2b8aac",
                    "LayerId": "f29e1a90-85cc-4ca4-887d-1dfb1f8ce536"
                }
            ]
        },
        {
            "id": "df312132-c745-4a53-8384-042495eded07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d687d8ea-3f19-433d-acb0-a67516f57979",
            "compositeImage": {
                "id": "ae8f6cee-4ad0-42f8-875f-b23c5fa5561e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df312132-c745-4a53-8384-042495eded07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82345c4e-433b-402c-940f-e02f429fad5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df312132-c745-4a53-8384-042495eded07",
                    "LayerId": "f29e1a90-85cc-4ca4-887d-1dfb1f8ce536"
                }
            ]
        },
        {
            "id": "d92a4e79-88d9-463e-8304-8805a31991e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d687d8ea-3f19-433d-acb0-a67516f57979",
            "compositeImage": {
                "id": "502f6df3-f896-44c9-90c5-b1e7a9858a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d92a4e79-88d9-463e-8304-8805a31991e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f99cb25-d35c-4317-8f19-e9086dfa817f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d92a4e79-88d9-463e-8304-8805a31991e3",
                    "LayerId": "f29e1a90-85cc-4ca4-887d-1dfb1f8ce536"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f29e1a90-85cc-4ca4-887d-1dfb1f8ce536",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d687d8ea-3f19-433d-acb0-a67516f57979",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}