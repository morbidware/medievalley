{
    "id": "032f0b8d-86cc-4df0-b41f-40ec8a8e77d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_lake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76daae75-0720-464c-91ef-dd0c22c5a6c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "032f0b8d-86cc-4df0-b41f-40ec8a8e77d6",
            "compositeImage": {
                "id": "3178418c-a884-46a9-92d0-25a1ef8638a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76daae75-0720-464c-91ef-dd0c22c5a6c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0e0f789-03c8-4099-9446-03a6e5f964b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76daae75-0720-464c-91ef-dd0c22c5a6c1",
                    "LayerId": "d6b3af66-b306-4943-bc47-811151cb1abb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d6b3af66-b306-4943-bc47-811151cb1abb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "032f0b8d-86cc-4df0-b41f-40ec8a8e77d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}