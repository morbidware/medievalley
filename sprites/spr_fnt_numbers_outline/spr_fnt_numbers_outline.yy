{
    "id": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_numbers_outline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4399035e-959c-48cd-ba52-d0c458848a98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "8207736c-c32b-4c50-9b1e-b3dd1212c88e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4399035e-959c-48cd-ba52-d0c458848a98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b356c9-7183-40ec-8a50-3c8d9953d52a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4399035e-959c-48cd-ba52-d0c458848a98",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "d2a1a8d1-f1cb-4e1c-a127-ed10b6c71468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "44cd8095-a301-4e8e-9b54-bd9cda0af9e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a1a8d1-f1cb-4e1c-a127-ed10b6c71468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7465a6c6-1e15-41b3-91cf-f21072616c49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a1a8d1-f1cb-4e1c-a127-ed10b6c71468",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "315861a7-0aa6-4351-b24b-40a0284451b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "e03b7bc3-a128-4c37-9ea4-9b2e2f924137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "315861a7-0aa6-4351-b24b-40a0284451b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3664416-adab-4c0b-9c94-e80a5d5bf092",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "315861a7-0aa6-4351-b24b-40a0284451b2",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "9939e06a-befe-4170-bb27-9604dff91a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "65fee69f-44f6-4455-876d-8375d6cbb7a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9939e06a-befe-4170-bb27-9604dff91a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "576ebe12-8c73-40a9-b53d-5e01dad69c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9939e06a-befe-4170-bb27-9604dff91a3b",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "708d2917-4320-4288-86ae-5eb9d3da1093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "7b66f06d-a9e8-4eca-b027-472e4ae1f0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708d2917-4320-4288-86ae-5eb9d3da1093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c01610f8-c3dd-4f32-96a2-67faf42eebdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708d2917-4320-4288-86ae-5eb9d3da1093",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "4d6db2fa-56e2-4e2a-83f0-77563a87ca83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "c6cc4da8-8a78-4a49-9a37-78dab163cdcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d6db2fa-56e2-4e2a-83f0-77563a87ca83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38cce4fd-66d0-444e-846f-8629b5ac0016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d6db2fa-56e2-4e2a-83f0-77563a87ca83",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "acdfd56c-0746-47fe-bd64-1f806cf54ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "37e97f87-d996-4d94-a32d-415214f827b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acdfd56c-0746-47fe-bd64-1f806cf54ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bce1a32-856b-40f1-81de-6220cd200f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acdfd56c-0746-47fe-bd64-1f806cf54ba8",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "dfc0b9ed-233f-4496-8f9f-bca9fead67e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "12bde6fb-c364-4ce1-a9f4-17270f71fc50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfc0b9ed-233f-4496-8f9f-bca9fead67e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecad9927-f238-444f-b9b2-77f372535243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfc0b9ed-233f-4496-8f9f-bca9fead67e6",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "f8c1f44e-2de9-44ce-9e8c-08ead8f38705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "61a56dad-3662-4213-a177-bf439dea2688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8c1f44e-2de9-44ce-9e8c-08ead8f38705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a88687-3bc8-433a-93ac-6e8c5b9d4ad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8c1f44e-2de9-44ce-9e8c-08ead8f38705",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "02cb0f2b-48e3-40bb-80fe-8e8718f95c33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "66a32922-e4dc-45f7-a106-4f458ac5caa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02cb0f2b-48e3-40bb-80fe-8e8718f95c33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bf4832f-66ba-4527-8789-af9ddc21a6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02cb0f2b-48e3-40bb-80fe-8e8718f95c33",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "98014996-ff78-4eae-ab93-e5acb0adf5f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "d75fa552-3a20-4139-9a62-118d91f214fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98014996-ff78-4eae-ab93-e5acb0adf5f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e78e4b82-dedc-4420-bbdf-8b8e1de8630a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98014996-ff78-4eae-ab93-e5acb0adf5f6",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        },
        {
            "id": "e42b4c24-a6b5-442e-8d1c-17ae35dcb915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "compositeImage": {
                "id": "cf890cdc-023a-404d-b18d-8393a14f3f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e42b4c24-a6b5-442e-8d1c-17ae35dcb915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d196215-6300-451f-bd49-7f92a4b6ca73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e42b4c24-a6b5-442e-8d1c-17ae35dcb915",
                    "LayerId": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "f5fd9e06-99b3-4b6c-9fed-cbe1bde26f08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e75212a-bb60-4f66-a0dc-8f0c0d446469",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}