{
    "id": "f4850ed4-3e46-4566-ae26-78a66679b79b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_edge_t",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ddd4832-1198-4d10-b6fe-1129c71888e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4850ed4-3e46-4566-ae26-78a66679b79b",
            "compositeImage": {
                "id": "31acb6b7-2952-4358-b664-c0a49a013f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ddd4832-1198-4d10-b6fe-1129c71888e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de46fe01-e022-4430-aee8-5e75b5b4b397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ddd4832-1198-4d10-b6fe-1129c71888e3",
                    "LayerId": "65ffe49a-f7a5-4099-8cf3-fa5b0d675df9"
                }
            ]
        },
        {
            "id": "dec17b4d-0d70-44dd-baae-67ceb820e63d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4850ed4-3e46-4566-ae26-78a66679b79b",
            "compositeImage": {
                "id": "4505bec0-1f93-46cf-ac43-dd791c5e65ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec17b4d-0d70-44dd-baae-67ceb820e63d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a629212a-141a-48b0-b963-5890ace98273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec17b4d-0d70-44dd-baae-67ceb820e63d",
                    "LayerId": "65ffe49a-f7a5-4099-8cf3-fa5b0d675df9"
                }
            ]
        },
        {
            "id": "623e6353-f337-4f2d-871a-ee413d0f0d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4850ed4-3e46-4566-ae26-78a66679b79b",
            "compositeImage": {
                "id": "48bc13b6-fd89-4310-8656-e2b9e5b91e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623e6353-f337-4f2d-871a-ee413d0f0d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06e6e45-01d1-4b27-bd63-07baa1fab0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623e6353-f337-4f2d-871a-ee413d0f0d5d",
                    "LayerId": "65ffe49a-f7a5-4099-8cf3-fa5b0d675df9"
                }
            ]
        },
        {
            "id": "6fe3af64-62bd-4e05-a3c6-57ff6466cea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4850ed4-3e46-4566-ae26-78a66679b79b",
            "compositeImage": {
                "id": "678ad39a-a1f9-4ee9-9e12-b395c1782a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe3af64-62bd-4e05-a3c6-57ff6466cea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c460819-7608-4027-bc00-54ed9ea85d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe3af64-62bd-4e05-a3c6-57ff6466cea1",
                    "LayerId": "65ffe49a-f7a5-4099-8cf3-fa5b0d675df9"
                }
            ]
        },
        {
            "id": "eb99bf25-fd9e-415f-ac8d-e7882e084e20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4850ed4-3e46-4566-ae26-78a66679b79b",
            "compositeImage": {
                "id": "898855bc-1d84-43c3-a8e5-84646c8b92c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb99bf25-fd9e-415f-ac8d-e7882e084e20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43ce611-2f0f-4048-be7b-abfbfcb9cf4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb99bf25-fd9e-415f-ac8d-e7882e084e20",
                    "LayerId": "65ffe49a-f7a5-4099-8cf3-fa5b0d675df9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "65ffe49a-f7a5-4099-8cf3-fa5b0d675df9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4850ed4-3e46-4566-ae26-78a66679b79b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}