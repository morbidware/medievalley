{
    "id": "c13e338c-bf72-465c-92d2-cc52341f0f2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 5,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d1669ec-1f26-40fb-909d-3a96e49a8206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c13e338c-bf72-465c-92d2-cc52341f0f2a",
            "compositeImage": {
                "id": "a9219acf-d462-4d2d-a1a8-681450f14bb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d1669ec-1f26-40fb-909d-3a96e49a8206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75e8ef06-b052-4651-b23e-d55845b8344c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d1669ec-1f26-40fb-909d-3a96e49a8206",
                    "LayerId": "8a513fcd-4762-4089-ab6d-0252b380a919"
                }
            ]
        },
        {
            "id": "bb9dd793-cd99-4430-a4d5-39a7083f1b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c13e338c-bf72-465c-92d2-cc52341f0f2a",
            "compositeImage": {
                "id": "08427b36-365a-42fe-b13c-59a83199512a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9dd793-cd99-4430-a4d5-39a7083f1b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f0c70b-4d56-4f3b-8763-3dc87abe74ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9dd793-cd99-4430-a4d5-39a7083f1b60",
                    "LayerId": "8a513fcd-4762-4089-ab6d-0252b380a919"
                }
            ]
        },
        {
            "id": "4efb5710-4b09-4bf1-b5f9-617503f4e4a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c13e338c-bf72-465c-92d2-cc52341f0f2a",
            "compositeImage": {
                "id": "b4da19fe-7c33-4931-a4cb-73a70b84afd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4efb5710-4b09-4bf1-b5f9-617503f4e4a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b500056-9537-4513-9a3c-c3ab8cd23fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4efb5710-4b09-4bf1-b5f9-617503f4e4a6",
                    "LayerId": "8a513fcd-4762-4089-ab6d-0252b380a919"
                }
            ]
        },
        {
            "id": "57fc1193-09b3-4aba-a85a-efcfd46a04da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c13e338c-bf72-465c-92d2-cc52341f0f2a",
            "compositeImage": {
                "id": "d94e7f89-36ec-4f05-8da4-ca69be20b7c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57fc1193-09b3-4aba-a85a-efcfd46a04da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5a4dc7-d131-4172-87aa-2014ea4813e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57fc1193-09b3-4aba-a85a-efcfd46a04da",
                    "LayerId": "8a513fcd-4762-4089-ab6d-0252b380a919"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a513fcd-4762-4089-ab6d-0252b380a919",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c13e338c-bf72-465c-92d2-cc52341f0f2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}