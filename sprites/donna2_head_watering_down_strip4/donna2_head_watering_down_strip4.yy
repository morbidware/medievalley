{
    "id": "0727d939-46f2-475f-8fb7-a7e3578fc9e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "611473b0-ca1f-45eb-8cfe-1edd15e0b9c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0727d939-46f2-475f-8fb7-a7e3578fc9e6",
            "compositeImage": {
                "id": "c15ba098-5df7-46e8-ace1-6ef11836a706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "611473b0-ca1f-45eb-8cfe-1edd15e0b9c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39393f9b-29f4-4f8d-81f4-4c6f98f4c936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "611473b0-ca1f-45eb-8cfe-1edd15e0b9c9",
                    "LayerId": "af08a42e-c1e5-4a5a-9abb-727691b206c7"
                }
            ]
        },
        {
            "id": "0dec6393-c871-40eb-89c4-21077f8364ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0727d939-46f2-475f-8fb7-a7e3578fc9e6",
            "compositeImage": {
                "id": "f4b969f8-9216-4a30-9d7b-6c87f77bdc5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dec6393-c871-40eb-89c4-21077f8364ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1cc94d7-e8c6-4340-8dbc-2d8f8b3d4a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dec6393-c871-40eb-89c4-21077f8364ce",
                    "LayerId": "af08a42e-c1e5-4a5a-9abb-727691b206c7"
                }
            ]
        },
        {
            "id": "0118989b-e220-4370-807b-d2eea73c9521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0727d939-46f2-475f-8fb7-a7e3578fc9e6",
            "compositeImage": {
                "id": "c4a142a1-35b6-4681-bc74-2c371be782f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0118989b-e220-4370-807b-d2eea73c9521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54bf557c-dbc7-45c8-994e-ea27859a1ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0118989b-e220-4370-807b-d2eea73c9521",
                    "LayerId": "af08a42e-c1e5-4a5a-9abb-727691b206c7"
                }
            ]
        },
        {
            "id": "1bd2d07d-7fdd-464c-820c-4d88c09023d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0727d939-46f2-475f-8fb7-a7e3578fc9e6",
            "compositeImage": {
                "id": "2cab9219-2556-43e2-9669-15fef71be56b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bd2d07d-7fdd-464c-820c-4d88c09023d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d85417-fafe-4ea6-be00-bfc69d45528d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bd2d07d-7fdd-464c-820c-4d88c09023d6",
                    "LayerId": "af08a42e-c1e5-4a5a-9abb-727691b206c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "af08a42e-c1e5-4a5a-9abb-727691b206c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0727d939-46f2-475f-8fb7-a7e3578fc9e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}