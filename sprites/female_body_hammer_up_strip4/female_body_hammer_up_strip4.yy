{
    "id": "05e3370f-62ca-426d-b3e9-1e22b0f0c984",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 6,
    "bbox_right": 9,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a783492-a8b6-484f-ab10-b2b06bcd860b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05e3370f-62ca-426d-b3e9-1e22b0f0c984",
            "compositeImage": {
                "id": "eaff975c-9c27-452c-8c7c-654327f63e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a783492-a8b6-484f-ab10-b2b06bcd860b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b02625c-254f-48da-85f0-78bba34bbfde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a783492-a8b6-484f-ab10-b2b06bcd860b",
                    "LayerId": "6e19da94-1beb-43b8-b1d7-4533c4d24315"
                }
            ]
        },
        {
            "id": "f069d96f-e770-4eae-a3ca-6ec8e07f0ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05e3370f-62ca-426d-b3e9-1e22b0f0c984",
            "compositeImage": {
                "id": "f597c4d5-9b5f-473f-84ec-f7bd2cfc70bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f069d96f-e770-4eae-a3ca-6ec8e07f0ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dfa06b2-3c84-4635-86f8-6d036a4ff64a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f069d96f-e770-4eae-a3ca-6ec8e07f0ffe",
                    "LayerId": "6e19da94-1beb-43b8-b1d7-4533c4d24315"
                }
            ]
        },
        {
            "id": "0a044b83-dc05-44fb-8dae-e1616dde7cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05e3370f-62ca-426d-b3e9-1e22b0f0c984",
            "compositeImage": {
                "id": "f0204522-c43a-4291-b878-f3d4ca23b111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a044b83-dc05-44fb-8dae-e1616dde7cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ea1fedf-57bc-41d5-9216-14c3d6e22779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a044b83-dc05-44fb-8dae-e1616dde7cda",
                    "LayerId": "6e19da94-1beb-43b8-b1d7-4533c4d24315"
                }
            ]
        },
        {
            "id": "28852d86-5f94-49f4-ad37-801f7bf1bdd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05e3370f-62ca-426d-b3e9-1e22b0f0c984",
            "compositeImage": {
                "id": "3a4dc7bd-0ab7-4ba3-a150-1af45f802247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28852d86-5f94-49f4-ad37-801f7bf1bdd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d6f9d8-a442-455b-9e62-fab5e5daa0d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28852d86-5f94-49f4-ad37-801f7bf1bdd6",
                    "LayerId": "6e19da94-1beb-43b8-b1d7-4533c4d24315"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6e19da94-1beb-43b8-b1d7-4533c4d24315",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05e3370f-62ca-426d-b3e9-1e22b0f0c984",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}