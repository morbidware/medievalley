{
    "id": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_ground_dust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f56f5f33-c7d4-4b59-9e17-30422960d4ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "a1a1ad3d-ad35-4415-b532-664fa042c75a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56f5f33-c7d4-4b59-9e17-30422960d4ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3760e37a-9d8c-4c0e-b579-4837700c65e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56f5f33-c7d4-4b59-9e17-30422960d4ef",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "a6eaf1a5-f738-48fd-bcef-f5f8127ca5e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "5470165f-2dd7-4830-9fbb-baa6242c2d58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6eaf1a5-f738-48fd-bcef-f5f8127ca5e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8cf675-035e-4dc1-b17f-078a545a0071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6eaf1a5-f738-48fd-bcef-f5f8127ca5e5",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "715461ca-d8ed-4879-829f-8e0e54feba88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "622e0df5-646f-442b-9f7b-9cbf6af92b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "715461ca-d8ed-4879-829f-8e0e54feba88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "699dd07b-b7cf-409e-bd16-d0b6d23a9fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "715461ca-d8ed-4879-829f-8e0e54feba88",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "a5d7ad39-b7e8-43dc-8416-997369f916d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "fbbe5995-b619-445e-941d-e20f7d3f5eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5d7ad39-b7e8-43dc-8416-997369f916d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6946e020-2c75-4f70-907a-578f67db02e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5d7ad39-b7e8-43dc-8416-997369f916d0",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "14d0904a-8798-457d-9d51-f489aecad7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "5702d7fa-e45d-4b9a-8c58-82f5e0338e80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14d0904a-8798-457d-9d51-f489aecad7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c8be65-5e91-4bbc-aff2-8a32f6df61d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14d0904a-8798-457d-9d51-f489aecad7b8",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "af989b5f-0d7d-4e1d-a0c2-4b4ad64af97c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "167b5fe7-8d89-478c-aa48-f7b389ef61e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af989b5f-0d7d-4e1d-a0c2-4b4ad64af97c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64dd4c8f-312c-480d-b1b0-a5606fdc0141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af989b5f-0d7d-4e1d-a0c2-4b4ad64af97c",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "65bcff49-c4ae-40e4-842c-b2e5751fffd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "0f2cd8c4-b0ce-431b-b4d2-58b68d50c294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65bcff49-c4ae-40e4-842c-b2e5751fffd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00219ef2-aa0a-4339-b083-c8bd379917b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65bcff49-c4ae-40e4-842c-b2e5751fffd1",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "42274273-8e5d-446b-880e-46fb9216b127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "32c2f4f7-7975-4363-89a4-bd78786ae51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42274273-8e5d-446b-880e-46fb9216b127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1753a575-8583-4aee-9cd3-afe7e91f4cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42274273-8e5d-446b-880e-46fb9216b127",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "eb730ead-5a74-45f4-922c-1a2bcde64773",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "755dbb56-6838-49fa-b9f3-03e231a7403b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb730ead-5a74-45f4-922c-1a2bcde64773",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89c90bda-05af-4ee4-b42a-7b390695ea31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb730ead-5a74-45f4-922c-1a2bcde64773",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "1cf20061-ad9f-4fd1-bb00-4c55bec18727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "e840396c-4ced-4f8a-b116-14051c42c468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cf20061-ad9f-4fd1-bb00-4c55bec18727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a75026-0891-4721-8acb-44dfd6cf8067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cf20061-ad9f-4fd1-bb00-4c55bec18727",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "843eb9ef-d987-40a6-8565-abaa3634b133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "3963f23c-0368-4bfd-a4ba-0aca2b80d7fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843eb9ef-d987-40a6-8565-abaa3634b133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3130e2d-bfbf-4f30-ba55-3620e83351ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843eb9ef-d987-40a6-8565-abaa3634b133",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "a6fe65bd-f8a2-4d5e-8635-beb78e2bab06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "5061899e-fa9d-400a-a573-d3389dc5e531",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6fe65bd-f8a2-4d5e-8635-beb78e2bab06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7fe2ade-6816-4f20-b806-854e107241cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6fe65bd-f8a2-4d5e-8635-beb78e2bab06",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "bd0ff160-3d17-44b5-adbb-4c9585d59f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "05899ac5-12ac-4561-b181-4206c1af9c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd0ff160-3d17-44b5-adbb-4c9585d59f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f62e503-ebab-43c6-96cb-bec1ec719414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd0ff160-3d17-44b5-adbb-4c9585d59f6f",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "b77fb7cb-f366-48e2-8b00-53115b8e9131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "1bc792bf-2b0c-404b-9ef9-b68596f8307f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77fb7cb-f366-48e2-8b00-53115b8e9131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38209ad6-85b1-44e5-a1e5-d0c1645c4525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77fb7cb-f366-48e2-8b00-53115b8e9131",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "dce078bd-86c8-4010-b370-a1aea273936b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "480951fe-3476-4566-9288-d4c7dcab4de3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dce078bd-86c8-4010-b370-a1aea273936b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73baca23-d782-47a6-bfa2-184721ca7183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dce078bd-86c8-4010-b370-a1aea273936b",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "d2595986-ac07-47ce-9d3f-a4c01c549bff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "ea648389-9993-4df3-84d1-7c4e825e4831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2595986-ac07-47ce-9d3f-a4c01c549bff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da772347-cf93-4254-aeb6-0165a4b27ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2595986-ac07-47ce-9d3f-a4c01c549bff",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        },
        {
            "id": "f0ca9008-313f-4ab0-b33d-2b3147c220c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "compositeImage": {
                "id": "a571de09-78e6-4a4a-8c8a-22ae0602b8de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ca9008-313f-4ab0-b33d-2b3147c220c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d10556b4-4525-4879-a528-f143f2bf8226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ca9008-313f-4ab0-b33d-2b3147c220c4",
                    "LayerId": "635d97e0-c292-4487-acc3-ee4c690759cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "635d97e0-c292-4487-acc3-ee4c690759cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfc149c3-f72a-410b-8032-9a1b5c00100d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 0.5,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 15
}