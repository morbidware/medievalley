{
    "id": "d6ddc1f8-1a7e-4226-96dc-1cddff927455",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_basefence",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3f5b033-b3ba-45c3-bc20-ac5e6a527a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6ddc1f8-1a7e-4226-96dc-1cddff927455",
            "compositeImage": {
                "id": "bd97eab1-e097-43f1-a69d-a445c36265f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f5b033-b3ba-45c3-bc20-ac5e6a527a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa3e855-9042-4999-b1c1-e37024b3e065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f5b033-b3ba-45c3-bc20-ac5e6a527a69",
                    "LayerId": "7d412c0f-3df3-488f-b340-b21689e1631b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7d412c0f-3df3-488f-b340-b21689e1631b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6ddc1f8-1a7e-4226-96dc-1cddff927455",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 11
}