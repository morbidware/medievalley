{
    "id": "e685e3d3-d4f7-4f28-8905-df4521a60969",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24541a3d-d900-49b8-b4f8-fe2de42607d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e685e3d3-d4f7-4f28-8905-df4521a60969",
            "compositeImage": {
                "id": "97dfab55-f54d-4ac8-81ba-2866f6de11c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24541a3d-d900-49b8-b4f8-fe2de42607d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1174b487-408e-4720-a56c-555c546183c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24541a3d-d900-49b8-b4f8-fe2de42607d1",
                    "LayerId": "386837e8-8998-45fa-85cd-942e69033130"
                }
            ]
        },
        {
            "id": "7843bf3b-8a03-4512-a9a1-6308df0a985a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e685e3d3-d4f7-4f28-8905-df4521a60969",
            "compositeImage": {
                "id": "190c3ab0-3388-4943-b457-f4495d2125ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7843bf3b-8a03-4512-a9a1-6308df0a985a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f180680-f7a3-4d98-a46f-f0978d4003f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7843bf3b-8a03-4512-a9a1-6308df0a985a",
                    "LayerId": "386837e8-8998-45fa-85cd-942e69033130"
                }
            ]
        },
        {
            "id": "a8476d6d-c199-4182-a3c6-3fb637fd0e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e685e3d3-d4f7-4f28-8905-df4521a60969",
            "compositeImage": {
                "id": "ce2dc5e7-cf87-4f1d-9d0d-6e5c47e3d18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8476d6d-c199-4182-a3c6-3fb637fd0e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d288bbf0-f1b6-42aa-abfb-06eb3a914fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8476d6d-c199-4182-a3c6-3fb637fd0e2f",
                    "LayerId": "386837e8-8998-45fa-85cd-942e69033130"
                }
            ]
        },
        {
            "id": "112a8cf3-4538-42d6-b7c4-a975f6de0d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e685e3d3-d4f7-4f28-8905-df4521a60969",
            "compositeImage": {
                "id": "b4bc4ae6-638c-4139-bddf-bb1ae8ce4279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "112a8cf3-4538-42d6-b7c4-a975f6de0d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df88af17-5633-4e5c-baca-648c16c16993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "112a8cf3-4538-42d6-b7c4-a975f6de0d30",
                    "LayerId": "386837e8-8998-45fa-85cd-942e69033130"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "386837e8-8998-45fa-85cd-942e69033130",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e685e3d3-d4f7-4f28-8905-df4521a60969",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}