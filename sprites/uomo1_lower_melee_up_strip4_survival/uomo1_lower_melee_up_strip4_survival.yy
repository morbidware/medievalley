{
    "id": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_melee_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8bace73-b1b6-44c9-b221-b46573ca563f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "compositeImage": {
                "id": "becba5ee-a01d-4871-bef8-a7a3a30e4365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bace73-b1b6-44c9-b221-b46573ca563f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3f0c89-f691-4ac5-888c-d2a7faa1789c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bace73-b1b6-44c9-b221-b46573ca563f",
                    "LayerId": "f56eeb27-7fbb-4ee8-b31a-8466927f9780"
                }
            ]
        },
        {
            "id": "38c69884-98ca-4a1b-80ab-998a3b22ddcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "compositeImage": {
                "id": "b5a8271d-b38c-44b4-ac7c-a778283f8e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c69884-98ca-4a1b-80ab-998a3b22ddcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1776d14f-c01a-4271-8dfe-630a13644fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c69884-98ca-4a1b-80ab-998a3b22ddcd",
                    "LayerId": "f56eeb27-7fbb-4ee8-b31a-8466927f9780"
                }
            ]
        },
        {
            "id": "e2304ce2-e87e-4982-afe1-2a5d7c163a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "compositeImage": {
                "id": "a502f7e1-fb76-410e-a569-ab9b8e0b3fd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2304ce2-e87e-4982-afe1-2a5d7c163a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2c4162-255f-4fc8-a9c5-36d2e6ba02eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2304ce2-e87e-4982-afe1-2a5d7c163a27",
                    "LayerId": "f56eeb27-7fbb-4ee8-b31a-8466927f9780"
                }
            ]
        },
        {
            "id": "e6d51a68-9622-462f-9d58-27d070956508",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "compositeImage": {
                "id": "375e4361-82af-482d-8b56-7adb38cc6a10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6d51a68-9622-462f-9d58-27d070956508",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6452a59-af43-4b59-b0b3-6df4b67768e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6d51a68-9622-462f-9d58-27d070956508",
                    "LayerId": "f56eeb27-7fbb-4ee8-b31a-8466927f9780"
                }
            ]
        },
        {
            "id": "fb48825c-e621-4c8e-b810-da31783406e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "compositeImage": {
                "id": "67fdfe05-1f6e-450e-87de-3750fb6ecf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb48825c-e621-4c8e-b810-da31783406e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c888795-80b8-4bd3-8927-e0fb4b0ab25c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb48825c-e621-4c8e-b810-da31783406e7",
                    "LayerId": "f56eeb27-7fbb-4ee8-b31a-8466927f9780"
                }
            ]
        },
        {
            "id": "c6a3f4c4-93b5-41b6-a2dc-d0a794bd272e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "compositeImage": {
                "id": "0d284e21-9050-4c29-96b9-ebb3d6ef0dfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a3f4c4-93b5-41b6-a2dc-d0a794bd272e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d4f6d53-dd56-47ca-adb8-ded78f71d585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a3f4c4-93b5-41b6-a2dc-d0a794bd272e",
                    "LayerId": "f56eeb27-7fbb-4ee8-b31a-8466927f9780"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f56eeb27-7fbb-4ee8-b31a-8466927f9780",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30c46f50-4c3d-436e-a41d-03eb4dbd7516",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}