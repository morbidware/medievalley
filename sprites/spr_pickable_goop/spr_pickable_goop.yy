{
    "id": "60827820-7e09-4419-8582-7f24106de248",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_goop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdb8975a-24a8-455c-8f43-907f25f78e1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60827820-7e09-4419-8582-7f24106de248",
            "compositeImage": {
                "id": "fb8c45cf-3dd6-4640-b1ec-ec899e77b7b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb8975a-24a8-455c-8f43-907f25f78e1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99e7aa8-6d2a-4e94-b6f4-adf5525f6a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb8975a-24a8-455c-8f43-907f25f78e1c",
                    "LayerId": "0a87e27b-da11-47ee-8d99-f165a031cc0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0a87e27b-da11-47ee-8d99-f165a031cc0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60827820-7e09-4419-8582-7f24106de248",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 13
}