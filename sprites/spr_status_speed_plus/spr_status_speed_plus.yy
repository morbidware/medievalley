{
    "id": "99de2647-e417-473b-b708-f5035e2f3a6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_status_speed_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6f9e6bd-b878-426e-8c28-33853c9a2f9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99de2647-e417-473b-b708-f5035e2f3a6e",
            "compositeImage": {
                "id": "2f119714-32ce-4595-aa84-be7ce0bde78a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f9e6bd-b878-426e-8c28-33853c9a2f9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928dfa03-3da8-42fc-b3d5-26570023ef04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f9e6bd-b878-426e-8c28-33853c9a2f9c",
                    "LayerId": "ce4e518e-eb68-418e-a805-127b15f3f088"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "ce4e518e-eb68-418e-a805-127b15f3f088",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99de2647-e417-473b-b708-f5035e2f3a6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}