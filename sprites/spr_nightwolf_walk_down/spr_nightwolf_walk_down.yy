{
    "id": "d4ba03e5-5632-4496-b46a-1a170cba09c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 33,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "915f82d0-ed01-4c46-a872-14a077428142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ba03e5-5632-4496-b46a-1a170cba09c3",
            "compositeImage": {
                "id": "55526bb8-7229-448a-827b-aca99c61de48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "915f82d0-ed01-4c46-a872-14a077428142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7222dc27-6851-4469-9f84-b96476ea7840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "915f82d0-ed01-4c46-a872-14a077428142",
                    "LayerId": "a0124c52-198d-484f-a58d-d2dc8a4a66cc"
                }
            ]
        },
        {
            "id": "42f852d3-038e-4f6b-a9b3-8d5125d16411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ba03e5-5632-4496-b46a-1a170cba09c3",
            "compositeImage": {
                "id": "b639358d-b26b-4d96-bddd-4f6be0024ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f852d3-038e-4f6b-a9b3-8d5125d16411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86099f02-d144-434a-937b-28051d10e8e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f852d3-038e-4f6b-a9b3-8d5125d16411",
                    "LayerId": "a0124c52-198d-484f-a58d-d2dc8a4a66cc"
                }
            ]
        },
        {
            "id": "acaf6385-e18f-4241-ad43-0fca87e96a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ba03e5-5632-4496-b46a-1a170cba09c3",
            "compositeImage": {
                "id": "cd3a2534-c09f-4b1c-9b63-a17fd8ad5951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acaf6385-e18f-4241-ad43-0fca87e96a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ca3ea0-4286-4d15-a472-24e1260e7f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acaf6385-e18f-4241-ad43-0fca87e96a38",
                    "LayerId": "a0124c52-198d-484f-a58d-d2dc8a4a66cc"
                }
            ]
        },
        {
            "id": "4790715b-fbe3-46e2-b419-99dda1ddcc3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4ba03e5-5632-4496-b46a-1a170cba09c3",
            "compositeImage": {
                "id": "9052ccbb-60e3-4a50-8301-0b523a08d2fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4790715b-fbe3-46e2-b419-99dda1ddcc3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604eb3ae-10b9-48a8-beaa-5305788984d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4790715b-fbe3-46e2-b419-99dda1ddcc3c",
                    "LayerId": "a0124c52-198d-484f-a58d-d2dc8a4a66cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "a0124c52-198d-484f-a58d-d2dc8a4a66cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4ba03e5-5632-4496-b46a-1a170cba09c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}