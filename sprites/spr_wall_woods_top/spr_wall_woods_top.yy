{
    "id": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_woods_top",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a04a4779-6d01-47cd-aa3f-8f728dd8b337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "compositeImage": {
                "id": "83dde681-97e1-4b88-824f-6f2f901337cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a04a4779-6d01-47cd-aa3f-8f728dd8b337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "727a4be3-9bca-435e-89bd-eb0f3c4c6d6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a04a4779-6d01-47cd-aa3f-8f728dd8b337",
                    "LayerId": "4151cb8e-377f-4f65-a190-b4a4a2377e9b"
                }
            ]
        },
        {
            "id": "0883325e-be3f-413e-b379-6c83d884b14f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "compositeImage": {
                "id": "05db33e8-df5b-4d9e-a81c-0d501fb1b921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0883325e-be3f-413e-b379-6c83d884b14f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d130e4f-c133-497d-bd80-590596478f7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0883325e-be3f-413e-b379-6c83d884b14f",
                    "LayerId": "4151cb8e-377f-4f65-a190-b4a4a2377e9b"
                }
            ]
        },
        {
            "id": "1ac10871-4d1f-4fa1-91a0-c1f50c122657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "compositeImage": {
                "id": "78023632-8322-4e0c-96d3-73bfedf2bb57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ac10871-4d1f-4fa1-91a0-c1f50c122657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f710fb09-4dd2-4a36-bce8-6b4ed3415aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ac10871-4d1f-4fa1-91a0-c1f50c122657",
                    "LayerId": "4151cb8e-377f-4f65-a190-b4a4a2377e9b"
                }
            ]
        },
        {
            "id": "44786e50-4250-40c4-8010-90d46ee2311e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "compositeImage": {
                "id": "a7c4cf8a-461e-4812-b963-ccf4dce48f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44786e50-4250-40c4-8010-90d46ee2311e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d78ee07-fb0a-49bd-8f5d-c03697b11d3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44786e50-4250-40c4-8010-90d46ee2311e",
                    "LayerId": "4151cb8e-377f-4f65-a190-b4a4a2377e9b"
                }
            ]
        },
        {
            "id": "35001c0b-9671-41c4-b43f-aee9361f68d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "compositeImage": {
                "id": "0cde1ab8-2be0-494a-a13d-325c911ab60b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35001c0b-9671-41c4-b43f-aee9361f68d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdde43f0-2ff6-4479-93e4-e6405e9d9647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35001c0b-9671-41c4-b43f-aee9361f68d5",
                    "LayerId": "4151cb8e-377f-4f65-a190-b4a4a2377e9b"
                }
            ]
        },
        {
            "id": "0e6cbce0-9f9e-443a-a800-10b38e214fa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "compositeImage": {
                "id": "f1d7177e-f67e-4658-a826-5c19de445237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e6cbce0-9f9e-443a-a800-10b38e214fa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3c8df0-d4c0-4fe0-a2b6-790650a7efde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e6cbce0-9f9e-443a-a800-10b38e214fa4",
                    "LayerId": "4151cb8e-377f-4f65-a190-b4a4a2377e9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4151cb8e-377f-4f65-a190-b4a4a2377e9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6fffe8d-3c99-4301-b725-7a4e0759e880",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}