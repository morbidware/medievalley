{
    "id": "69cfcac6-32dd-4b30-b4de-a58434513f32",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_overlay",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77b3a492-af9a-4190-a748-776706a70d1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69cfcac6-32dd-4b30-b4de-a58434513f32",
            "compositeImage": {
                "id": "579d194b-24ac-44d8-8e41-9b13641e948e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77b3a492-af9a-4190-a748-776706a70d1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6dfe724-6165-400a-abaf-b3de77e4141a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77b3a492-af9a-4190-a748-776706a70d1e",
                    "LayerId": "4275a322-4983-4c95-bd6a-5bac23aa4173"
                }
            ]
        },
        {
            "id": "a3388678-51c5-407a-b33b-6e812fbb319d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69cfcac6-32dd-4b30-b4de-a58434513f32",
            "compositeImage": {
                "id": "74a78efb-30bc-4576-be2f-76dda7dddd45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3388678-51c5-407a-b33b-6e812fbb319d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32e7df3f-05a6-4498-89bc-52dfb7728a00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3388678-51c5-407a-b33b-6e812fbb319d",
                    "LayerId": "4275a322-4983-4c95-bd6a-5bac23aa4173"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4275a322-4983-4c95-bd6a-5bac23aa4173",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69cfcac6-32dd-4b30-b4de-a58434513f32",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}