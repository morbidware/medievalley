{
    "id": "140d7305-7f99-4194-8634-a708564f7053",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29d57115-1ef2-439a-a750-2ce4a89c5ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "140d7305-7f99-4194-8634-a708564f7053",
            "compositeImage": {
                "id": "81d94209-074c-4a11-8a32-db6698273051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29d57115-1ef2-439a-a750-2ce4a89c5ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5cf634b-f376-400f-81cd-7bf385a0717c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29d57115-1ef2-439a-a750-2ce4a89c5ed3",
                    "LayerId": "3b096e09-0beb-45a7-8669-404e29f9a8d9"
                }
            ]
        },
        {
            "id": "1af0990b-2681-4d09-83c4-4d19e4076a51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "140d7305-7f99-4194-8634-a708564f7053",
            "compositeImage": {
                "id": "c10952da-bc52-41d2-8ada-ae1f3404822c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af0990b-2681-4d09-83c4-4d19e4076a51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47651f2f-d759-4ed0-88a3-b5400fb52a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af0990b-2681-4d09-83c4-4d19e4076a51",
                    "LayerId": "3b096e09-0beb-45a7-8669-404e29f9a8d9"
                }
            ]
        },
        {
            "id": "a62c4a74-90ab-4151-aaaf-f5623f0235f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "140d7305-7f99-4194-8634-a708564f7053",
            "compositeImage": {
                "id": "f1425846-cf9f-474d-bcfd-8a780b0605eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a62c4a74-90ab-4151-aaaf-f5623f0235f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d3ce9a-7c77-44f3-b069-43853dfdaf46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a62c4a74-90ab-4151-aaaf-f5623f0235f5",
                    "LayerId": "3b096e09-0beb-45a7-8669-404e29f9a8d9"
                }
            ]
        },
        {
            "id": "fb35ef8e-5921-4836-bd56-51600dd2596b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "140d7305-7f99-4194-8634-a708564f7053",
            "compositeImage": {
                "id": "748edbbb-8eaf-4f55-93ec-e58240c7c602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb35ef8e-5921-4836-bd56-51600dd2596b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eded9b91-4b38-445d-a1e7-eb814a4b4933",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb35ef8e-5921-4836-bd56-51600dd2596b",
                    "LayerId": "3b096e09-0beb-45a7-8669-404e29f9a8d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b096e09-0beb-45a7-8669-404e29f9a8d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "140d7305-7f99-4194-8634-a708564f7053",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}