{
    "id": "19deb3f1-dd01-4d16-9669-9efebb1fdcd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_sugarbeet_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a7a391e-3915-4723-aa01-73085213d46c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19deb3f1-dd01-4d16-9669-9efebb1fdcd5",
            "compositeImage": {
                "id": "da653b87-996d-425c-b1d0-41dfac5e3832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a7a391e-3915-4723-aa01-73085213d46c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7da0949-079e-474a-a20e-40e90ab44e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a7a391e-3915-4723-aa01-73085213d46c",
                    "LayerId": "e5ee5843-9599-4555-a8f1-869f5a0cbae8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e5ee5843-9599-4555-a8f1-869f5a0cbae8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19deb3f1-dd01-4d16-9669-9efebb1fdcd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}