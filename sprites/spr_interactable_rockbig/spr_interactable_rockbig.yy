{
    "id": "3aebbf65-b5fa-4658-821c-f61a32bf7b9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_rockbig",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cb3bbb7-335e-4e16-b7a8-a9850ad8ea80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aebbf65-b5fa-4658-821c-f61a32bf7b9e",
            "compositeImage": {
                "id": "dd2367ef-6c67-4a70-a3ac-3d9d10a0be2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cb3bbb7-335e-4e16-b7a8-a9850ad8ea80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "816ddfdd-40de-4f09-854e-45d2023718da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cb3bbb7-335e-4e16-b7a8-a9850ad8ea80",
                    "LayerId": "2b2c5b82-1bc7-4751-93d8-99f55497cdf2"
                }
            ]
        },
        {
            "id": "614cfb8b-a6be-4524-b561-47111e52ef20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3aebbf65-b5fa-4658-821c-f61a32bf7b9e",
            "compositeImage": {
                "id": "ff730639-9132-45bb-a0ed-59ce0e719bdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614cfb8b-a6be-4524-b561-47111e52ef20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c217a70-e3dd-40fb-a78e-713be8307db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614cfb8b-a6be-4524-b561-47111e52ef20",
                    "LayerId": "2b2c5b82-1bc7-4751-93d8-99f55497cdf2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2b2c5b82-1bc7-4751-93d8-99f55497cdf2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3aebbf65-b5fa-4658-821c-f61a32bf7b9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 21
}