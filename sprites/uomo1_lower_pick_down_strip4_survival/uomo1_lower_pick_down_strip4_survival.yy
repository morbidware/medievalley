{
    "id": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_pick_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae1a6f2d-8430-4e67-9a19-22994c98636b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "compositeImage": {
                "id": "99fcf1c8-7a9e-4abf-b307-8dfb81ec7d55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae1a6f2d-8430-4e67-9a19-22994c98636b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0b4ba94-118e-433e-b728-476d2fb4afc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae1a6f2d-8430-4e67-9a19-22994c98636b",
                    "LayerId": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a"
                }
            ]
        },
        {
            "id": "7a8f804a-6db4-46b2-a217-01aa7e6ddbc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "compositeImage": {
                "id": "22c4a63f-7bb5-454c-b390-720d0655f0ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a8f804a-6db4-46b2-a217-01aa7e6ddbc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fab18b-6bfc-47ec-ac42-6d6723e506d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a8f804a-6db4-46b2-a217-01aa7e6ddbc1",
                    "LayerId": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a"
                }
            ]
        },
        {
            "id": "a26dc79e-6407-4eeb-8cab-03caa3e57f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "compositeImage": {
                "id": "d87e1f65-52c1-4170-8efc-be2e24754597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26dc79e-6407-4eeb-8cab-03caa3e57f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e5b834-5be3-4b81-8060-a4cc82e17538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26dc79e-6407-4eeb-8cab-03caa3e57f06",
                    "LayerId": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a"
                }
            ]
        },
        {
            "id": "88400f0a-f55d-4c69-917f-810a851e611d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "compositeImage": {
                "id": "f1ea31b1-194a-40dc-9e4a-6c1119d11bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88400f0a-f55d-4c69-917f-810a851e611d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea92c3ec-2cad-4731-a8ba-31e740ec6c60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88400f0a-f55d-4c69-917f-810a851e611d",
                    "LayerId": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a"
                }
            ]
        },
        {
            "id": "c254d37e-a02c-48d6-9add-eb8cbbed2138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "compositeImage": {
                "id": "2958ea67-df1d-4db4-901e-4e14c0020a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c254d37e-a02c-48d6-9add-eb8cbbed2138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a294476-48cf-4a8a-9e60-b4b272e322f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c254d37e-a02c-48d6-9add-eb8cbbed2138",
                    "LayerId": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a"
                }
            ]
        },
        {
            "id": "ecd240ee-4beb-4de7-8a02-c19363f093b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "compositeImage": {
                "id": "ac527b7e-9b92-4dbb-bbdd-ce33ceed7631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecd240ee-4beb-4de7-8a02-c19363f093b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b34cdbdd-852f-415e-bb29-0fc73af1cbe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecd240ee-4beb-4de7-8a02-c19363f093b3",
                    "LayerId": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ca461cc2-9cc5-42f5-9ce0-88aaba143a8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c9a39bc-b8f2-4d29-b3e6-b4c14367c23e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}