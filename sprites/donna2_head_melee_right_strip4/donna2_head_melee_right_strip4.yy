{
    "id": "4d3404db-258e-4bcc-a828-4d3071e713f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c33b0aa-4dd5-4715-a5c5-710f227dac1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d3404db-258e-4bcc-a828-4d3071e713f8",
            "compositeImage": {
                "id": "52463881-c727-4090-a274-bba9f7b61bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c33b0aa-4dd5-4715-a5c5-710f227dac1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe3516cc-0ed3-4726-a67b-3838ecf61d39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c33b0aa-4dd5-4715-a5c5-710f227dac1d",
                    "LayerId": "7721dfc9-87d7-4bec-9eb7-4929e64e5810"
                }
            ]
        },
        {
            "id": "62d5fc07-5628-4158-bf4d-028063baccf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d3404db-258e-4bcc-a828-4d3071e713f8",
            "compositeImage": {
                "id": "dbb8f36c-a086-4541-8a4e-184712b5f762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62d5fc07-5628-4158-bf4d-028063baccf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3db9461f-069a-4c9a-a7ea-98e4a63743ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62d5fc07-5628-4158-bf4d-028063baccf3",
                    "LayerId": "7721dfc9-87d7-4bec-9eb7-4929e64e5810"
                }
            ]
        },
        {
            "id": "2949cf6f-6415-470c-b0de-0cadcb93392d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d3404db-258e-4bcc-a828-4d3071e713f8",
            "compositeImage": {
                "id": "34c89116-c378-4298-bfaf-41712374898b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2949cf6f-6415-470c-b0de-0cadcb93392d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cebb0edb-bd4d-4d80-b9c7-7c17772433e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2949cf6f-6415-470c-b0de-0cadcb93392d",
                    "LayerId": "7721dfc9-87d7-4bec-9eb7-4929e64e5810"
                }
            ]
        },
        {
            "id": "56c86b89-a890-4933-bf6b-6b295f6755dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d3404db-258e-4bcc-a828-4d3071e713f8",
            "compositeImage": {
                "id": "f12aba83-2651-4201-a674-7635a7606972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c86b89-a890-4933-bf6b-6b295f6755dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f730d80f-44ed-4d45-b2bc-93a98e05072a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c86b89-a890-4933-bf6b-6b295f6755dc",
                    "LayerId": "7721dfc9-87d7-4bec-9eb7-4929e64e5810"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7721dfc9-87d7-4bec-9eb7-4929e64e5810",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d3404db-258e-4bcc-a828-4d3071e713f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}