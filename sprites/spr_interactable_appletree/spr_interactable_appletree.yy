{
    "id": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_appletree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 19,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a6696fd-7ac4-4d3a-9a63-5fd9408bc97a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "compositeImage": {
                "id": "ac44454a-b7ca-47f8-b75c-ed0f3b854112",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a6696fd-7ac4-4d3a-9a63-5fd9408bc97a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332813bb-cf74-4202-8787-160b27756ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a6696fd-7ac4-4d3a-9a63-5fd9408bc97a",
                    "LayerId": "6bf733be-99fc-4eca-8638-aa2100cf791a"
                }
            ]
        },
        {
            "id": "63bde5e8-2b2c-4225-8a50-42599bbb8dbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "compositeImage": {
                "id": "500d3360-8202-4e61-97df-a351e36a48dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63bde5e8-2b2c-4225-8a50-42599bbb8dbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df42a826-a527-4e76-b530-4b09c8d08d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63bde5e8-2b2c-4225-8a50-42599bbb8dbf",
                    "LayerId": "6bf733be-99fc-4eca-8638-aa2100cf791a"
                }
            ]
        },
        {
            "id": "3aac80c3-c1f2-4f94-aa6c-76db165e6ac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "compositeImage": {
                "id": "bba777f2-0859-4da2-a98e-6703f0f2af28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aac80c3-c1f2-4f94-aa6c-76db165e6ac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691579e5-7a7f-4e77-963f-8600cd603a9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aac80c3-c1f2-4f94-aa6c-76db165e6ac7",
                    "LayerId": "6bf733be-99fc-4eca-8638-aa2100cf791a"
                }
            ]
        },
        {
            "id": "69c2d7a3-beef-4099-a8f6-cded4c2753aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "compositeImage": {
                "id": "88599e1c-1d75-4fd7-972f-f8b745f79b11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69c2d7a3-beef-4099-a8f6-cded4c2753aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23e9622e-c85e-41f8-8b88-58e0e7b04081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69c2d7a3-beef-4099-a8f6-cded4c2753aa",
                    "LayerId": "6bf733be-99fc-4eca-8638-aa2100cf791a"
                }
            ]
        },
        {
            "id": "90723b6f-116c-4841-b1a1-08b0d13fb0cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "compositeImage": {
                "id": "a848eb11-f1ce-4ddd-a4ea-b770568eea56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90723b6f-116c-4841-b1a1-08b0d13fb0cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6a5360-38ca-40d6-9568-1937f47305b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90723b6f-116c-4841-b1a1-08b0d13fb0cd",
                    "LayerId": "6bf733be-99fc-4eca-8638-aa2100cf791a"
                }
            ]
        },
        {
            "id": "243b2470-dced-497f-8082-d714915318f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "compositeImage": {
                "id": "02123a9f-e4ad-4448-ab2c-84a984c2b5cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "243b2470-dced-497f-8082-d714915318f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a89fce29-8212-4598-a004-2f954682194e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "243b2470-dced-497f-8082-d714915318f4",
                    "LayerId": "6bf733be-99fc-4eca-8638-aa2100cf791a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "6bf733be-99fc-4eca-8638-aa2100cf791a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2013646a-1b13-4f7b-bfe3-2b169abe97fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 39,
    "yorig": 72
}