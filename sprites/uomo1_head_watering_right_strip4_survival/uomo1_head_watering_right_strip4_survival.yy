{
    "id": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_watering_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a1e946b-be08-4e62-9cc8-a5046c8f3de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "compositeImage": {
                "id": "6eb842b5-0ac3-4e93-b40f-90376af9ce59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a1e946b-be08-4e62-9cc8-a5046c8f3de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7471f829-549b-4a0f-a877-ad60cbcc5307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a1e946b-be08-4e62-9cc8-a5046c8f3de5",
                    "LayerId": "73af27cd-880b-425f-b2e5-f810f1b4a18e"
                }
            ]
        },
        {
            "id": "41dc9b94-745f-4747-8bde-739067c766db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "compositeImage": {
                "id": "b1e863b3-3f62-473c-a7bd-8ca64fbe4538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41dc9b94-745f-4747-8bde-739067c766db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4ebf52b-22d8-4674-b783-bc918343953f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41dc9b94-745f-4747-8bde-739067c766db",
                    "LayerId": "73af27cd-880b-425f-b2e5-f810f1b4a18e"
                }
            ]
        },
        {
            "id": "7807fbd1-bdc5-4bf8-83b6-d21a3f75ff65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "compositeImage": {
                "id": "60654595-d7ea-416e-aeee-4e956d6294f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7807fbd1-bdc5-4bf8-83b6-d21a3f75ff65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b170803c-57c0-49d8-b6c5-1df78c70ab10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7807fbd1-bdc5-4bf8-83b6-d21a3f75ff65",
                    "LayerId": "73af27cd-880b-425f-b2e5-f810f1b4a18e"
                }
            ]
        },
        {
            "id": "12443e4d-2b76-47fe-8789-808f040bb0d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "compositeImage": {
                "id": "c8c749a1-e087-4d88-83b4-5d2965847fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12443e4d-2b76-47fe-8789-808f040bb0d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a00ddf-0a08-4417-9ea9-945468e6dc3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12443e4d-2b76-47fe-8789-808f040bb0d9",
                    "LayerId": "73af27cd-880b-425f-b2e5-f810f1b4a18e"
                }
            ]
        },
        {
            "id": "4fa409f9-3534-4aae-926c-c30c65936ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "compositeImage": {
                "id": "1c4b643b-ade7-444e-a9ee-8fd44b0bd3a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa409f9-3534-4aae-926c-c30c65936ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07827f5-951a-4418-8b22-692e9583a967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa409f9-3534-4aae-926c-c30c65936ac2",
                    "LayerId": "73af27cd-880b-425f-b2e5-f810f1b4a18e"
                }
            ]
        },
        {
            "id": "3c37e3d5-819a-4d29-8bf6-8c4573011257",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "compositeImage": {
                "id": "246ccf3b-10a0-44da-aac9-5d35ae1be543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c37e3d5-819a-4d29-8bf6-8c4573011257",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a39cb3c2-1d38-4c23-86cc-66fdb5eaa8b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c37e3d5-819a-4d29-8bf6-8c4573011257",
                    "LayerId": "73af27cd-880b-425f-b2e5-f810f1b4a18e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "73af27cd-880b-425f-b2e5-f810f1b4a18e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b71929ef-1edc-4e9f-9e9f-643dd4eeb00d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}