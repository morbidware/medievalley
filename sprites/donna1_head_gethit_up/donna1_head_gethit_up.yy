{
    "id": "d4944ac2-9c01-4198-a58b-d5621e110864",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_head_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07192a0b-a115-4e7c-8ed7-177e190971fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4944ac2-9c01-4198-a58b-d5621e110864",
            "compositeImage": {
                "id": "0de77e1c-feb8-4312-8db6-97a0e2575493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07192a0b-a115-4e7c-8ed7-177e190971fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb074d66-8411-491c-b09a-1d3fa7699d1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07192a0b-a115-4e7c-8ed7-177e190971fe",
                    "LayerId": "9aed15b8-bbf6-40f8-808e-4000b9d49d3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9aed15b8-bbf6-40f8-808e-4000b9d49d3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4944ac2-9c01-4198-a58b-d5621e110864",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}