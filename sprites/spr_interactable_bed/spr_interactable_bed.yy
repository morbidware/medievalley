{
    "id": "80f8f0e2-bcf7-4747-9f20-a7da2a4e04c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_bed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae9a69a9-c60d-4244-8931-2462368d1d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80f8f0e2-bcf7-4747-9f20-a7da2a4e04c1",
            "compositeImage": {
                "id": "437311ce-d186-49ae-a3b2-27e318001493",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae9a69a9-c60d-4244-8931-2462368d1d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da502125-aa01-4222-8b99-dc729bd4c3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae9a69a9-c60d-4244-8931-2462368d1d74",
                    "LayerId": "3f6a4415-1b97-4b69-8cfc-570a9a4b1690"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "3f6a4415-1b97-4b69-8cfc-570a9a4b1690",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80f8f0e2-bcf7-4747-9f20-a7da2a4e04c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}