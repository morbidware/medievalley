{
    "id": "a917bd6d-ac50-43f5-91bb-a8ad87d933ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_zoom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08c6cb3f-d42a-4bcf-a1e2-6d3ea0d59d08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a917bd6d-ac50-43f5-91bb-a8ad87d933ec",
            "compositeImage": {
                "id": "cb824963-9dd9-40b8-bb03-264cfee58eaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c6cb3f-d42a-4bcf-a1e2-6d3ea0d59d08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f55e40-ab92-4c72-94f7-280c60f41bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c6cb3f-d42a-4bcf-a1e2-6d3ea0d59d08",
                    "LayerId": "c6d95e73-2240-401f-8010-9a0f2a58ab98"
                }
            ]
        },
        {
            "id": "97677f42-27af-48e6-abe2-6a8e21af5510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a917bd6d-ac50-43f5-91bb-a8ad87d933ec",
            "compositeImage": {
                "id": "7781b500-0513-421a-89e2-af8a1372212a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97677f42-27af-48e6-abe2-6a8e21af5510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d167cb-5bee-4b2e-9a22-501d193561be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97677f42-27af-48e6-abe2-6a8e21af5510",
                    "LayerId": "c6d95e73-2240-401f-8010-9a0f2a58ab98"
                }
            ]
        },
        {
            "id": "ab842345-762f-45ec-b040-f8d5be3675d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a917bd6d-ac50-43f5-91bb-a8ad87d933ec",
            "compositeImage": {
                "id": "d2f2e3d2-8d77-4afa-934f-bbc602da9f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab842345-762f-45ec-b040-f8d5be3675d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "248a6e05-35e1-44e8-b7a7-91de24f490b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab842345-762f-45ec-b040-f8d5be3675d9",
                    "LayerId": "c6d95e73-2240-401f-8010-9a0f2a58ab98"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c6d95e73-2240-401f-8010-9a0f2a58ab98",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a917bd6d-ac50-43f5-91bb-a8ad87d933ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}