{
    "id": "b2596d29-4683-40ba-8ac4-98c5f6741c76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_lavender_png",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a186a72-c2a1-4715-8782-dbe26a20bc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2596d29-4683-40ba-8ac4-98c5f6741c76",
            "compositeImage": {
                "id": "c1923697-abcd-45e0-bc75-b4ddeb268265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a186a72-c2a1-4715-8782-dbe26a20bc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45518d52-447a-4f15-82d4-dccb4baa7986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a186a72-c2a1-4715-8782-dbe26a20bc07",
                    "LayerId": "ac659e1b-0db6-47cb-a73b-6c62a1b2c517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac659e1b-0db6-47cb-a73b-6c62a1b2c517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2596d29-4683-40ba-8ac4-98c5f6741c76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 29
}