{
    "id": "8c4cf981-e1c9-4c85-a002-f970c379c632",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_timer_label",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0340c30c-4b1b-4127-823c-94984bc71342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c4cf981-e1c9-4c85-a002-f970c379c632",
            "compositeImage": {
                "id": "d4612d5b-f93f-486b-bccd-4c88bd1de65e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0340c30c-4b1b-4127-823c-94984bc71342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c91226c-7414-43aa-9336-ff89eabc9a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0340c30c-4b1b-4127-823c-94984bc71342",
                    "LayerId": "62a28cbb-45d2-4ad4-9c2f-d54c3627cba3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "62a28cbb-45d2-4ad4-9c2f-d54c3627cba3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c4cf981-e1c9-4c85-a002-f970c379c632",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 47,
    "xorig": 28,
    "yorig": 0
}