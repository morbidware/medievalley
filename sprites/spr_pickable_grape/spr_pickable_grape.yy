{
    "id": "d1ab18f4-853e-48e8-ab8e-5ba2f45f25f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_grape",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ed54d17-9acd-42ff-91ab-1a7bb9101f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1ab18f4-853e-48e8-ab8e-5ba2f45f25f7",
            "compositeImage": {
                "id": "4bc806be-e181-476a-85cf-dee480d8c3ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed54d17-9acd-42ff-91ab-1a7bb9101f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb9a4c52-20be-4f7a-bac9-7bd7782417f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed54d17-9acd-42ff-91ab-1a7bb9101f91",
                    "LayerId": "69f9a1e8-816c-431e-9398-14eaa4184e97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "69f9a1e8-816c-431e-9398-14eaa4184e97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1ab18f4-853e-48e8-ab8e-5ba2f45f25f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}