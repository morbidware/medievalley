{
    "id": "0bb77e57-7c16-499e-a99d-a5affe52076f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_titles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "257b4777-38c5-48d5-919b-dc1a0cc1b1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "a86c82eb-4d64-4bfb-9afd-f8354beb12a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "257b4777-38c5-48d5-919b-dc1a0cc1b1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ec1711-6b50-49ba-aa18-a7ffc1bbf898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "257b4777-38c5-48d5-919b-dc1a0cc1b1ea",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "ba4959ea-acf2-45fa-877f-8024961e1fb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "44fe4f12-7be3-46e1-bdb6-9690e4f8f73a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4959ea-acf2-45fa-877f-8024961e1fb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984adc7e-597f-4443-a183-b49fefc5e2e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4959ea-acf2-45fa-877f-8024961e1fb8",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "994e9c97-2707-48fe-bc14-ea15d1188123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "4d566a66-97bf-403a-b847-48d661769091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "994e9c97-2707-48fe-bc14-ea15d1188123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0f7a43-3fe5-4b6a-8b19-085873869e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "994e9c97-2707-48fe-bc14-ea15d1188123",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "271279ea-fdea-4f95-9691-cd0ffabbbbb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "48997dc4-0ea5-4b95-b044-7965b0f166d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "271279ea-fdea-4f95-9691-cd0ffabbbbb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0dfe712-10f1-439a-8d00-1492397759eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "271279ea-fdea-4f95-9691-cd0ffabbbbb7",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "03fef615-aae4-4269-b7b4-30dc245d1c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "fc21b5e5-2580-4d5e-8f8c-24664ce362ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03fef615-aae4-4269-b7b4-30dc245d1c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cffe3d7-8eb0-48a0-87d3-bb7257ec7328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03fef615-aae4-4269-b7b4-30dc245d1c83",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "e35a00d3-c713-4aae-a5f1-e6953f3fe5cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "af50985e-b146-49e5-ab55-b04b840559a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e35a00d3-c713-4aae-a5f1-e6953f3fe5cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fd429a9-d2a3-4068-8550-8ed197c3cb18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e35a00d3-c713-4aae-a5f1-e6953f3fe5cb",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "1c5cd7ce-c1ce-4ee4-bc08-4ff8307751c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "0f916cf9-f2c1-40df-afdf-b8f9ddf33ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c5cd7ce-c1ce-4ee4-bc08-4ff8307751c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2539e728-d39a-48a0-884f-b1a3ba9abcac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c5cd7ce-c1ce-4ee4-bc08-4ff8307751c7",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "808dc4e1-ac6e-43dc-8f88-1a2b6def5208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "e30cbad7-7395-43b8-bfcf-788c25e942a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "808dc4e1-ac6e-43dc-8f88-1a2b6def5208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f05d28b9-6637-435c-9b65-9740523e55a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "808dc4e1-ac6e-43dc-8f88-1a2b6def5208",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "0babcfb5-df85-4971-8e28-b1526dcefd7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "1dd6a76a-f799-488f-aba6-4b9e412a5c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0babcfb5-df85-4971-8e28-b1526dcefd7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4074c46-2c9e-461b-8641-14865e1878ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0babcfb5-df85-4971-8e28-b1526dcefd7b",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "c57dab1d-3af5-403c-bb6b-a558bd9cc275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "61c0adff-601c-445c-8c03-0cbee08eefa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c57dab1d-3af5-403c-bb6b-a558bd9cc275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acbd2e6b-ca48-41ed-be8d-81d7a49c0a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c57dab1d-3af5-403c-bb6b-a558bd9cc275",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "c1c9b0c4-c07d-4ef4-a17d-67ca81ec1546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "dd082e33-011a-48b5-a972-40e2d3f56b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1c9b0c4-c07d-4ef4-a17d-67ca81ec1546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ae43594-1b18-4c2b-9569-42b6b19d0ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1c9b0c4-c07d-4ef4-a17d-67ca81ec1546",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "926aadfa-d08d-4e54-8989-ff9393cbcea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "1223a0e1-a65c-4690-9857-1f25d64d3bb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926aadfa-d08d-4e54-8989-ff9393cbcea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e177bff7-7c43-4b84-868e-9149288dca50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926aadfa-d08d-4e54-8989-ff9393cbcea7",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "cbd20d91-216f-4f9a-8e98-dac3f94c97ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "2f95ce73-57a3-4c7e-9109-037b3420bcb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd20d91-216f-4f9a-8e98-dac3f94c97ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6725354-36c8-4685-bbba-0e00323ef044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd20d91-216f-4f9a-8e98-dac3f94c97ba",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "6488d1dc-c7d9-4bb3-a41f-676c4f30e2ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "899a3b11-bd82-4259-a9d2-a4db021d0f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6488d1dc-c7d9-4bb3-a41f-676c4f30e2ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3764f7f9-281c-464e-b134-c40179db5790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6488d1dc-c7d9-4bb3-a41f-676c4f30e2ef",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "2be5b30d-8c8f-4dfa-8fde-b7c84d11bce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "0e21c2de-ddc2-42d1-bd43-fa1e07c87a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2be5b30d-8c8f-4dfa-8fde-b7c84d11bce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f532eb-6953-48fc-b960-bf626a1beb71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2be5b30d-8c8f-4dfa-8fde-b7c84d11bce0",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "6bab74f0-85af-4047-b376-f38f70af903e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "42280d35-2892-4787-8dae-78ffa84f6b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bab74f0-85af-4047-b376-f38f70af903e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f13a5c78-6d89-4be5-9a42-817850006696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bab74f0-85af-4047-b376-f38f70af903e",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "f9816f53-14cd-4e8c-aba5-6e24e754a068",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "2dc2ed06-4398-48c6-a631-fe9d8da2cd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9816f53-14cd-4e8c-aba5-6e24e754a068",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4646fc7b-a917-4528-bdb3-1664de9bddc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9816f53-14cd-4e8c-aba5-6e24e754a068",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "f0bb12d0-1aee-48c3-9353-eaa84fc89112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "c5142e6c-608f-4450-bbf0-d5581bc880fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0bb12d0-1aee-48c3-9353-eaa84fc89112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223bc3e5-3cbb-430d-bbdf-2d280608505e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0bb12d0-1aee-48c3-9353-eaa84fc89112",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "6c17aa3d-041f-4aae-8da9-d5b2e1161df0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "9578ada3-3a24-4c82-ac7d-dbd534b33f66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c17aa3d-041f-4aae-8da9-d5b2e1161df0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebbb7e7d-ec30-4bc5-ab1e-75196fe4082a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c17aa3d-041f-4aae-8da9-d5b2e1161df0",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "ef8884b2-eaa7-40c4-a7c9-7f4a1ec6a251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "aedb20b3-9e87-4c40-95e4-5e16c137edd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8884b2-eaa7-40c4-a7c9-7f4a1ec6a251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b357eac1-a9c6-4208-8314-3b7ca62b9198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8884b2-eaa7-40c4-a7c9-7f4a1ec6a251",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "5231879e-def3-4c57-8621-7053a1a26c6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "0d7ced11-af13-4bc6-b267-818b7e3ff971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5231879e-def3-4c57-8621-7053a1a26c6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6645b8ee-38f9-4dd1-a0a9-52890461ccab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5231879e-def3-4c57-8621-7053a1a26c6a",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "0f1edd6a-ef04-437f-9a3d-49efaf8072e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "bf21a099-a17d-4fe8-ad30-a894e76c7810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f1edd6a-ef04-437f-9a3d-49efaf8072e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "941bbb36-bb9b-4eec-9fb9-835159d75c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f1edd6a-ef04-437f-9a3d-49efaf8072e8",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "0f4e8d7b-83f4-4696-a963-22d749720a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "406de36d-6038-459a-88fb-40538d0f5fe7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f4e8d7b-83f4-4696-a963-22d749720a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4319bb94-cc8d-4956-b52e-fb9109e5ac49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f4e8d7b-83f4-4696-a963-22d749720a86",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "841aac09-15a6-47b6-bcc3-c554eb0f0fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "4dfed9d6-f2e8-4aab-8922-ca71dffb1155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "841aac09-15a6-47b6-bcc3-c554eb0f0fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32fdb8b1-b337-4609-8cc5-7cc3e5f2f076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "841aac09-15a6-47b6-bcc3-c554eb0f0fcb",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "cedf67c6-ce29-4382-8fb5-ad041f1b3089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "3c9f9a97-53bb-4b99-8a68-40cfdc394f93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cedf67c6-ce29-4382-8fb5-ad041f1b3089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7c1624-5db7-434d-97e8-e3ead55840fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cedf67c6-ce29-4382-8fb5-ad041f1b3089",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "c71905b9-450d-4f17-8173-3e8d0a6552e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "f75f9048-b516-46f6-a481-bde71d7cde11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71905b9-450d-4f17-8173-3e8d0a6552e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80c58f18-e419-4f45-979a-7aaf49005a5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71905b9-450d-4f17-8173-3e8d0a6552e0",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "4a075516-605c-49f1-9675-afb8603906de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "d90e92b0-c411-4cea-949f-e09baffa99e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a075516-605c-49f1-9675-afb8603906de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "180978d8-9742-41b1-9d85-221eb596c1dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a075516-605c-49f1-9675-afb8603906de",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "ea4a2ec6-4bc4-4ed0-adef-711d474dc8e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "a17cf5e5-c79a-4f45-b3a4-79295a1bdaf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea4a2ec6-4bc4-4ed0-adef-711d474dc8e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0e70234-a354-46af-a6c7-5291f889f1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea4a2ec6-4bc4-4ed0-adef-711d474dc8e2",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "f5278ab9-d25b-48e9-a756-fa1bca98ff7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "275ca414-93d3-46df-a857-3ad2ad7e633c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5278ab9-d25b-48e9-a756-fa1bca98ff7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066933b1-7f04-4a12-a131-d06a76a1abd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5278ab9-d25b-48e9-a756-fa1bca98ff7d",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "99694726-1763-4b91-adcd-b60daa4ede16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "5030b489-654c-4338-8b28-5d4f22faac04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99694726-1763-4b91-adcd-b60daa4ede16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5609ea1-3ad9-41b0-897b-a8369952ef4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99694726-1763-4b91-adcd-b60daa4ede16",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "e733605b-1835-4c5e-95cb-cfc7b260cf14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "6b4e91fc-aed1-47c4-b28e-7422c06f8d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e733605b-1835-4c5e-95cb-cfc7b260cf14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f888c077-d60c-48e9-8968-e07588b348bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e733605b-1835-4c5e-95cb-cfc7b260cf14",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "09852ee1-8969-4c76-88d5-613dfd7d18fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "203b8e18-5f5d-407f-bce1-6549c33231c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09852ee1-8969-4c76-88d5-613dfd7d18fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20bfa15f-d1c2-4449-9f88-cf437469c3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09852ee1-8969-4c76-88d5-613dfd7d18fd",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "fa13bc70-6863-4170-9535-602140aa769f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "362980bc-a9b5-43cf-bcd6-90585721b035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa13bc70-6863-4170-9535-602140aa769f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d60dff-aa2d-4095-b81d-38d260325eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa13bc70-6863-4170-9535-602140aa769f",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "65565e3a-25ba-4564-b034-7fb973d236e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "b191059b-567a-4673-b930-313d64d1a542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65565e3a-25ba-4564-b034-7fb973d236e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d618775a-42f8-499e-87c6-a94777f97022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65565e3a-25ba-4564-b034-7fb973d236e5",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "109b0785-ea60-47d5-93c3-d1c0645396fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "18480015-1744-4e80-98ee-132f7d0fe3f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109b0785-ea60-47d5-93c3-d1c0645396fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4328e339-6945-42d5-b2a6-58ef75d0fdba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109b0785-ea60-47d5-93c3-d1c0645396fc",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "ad75e94b-5de9-46e2-a9f2-34d74b4a5287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "ef621c18-27b5-49d7-a965-f7803bf85baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad75e94b-5de9-46e2-a9f2-34d74b4a5287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e8e2b62-67cd-424b-a6f8-b8a746cf6f05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad75e94b-5de9-46e2-a9f2-34d74b4a5287",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "1ece9b9c-1a01-4049-a172-d59171504095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "01f51f5a-0609-42fa-94fd-ef30c29acf03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ece9b9c-1a01-4049-a172-d59171504095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2078d361-0cbe-49b8-b2ec-fc69d03d78d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ece9b9c-1a01-4049-a172-d59171504095",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "44954d19-1df2-4dae-9aed-e0507c36a4e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "6fd3c19d-d639-4bdd-bff5-7a9a0f84571a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44954d19-1df2-4dae-9aed-e0507c36a4e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f08db4-012c-4923-8d10-b2a115207650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44954d19-1df2-4dae-9aed-e0507c36a4e7",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "b93b75df-8762-4dfc-a8ea-afbffc3c6f44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "d2f840e7-1fd5-4b2b-83e7-e08adf69d18c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b93b75df-8762-4dfc-a8ea-afbffc3c6f44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d77fb90-a6b7-44f6-9f31-866d30805d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b93b75df-8762-4dfc-a8ea-afbffc3c6f44",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "1748d9e7-283f-49c0-97d8-eb6f4d2a96fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "fd255c16-e1fc-4b5b-bb08-9f3be1028b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1748d9e7-283f-49c0-97d8-eb6f4d2a96fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83563a88-2a96-4152-bd9a-185a4264ae32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1748d9e7-283f-49c0-97d8-eb6f4d2a96fe",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "4336d21d-c370-455a-87cd-8c79808ba031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "ddaf7227-a7c1-4b52-88d5-c874e7cadc91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4336d21d-c370-455a-87cd-8c79808ba031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deb75ff8-cc75-4d39-b615-59b12cdac5e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4336d21d-c370-455a-87cd-8c79808ba031",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "1c6665c1-6801-40ac-934e-c02b2b102126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "cbc3ec55-35c9-4b40-8643-f08f3f0c85cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6665c1-6801-40ac-934e-c02b2b102126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ef98bb5-12cf-4196-be24-07d00b8b0158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6665c1-6801-40ac-934e-c02b2b102126",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "fe226951-cfdb-4851-bb69-db233edc407f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "4573e0ce-d9ae-404c-8013-61b2462503bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe226951-cfdb-4851-bb69-db233edc407f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62443e52-ff01-46b4-9944-b0e80fb89f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe226951-cfdb-4851-bb69-db233edc407f",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "44ff6af5-128e-4417-ba75-7a837154af97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "145075a7-e9c6-4c9b-ae93-12afef34ac34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44ff6af5-128e-4417-ba75-7a837154af97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d38c605-2366-4963-819e-74b600519975",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44ff6af5-128e-4417-ba75-7a837154af97",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "dbfd332f-ef85-4dc2-9214-3d26281474eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "26a703de-1e95-4c62-82bd-70865a5be3b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbfd332f-ef85-4dc2-9214-3d26281474eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05f3eb27-d56f-4e0f-9454-3a982f0b2193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbfd332f-ef85-4dc2-9214-3d26281474eb",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "99e6ed3d-27a2-43e7-a927-25fdb459137b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "e5f4d9ef-c2b8-4d23-b3b1-12dc549405dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e6ed3d-27a2-43e7-a927-25fdb459137b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b812d9-bb7b-4e7c-8e76-835f4718a0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e6ed3d-27a2-43e7-a927-25fdb459137b",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "f2ad4752-a084-47ec-9397-abd0cb709da1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "9663ab1e-32ba-4b19-86e5-e9048446453c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ad4752-a084-47ec-9397-abd0cb709da1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2892a2ae-761c-440a-9149-323efbe4f9bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ad4752-a084-47ec-9397-abd0cb709da1",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "d63b9d89-57a3-472d-b9c0-67fa735c355a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "2e1bdfa0-38be-40e3-8a26-0c2647d6ea65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63b9d89-57a3-472d-b9c0-67fa735c355a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8985192b-80c8-472c-bdb3-6d6f6dcf4dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63b9d89-57a3-472d-b9c0-67fa735c355a",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "d3fe37b7-5dde-4eb9-9999-2825e26038f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "ab831e56-2d5b-4559-81bd-8e9a07552691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3fe37b7-5dde-4eb9-9999-2825e26038f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc79b77-b841-4eda-ba13-13a1e0a00830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3fe37b7-5dde-4eb9-9999-2825e26038f4",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "87d64098-a97a-4b14-acff-f41035a31bea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "de401bb9-1bcd-4601-b18a-e62e2359c8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87d64098-a97a-4b14-acff-f41035a31bea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38643db0-8819-4f5f-b5f8-06f1e9afd781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87d64098-a97a-4b14-acff-f41035a31bea",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "56e8dba0-e830-443f-8125-6811ec8c6896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "387a8fa9-fc2b-4172-8d8d-6f1ee7c1d95e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e8dba0-e830-443f-8125-6811ec8c6896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcfed3ff-2a64-4122-803b-ea9448178840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e8dba0-e830-443f-8125-6811ec8c6896",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "d0ae6c99-3fc7-4453-9245-7b625f864ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "8a169236-e0b9-4aea-828e-7b1f8f2d9e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0ae6c99-3fc7-4453-9245-7b625f864ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f0f997-55b3-4d31-a9db-f6a7940efde8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0ae6c99-3fc7-4453-9245-7b625f864ba0",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "d131bae2-4991-4384-ba24-b73a0af899be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "32d3446f-e766-46b8-a3eb-ffce3b6fd2b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d131bae2-4991-4384-ba24-b73a0af899be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "696d9af4-ec7a-4290-82b3-a1dc4df333f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d131bae2-4991-4384-ba24-b73a0af899be",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "0ff5fa2d-2fae-4157-8f67-3f601e607706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "15841ce5-e585-4178-8fbb-3dcb11eddfa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ff5fa2d-2fae-4157-8f67-3f601e607706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f661499e-f823-488c-95e3-2328ad12a142",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ff5fa2d-2fae-4157-8f67-3f601e607706",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "6482de8a-e61e-4a36-bd08-c47f6b931f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "c708d1ce-0c4a-4cb2-b872-cadc99e45f45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6482de8a-e61e-4a36-bd08-c47f6b931f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cbc727f-b976-4651-8182-c501d560e0ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6482de8a-e61e-4a36-bd08-c47f6b931f3c",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "375b052f-1c64-4de4-9980-982bef56ab2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "18f6f969-e8ab-4d0c-8b46-ac57d6f5d2f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375b052f-1c64-4de4-9980-982bef56ab2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1a4bfb3-d7bc-4f01-a082-1c5365a13461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375b052f-1c64-4de4-9980-982bef56ab2b",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "ce8e2e14-5dd9-40f5-af40-beb168e76569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "70227ce6-bca1-45d8-8687-637c8e81fbd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8e2e14-5dd9-40f5-af40-beb168e76569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5205bf6-fab4-4aeb-9e42-08c1667a9dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8e2e14-5dd9-40f5-af40-beb168e76569",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "8498638e-49b3-4034-974c-39f6508b44e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "4d4770c1-a46b-409a-be55-d644d868712b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8498638e-49b3-4034-974c-39f6508b44e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0cba7f3-f2ef-4e25-b113-f89f2cfac12b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8498638e-49b3-4034-974c-39f6508b44e7",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "a4392e35-1dd8-4735-b323-6163fb808335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "0cfd9e1c-615f-49b1-9dd0-d0708a2ab931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4392e35-1dd8-4735-b323-6163fb808335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42281c98-2ddd-41c6-97d0-bfe704021d0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4392e35-1dd8-4735-b323-6163fb808335",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "c753a195-9fd3-4e74-8811-5cd9eff270ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "348b4dd4-7f11-47ec-90e1-3460400eaa68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c753a195-9fd3-4e74-8811-5cd9eff270ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b04806eb-4137-4637-9fe9-213f1acc6e4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c753a195-9fd3-4e74-8811-5cd9eff270ff",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "2807393a-c180-4d81-b6a7-adcc6893163f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "3c8e3d13-d50a-41f9-aa8c-a834369951cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2807393a-c180-4d81-b6a7-adcc6893163f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ad51e4-6af7-45af-a412-ff7dc25fafb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2807393a-c180-4d81-b6a7-adcc6893163f",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "4305be67-ad22-4dcd-a882-ac7037873d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "598e477a-271e-4433-8ea2-e28bf539284f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4305be67-ad22-4dcd-a882-ac7037873d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6865f5b8-0128-4a9b-aca4-fcb51691cc7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4305be67-ad22-4dcd-a882-ac7037873d50",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "8c690936-df2f-4d5e-af42-b35e1d27acb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "ca15ba98-bbe8-4ca2-96a3-7cef8749ce5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c690936-df2f-4d5e-af42-b35e1d27acb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c7a89f0-e20a-4f42-b73b-73569333765a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c690936-df2f-4d5e-af42-b35e1d27acb0",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "8a4319d8-53a4-489c-b2e3-1201693d03fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "461b1ffa-4c85-4ff9-a89b-f7723d16a4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a4319d8-53a4-489c-b2e3-1201693d03fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b494af-ace4-43d5-b833-64c9e52783f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a4319d8-53a4-489c-b2e3-1201693d03fc",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "15438c88-cc82-4eef-bdd1-fe27b3bdd500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "d0a641d0-9641-4eae-93eb-cfc8214b83af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15438c88-cc82-4eef-bdd1-fe27b3bdd500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c430a7a6-3f1b-49a6-93a5-61cb7a356051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15438c88-cc82-4eef-bdd1-fe27b3bdd500",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "c45c39f2-90b8-424f-a7e0-75bb7df37ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "99b4be8a-c6c2-44a2-958e-48bca78339f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45c39f2-90b8-424f-a7e0-75bb7df37ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b9139c-f122-4d03-adfa-20bf17fb2e59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45c39f2-90b8-424f-a7e0-75bb7df37ced",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "c8b4b09d-1add-4d35-9b00-1703e0aab45e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "dfc79fac-650f-41e3-a6a3-03996a6b11b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b4b09d-1add-4d35-9b00-1703e0aab45e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ad131d-6d79-4889-820c-dda9ef2cef1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b4b09d-1add-4d35-9b00-1703e0aab45e",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "55332da0-1de3-4e50-affa-a20dc20cf06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "6ea8ab90-bd7c-4270-8238-324d151aae07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55332da0-1de3-4e50-affa-a20dc20cf06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1af2bc78-7ad3-45a2-96ba-9ba4ed25beda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55332da0-1de3-4e50-affa-a20dc20cf06b",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "2f80afc0-3aa4-4b97-a38e-df63c671f3c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "1037fdcc-77c9-4401-b05c-200ef56e8279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f80afc0-3aa4-4b97-a38e-df63c671f3c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce6d799-0896-4ab4-9ee3-1b5d2fa3564f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f80afc0-3aa4-4b97-a38e-df63c671f3c9",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "024a5277-66fe-49de-9019-a2275101c1a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "a649c994-4fca-4347-9d02-3cd5d253635d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "024a5277-66fe-49de-9019-a2275101c1a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e94cb20c-c184-4a25-ac60-47e35ce7b5d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "024a5277-66fe-49de-9019-a2275101c1a3",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "9fe6ba60-2fc6-439e-8013-2f9abbc38ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "525cd277-c43c-46c0-851e-c26cdac85baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fe6ba60-2fc6-439e-8013-2f9abbc38ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4db72a-38bf-479e-93d0-8be321295443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fe6ba60-2fc6-439e-8013-2f9abbc38ca4",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "6b8ce48a-8e97-468c-8c62-1ba239c37a0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "0bc7b756-dfa6-43ff-b3e1-99f51c9747ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b8ce48a-8e97-468c-8c62-1ba239c37a0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de42b6d1-3d3e-4868-ab7f-c01d49e6efd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b8ce48a-8e97-468c-8c62-1ba239c37a0f",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "a2e71d76-dae4-4355-9e57-22203f6e3488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "612c3313-cd89-41ef-8e5e-a94a8bc3a600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2e71d76-dae4-4355-9e57-22203f6e3488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "456f4314-4ac8-4fe5-8f5c-62bdadd8b5cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2e71d76-dae4-4355-9e57-22203f6e3488",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "6913da6b-7a7f-4382-85b9-4982b22b8ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "cd822310-dd5d-41e0-872b-ab3930daae72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6913da6b-7a7f-4382-85b9-4982b22b8ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a587d92-6335-4982-ae9c-56f8b0ead1d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6913da6b-7a7f-4382-85b9-4982b22b8ddf",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "e6a94620-9d41-4ddd-a385-f88b1b3dd76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "9f73e88f-b840-4f38-9485-829ee504f634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a94620-9d41-4ddd-a385-f88b1b3dd76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe73f11-c1fc-4475-b3c0-c5febc54e991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a94620-9d41-4ddd-a385-f88b1b3dd76f",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "83ac01f1-82ed-47a6-b78b-0a014b58d5bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "341747de-e4d9-455f-b062-918a344ded46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ac01f1-82ed-47a6-b78b-0a014b58d5bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "940c8e47-d8ac-46fd-83ba-b3b36062782a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ac01f1-82ed-47a6-b78b-0a014b58d5bf",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "da848eeb-6132-4866-84a7-7c95f13541eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "e5e112b3-e786-4fe2-9a8b-b59a1d8c8835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da848eeb-6132-4866-84a7-7c95f13541eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b0e14dd-cdbf-4574-9303-362e01e40f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da848eeb-6132-4866-84a7-7c95f13541eb",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "09e6abc7-3ba7-4b45-823f-f43aa7cd1db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "df672af2-295e-4d19-9dc6-feddf9f74797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09e6abc7-3ba7-4b45-823f-f43aa7cd1db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff488279-d269-48f4-8f52-056f584c5045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09e6abc7-3ba7-4b45-823f-f43aa7cd1db2",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "83e08bca-aa6f-40eb-b640-6b48941c0726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "827d0aa0-9786-483c-84c7-507bfa2b7927",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e08bca-aa6f-40eb-b640-6b48941c0726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba817dcb-6b85-4666-bc19-019034ae8000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e08bca-aa6f-40eb-b640-6b48941c0726",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "1dd468e8-6d01-4279-8514-2bd1c6206772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "8523a8f3-1197-43b3-a3eb-43e801ca63ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dd468e8-6d01-4279-8514-2bd1c6206772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f347bac1-d34e-4bf1-8149-1594f35e0d6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dd468e8-6d01-4279-8514-2bd1c6206772",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "361647ab-27b9-4e73-a077-99c626db88ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "bd0fe237-d9a9-4f00-bd53-83e8ed7a6e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "361647ab-27b9-4e73-a077-99c626db88ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644ce903-dd3e-44be-b922-7a3da0df418c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "361647ab-27b9-4e73-a077-99c626db88ea",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "4d8e5afa-3e94-48db-8f76-8d5ad0b9864c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "b9f91624-c9f0-4048-982e-6e5648f112c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d8e5afa-3e94-48db-8f76-8d5ad0b9864c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bbad23-c0bc-4c78-b603-bacefec1a3a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d8e5afa-3e94-48db-8f76-8d5ad0b9864c",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "84cdd8a6-cd9f-4502-8813-5f2cab7b080f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "cc40529e-463d-4c62-abb0-77931a28ebf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84cdd8a6-cd9f-4502-8813-5f2cab7b080f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a6eed3f-90be-4e0d-954d-f44f509916e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84cdd8a6-cd9f-4502-8813-5f2cab7b080f",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "7fe04769-53b8-4929-add5-327af8b4e04d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "0fb01eaa-503b-4199-8cad-505ef69ab85c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe04769-53b8-4929-add5-327af8b4e04d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a939de52-121d-4a92-bf82-c316e1b35d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe04769-53b8-4929-add5-327af8b4e04d",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "4b6c09e5-3c2e-4670-a1dc-e50cd989214d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "b54e242b-c3a6-4168-bb34-00637cae36df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6c09e5-3c2e-4670-a1dc-e50cd989214d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f85ec9a8-f57c-4d2b-9216-108f01b62315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6c09e5-3c2e-4670-a1dc-e50cd989214d",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "d055fbc2-7ea1-454b-9321-760bf7034342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "68c424e1-41a9-4e16-a376-b3f5e0c10750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d055fbc2-7ea1-454b-9321-760bf7034342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acc0245-448d-446b-88d9-b5e3425c4df0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d055fbc2-7ea1-454b-9321-760bf7034342",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "01f96220-20d7-42e1-aa67-d058707ab6ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "37a29711-1ae6-4daf-9519-3dac1317ac03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01f96220-20d7-42e1-aa67-d058707ab6ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac00bb1d-2ff1-4885-9344-365c96106d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01f96220-20d7-42e1-aa67-d058707ab6ee",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "24af2dc4-7b94-4500-9674-d40085c109b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "58ccb6be-7a41-4297-b0bd-2c5f9f600026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24af2dc4-7b94-4500-9674-d40085c109b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa6a7c6-2b14-4253-9f9f-21543d5133e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24af2dc4-7b94-4500-9674-d40085c109b7",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        },
        {
            "id": "8c1e66ac-25b7-4936-bdbe-dc672df7ad1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "compositeImage": {
                "id": "8a39ddb8-2f7e-4216-845c-212cc8a483f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1e66ac-25b7-4936-bdbe-dc672df7ad1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c87207-0c0e-494a-b974-7f06ccc33798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1e66ac-25b7-4936-bdbe-dc672df7ad1c",
                    "LayerId": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "b747ee9c-e7fc-46f1-b201-71dc6faec6e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bb77e57-7c16-499e-a99d-a5affe52076f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}