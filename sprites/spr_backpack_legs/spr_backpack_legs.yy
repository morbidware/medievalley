{
    "id": "27df0702-e1c6-489c-ac41-7ac398deb676",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_backpack_legs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc2af7b7-f6c5-43fd-ad8e-32b85713c213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27df0702-e1c6-489c-ac41-7ac398deb676",
            "compositeImage": {
                "id": "a68bedaa-8188-472c-a566-632372bbada0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc2af7b7-f6c5-43fd-ad8e-32b85713c213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36df5967-b5d9-4cdb-976e-10b649485a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc2af7b7-f6c5-43fd-ad8e-32b85713c213",
                    "LayerId": "58ecdb28-0fa8-49bb-a1d7-1fe7ea834ac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "58ecdb28-0fa8-49bb-a1d7-1fe7ea834ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27df0702-e1c6-489c-ac41-7ac398deb676",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 6
}