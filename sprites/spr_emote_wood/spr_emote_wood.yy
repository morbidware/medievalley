{
    "id": "450f1c65-5ed4-4193-ae5d-6a558a422573",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd9840d0-a690-474e-a57c-94ccbf674666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "450f1c65-5ed4-4193-ae5d-6a558a422573",
            "compositeImage": {
                "id": "984a532c-9893-4c98-9d3c-fc89a9a5c727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9840d0-a690-474e-a57c-94ccbf674666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87edefe8-7fd3-453c-a445-66aea16d36fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9840d0-a690-474e-a57c-94ccbf674666",
                    "LayerId": "21f7c99d-5b35-4d74-a0ba-1bcae352cbc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "21f7c99d-5b35-4d74-a0ba-1bcae352cbc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "450f1c65-5ed4-4193-ae5d-6a558a422573",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 5
}