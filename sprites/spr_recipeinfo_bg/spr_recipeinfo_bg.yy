{
    "id": "83f6788e-d4d1-4880-8ec3-d4a97d5bfbae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recipeinfo_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 205,
    "bbox_left": 0,
    "bbox_right": 173,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f794574-670e-437d-9276-9716801c9bb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83f6788e-d4d1-4880-8ec3-d4a97d5bfbae",
            "compositeImage": {
                "id": "ea456a7e-7082-4016-9a12-aad784471276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f794574-670e-437d-9276-9716801c9bb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "179a8cc4-a3f8-4c58-8c92-d0509b07a4d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f794574-670e-437d-9276-9716801c9bb5",
                    "LayerId": "db603a13-1fcb-47a6-9652-f877ffb4940d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 206,
    "layers": [
        {
            "id": "db603a13-1fcb-47a6-9652-f877ffb4940d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83f6788e-d4d1-4880-8ec3-d4a97d5bfbae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 174,
    "xorig": 0,
    "yorig": 0
}