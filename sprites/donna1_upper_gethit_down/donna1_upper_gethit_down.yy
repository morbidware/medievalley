{
    "id": "9f92bb53-179a-40d0-a9f4-2d3d617efaa0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b6d8e55-06fc-46d0-abf2-deea3800fea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f92bb53-179a-40d0-a9f4-2d3d617efaa0",
            "compositeImage": {
                "id": "9ddb5941-247d-4500-b7f5-f5f00dbe5b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6d8e55-06fc-46d0-abf2-deea3800fea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1ae0a6b-2f8c-42fc-b5db-59174545d095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6d8e55-06fc-46d0-abf2-deea3800fea3",
                    "LayerId": "22f424fb-5640-4276-9e28-31679e7de8de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "22f424fb-5640-4276-9e28-31679e7de8de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f92bb53-179a-40d0-a9f4-2d3d617efaa0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}