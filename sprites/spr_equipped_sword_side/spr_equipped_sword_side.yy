{
    "id": "8d9f1b49-34ba-43c1-a9c2-b5e85b6958fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_sword_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "058c3d75-407e-49ca-a939-ac74f02fe562",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d9f1b49-34ba-43c1-a9c2-b5e85b6958fa",
            "compositeImage": {
                "id": "9d17d51a-d18d-409e-bf76-553f57a7676a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "058c3d75-407e-49ca-a939-ac74f02fe562",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf6b4b1a-8507-4e02-a03b-99be33292c30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "058c3d75-407e-49ca-a939-ac74f02fe562",
                    "LayerId": "91a5e38b-bb82-4091-971f-e8bc0fddeef1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "91a5e38b-bb82-4091-971f-e8bc0fddeef1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d9f1b49-34ba-43c1-a9c2-b5e85b6958fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 1,
    "yorig": 14
}