{
    "id": "d4248387-2363-4daf-a3ce-4737c55d41ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow_cliff",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f48bf49-8ec8-4548-881d-37dccdf86b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4248387-2363-4daf-a3ce-4737c55d41ca",
            "compositeImage": {
                "id": "d3bdca15-fef6-450e-8047-53644eaf3d64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f48bf49-8ec8-4548-881d-37dccdf86b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "991f45fd-1108-423b-baad-0ab3f75687ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f48bf49-8ec8-4548-881d-37dccdf86b7e",
                    "LayerId": "2f34bdaf-a81a-45af-80c8-bfbe28e5e88d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2f34bdaf-a81a-45af-80c8-bfbe28e5e88d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4248387-2363-4daf-a3ce-4737c55d41ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 56
}