{
    "id": "6b8b0344-c0b8-4c72-b03a-52263e131187",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_audio_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cdeab16-695e-41b3-a380-ca7e9460fe6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b8b0344-c0b8-4c72-b03a-52263e131187",
            "compositeImage": {
                "id": "f6dbb12d-4438-41ec-8ca4-a41aec043b8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cdeab16-695e-41b3-a380-ca7e9460fe6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09c382fc-e9e7-4c4d-a5e1-7fc3cc6a8ffb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdeab16-695e-41b3-a380-ca7e9460fe6d",
                    "LayerId": "e2c29ff2-1b9e-47ed-98b6-ce5bf728ae64"
                },
                {
                    "id": "67353841-e3cc-460b-aadf-31986b0f787a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdeab16-695e-41b3-a380-ca7e9460fe6d",
                    "LayerId": "0423e2e7-d679-4a09-87fd-21347f8c6b8c"
                }
            ]
        },
        {
            "id": "ae7a8ba8-a33c-4fbd-a0e1-ab2d534179e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b8b0344-c0b8-4c72-b03a-52263e131187",
            "compositeImage": {
                "id": "c68dbee3-348c-4c1b-8a8a-ebd34c34fe64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae7a8ba8-a33c-4fbd-a0e1-ab2d534179e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d239fcf9-c11c-4feb-8fe6-76097da351e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae7a8ba8-a33c-4fbd-a0e1-ab2d534179e7",
                    "LayerId": "e2c29ff2-1b9e-47ed-98b6-ce5bf728ae64"
                },
                {
                    "id": "9604399c-18f8-4f77-b16c-4d9d158dc198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae7a8ba8-a33c-4fbd-a0e1-ab2d534179e7",
                    "LayerId": "0423e2e7-d679-4a09-87fd-21347f8c6b8c"
                }
            ]
        },
        {
            "id": "ad1a35fb-a405-44de-b352-d60004ee8a05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b8b0344-c0b8-4c72-b03a-52263e131187",
            "compositeImage": {
                "id": "9f7775d3-72a1-4295-9be8-b8b8522f81d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad1a35fb-a405-44de-b352-d60004ee8a05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9e75ea2-27c3-4672-ab4d-f4d53a02d924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1a35fb-a405-44de-b352-d60004ee8a05",
                    "LayerId": "e2c29ff2-1b9e-47ed-98b6-ce5bf728ae64"
                },
                {
                    "id": "e05e8564-9a2e-494f-ab8d-00de38e99f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1a35fb-a405-44de-b352-d60004ee8a05",
                    "LayerId": "0423e2e7-d679-4a09-87fd-21347f8c6b8c"
                }
            ]
        },
        {
            "id": "afc0a1e9-9a92-4e04-b9b6-3b702470d0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b8b0344-c0b8-4c72-b03a-52263e131187",
            "compositeImage": {
                "id": "777a6367-5775-4754-8663-78b640df024c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afc0a1e9-9a92-4e04-b9b6-3b702470d0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fad0730-466a-42d6-a5ff-4ea5bdb17b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc0a1e9-9a92-4e04-b9b6-3b702470d0a9",
                    "LayerId": "e2c29ff2-1b9e-47ed-98b6-ce5bf728ae64"
                },
                {
                    "id": "85da0c2c-1e19-4ad4-9834-8a4ead16c075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afc0a1e9-9a92-4e04-b9b6-3b702470d0a9",
                    "LayerId": "0423e2e7-d679-4a09-87fd-21347f8c6b8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "0423e2e7-d679-4a09-87fd-21347f8c6b8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b8b0344-c0b8-4c72-b03a-52263e131187",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e2c29ff2-1b9e-47ed-98b6-ce5bf728ae64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b8b0344-c0b8-4c72-b03a-52263e131187",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}