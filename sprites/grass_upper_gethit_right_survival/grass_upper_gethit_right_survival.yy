{
    "id": "a4afc761-61dc-4d34-9e75-6519c66ad49c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_upper_gethit_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25717d1b-666c-43df-9858-163e599b716b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4afc761-61dc-4d34-9e75-6519c66ad49c",
            "compositeImage": {
                "id": "765909e8-e6de-4d6e-a2d9-a99d7de4e5e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25717d1b-666c-43df-9858-163e599b716b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9e559ad-01d5-4ac8-a0b4-fbcd5ec32ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25717d1b-666c-43df-9858-163e599b716b",
                    "LayerId": "cc86ad61-a507-4636-98bb-75f25b9cdf5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cc86ad61-a507-4636-98bb-75f25b9cdf5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4afc761-61dc-4d34-9e75-6519c66ad49c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}