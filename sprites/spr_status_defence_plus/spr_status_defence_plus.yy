{
    "id": "8454e664-4bd8-4a1d-8170-7e957468e01d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_status_defence_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d74ef7fd-efea-4f5a-9e75-23a9ddef4f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8454e664-4bd8-4a1d-8170-7e957468e01d",
            "compositeImage": {
                "id": "e19db354-f285-45fe-9875-111cbe36f7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d74ef7fd-efea-4f5a-9e75-23a9ddef4f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80f1b26e-3d49-459d-a14b-3e6ddc58a0d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d74ef7fd-efea-4f5a-9e75-23a9ddef4f82",
                    "LayerId": "05bffd4d-0f3f-42fd-ac89-b3dbb7a8222a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "05bffd4d-0f3f-42fd-ac89-b3dbb7a8222a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8454e664-4bd8-4a1d-8170-7e957468e01d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}