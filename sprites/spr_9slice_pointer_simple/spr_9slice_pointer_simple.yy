{
    "id": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_9slice_pointer_simple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48e2e35a-074a-453d-8f1b-c96704e33167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "2291b510-3018-4e32-a402-7a37ad91f1ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48e2e35a-074a-453d-8f1b-c96704e33167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4d5209-9661-4740-a0d3-693b392b8433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48e2e35a-074a-453d-8f1b-c96704e33167",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "4ea2ae41-3263-4ffb-8b05-13528e0447c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48e2e35a-074a-453d-8f1b-c96704e33167",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "8e5042ba-4879-410e-b235-2d5964fcacd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "6b8d2f59-b3ef-4f75-a595-3251e1e6ff10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5042ba-4879-410e-b235-2d5964fcacd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "155dc320-4316-493b-a542-89db6bebfbb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5042ba-4879-410e-b235-2d5964fcacd3",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "6f882aaa-d2ec-478b-8cd3-2ede620cdd4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5042ba-4879-410e-b235-2d5964fcacd3",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "d5c82969-cd5c-46a9-8c0b-90a803acf988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "184b5a66-ef71-4e4a-97ef-2989b583bfe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5c82969-cd5c-46a9-8c0b-90a803acf988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f46b74dc-a223-455b-bd71-aee973a718ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c82969-cd5c-46a9-8c0b-90a803acf988",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "5e0f1c9d-73ca-436b-85dd-3a671fa06d18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5c82969-cd5c-46a9-8c0b-90a803acf988",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "26576c29-6d89-46d9-991e-030b418b820d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "4eae0a91-557a-461d-804a-ee434f265381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26576c29-6d89-46d9-991e-030b418b820d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e6af95-8092-4957-a70d-53a2b304e6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26576c29-6d89-46d9-991e-030b418b820d",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "1ce13f3c-530c-4566-af4a-b808379e36b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26576c29-6d89-46d9-991e-030b418b820d",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "57db7688-002c-4ecc-9532-0a2f22d0bfe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "cf99532c-a638-4b92-b024-3fdba4913602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57db7688-002c-4ecc-9532-0a2f22d0bfe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f832f6fe-dd89-42d8-aa6a-6b169f5aaea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57db7688-002c-4ecc-9532-0a2f22d0bfe0",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "2212b3a9-897e-4b83-85a9-e3ec8186c77e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57db7688-002c-4ecc-9532-0a2f22d0bfe0",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "2440f4cf-4994-40f6-bbc2-9112a810db54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "f7f9bacf-0ef0-4b35-8dc3-1d04ba5ec6ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2440f4cf-4994-40f6-bbc2-9112a810db54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "462c4dbc-fc1d-448c-8561-a15773b29dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2440f4cf-4994-40f6-bbc2-9112a810db54",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "119c6ce4-4f57-4062-a699-fe20c4b4078c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2440f4cf-4994-40f6-bbc2-9112a810db54",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "f2fa5b40-1bff-4b29-a863-065f71f1d304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "a1ccbed9-7601-4fcc-b564-ee250f897e39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2fa5b40-1bff-4b29-a863-065f71f1d304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "620cf9c3-a6f1-4b6a-a7b9-d8dd20100ba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2fa5b40-1bff-4b29-a863-065f71f1d304",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "627beb02-5856-4329-9142-8da9b5070e7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2fa5b40-1bff-4b29-a863-065f71f1d304",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "06e70dc6-d536-49ec-ae3e-c51bd51a4ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "64954ff3-7560-4663-b75b-8e85144d09d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06e70dc6-d536-49ec-ae3e-c51bd51a4ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8702d738-9d2e-4832-931b-945afb21f699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e70dc6-d536-49ec-ae3e-c51bd51a4ba4",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "09e65ee5-09dc-4e6c-a968-4514481120d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06e70dc6-d536-49ec-ae3e-c51bd51a4ba4",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        },
        {
            "id": "73628735-6728-41eb-9628-758d600d0d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "compositeImage": {
                "id": "35258c81-a0f2-4f6c-9156-ce39a3b6c77e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73628735-6728-41eb-9628-758d600d0d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab9afbd-1d2a-465d-af0a-afa10e54b15e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73628735-6728-41eb-9628-758d600d0d15",
                    "LayerId": "8c8f4856-f953-481a-bbed-e8a72738927c"
                },
                {
                    "id": "52bf5065-cd79-4060-999c-e47d819a5ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73628735-6728-41eb-9628-758d600d0d15",
                    "LayerId": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "8c8f4856-f953-481a-bbed-e8a72738927c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "733bcf9c-c459-4d29-bb76-abcb8f1aebfc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "251d53cc-4ad9-4e52-8ee3-ba5e0a513d61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}