{
    "id": "beb59ffb-b11e-48e9-a6c1-5c0514ec8e09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25c27701-99c2-4aba-894d-875b7d1955ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb59ffb-b11e-48e9-a6c1-5c0514ec8e09",
            "compositeImage": {
                "id": "f1fabd03-5562-4c4f-9e44-e18c87bf27cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25c27701-99c2-4aba-894d-875b7d1955ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c978b583-80c2-4f4b-b230-43f15aaf9aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25c27701-99c2-4aba-894d-875b7d1955ac",
                    "LayerId": "950685d8-d7bd-4f28-9760-8ff8c00becfb"
                }
            ]
        },
        {
            "id": "12559ae4-c777-4982-ae92-d905b6263717",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb59ffb-b11e-48e9-a6c1-5c0514ec8e09",
            "compositeImage": {
                "id": "5745b536-5dfd-4e29-8529-cf4133f092e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12559ae4-c777-4982-ae92-d905b6263717",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86e12f0-abb9-44f0-b15c-8f7caa4aab69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12559ae4-c777-4982-ae92-d905b6263717",
                    "LayerId": "950685d8-d7bd-4f28-9760-8ff8c00becfb"
                }
            ]
        },
        {
            "id": "4348de90-4088-4ccc-b241-76bfcff43ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb59ffb-b11e-48e9-a6c1-5c0514ec8e09",
            "compositeImage": {
                "id": "44397924-e9b4-422e-9b64-9d94fd93790a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4348de90-4088-4ccc-b241-76bfcff43ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "520a1daf-f437-4005-a1fd-1ac08518cf89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4348de90-4088-4ccc-b241-76bfcff43ecd",
                    "LayerId": "950685d8-d7bd-4f28-9760-8ff8c00becfb"
                }
            ]
        },
        {
            "id": "ca5ff1ef-5bf5-481d-808c-ca82447c6144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb59ffb-b11e-48e9-a6c1-5c0514ec8e09",
            "compositeImage": {
                "id": "d69c57a3-c6c2-4147-8b26-788650eb2f38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca5ff1ef-5bf5-481d-808c-ca82447c6144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a160f7-5713-491a-9cfc-02f35d191a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca5ff1ef-5bf5-481d-808c-ca82447c6144",
                    "LayerId": "950685d8-d7bd-4f28-9760-8ff8c00becfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "950685d8-d7bd-4f28-9760-8ff8c00becfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beb59ffb-b11e-48e9-a6c1-5c0514ec8e09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}