{
    "id": "c9be6008-e968-43e8-88a1-9b2f4e454564",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_amanitamushroom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bb6628b-adc4-4eef-9a12-56397d14567f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9be6008-e968-43e8-88a1-9b2f4e454564",
            "compositeImage": {
                "id": "c4083a0c-77e3-4455-8143-21850ade105d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bb6628b-adc4-4eef-9a12-56397d14567f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d74e0a3-46ad-4442-ae44-cd78c1a15caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bb6628b-adc4-4eef-9a12-56397d14567f",
                    "LayerId": "28b81c94-5d58-43c4-843a-85aaebee6895"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "28b81c94-5d58-43c4-843a-85aaebee6895",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9be6008-e968-43e8-88a1-9b2f4e454564",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}