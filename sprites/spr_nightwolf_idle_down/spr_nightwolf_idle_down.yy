{
    "id": "d764e9a6-dd97-4b88-9025-537f3d4a2c40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 17,
    "bbox_right": 32,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b919e863-b9e9-4009-b36a-1b098c10c6d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d764e9a6-dd97-4b88-9025-537f3d4a2c40",
            "compositeImage": {
                "id": "86182507-71a7-41eb-8267-d89f42bccf11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b919e863-b9e9-4009-b36a-1b098c10c6d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e88c8f-cfee-4ae5-bcd9-7dee5981831d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b919e863-b9e9-4009-b36a-1b098c10c6d1",
                    "LayerId": "21ee256a-c148-4dd3-8eb8-3b52431f03fb"
                }
            ]
        },
        {
            "id": "2409be69-5bf7-4182-935a-67e53e0a6101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d764e9a6-dd97-4b88-9025-537f3d4a2c40",
            "compositeImage": {
                "id": "2c5db9f1-e4f6-4fde-8a2f-4c553f3d4887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2409be69-5bf7-4182-935a-67e53e0a6101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "188bd2e5-8486-4ec9-ac9d-34ce6c869319",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2409be69-5bf7-4182-935a-67e53e0a6101",
                    "LayerId": "21ee256a-c148-4dd3-8eb8-3b52431f03fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "21ee256a-c148-4dd3-8eb8-3b52431f03fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d764e9a6-dd97-4b88-9025-537f3d4a2c40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}