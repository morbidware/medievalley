{
    "id": "b81419dc-baf0-45a2-8c8f-a98bb8309be0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_gethit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7618cc17-741b-4b25-aaae-7aed41393656",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b81419dc-baf0-45a2-8c8f-a98bb8309be0",
            "compositeImage": {
                "id": "698aac28-20de-48c7-a3b8-a4a9bc354c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7618cc17-741b-4b25-aaae-7aed41393656",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "587ae6b8-640a-4f08-97ed-c87d9c859859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7618cc17-741b-4b25-aaae-7aed41393656",
                    "LayerId": "57d0f422-d171-401b-9781-9f040db55f83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "57d0f422-d171-401b-9781-9f040db55f83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b81419dc-baf0-45a2-8c8f-a98bb8309be0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}