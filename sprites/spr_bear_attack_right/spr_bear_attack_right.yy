{
    "id": "babca0c9-ae18-4cde-83a2-5eb1f34c032f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88ae86f6-3e21-4f35-99de-b56eabba88f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "babca0c9-ae18-4cde-83a2-5eb1f34c032f",
            "compositeImage": {
                "id": "0d1f61fa-433b-4f0f-a449-98c00b961459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ae86f6-3e21-4f35-99de-b56eabba88f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "656f029f-bbab-4565-9247-8a5813d7bcce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ae86f6-3e21-4f35-99de-b56eabba88f8",
                    "LayerId": "19f7ed1b-f46a-4870-ab21-20ede744bb62"
                }
            ]
        },
        {
            "id": "5292a725-7111-4acf-acff-1a275c249ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "babca0c9-ae18-4cde-83a2-5eb1f34c032f",
            "compositeImage": {
                "id": "e0024354-81af-4558-8681-c8c8f4f126a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5292a725-7111-4acf-acff-1a275c249ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e9400a-76f3-437d-8d48-50acfc90b1d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5292a725-7111-4acf-acff-1a275c249ef5",
                    "LayerId": "19f7ed1b-f46a-4870-ab21-20ede744bb62"
                }
            ]
        },
        {
            "id": "c6e2c901-b2fe-4933-80c9-f68e5ff3b14f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "babca0c9-ae18-4cde-83a2-5eb1f34c032f",
            "compositeImage": {
                "id": "aa9a7530-7d11-48a4-bfff-fb969ccb667c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e2c901-b2fe-4933-80c9-f68e5ff3b14f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f41e887-e369-40d8-9793-f75a74f0dff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e2c901-b2fe-4933-80c9-f68e5ff3b14f",
                    "LayerId": "19f7ed1b-f46a-4870-ab21-20ede744bb62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "19f7ed1b-f46a-4870-ab21-20ede744bb62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "babca0c9-ae18-4cde-83a2-5eb1f34c032f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 16,
    "yorig": 44
}