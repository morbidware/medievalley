{
    "id": "8ccfde87-ba71-4486-826a-58087c0f1bda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_carrot_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bbd2e5a-e60c-4a7b-bc33-76ccfe1a009d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ccfde87-ba71-4486-826a-58087c0f1bda",
            "compositeImage": {
                "id": "b035e63a-3091-4333-becf-d13bb0b63c2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bbd2e5a-e60c-4a7b-bc33-76ccfe1a009d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "246455bb-3673-4433-8f41-e61758d0bdc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bbd2e5a-e60c-4a7b-bc33-76ccfe1a009d",
                    "LayerId": "967a592f-c6f0-4fc3-8ee4-61e7895c5386"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "967a592f-c6f0-4fc3-8ee4-61e7895c5386",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ccfde87-ba71-4486-826a-58087c0f1bda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}