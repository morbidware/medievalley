{
    "id": "8f9e315a-d536-443d-88d5-c44c6dc91fe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_walk_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 33,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c8e8dda-688a-4bde-b9c8-b8d6bea4aaca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f9e315a-d536-443d-88d5-c44c6dc91fe3",
            "compositeImage": {
                "id": "9cba1010-fa5d-4aab-b5b5-822bf2813144",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c8e8dda-688a-4bde-b9c8-b8d6bea4aaca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4dabb9-cfb2-493e-ae96-a85feb5d69ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c8e8dda-688a-4bde-b9c8-b8d6bea4aaca",
                    "LayerId": "f1a96e22-6072-45ed-9cb8-bb8c58bfff6b"
                }
            ]
        },
        {
            "id": "cdfbcf47-c16a-4664-8ade-4707ddc8fd61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f9e315a-d536-443d-88d5-c44c6dc91fe3",
            "compositeImage": {
                "id": "eac9e5a6-f78d-4bb4-baa0-9e1f355a15f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdfbcf47-c16a-4664-8ade-4707ddc8fd61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c204efdc-c34c-4b6a-9212-e9c53cedd94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdfbcf47-c16a-4664-8ade-4707ddc8fd61",
                    "LayerId": "f1a96e22-6072-45ed-9cb8-bb8c58bfff6b"
                }
            ]
        },
        {
            "id": "e57f288a-eb73-4975-a108-ec60b4b200eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f9e315a-d536-443d-88d5-c44c6dc91fe3",
            "compositeImage": {
                "id": "71b49519-31c1-4ba9-beec-6090b066fb85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e57f288a-eb73-4975-a108-ec60b4b200eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4333c241-58f7-4a87-9399-85c4b5a4fbf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e57f288a-eb73-4975-a108-ec60b4b200eb",
                    "LayerId": "f1a96e22-6072-45ed-9cb8-bb8c58bfff6b"
                }
            ]
        },
        {
            "id": "e29fd35e-37ad-4fcb-a2aa-be3c602d709c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f9e315a-d536-443d-88d5-c44c6dc91fe3",
            "compositeImage": {
                "id": "eee035b5-8c86-4b03-81a0-8aa85a8bee54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e29fd35e-37ad-4fcb-a2aa-be3c602d709c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7f6ee8-5bd5-4692-badb-e57df78147e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e29fd35e-37ad-4fcb-a2aa-be3c602d709c",
                    "LayerId": "f1a96e22-6072-45ed-9cb8-bb8c58bfff6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "f1a96e22-6072-45ed-9cb8-bb8c58bfff6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f9e315a-d536-443d-88d5-c44c6dc91fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}