{
    "id": "c91ab69a-0996-47bf-b405-a7776c73e854",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_redcabbage_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f73f904c-5205-4256-899f-c803188aa19f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c91ab69a-0996-47bf-b405-a7776c73e854",
            "compositeImage": {
                "id": "5d79f29a-7c2a-457d-a2d6-9dd031b99e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f73f904c-5205-4256-899f-c803188aa19f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a109e36-02d9-43ba-9a52-1537d431eb1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f73f904c-5205-4256-899f-c803188aa19f",
                    "LayerId": "82448e53-47cc-4355-9a90-8d23f4fb738e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "82448e53-47cc-4355-9a90-8d23f4fb738e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c91ab69a-0996-47bf-b405-a7776c73e854",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}