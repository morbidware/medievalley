{
    "id": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_olivetree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 6,
    "bbox_right": 53,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3eed1655-671d-4210-85e4-64b109301323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "compositeImage": {
                "id": "30f87181-fdfc-44d1-bae8-bd93c52447a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eed1655-671d-4210-85e4-64b109301323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb3fb16f-6829-4a7a-bc15-91e68bedf559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eed1655-671d-4210-85e4-64b109301323",
                    "LayerId": "385cf844-9425-4d29-9508-c32463484bc3"
                }
            ]
        },
        {
            "id": "f3c9a1c4-6e82-447e-818a-8d40e1f2e8e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "compositeImage": {
                "id": "8f703f30-ea03-45cc-a138-613603ed84e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3c9a1c4-6e82-447e-818a-8d40e1f2e8e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c26a603f-a464-40ba-8c71-6bf813c8a6a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3c9a1c4-6e82-447e-818a-8d40e1f2e8e1",
                    "LayerId": "385cf844-9425-4d29-9508-c32463484bc3"
                }
            ]
        },
        {
            "id": "dbfe5b69-c276-405d-8910-3c66d8d80715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "compositeImage": {
                "id": "b32dd878-fe18-4faa-9a7b-d5fd2bfe55ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbfe5b69-c276-405d-8910-3c66d8d80715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d36d3c2f-3f8e-4805-a632-a2a4bb8cd042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbfe5b69-c276-405d-8910-3c66d8d80715",
                    "LayerId": "385cf844-9425-4d29-9508-c32463484bc3"
                }
            ]
        },
        {
            "id": "c6716ad6-f3bc-4319-ac79-fe1fcda7cceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "compositeImage": {
                "id": "b09723e7-c59b-49c3-848c-27bc766e2957",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6716ad6-f3bc-4319-ac79-fe1fcda7cceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc160845-1d8e-4a9d-a70e-4e275738f13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6716ad6-f3bc-4319-ac79-fe1fcda7cceb",
                    "LayerId": "385cf844-9425-4d29-9508-c32463484bc3"
                }
            ]
        },
        {
            "id": "3b21bc20-3aa3-4382-8b86-093e8a6a25d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "compositeImage": {
                "id": "c150fcef-cccc-427c-a6c7-5ef9e99eee3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b21bc20-3aa3-4382-8b86-093e8a6a25d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae6ebdb-fb33-4a93-b0dd-37a349561877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b21bc20-3aa3-4382-8b86-093e8a6a25d5",
                    "LayerId": "385cf844-9425-4d29-9508-c32463484bc3"
                }
            ]
        },
        {
            "id": "13fec34d-fd14-421a-8230-821e47a3e010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "compositeImage": {
                "id": "f95ec90f-d71a-4b00-9ead-509cbbd301eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13fec34d-fd14-421a-8230-821e47a3e010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc7a6fb-b6d0-413c-b5be-8919f8b44c49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fec34d-fd14-421a-8230-821e47a3e010",
                    "LayerId": "385cf844-9425-4d29-9508-c32463484bc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "385cf844-9425-4d29-9508-c32463484bc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0a300a1-1ab3-483c-8f95-5b5af5dda901",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 29,
    "yorig": 70
}