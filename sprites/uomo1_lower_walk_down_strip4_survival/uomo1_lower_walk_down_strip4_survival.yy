{
    "id": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_walk_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8ae8ed5-2e95-49a1-b80b-7ca4bbce9861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "compositeImage": {
                "id": "4eb4cd85-82b2-46a7-abfc-94f8ae3a2213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ae8ed5-2e95-49a1-b80b-7ca4bbce9861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a65b68-53ca-43a2-9b37-0b431753de01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ae8ed5-2e95-49a1-b80b-7ca4bbce9861",
                    "LayerId": "77d4a062-9290-42f9-b766-10468aa5de1e"
                }
            ]
        },
        {
            "id": "b97530eb-4e89-4b34-86a3-82310ff9ba24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "compositeImage": {
                "id": "7c66886b-6f54-40f7-8cb5-14ae27bf0903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97530eb-4e89-4b34-86a3-82310ff9ba24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fe97242-1122-44ff-9f47-5f1186c294a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97530eb-4e89-4b34-86a3-82310ff9ba24",
                    "LayerId": "77d4a062-9290-42f9-b766-10468aa5de1e"
                }
            ]
        },
        {
            "id": "cffd370d-db96-47b5-b5c4-bd5946735cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "compositeImage": {
                "id": "98f16a7f-3f75-48e3-a9de-c77d48942926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cffd370d-db96-47b5-b5c4-bd5946735cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b4ef10-27cf-4d90-9e43-1b2fae5c3b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cffd370d-db96-47b5-b5c4-bd5946735cb8",
                    "LayerId": "77d4a062-9290-42f9-b766-10468aa5de1e"
                }
            ]
        },
        {
            "id": "ec9615f4-848c-41ff-be50-0a56ae07d20b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "compositeImage": {
                "id": "44303830-b465-42e4-95c3-ba240b223574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9615f4-848c-41ff-be50-0a56ae07d20b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98e967b3-fad5-4bf1-9140-b6fbe8dc9b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9615f4-848c-41ff-be50-0a56ae07d20b",
                    "LayerId": "77d4a062-9290-42f9-b766-10468aa5de1e"
                }
            ]
        },
        {
            "id": "d6d2b6b7-1594-4256-86da-5d7e552a5fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "compositeImage": {
                "id": "2bb83ef4-d541-414e-87f7-96ab14286a4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d2b6b7-1594-4256-86da-5d7e552a5fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6282bd5-8b51-4ecc-8bd1-7bc568d36d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d2b6b7-1594-4256-86da-5d7e552a5fc7",
                    "LayerId": "77d4a062-9290-42f9-b766-10468aa5de1e"
                }
            ]
        },
        {
            "id": "b5f2ac17-5332-47be-85d1-bfd47aa338db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "compositeImage": {
                "id": "a364bd1e-5acd-4077-a4d3-ce8fffd96537",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f2ac17-5332-47be-85d1-bfd47aa338db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4258cd-29f8-4b31-884b-6b4d6121dadb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f2ac17-5332-47be-85d1-bfd47aa338db",
                    "LayerId": "77d4a062-9290-42f9-b766-10468aa5de1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "77d4a062-9290-42f9-b766-10468aa5de1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a618afa7-4b2a-49f1-bd6b-9fa96668b94c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}