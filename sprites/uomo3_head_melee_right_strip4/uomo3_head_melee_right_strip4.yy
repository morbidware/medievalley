{
    "id": "943a95a8-47f9-44da-95f7-205e1e8a6040",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26072ec8-bcec-4a99-9295-5976565bf3c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943a95a8-47f9-44da-95f7-205e1e8a6040",
            "compositeImage": {
                "id": "bcf020f7-e249-4390-b7d9-de7ab923f3c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26072ec8-bcec-4a99-9295-5976565bf3c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac46d5c-3c8b-45e4-a8be-e10e3b8f5087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26072ec8-bcec-4a99-9295-5976565bf3c1",
                    "LayerId": "6daffadd-ef25-4784-8a9f-918598e55aad"
                }
            ]
        },
        {
            "id": "97dd6a10-27ca-4b53-9254-0797b4c9e495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943a95a8-47f9-44da-95f7-205e1e8a6040",
            "compositeImage": {
                "id": "9bbc4a01-862b-4638-8931-c19a7f98708c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97dd6a10-27ca-4b53-9254-0797b4c9e495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff5b21f-a498-4d1c-8cb7-eab86703581a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97dd6a10-27ca-4b53-9254-0797b4c9e495",
                    "LayerId": "6daffadd-ef25-4784-8a9f-918598e55aad"
                }
            ]
        },
        {
            "id": "9af9d771-b97e-45ec-9118-044c07baa68b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943a95a8-47f9-44da-95f7-205e1e8a6040",
            "compositeImage": {
                "id": "8239cbee-a396-4aa8-bfcf-29b1fcaed8ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af9d771-b97e-45ec-9118-044c07baa68b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73f9bd3a-d021-4cbb-8448-607ce4a12b26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af9d771-b97e-45ec-9118-044c07baa68b",
                    "LayerId": "6daffadd-ef25-4784-8a9f-918598e55aad"
                }
            ]
        },
        {
            "id": "1a87357f-f0ac-4f26-8056-3871a539dc12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943a95a8-47f9-44da-95f7-205e1e8a6040",
            "compositeImage": {
                "id": "ad11e755-9e9b-4f12-863f-c2bf47a8aad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a87357f-f0ac-4f26-8056-3871a539dc12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51133cba-3b76-424d-9468-d3ea7591cdf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a87357f-f0ac-4f26-8056-3871a539dc12",
                    "LayerId": "6daffadd-ef25-4784-8a9f-918598e55aad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6daffadd-ef25-4784-8a9f-918598e55aad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "943a95a8-47f9-44da-95f7-205e1e8a6040",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}