{
    "id": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_melee_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4af3370-b76b-446e-a705-1bcafdda7a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "compositeImage": {
                "id": "e4f2e12b-4ee5-4cd3-8986-f28c782f9553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4af3370-b76b-446e-a705-1bcafdda7a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d70a71b-ab36-4d01-b6ee-dfc4f5d7433a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4af3370-b76b-446e-a705-1bcafdda7a2a",
                    "LayerId": "04f84903-7cd2-4e41-8bd9-57fd2c571f91"
                }
            ]
        },
        {
            "id": "b8eeb063-43dc-4509-a51e-551fd0267d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "compositeImage": {
                "id": "9fa07726-b8b3-4afb-b1bf-47d8407b09de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8eeb063-43dc-4509-a51e-551fd0267d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c86c636-009e-4d16-8d38-2199aea1c0bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8eeb063-43dc-4509-a51e-551fd0267d41",
                    "LayerId": "04f84903-7cd2-4e41-8bd9-57fd2c571f91"
                }
            ]
        },
        {
            "id": "8f8203d0-8de3-47cd-af3a-c5d8d6e898d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "compositeImage": {
                "id": "0c1a86d2-a432-480c-ab60-079b4140ddce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8203d0-8de3-47cd-af3a-c5d8d6e898d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16969345-7fb0-4913-9d01-a32414a0809f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8203d0-8de3-47cd-af3a-c5d8d6e898d6",
                    "LayerId": "04f84903-7cd2-4e41-8bd9-57fd2c571f91"
                }
            ]
        },
        {
            "id": "71b8cb83-dfd4-41ad-a508-4bb2493358f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "compositeImage": {
                "id": "87840a6c-dc0a-4db4-a3d2-1099b22c0291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b8cb83-dfd4-41ad-a508-4bb2493358f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90fe9c9b-4542-416a-84dc-e9d3aecb0ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b8cb83-dfd4-41ad-a508-4bb2493358f1",
                    "LayerId": "04f84903-7cd2-4e41-8bd9-57fd2c571f91"
                }
            ]
        },
        {
            "id": "3aee10f3-1701-499b-bb93-bbd30dd7ffaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "compositeImage": {
                "id": "cf23a728-7b9b-449e-8662-b6ce065288e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aee10f3-1701-499b-bb93-bbd30dd7ffaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d044da8b-0162-42d8-b033-1b8006962810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aee10f3-1701-499b-bb93-bbd30dd7ffaf",
                    "LayerId": "04f84903-7cd2-4e41-8bd9-57fd2c571f91"
                }
            ]
        },
        {
            "id": "f4e43f2f-b47b-45c6-90e7-d4d7be194a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "compositeImage": {
                "id": "63cc58ca-a449-47ea-9df4-a767f77bec88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4e43f2f-b47b-45c6-90e7-d4d7be194a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ae4a8d-f0cc-4a13-8c1d-7f789334b41d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4e43f2f-b47b-45c6-90e7-d4d7be194a63",
                    "LayerId": "04f84903-7cd2-4e41-8bd9-57fd2c571f91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "04f84903-7cd2-4e41-8bd9-57fd2c571f91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a325962-33ba-4fb9-8c2d-f6018b4960e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}