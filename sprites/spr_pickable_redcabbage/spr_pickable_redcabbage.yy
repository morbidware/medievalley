{
    "id": "bbd9bbd0-09fc-4ff8-b767-c0722394136a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_redcabbage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5c90f6a-6b97-49e4-88a1-99747c2f85b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbd9bbd0-09fc-4ff8-b767-c0722394136a",
            "compositeImage": {
                "id": "36017e80-37dc-4930-8cb6-b34d809d20e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5c90f6a-6b97-49e4-88a1-99747c2f85b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969d15f2-dc81-4c75-ab0e-944a2370b6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5c90f6a-6b97-49e4-88a1-99747c2f85b7",
                    "LayerId": "4cce9ad7-709f-4ef9-bdb7-770e59c853e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4cce9ad7-709f-4ef9-bdb7-770e59c853e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbd9bbd0-09fc-4ff8-b767-c0722394136a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}