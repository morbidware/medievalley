{
    "id": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9af48cb-44a2-46b3-b25c-0b071353b7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
            "compositeImage": {
                "id": "bf182c58-776a-4528-976f-e10c8fea792c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9af48cb-44a2-46b3-b25c-0b071353b7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a6449d-f786-49af-9ad3-97bba05950cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9af48cb-44a2-46b3-b25c-0b071353b7e4",
                    "LayerId": "a6987dd5-b591-4ebc-bf35-b3a7f3f1a453"
                }
            ]
        },
        {
            "id": "bf19232a-370c-4132-87c9-deee7351b478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
            "compositeImage": {
                "id": "bb7332da-24bd-4a2e-b758-c2c2df436c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf19232a-370c-4132-87c9-deee7351b478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17c55da-db8f-495b-b7af-2f41df5ee464",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf19232a-370c-4132-87c9-deee7351b478",
                    "LayerId": "a6987dd5-b591-4ebc-bf35-b3a7f3f1a453"
                }
            ]
        },
        {
            "id": "fd8be48f-dade-4ebd-8e87-71abacd5b217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
            "compositeImage": {
                "id": "db2e2705-dada-4127-83fb-8071800d8c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd8be48f-dade-4ebd-8e87-71abacd5b217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cc6bd0a-5a20-4a84-8eed-0c422cf56f43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd8be48f-dade-4ebd-8e87-71abacd5b217",
                    "LayerId": "a6987dd5-b591-4ebc-bf35-b3a7f3f1a453"
                }
            ]
        },
        {
            "id": "d847fe82-7f89-4d79-a883-d3f580adde84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
            "compositeImage": {
                "id": "bc5d006a-6d22-4aa4-9f76-d76b04b4a47b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d847fe82-7f89-4d79-a883-d3f580adde84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f914ac0b-032d-40da-ada9-28ab094d24a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d847fe82-7f89-4d79-a883-d3f580adde84",
                    "LayerId": "a6987dd5-b591-4ebc-bf35-b3a7f3f1a453"
                }
            ]
        },
        {
            "id": "9000bfe2-0640-4a47-a78b-4c3bbc6c87b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
            "compositeImage": {
                "id": "3e2208a1-f9bd-44d4-a6d0-3ecb495d2a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9000bfe2-0640-4a47-a78b-4c3bbc6c87b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80dcdc26-4198-48e7-8ddd-65e655ba7b0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9000bfe2-0640-4a47-a78b-4c3bbc6c87b2",
                    "LayerId": "a6987dd5-b591-4ebc-bf35-b3a7f3f1a453"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a6987dd5-b591-4ebc-bf35-b3a7f3f1a453",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a25041f-e4c4-4879-9ebf-d2086ebae0c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}