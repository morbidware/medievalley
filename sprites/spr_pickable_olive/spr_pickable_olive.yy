{
    "id": "5d44d6bf-7d3f-40b7-bc88-76ec8eaa8633",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_olive",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "416714b3-b601-458e-b75c-7cee81363814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d44d6bf-7d3f-40b7-bc88-76ec8eaa8633",
            "compositeImage": {
                "id": "ae021864-9fda-4074-bd01-a48f5b12fa80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "416714b3-b601-458e-b75c-7cee81363814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2102c0-a9cb-4686-b79d-5a227d58edf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "416714b3-b601-458e-b75c-7cee81363814",
                    "LayerId": "cd63f07f-c9d0-4324-b280-730df10428ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cd63f07f-c9d0-4324-b280-730df10428ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d44d6bf-7d3f-40b7-bc88-76ec8eaa8633",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}