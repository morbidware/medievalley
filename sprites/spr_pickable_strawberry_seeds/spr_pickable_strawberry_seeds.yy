{
    "id": "727f278c-bac9-4267-850d-a6eff996386b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_strawberry_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "302a966f-e1bf-498c-abda-3499db1642bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "727f278c-bac9-4267-850d-a6eff996386b",
            "compositeImage": {
                "id": "7699ced2-b245-4c1f-8d15-b3a414bbbebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302a966f-e1bf-498c-abda-3499db1642bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8982938-c7cc-47f0-b78c-8bc9c47730c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302a966f-e1bf-498c-abda-3499db1642bc",
                    "LayerId": "2ac5dbd2-34cf-4fa5-ba4f-51332c3efe1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2ac5dbd2-34cf-4fa5-ba4f-51332c3efe1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "727f278c-bac9-4267-850d-a6eff996386b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}