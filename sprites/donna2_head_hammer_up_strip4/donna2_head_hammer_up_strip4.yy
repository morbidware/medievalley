{
    "id": "317e69f5-9859-4ecf-9d61-d3225c8c7675",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "edf62121-4444-482f-92d6-f08ab920ed9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "317e69f5-9859-4ecf-9d61-d3225c8c7675",
            "compositeImage": {
                "id": "64a25568-b501-45e0-b25d-02f432be490e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edf62121-4444-482f-92d6-f08ab920ed9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10eaffa2-a1a2-4aed-b2bd-a24ef25f302b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edf62121-4444-482f-92d6-f08ab920ed9b",
                    "LayerId": "1c75f3f4-a316-4d5b-a8ee-109caf6e8dac"
                }
            ]
        },
        {
            "id": "fe64b558-95fb-4a5d-9709-281c650ccdfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "317e69f5-9859-4ecf-9d61-d3225c8c7675",
            "compositeImage": {
                "id": "10db3063-97a4-495a-b771-f628e4fd86e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe64b558-95fb-4a5d-9709-281c650ccdfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "937734fe-6a8c-4379-ad7c-dd6bdd16b9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe64b558-95fb-4a5d-9709-281c650ccdfa",
                    "LayerId": "1c75f3f4-a316-4d5b-a8ee-109caf6e8dac"
                }
            ]
        },
        {
            "id": "34846e2d-f3c4-4a32-a5c4-eb5f9996b723",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "317e69f5-9859-4ecf-9d61-d3225c8c7675",
            "compositeImage": {
                "id": "1b8bbf20-c8f1-4d80-a96e-a1cfe2872094",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34846e2d-f3c4-4a32-a5c4-eb5f9996b723",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed91d4c-0e30-45b1-8b1b-27ca4465a3fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34846e2d-f3c4-4a32-a5c4-eb5f9996b723",
                    "LayerId": "1c75f3f4-a316-4d5b-a8ee-109caf6e8dac"
                }
            ]
        },
        {
            "id": "c72b4a64-3b4d-4b05-9ca1-904d9cf4b1d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "317e69f5-9859-4ecf-9d61-d3225c8c7675",
            "compositeImage": {
                "id": "721e37bc-a96c-4227-8327-f161f04085c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c72b4a64-3b4d-4b05-9ca1-904d9cf4b1d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f870eaf-58d8-4c65-8363-681a813e2362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c72b4a64-3b4d-4b05-9ca1-904d9cf4b1d9",
                    "LayerId": "1c75f3f4-a316-4d5b-a8ee-109caf6e8dac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1c75f3f4-a316-4d5b-a8ee-109caf6e8dac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "317e69f5-9859-4ecf-9d61-d3225c8c7675",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}