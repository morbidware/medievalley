{
    "id": "20fe7a07-1f15-4822-8818-411ad96a0529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8620948-7a39-44d7-8600-30a6ad7f4b33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20fe7a07-1f15-4822-8818-411ad96a0529",
            "compositeImage": {
                "id": "1ec6c433-092e-4dde-9ced-27e96b16e85a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8620948-7a39-44d7-8600-30a6ad7f4b33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087e4f65-0f85-46cc-a35c-581f619a1fb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8620948-7a39-44d7-8600-30a6ad7f4b33",
                    "LayerId": "ed1bb5c1-e859-4de5-9d21-9e5bef0034b0"
                }
            ]
        },
        {
            "id": "9ad31ff0-10a4-4559-a339-961dc2023298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20fe7a07-1f15-4822-8818-411ad96a0529",
            "compositeImage": {
                "id": "14719f2a-7111-4aec-b352-22041952d5b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad31ff0-10a4-4559-a339-961dc2023298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd24eb88-563f-49dd-8628-6f670a609e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad31ff0-10a4-4559-a339-961dc2023298",
                    "LayerId": "ed1bb5c1-e859-4de5-9d21-9e5bef0034b0"
                }
            ]
        },
        {
            "id": "955939d3-029b-4b5a-885e-54b26666d3d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20fe7a07-1f15-4822-8818-411ad96a0529",
            "compositeImage": {
                "id": "4694909e-6f53-49e6-8b58-c9b31f762043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955939d3-029b-4b5a-885e-54b26666d3d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cedc3a9b-2f8a-42de-a886-7e888a0975e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955939d3-029b-4b5a-885e-54b26666d3d0",
                    "LayerId": "ed1bb5c1-e859-4de5-9d21-9e5bef0034b0"
                }
            ]
        },
        {
            "id": "0497f28c-9808-4b26-b014-bd613d661842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20fe7a07-1f15-4822-8818-411ad96a0529",
            "compositeImage": {
                "id": "f5d787fb-44c0-4bb3-b984-4d1d41cf31f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0497f28c-9808-4b26-b014-bd613d661842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbe5e203-870c-4409-99ae-38881367cf27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0497f28c-9808-4b26-b014-bd613d661842",
                    "LayerId": "ed1bb5c1-e859-4de5-9d21-9e5bef0034b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ed1bb5c1-e859-4de5-9d21-9e5bef0034b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20fe7a07-1f15-4822-8818-411ad96a0529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}