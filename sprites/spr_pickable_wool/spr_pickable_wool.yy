{
    "id": "3f0c9eca-4522-4671-80d4-3cf24db36630",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_wool",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b389d7cf-c293-4aa3-9e35-6f07e058101c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f0c9eca-4522-4671-80d4-3cf24db36630",
            "compositeImage": {
                "id": "cfb1b88e-ce73-43f9-b76e-f488f562d9d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b389d7cf-c293-4aa3-9e35-6f07e058101c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd121704-7028-4c7f-b2d0-597223c9f043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b389d7cf-c293-4aa3-9e35-6f07e058101c",
                    "LayerId": "4314f552-1bb5-4ec7-b3dd-396c1cd93da8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4314f552-1bb5-4ec7-b3dd-396c1cd93da8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f0c9eca-4522-4671-80d4-3cf24db36630",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}