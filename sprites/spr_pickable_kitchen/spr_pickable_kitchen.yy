{
    "id": "817576ac-f7ad-442d-89f9-d3b26f08d453",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_kitchen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb9b03b9-e177-4e21-a7b9-9f9094856d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "817576ac-f7ad-442d-89f9-d3b26f08d453",
            "compositeImage": {
                "id": "2b9039e8-e9a6-4165-8c07-479e52b52205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9b03b9-e177-4e21-a7b9-9f9094856d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2fda31-7f49-4a7d-a0c3-d2bd9e923e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9b03b9-e177-4e21-a7b9-9f9094856d6d",
                    "LayerId": "13978fea-3d8f-41d1-95ea-5816e34c24bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "13978fea-3d8f-41d1-95ea-5816e34c24bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "817576ac-f7ad-442d-89f9-d3b26f08d453",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}