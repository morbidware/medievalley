{
    "id": "dcdfaf4e-cdd5-42dc-b95d-e7a936224e89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5bd95e3-da13-4a67-9296-48d5f209252e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdfaf4e-cdd5-42dc-b95d-e7a936224e89",
            "compositeImage": {
                "id": "0f5df78f-1181-4e6b-b8e6-23d25b74277d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5bd95e3-da13-4a67-9296-48d5f209252e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a9a264-7499-4a72-a59c-9bc59cead769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5bd95e3-da13-4a67-9296-48d5f209252e",
                    "LayerId": "bc8be893-88ee-4395-bc19-06e22f6736e2"
                }
            ]
        },
        {
            "id": "9bd1acf4-bf5a-4500-ae15-282c58837bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdfaf4e-cdd5-42dc-b95d-e7a936224e89",
            "compositeImage": {
                "id": "07eac6f2-66cf-4faa-a675-4fa4dee81399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bd1acf4-bf5a-4500-ae15-282c58837bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb2d88f0-054a-43f8-80dc-01dd911d889c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bd1acf4-bf5a-4500-ae15-282c58837bd5",
                    "LayerId": "bc8be893-88ee-4395-bc19-06e22f6736e2"
                }
            ]
        },
        {
            "id": "e147869a-7628-40e1-91c5-92544c8b0226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdfaf4e-cdd5-42dc-b95d-e7a936224e89",
            "compositeImage": {
                "id": "e6d2d994-b2c9-4f80-83bf-d86ff02a8c69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e147869a-7628-40e1-91c5-92544c8b0226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5199c9-d786-4bd2-8555-1a2891da472d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e147869a-7628-40e1-91c5-92544c8b0226",
                    "LayerId": "bc8be893-88ee-4395-bc19-06e22f6736e2"
                }
            ]
        },
        {
            "id": "b92b3bf6-4d6a-4a50-960a-047635dbc1cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcdfaf4e-cdd5-42dc-b95d-e7a936224e89",
            "compositeImage": {
                "id": "23dcd299-234f-4d5e-84c5-959a9285c740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92b3bf6-4d6a-4a50-960a-047635dbc1cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208e94a4-f891-4080-879b-381f52b15842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92b3bf6-4d6a-4a50-960a-047635dbc1cc",
                    "LayerId": "bc8be893-88ee-4395-bc19-06e22f6736e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc8be893-88ee-4395-bc19-06e22f6736e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcdfaf4e-cdd5-42dc-b95d-e7a936224e89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}