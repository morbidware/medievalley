{
    "id": "73d133f8-66a9-466c-8148-0cc83cd6c8bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0871631c-1b27-4133-a6de-291bb2baacdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73d133f8-66a9-466c-8148-0cc83cd6c8bf",
            "compositeImage": {
                "id": "54df3b4f-5e7d-4d63-837e-e8cb07997e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0871631c-1b27-4133-a6de-291bb2baacdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab7c1ba-1416-4953-8919-d687b114ead5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0871631c-1b27-4133-a6de-291bb2baacdb",
                    "LayerId": "e4467972-2dfa-45f6-8ef2-906cd09cb2cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e4467972-2dfa-45f6-8ef2-906cd09cb2cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73d133f8-66a9-466c-8148-0cc83cd6c8bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}