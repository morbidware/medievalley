{
    "id": "4968f4a1-0282-4426-871c-526e2bf120c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_hammer_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2e85828-c008-41dd-a3e4-110c913f3f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "compositeImage": {
                "id": "fece90cc-e479-43f6-bdef-6ea9ce978af3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2e85828-c008-41dd-a3e4-110c913f3f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8936a137-de8f-4936-8d09-d1c42a283ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2e85828-c008-41dd-a3e4-110c913f3f86",
                    "LayerId": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe"
                }
            ]
        },
        {
            "id": "8904831f-1de9-4f1a-9699-90c272d6d8e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "compositeImage": {
                "id": "d837053e-9e03-48cf-b4e8-964313c0f820",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8904831f-1de9-4f1a-9699-90c272d6d8e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92a6b481-de58-42f4-8bf7-f8090e39c9a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8904831f-1de9-4f1a-9699-90c272d6d8e5",
                    "LayerId": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe"
                }
            ]
        },
        {
            "id": "07871982-31b4-467c-8d92-c1be7cee5d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "compositeImage": {
                "id": "9564fe8b-25fa-4790-866e-be663fdf6def",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07871982-31b4-467c-8d92-c1be7cee5d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e90f92e-dd12-4c07-b066-d5c16040402c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07871982-31b4-467c-8d92-c1be7cee5d25",
                    "LayerId": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe"
                }
            ]
        },
        {
            "id": "5dfa9cdc-4229-483f-86a6-e3b05b6bdfb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "compositeImage": {
                "id": "749eeb55-9b1e-4347-a679-ce91d8268dcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dfa9cdc-4229-483f-86a6-e3b05b6bdfb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e5c60cc-db59-49e7-ba36-afc7833f64af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dfa9cdc-4229-483f-86a6-e3b05b6bdfb8",
                    "LayerId": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe"
                }
            ]
        },
        {
            "id": "50d2d29a-a7ca-452a-8c21-038fb39d2c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "compositeImage": {
                "id": "98e87bdf-1f5b-433a-bd25-6faacbfaaf67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d2d29a-a7ca-452a-8c21-038fb39d2c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772c6fb8-c3fd-4e5d-935a-589847157926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d2d29a-a7ca-452a-8c21-038fb39d2c47",
                    "LayerId": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe"
                }
            ]
        },
        {
            "id": "aa5027ee-e420-4435-accd-5fe15fae6f93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "compositeImage": {
                "id": "0e7f2347-b516-49dd-9fd7-cb49f65b93a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5027ee-e420-4435-accd-5fe15fae6f93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207ed6d8-c025-4f83-8c48-b4d3a3c9e32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5027ee-e420-4435-accd-5fe15fae6f93",
                    "LayerId": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1c2055d1-366a-4c5f-8ba6-9aa5007a21fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4968f4a1-0282-4426-871c-526e2bf120c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}