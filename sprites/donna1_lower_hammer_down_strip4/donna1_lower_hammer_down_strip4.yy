{
    "id": "77e89c8c-e409-4b81-b9ff-c09458777796",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "245124ca-06b1-4430-a7f0-969ff39846cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e89c8c-e409-4b81-b9ff-c09458777796",
            "compositeImage": {
                "id": "2ea59dda-0852-4631-98de-b822e9794c52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "245124ca-06b1-4430-a7f0-969ff39846cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd65028d-8690-4372-8c0a-b446da00c7d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "245124ca-06b1-4430-a7f0-969ff39846cc",
                    "LayerId": "bb09be77-777a-4896-9126-57b8baacb2c2"
                }
            ]
        },
        {
            "id": "08820185-ce69-4aae-a149-1ef720b48f85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e89c8c-e409-4b81-b9ff-c09458777796",
            "compositeImage": {
                "id": "6bff3daf-90dc-447d-908c-bee6be7c3d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08820185-ce69-4aae-a149-1ef720b48f85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69611551-decd-4411-9734-962e1f05807c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08820185-ce69-4aae-a149-1ef720b48f85",
                    "LayerId": "bb09be77-777a-4896-9126-57b8baacb2c2"
                }
            ]
        },
        {
            "id": "5e995d8e-8296-473e-a960-2066c7dfeb69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e89c8c-e409-4b81-b9ff-c09458777796",
            "compositeImage": {
                "id": "d355c6fa-d2ec-423b-85ca-295f046aa748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e995d8e-8296-473e-a960-2066c7dfeb69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3025df-4012-445d-aae7-811b625a3160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e995d8e-8296-473e-a960-2066c7dfeb69",
                    "LayerId": "bb09be77-777a-4896-9126-57b8baacb2c2"
                }
            ]
        },
        {
            "id": "8340c8cb-8c24-46d0-aa1b-142881e64687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77e89c8c-e409-4b81-b9ff-c09458777796",
            "compositeImage": {
                "id": "bac13e57-cb98-4e13-8dc6-11e34aaa7223",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8340c8cb-8c24-46d0-aa1b-142881e64687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8c47bb6-f375-4009-823e-58764e84f51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8340c8cb-8c24-46d0-aa1b-142881e64687",
                    "LayerId": "bb09be77-777a-4896-9126-57b8baacb2c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bb09be77-777a-4896-9126-57b8baacb2c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77e89c8c-e409-4b81-b9ff-c09458777796",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}