{
    "id": "5f4cdf31-64bf-4bbb-9117-771309be209b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_redcabbage_seeds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf38fc61-2614-4448-b986-9868be3578f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f4cdf31-64bf-4bbb-9117-771309be209b",
            "compositeImage": {
                "id": "5bb6207f-c11b-47d0-a79b-41c4c0a852a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf38fc61-2614-4448-b986-9868be3578f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d130a599-4e13-43e2-bc18-f4a4237aa7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf38fc61-2614-4448-b986-9868be3578f2",
                    "LayerId": "3fe2820b-5bee-499b-8dc1-e0ba3c8f3010"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3fe2820b-5bee-499b-8dc1-e0ba3c8f3010",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f4cdf31-64bf-4bbb-9117-771309be209b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}