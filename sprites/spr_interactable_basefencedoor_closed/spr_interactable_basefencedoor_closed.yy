{
    "id": "bb26fb67-4526-41d8-beab-57209fa1246e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_basefencedoor_closed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9511ca28-8a2f-4bb1-9500-3f5b65ba52ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb26fb67-4526-41d8-beab-57209fa1246e",
            "compositeImage": {
                "id": "0f5c3c59-270e-4a60-a216-13d90ceaa574",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9511ca28-8a2f-4bb1-9500-3f5b65ba52ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "215a7515-c514-4cda-9322-fad9c350c563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9511ca28-8a2f-4bb1-9500-3f5b65ba52ee",
                    "LayerId": "fea72ed7-9a11-46fe-b7e5-00c8916147cf"
                }
            ]
        },
        {
            "id": "0227979e-4b61-461a-bf5d-10e76f404a3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb26fb67-4526-41d8-beab-57209fa1246e",
            "compositeImage": {
                "id": "32e604d3-cfac-44dc-b859-d770d8d50c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0227979e-4b61-461a-bf5d-10e76f404a3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1d4007-5e7a-43bd-bb2a-ffae19a289a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0227979e-4b61-461a-bf5d-10e76f404a3e",
                    "LayerId": "fea72ed7-9a11-46fe-b7e5-00c8916147cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fea72ed7-9a11-46fe-b7e5-00c8916147cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb26fb67-4526-41d8-beab-57209fa1246e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 29
}