{
    "id": "1281e640-87a8-4bf5-8c19-9df112b4e32e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_attack_down_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 22,
    "bbox_right": 27,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec8fe316-7c7d-41a0-b096-be803aad6c41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281e640-87a8-4bf5-8c19-9df112b4e32e",
            "compositeImage": {
                "id": "99412926-45aa-4a57-91b3-98f8c5c5766b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8fe316-7c7d-41a0-b096-be803aad6c41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ba677ec-a69a-490d-b829-5c0ad7a58aa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8fe316-7c7d-41a0-b096-be803aad6c41",
                    "LayerId": "365dde9c-db43-4c11-8cd3-91ae7af53922"
                }
            ]
        },
        {
            "id": "238db33e-d811-4cf8-853a-c2fba556f9e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281e640-87a8-4bf5-8c19-9df112b4e32e",
            "compositeImage": {
                "id": "2eedda70-62bb-4c27-9861-dc59bc09fba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "238db33e-d811-4cf8-853a-c2fba556f9e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b5b095f-32b4-4dd1-8baa-b5ceabca1683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "238db33e-d811-4cf8-853a-c2fba556f9e4",
                    "LayerId": "365dde9c-db43-4c11-8cd3-91ae7af53922"
                }
            ]
        },
        {
            "id": "6a882dde-3c89-4f19-ad31-30404b192e8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1281e640-87a8-4bf5-8c19-9df112b4e32e",
            "compositeImage": {
                "id": "78930ea0-c78c-460d-9a7c-2069b18c8eb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a882dde-3c89-4f19-ad31-30404b192e8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a496ca82-cbda-4bdb-b7f9-2211c699233b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a882dde-3c89-4f19-ad31-30404b192e8b",
                    "LayerId": "365dde9c-db43-4c11-8cd3-91ae7af53922"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "365dde9c-db43-4c11-8cd3-91ae7af53922",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1281e640-87a8-4bf5-8c19-9df112b4e32e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}