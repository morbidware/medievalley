{
    "id": "c166a508-91bf-4d8c-9867-77dc5b916395",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a4d162b-54f8-46dd-ac75-e3fe5190018a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c166a508-91bf-4d8c-9867-77dc5b916395",
            "compositeImage": {
                "id": "4509c2e6-3db5-4196-8fbe-c2844387717f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4d162b-54f8-46dd-ac75-e3fe5190018a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b0afb7-da3a-45ba-8009-1fdd2fe872d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4d162b-54f8-46dd-ac75-e3fe5190018a",
                    "LayerId": "f7a9e8ba-e40e-4319-b9ad-fb79b6d97b08"
                }
            ]
        },
        {
            "id": "f2f31d0b-a321-42f1-a1cd-b021590883aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c166a508-91bf-4d8c-9867-77dc5b916395",
            "compositeImage": {
                "id": "fee08cbb-fa86-4a22-859d-639eb9c7f57d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2f31d0b-a321-42f1-a1cd-b021590883aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55db5cf9-1380-4ed7-aeb0-4bb702dc0680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2f31d0b-a321-42f1-a1cd-b021590883aa",
                    "LayerId": "f7a9e8ba-e40e-4319-b9ad-fb79b6d97b08"
                }
            ]
        },
        {
            "id": "b71b86a7-d21b-470e-a3b7-41251f98e10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c166a508-91bf-4d8c-9867-77dc5b916395",
            "compositeImage": {
                "id": "fb149a06-be9d-415e-a78e-8fead2d2e560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71b86a7-d21b-470e-a3b7-41251f98e10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce9b3628-ebc0-48a9-97a7-26795717b1dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71b86a7-d21b-470e-a3b7-41251f98e10c",
                    "LayerId": "f7a9e8ba-e40e-4319-b9ad-fb79b6d97b08"
                }
            ]
        },
        {
            "id": "2b7c3d48-acea-454a-bee0-b326a6e2deaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c166a508-91bf-4d8c-9867-77dc5b916395",
            "compositeImage": {
                "id": "94d2dff9-0040-4ad1-a300-860cb982bb4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b7c3d48-acea-454a-bee0-b326a6e2deaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53bf8be8-bef5-4feb-935e-0d079f1736ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b7c3d48-acea-454a-bee0-b326a6e2deaf",
                    "LayerId": "f7a9e8ba-e40e-4319-b9ad-fb79b6d97b08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f7a9e8ba-e40e-4319-b9ad-fb79b6d97b08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c166a508-91bf-4d8c-9867-77dc5b916395",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}