{
    "id": "341a4cca-65af-4837-8376-f04d87cab6dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fx_sweat_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89f129ff-d2ee-4590-a663-0fc8854314ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "341a4cca-65af-4837-8376-f04d87cab6dc",
            "compositeImage": {
                "id": "125f55fd-9ed0-48bf-a2dd-12ba8b17eb48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f129ff-d2ee-4590-a663-0fc8854314ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e34ec37-b500-42db-b69d-3a7acfe35076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f129ff-d2ee-4590-a663-0fc8854314ae",
                    "LayerId": "9e051e67-bc6c-464c-ba3d-9425eb351c46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "9e051e67-bc6c-464c-ba3d-9425eb351c46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "341a4cca-65af-4837-8376-f04d87cab6dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 2
}