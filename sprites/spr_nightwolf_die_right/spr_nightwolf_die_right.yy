{
    "id": "cd832640-0565-45e0-b515-1fdcb28f0707",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_die_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 7,
    "bbox_right": 40,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80d5b161-421c-4564-befe-6e8cc4c407cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd832640-0565-45e0-b515-1fdcb28f0707",
            "compositeImage": {
                "id": "778a2b6d-4897-4ce3-8b41-7437d4e6a2eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80d5b161-421c-4564-befe-6e8cc4c407cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11cae122-523b-4299-9d4c-5bb417f45c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80d5b161-421c-4564-befe-6e8cc4c407cd",
                    "LayerId": "3ac4475e-c993-4659-a8ed-b010082353d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "3ac4475e-c993-4659-a8ed-b010082353d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd832640-0565-45e0-b515-1fdcb28f0707",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}