{
    "id": "b7b0a8ab-462d-4f0e-a52d-1bad50a8f07f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6459d481-e26a-4477-953c-b8aa316fad2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7b0a8ab-462d-4f0e-a52d-1bad50a8f07f",
            "compositeImage": {
                "id": "66d2582c-7a0e-4225-b189-05ab9acfae2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6459d481-e26a-4477-953c-b8aa316fad2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c7040d-d671-45a8-bf8e-2364e178166a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6459d481-e26a-4477-953c-b8aa316fad2f",
                    "LayerId": "60e326cb-2bf7-4418-a303-777a3447df62"
                }
            ]
        },
        {
            "id": "4a07b567-71a5-4e6b-8cd4-c62df1105fd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7b0a8ab-462d-4f0e-a52d-1bad50a8f07f",
            "compositeImage": {
                "id": "3e9d9a65-8803-4b48-9d8a-8b4fc16c3c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a07b567-71a5-4e6b-8cd4-c62df1105fd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266ca943-4b2b-4ad9-80bf-009ac81eb06e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a07b567-71a5-4e6b-8cd4-c62df1105fd3",
                    "LayerId": "60e326cb-2bf7-4418-a303-777a3447df62"
                }
            ]
        },
        {
            "id": "66ae096b-1841-4e6e-90a3-90eb7cf6f299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7b0a8ab-462d-4f0e-a52d-1bad50a8f07f",
            "compositeImage": {
                "id": "84d11ad1-9c03-4c73-a86f-f6fb7f4d7497",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66ae096b-1841-4e6e-90a3-90eb7cf6f299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0ed352-3b32-40b9-b0aa-6be6315172be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66ae096b-1841-4e6e-90a3-90eb7cf6f299",
                    "LayerId": "60e326cb-2bf7-4418-a303-777a3447df62"
                }
            ]
        },
        {
            "id": "a550c71a-a9fd-47f5-ba9e-12987c0ce4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7b0a8ab-462d-4f0e-a52d-1bad50a8f07f",
            "compositeImage": {
                "id": "7275465b-0a39-4d1a-bf8b-9b94cbaa88b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a550c71a-a9fd-47f5-ba9e-12987c0ce4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b986d84-5606-4bc3-ad63-3b512aeb72bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a550c71a-a9fd-47f5-ba9e-12987c0ce4c4",
                    "LayerId": "60e326cb-2bf7-4418-a303-777a3447df62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "60e326cb-2bf7-4418-a303-777a3447df62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7b0a8ab-462d-4f0e-a52d-1bad50a8f07f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}