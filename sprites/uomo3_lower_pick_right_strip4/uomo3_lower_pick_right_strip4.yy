{
    "id": "1b17a4e4-68ed-4b46-8b18-21b10d997506",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64762ebb-f6fc-4ffe-aa9b-1e5f328e7daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b17a4e4-68ed-4b46-8b18-21b10d997506",
            "compositeImage": {
                "id": "072f9d14-8124-4e98-8d85-d8614138c2a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64762ebb-f6fc-4ffe-aa9b-1e5f328e7daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a6d0638-a530-4a75-baf3-0c93cf899218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64762ebb-f6fc-4ffe-aa9b-1e5f328e7daf",
                    "LayerId": "2f20af63-1629-44d8-b43c-5850a04512d1"
                }
            ]
        },
        {
            "id": "203c1ff4-b263-458d-82ed-ddd180f88d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b17a4e4-68ed-4b46-8b18-21b10d997506",
            "compositeImage": {
                "id": "7ae216f4-e27a-41e5-a6d2-a575dfbe2430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "203c1ff4-b263-458d-82ed-ddd180f88d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d23afb-030d-4365-ba04-acbcbe24427a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "203c1ff4-b263-458d-82ed-ddd180f88d93",
                    "LayerId": "2f20af63-1629-44d8-b43c-5850a04512d1"
                }
            ]
        },
        {
            "id": "9cb4e646-eed5-42df-bbc5-3df83320cb74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b17a4e4-68ed-4b46-8b18-21b10d997506",
            "compositeImage": {
                "id": "9c9403fa-1b0c-411f-9055-71ccfa0ae15b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cb4e646-eed5-42df-bbc5-3df83320cb74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "213ebe4c-e0ac-4ea3-9a93-b6027c952bf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cb4e646-eed5-42df-bbc5-3df83320cb74",
                    "LayerId": "2f20af63-1629-44d8-b43c-5850a04512d1"
                }
            ]
        },
        {
            "id": "99a9ee8e-1a71-4206-9c12-d5a91a282767",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b17a4e4-68ed-4b46-8b18-21b10d997506",
            "compositeImage": {
                "id": "be335a12-ef18-439e-9141-81ceaf316aac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99a9ee8e-1a71-4206-9c12-d5a91a282767",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bd17255-72ae-4317-9eaa-e3bea0d70c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99a9ee8e-1a71-4206-9c12-d5a91a282767",
                    "LayerId": "2f20af63-1629-44d8-b43c-5850a04512d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2f20af63-1629-44d8-b43c-5850a04512d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b17a4e4-68ed-4b46-8b18-21b10d997506",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}