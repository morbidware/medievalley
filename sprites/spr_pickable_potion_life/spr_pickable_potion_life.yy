{
    "id": "bb4b5805-f197-42e2-a7f8-d4b271260198",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_potion_life",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86f48d9d-5b2f-4e05-a42a-2506a8e45898",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb4b5805-f197-42e2-a7f8-d4b271260198",
            "compositeImage": {
                "id": "8fbbfe3d-12f5-415f-be1f-aaf54dc90dea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86f48d9d-5b2f-4e05-a42a-2506a8e45898",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88fa1d2e-460b-47ae-b123-4bdccfd287b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86f48d9d-5b2f-4e05-a42a-2506a8e45898",
                    "LayerId": "65227ef5-bdea-497d-8eba-7b903f880c2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "65227ef5-bdea-497d-8eba-7b903f880c2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb4b5805-f197-42e2-a7f8-d4b271260198",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}