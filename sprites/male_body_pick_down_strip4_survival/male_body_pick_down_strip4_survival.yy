{
    "id": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_pick_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1bc945f-fe07-456f-a64c-f73020343968",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "compositeImage": {
                "id": "b2cfabf9-c94f-4c58-bf22-27cd1a1dbb5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1bc945f-fe07-456f-a64c-f73020343968",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d804fb1-2370-4207-abcc-3194cccf4b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1bc945f-fe07-456f-a64c-f73020343968",
                    "LayerId": "e152506c-3904-400b-85bd-23c47297be2b"
                }
            ]
        },
        {
            "id": "b0608b3e-fd2b-43e7-bb8c-63800a34b6dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "compositeImage": {
                "id": "35bd3331-c0a4-4619-812e-ae7a5ada062d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0608b3e-fd2b-43e7-bb8c-63800a34b6dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382e4727-a522-4cae-bf0a-3f5ac07ecb8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0608b3e-fd2b-43e7-bb8c-63800a34b6dc",
                    "LayerId": "e152506c-3904-400b-85bd-23c47297be2b"
                }
            ]
        },
        {
            "id": "072e4a05-072d-46d2-9508-d507c4221771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "compositeImage": {
                "id": "c340b9a6-111d-4e29-ad6f-c91c47d584ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "072e4a05-072d-46d2-9508-d507c4221771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35f03e3-3f5e-4654-a8b0-5fef3909d289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "072e4a05-072d-46d2-9508-d507c4221771",
                    "LayerId": "e152506c-3904-400b-85bd-23c47297be2b"
                }
            ]
        },
        {
            "id": "79350b9d-513f-4828-b7bb-be4b26ec831f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "compositeImage": {
                "id": "600943db-58c0-4684-a079-977df764d530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79350b9d-513f-4828-b7bb-be4b26ec831f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9ead95-9816-48c1-8bdb-7f180c7d4041",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79350b9d-513f-4828-b7bb-be4b26ec831f",
                    "LayerId": "e152506c-3904-400b-85bd-23c47297be2b"
                }
            ]
        },
        {
            "id": "55687abd-6fb3-4f3d-92d4-66b2a4d65d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "compositeImage": {
                "id": "1bd9511f-804b-4b6f-9a6c-a05c43e00955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55687abd-6fb3-4f3d-92d4-66b2a4d65d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7276ba9-6916-4b95-8e92-02b786ab5bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55687abd-6fb3-4f3d-92d4-66b2a4d65d57",
                    "LayerId": "e152506c-3904-400b-85bd-23c47297be2b"
                }
            ]
        },
        {
            "id": "4bf2fd20-addc-4f2b-b82f-54030cd8ed68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "compositeImage": {
                "id": "24c6c876-388b-4a71-9e39-a72add3a50c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf2fd20-addc-4f2b-b82f-54030cd8ed68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25bb96bd-a83e-485a-be0e-1e4128b9d8af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf2fd20-addc-4f2b-b82f-54030cd8ed68",
                    "LayerId": "e152506c-3904-400b-85bd-23c47297be2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e152506c-3904-400b-85bd-23c47297be2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c3d19ff-6cde-4860-a8fc-60ac2b44c9f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}