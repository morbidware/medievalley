{
    "id": "f94ef537-871c-47c8-99a0-e0165753c9be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_stamina_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e5581dc-2717-49e1-8363-dc26033a8949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f94ef537-871c-47c8-99a0-e0165753c9be",
            "compositeImage": {
                "id": "a9c4ecd5-81dd-4038-aae4-1d2cd77e040f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e5581dc-2717-49e1-8363-dc26033a8949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "285e4a61-f0dd-47c2-98dd-a825a5e7cf82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e5581dc-2717-49e1-8363-dc26033a8949",
                    "LayerId": "b58053a0-09b3-4277-97e7-f4dac3c26ebf"
                }
            ]
        },
        {
            "id": "79c00ef2-916d-493b-809a-32eb81ed85b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f94ef537-871c-47c8-99a0-e0165753c9be",
            "compositeImage": {
                "id": "115b13f1-a013-4a0d-aaa6-5baf38547f74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79c00ef2-916d-493b-809a-32eb81ed85b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b51962af-e307-4435-9a5c-903007db6184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79c00ef2-916d-493b-809a-32eb81ed85b3",
                    "LayerId": "b58053a0-09b3-4277-97e7-f4dac3c26ebf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 69,
    "layers": [
        {
            "id": "b58053a0-09b3-4277-97e7-f4dac3c26ebf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f94ef537-871c-47c8-99a0-e0165753c9be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 68
}