{
    "id": "d843a798-9dce-40b7-9b2e-89e4d81491f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_gethit_up_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88f959aa-fd54-4abb-a23a-8c1f4fff7d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d843a798-9dce-40b7-9b2e-89e4d81491f1",
            "compositeImage": {
                "id": "af50299e-d451-4ca6-b3f3-226894b2660b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88f959aa-fd54-4abb-a23a-8c1f4fff7d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60cbd88a-8658-4a3f-a4df-639b9938f25d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88f959aa-fd54-4abb-a23a-8c1f4fff7d6d",
                    "LayerId": "4a1ce8ee-6d98-4d66-b728-f60eb085637f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4a1ce8ee-6d98-4d66-b728-f60eb085637f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d843a798-9dce-40b7-9b2e-89e4d81491f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}