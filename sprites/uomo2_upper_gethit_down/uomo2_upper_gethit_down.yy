{
    "id": "d4404b23-09c7-461c-aec0-c4a5f2316b15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40199a41-c80f-4411-99d6-6ff2b6eb5af6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4404b23-09c7-461c-aec0-c4a5f2316b15",
            "compositeImage": {
                "id": "9de205c0-d4e3-4993-87b3-8cb9a1635045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40199a41-c80f-4411-99d6-6ff2b6eb5af6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19dcc7d2-712d-4fa5-9c4a-7bd0672c4e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40199a41-c80f-4411-99d6-6ff2b6eb5af6",
                    "LayerId": "6372b792-71af-43a4-9d02-505e2af11efa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6372b792-71af-43a4-9d02-505e2af11efa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4404b23-09c7-461c-aec0-c4a5f2316b15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}