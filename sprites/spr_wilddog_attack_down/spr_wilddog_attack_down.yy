{
    "id": "b2261c54-9b3a-4fc2-a587-dca25a092a5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 13,
    "bbox_right": 26,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3d934a9-4cfb-45ad-aea8-7e94a8213833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2261c54-9b3a-4fc2-a587-dca25a092a5f",
            "compositeImage": {
                "id": "22930310-6148-471e-b61f-2d5fa129cb24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d934a9-4cfb-45ad-aea8-7e94a8213833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47bc8422-3ce9-4e21-a81a-d3618b8df5f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d934a9-4cfb-45ad-aea8-7e94a8213833",
                    "LayerId": "eabb79dc-88b6-4b8c-8580-726d4971c066"
                }
            ]
        },
        {
            "id": "425493ba-ec18-41c5-a0bc-e8917a04cbeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2261c54-9b3a-4fc2-a587-dca25a092a5f",
            "compositeImage": {
                "id": "369b5d69-e2f9-4c88-a99b-0976c80d68b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "425493ba-ec18-41c5-a0bc-e8917a04cbeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d82565-c2f5-4a74-980a-10783cb294ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "425493ba-ec18-41c5-a0bc-e8917a04cbeb",
                    "LayerId": "eabb79dc-88b6-4b8c-8580-726d4971c066"
                }
            ]
        },
        {
            "id": "854f0adf-4669-4ef8-b74d-3525e16869ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2261c54-9b3a-4fc2-a587-dca25a092a5f",
            "compositeImage": {
                "id": "7ff7d4e3-b244-409b-9744-b355c52194a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "854f0adf-4669-4ef8-b74d-3525e16869ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01635f01-f413-4bec-ac5e-34c552eeea66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "854f0adf-4669-4ef8-b74d-3525e16869ad",
                    "LayerId": "eabb79dc-88b6-4b8c-8580-726d4971c066"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eabb79dc-88b6-4b8c-8580-726d4971c066",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2261c54-9b3a-4fc2-a587-dca25a092a5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}