{
    "id": "5673838d-0b9f-40ba-9972-6a06854127a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc6afdef-95df-466d-bff4-99d988cef568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5673838d-0b9f-40ba-9972-6a06854127a8",
            "compositeImage": {
                "id": "e9d50abf-aa5a-42ae-8934-b406f6e5f30b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc6afdef-95df-466d-bff4-99d988cef568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec47a5f1-35b1-4dd4-b340-7d8f50cde0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc6afdef-95df-466d-bff4-99d988cef568",
                    "LayerId": "35436113-bba9-49fb-bc19-0af47923c88f"
                }
            ]
        },
        {
            "id": "5db79cfc-fa09-4a60-bdfc-eef3d2de59d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5673838d-0b9f-40ba-9972-6a06854127a8",
            "compositeImage": {
                "id": "a6e0371f-4a08-4ae9-a0c5-dab3d5d36267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5db79cfc-fa09-4a60-bdfc-eef3d2de59d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36da6780-b89f-467a-ac77-385e101c42cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5db79cfc-fa09-4a60-bdfc-eef3d2de59d9",
                    "LayerId": "35436113-bba9-49fb-bc19-0af47923c88f"
                }
            ]
        },
        {
            "id": "979f141a-d078-49ff-97ff-031556df2b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5673838d-0b9f-40ba-9972-6a06854127a8",
            "compositeImage": {
                "id": "1903d6d3-3a24-4f97-b5d1-a16abc3ef991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "979f141a-d078-49ff-97ff-031556df2b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "209af6c7-2f10-4a91-b602-929773dff896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "979f141a-d078-49ff-97ff-031556df2b61",
                    "LayerId": "35436113-bba9-49fb-bc19-0af47923c88f"
                }
            ]
        },
        {
            "id": "83bc287e-7380-46e4-9873-e1b87efe6512",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5673838d-0b9f-40ba-9972-6a06854127a8",
            "compositeImage": {
                "id": "8def246d-04bd-4941-a483-62f0a00e55a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83bc287e-7380-46e4-9873-e1b87efe6512",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46328b5a-c16e-4b16-b758-c43a8e246c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83bc287e-7380-46e4-9873-e1b87efe6512",
                    "LayerId": "35436113-bba9-49fb-bc19-0af47923c88f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "35436113-bba9-49fb-bc19-0af47923c88f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5673838d-0b9f-40ba-9972-6a06854127a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}