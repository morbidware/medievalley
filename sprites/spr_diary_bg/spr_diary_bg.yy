{
    "id": "5727dfad-24c5-4e52-9090-9598a585d00b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_diary_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6032831d-b8ba-49a4-ab4d-1cca0faccaae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5727dfad-24c5-4e52-9090-9598a585d00b",
            "compositeImage": {
                "id": "9ee7aa43-ee9f-46c5-8a34-09befceb4d3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6032831d-b8ba-49a4-ab4d-1cca0faccaae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a7330df-9ea0-443d-8751-8bf5609f3ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6032831d-b8ba-49a4-ab4d-1cca0faccaae",
                    "LayerId": "e28d7417-dbc4-494c-b8f2-f4e49fe1a647"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 198,
    "layers": [
        {
            "id": "e28d7417-dbc4-494c-b8f2-f4e49fe1a647",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5727dfad-24c5-4e52-9090-9598a585d00b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 177,
    "yorig": 114
}