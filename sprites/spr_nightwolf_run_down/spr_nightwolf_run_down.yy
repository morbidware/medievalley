{
    "id": "c843d217-9529-4c75-8878-fbcd9f382723",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4e54521-ceab-47a7-bd90-bcbafcf9e100",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "compositeImage": {
                "id": "92858cd5-5597-4c6d-be52-a3e7e3abc97d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4e54521-ceab-47a7-bd90-bcbafcf9e100",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efefa4ba-823b-4643-8cb5-726b4a08e33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4e54521-ceab-47a7-bd90-bcbafcf9e100",
                    "LayerId": "d61f5eba-605f-4f1a-99e6-724e7d296ece"
                }
            ]
        },
        {
            "id": "86e316e0-35a5-4b4c-aede-bd1182a05525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "compositeImage": {
                "id": "c28068e7-9a0f-4248-b7fc-6d43565ee030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e316e0-35a5-4b4c-aede-bd1182a05525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3190716b-61dd-426f-9a49-01bcd2b3ef55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e316e0-35a5-4b4c-aede-bd1182a05525",
                    "LayerId": "d61f5eba-605f-4f1a-99e6-724e7d296ece"
                }
            ]
        },
        {
            "id": "37c1afd4-cdff-4909-a47d-f8d5e17b62e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "compositeImage": {
                "id": "ebdb9c2f-179a-40c5-880b-8f3fd3c6e0cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c1afd4-cdff-4909-a47d-f8d5e17b62e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ee0a86-50c3-413d-a77f-f481ce9a9543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c1afd4-cdff-4909-a47d-f8d5e17b62e7",
                    "LayerId": "d61f5eba-605f-4f1a-99e6-724e7d296ece"
                }
            ]
        },
        {
            "id": "133c5742-5ba7-4ea4-9419-6c4ee1e5cda5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "compositeImage": {
                "id": "93286199-9af3-4af2-bd9c-cf5208161f48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133c5742-5ba7-4ea4-9419-6c4ee1e5cda5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefdd014-565c-4db1-ac7c-e997a7600c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133c5742-5ba7-4ea4-9419-6c4ee1e5cda5",
                    "LayerId": "d61f5eba-605f-4f1a-99e6-724e7d296ece"
                }
            ]
        },
        {
            "id": "2523ad87-6bd6-4ccc-88d3-93432e30c73f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "compositeImage": {
                "id": "c51f285e-4503-40b2-9622-5d61c0997d3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2523ad87-6bd6-4ccc-88d3-93432e30c73f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df1f86c-579f-4b6a-9bbd-e7ecc648fa35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2523ad87-6bd6-4ccc-88d3-93432e30c73f",
                    "LayerId": "d61f5eba-605f-4f1a-99e6-724e7d296ece"
                }
            ]
        },
        {
            "id": "b711459d-4d04-4bd5-906b-a03e1bc90ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "compositeImage": {
                "id": "3222c9df-d8a4-4fab-842b-8284f66a8cd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b711459d-4d04-4bd5-906b-a03e1bc90ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d701a95e-941e-4519-888c-5de5d8e61416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b711459d-4d04-4bd5-906b-a03e1bc90ac3",
                    "LayerId": "d61f5eba-605f-4f1a-99e6-724e7d296ece"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "d61f5eba-605f-4f1a-99e6-724e7d296ece",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c843d217-9529-4c75-8878-fbcd9f382723",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}