{
    "id": "b8459b6e-5ec0-4340-a312-c8d403ee6e94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfc7fc36-d1ee-47a0-a357-f471f67032c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8459b6e-5ec0-4340-a312-c8d403ee6e94",
            "compositeImage": {
                "id": "242295c5-0401-4734-b855-4f1c39b88b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfc7fc36-d1ee-47a0-a357-f471f67032c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0de558-69ef-4a74-a7a3-75406d920605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfc7fc36-d1ee-47a0-a357-f471f67032c1",
                    "LayerId": "5cc497ae-7238-4cb5-9da7-d755ddd57679"
                }
            ]
        },
        {
            "id": "fff78810-27d2-43f5-ad1e-e588403b2ad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8459b6e-5ec0-4340-a312-c8d403ee6e94",
            "compositeImage": {
                "id": "bf6724f2-bbf2-41d0-8fb4-f6388eb7dcef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff78810-27d2-43f5-ad1e-e588403b2ad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc8f09b6-6ff1-463c-8b18-c4e8cd30b5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff78810-27d2-43f5-ad1e-e588403b2ad7",
                    "LayerId": "5cc497ae-7238-4cb5-9da7-d755ddd57679"
                }
            ]
        },
        {
            "id": "da1fb39b-a00d-4b67-8d01-88dfc5b5ec12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8459b6e-5ec0-4340-a312-c8d403ee6e94",
            "compositeImage": {
                "id": "70e4b3fa-a814-40d0-a2f4-f156d2919046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da1fb39b-a00d-4b67-8d01-88dfc5b5ec12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d714cef6-8f5b-4366-9965-60b23cdbd817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da1fb39b-a00d-4b67-8d01-88dfc5b5ec12",
                    "LayerId": "5cc497ae-7238-4cb5-9da7-d755ddd57679"
                }
            ]
        },
        {
            "id": "0af7d0c5-38d7-4eaf-a938-5ed0cc59ed00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8459b6e-5ec0-4340-a312-c8d403ee6e94",
            "compositeImage": {
                "id": "e7f05c1b-5151-4fc0-a5d7-2c02deab9cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0af7d0c5-38d7-4eaf-a938-5ed0cc59ed00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ac3962-2ccf-417b-9988-6709e74f17aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0af7d0c5-38d7-4eaf-a938-5ed0cc59ed00",
                    "LayerId": "5cc497ae-7238-4cb5-9da7-d755ddd57679"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5cc497ae-7238-4cb5-9da7-d755ddd57679",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8459b6e-5ec0-4340-a312-c8d403ee6e94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}