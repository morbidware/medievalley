{
    "id": "b345e203-7df0-440c-acc4-89d04157e9ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_roadsign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9cbf0b2-fab2-450d-9915-7ab24a242396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b345e203-7df0-440c-acc4-89d04157e9ac",
            "compositeImage": {
                "id": "519aaad5-4c6b-4e60-a21a-100759e7b2eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9cbf0b2-fab2-450d-9915-7ab24a242396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb92433-a5f5-4617-826c-be5d21aa8e1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9cbf0b2-fab2-450d-9915-7ab24a242396",
                    "LayerId": "c8bcef92-307f-4987-85c6-8d8ea4ba05d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c8bcef92-307f-4987-85c6-8d8ea4ba05d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b345e203-7df0-440c-acc4-89d04157e9ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 28
}