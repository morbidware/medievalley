{
    "id": "84fdc5c8-be2a-4d72-a8a9-cba52d7fe427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_watering_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "290de38d-ad30-429a-8576-999d3a0b8aad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84fdc5c8-be2a-4d72-a8a9-cba52d7fe427",
            "compositeImage": {
                "id": "41a78a21-7796-44e5-a2a1-355a359f02aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290de38d-ad30-429a-8576-999d3a0b8aad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1051ff80-70db-4c56-84c3-9988ed8c1d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290de38d-ad30-429a-8576-999d3a0b8aad",
                    "LayerId": "38a8228a-23d2-4349-b890-cf4d27cde25d"
                }
            ]
        },
        {
            "id": "2688a1f8-7aa1-48f5-b94e-b3a2525660ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84fdc5c8-be2a-4d72-a8a9-cba52d7fe427",
            "compositeImage": {
                "id": "be8336a6-209c-423e-8736-ffe259767968",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2688a1f8-7aa1-48f5-b94e-b3a2525660ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85042f19-8b95-4726-830e-794d8b44d2c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2688a1f8-7aa1-48f5-b94e-b3a2525660ce",
                    "LayerId": "38a8228a-23d2-4349-b890-cf4d27cde25d"
                }
            ]
        },
        {
            "id": "cd7199f9-bcc8-4de1-89f1-3eebedb0d690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84fdc5c8-be2a-4d72-a8a9-cba52d7fe427",
            "compositeImage": {
                "id": "6b8a5825-7231-4663-81bc-b8cff84eb95e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd7199f9-bcc8-4de1-89f1-3eebedb0d690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89de30b5-36d5-4801-bb64-4a7e2fafece7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd7199f9-bcc8-4de1-89f1-3eebedb0d690",
                    "LayerId": "38a8228a-23d2-4349-b890-cf4d27cde25d"
                }
            ]
        },
        {
            "id": "06643cc7-16bc-4aa0-b633-43c1c648c7ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84fdc5c8-be2a-4d72-a8a9-cba52d7fe427",
            "compositeImage": {
                "id": "5937fea0-a9bb-454b-87bc-1c030d5d9f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06643cc7-16bc-4aa0-b633-43c1c648c7ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a87d18f-92a1-4514-b275-a2db5bb380d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06643cc7-16bc-4aa0-b633-43c1c648c7ba",
                    "LayerId": "38a8228a-23d2-4349-b890-cf4d27cde25d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "38a8228a-23d2-4349-b890-cf4d27cde25d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84fdc5c8-be2a-4d72-a8a9-cba52d7fe427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}