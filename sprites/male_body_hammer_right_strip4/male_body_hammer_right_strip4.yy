{
    "id": "64a9748e-8a51-4086-ad86-c30f04cbeb2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92959c12-e8bd-4437-9ac6-0011c390920d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a9748e-8a51-4086-ad86-c30f04cbeb2a",
            "compositeImage": {
                "id": "5ce6d7d3-acbf-43bf-9731-8735d5a792f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92959c12-e8bd-4437-9ac6-0011c390920d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28a9fdba-2c0a-40aa-8228-6a04e9dedd05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92959c12-e8bd-4437-9ac6-0011c390920d",
                    "LayerId": "8bf8b563-cfea-4422-8bbe-7f004245f9e5"
                }
            ]
        },
        {
            "id": "b3948b93-fa35-41c5-9db3-a7a13f4d9d6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a9748e-8a51-4086-ad86-c30f04cbeb2a",
            "compositeImage": {
                "id": "389f204e-db2b-4e9e-b3b3-351ff224af52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3948b93-fa35-41c5-9db3-a7a13f4d9d6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a93aec10-e9ff-4c87-8e6a-786d73ea9bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3948b93-fa35-41c5-9db3-a7a13f4d9d6b",
                    "LayerId": "8bf8b563-cfea-4422-8bbe-7f004245f9e5"
                }
            ]
        },
        {
            "id": "987798ac-c35b-4da3-b75c-5ea604f520ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a9748e-8a51-4086-ad86-c30f04cbeb2a",
            "compositeImage": {
                "id": "c72e540d-d304-4a5a-8091-b52b66cfbf58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "987798ac-c35b-4da3-b75c-5ea604f520ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1899f133-7d1c-4066-853c-2a45bf0451bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "987798ac-c35b-4da3-b75c-5ea604f520ef",
                    "LayerId": "8bf8b563-cfea-4422-8bbe-7f004245f9e5"
                }
            ]
        },
        {
            "id": "4544db04-6e7d-4302-8d81-69ff3d5465dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64a9748e-8a51-4086-ad86-c30f04cbeb2a",
            "compositeImage": {
                "id": "efb07a35-0595-4f8a-8fad-01131d2e8073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4544db04-6e7d-4302-8d81-69ff3d5465dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66a6d490-4271-4eab-ba58-a9af98aeb770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4544db04-6e7d-4302-8d81-69ff3d5465dd",
                    "LayerId": "8bf8b563-cfea-4422-8bbe-7f004245f9e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8bf8b563-cfea-4422-8bbe-7f004245f9e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64a9748e-8a51-4086-ad86-c30f04cbeb2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}