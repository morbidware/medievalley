{
    "id": "8b93a5d3-97a7-4226-b224-d8c35c9c5d66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wildlife_firefly",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 7,
    "bbox_right": 7,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4001743-5555-4122-8f55-e250b50a2965",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b93a5d3-97a7-4226-b224-d8c35c9c5d66",
            "compositeImage": {
                "id": "087bc588-56a1-4abf-8db3-ea5e015465c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4001743-5555-4122-8f55-e250b50a2965",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "852851a9-272b-463d-b291-995e88ef9a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4001743-5555-4122-8f55-e250b50a2965",
                    "LayerId": "93cbc317-5954-433b-a61b-0066cccad329"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "93cbc317-5954-433b-a61b-0066cccad329",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b93a5d3-97a7-4226-b224-d8c35c9c5d66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}