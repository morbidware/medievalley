{
    "id": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wildlife_butterfly_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12da64a6-5424-4949-8604-1c6bc8a6874f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "ea7e09b9-0a7b-4c8c-ac38-357e10f13f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12da64a6-5424-4949-8604-1c6bc8a6874f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3500977-06f3-4d88-9e11-362cfe8bf608",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12da64a6-5424-4949-8604-1c6bc8a6874f",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "86280f52-23ed-44fa-9c8a-c027ff79fc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "e227db63-af38-45ed-bfea-c0092a2c6853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86280f52-23ed-44fa-9c8a-c027ff79fc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef740bf2-3919-478e-825c-8577695abf60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86280f52-23ed-44fa-9c8a-c027ff79fc20",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "ddf53278-43a9-4d31-b1f5-ca0022042e9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "93a19ca3-75f7-42c5-9e6c-a9e5c3d944ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddf53278-43a9-4d31-b1f5-ca0022042e9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31c9b3c1-eedd-4204-aa9f-43181c45dd29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddf53278-43a9-4d31-b1f5-ca0022042e9d",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "621c4c5e-71d1-4a7e-acd1-604974f7619b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "fbe8acab-788e-4a38-9bf9-f978168b3cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "621c4c5e-71d1-4a7e-acd1-604974f7619b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ba184b-d571-4576-9ea1-925c72c9ba85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "621c4c5e-71d1-4a7e-acd1-604974f7619b",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "a864685d-2f9f-446e-a24a-0faeb50da093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "049f740a-60a9-460f-948e-a5d1d7429fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a864685d-2f9f-446e-a24a-0faeb50da093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "237db41e-74ee-4141-8c59-9122f759c427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a864685d-2f9f-446e-a24a-0faeb50da093",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "859d3a72-b5c6-40ee-af21-a48f351247ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "e555bc1d-f712-42ff-ad9c-0e5e3f1b046d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "859d3a72-b5c6-40ee-af21-a48f351247ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c9650c2-4c35-4b28-8a44-27f03ced4810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "859d3a72-b5c6-40ee-af21-a48f351247ad",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "f1f6d9dd-d858-4817-93b4-15fe21a857da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "9348d2ee-5165-4906-9048-f99412ce52ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f6d9dd-d858-4817-93b4-15fe21a857da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffe209a3-a3be-4bcc-a95d-55b6374d844f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f6d9dd-d858-4817-93b4-15fe21a857da",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "5ecb8527-efce-4e73-8d65-291d7e1b074a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "36913954-a97f-4839-bb5a-666f99fc7a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ecb8527-efce-4e73-8d65-291d7e1b074a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a348ba5-8807-48d6-8f36-ca7a3edc8a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ecb8527-efce-4e73-8d65-291d7e1b074a",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "04c4fe45-5cea-4f5d-a5bb-f4ef36d03ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "704be4bb-f6a9-4bb2-aae2-73fec2dc766a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c4fe45-5cea-4f5d-a5bb-f4ef36d03ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa599ee6-c7b0-419e-8c2b-69b8a287dd6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c4fe45-5cea-4f5d-a5bb-f4ef36d03ca9",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "18df0d83-93fa-44fc-afee-a0bd7cb8e7a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "c6091f3f-c7d1-4ce5-91ea-b61588fe5927",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18df0d83-93fa-44fc-afee-a0bd7cb8e7a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4811b62c-c8f5-4794-8a6e-0f7910d68c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18df0d83-93fa-44fc-afee-a0bd7cb8e7a6",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "bb495ae3-01e1-433d-b6eb-56f9da49ddcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "17f3f6ef-da94-442a-ad08-42ae88e5584a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb495ae3-01e1-433d-b6eb-56f9da49ddcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d80c6db-4687-4023-90d7-2705356b47b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb495ae3-01e1-433d-b6eb-56f9da49ddcb",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        },
        {
            "id": "b61e2aa4-51aa-406c-9a7c-a5b5fcf5c551",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "compositeImage": {
                "id": "22eb28ce-5b42-4af6-817a-0dd7f15f328b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b61e2aa4-51aa-406c-9a7c-a5b5fcf5c551",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6533e970-fc1f-40b9-9a7c-817c459a634c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b61e2aa4-51aa-406c-9a7c-a5b5fcf5c551",
                    "LayerId": "ad51842c-3dea-4a78-844b-568749cab127"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ad51842c-3dea-4a78-844b-568749cab127",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "43cae02a-e1a6-4f4a-8afb-b8762d8e7ee2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}