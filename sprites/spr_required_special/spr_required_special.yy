{
    "id": "f6067199-d029-4055-9e49-a7df07ac22f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_required_special",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 6,
    "bbox_right": 10,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7507d8c-568e-41c5-af09-910ef5104945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6067199-d029-4055-9e49-a7df07ac22f9",
            "compositeImage": {
                "id": "8987207d-1858-4f5a-a040-5af28d27a835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7507d8c-568e-41c5-af09-910ef5104945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5646684-6175-4d43-8d6a-51f758417c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7507d8c-568e-41c5-af09-910ef5104945",
                    "LayerId": "75a2fc1b-e75d-4ed3-863f-155bc1e40cc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "75a2fc1b-e75d-4ed3-863f-155bc1e40cc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6067199-d029-4055-9e49-a7df07ac22f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}