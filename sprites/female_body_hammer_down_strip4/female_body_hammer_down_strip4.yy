{
    "id": "b3b2cb3e-f360-4be8-99e4-753dd9f7b6cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d55ee99c-50ba-42d3-994e-b6dd312b6b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b2cb3e-f360-4be8-99e4-753dd9f7b6cb",
            "compositeImage": {
                "id": "2ef02615-7cc0-412a-a5d9-53257f0dbd96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d55ee99c-50ba-42d3-994e-b6dd312b6b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836df709-6c17-47a6-a44a-b5db87bed6a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55ee99c-50ba-42d3-994e-b6dd312b6b8d",
                    "LayerId": "58383d96-22fc-47b9-9310-324ee4ddc36e"
                }
            ]
        },
        {
            "id": "9172783e-a146-40ea-98c8-a2d8bff421ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b2cb3e-f360-4be8-99e4-753dd9f7b6cb",
            "compositeImage": {
                "id": "3deac1d2-cb46-4fd9-a31e-c3788b703b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9172783e-a146-40ea-98c8-a2d8bff421ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53bc069-e8f8-45cf-904a-714815ad9a4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9172783e-a146-40ea-98c8-a2d8bff421ac",
                    "LayerId": "58383d96-22fc-47b9-9310-324ee4ddc36e"
                }
            ]
        },
        {
            "id": "4291e4a8-35f2-445e-a3fa-842b88f98e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b2cb3e-f360-4be8-99e4-753dd9f7b6cb",
            "compositeImage": {
                "id": "23a7ae04-20c3-4bc1-ac64-e094827eda21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4291e4a8-35f2-445e-a3fa-842b88f98e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d061ee7-ae15-4992-b10c-2a84e1662cc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4291e4a8-35f2-445e-a3fa-842b88f98e89",
                    "LayerId": "58383d96-22fc-47b9-9310-324ee4ddc36e"
                }
            ]
        },
        {
            "id": "18c273a2-1357-47e7-9b07-c7c756d38ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3b2cb3e-f360-4be8-99e4-753dd9f7b6cb",
            "compositeImage": {
                "id": "513e32a8-13c6-4473-8c94-5c74dfd36f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18c273a2-1357-47e7-9b07-c7c756d38ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae387430-8f4d-48b2-a051-357bb9cf4353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18c273a2-1357-47e7-9b07-c7c756d38ae9",
                    "LayerId": "58383d96-22fc-47b9-9310-324ee4ddc36e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "58383d96-22fc-47b9-9310-324ee4ddc36e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3b2cb3e-f360-4be8-99e4-753dd9f7b6cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}