{
    "id": "83e0a94d-d96a-4e48-ab96-f15ddbf32330",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_woodlog",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa8f3b3b-72e2-4abe-b140-e6e2e5237d36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83e0a94d-d96a-4e48-ab96-f15ddbf32330",
            "compositeImage": {
                "id": "34e5c180-afd4-46b3-b058-5f6bd7809550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa8f3b3b-72e2-4abe-b140-e6e2e5237d36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edf62fa6-7d09-4b8e-aeed-9375b461f595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa8f3b3b-72e2-4abe-b140-e6e2e5237d36",
                    "LayerId": "f46407dc-4f75-4281-beda-360de1bb1be8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f46407dc-4f75-4281-beda-360de1bb1be8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83e0a94d-d96a-4e48-ab96-f15ddbf32330",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}