{
    "id": "8529da63-6d84-4441-af44-b7e18e765c4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_idle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 39,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fd98ff9-f752-43af-9f52-553b21c78ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8529da63-6d84-4441-af44-b7e18e765c4b",
            "compositeImage": {
                "id": "be0bfb4e-0005-4d69-9690-d295ea9b3265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd98ff9-f752-43af-9f52-553b21c78ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a7ef598-d9a3-40ee-bb8e-4d4a07f8b00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd98ff9-f752-43af-9f52-553b21c78ab7",
                    "LayerId": "043b1e87-e03b-42ef-ac06-18db440e8c7b"
                }
            ]
        },
        {
            "id": "1cb9ac4e-8fce-4c96-bc28-16bb81012582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8529da63-6d84-4441-af44-b7e18e765c4b",
            "compositeImage": {
                "id": "778bb70a-9572-4196-a327-21414aeaaecd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cb9ac4e-8fce-4c96-bc28-16bb81012582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72f406be-3ba8-4acf-89b0-2eae3b80d0f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cb9ac4e-8fce-4c96-bc28-16bb81012582",
                    "LayerId": "043b1e87-e03b-42ef-ac06-18db440e8c7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "043b1e87-e03b-42ef-ac06-18db440e8c7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8529da63-6d84-4441-af44-b7e18e765c4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}