{
    "id": "c1e88c47-a392-4c53-be54-f4ca82c64703",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_sugarbeet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d10dcc87-47f5-4d8b-91a9-bbfc07a6a0ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1e88c47-a392-4c53-be54-f4ca82c64703",
            "compositeImage": {
                "id": "6f3c2846-48db-490a-9383-bb5a5a14210d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d10dcc87-47f5-4d8b-91a9-bbfc07a6a0ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c24371-5f69-4dc1-b0d5-9a752c07bcf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d10dcc87-47f5-4d8b-91a9-bbfc07a6a0ce",
                    "LayerId": "b234be2c-1d31-4c88-87ba-787fbd82f6e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b234be2c-1d31-4c88-87ba-787fbd82f6e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1e88c47-a392-4c53-be54-f4ca82c64703",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}