{
    "id": "82ab71dd-6ba6-43fc-9cf0-76b08aece3bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_hypericum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 2,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08ff47da-fe03-4417-bd6e-96bec39f6fa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ab71dd-6ba6-43fc-9cf0-76b08aece3bc",
            "compositeImage": {
                "id": "9f5b50cd-c26f-433a-b10b-cdafc51cdd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ff47da-fe03-4417-bd6e-96bec39f6fa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a73f65b-ff29-4d67-a292-9f43d573eef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ff47da-fe03-4417-bd6e-96bec39f6fa3",
                    "LayerId": "4f27f9e1-a71a-4085-9e18-c28ba53bb7ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4f27f9e1-a71a-4085-9e18-c28ba53bb7ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82ab71dd-6ba6-43fc-9cf0-76b08aece3bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}