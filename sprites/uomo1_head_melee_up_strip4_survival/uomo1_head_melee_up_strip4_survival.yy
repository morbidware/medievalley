{
    "id": "bde129eb-0a80-4275-8007-095b79e7784a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_melee_up_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "037acbb9-e28c-402f-ad16-773db5f64cfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "compositeImage": {
                "id": "7d73ff8e-4feb-477a-89ca-dce31843d390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "037acbb9-e28c-402f-ad16-773db5f64cfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505a294c-a324-4b94-a5ec-0ae9e60c5b36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "037acbb9-e28c-402f-ad16-773db5f64cfc",
                    "LayerId": "a5b86535-38b7-4638-9f11-03dbf26df51c"
                }
            ]
        },
        {
            "id": "bea4db54-211a-4b3f-bb33-aecd0268fea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "compositeImage": {
                "id": "c044f954-338f-42ad-bcfc-e8abd562413a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bea4db54-211a-4b3f-bb33-aecd0268fea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee64aa8c-98a3-4c4d-9f43-602679cbae6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea4db54-211a-4b3f-bb33-aecd0268fea0",
                    "LayerId": "a5b86535-38b7-4638-9f11-03dbf26df51c"
                }
            ]
        },
        {
            "id": "c75ccebc-3d40-486d-9fc0-2d0d23713381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "compositeImage": {
                "id": "452c7c60-e632-4362-b24d-9ed8ee0779b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c75ccebc-3d40-486d-9fc0-2d0d23713381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39eaaeb5-c8d6-4774-ae20-b2fe5f490042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c75ccebc-3d40-486d-9fc0-2d0d23713381",
                    "LayerId": "a5b86535-38b7-4638-9f11-03dbf26df51c"
                }
            ]
        },
        {
            "id": "84fd303e-c773-4c1e-b10d-c35f0a84a845",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "compositeImage": {
                "id": "47c73500-918b-4f90-825b-67726b254d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84fd303e-c773-4c1e-b10d-c35f0a84a845",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b1a5b6-2e9d-4b20-809b-a414c7bf804f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84fd303e-c773-4c1e-b10d-c35f0a84a845",
                    "LayerId": "a5b86535-38b7-4638-9f11-03dbf26df51c"
                }
            ]
        },
        {
            "id": "26373a3f-f1e8-4c21-9f0e-a6a1de6e9d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "compositeImage": {
                "id": "47c9ce49-4cf4-4f9e-9e29-10b0d995d0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26373a3f-f1e8-4c21-9f0e-a6a1de6e9d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bf70df-1fdf-4c65-97ec-57f1d8e10d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26373a3f-f1e8-4c21-9f0e-a6a1de6e9d41",
                    "LayerId": "a5b86535-38b7-4638-9f11-03dbf26df51c"
                }
            ]
        },
        {
            "id": "79a8c4e2-aae1-4670-b802-1cb6a462b7d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "compositeImage": {
                "id": "1eedd397-c8d2-4a08-b73d-d39e9e7add99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79a8c4e2-aae1-4670-b802-1cb6a462b7d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e1cb59-53ea-410e-818f-a4fb179f058d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79a8c4e2-aae1-4670-b802-1cb6a462b7d5",
                    "LayerId": "a5b86535-38b7-4638-9f11-03dbf26df51c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a5b86535-38b7-4638-9f11-03dbf26df51c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bde129eb-0a80-4275-8007-095b79e7784a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}