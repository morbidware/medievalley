{
    "id": "399d55cc-336a-4c29-873d-e1437bbd5646",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_settings_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89fbf187-e1bd-4ac3-8f25-9adddd47f1bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "399d55cc-336a-4c29-873d-e1437bbd5646",
            "compositeImage": {
                "id": "57e0604d-4064-4b56-93b2-df3bb708492f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89fbf187-e1bd-4ac3-8f25-9adddd47f1bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8cf1c6-d092-4697-a20b-50da0557ebd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89fbf187-e1bd-4ac3-8f25-9adddd47f1bf",
                    "LayerId": "07a3a2f6-49c2-41b3-a0ae-bf9a576e01fa"
                }
            ]
        },
        {
            "id": "aa89fec5-aaf2-423c-a1ab-585c6663208a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "399d55cc-336a-4c29-873d-e1437bbd5646",
            "compositeImage": {
                "id": "8d6b2a1e-85b2-48b3-8ac6-a4b4557b1147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa89fec5-aaf2-423c-a1ab-585c6663208a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c27f23c-bdf2-43c8-84f6-a40e903076e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa89fec5-aaf2-423c-a1ab-585c6663208a",
                    "LayerId": "07a3a2f6-49c2-41b3-a0ae-bf9a576e01fa"
                }
            ]
        },
        {
            "id": "0881ae31-25bc-4ded-9063-3d894f92478b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "399d55cc-336a-4c29-873d-e1437bbd5646",
            "compositeImage": {
                "id": "3091b17a-5e05-48c0-a9fb-910219e9a93a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0881ae31-25bc-4ded-9063-3d894f92478b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70d19b4-32d9-43fa-ae1a-a7d135f2f850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0881ae31-25bc-4ded-9063-3d894f92478b",
                    "LayerId": "07a3a2f6-49c2-41b3-a0ae-bf9a576e01fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "07a3a2f6-49c2-41b3-a0ae-bf9a576e01fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "399d55cc-336a-4c29-873d-e1437bbd5646",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}