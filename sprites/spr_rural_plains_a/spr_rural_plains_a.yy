{
    "id": "73ae7bf6-49e1-4b9b-a3c4-9cf012f164a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_plains_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ff04be0-6d78-4b84-8309-1105a06256ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73ae7bf6-49e1-4b9b-a3c4-9cf012f164a6",
            "compositeImage": {
                "id": "55daafc5-7287-4a4c-b88d-a60f84b5b090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff04be0-6d78-4b84-8309-1105a06256ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfb0217d-d9de-435c-a23d-4815a43f0341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff04be0-6d78-4b84-8309-1105a06256ca",
                    "LayerId": "ffdb161a-c7b2-4250-b4c7-8b007681a09f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ffdb161a-c7b2-4250-b4c7-8b007681a09f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73ae7bf6-49e1-4b9b-a3c4-9cf012f164a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}