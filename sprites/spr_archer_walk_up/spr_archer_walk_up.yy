{
    "id": "e774aa56-52dd-4298-986b-c8679e336582",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_walk_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf74b4b9-11b9-49d0-a00b-42f50f26a83c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e774aa56-52dd-4298-986b-c8679e336582",
            "compositeImage": {
                "id": "7b15d225-bd7d-4789-a893-268e96abfa7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf74b4b9-11b9-49d0-a00b-42f50f26a83c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08832134-d489-4561-8b95-26681e961741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf74b4b9-11b9-49d0-a00b-42f50f26a83c",
                    "LayerId": "1da52566-c9e1-48fe-9b2c-808ee47c0a02"
                }
            ]
        },
        {
            "id": "8f542250-7ddb-4aa5-8e9a-8f96e05c6829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e774aa56-52dd-4298-986b-c8679e336582",
            "compositeImage": {
                "id": "aa9847ec-97b6-44ce-a58c-517e114c4f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f542250-7ddb-4aa5-8e9a-8f96e05c6829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5163f4e-40e0-45a5-bb83-21e9fb7833a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f542250-7ddb-4aa5-8e9a-8f96e05c6829",
                    "LayerId": "1da52566-c9e1-48fe-9b2c-808ee47c0a02"
                }
            ]
        },
        {
            "id": "0022e168-fa51-4db6-b63e-bb18281c80ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e774aa56-52dd-4298-986b-c8679e336582",
            "compositeImage": {
                "id": "d14695e1-9e5d-4817-acd4-2d99281888f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0022e168-fa51-4db6-b63e-bb18281c80ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8674f633-c2c5-4ca8-8150-67ea7b9744d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0022e168-fa51-4db6-b63e-bb18281c80ae",
                    "LayerId": "1da52566-c9e1-48fe-9b2c-808ee47c0a02"
                }
            ]
        },
        {
            "id": "3984f7a8-464d-48f0-8f22-40fe51196023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e774aa56-52dd-4298-986b-c8679e336582",
            "compositeImage": {
                "id": "fbfc687b-5103-4af6-ab03-73d6c756c28c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3984f7a8-464d-48f0-8f22-40fe51196023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb83ee3-63a0-4537-afdb-b08f7a732571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3984f7a8-464d-48f0-8f22-40fe51196023",
                    "LayerId": "1da52566-c9e1-48fe-9b2c-808ee47c0a02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1da52566-c9e1-48fe-9b2c-808ee47c0a02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e774aa56-52dd-4298-986b-c8679e336582",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}