{
    "id": "21c77b4f-d7e8-4504-ab88-00dd81013eb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wolf_suffer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 5,
    "bbox_right": 43,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bc2b970-d517-4883-a307-66917f967318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21c77b4f-d7e8-4504-ab88-00dd81013eb7",
            "compositeImage": {
                "id": "704c9c97-d30a-45b0-9e55-ea564bcec73e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bc2b970-d517-4883-a307-66917f967318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b0a0c9-da72-4e80-a6b2-0f24da83b37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bc2b970-d517-4883-a307-66917f967318",
                    "LayerId": "b8e162f0-38c0-4b72-ab68-76a48bc09de8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "b8e162f0-38c0-4b72-ab68-76a48bc09de8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21c77b4f-d7e8-4504-ab88-00dd81013eb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}