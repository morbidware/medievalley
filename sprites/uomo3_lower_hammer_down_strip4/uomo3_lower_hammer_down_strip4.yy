{
    "id": "2c7532fd-a273-4896-9045-7c9d0c16780c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4493fa9b-38e1-41f6-96f6-a3692e3ef469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c7532fd-a273-4896-9045-7c9d0c16780c",
            "compositeImage": {
                "id": "8c3e5c27-e198-4655-810d-ef1ffaecc6ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4493fa9b-38e1-41f6-96f6-a3692e3ef469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b55ae427-b908-42d0-b44c-fb386eb3f5a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4493fa9b-38e1-41f6-96f6-a3692e3ef469",
                    "LayerId": "4f95bb9d-d5b4-463e-9075-575fb919d3b2"
                }
            ]
        },
        {
            "id": "d4d4f142-102f-4db7-b4dc-33e3dc6d5d31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c7532fd-a273-4896-9045-7c9d0c16780c",
            "compositeImage": {
                "id": "b713dfa3-1d11-43f8-9126-c3a106945a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d4f142-102f-4db7-b4dc-33e3dc6d5d31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d48f10d-ffd7-4f3d-bedf-2d62b4f96400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d4f142-102f-4db7-b4dc-33e3dc6d5d31",
                    "LayerId": "4f95bb9d-d5b4-463e-9075-575fb919d3b2"
                }
            ]
        },
        {
            "id": "f77fbdf8-07d8-446f-8a98-b1ed484ff413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c7532fd-a273-4896-9045-7c9d0c16780c",
            "compositeImage": {
                "id": "6f65de18-981f-4564-8d56-4642741e62c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77fbdf8-07d8-446f-8a98-b1ed484ff413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce598f44-74ba-4ceb-8ba2-2bdcbacdb424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77fbdf8-07d8-446f-8a98-b1ed484ff413",
                    "LayerId": "4f95bb9d-d5b4-463e-9075-575fb919d3b2"
                }
            ]
        },
        {
            "id": "4dcdd487-0f26-4e56-9be1-1bfce372b1c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c7532fd-a273-4896-9045-7c9d0c16780c",
            "compositeImage": {
                "id": "8ea9a130-83d2-4989-8530-92f0e8d34b2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dcdd487-0f26-4e56-9be1-1bfce372b1c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e2579c-d554-4da4-aa5f-6f854eb7aefa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dcdd487-0f26-4e56-9be1-1bfce372b1c1",
                    "LayerId": "4f95bb9d-d5b4-463e-9075-575fb919d3b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4f95bb9d-d5b4-463e-9075-575fb919d3b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c7532fd-a273-4896-9045-7c9d0c16780c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}