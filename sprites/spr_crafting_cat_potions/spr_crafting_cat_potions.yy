{
    "id": "bcca0029-513c-4907-bc27-032a7c731b79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_cat_potions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b97bce88-523a-4e99-acbd-8e6bb4648eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcca0029-513c-4907-bc27-032a7c731b79",
            "compositeImage": {
                "id": "108a57d7-d0e2-43ab-9de7-6084ed0e9f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b97bce88-523a-4e99-acbd-8e6bb4648eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88cc826-735c-43c2-984b-1f6d04cdf99c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97bce88-523a-4e99-acbd-8e6bb4648eb7",
                    "LayerId": "c234a6d3-b722-4a19-bfb4-85f4b317f1ea"
                },
                {
                    "id": "615d8457-4a7a-475d-a815-a8ad2419a49a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b97bce88-523a-4e99-acbd-8e6bb4648eb7",
                    "LayerId": "37198a42-4525-424c-88bb-0f3f36e322eb"
                }
            ]
        },
        {
            "id": "c5fe0760-33f9-43c6-b97f-42138e781b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcca0029-513c-4907-bc27-032a7c731b79",
            "compositeImage": {
                "id": "e188d7a2-88aa-4b4e-ba84-40a0735f65c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5fe0760-33f9-43c6-b97f-42138e781b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7179b74f-7557-4d76-8d03-80a36f607cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5fe0760-33f9-43c6-b97f-42138e781b93",
                    "LayerId": "c234a6d3-b722-4a19-bfb4-85f4b317f1ea"
                },
                {
                    "id": "976dca90-953f-4953-89a9-c14991e1c9eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5fe0760-33f9-43c6-b97f-42138e781b93",
                    "LayerId": "37198a42-4525-424c-88bb-0f3f36e322eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "c234a6d3-b722-4a19-bfb4-85f4b317f1ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcca0029-513c-4907-bc27-032a7c731b79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "37198a42-4525-424c-88bb-0f3f36e322eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcca0029-513c-4907-bc27-032a7c731b79",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}