{
    "id": "2a39189a-4184-41d6-a6da-84f82c10eb49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_abandoned_castle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c01f1a92-dd5b-4248-8d33-584e1bb59cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a39189a-4184-41d6-a6da-84f82c10eb49",
            "compositeImage": {
                "id": "d105970a-b1a2-4769-8cad-2a5bd0f9c019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01f1a92-dd5b-4248-8d33-584e1bb59cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab6674ba-a742-4f60-ad1d-01359e9349ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01f1a92-dd5b-4248-8d33-584e1bb59cf1",
                    "LayerId": "f9f608a3-93ed-48c8-8c19-293e3a61b162"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f9f608a3-93ed-48c8-8c19-293e3a61b162",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a39189a-4184-41d6-a6da-84f82c10eb49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}