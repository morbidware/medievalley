{
    "id": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision_mask_16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5b98d19-6aa2-487d-821e-9a165536d5f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
            "compositeImage": {
                "id": "06bf2f62-8f43-4be2-a800-c6c605c565c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b98d19-6aa2-487d-821e-9a165536d5f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47f9df00-f2a5-4367-8535-49b188dfee98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b98d19-6aa2-487d-821e-9a165536d5f0",
                    "LayerId": "ed878c7d-3611-484d-ad2f-27b84384305f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ed878c7d-3611-484d-ad2f-27b84384305f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9c36706-4d90-4582-9465-5e1b0e5b5d72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}