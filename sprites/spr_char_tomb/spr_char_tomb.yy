{
    "id": "2f0970be-dd88-48a5-94de-1b249c940b6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_char_tomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c977ad24-8a0d-4e24-bf8c-09c41d2c7871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f0970be-dd88-48a5-94de-1b249c940b6a",
            "compositeImage": {
                "id": "2203517e-db07-4216-918f-07d08fbfcc3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c977ad24-8a0d-4e24-bf8c-09c41d2c7871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eef9b83-340d-405a-8eb8-ad150bffbf46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c977ad24-8a0d-4e24-bf8c-09c41d2c7871",
                    "LayerId": "72c717a8-21e6-4cf7-b785-c8002dd3f783"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "72c717a8-21e6-4cf7-b785-c8002dd3f783",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f0970be-dd88-48a5-94de-1b249c940b6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 28
}