{
    "id": "c43a8eff-22cd-4bd4-9335-f80f0794a473",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbe3657a-5662-4256-b7bb-c4ce6ed5996b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43a8eff-22cd-4bd4-9335-f80f0794a473",
            "compositeImage": {
                "id": "4c6fd400-f96d-4032-b765-b4bc42deab0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe3657a-5662-4256-b7bb-c4ce6ed5996b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df973683-a11f-44e4-a732-01641bcbd784",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe3657a-5662-4256-b7bb-c4ce6ed5996b",
                    "LayerId": "afe6b91e-6369-49a9-a722-4158884de9fe"
                }
            ]
        },
        {
            "id": "d6a77a89-fae5-498a-9f85-625a744ff0fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43a8eff-22cd-4bd4-9335-f80f0794a473",
            "compositeImage": {
                "id": "9e055f20-fe78-43f2-8c7f-5b34011a339d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a77a89-fae5-498a-9f85-625a744ff0fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4dac31-8658-4c94-846b-25eaf570059c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a77a89-fae5-498a-9f85-625a744ff0fc",
                    "LayerId": "afe6b91e-6369-49a9-a722-4158884de9fe"
                }
            ]
        },
        {
            "id": "3dcf6bf1-684c-4a2b-a994-d30ee7f9f0f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43a8eff-22cd-4bd4-9335-f80f0794a473",
            "compositeImage": {
                "id": "306650c9-5129-48cc-8b4a-390b5b787100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dcf6bf1-684c-4a2b-a994-d30ee7f9f0f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115d7483-bf12-4a58-9495-fc00fcff3b41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dcf6bf1-684c-4a2b-a994-d30ee7f9f0f5",
                    "LayerId": "afe6b91e-6369-49a9-a722-4158884de9fe"
                }
            ]
        },
        {
            "id": "e78ceedc-7580-4cad-ad22-efadf48c18e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c43a8eff-22cd-4bd4-9335-f80f0794a473",
            "compositeImage": {
                "id": "b8b43349-60fb-44bc-9c79-1f935814c4b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e78ceedc-7580-4cad-ad22-efadf48c18e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04711798-69d3-4d67-a366-00c8fe8b45c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e78ceedc-7580-4cad-ad22-efadf48c18e5",
                    "LayerId": "afe6b91e-6369-49a9-a722-4158884de9fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "afe6b91e-6369-49a9-a722-4158884de9fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c43a8eff-22cd-4bd4-9335-f80f0794a473",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}