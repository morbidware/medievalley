{
    "id": "3da72714-9ead-48b3-9276-5703d9103e9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0393bc0b-f8a0-4f81-b7fa-0aab54109ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3da72714-9ead-48b3-9276-5703d9103e9e",
            "compositeImage": {
                "id": "6c2dba60-2039-4ae7-a433-e9906106a2b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0393bc0b-f8a0-4f81-b7fa-0aab54109ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bbe7c80-c0c1-486f-9e70-0d52b81125b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0393bc0b-f8a0-4f81-b7fa-0aab54109ce6",
                    "LayerId": "ce59eb09-423e-4e52-815f-ddbd43c9e722"
                }
            ]
        },
        {
            "id": "169c3a9b-0c7e-4970-a8b4-562a4427cfd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3da72714-9ead-48b3-9276-5703d9103e9e",
            "compositeImage": {
                "id": "2ee633b7-d0bd-4dd1-b9d1-b6488d4a57af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "169c3a9b-0c7e-4970-a8b4-562a4427cfd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79648835-ac26-4a1c-9de9-4c4b3c6323eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "169c3a9b-0c7e-4970-a8b4-562a4427cfd4",
                    "LayerId": "ce59eb09-423e-4e52-815f-ddbd43c9e722"
                }
            ]
        },
        {
            "id": "fdcbdcc1-37f3-4d13-8938-82b32c2bf267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3da72714-9ead-48b3-9276-5703d9103e9e",
            "compositeImage": {
                "id": "7218697f-65cb-4b92-a881-c568a4308d29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdcbdcc1-37f3-4d13-8938-82b32c2bf267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf83346-4ce7-4d97-b281-d6ce1314ec2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdcbdcc1-37f3-4d13-8938-82b32c2bf267",
                    "LayerId": "ce59eb09-423e-4e52-815f-ddbd43c9e722"
                }
            ]
        },
        {
            "id": "ace9c34d-3020-499d-a2aa-66b752b37172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3da72714-9ead-48b3-9276-5703d9103e9e",
            "compositeImage": {
                "id": "d077e88b-a75d-4c62-a043-103ab6e5dcda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace9c34d-3020-499d-a2aa-66b752b37172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c0d725-e4ab-4689-b23c-4788ff3a1a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace9c34d-3020-499d-a2aa-66b752b37172",
                    "LayerId": "ce59eb09-423e-4e52-815f-ddbd43c9e722"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce59eb09-423e-4e52-815f-ddbd43c9e722",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3da72714-9ead-48b3-9276-5703d9103e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}