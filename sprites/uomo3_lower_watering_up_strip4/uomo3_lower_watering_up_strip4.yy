{
    "id": "50464c5f-fe49-4669-bbc4-e3ec350dda8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4644b16f-04d9-4402-aef9-4247484a313c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50464c5f-fe49-4669-bbc4-e3ec350dda8c",
            "compositeImage": {
                "id": "c414a9cc-adbc-4023-b59d-f816864b909b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4644b16f-04d9-4402-aef9-4247484a313c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f9205e4-e42e-4797-b3da-15747dcace82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4644b16f-04d9-4402-aef9-4247484a313c",
                    "LayerId": "9f77647b-bbd5-4ad6-9f80-daa626945abf"
                }
            ]
        },
        {
            "id": "755f61f9-3ce8-4aa3-ae06-558c0689641d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50464c5f-fe49-4669-bbc4-e3ec350dda8c",
            "compositeImage": {
                "id": "ddd01797-2cde-41a2-bb08-d2e0327551b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "755f61f9-3ce8-4aa3-ae06-558c0689641d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c050fe5-fd48-43bf-bc78-f094b3327a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "755f61f9-3ce8-4aa3-ae06-558c0689641d",
                    "LayerId": "9f77647b-bbd5-4ad6-9f80-daa626945abf"
                }
            ]
        },
        {
            "id": "5c51f830-3a61-4508-b3bc-64f78d924b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50464c5f-fe49-4669-bbc4-e3ec350dda8c",
            "compositeImage": {
                "id": "da9abed7-9d67-470a-a29f-4964a9164627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c51f830-3a61-4508-b3bc-64f78d924b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da7549bc-fcb1-4432-b816-158d42b8edac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c51f830-3a61-4508-b3bc-64f78d924b94",
                    "LayerId": "9f77647b-bbd5-4ad6-9f80-daa626945abf"
                }
            ]
        },
        {
            "id": "67c3ed51-e268-4ba0-9cc5-70c34e811316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50464c5f-fe49-4669-bbc4-e3ec350dda8c",
            "compositeImage": {
                "id": "3d22b9f9-e8d6-4fe2-b824-e4645a7e63cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67c3ed51-e268-4ba0-9cc5-70c34e811316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8331113-125f-462f-9e66-e10108b3b47e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67c3ed51-e268-4ba0-9cc5-70c34e811316",
                    "LayerId": "9f77647b-bbd5-4ad6-9f80-daa626945abf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9f77647b-bbd5-4ad6-9f80-daa626945abf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50464c5f-fe49-4669-bbc4-e3ec350dda8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}