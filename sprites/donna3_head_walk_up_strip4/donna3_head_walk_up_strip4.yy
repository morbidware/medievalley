{
    "id": "dfb42524-3163-48e2-8d2d-b2f50a1b6890",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b2f525f-1b77-42a4-a435-5dec557e5319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb42524-3163-48e2-8d2d-b2f50a1b6890",
            "compositeImage": {
                "id": "a8d12b88-6183-4148-9933-1dd16ac9af4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b2f525f-1b77-42a4-a435-5dec557e5319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4be2db07-6621-4133-acec-211fdc76b82d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b2f525f-1b77-42a4-a435-5dec557e5319",
                    "LayerId": "6b07177b-41d0-419f-93ce-c8579212112d"
                }
            ]
        },
        {
            "id": "ff46a374-d34e-4eda-a710-f41181120334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb42524-3163-48e2-8d2d-b2f50a1b6890",
            "compositeImage": {
                "id": "cb3b24b4-9714-43fb-a0ed-155c2440068c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff46a374-d34e-4eda-a710-f41181120334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c8881e7-1fe5-4d1d-9219-191d5c3446de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff46a374-d34e-4eda-a710-f41181120334",
                    "LayerId": "6b07177b-41d0-419f-93ce-c8579212112d"
                }
            ]
        },
        {
            "id": "8cba9a57-c658-4288-ae1f-48ffc2036c06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb42524-3163-48e2-8d2d-b2f50a1b6890",
            "compositeImage": {
                "id": "a1e4def8-16af-46de-8d8e-26a93187e474",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cba9a57-c658-4288-ae1f-48ffc2036c06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7af5c90-4cb7-452f-9f43-d1e52fd05a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cba9a57-c658-4288-ae1f-48ffc2036c06",
                    "LayerId": "6b07177b-41d0-419f-93ce-c8579212112d"
                }
            ]
        },
        {
            "id": "709db35c-e107-42d3-8025-5b769719845e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb42524-3163-48e2-8d2d-b2f50a1b6890",
            "compositeImage": {
                "id": "f18d6ce6-b237-44dd-87c4-5cbd2e3f6e46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "709db35c-e107-42d3-8025-5b769719845e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5703eb39-187d-46fd-adb9-15d07d2edb1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "709db35c-e107-42d3-8025-5b769719845e",
                    "LayerId": "6b07177b-41d0-419f-93ce-c8579212112d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6b07177b-41d0-419f-93ce-c8579212112d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfb42524-3163-48e2-8d2d-b2f50a1b6890",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}