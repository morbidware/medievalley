{
    "id": "611cf363-2f68-41c4-8400-359cc5e153fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_seed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7ff31e0-5282-451b-bbf2-bd2b155fb316",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "611cf363-2f68-41c4-8400-359cc5e153fa",
            "compositeImage": {
                "id": "7b2589f6-5f4d-45f9-a016-6908664afeea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ff31e0-5282-451b-bbf2-bd2b155fb316",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f509784-cb8b-48d9-bd85-7009f8219caa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ff31e0-5282-451b-bbf2-bd2b155fb316",
                    "LayerId": "df2b5afe-68f9-4b48-91c9-171dfcd8be5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "df2b5afe-68f9-4b48-91c9-171dfcd8be5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "611cf363-2f68-41c4-8400-359cc5e153fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}