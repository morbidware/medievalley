{
    "id": "79fa88c5-d121-49c0-a9db-67446215e5e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de36e0d5-2eb6-4e78-be86-77af73b0e0a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79fa88c5-d121-49c0-a9db-67446215e5e2",
            "compositeImage": {
                "id": "6503b0d8-b930-4c0c-9290-ff5bcc105b08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de36e0d5-2eb6-4e78-be86-77af73b0e0a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46060c67-411c-4afc-8682-2111b85c7dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de36e0d5-2eb6-4e78-be86-77af73b0e0a1",
                    "LayerId": "814b099a-d01f-4067-8f52-829722bc3abb"
                }
            ]
        },
        {
            "id": "8d9729f1-f57e-4a62-a5d5-3f889662a4f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79fa88c5-d121-49c0-a9db-67446215e5e2",
            "compositeImage": {
                "id": "1764bd38-f98d-4a45-81a6-b85f5f8c91e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9729f1-f57e-4a62-a5d5-3f889662a4f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b52fb01-a89f-4f87-8d96-2766756f757d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9729f1-f57e-4a62-a5d5-3f889662a4f6",
                    "LayerId": "814b099a-d01f-4067-8f52-829722bc3abb"
                }
            ]
        },
        {
            "id": "81de6f3c-eaf2-4571-8995-9c96013930a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79fa88c5-d121-49c0-a9db-67446215e5e2",
            "compositeImage": {
                "id": "a5dadc85-6307-44d4-a328-ce13d61524e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81de6f3c-eaf2-4571-8995-9c96013930a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01040c47-ba60-4a26-960c-c75cad7dd2e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81de6f3c-eaf2-4571-8995-9c96013930a1",
                    "LayerId": "814b099a-d01f-4067-8f52-829722bc3abb"
                }
            ]
        },
        {
            "id": "31d1d884-d900-449e-b068-313544196176",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79fa88c5-d121-49c0-a9db-67446215e5e2",
            "compositeImage": {
                "id": "50b1d8bb-2561-4754-88e0-03f9917a140a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d1d884-d900-449e-b068-313544196176",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4acaff99-fe4a-4408-b0e3-408b762cf848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d1d884-d900-449e-b068-313544196176",
                    "LayerId": "814b099a-d01f-4067-8f52-829722bc3abb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "814b099a-d01f-4067-8f52-829722bc3abb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79fa88c5-d121-49c0-a9db-67446215e5e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}