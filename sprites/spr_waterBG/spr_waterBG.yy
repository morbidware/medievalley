{
    "id": "cca39418-d2f4-45fc-ba9a-139c6a074025",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_waterBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af3a5a9a-d49a-4e74-80d8-0e106537cea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca39418-d2f4-45fc-ba9a-139c6a074025",
            "compositeImage": {
                "id": "3912387c-bee1-44c1-988c-581114fd0159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af3a5a9a-d49a-4e74-80d8-0e106537cea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e1897a6-1f56-428c-af84-47574f93be76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af3a5a9a-d49a-4e74-80d8-0e106537cea5",
                    "LayerId": "bd414d64-9ed9-4cda-b20c-dc7323859109"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "bd414d64-9ed9-4cda-b20c-dc7323859109",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cca39418-d2f4-45fc-ba9a-139c6a074025",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}