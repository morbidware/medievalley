{
    "id": "873a5a3c-aa8c-47aa-be76-568d9f57bbf5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_potion_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04602a76-89c0-42f0-bcc7-b84fb472dd29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "873a5a3c-aa8c-47aa-be76-568d9f57bbf5",
            "compositeImage": {
                "id": "611e65a3-a010-4860-b7e5-6c6e2342cb62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04602a76-89c0-42f0-bcc7-b84fb472dd29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f389bc38-91b8-4849-a082-24e5af6c3aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04602a76-89c0-42f0-bcc7-b84fb472dd29",
                    "LayerId": "2616350d-452b-4de2-be91-69025f722132"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2616350d-452b-4de2-be91-69025f722132",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "873a5a3c-aa8c-47aa-be76-568d9f57bbf5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}