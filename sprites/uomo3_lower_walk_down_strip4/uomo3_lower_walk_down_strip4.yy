{
    "id": "b90d55bb-b472-48e0-b595-7a95b82910f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_lower_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4fe3d2c-3b3e-48d3-9e53-dce0e27b2545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d55bb-b472-48e0-b595-7a95b82910f2",
            "compositeImage": {
                "id": "99fc5464-292f-490a-ad2b-32978ede7208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4fe3d2c-3b3e-48d3-9e53-dce0e27b2545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f90ca5f2-b7c6-4923-b91c-c94621fc5d7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4fe3d2c-3b3e-48d3-9e53-dce0e27b2545",
                    "LayerId": "2506bedf-5c22-4615-aba8-55c7eea9231a"
                }
            ]
        },
        {
            "id": "29988b11-aa9e-4dcd-bc49-68d643e38f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d55bb-b472-48e0-b595-7a95b82910f2",
            "compositeImage": {
                "id": "7237fc15-6916-42b7-b707-71cb5ed8e722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29988b11-aa9e-4dcd-bc49-68d643e38f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c97a509-cd2a-46c0-af30-70d284ce26fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29988b11-aa9e-4dcd-bc49-68d643e38f87",
                    "LayerId": "2506bedf-5c22-4615-aba8-55c7eea9231a"
                }
            ]
        },
        {
            "id": "72f4f9ff-8faa-49bb-88fd-e8944cced53f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d55bb-b472-48e0-b595-7a95b82910f2",
            "compositeImage": {
                "id": "218dd19a-2968-49d7-ba6b-c3876874feac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f4f9ff-8faa-49bb-88fd-e8944cced53f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a00d2d21-b193-49c4-a391-031e7286a1b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f4f9ff-8faa-49bb-88fd-e8944cced53f",
                    "LayerId": "2506bedf-5c22-4615-aba8-55c7eea9231a"
                }
            ]
        },
        {
            "id": "e74c66dd-608a-4f9a-83e1-3dc52de2de3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d55bb-b472-48e0-b595-7a95b82910f2",
            "compositeImage": {
                "id": "1be0d20f-0710-4920-8666-8b7491387164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e74c66dd-608a-4f9a-83e1-3dc52de2de3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9717867-00d3-4770-97ad-35373cfdf915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e74c66dd-608a-4f9a-83e1-3dc52de2de3e",
                    "LayerId": "2506bedf-5c22-4615-aba8-55c7eea9231a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2506bedf-5c22-4615-aba8-55c7eea9231a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b90d55bb-b472-48e0-b595-7a95b82910f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}