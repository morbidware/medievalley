{
    "id": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_head_crafting_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1e5f32f-576c-4689-ae30-47fdb985d308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "compositeImage": {
                "id": "0f410f39-b0fa-471f-8256-ce782a61c4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e5f32f-576c-4689-ae30-47fdb985d308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df29af9-5e3c-471d-8724-c439c5cbdf5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e5f32f-576c-4689-ae30-47fdb985d308",
                    "LayerId": "66d7c391-8c63-4509-a071-a52a563d6359"
                }
            ]
        },
        {
            "id": "0313988c-7568-49bc-8a6f-28170a169079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "compositeImage": {
                "id": "4155a325-9164-4153-baad-c8e3fe09c6cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0313988c-7568-49bc-8a6f-28170a169079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0007725-0b86-447c-aad3-4500618112d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0313988c-7568-49bc-8a6f-28170a169079",
                    "LayerId": "66d7c391-8c63-4509-a071-a52a563d6359"
                }
            ]
        },
        {
            "id": "5e1dc4e6-15b7-4ffc-a2ee-0e2969913101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "compositeImage": {
                "id": "8f8496cc-d8c2-4bb1-988b-f4f3a39e6517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e1dc4e6-15b7-4ffc-a2ee-0e2969913101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998390cb-59a2-4f64-9d38-a1bf99a485e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e1dc4e6-15b7-4ffc-a2ee-0e2969913101",
                    "LayerId": "66d7c391-8c63-4509-a071-a52a563d6359"
                }
            ]
        },
        {
            "id": "0a10a37d-b1ab-4d33-a60e-65c414f746d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "compositeImage": {
                "id": "4c2983df-e4a3-4cae-b809-5d489ff70bbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a10a37d-b1ab-4d33-a60e-65c414f746d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f2587a6-4f33-4f07-83c8-934357326bf2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a10a37d-b1ab-4d33-a60e-65c414f746d8",
                    "LayerId": "66d7c391-8c63-4509-a071-a52a563d6359"
                }
            ]
        },
        {
            "id": "1fc69ad2-580f-46da-9e06-397d0cd7a3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "compositeImage": {
                "id": "88c9a468-9ac7-4b9d-bc45-fe9e128caf30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fc69ad2-580f-46da-9e06-397d0cd7a3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fee9e1d-84f9-47c9-a5ca-d03114c74295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fc69ad2-580f-46da-9e06-397d0cd7a3cf",
                    "LayerId": "66d7c391-8c63-4509-a071-a52a563d6359"
                }
            ]
        },
        {
            "id": "998b953b-f0b2-4058-ae2d-9a79c95ff535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "compositeImage": {
                "id": "d745c125-660f-476b-812e-a2aff0a516b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "998b953b-f0b2-4058-ae2d-9a79c95ff535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5939982b-a081-408e-aa44-2416b91b7710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "998b953b-f0b2-4058-ae2d-9a79c95ff535",
                    "LayerId": "66d7c391-8c63-4509-a071-a52a563d6359"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "66d7c391-8c63-4509-a071-a52a563d6359",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77c98f6b-928d-447a-8a3b-2c3020d00f8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}