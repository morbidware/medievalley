{
    "id": "f507ea47-1c07-4b08-ad08-a6811d9f52e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_atk_melee_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c0a123b-6ff2-4933-a52a-457842ce624b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f507ea47-1c07-4b08-ad08-a6811d9f52e3",
            "compositeImage": {
                "id": "992b6efd-aee4-4661-8ae6-7efdedea6c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0a123b-6ff2-4933-a52a-457842ce624b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0cb6f57-104e-4665-a825-c35374933f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0a123b-6ff2-4933-a52a-457842ce624b",
                    "LayerId": "52bd690c-e638-463a-a66c-02e539fe66a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "52bd690c-e638-463a-a66c-02e539fe66a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f507ea47-1c07-4b08-ad08-a6811d9f52e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 11
}