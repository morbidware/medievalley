{
    "id": "4d83777a-8335-4417-a1eb-51ff788cfbf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_lower_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48f313aa-7531-41b6-a33d-7a5c95b467bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d83777a-8335-4417-a1eb-51ff788cfbf7",
            "compositeImage": {
                "id": "2d8670b3-b62b-42b7-baa5-796593bec1ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f313aa-7531-41b6-a33d-7a5c95b467bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e3b3c9-6763-4a4b-a602-05b50c933599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f313aa-7531-41b6-a33d-7a5c95b467bc",
                    "LayerId": "789c0d12-a195-4963-8902-cc01447917f0"
                }
            ]
        },
        {
            "id": "62b9e157-b1d8-4efa-b76d-1ea5b47a7eb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d83777a-8335-4417-a1eb-51ff788cfbf7",
            "compositeImage": {
                "id": "55b41281-5166-4811-93d2-7708b965aba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b9e157-b1d8-4efa-b76d-1ea5b47a7eb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb5fab16-1199-48d1-8f30-7ef7bf8e47ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b9e157-b1d8-4efa-b76d-1ea5b47a7eb8",
                    "LayerId": "789c0d12-a195-4963-8902-cc01447917f0"
                }
            ]
        },
        {
            "id": "46dd2305-b97d-4ee4-9242-3140701552ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d83777a-8335-4417-a1eb-51ff788cfbf7",
            "compositeImage": {
                "id": "6c9c24a0-5a36-4b50-99ad-6b0b590906ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46dd2305-b97d-4ee4-9242-3140701552ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a67b10-b666-424f-930f-e5a6774c54da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46dd2305-b97d-4ee4-9242-3140701552ba",
                    "LayerId": "789c0d12-a195-4963-8902-cc01447917f0"
                }
            ]
        },
        {
            "id": "b7fc085e-47fa-4cf3-8bcb-385b5d3576c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d83777a-8335-4417-a1eb-51ff788cfbf7",
            "compositeImage": {
                "id": "fbcc4110-b16f-4dce-856a-aa1447a12a90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7fc085e-47fa-4cf3-8bcb-385b5d3576c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d634a27-fdb7-4cfe-bdc6-4b8a1c8d7b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7fc085e-47fa-4cf3-8bcb-385b5d3576c2",
                    "LayerId": "789c0d12-a195-4963-8902-cc01447917f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "789c0d12-a195-4963-8902-cc01447917f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d83777a-8335-4417-a1eb-51ff788cfbf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}