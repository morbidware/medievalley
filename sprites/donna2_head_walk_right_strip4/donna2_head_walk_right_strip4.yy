{
    "id": "070bc162-416a-4133-a861-65c200a8848a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08d6874b-2e70-4600-8c4d-2c05621e2956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070bc162-416a-4133-a861-65c200a8848a",
            "compositeImage": {
                "id": "3ceb4f67-f0e9-4486-8c80-735e1bc6942d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d6874b-2e70-4600-8c4d-2c05621e2956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db48ebd1-9deb-4045-81f8-d896afc5d54a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d6874b-2e70-4600-8c4d-2c05621e2956",
                    "LayerId": "2c1b19f1-f102-47c7-87da-a421299ed83b"
                }
            ]
        },
        {
            "id": "a159becb-b593-49c0-8464-52591cb89ca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070bc162-416a-4133-a861-65c200a8848a",
            "compositeImage": {
                "id": "aad3db78-de6f-457a-b837-5bcff197b2ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a159becb-b593-49c0-8464-52591cb89ca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c6e1a9-9723-41c6-97e5-60668c0fa4f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a159becb-b593-49c0-8464-52591cb89ca2",
                    "LayerId": "2c1b19f1-f102-47c7-87da-a421299ed83b"
                }
            ]
        },
        {
            "id": "7982a8fc-a490-431e-a0e2-d7a06a653ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070bc162-416a-4133-a861-65c200a8848a",
            "compositeImage": {
                "id": "6c1ce4d5-9070-40a7-9b16-188351151d90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7982a8fc-a490-431e-a0e2-d7a06a653ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eeeff42-42fa-4e45-b7f4-7999b8d9cd9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7982a8fc-a490-431e-a0e2-d7a06a653ee0",
                    "LayerId": "2c1b19f1-f102-47c7-87da-a421299ed83b"
                }
            ]
        },
        {
            "id": "1cd18a72-0ff9-4101-ba9e-bce0baba2b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "070bc162-416a-4133-a861-65c200a8848a",
            "compositeImage": {
                "id": "c6b861d7-8065-4af0-a384-823c0cbc46bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd18a72-0ff9-4101-ba9e-bce0baba2b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8fe57f-21f1-4417-90f1-2ec1b8f02288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd18a72-0ff9-4101-ba9e-bce0baba2b99",
                    "LayerId": "2c1b19f1-f102-47c7-87da-a421299ed83b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2c1b19f1-f102-47c7-87da-a421299ed83b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "070bc162-416a-4133-a861-65c200a8848a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}