{
    "id": "16655c9a-5285-4899-93e4-af3d01ed0291",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5aad2828-64a1-4a80-a24b-b10b8a7d56ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16655c9a-5285-4899-93e4-af3d01ed0291",
            "compositeImage": {
                "id": "4fcc1db2-9c5f-46d7-9194-ee6736bb2c24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aad2828-64a1-4a80-a24b-b10b8a7d56ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd8dffe-509b-44b9-809f-aacce0ad0067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aad2828-64a1-4a80-a24b-b10b8a7d56ef",
                    "LayerId": "1ac63b2e-454b-49a6-9b5a-c09fdb4f369d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1ac63b2e-454b-49a6-9b5a-c09fdb4f369d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16655c9a-5285-4899-93e4-af3d01ed0291",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}