{
    "id": "4cbcc9ef-fa78-4312-92f7-7531a786356e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85ef3989-9c6a-4c3b-9905-269ec105a076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cbcc9ef-fa78-4312-92f7-7531a786356e",
            "compositeImage": {
                "id": "ebaff5bc-77c9-41f9-b18c-84399878c68b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85ef3989-9c6a-4c3b-9905-269ec105a076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65133875-eaf1-4ac2-9265-76e9caf7812d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85ef3989-9c6a-4c3b-9905-269ec105a076",
                    "LayerId": "3b6008e0-eac5-4b1a-988a-a4f71128e4c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3b6008e0-eac5-4b1a-988a-a4f71128e4c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cbcc9ef-fa78-4312-92f7-7531a786356e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}