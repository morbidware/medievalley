{
    "id": "b0e28a33-fe7a-4a75-be83-6214b3bec9f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_lower_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c829ceb-495a-422a-a3b1-2944daf5741c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e28a33-fe7a-4a75-be83-6214b3bec9f7",
            "compositeImage": {
                "id": "9d2be962-2b8b-4480-a85d-b55dec34ce23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c829ceb-495a-422a-a3b1-2944daf5741c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "991a7d4f-6b73-41d1-b547-2caad9d9b214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c829ceb-495a-422a-a3b1-2944daf5741c",
                    "LayerId": "6536c4d0-adfb-4a95-a45e-e1b5b2407a4c"
                }
            ]
        },
        {
            "id": "31bb5bcd-9418-43d1-a9fb-b12a496ca951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e28a33-fe7a-4a75-be83-6214b3bec9f7",
            "compositeImage": {
                "id": "df3ed8bb-d136-44f5-ad68-448b76b384a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31bb5bcd-9418-43d1-a9fb-b12a496ca951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e019692f-9e5f-4430-a02c-9752850809a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31bb5bcd-9418-43d1-a9fb-b12a496ca951",
                    "LayerId": "6536c4d0-adfb-4a95-a45e-e1b5b2407a4c"
                }
            ]
        },
        {
            "id": "a80ec209-d827-4a7e-9acf-defd3b5b0f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e28a33-fe7a-4a75-be83-6214b3bec9f7",
            "compositeImage": {
                "id": "98e511bc-5d9e-4fb8-bc57-6cca3a327386",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80ec209-d827-4a7e-9acf-defd3b5b0f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83cdbf5-7916-455b-994e-3eb4523829d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80ec209-d827-4a7e-9acf-defd3b5b0f7a",
                    "LayerId": "6536c4d0-adfb-4a95-a45e-e1b5b2407a4c"
                }
            ]
        },
        {
            "id": "64a64234-4718-4b1c-9f55-1b47825d0856",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e28a33-fe7a-4a75-be83-6214b3bec9f7",
            "compositeImage": {
                "id": "e3904c20-b4e0-40a8-9fdb-abdd20a85f38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a64234-4718-4b1c-9f55-1b47825d0856",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e9aa26-ba0f-435b-a269-427c8e6f1201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a64234-4718-4b1c-9f55-1b47825d0856",
                    "LayerId": "6536c4d0-adfb-4a95-a45e-e1b5b2407a4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6536c4d0-adfb-4a95-a45e-e1b5b2407a4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0e28a33-fe7a-4a75-be83-6214b3bec9f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}