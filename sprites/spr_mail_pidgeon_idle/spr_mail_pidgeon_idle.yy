{
    "id": "1e2756bf-0c58-418d-be34-334cde1ea4ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mail_pidgeon_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 17,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "134ca5e8-2919-46d7-98d2-3e59928c8b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e2756bf-0c58-418d-be34-334cde1ea4ac",
            "compositeImage": {
                "id": "53c4f85c-e054-4e00-b56b-47901ea3b735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "134ca5e8-2919-46d7-98d2-3e59928c8b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbef6280-e94d-4045-9c34-6e7c5fa8b5ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "134ca5e8-2919-46d7-98d2-3e59928c8b0f",
                    "LayerId": "4beee102-dda1-45d5-a223-5dc03689013a"
                }
            ]
        },
        {
            "id": "0935fc9c-aeb7-479a-89a6-f2e0b15701d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e2756bf-0c58-418d-be34-334cde1ea4ac",
            "compositeImage": {
                "id": "44f254b5-df6c-46bf-b406-4e2589adcbd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0935fc9c-aeb7-479a-89a6-f2e0b15701d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c06e6e-7210-4b33-977f-dbda2866662d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0935fc9c-aeb7-479a-89a6-f2e0b15701d5",
                    "LayerId": "4beee102-dda1-45d5-a223-5dc03689013a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4beee102-dda1-45d5-a223-5dc03689013a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e2756bf-0c58-418d-be34-334cde1ea4ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 15
}