{
    "id": "672e1f84-e11a-408f-a521-4831e7b2794b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_gethit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fad10c47-ea09-4c3c-9b9e-81cbc6cd2797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "672e1f84-e11a-408f-a521-4831e7b2794b",
            "compositeImage": {
                "id": "f7ce71b9-c11b-427f-92f3-f1ffb9340e88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fad10c47-ea09-4c3c-9b9e-81cbc6cd2797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffb97290-1a9f-4dc8-8ea1-2f70b47d9db5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fad10c47-ea09-4c3c-9b9e-81cbc6cd2797",
                    "LayerId": "ef2ff47b-63f8-4cf2-9888-e97150ee05d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef2ff47b-63f8-4cf2-9888-e97150ee05d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "672e1f84-e11a-408f-a521-4831e7b2794b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}