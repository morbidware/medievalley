{
    "id": "8810aa63-fbdd-40ec-b4f3-fbab8943502e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_farmboard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9082ff0-4362-46e6-92a9-c2eed2de59ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8810aa63-fbdd-40ec-b4f3-fbab8943502e",
            "compositeImage": {
                "id": "1b74739f-f762-4f6a-954c-2d9dc752d4c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9082ff0-4362-46e6-92a9-c2eed2de59ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be2740a-215d-4fe4-94d2-0c9e5641de54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9082ff0-4362-46e6-92a9-c2eed2de59ab",
                    "LayerId": "f42d6b09-6784-43ed-8087-f2440e031fde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f42d6b09-6784-43ed-8087-f2440e031fde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8810aa63-fbdd-40ec-b4f3-fbab8943502e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}