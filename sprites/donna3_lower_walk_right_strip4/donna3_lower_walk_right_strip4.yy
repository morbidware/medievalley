{
    "id": "751145aa-f96b-4b13-b2d4-212c03e0e077",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6e8ceec-5bf3-4655-b44c-76fa5003ac86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751145aa-f96b-4b13-b2d4-212c03e0e077",
            "compositeImage": {
                "id": "08cfe012-39c1-4ab0-9f23-c1f2721c952a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e8ceec-5bf3-4655-b44c-76fa5003ac86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "777c885b-4e47-4fa1-86e2-8bb005f207ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e8ceec-5bf3-4655-b44c-76fa5003ac86",
                    "LayerId": "ca5a2f18-a336-4367-ba7f-48dab1ad43e2"
                }
            ]
        },
        {
            "id": "fef7c29b-7ec4-455e-b2f0-7597a505c103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751145aa-f96b-4b13-b2d4-212c03e0e077",
            "compositeImage": {
                "id": "d4bbc8fa-8126-40e5-907c-72d5d5275aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef7c29b-7ec4-455e-b2f0-7597a505c103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bac124b0-60ff-4aab-96c2-e8288787479c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef7c29b-7ec4-455e-b2f0-7597a505c103",
                    "LayerId": "ca5a2f18-a336-4367-ba7f-48dab1ad43e2"
                }
            ]
        },
        {
            "id": "98758aa1-fefc-4cd9-b232-ae37bd51a242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751145aa-f96b-4b13-b2d4-212c03e0e077",
            "compositeImage": {
                "id": "ab4d181b-40c9-4146-81de-cd4f5d1024d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98758aa1-fefc-4cd9-b232-ae37bd51a242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c322d250-8013-44c8-8b70-f57112606cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98758aa1-fefc-4cd9-b232-ae37bd51a242",
                    "LayerId": "ca5a2f18-a336-4367-ba7f-48dab1ad43e2"
                }
            ]
        },
        {
            "id": "c6c76dd8-f7c5-4c6c-8dae-a7da9090a794",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751145aa-f96b-4b13-b2d4-212c03e0e077",
            "compositeImage": {
                "id": "4028d2e5-821d-458a-a058-ac8412077218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c76dd8-f7c5-4c6c-8dae-a7da9090a794",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fa4ab9e-ffb9-4a91-acf6-1e55130f4bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c76dd8-f7c5-4c6c-8dae-a7da9090a794",
                    "LayerId": "ca5a2f18-a336-4367-ba7f-48dab1ad43e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ca5a2f18-a336-4367-ba7f-48dab1ad43e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "751145aa-f96b-4b13-b2d4-212c03e0e077",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}