{
    "id": "3e575e56-3933-4331-9479-282b57fecddb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_spear_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5afc4ab6-2246-46cf-a097-098908cc3370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e575e56-3933-4331-9479-282b57fecddb",
            "compositeImage": {
                "id": "21aa4d01-f1ef-4b01-a955-cea592b6435b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afc4ab6-2246-46cf-a097-098908cc3370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "251d0694-7dc7-452a-981d-aaed49029336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afc4ab6-2246-46cf-a097-098908cc3370",
                    "LayerId": "a9f9c552-6cf7-4b71-973f-a528a6c7c74d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a9f9c552-6cf7-4b71-973f-a528a6c7c74d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e575e56-3933-4331-9479-282b57fecddb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 12
}