{
    "id": "5ba03fe3-d5c6-405e-8b14-b3ecf1c0d1b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_gethit_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0873bb8d-8271-434e-96aa-941bfa5db8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ba03fe3-d5c6-405e-8b14-b3ecf1c0d1b3",
            "compositeImage": {
                "id": "7b308ed9-7e49-43c3-ab6f-203b8abc1337",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0873bb8d-8271-434e-96aa-941bfa5db8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a6b4323-e2e6-4a63-9f2c-80d517b87a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0873bb8d-8271-434e-96aa-941bfa5db8e4",
                    "LayerId": "619b8f61-dd6b-4b84-8bfe-e1edb3cbbc04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "619b8f61-dd6b-4b84-8bfe-e1edb3cbbc04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ba03fe3-d5c6-405e-8b14-b3ecf1c0d1b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}