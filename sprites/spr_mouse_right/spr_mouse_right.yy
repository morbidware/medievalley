{
    "id": "99b5e0e4-75f4-4f7b-93e6-6b11033519ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fccb42c9-7f3f-4560-ade6-36ac1f4265e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99b5e0e4-75f4-4f7b-93e6-6b11033519ff",
            "compositeImage": {
                "id": "b8d783c0-a186-49a9-8ae9-5c0a0c701b8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fccb42c9-7f3f-4560-ade6-36ac1f4265e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61fbe23-c825-4fd4-91c8-d054afb3495c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fccb42c9-7f3f-4560-ade6-36ac1f4265e5",
                    "LayerId": "040237d0-5bea-4b5a-a68f-2be071c51fac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "040237d0-5bea-4b5a-a68f-2be071c51fac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99b5e0e4-75f4-4f7b-93e6-6b11033519ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 6
}