{
    "id": "31de3b39-d4f1-4350-9dfa-eb706e434640",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_crafting_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4afb0fa6-5d7b-4e25-8d8e-e62dc171c6e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31de3b39-d4f1-4350-9dfa-eb706e434640",
            "compositeImage": {
                "id": "fa5b32ed-dccb-404e-a858-39574ad29154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4afb0fa6-5d7b-4e25-8d8e-e62dc171c6e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76771205-9473-4214-bf67-036bfde83048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4afb0fa6-5d7b-4e25-8d8e-e62dc171c6e9",
                    "LayerId": "bcb3a6b6-8ec4-4851-aebb-9bc39353529b"
                }
            ]
        },
        {
            "id": "c0a0f85a-d5b2-4c88-8df0-be51831e5622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31de3b39-d4f1-4350-9dfa-eb706e434640",
            "compositeImage": {
                "id": "f46f64e0-630c-49d0-95d6-5133b6cf3929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0a0f85a-d5b2-4c88-8df0-be51831e5622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad33c49-dd3d-4f22-a57f-8fba6b3327b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0a0f85a-d5b2-4c88-8df0-be51831e5622",
                    "LayerId": "bcb3a6b6-8ec4-4851-aebb-9bc39353529b"
                }
            ]
        },
        {
            "id": "05565267-8cce-4d6e-9a48-16459113b9c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31de3b39-d4f1-4350-9dfa-eb706e434640",
            "compositeImage": {
                "id": "ed593f44-232e-4ce9-87d5-05314641a5d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05565267-8cce-4d6e-9a48-16459113b9c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e74e95e3-47d6-488b-888e-f05941eb6239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05565267-8cce-4d6e-9a48-16459113b9c8",
                    "LayerId": "bcb3a6b6-8ec4-4851-aebb-9bc39353529b"
                }
            ]
        },
        {
            "id": "298459eb-6ec1-4766-a562-e171ce0ad1d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31de3b39-d4f1-4350-9dfa-eb706e434640",
            "compositeImage": {
                "id": "8cce1317-5aab-4946-9926-69ff9220b98c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "298459eb-6ec1-4766-a562-e171ce0ad1d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a46b88-95d3-48d8-bb53-d912977cffc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298459eb-6ec1-4766-a562-e171ce0ad1d6",
                    "LayerId": "bcb3a6b6-8ec4-4851-aebb-9bc39353529b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bcb3a6b6-8ec4-4851-aebb-9bc39353529b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31de3b39-d4f1-4350-9dfa-eb706e434640",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}