{
    "id": "b334a482-9380-48b1-b363-ebff603c48c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64395927-220f-418b-9b82-2b9b8596f31d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b334a482-9380-48b1-b363-ebff603c48c4",
            "compositeImage": {
                "id": "961a505f-dc47-41ca-8b35-d2e9a968c5b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64395927-220f-418b-9b82-2b9b8596f31d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb6a111a-7e06-4cbc-9ddd-9c1ed2438116",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64395927-220f-418b-9b82-2b9b8596f31d",
                    "LayerId": "0d2abf43-4e76-4848-b4b5-31304db7f733"
                }
            ]
        },
        {
            "id": "6e9fc5b2-9ae6-4234-8285-cce21ea4360b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b334a482-9380-48b1-b363-ebff603c48c4",
            "compositeImage": {
                "id": "d1e0d0ac-bb63-4a38-a4eb-90e460fd9ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e9fc5b2-9ae6-4234-8285-cce21ea4360b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd477e2e-2106-4757-8032-2c981a2dc643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e9fc5b2-9ae6-4234-8285-cce21ea4360b",
                    "LayerId": "0d2abf43-4e76-4848-b4b5-31304db7f733"
                }
            ]
        },
        {
            "id": "2dd12ec1-0133-42e3-bde5-5e587cb8a1c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b334a482-9380-48b1-b363-ebff603c48c4",
            "compositeImage": {
                "id": "4d4dc714-2e95-46dc-b2b1-55481000cc62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd12ec1-0133-42e3-bde5-5e587cb8a1c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c92ae0-f489-44db-84ca-4f270395667d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd12ec1-0133-42e3-bde5-5e587cb8a1c5",
                    "LayerId": "0d2abf43-4e76-4848-b4b5-31304db7f733"
                }
            ]
        },
        {
            "id": "fcb33d15-fa58-4069-94d0-f777f018eeb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b334a482-9380-48b1-b363-ebff603c48c4",
            "compositeImage": {
                "id": "16817312-30f7-456c-a96f-a017b88d70cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb33d15-fa58-4069-94d0-f777f018eeb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d6d9e34-45be-4631-98eb-236303c745f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb33d15-fa58-4069-94d0-f777f018eeb3",
                    "LayerId": "0d2abf43-4e76-4848-b4b5-31304db7f733"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d2abf43-4e76-4848-b4b5-31304db7f733",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b334a482-9380-48b1-b363-ebff603c48c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}