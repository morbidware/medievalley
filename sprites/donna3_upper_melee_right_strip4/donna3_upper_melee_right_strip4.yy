{
    "id": "7305508f-a291-4284-95bd-3b735f8094ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_upper_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f2ee2ee-1223-4d52-baa6-6aeb4eb0e018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7305508f-a291-4284-95bd-3b735f8094ce",
            "compositeImage": {
                "id": "d8b50688-677b-451f-8941-69e98dabdd67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f2ee2ee-1223-4d52-baa6-6aeb4eb0e018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c35fc400-70d6-4d6f-b536-2bcf4415c693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f2ee2ee-1223-4d52-baa6-6aeb4eb0e018",
                    "LayerId": "c84e315c-ec55-4728-be86-2a20668bfc36"
                }
            ]
        },
        {
            "id": "8f8e7e8c-3bb0-4aae-b88f-334d3d99cc7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7305508f-a291-4284-95bd-3b735f8094ce",
            "compositeImage": {
                "id": "1d348f5e-1bf7-4f6c-b652-ee5580c3f184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8e7e8c-3bb0-4aae-b88f-334d3d99cc7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15541d33-fbcd-4664-9550-f78bb33efc1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8e7e8c-3bb0-4aae-b88f-334d3d99cc7a",
                    "LayerId": "c84e315c-ec55-4728-be86-2a20668bfc36"
                }
            ]
        },
        {
            "id": "a03c7b34-2daa-4187-b035-eb59449c3e94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7305508f-a291-4284-95bd-3b735f8094ce",
            "compositeImage": {
                "id": "edbf9262-a31f-447e-9a24-51471e2361ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a03c7b34-2daa-4187-b035-eb59449c3e94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45a51cd8-1b69-429d-9a14-d93c51b93b2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a03c7b34-2daa-4187-b035-eb59449c3e94",
                    "LayerId": "c84e315c-ec55-4728-be86-2a20668bfc36"
                }
            ]
        },
        {
            "id": "dc3cd40c-9f1e-40d4-b176-4cc920fcaa75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7305508f-a291-4284-95bd-3b735f8094ce",
            "compositeImage": {
                "id": "ea7294c5-a255-476b-b03d-ca46e0e2e468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc3cd40c-9f1e-40d4-b176-4cc920fcaa75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18b27a1f-9611-4618-9597-0cbeaf10667a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc3cd40c-9f1e-40d4-b176-4cc920fcaa75",
                    "LayerId": "c84e315c-ec55-4728-be86-2a20668bfc36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c84e315c-ec55-4728-be86-2a20668bfc36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7305508f-a291-4284-95bd-3b735f8094ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}