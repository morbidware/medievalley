{
    "id": "de4ad6f3-119f-4b62-9842-7767e6295bd4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_home_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7647dce1-ddc7-42d4-82f7-72c9c87c444d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4ad6f3-119f-4b62-9842-7767e6295bd4",
            "compositeImage": {
                "id": "96b6c76b-72dd-4d77-abcc-0cc14f098c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7647dce1-ddc7-42d4-82f7-72c9c87c444d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef87cd9c-73a5-42d0-a214-a32048744484",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7647dce1-ddc7-42d4-82f7-72c9c87c444d",
                    "LayerId": "5ad5efa6-83b9-436c-bbb2-98c8a336dc8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "5ad5efa6-83b9-436c-bbb2-98c8a336dc8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de4ad6f3-119f-4b62-9842-7767e6295bd4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 135
}