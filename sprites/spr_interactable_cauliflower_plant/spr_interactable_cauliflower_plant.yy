{
    "id": "0c099607-9c00-40d8-923e-71f2baf7dda3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_cauliflower_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7ed17c4-6762-435e-ac1d-21db097b3509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
            "compositeImage": {
                "id": "921e60ce-1208-49ea-8ed1-3d4ab1e7ab29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ed17c4-6762-435e-ac1d-21db097b3509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "635b73a6-b4fa-42a2-82f0-8d8ac791cae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ed17c4-6762-435e-ac1d-21db097b3509",
                    "LayerId": "336db3d0-aa8f-4454-b537-39d12929d656"
                }
            ]
        },
        {
            "id": "d52f4c6c-90c1-43d2-85e4-35b8593b8108",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
            "compositeImage": {
                "id": "76432b5b-a542-49ab-95b2-4b660b8995ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d52f4c6c-90c1-43d2-85e4-35b8593b8108",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ac96f5-d221-4805-bb42-b49cb2c0cc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d52f4c6c-90c1-43d2-85e4-35b8593b8108",
                    "LayerId": "336db3d0-aa8f-4454-b537-39d12929d656"
                }
            ]
        },
        {
            "id": "65d55fe0-f2ea-49a4-ad00-abca16679a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
            "compositeImage": {
                "id": "0086bc35-ade2-4701-a3ea-66c8616388c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65d55fe0-f2ea-49a4-ad00-abca16679a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "736e999c-b282-4ede-ae65-5545873c50ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65d55fe0-f2ea-49a4-ad00-abca16679a96",
                    "LayerId": "336db3d0-aa8f-4454-b537-39d12929d656"
                }
            ]
        },
        {
            "id": "aa7fc1b0-71f8-4952-8674-f8a78fbacd62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
            "compositeImage": {
                "id": "a7092fc1-a26e-42be-94ed-85a2a2f250cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa7fc1b0-71f8-4952-8674-f8a78fbacd62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e2b3618-0bfc-425d-b614-7b957ea3308e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa7fc1b0-71f8-4952-8674-f8a78fbacd62",
                    "LayerId": "336db3d0-aa8f-4454-b537-39d12929d656"
                }
            ]
        },
        {
            "id": "b5fce1e2-4605-44f2-a541-cdfefbbc92cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
            "compositeImage": {
                "id": "ef68066a-0a1c-4f75-a04d-2e5cb8920b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fce1e2-4605-44f2-a541-cdfefbbc92cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa9e433-b197-4769-9e14-4115ba6d0f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fce1e2-4605-44f2-a541-cdfefbbc92cf",
                    "LayerId": "336db3d0-aa8f-4454-b537-39d12929d656"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "336db3d0-aa8f-4454-b537-39d12929d656",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c099607-9c00-40d8-923e-71f2baf7dda3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 11
}