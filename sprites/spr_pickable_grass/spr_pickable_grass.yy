{
    "id": "8cd9a8d0-807f-40fc-8273-c055d6fb07d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "827790cd-c9ef-4104-8e96-044242791a6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cd9a8d0-807f-40fc-8273-c055d6fb07d3",
            "compositeImage": {
                "id": "8e0f8cea-cbfd-4555-8e77-cb78b1f19f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "827790cd-c9ef-4104-8e96-044242791a6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c932dab8-b76b-4454-828e-b00914a0e097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "827790cd-c9ef-4104-8e96-044242791a6c",
                    "LayerId": "123f50e1-8ba7-46eb-a5a9-69b2c6a095d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "123f50e1-8ba7-46eb-a5a9-69b2c6a095d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cd9a8d0-807f-40fc-8273-c055d6fb07d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}