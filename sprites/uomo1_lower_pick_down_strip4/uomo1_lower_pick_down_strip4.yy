{
    "id": "05d7e4ff-cd96-46db-8b46-2436874b649c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_pick_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05c9b240-89b8-4836-9072-7562e968f045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05d7e4ff-cd96-46db-8b46-2436874b649c",
            "compositeImage": {
                "id": "153279de-8ebf-4a4f-81b3-317c493a53c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c9b240-89b8-4836-9072-7562e968f045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4439ce-0d8f-4431-9ade-8f07955b5c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c9b240-89b8-4836-9072-7562e968f045",
                    "LayerId": "c2910aa9-fecc-481c-ad1d-06522c6665d9"
                }
            ]
        },
        {
            "id": "887319fb-e57e-4dff-a20b-b61c70f2ca85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05d7e4ff-cd96-46db-8b46-2436874b649c",
            "compositeImage": {
                "id": "4f9bf258-443b-49f3-91d6-1372c47af5e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "887319fb-e57e-4dff-a20b-b61c70f2ca85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c4704d-f377-4686-a2ba-380e2dced043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "887319fb-e57e-4dff-a20b-b61c70f2ca85",
                    "LayerId": "c2910aa9-fecc-481c-ad1d-06522c6665d9"
                }
            ]
        },
        {
            "id": "0ee015dd-7111-4bea-a224-4275f0bd26a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05d7e4ff-cd96-46db-8b46-2436874b649c",
            "compositeImage": {
                "id": "9996c7d4-e2fc-4238-a197-470faaa25f28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee015dd-7111-4bea-a224-4275f0bd26a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1df7baa8-8827-42a9-8530-2a0633122325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee015dd-7111-4bea-a224-4275f0bd26a6",
                    "LayerId": "c2910aa9-fecc-481c-ad1d-06522c6665d9"
                }
            ]
        },
        {
            "id": "7efb6dfa-ccbf-4137-80f5-60ae31e9d547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05d7e4ff-cd96-46db-8b46-2436874b649c",
            "compositeImage": {
                "id": "9c8d1c47-47ba-4202-a179-4696e7ba6f6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7efb6dfa-ccbf-4137-80f5-60ae31e9d547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c3360c-4a00-4197-be52-a686eecc51f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7efb6dfa-ccbf-4137-80f5-60ae31e9d547",
                    "LayerId": "c2910aa9-fecc-481c-ad1d-06522c6665d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c2910aa9-fecc-481c-ad1d-06522c6665d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05d7e4ff-cd96-46db-8b46-2436874b649c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}