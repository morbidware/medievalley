{
    "id": "86c7548c-455b-4002-aec1-d0f1d7e87f33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7fb7728-ffde-4bb1-882a-880348e3898c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c7548c-455b-4002-aec1-d0f1d7e87f33",
            "compositeImage": {
                "id": "82539f43-f740-4292-8dc7-08d3bcd25f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fb7728-ffde-4bb1-882a-880348e3898c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44763f16-aa96-4975-9617-cde9f1adaee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fb7728-ffde-4bb1-882a-880348e3898c",
                    "LayerId": "7748f391-686d-49f2-8391-011e31e14f6f"
                }
            ]
        },
        {
            "id": "6ff112a5-1f9c-4388-8205-57d7327e0f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c7548c-455b-4002-aec1-d0f1d7e87f33",
            "compositeImage": {
                "id": "8a171d33-3e56-49bd-b8ea-179eeee558a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff112a5-1f9c-4388-8205-57d7327e0f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf7f647-afa8-40ff-9742-31f8819f13b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff112a5-1f9c-4388-8205-57d7327e0f28",
                    "LayerId": "7748f391-686d-49f2-8391-011e31e14f6f"
                }
            ]
        },
        {
            "id": "126414ed-728d-4fa5-80c2-766cbd78bcd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c7548c-455b-4002-aec1-d0f1d7e87f33",
            "compositeImage": {
                "id": "dad68618-d31a-41bd-aa36-12a751ee6f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "126414ed-728d-4fa5-80c2-766cbd78bcd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa74f72-bf47-4379-a6c4-91ec72acf543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "126414ed-728d-4fa5-80c2-766cbd78bcd1",
                    "LayerId": "7748f391-686d-49f2-8391-011e31e14f6f"
                }
            ]
        },
        {
            "id": "cdd41b5c-e8a4-4b90-9c57-fe1d49be4a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c7548c-455b-4002-aec1-d0f1d7e87f33",
            "compositeImage": {
                "id": "603fa009-8f94-4e61-a3dc-92d1b70b97f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd41b5c-e8a4-4b90-9c57-fe1d49be4a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947d5888-71cd-4c98-8eaa-d58469e8696e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd41b5c-e8a4-4b90-9c57-fe1d49be4a39",
                    "LayerId": "7748f391-686d-49f2-8391-011e31e14f6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7748f391-686d-49f2-8391-011e31e14f6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86c7548c-455b-4002-aec1-d0f1d7e87f33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}