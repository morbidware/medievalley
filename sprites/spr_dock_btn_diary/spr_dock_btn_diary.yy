{
    "id": "6007710a-7b6a-4fdf-ada3-7a7349247358",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_btn_diary",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eaa65da2-1c09-4a21-8f9a-c4aa62717b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6007710a-7b6a-4fdf-ada3-7a7349247358",
            "compositeImage": {
                "id": "33387ec8-9f85-4586-8bc2-b13ee70b219d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaa65da2-1c09-4a21-8f9a-c4aa62717b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f162b3c5-2b78-48dc-99a7-db392d7b393f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaa65da2-1c09-4a21-8f9a-c4aa62717b28",
                    "LayerId": "759e07d7-1530-45ea-877d-1ef1f2f41a79"
                }
            ]
        },
        {
            "id": "96b6e3e4-a3c6-4e3b-8918-7b6fb2245b55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6007710a-7b6a-4fdf-ada3-7a7349247358",
            "compositeImage": {
                "id": "b6d01ee5-50f1-4cfc-8abc-bb90759381b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b6e3e4-a3c6-4e3b-8918-7b6fb2245b55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3961981f-7600-4640-a033-6430cd5b539f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b6e3e4-a3c6-4e3b-8918-7b6fb2245b55",
                    "LayerId": "759e07d7-1530-45ea-877d-1ef1f2f41a79"
                }
            ]
        },
        {
            "id": "7881b4ab-6fa9-44bd-a8ee-a8640b2402c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6007710a-7b6a-4fdf-ada3-7a7349247358",
            "compositeImage": {
                "id": "bae7eb5b-8ba8-4aca-94a3-e3e5ddc9e197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7881b4ab-6fa9-44bd-a8ee-a8640b2402c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3a8e79-6890-4129-830f-6e8ecacff13e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7881b4ab-6fa9-44bd-a8ee-a8640b2402c0",
                    "LayerId": "759e07d7-1530-45ea-877d-1ef1f2f41a79"
                }
            ]
        },
        {
            "id": "f4385df0-1312-418c-8c9d-50767ea9a64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6007710a-7b6a-4fdf-ada3-7a7349247358",
            "compositeImage": {
                "id": "767e9ba5-bda2-4c91-bb5f-3a0f881759ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4385df0-1312-418c-8c9d-50767ea9a64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db688106-7f18-42b9-9105-72be99d58847",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4385df0-1312-418c-8c9d-50767ea9a64b",
                    "LayerId": "759e07d7-1530-45ea-877d-1ef1f2f41a79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "759e07d7-1530-45ea-877d-1ef1f2f41a79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6007710a-7b6a-4fdf-ada3-7a7349247358",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}