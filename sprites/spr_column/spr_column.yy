{
    "id": "383357f7-be4c-481b-856b-925aef008d8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_column",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c2e0948-ca67-4061-8e06-01fec30a2ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383357f7-be4c-481b-856b-925aef008d8a",
            "compositeImage": {
                "id": "9eb07d51-76dd-466a-80f3-587069e0cab2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c2e0948-ca67-4061-8e06-01fec30a2ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a496c1b8-777b-431e-8141-cafc0106058d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c2e0948-ca67-4061-8e06-01fec30a2ef0",
                    "LayerId": "1ac8b13b-9aaa-4bf2-bcfe-0c0ade727a8b"
                }
            ]
        },
        {
            "id": "1748726e-7c30-4a95-8b78-a8da904d2919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "383357f7-be4c-481b-856b-925aef008d8a",
            "compositeImage": {
                "id": "6286f117-d9a6-4aa8-b703-6b1586f40422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1748726e-7c30-4a95-8b78-a8da904d2919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83364f10-51e6-4f59-a5ff-516c039c26f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1748726e-7c30-4a95-8b78-a8da904d2919",
                    "LayerId": "1ac8b13b-9aaa-4bf2-bcfe-0c0ade727a8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1ac8b13b-9aaa-4bf2-bcfe-0c0ade727a8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "383357f7-be4c-481b-856b-925aef008d8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 56
}