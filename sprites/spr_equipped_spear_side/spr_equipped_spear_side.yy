{
    "id": "ddb43db9-7450-489a-adaf-ba41344f5526",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_spear_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c28c50cf-84a3-4066-94e4-767267776d77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddb43db9-7450-489a-adaf-ba41344f5526",
            "compositeImage": {
                "id": "67cfc524-1c46-49ff-99b7-ac5c646251f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28c50cf-84a3-4066-94e4-767267776d77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c984adc8-b34a-4ba2-a906-3977b2059456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28c50cf-84a3-4066-94e4-767267776d77",
                    "LayerId": "ec8212c2-9408-46df-98f1-738c7f4b2cc1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ec8212c2-9408-46df-98f1-738c7f4b2cc1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddb43db9-7450-489a-adaf-ba41344f5526",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 3,
    "yorig": 12
}