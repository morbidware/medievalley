{
    "id": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_grasshat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d763d1c-e40a-4f2d-8994-0200267dea54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
            "compositeImage": {
                "id": "373436c6-7b27-4e4f-8ee1-c349deaf58f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d763d1c-e40a-4f2d-8994-0200267dea54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf8a5b4-f22a-4757-850d-6465b50e161a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d763d1c-e40a-4f2d-8994-0200267dea54",
                    "LayerId": "59688dee-9627-4f21-9fd3-7992f5f76998"
                }
            ]
        },
        {
            "id": "68eb593b-c789-4701-9a74-a3aa0b6e2450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
            "compositeImage": {
                "id": "d9ce3541-cd7e-42ed-8a46-ce4a11422d12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68eb593b-c789-4701-9a74-a3aa0b6e2450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2134b12f-33f6-4b60-9ac6-490447bc90dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68eb593b-c789-4701-9a74-a3aa0b6e2450",
                    "LayerId": "59688dee-9627-4f21-9fd3-7992f5f76998"
                }
            ]
        },
        {
            "id": "941b9aa1-ea9c-4a09-aa33-0cbf05ab8d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
            "compositeImage": {
                "id": "a1d04243-4b27-491f-9e32-b355cbbc2b63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "941b9aa1-ea9c-4a09-aa33-0cbf05ab8d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa70c008-3ee5-4bbc-b29b-432b37881024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "941b9aa1-ea9c-4a09-aa33-0cbf05ab8d0b",
                    "LayerId": "59688dee-9627-4f21-9fd3-7992f5f76998"
                }
            ]
        },
        {
            "id": "9382cf17-7406-47b2-9b7e-b5cf675a6bd4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
            "compositeImage": {
                "id": "f5466fa3-ff5b-4a25-a7de-fdfe1a977d06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9382cf17-7406-47b2-9b7e-b5cf675a6bd4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecb77a38-abcc-45e3-b079-1816fc3df335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9382cf17-7406-47b2-9b7e-b5cf675a6bd4",
                    "LayerId": "59688dee-9627-4f21-9fd3-7992f5f76998"
                }
            ]
        },
        {
            "id": "fbe1ab83-d944-496f-a262-9770d49d492e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
            "compositeImage": {
                "id": "5cb00517-b784-4875-a439-0d1d9689ca7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe1ab83-d944-496f-a262-9770d49d492e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3a23a57-4cf0-4b9b-aa4c-b1ff2b7b261d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe1ab83-d944-496f-a262-9770d49d492e",
                    "LayerId": "59688dee-9627-4f21-9fd3-7992f5f76998"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "59688dee-9627-4f21-9fd3-7992f5f76998",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "89d575fb-7bc7-4883-9dd0-0b6eea4622db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}