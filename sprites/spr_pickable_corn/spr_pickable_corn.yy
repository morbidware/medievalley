{
    "id": "0ca21966-73ef-4547-9175-e0bb1691bc69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_corn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cbf52d9-9dc3-4582-a399-a66535fb4ca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ca21966-73ef-4547-9175-e0bb1691bc69",
            "compositeImage": {
                "id": "477d1f68-afab-4381-b9a2-8c2cc0d17196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cbf52d9-9dc3-4582-a399-a66535fb4ca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c685c1-59f5-4d7c-bf1c-0ab8566a264a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cbf52d9-9dc3-4582-a399-a66535fb4ca3",
                    "LayerId": "1615c393-ced6-4975-8fa9-4c6fbb823707"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1615c393-ced6-4975-8fa9-4c6fbb823707",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ca21966-73ef-4547-9175-e0bb1691bc69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}