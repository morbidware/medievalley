{
    "id": "4b167e52-bb83-4435-a364-13c898e2d074",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 198,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34ce6602-f002-4acb-8f53-f770d33eb3b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b167e52-bb83-4435-a364-13c898e2d074",
            "compositeImage": {
                "id": "567a39f1-8d18-447b-afa9-ef331dd01610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ce6602-f002-4acb-8f53-f770d33eb3b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "422b1028-e0aa-4fb8-9f10-e125e727089a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ce6602-f002-4acb-8f53-f770d33eb3b3",
                    "LayerId": "c2347bcd-37d9-45e1-b28a-aab392acd8e2"
                }
            ]
        },
        {
            "id": "55a43b11-8d80-4b57-b95e-55c48125f473",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b167e52-bb83-4435-a364-13c898e2d074",
            "compositeImage": {
                "id": "fcba6fa6-4c64-47ed-af17-5dd61d9a3c5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55a43b11-8d80-4b57-b95e-55c48125f473",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c7f8d8-6392-4693-9da9-5d3606c1f417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55a43b11-8d80-4b57-b95e-55c48125f473",
                    "LayerId": "c2347bcd-37d9-45e1-b28a-aab392acd8e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "c2347bcd-37d9-45e1-b28a-aab392acd8e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b167e52-bb83-4435-a364-13c898e2d074",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 199,
    "xorig": 0,
    "yorig": 0
}