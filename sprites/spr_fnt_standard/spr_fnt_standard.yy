{
    "id": "16dceb3f-6387-40e2-8271-041381d57394",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fnt_standard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "498ae68d-3fdf-49dc-b45a-7ef9fd6bae64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "ce657af0-bf28-4784-845c-8103648f9fd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "498ae68d-3fdf-49dc-b45a-7ef9fd6bae64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "080d0d79-3e01-4ca1-823c-6b359d2a3781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "498ae68d-3fdf-49dc-b45a-7ef9fd6bae64",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "8a39db8f-c81f-4912-88b1-1b1df5ecf9d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "29376dd6-d5e3-4bfc-9756-0c7532feb986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a39db8f-c81f-4912-88b1-1b1df5ecf9d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78614472-5965-4a9c-9218-d921f6532fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a39db8f-c81f-4912-88b1-1b1df5ecf9d5",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "f90b62fe-3337-4b99-9cc6-4ed41e0c0cd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "15b4008d-b20f-4e86-86a3-9a32a2fa5866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f90b62fe-3337-4b99-9cc6-4ed41e0c0cd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad8f7b3-6dbc-4837-98cf-727bdf09a887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f90b62fe-3337-4b99-9cc6-4ed41e0c0cd0",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "ecf2d843-a1f0-458b-90d2-a2234916e308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "27a3e28e-1d0a-406d-bcdc-4b5f73c7fff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf2d843-a1f0-458b-90d2-a2234916e308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9909e9b8-219d-40c3-ac6d-abf731361701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf2d843-a1f0-458b-90d2-a2234916e308",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "d6120877-62a7-4358-a3ca-26df605c92c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "90252480-2595-48d7-b1f9-6bad0bcc2cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6120877-62a7-4358-a3ca-26df605c92c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919ea871-23c5-403f-9644-ec2bca275f2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6120877-62a7-4358-a3ca-26df605c92c5",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "771efb93-2023-43c6-8766-fbceaf8e6e7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "74e80dd5-70e7-4fd2-b4b5-0fe8525d45c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "771efb93-2023-43c6-8766-fbceaf8e6e7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095d68ad-d459-42d7-ac01-cf3d141476fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "771efb93-2023-43c6-8766-fbceaf8e6e7c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "40d3bde7-236a-4471-892b-8832e2c3c932",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "b17296c2-930e-4c73-a02e-def9231391b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40d3bde7-236a-4471-892b-8832e2c3c932",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a39edb-494e-40c9-b8ae-82f6acaa2672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40d3bde7-236a-4471-892b-8832e2c3c932",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "9f0d1c0d-72d3-42f1-b574-311881e60d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "7f0b5146-6c36-4875-a202-f2dbc7eef319",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f0d1c0d-72d3-42f1-b574-311881e60d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5bbc05-c952-4f74-8352-aa680ed967c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f0d1c0d-72d3-42f1-b574-311881e60d6d",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "559f5b8f-fad6-4a4d-8421-dea1f686a796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "bd2942df-5a7b-4f17-9a45-d7c2b1592903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "559f5b8f-fad6-4a4d-8421-dea1f686a796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "461503a7-8505-41a7-9e7b-efca56937192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "559f5b8f-fad6-4a4d-8421-dea1f686a796",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "31dce9c6-a888-40c4-aec2-e8fccbd31c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "f93e3f27-1777-423f-bfc6-26e46d1d2487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31dce9c6-a888-40c4-aec2-e8fccbd31c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "877a8753-470f-4c13-a85e-fb4a5e3cd199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31dce9c6-a888-40c4-aec2-e8fccbd31c56",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "3c5c0882-97b0-4a96-9aef-26fbebb1a589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "8b7929b0-7587-4d53-8e17-bddc877d19f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c5c0882-97b0-4a96-9aef-26fbebb1a589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdcf0979-f5d0-455a-bb83-efdfac940dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c5c0882-97b0-4a96-9aef-26fbebb1a589",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "08eb2369-39fa-4f13-8848-ab1820508b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "a9e10c40-88a8-4532-8d05-eb2e43df0180",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08eb2369-39fa-4f13-8848-ab1820508b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2640241-58ba-4bd7-a523-b29fcb0f1c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08eb2369-39fa-4f13-8848-ab1820508b2c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "fce99fb9-9628-40c2-b4db-00365ab98aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "c82fc306-df0c-4d7c-a40a-8b6278387afb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce99fb9-9628-40c2-b4db-00365ab98aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e78ce7b0-a875-4ede-89dd-45c8a6078073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce99fb9-9628-40c2-b4db-00365ab98aec",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "df54bd9c-27bf-4c3e-a2bc-1464712604fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "8dad4c8b-b38d-4a58-8d0f-a2bcd3eedb05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df54bd9c-27bf-4c3e-a2bc-1464712604fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "153bff4b-a6ce-4270-b14f-1590aa0b3714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df54bd9c-27bf-4c3e-a2bc-1464712604fb",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "7f1d96d6-2d2e-43e2-a203-a6639876afce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "b9027e1e-a6b0-4005-9a1d-b80304f73956",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1d96d6-2d2e-43e2-a203-a6639876afce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ec99fd-215d-4da6-9b9d-0142fe874059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1d96d6-2d2e-43e2-a203-a6639876afce",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "4460c453-cd10-4373-bc72-a627e44abd6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "ddee4a79-4712-4513-b5c2-da6e123ef17e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4460c453-cd10-4373-bc72-a627e44abd6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9017f86c-8e22-4741-8014-6c4c80f4df07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4460c453-cd10-4373-bc72-a627e44abd6b",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "2553c299-48c7-4c63-93eb-a8ee01369543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "08ea2fb5-3bb1-433f-9521-c930e3ab58bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2553c299-48c7-4c63-93eb-a8ee01369543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c60ecb6-4663-421d-a6dc-d632950105c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2553c299-48c7-4c63-93eb-a8ee01369543",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "133dec5a-57b1-4374-9692-8bbad2c0e772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "10a2891a-911c-4f8d-8825-17a5b12d3cea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133dec5a-57b1-4374-9692-8bbad2c0e772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e34a2e5e-7e3b-4d06-93ad-8f34ff659d58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133dec5a-57b1-4374-9692-8bbad2c0e772",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "97bc79af-084f-463e-b2bc-5dda06e061c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "457449e1-bf57-410f-be4b-c532386ea6e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97bc79af-084f-463e-b2bc-5dda06e061c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a2cb43-a405-44d9-92c3-038a4b048da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97bc79af-084f-463e-b2bc-5dda06e061c8",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "28314225-d2a7-4ced-9f0c-8e9d5289091a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "54da8866-059d-493a-8a91-5bd9f00481ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28314225-d2a7-4ced-9f0c-8e9d5289091a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dadeaaf-1dce-46f5-a7f3-df92d90200e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28314225-d2a7-4ced-9f0c-8e9d5289091a",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "084b5ed2-1d2b-4a98-9313-d5f14066320c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "4d4b0168-78f4-4f27-a712-20521ee04cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "084b5ed2-1d2b-4a98-9313-d5f14066320c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f529d5e-14a6-4e91-8c12-b29b1965ee99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "084b5ed2-1d2b-4a98-9313-d5f14066320c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "8c21a8b0-d53d-490c-9d00-bc2cee1710dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "43d05ec4-19f5-43b5-996d-10a4101b8422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c21a8b0-d53d-490c-9d00-bc2cee1710dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e592ed0-f344-4cdb-a836-f82902f767c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c21a8b0-d53d-490c-9d00-bc2cee1710dd",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "f1f9ab45-2984-4a7d-bba4-4315f841c753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "2cd17f7e-7c7b-419a-a02d-b497171909f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f9ab45-2984-4a7d-bba4-4315f841c753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f94bb8d-8c3c-45bb-af43-dce8ce492de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f9ab45-2984-4a7d-bba4-4315f841c753",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "f3aa6062-77ca-4a77-9a96-2c9597ef12af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "95bb3abe-9034-4865-af86-e202da8f1e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3aa6062-77ca-4a77-9a96-2c9597ef12af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "318bbb33-eb27-4ffe-a467-b1bbbef3bbc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3aa6062-77ca-4a77-9a96-2c9597ef12af",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "ae6ec299-f837-4554-a93a-06c06b172cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "604235ce-e039-405c-bec3-5a2dbe087710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae6ec299-f837-4554-a93a-06c06b172cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22296e77-3cb0-437d-90f8-1201147dc3e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae6ec299-f837-4554-a93a-06c06b172cde",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "aedaf403-3511-4098-8a9d-a5cc02a4010d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "b23f0b64-13a9-46f2-b1b6-de93b759bb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aedaf403-3511-4098-8a9d-a5cc02a4010d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "702da21a-67f9-49ce-82dc-5d605042e16d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aedaf403-3511-4098-8a9d-a5cc02a4010d",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "ce7892b2-9960-4a99-b130-08822c7296ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "be77761a-c8e1-4ba7-8d1b-15efbe645663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce7892b2-9960-4a99-b130-08822c7296ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e99519-e3de-4345-8854-3738eae2a8e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce7892b2-9960-4a99-b130-08822c7296ac",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "36c8da81-42f9-43da-9598-204d68778410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "3bb60b3a-ffb8-4a44-afcf-8554f9b7a0c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36c8da81-42f9-43da-9598-204d68778410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fddb8ff4-0613-4fde-a67f-ad18442cbf9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36c8da81-42f9-43da-9598-204d68778410",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "0e7e2726-d892-44e0-aa95-2bca3ca1cb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "fde6834c-fc69-4b78-bcdf-88c85b749198",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7e2726-d892-44e0-aa95-2bca3ca1cb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98eb082-bbf7-4023-83e8-6171b210adda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7e2726-d892-44e0-aa95-2bca3ca1cb87",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "08be5059-1d36-4bb7-be82-70a9be672037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "f55613e8-2794-42fa-b0a9-ead06f21c49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08be5059-1d36-4bb7-be82-70a9be672037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "330f3b0d-6729-4da4-a486-6f1cefaac671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08be5059-1d36-4bb7-be82-70a9be672037",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "75ad54d7-2ae5-4a35-a545-c67355cced4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "c921086e-1e15-49ad-8a2b-ec0f3f6a76b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ad54d7-2ae5-4a35-a545-c67355cced4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58e31934-f016-43ee-9c2f-67229c80f325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ad54d7-2ae5-4a35-a545-c67355cced4f",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "22208483-0378-445a-aecf-5b191f33d5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "6a19df25-9659-47ba-a33b-10376f011471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22208483-0378-445a-aecf-5b191f33d5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ba328a1-88f7-432b-bb2f-939df070e61a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22208483-0378-445a-aecf-5b191f33d5a4",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "5d1a9099-a812-4a49-9592-a1cd24aaa741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "d73a238f-81b0-4287-9824-e598fcf62244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d1a9099-a812-4a49-9592-a1cd24aaa741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeed9ea8-0f99-4ffe-913b-58af462fa5c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d1a9099-a812-4a49-9592-a1cd24aaa741",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "5de7ec95-d280-4066-b57d-105abef51e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "3e3dff75-daeb-4ffe-95ad-5ee14f2b2011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de7ec95-d280-4066-b57d-105abef51e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bfecc4-091b-401f-af80-e5f0f15bd69c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de7ec95-d280-4066-b57d-105abef51e81",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "682a5080-f7c4-4c88-899d-354d7a704098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "bbb28d09-3bd6-4810-9f1a-9070bf6a180f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "682a5080-f7c4-4c88-899d-354d7a704098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0995673c-423b-4f48-939b-bb519ba838e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "682a5080-f7c4-4c88-899d-354d7a704098",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "4db2c364-621a-4fd5-8361-8f3625dc2245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "4aed6649-f3b6-4452-aa3d-791698b54bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4db2c364-621a-4fd5-8361-8f3625dc2245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d688bafe-7461-46ba-940d-4ae3d048af47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4db2c364-621a-4fd5-8361-8f3625dc2245",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "dcb96d83-1021-41aa-968b-5be4b8778377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "0c9a17c3-f396-4697-b12c-aafccd82d60e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb96d83-1021-41aa-968b-5be4b8778377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0a5386-ecc4-4aad-b617-af2b47f86ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb96d83-1021-41aa-968b-5be4b8778377",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "f0f11375-b04d-4067-9308-bb5080936b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "f73b496c-9bc2-48ef-8a1c-f88a8f948bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0f11375-b04d-4067-9308-bb5080936b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ab39e36-882b-421b-8651-ad15dcad1aaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0f11375-b04d-4067-9308-bb5080936b72",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "253964cd-4477-41d8-a653-4321af8f882e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "2ddff1a0-1f2a-4474-9c8e-ee75553c63fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "253964cd-4477-41d8-a653-4321af8f882e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae03d941-95c1-426e-84e1-8ea269c74e4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "253964cd-4477-41d8-a653-4321af8f882e",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "8be0b8ab-2566-4439-b9a9-451b5026affd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "88c8036c-2a94-41ed-b2d3-043fb0a19f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be0b8ab-2566-4439-b9a9-451b5026affd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82bb2db-9fae-4560-bb52-c093a307c104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be0b8ab-2566-4439-b9a9-451b5026affd",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "1034ebf2-92b9-4208-8cb5-d8f27e902d23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "8ce846e9-5af5-48b0-8307-1d7db7889b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1034ebf2-92b9-4208-8cb5-d8f27e902d23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc5ae48-a47b-4a14-bb1d-1627ae603781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1034ebf2-92b9-4208-8cb5-d8f27e902d23",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "e729d0eb-e63a-4180-ab95-28f5677e0f20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "69ca11df-c3a0-447d-9bd0-4b605bf1dc5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e729d0eb-e63a-4180-ab95-28f5677e0f20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5c71ff2-d986-477b-8172-0a2cc83193cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e729d0eb-e63a-4180-ab95-28f5677e0f20",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "ee3a0fea-b0bf-4d96-a49a-11f3eab0a238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "f32ddd0e-da33-4275-80ac-30cfdd428e5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3a0fea-b0bf-4d96-a49a-11f3eab0a238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947c19dc-b472-451a-99de-5cbb00172f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3a0fea-b0bf-4d96-a49a-11f3eab0a238",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "7fd4d837-6153-41c4-920e-0c2af5a2ecf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "c0392660-52f5-4a16-96b1-40b3a0e63590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd4d837-6153-41c4-920e-0c2af5a2ecf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01210338-129b-48dc-b42e-202f74e13284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd4d837-6153-41c4-920e-0c2af5a2ecf9",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "33748166-a148-4052-9953-f2b62bd2df6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "085d9c5a-f373-46fd-bb12-0498f9e44018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33748166-a148-4052-9953-f2b62bd2df6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c2aee9-9735-4b99-9676-07f02376b751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33748166-a148-4052-9953-f2b62bd2df6f",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "9157062a-e1ba-4495-a1e9-16b4cec40483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "1be7039d-dbf4-43d2-a4f5-f57becf7856a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9157062a-e1ba-4495-a1e9-16b4cec40483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c544db-e92d-4db4-8b37-877968698f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9157062a-e1ba-4495-a1e9-16b4cec40483",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "4fd39a86-2235-41db-adf2-ad47a55c1664",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "adaada65-9990-4a69-9955-738ff4c4dd56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd39a86-2235-41db-adf2-ad47a55c1664",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "041bfee2-ffd4-4b94-a0ef-9abd4d18a2f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd39a86-2235-41db-adf2-ad47a55c1664",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "8accde5e-d224-4d5f-ad25-e24d76cecb08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "ef3e00ab-d9ad-4ff1-81b2-3208629f88f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8accde5e-d224-4d5f-ad25-e24d76cecb08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e3004e2-f702-4fe0-a8c2-a31bc6942f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8accde5e-d224-4d5f-ad25-e24d76cecb08",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "fedf3619-67ae-45fa-80f4-e656fb94b821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "01006371-6452-4bbf-89f3-8dfb14dc3692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fedf3619-67ae-45fa-80f4-e656fb94b821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8d10583-c4c8-464f-ab1c-1b6ac425691c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fedf3619-67ae-45fa-80f4-e656fb94b821",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "c00c9e1d-5abe-4000-94a2-f46902d3d92e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "e04308d3-2193-4978-aec2-3116c8f5cef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c00c9e1d-5abe-4000-94a2-f46902d3d92e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "894cf3b3-e8c9-493a-841c-39b0b1d3d157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c00c9e1d-5abe-4000-94a2-f46902d3d92e",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "377dc927-63cf-488e-aeff-1e601c53ae8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "64d7293e-a340-4b25-a303-071c9358930c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377dc927-63cf-488e-aeff-1e601c53ae8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cfe8a23-0a1a-48fe-bb8f-5a2ad32b1c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377dc927-63cf-488e-aeff-1e601c53ae8b",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "fb483e70-a032-4c62-bd72-e3c6c7243b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "a6710f5a-4f3a-4d32-a49e-49805112f4e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb483e70-a032-4c62-bd72-e3c6c7243b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a98e4244-a8b7-4b70-a31d-20476ee64e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb483e70-a032-4c62-bd72-e3c6c7243b21",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "05f74cd7-8b82-4808-99a4-6367847d5f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "e4db11bc-14d1-4ef5-aa62-0ff0d3d9f847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05f74cd7-8b82-4808-99a4-6367847d5f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72312145-04ff-4e86-bcec-f5a8055ad1b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05f74cd7-8b82-4808-99a4-6367847d5f06",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "383c762e-506c-4074-b35f-ce014a61f632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "fc9c19e1-825b-4b15-a942-241749657821",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "383c762e-506c-4074-b35f-ce014a61f632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3110dbf-36c5-460b-916a-933ad48d6348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "383c762e-506c-4074-b35f-ce014a61f632",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "7f29695a-c615-472b-91e6-06de004d4df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "e2e5ef21-fd38-43ec-89ed-1ec8ac8ba361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f29695a-c615-472b-91e6-06de004d4df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbff2ca5-128c-49df-8f3c-afc733da2b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f29695a-c615-472b-91e6-06de004d4df1",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "fef37f89-0bcc-4ee1-aa66-41767f51ffff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "f21da5f9-feb4-472b-aeb0-5f7791a15a09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fef37f89-0bcc-4ee1-aa66-41767f51ffff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61210d5c-ab30-4f1e-bd57-5d39edcfb6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fef37f89-0bcc-4ee1-aa66-41767f51ffff",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "150523f2-9f79-4dd7-ab13-757f2caa4132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "3f71ac3f-c415-4a13-9190-0aa508b43f60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "150523f2-9f79-4dd7-ab13-757f2caa4132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "accefa77-f164-4136-aa7b-008915f37191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "150523f2-9f79-4dd7-ab13-757f2caa4132",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "d4a1f777-622d-4fc1-95ab-15e90c6bc5bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "8ca38302-4b99-4cbf-853f-451717227f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a1f777-622d-4fc1-95ab-15e90c6bc5bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e547fbb4-45b7-42f1-87a4-d3db209e0a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a1f777-622d-4fc1-95ab-15e90c6bc5bb",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "0626fff5-5783-477e-bc00-33ebac388ebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "657637b6-f1f3-43d7-87a6-b6d0fbaa5542",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0626fff5-5783-477e-bc00-33ebac388ebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7a83423-4560-4b5f-a23b-30e67c05a616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0626fff5-5783-477e-bc00-33ebac388ebe",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "f28f8247-296a-46ce-831d-e7cb767f12d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "12b102b7-2b9b-42bd-959c-ff2acbbe4c04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f28f8247-296a-46ce-831d-e7cb767f12d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff96359a-9320-42ed-b587-6f5e049601b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f28f8247-296a-46ce-831d-e7cb767f12d5",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "5b22a2e3-4d57-4683-aa49-2cedbbd875e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "17ef7173-9fc3-482a-825e-520f8e0e5ae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b22a2e3-4d57-4683-aa49-2cedbbd875e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e4c54f3-42d7-4b38-93b1-6349655a5a64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b22a2e3-4d57-4683-aa49-2cedbbd875e5",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "0b73e30c-fb4d-4dfe-8d2c-d8a05bd79022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "8238395d-e497-44d9-ab2f-b9e56c0cb149",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b73e30c-fb4d-4dfe-8d2c-d8a05bd79022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1acfd48b-4114-4c26-a230-fb69f201243a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b73e30c-fb4d-4dfe-8d2c-d8a05bd79022",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "536f519a-0bae-4558-bdd9-651efb4c33c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "31ea1175-acce-4601-9780-847798d39603",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536f519a-0bae-4558-bdd9-651efb4c33c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445b1255-9d98-479e-ade7-82ea560889fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536f519a-0bae-4558-bdd9-651efb4c33c8",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "3c991ea9-6bcd-4667-9c36-1e4acef49152",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "97ea0f8d-8b41-4c89-a36f-ee2da5917c10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c991ea9-6bcd-4667-9c36-1e4acef49152",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7e6d96e-c1e8-4fe3-af5d-e8111781c8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c991ea9-6bcd-4667-9c36-1e4acef49152",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "0cff4c07-522e-493d-a987-23d83f41885e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "a26fcf61-f3e9-4916-b664-7eda3fed72fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cff4c07-522e-493d-a987-23d83f41885e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f868237c-5731-48ee-951a-e6e52c85d811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cff4c07-522e-493d-a987-23d83f41885e",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "1faf0c43-c05b-4b0b-8eb8-58de5f286cc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "ccabf52c-9fc6-4981-9463-5891cd76dada",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1faf0c43-c05b-4b0b-8eb8-58de5f286cc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4d8d423-d064-43dc-8954-6f8f1ce011c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1faf0c43-c05b-4b0b-8eb8-58de5f286cc8",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "2abad4ff-73bd-4830-8a7c-76f01ad9bb3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "04127de4-9817-4854-a9d0-641b4ae3c2f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2abad4ff-73bd-4830-8a7c-76f01ad9bb3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c6c811-4617-410f-bd37-dea6ab2ac634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2abad4ff-73bd-4830-8a7c-76f01ad9bb3c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "8532e753-6525-46f9-b01f-63952101054e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "c0917d9f-84b5-4892-866c-85005e89ad9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8532e753-6525-46f9-b01f-63952101054e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79fb3167-7f22-429a-a2e6-5698f8f749c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8532e753-6525-46f9-b01f-63952101054e",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "e7721ab3-edb8-4860-a94f-ff906ff943a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "e49d37e9-8d02-4c2d-935b-d11c0b212c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7721ab3-edb8-4860-a94f-ff906ff943a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e1c220-6766-4c6b-b093-7acc592c2e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7721ab3-edb8-4860-a94f-ff906ff943a8",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "24238b12-2ae7-4d90-a966-7b6cacfa95e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "38b7405c-7170-468f-8101-1583a5e9637f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24238b12-2ae7-4d90-a966-7b6cacfa95e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71dbc61e-6047-4ab1-a74a-bf4a0d6780de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24238b12-2ae7-4d90-a966-7b6cacfa95e4",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "8915c837-4fd0-43ee-b12d-ee332635c4f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "4898811d-0d07-4063-8a07-f009f5028cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8915c837-4fd0-43ee-b12d-ee332635c4f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129c8218-a572-42ab-99ac-f0bd1d2cf2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8915c837-4fd0-43ee-b12d-ee332635c4f2",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "6ad24a23-2f04-4f5b-aa24-436a8c485903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "ae903c53-ec24-4976-a66c-a56660a8d872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad24a23-2f04-4f5b-aa24-436a8c485903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b3ccde-a61b-4817-b0fb-2a2043d5056a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad24a23-2f04-4f5b-aa24-436a8c485903",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "55e6f6ec-39c5-47ac-ba31-2f96e00050e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "bbbb7dcc-7a46-4c88-bb3b-982e46d4b78e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e6f6ec-39c5-47ac-ba31-2f96e00050e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a1feab8-279b-45b9-90de-1fab394c21a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e6f6ec-39c5-47ac-ba31-2f96e00050e9",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "6427554f-63b9-47d9-8fad-3558a0bf078c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "8ba66e7b-4039-4529-9f20-df170ca37588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6427554f-63b9-47d9-8fad-3558a0bf078c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afbe33a5-0236-4dd8-b338-91043732cbad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6427554f-63b9-47d9-8fad-3558a0bf078c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "72cc8ff0-1b31-4890-b61a-0775e68fa01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "a2018c0b-d466-49f0-9f53-5c2fd5760e5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72cc8ff0-1b31-4890-b61a-0775e68fa01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1197472-4c19-4a4b-9f64-1c9b8ffd1f7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72cc8ff0-1b31-4890-b61a-0775e68fa01c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "d816808d-ba1d-4475-a99e-638d5401d22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "0961afad-33e4-47b8-90a0-31e9307d5a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d816808d-ba1d-4475-a99e-638d5401d22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdca2475-51ec-49fe-8b0d-df1c1fa8f0db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d816808d-ba1d-4475-a99e-638d5401d22f",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "f6dbd105-9558-46c2-a719-008704578579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "3822a047-49d3-4989-851d-b0bdb1b79b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6dbd105-9558-46c2-a719-008704578579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc619ce0-cc7f-4803-8f75-0c66de1a642e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6dbd105-9558-46c2-a719-008704578579",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "13111070-06f2-420a-a1b1-2df357beb486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "64da773c-2adb-453a-b01f-0ae965a98747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13111070-06f2-420a-a1b1-2df357beb486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f2bd14-aeca-49af-83dd-6e7c3b88f0c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13111070-06f2-420a-a1b1-2df357beb486",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        },
        {
            "id": "2d8b38bc-7345-44b9-96c1-fc0b0b61c01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "compositeImage": {
                "id": "380be2aa-2d85-401e-abdd-bfedea4a4f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d8b38bc-7345-44b9-96c1-fc0b0b61c01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8f27de-9c53-4c25-8eb2-75c98a12926f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d8b38bc-7345-44b9-96c1-fc0b0b61c01c",
                    "LayerId": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "2398b74a-e46b-40f3-8bd8-fa04b3dfe0d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16dceb3f-6387-40e2-8271-041381d57394",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}