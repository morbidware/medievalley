{
    "id": "02f0d7b3-6cfe-4be1-b839-eea01993084b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_melee_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c846398-937d-4ab8-b55c-991b3263f6b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02f0d7b3-6cfe-4be1-b839-eea01993084b",
            "compositeImage": {
                "id": "cee19643-fcd1-4c33-ba2b-97810fc7c3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c846398-937d-4ab8-b55c-991b3263f6b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2ff7b2b-fc71-42e4-ae19-088aca569f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c846398-937d-4ab8-b55c-991b3263f6b3",
                    "LayerId": "8136b917-6435-471a-a152-e10a5ad941dd"
                }
            ]
        },
        {
            "id": "aa5486c4-0488-45c2-8a89-304c76becb61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02f0d7b3-6cfe-4be1-b839-eea01993084b",
            "compositeImage": {
                "id": "798bbd85-2f2b-4dcc-91a0-3cd1b6d7b042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa5486c4-0488-45c2-8a89-304c76becb61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb045a3f-73c2-440d-9f7f-7b28dadaba0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa5486c4-0488-45c2-8a89-304c76becb61",
                    "LayerId": "8136b917-6435-471a-a152-e10a5ad941dd"
                }
            ]
        },
        {
            "id": "5898b8da-c45e-4ebe-a190-198ebed3c18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02f0d7b3-6cfe-4be1-b839-eea01993084b",
            "compositeImage": {
                "id": "9b77136b-5465-4548-b38e-04dd1caafcb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5898b8da-c45e-4ebe-a190-198ebed3c18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35e1f29-c438-46ef-a7ad-128c338a4985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5898b8da-c45e-4ebe-a190-198ebed3c18a",
                    "LayerId": "8136b917-6435-471a-a152-e10a5ad941dd"
                }
            ]
        },
        {
            "id": "01769bfa-7ec2-4169-adec-e05fae74790b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02f0d7b3-6cfe-4be1-b839-eea01993084b",
            "compositeImage": {
                "id": "626238e7-8613-47d0-9fcc-5bad730c901e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01769bfa-7ec2-4169-adec-e05fae74790b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1dd11f-6a02-4a72-9797-1e0fb99105ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01769bfa-7ec2-4169-adec-e05fae74790b",
                    "LayerId": "8136b917-6435-471a-a152-e10a5ad941dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8136b917-6435-471a-a152-e10a5ad941dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02f0d7b3-6cfe-4be1-b839-eea01993084b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}