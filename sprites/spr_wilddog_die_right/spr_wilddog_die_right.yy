{
    "id": "4b8d4c13-5f54-42f1-bfcb-f080c0ad0a72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_die_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 38,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b04b0bd5-5693-4cbd-bde1-d197b78490bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b8d4c13-5f54-42f1-bfcb-f080c0ad0a72",
            "compositeImage": {
                "id": "b603a352-5848-44ab-bf3b-4902042bca23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04b0bd5-5693-4cbd-bde1-d197b78490bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c4b3ce9-9f9a-4e27-ae94-314bbf0ca8c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04b0bd5-5693-4cbd-bde1-d197b78490bd",
                    "LayerId": "c90499c0-371e-45f2-9331-94330a565015"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c90499c0-371e-45f2-9331-94330a565015",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b8d4c13-5f54-42f1-bfcb-f080c0ad0a72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}