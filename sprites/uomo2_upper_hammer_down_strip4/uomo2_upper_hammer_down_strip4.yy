{
    "id": "037f9594-cd70-4e77-83a4-7343d12112dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_hammer_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42f3937d-081c-4808-b03f-61ac25082eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "037f9594-cd70-4e77-83a4-7343d12112dd",
            "compositeImage": {
                "id": "fd63a9cc-d6e2-4691-95ec-ab0a84721fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f3937d-081c-4808-b03f-61ac25082eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33602eb3-ba47-46be-9b26-7cd3a4568fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f3937d-081c-4808-b03f-61ac25082eed",
                    "LayerId": "dd5887de-5ae4-4164-a856-28db4889b112"
                }
            ]
        },
        {
            "id": "32c99eb2-aa22-4e21-a86e-d4ccd0ace658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "037f9594-cd70-4e77-83a4-7343d12112dd",
            "compositeImage": {
                "id": "baba6648-d856-4c37-9612-ef7e025e21ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c99eb2-aa22-4e21-a86e-d4ccd0ace658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc354061-d3e9-4c0b-b8e3-4fbeff27beca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c99eb2-aa22-4e21-a86e-d4ccd0ace658",
                    "LayerId": "dd5887de-5ae4-4164-a856-28db4889b112"
                }
            ]
        },
        {
            "id": "d49adfa5-e4dd-4036-8027-aa7e38260ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "037f9594-cd70-4e77-83a4-7343d12112dd",
            "compositeImage": {
                "id": "b4673d23-f07e-4b02-bf1d-ab7af2be7ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d49adfa5-e4dd-4036-8027-aa7e38260ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634987d2-05b2-4455-a3ff-5cf8d870a016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d49adfa5-e4dd-4036-8027-aa7e38260ee4",
                    "LayerId": "dd5887de-5ae4-4164-a856-28db4889b112"
                }
            ]
        },
        {
            "id": "65facc17-aa29-4b85-94bb-4c13f30d84e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "037f9594-cd70-4e77-83a4-7343d12112dd",
            "compositeImage": {
                "id": "e49910b1-ed05-418d-a4d4-5378ffac26f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65facc17-aa29-4b85-94bb-4c13f30d84e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1dd0022-c849-4be7-ba1a-a1d7a1e67d33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65facc17-aa29-4b85-94bb-4c13f30d84e8",
                    "LayerId": "dd5887de-5ae4-4164-a856-28db4889b112"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd5887de-5ae4-4164-a856-28db4889b112",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "037f9594-cd70-4e77-83a4-7343d12112dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}