{
    "id": "fa4ffe7d-b580-488d-a1e1-4b67fb53e008",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_idle_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8a268e9-5560-440d-9c4d-1640ac810865",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4ffe7d-b580-488d-a1e1-4b67fb53e008",
            "compositeImage": {
                "id": "087d17c4-2169-4247-935d-adda69ca0f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8a268e9-5560-440d-9c4d-1640ac810865",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef8991e-1384-4205-9677-f1dca53e6a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a268e9-5560-440d-9c4d-1640ac810865",
                    "LayerId": "cf95816f-7f6d-4ef0-94c0-08fc09fe4e24"
                }
            ]
        },
        {
            "id": "3058a869-8c96-4393-93de-c029770906af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4ffe7d-b580-488d-a1e1-4b67fb53e008",
            "compositeImage": {
                "id": "71863738-f91d-43d3-9320-0847f36ae526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3058a869-8c96-4393-93de-c029770906af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "593fb164-02d5-48e7-8fb8-4917962d11cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3058a869-8c96-4393-93de-c029770906af",
                    "LayerId": "cf95816f-7f6d-4ef0-94c0-08fc09fe4e24"
                }
            ]
        },
        {
            "id": "8bbc91c3-ab5b-41ba-9f96-6e5c3f4a9f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4ffe7d-b580-488d-a1e1-4b67fb53e008",
            "compositeImage": {
                "id": "5dc17399-6748-4c09-85ec-7eaf50b13033",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bbc91c3-ab5b-41ba-9f96-6e5c3f4a9f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa51605d-3ee0-429b-97d2-b5ef689f2f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bbc91c3-ab5b-41ba-9f96-6e5c3f4a9f47",
                    "LayerId": "cf95816f-7f6d-4ef0-94c0-08fc09fe4e24"
                }
            ]
        },
        {
            "id": "828bcd45-0b10-4b04-819b-87ee89619d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4ffe7d-b580-488d-a1e1-4b67fb53e008",
            "compositeImage": {
                "id": "0610d65c-ea3f-4669-a359-48e6a9fadfa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "828bcd45-0b10-4b04-819b-87ee89619d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "438d920a-a3ba-45bc-acf2-261afa9cfb28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "828bcd45-0b10-4b04-819b-87ee89619d86",
                    "LayerId": "cf95816f-7f6d-4ef0-94c0-08fc09fe4e24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cf95816f-7f6d-4ef0-94c0-08fc09fe4e24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa4ffe7d-b580-488d-a1e1-4b67fb53e008",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}