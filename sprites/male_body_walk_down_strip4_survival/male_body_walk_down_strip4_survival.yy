{
    "id": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_walk_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e49ee4b-17b5-48f1-83a8-f1b3e4419822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "compositeImage": {
                "id": "d34368b8-b4ab-4e9e-8f03-a85ad0e75f2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e49ee4b-17b5-48f1-83a8-f1b3e4419822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c550d5a3-d66f-47f1-9062-4d25564cc892",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e49ee4b-17b5-48f1-83a8-f1b3e4419822",
                    "LayerId": "12aecdbf-0b5b-46f0-a744-284c4d3c1349"
                }
            ]
        },
        {
            "id": "af1e02ed-a75b-489d-9011-d4303574a1a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "compositeImage": {
                "id": "705d4600-4f27-436d-916e-22cb9ccd5dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af1e02ed-a75b-489d-9011-d4303574a1a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "014f01eb-e931-4313-90c8-f939d941946f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af1e02ed-a75b-489d-9011-d4303574a1a8",
                    "LayerId": "12aecdbf-0b5b-46f0-a744-284c4d3c1349"
                }
            ]
        },
        {
            "id": "4cdce3b2-b6aa-40a4-bf3a-93b02a6bac7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "compositeImage": {
                "id": "947f8822-2ea7-4e89-b52d-7c4280ab8b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cdce3b2-b6aa-40a4-bf3a-93b02a6bac7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a979e957-ae7c-48a6-8ebf-e37fa139ca7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cdce3b2-b6aa-40a4-bf3a-93b02a6bac7b",
                    "LayerId": "12aecdbf-0b5b-46f0-a744-284c4d3c1349"
                }
            ]
        },
        {
            "id": "3c953323-81b1-4918-a529-6a8046693658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "compositeImage": {
                "id": "215d98f1-b688-4062-92e3-ec6415ad1d78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c953323-81b1-4918-a529-6a8046693658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0047579a-4ca0-426b-9c93-bb138ad4256a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c953323-81b1-4918-a529-6a8046693658",
                    "LayerId": "12aecdbf-0b5b-46f0-a744-284c4d3c1349"
                }
            ]
        },
        {
            "id": "226435df-80a4-4348-8f07-9315ab9355e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "compositeImage": {
                "id": "1ee9b073-6628-4a9e-8c88-9e9083186a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "226435df-80a4-4348-8f07-9315ab9355e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e85743e-e857-4454-9f69-a363b878bf0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "226435df-80a4-4348-8f07-9315ab9355e7",
                    "LayerId": "12aecdbf-0b5b-46f0-a744-284c4d3c1349"
                }
            ]
        },
        {
            "id": "7fcb4935-9bf3-4492-8d24-487bc7eb1e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "compositeImage": {
                "id": "30047c1d-8047-46c9-80bd-4669f8a6e29f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcb4935-9bf3-4492-8d24-487bc7eb1e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186be781-dc82-4708-9eba-812bb25a9dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcb4935-9bf3-4492-8d24-487bc7eb1e82",
                    "LayerId": "12aecdbf-0b5b-46f0-a744-284c4d3c1349"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "12aecdbf-0b5b-46f0-a744-284c4d3c1349",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b19f7692-e85f-4ed9-adb1-d1e1d255c9a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}