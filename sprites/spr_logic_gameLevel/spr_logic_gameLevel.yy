{
    "id": "a5b1b00f-4401-4d91-a190-b0f64509c146",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logic_gameLevel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0f0a677-277a-4749-8b4b-509c483a6008",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5b1b00f-4401-4d91-a190-b0f64509c146",
            "compositeImage": {
                "id": "48499f14-b498-45f8-9715-bce96d52f679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f0a677-277a-4749-8b4b-509c483a6008",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badd2c74-bf7e-4960-815c-1666c5e5071f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f0a677-277a-4749-8b4b-509c483a6008",
                    "LayerId": "42e4b4a5-d324-429f-a9e3-bd42d9290520"
                },
                {
                    "id": "6a4611ba-e0f8-4ea3-9b04-2c064b291a63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f0a677-277a-4749-8b4b-509c483a6008",
                    "LayerId": "bd14c0c7-66d3-464e-91a5-f9409862b4a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bd14c0c7-66d3-464e-91a5-f9409862b4a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5b1b00f-4401-4d91-a190-b0f64509c146",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "42e4b4a5-d324-429f-a9e3-bd42d9290520",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5b1b00f-4401-4d91-a190-b0f64509c146",
            "blendMode": 0,
            "isLocked": true,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}