{
    "id": "10bcaa51-809c-47f5-96f9-a9de78061191",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_watering_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf77732c-d712-4c6b-8c83-8cd2b96f9fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10bcaa51-809c-47f5-96f9-a9de78061191",
            "compositeImage": {
                "id": "d6b27c12-d629-47da-a36f-816d4e0d3593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf77732c-d712-4c6b-8c83-8cd2b96f9fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79813ce4-4086-43e4-b279-de67c72459a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf77732c-d712-4c6b-8c83-8cd2b96f9fef",
                    "LayerId": "1f45f6a4-632b-4dcf-83e7-27139538080e"
                }
            ]
        },
        {
            "id": "b20a4ead-7c85-47db-bd18-7df1b2f45f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10bcaa51-809c-47f5-96f9-a9de78061191",
            "compositeImage": {
                "id": "865d487e-d100-447e-9829-809e0e5ed6c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b20a4ead-7c85-47db-bd18-7df1b2f45f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d72f9e5-83c7-44fc-99c5-dde8da4b97fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b20a4ead-7c85-47db-bd18-7df1b2f45f0d",
                    "LayerId": "1f45f6a4-632b-4dcf-83e7-27139538080e"
                }
            ]
        },
        {
            "id": "73445e4c-52b8-4d51-ba44-8f542adf0b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10bcaa51-809c-47f5-96f9-a9de78061191",
            "compositeImage": {
                "id": "9e269990-51cf-47a5-906a-df1a2f298941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73445e4c-52b8-4d51-ba44-8f542adf0b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74cb40c-b7b9-432d-bb79-4de067745d29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73445e4c-52b8-4d51-ba44-8f542adf0b24",
                    "LayerId": "1f45f6a4-632b-4dcf-83e7-27139538080e"
                }
            ]
        },
        {
            "id": "c5428435-a2af-4304-afac-ebe92d5d07c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10bcaa51-809c-47f5-96f9-a9de78061191",
            "compositeImage": {
                "id": "dc63f239-23fd-4380-818e-2ca0fd41d610",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5428435-a2af-4304-afac-ebe92d5d07c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa875684-f6b3-4838-9751-91a4b10c515f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5428435-a2af-4304-afac-ebe92d5d07c0",
                    "LayerId": "1f45f6a4-632b-4dcf-83e7-27139538080e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1f45f6a4-632b-4dcf-83e7-27139538080e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10bcaa51-809c-47f5-96f9-a9de78061191",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}