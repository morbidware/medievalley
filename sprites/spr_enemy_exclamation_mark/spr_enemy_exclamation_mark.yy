{
    "id": "775676e4-0ee5-4806-958d-874fbe0ec404",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_exclamation_mark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2678c892-57e2-41dc-990c-68b67e99a71e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "775676e4-0ee5-4806-958d-874fbe0ec404",
            "compositeImage": {
                "id": "00748a8c-98ba-47ae-800c-d8d281de4f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2678c892-57e2-41dc-990c-68b67e99a71e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9cd2d3f-ad90-41d6-aba1-360b4a5b877a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2678c892-57e2-41dc-990c-68b67e99a71e",
                    "LayerId": "290dc288-5a63-43a5-a13d-049e614c255a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "290dc288-5a63-43a5-a13d-049e614c255a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "775676e4-0ee5-4806-958d-874fbe0ec404",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 11
}