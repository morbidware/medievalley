{
    "id": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "964f1632-5cc7-47c5-9ef0-0f192b9ed809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "compositeImage": {
                "id": "d7d3e55a-41fd-4fc1-95ba-86a6e9e4823e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "964f1632-5cc7-47c5-9ef0-0f192b9ed809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31cbb1ba-eb3a-4f75-bf8f-056263505c2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "964f1632-5cc7-47c5-9ef0-0f192b9ed809",
                    "LayerId": "fa51cb38-7103-40f3-a2f1-3e00e82340c4"
                }
            ]
        },
        {
            "id": "2b1b7a3e-a2e6-4914-a758-4eac0abd1630",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "compositeImage": {
                "id": "0a9e5080-6bab-4dbd-b491-646e3c5fa301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b1b7a3e-a2e6-4914-a758-4eac0abd1630",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53cb8243-d661-4e08-81f9-8e0acb46856f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b1b7a3e-a2e6-4914-a758-4eac0abd1630",
                    "LayerId": "fa51cb38-7103-40f3-a2f1-3e00e82340c4"
                }
            ]
        },
        {
            "id": "266438ba-cc5c-48da-8d63-871de86c15f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "compositeImage": {
                "id": "e50e99d0-4327-4e82-9b85-1c789476bf67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266438ba-cc5c-48da-8d63-871de86c15f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3883bd15-aad5-4369-9253-e45f55d96ca4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266438ba-cc5c-48da-8d63-871de86c15f3",
                    "LayerId": "fa51cb38-7103-40f3-a2f1-3e00e82340c4"
                }
            ]
        },
        {
            "id": "37f6a998-db96-4fef-9493-69605174f930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "compositeImage": {
                "id": "e6823244-7b4e-4895-8562-8ae87c219a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f6a998-db96-4fef-9493-69605174f930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef40c87c-ddde-4259-b785-e6668f65954e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f6a998-db96-4fef-9493-69605174f930",
                    "LayerId": "fa51cb38-7103-40f3-a2f1-3e00e82340c4"
                }
            ]
        },
        {
            "id": "fac2dd2f-ab80-4381-ae03-9243626c5097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "compositeImage": {
                "id": "84c0f56d-47a9-4e52-83c4-5eac887e75ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac2dd2f-ab80-4381-ae03-9243626c5097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7290d25-eaf2-4959-a8ab-6930a4061da6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac2dd2f-ab80-4381-ae03-9243626c5097",
                    "LayerId": "fa51cb38-7103-40f3-a2f1-3e00e82340c4"
                }
            ]
        },
        {
            "id": "128ca5c6-4d34-476b-b42a-24ec2b8a5b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "compositeImage": {
                "id": "22a61a09-114a-4268-87f2-29be0d58421c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "128ca5c6-4d34-476b-b42a-24ec2b8a5b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64d7bf04-0d1a-4f3a-8277-c34db7e04cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128ca5c6-4d34-476b-b42a-24ec2b8a5b2f",
                    "LayerId": "fa51cb38-7103-40f3-a2f1-3e00e82340c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa51cb38-7103-40f3-a2f1-3e00e82340c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bae3eae3-de37-4bb3-be69-a6d4cabac7a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}