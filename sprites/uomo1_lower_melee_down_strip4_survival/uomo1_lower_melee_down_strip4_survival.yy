{
    "id": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_melee_down_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9810920d-8d6b-472d-9bfd-f5d84ee58105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "compositeImage": {
                "id": "33c6fc27-0811-4034-a499-fd4b9f200360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9810920d-8d6b-472d-9bfd-f5d84ee58105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2289df2-6f88-472d-8b94-eaa8f9e0e3c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9810920d-8d6b-472d-9bfd-f5d84ee58105",
                    "LayerId": "fe854fac-472f-4384-a6e5-bfa5a6e3f996"
                }
            ]
        },
        {
            "id": "2bad3c58-8a04-4f0d-ae51-748db95676b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "compositeImage": {
                "id": "f0990c03-011b-48f1-a677-6371c3a89dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bad3c58-8a04-4f0d-ae51-748db95676b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad4c0a9-3a2a-43f6-a8a3-cf33dc54d7a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bad3c58-8a04-4f0d-ae51-748db95676b4",
                    "LayerId": "fe854fac-472f-4384-a6e5-bfa5a6e3f996"
                }
            ]
        },
        {
            "id": "652371b7-4aef-4aed-93ee-f1f904dab0e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "compositeImage": {
                "id": "4a71da09-8759-4fc5-b48d-a68c6329e7c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652371b7-4aef-4aed-93ee-f1f904dab0e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a936db-e470-4e98-8fd2-61b175033e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652371b7-4aef-4aed-93ee-f1f904dab0e2",
                    "LayerId": "fe854fac-472f-4384-a6e5-bfa5a6e3f996"
                }
            ]
        },
        {
            "id": "8c4cff70-8989-4c5d-a7a7-0fb5f9695f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "compositeImage": {
                "id": "2a450454-0dc4-49bd-92dd-19c12f68702a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c4cff70-8989-4c5d-a7a7-0fb5f9695f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68859f5c-09f9-4c36-9a2e-39a5693ee9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c4cff70-8989-4c5d-a7a7-0fb5f9695f96",
                    "LayerId": "fe854fac-472f-4384-a6e5-bfa5a6e3f996"
                }
            ]
        },
        {
            "id": "dac2f5b9-2865-41a5-af3e-29d714f86629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "compositeImage": {
                "id": "886ed6cc-5a9f-430c-a86f-48f742a66656",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dac2f5b9-2865-41a5-af3e-29d714f86629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815a6ac7-a30b-459d-aded-e96ba4d02c21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dac2f5b9-2865-41a5-af3e-29d714f86629",
                    "LayerId": "fe854fac-472f-4384-a6e5-bfa5a6e3f996"
                }
            ]
        },
        {
            "id": "029827a1-cca9-4810-8970-324e309bbbcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "compositeImage": {
                "id": "d492141e-ce01-4966-a0ce-6fb2c03c713f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "029827a1-cca9-4810-8970-324e309bbbcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8548a5a-52ef-441d-8414-07c000c10e3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "029827a1-cca9-4810-8970-324e309bbbcb",
                    "LayerId": "fe854fac-472f-4384-a6e5-bfa5a6e3f996"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fe854fac-472f-4384-a6e5-bfa5a6e3f996",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d4a3fc-c56d-4767-8c06-a717069166b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}