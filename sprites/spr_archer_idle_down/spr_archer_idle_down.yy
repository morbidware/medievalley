{
    "id": "2e250483-c5c8-4cd3-9748-df4c992c4c57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ce0e87f-22a9-42af-9279-aaa56e60cb9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e250483-c5c8-4cd3-9748-df4c992c4c57",
            "compositeImage": {
                "id": "fb61134c-4b0c-4afb-87a7-dfdcc5b1df93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce0e87f-22a9-42af-9279-aaa56e60cb9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e934b5-28b2-4b2e-8a5c-330d3b0c0ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce0e87f-22a9-42af-9279-aaa56e60cb9f",
                    "LayerId": "68226023-1b64-4e80-b550-07a063a660b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "68226023-1b64-4e80-b550-07a063a660b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e250483-c5c8-4cd3-9748-df4c992c4c57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}