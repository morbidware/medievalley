{
    "id": "ced7ed0a-ea72-4950-933d-e18548e2b012",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_redcabbage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48704c17-5c31-47df-bb04-93018158e96f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced7ed0a-ea72-4950-933d-e18548e2b012",
            "compositeImage": {
                "id": "c4686bbf-621e-453a-987a-066bff803614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48704c17-5c31-47df-bb04-93018158e96f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ce9c1fe-b77c-4a87-9363-01a66ecb84bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48704c17-5c31-47df-bb04-93018158e96f",
                    "LayerId": "6268fbbe-d782-4929-b845-f614add40517"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6268fbbe-d782-4929-b845-f614add40517",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ced7ed0a-ea72-4950-933d-e18548e2b012",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}