{
    "id": "287c2740-4c85-4226-aa1e-0bdc77d03783",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_stats_bg_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23292c2f-b79f-4157-bbef-b88c0583ebb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "287c2740-4c85-4226-aa1e-0bdc77d03783",
            "compositeImage": {
                "id": "1ccc717f-b067-49fa-8cc4-1bfc7ce1923a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23292c2f-b79f-4157-bbef-b88c0583ebb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e29c3ef-bc6b-40e8-a35c-dad58d2217bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23292c2f-b79f-4157-bbef-b88c0583ebb7",
                    "LayerId": "61ed539d-a973-419f-ba27-cd442fce7630"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 81,
    "layers": [
        {
            "id": "61ed539d-a973-419f-ba27-cd442fce7630",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "287c2740-4c85-4226-aa1e-0bdc77d03783",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 35,
    "xorig": 6,
    "yorig": 2
}