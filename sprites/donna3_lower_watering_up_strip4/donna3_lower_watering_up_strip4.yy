{
    "id": "b084876c-0dc9-4b4d-b0c8-489c612f9819",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_lower_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbfd34b9-0343-48ca-bebd-ef3aa2e57b53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084876c-0dc9-4b4d-b0c8-489c612f9819",
            "compositeImage": {
                "id": "af724c59-02dc-4ed9-abeb-523739c35f70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbfd34b9-0343-48ca-bebd-ef3aa2e57b53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3fc9118-04b5-47ce-8407-5d1805ec0272",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbfd34b9-0343-48ca-bebd-ef3aa2e57b53",
                    "LayerId": "5993a2d4-358b-48ca-9403-fb80d3213dbf"
                }
            ]
        },
        {
            "id": "3965f135-3e82-46eb-a21c-1853ea80ec23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084876c-0dc9-4b4d-b0c8-489c612f9819",
            "compositeImage": {
                "id": "cf09fa60-f161-4f6e-a15e-f079586d4171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3965f135-3e82-46eb-a21c-1853ea80ec23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca10d79-1e22-4c15-867b-0d9f8d3fc66a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3965f135-3e82-46eb-a21c-1853ea80ec23",
                    "LayerId": "5993a2d4-358b-48ca-9403-fb80d3213dbf"
                }
            ]
        },
        {
            "id": "5101e773-fa79-4155-86a8-46d7493d80ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084876c-0dc9-4b4d-b0c8-489c612f9819",
            "compositeImage": {
                "id": "86a41b64-b60b-476e-a790-39dfa3b5f265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5101e773-fa79-4155-86a8-46d7493d80ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6d73da7-3fda-45b0-a12d-4a0ee3940f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5101e773-fa79-4155-86a8-46d7493d80ff",
                    "LayerId": "5993a2d4-358b-48ca-9403-fb80d3213dbf"
                }
            ]
        },
        {
            "id": "71de2ee8-8aa9-4e1e-8f47-565d3203e110",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b084876c-0dc9-4b4d-b0c8-489c612f9819",
            "compositeImage": {
                "id": "6d9ca1ae-d722-4919-a1b6-7df9f12b269b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71de2ee8-8aa9-4e1e-8f47-565d3203e110",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0250b574-d7e2-497d-b2ef-48259f3660a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71de2ee8-8aa9-4e1e-8f47-565d3203e110",
                    "LayerId": "5993a2d4-358b-48ca-9403-fb80d3213dbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5993a2d4-358b-48ca-9403-fb80d3213dbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b084876c-0dc9-4b4d-b0c8-489c612f9819",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}