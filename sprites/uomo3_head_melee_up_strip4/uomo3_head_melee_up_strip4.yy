{
    "id": "efe09479-7c3e-4e1c-9f55-ef47ab77452f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_head_melee_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22e33ec7-be88-4088-a4ec-b737fb0eea23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efe09479-7c3e-4e1c-9f55-ef47ab77452f",
            "compositeImage": {
                "id": "6cfb7669-2e98-4226-8361-40c18a4e4c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e33ec7-be88-4088-a4ec-b737fb0eea23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4520684f-b895-484b-882b-a0a946d18636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e33ec7-be88-4088-a4ec-b737fb0eea23",
                    "LayerId": "64e26a60-f6db-4609-afc4-7ec75736bdfb"
                }
            ]
        },
        {
            "id": "7662647c-feb5-4d84-9af5-99bdb249b618",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efe09479-7c3e-4e1c-9f55-ef47ab77452f",
            "compositeImage": {
                "id": "b0473d8e-85d0-42b3-b372-6e6a5c63541a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7662647c-feb5-4d84-9af5-99bdb249b618",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec825b95-8f4b-4f0a-9abd-81bebeecf01b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7662647c-feb5-4d84-9af5-99bdb249b618",
                    "LayerId": "64e26a60-f6db-4609-afc4-7ec75736bdfb"
                }
            ]
        },
        {
            "id": "2314eeac-17a9-4291-9078-7542ea465778",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efe09479-7c3e-4e1c-9f55-ef47ab77452f",
            "compositeImage": {
                "id": "a97a3075-d200-47d8-9745-3fef270a88b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2314eeac-17a9-4291-9078-7542ea465778",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b403609-eee7-40d7-8ff3-9ebc3b5bb6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2314eeac-17a9-4291-9078-7542ea465778",
                    "LayerId": "64e26a60-f6db-4609-afc4-7ec75736bdfb"
                }
            ]
        },
        {
            "id": "526e4ba0-9a1b-41fa-a505-7dcbf7c1c344",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efe09479-7c3e-4e1c-9f55-ef47ab77452f",
            "compositeImage": {
                "id": "a9a0bac6-090f-4879-9c7e-df597a6bcfbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "526e4ba0-9a1b-41fa-a505-7dcbf7c1c344",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f4c2004-9b89-4dde-854f-3cb73970399f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "526e4ba0-9a1b-41fa-a505-7dcbf7c1c344",
                    "LayerId": "64e26a60-f6db-4609-afc4-7ec75736bdfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "64e26a60-f6db-4609-afc4-7ec75736bdfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efe09479-7c3e-4e1c-9f55-ef47ab77452f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}