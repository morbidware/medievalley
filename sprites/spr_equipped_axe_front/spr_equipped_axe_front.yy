{
    "id": "15f538e1-5c5d-4033-b4f1-96e6b360e0f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_equipped_axe_front",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daf744bb-c757-471c-8523-e4f165b3ba98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15f538e1-5c5d-4033-b4f1-96e6b360e0f0",
            "compositeImage": {
                "id": "c8c20718-26d8-41be-8828-6f839046d40b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf744bb-c757-471c-8523-e4f165b3ba98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c20b342-f5a6-418c-9ab4-ca36d66821b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf744bb-c757-471c-8523-e4f165b3ba98",
                    "LayerId": "c8918c25-b6e6-484c-bc89-b77dffa6f552"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "c8918c25-b6e6-484c-bc89-b77dffa6f552",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15f538e1-5c5d-4033-b4f1-96e6b360e0f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 2,
    "yorig": 18
}