{
    "id": "7c764e78-cea9-41d7-b194-eba1938f7f5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_pick_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3e89da6-a0f9-4c72-8ccf-2429587fc378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c764e78-cea9-41d7-b194-eba1938f7f5f",
            "compositeImage": {
                "id": "5dd91a24-6986-4244-a090-066d26982e63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3e89da6-a0f9-4c72-8ccf-2429587fc378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3808521-5c28-4fd4-be03-8823a5ae8186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3e89da6-a0f9-4c72-8ccf-2429587fc378",
                    "LayerId": "bf459d00-dc84-4db6-b07b-1e43b65e60e7"
                }
            ]
        },
        {
            "id": "153b1010-44a2-4372-9ed2-f9423cd78f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c764e78-cea9-41d7-b194-eba1938f7f5f",
            "compositeImage": {
                "id": "872cccbe-cf61-43a3-86c2-a70e483e6a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "153b1010-44a2-4372-9ed2-f9423cd78f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d590e7c3-a837-4594-9e82-f121c54100c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "153b1010-44a2-4372-9ed2-f9423cd78f7f",
                    "LayerId": "bf459d00-dc84-4db6-b07b-1e43b65e60e7"
                }
            ]
        },
        {
            "id": "949f6ee7-737c-4b0a-94f7-a619ea0f9dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c764e78-cea9-41d7-b194-eba1938f7f5f",
            "compositeImage": {
                "id": "4aee22ec-f3ab-46b8-b2c9-126007842b14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "949f6ee7-737c-4b0a-94f7-a619ea0f9dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0f369a7-13f5-429e-9f01-5a2b22c66c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "949f6ee7-737c-4b0a-94f7-a619ea0f9dc3",
                    "LayerId": "bf459d00-dc84-4db6-b07b-1e43b65e60e7"
                }
            ]
        },
        {
            "id": "aa62ae8e-6cbc-4e23-8334-9d6de79cf8ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c764e78-cea9-41d7-b194-eba1938f7f5f",
            "compositeImage": {
                "id": "9de54b4d-c8c2-4925-8cf9-c3a6cbb7f554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa62ae8e-6cbc-4e23-8334-9d6de79cf8ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84830f79-6352-466f-8346-6c4d063f7830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa62ae8e-6cbc-4e23-8334-9d6de79cf8ba",
                    "LayerId": "bf459d00-dc84-4db6-b07b-1e43b65e60e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf459d00-dc84-4db6-b07b-1e43b65e60e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c764e78-cea9-41d7-b194-eba1938f7f5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}