{
    "id": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_pick_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3c5f415-aeb2-4841-aaad-b9e297e1485f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "compositeImage": {
                "id": "e1a70540-86f0-4f9f-bd53-60d5d4d1f143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3c5f415-aeb2-4841-aaad-b9e297e1485f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2426ee93-2b47-4321-a0d8-2eb70c638151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3c5f415-aeb2-4841-aaad-b9e297e1485f",
                    "LayerId": "0845f240-4c5c-4dec-bf11-ff7c733cb81c"
                }
            ]
        },
        {
            "id": "d0780378-8f1a-44ed-99fe-874e08c2d2e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "compositeImage": {
                "id": "c9162c91-50ec-4ddc-b9c6-621ae3c27426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0780378-8f1a-44ed-99fe-874e08c2d2e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be269143-c23b-4367-bbbc-41c6a40adf55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0780378-8f1a-44ed-99fe-874e08c2d2e4",
                    "LayerId": "0845f240-4c5c-4dec-bf11-ff7c733cb81c"
                }
            ]
        },
        {
            "id": "b6c7ed52-ae42-43f4-b036-065f35ae84d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "compositeImage": {
                "id": "94e432aa-3001-4460-be41-8e07313c27cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6c7ed52-ae42-43f4-b036-065f35ae84d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45bac96-a67d-474f-aaee-ee9bab273538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6c7ed52-ae42-43f4-b036-065f35ae84d3",
                    "LayerId": "0845f240-4c5c-4dec-bf11-ff7c733cb81c"
                }
            ]
        },
        {
            "id": "0ef3ae90-8ca2-4fb9-aaf3-a556a2a681f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "compositeImage": {
                "id": "a05c834b-4fa2-4a4a-82a9-8e6f9caf950a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef3ae90-8ca2-4fb9-aaf3-a556a2a681f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce377f14-118c-407e-b5fe-5e57025db09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef3ae90-8ca2-4fb9-aaf3-a556a2a681f0",
                    "LayerId": "0845f240-4c5c-4dec-bf11-ff7c733cb81c"
                }
            ]
        },
        {
            "id": "b0b9dade-7497-46c8-a377-37b1ea80f2c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "compositeImage": {
                "id": "143da5c4-1e43-4f6f-b2bf-90ff35186742",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b9dade-7497-46c8-a377-37b1ea80f2c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e2baad5-02e1-4014-85a6-94e91238b2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b9dade-7497-46c8-a377-37b1ea80f2c0",
                    "LayerId": "0845f240-4c5c-4dec-bf11-ff7c733cb81c"
                }
            ]
        },
        {
            "id": "dca98ca5-f3d2-4663-b8d5-533372ccd747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "compositeImage": {
                "id": "052556cf-59dd-4e8a-87d3-21f9683f266f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dca98ca5-f3d2-4663-b8d5-533372ccd747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4760e0-f1f3-4e3a-918a-88da8e01eb27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dca98ca5-f3d2-4663-b8d5-533372ccd747",
                    "LayerId": "0845f240-4c5c-4dec-bf11-ff7c733cb81c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0845f240-4c5c-4dec-bf11-ff7c733cb81c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3101e098-c9df-42b2-9673-d43f10bbd0f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}