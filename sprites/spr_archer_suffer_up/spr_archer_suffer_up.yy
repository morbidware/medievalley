{
    "id": "71ba46a6-e075-4c3a-8ba2-1c080bafbf9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_suffer_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3dc6b52-4c02-4775-8d48-f4370d99f7e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71ba46a6-e075-4c3a-8ba2-1c080bafbf9a",
            "compositeImage": {
                "id": "7c9fe3c1-1099-43d1-a880-d54a4c2bb6ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3dc6b52-4c02-4775-8d48-f4370d99f7e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f89eea3f-9077-4f72-9aca-e7c130e4ca28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3dc6b52-4c02-4775-8d48-f4370d99f7e3",
                    "LayerId": "1300e8cc-7cef-4830-8f1e-d3a0dc6f1455"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1300e8cc-7cef-4830-8f1e-d3a0dc6f1455",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71ba46a6-e075-4c3a-8ba2-1c080bafbf9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}