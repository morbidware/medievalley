{
    "id": "7a0e958e-65f5-4281-a146-96b34eeff5b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2b8dde9-4a9c-4a3c-9076-f17b6fcc4c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a0e958e-65f5-4281-a146-96b34eeff5b8",
            "compositeImage": {
                "id": "c343d7b1-e424-4f9d-8782-6ea6bc45367f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b8dde9-4a9c-4a3c-9076-f17b6fcc4c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47fb5cfc-bdee-42e6-9130-6e66a9f445eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b8dde9-4a9c-4a3c-9076-f17b6fcc4c32",
                    "LayerId": "56e7c733-01ef-48bd-a690-3e89e35bc283"
                }
            ]
        },
        {
            "id": "4546d308-23ce-45b3-81d7-f15f9b6e82a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a0e958e-65f5-4281-a146-96b34eeff5b8",
            "compositeImage": {
                "id": "d6381af4-aa03-4022-a5c9-78993b2f15b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4546d308-23ce-45b3-81d7-f15f9b6e82a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc7f04b9-5be4-4e01-b5aa-9b439954a0f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4546d308-23ce-45b3-81d7-f15f9b6e82a3",
                    "LayerId": "56e7c733-01ef-48bd-a690-3e89e35bc283"
                }
            ]
        },
        {
            "id": "807eed9f-c660-49e7-ac6a-2ed437fe9c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a0e958e-65f5-4281-a146-96b34eeff5b8",
            "compositeImage": {
                "id": "3f62f87e-3c3a-414c-b46f-a46556567e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "807eed9f-c660-49e7-ac6a-2ed437fe9c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "883b21a1-03d4-4b23-866b-1f60c3215e4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "807eed9f-c660-49e7-ac6a-2ed437fe9c56",
                    "LayerId": "56e7c733-01ef-48bd-a690-3e89e35bc283"
                }
            ]
        },
        {
            "id": "59649709-12ea-43e5-a68a-ffbc67701144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a0e958e-65f5-4281-a146-96b34eeff5b8",
            "compositeImage": {
                "id": "4aa47a7b-0aa0-4695-9f98-fd99126364c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59649709-12ea-43e5-a68a-ffbc67701144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2196e8e0-e11a-43b2-ac6b-e87a847d4576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59649709-12ea-43e5-a68a-ffbc67701144",
                    "LayerId": "56e7c733-01ef-48bd-a690-3e89e35bc283"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56e7c733-01ef-48bd-a690-3e89e35bc283",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a0e958e-65f5-4281-a146-96b34eeff5b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}