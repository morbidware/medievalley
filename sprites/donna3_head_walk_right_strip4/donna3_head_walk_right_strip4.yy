{
    "id": "2ea4420c-3855-4301-b470-dfd86167ee31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna3_head_walk_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d34e87ba-7541-4459-bda7-4b97bd8d266d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea4420c-3855-4301-b470-dfd86167ee31",
            "compositeImage": {
                "id": "13ac830c-37c5-41d5-b9d2-185616072684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d34e87ba-7541-4459-bda7-4b97bd8d266d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e62d85-6612-415c-abbc-b467b6b96437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d34e87ba-7541-4459-bda7-4b97bd8d266d",
                    "LayerId": "ba8804b0-d232-4e7b-a112-4bd3f1f300aa"
                }
            ]
        },
        {
            "id": "d6082630-7ce7-45e4-9596-5c921f32224c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea4420c-3855-4301-b470-dfd86167ee31",
            "compositeImage": {
                "id": "d200fc07-6ee0-4b95-943c-417739b0417b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6082630-7ce7-45e4-9596-5c921f32224c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2599bca0-4301-4ee4-85e7-dd9f960d9aae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6082630-7ce7-45e4-9596-5c921f32224c",
                    "LayerId": "ba8804b0-d232-4e7b-a112-4bd3f1f300aa"
                }
            ]
        },
        {
            "id": "a489e19b-209b-4e70-ae87-0367f5c12923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea4420c-3855-4301-b470-dfd86167ee31",
            "compositeImage": {
                "id": "ebb93dcf-3e33-4417-8883-a892918d745e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a489e19b-209b-4e70-ae87-0367f5c12923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c38920-440f-42a5-b57e-92bf23af3fbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a489e19b-209b-4e70-ae87-0367f5c12923",
                    "LayerId": "ba8804b0-d232-4e7b-a112-4bd3f1f300aa"
                }
            ]
        },
        {
            "id": "a4011ac7-b807-4b58-9b7f-163648ef21e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea4420c-3855-4301-b470-dfd86167ee31",
            "compositeImage": {
                "id": "99df79d4-debc-4716-97e4-97d0f32e96c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4011ac7-b807-4b58-9b7f-163648ef21e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ce8711-d0b8-4d36-96a6-cdcd1cca1f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4011ac7-b807-4b58-9b7f-163648ef21e9",
                    "LayerId": "ba8804b0-d232-4e7b-a112-4bd3f1f300aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba8804b0-d232-4e7b-a112-4bd3f1f300aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ea4420c-3855-4301-b470-dfd86167ee31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}