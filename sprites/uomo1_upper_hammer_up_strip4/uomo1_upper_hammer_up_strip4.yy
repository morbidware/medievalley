{
    "id": "2a7e3948-6d88-42a4-9166-f19ed70aebdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4408192b-73b7-4d77-b278-2f06f80745e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a7e3948-6d88-42a4-9166-f19ed70aebdd",
            "compositeImage": {
                "id": "735c7aa0-98ba-414d-a726-7766af5de3f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4408192b-73b7-4d77-b278-2f06f80745e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60ccdc53-b2d4-4afc-8fab-081a4941bb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4408192b-73b7-4d77-b278-2f06f80745e7",
                    "LayerId": "44a957d0-a23f-4094-93b9-033bcf199c8c"
                }
            ]
        },
        {
            "id": "0918fe4e-ad54-4025-9d44-e0f809b94cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a7e3948-6d88-42a4-9166-f19ed70aebdd",
            "compositeImage": {
                "id": "074c3db1-1f27-492e-8877-05cc778978ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0918fe4e-ad54-4025-9d44-e0f809b94cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "060b32e1-c2ac-46a8-b450-f08c0eb51377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0918fe4e-ad54-4025-9d44-e0f809b94cf2",
                    "LayerId": "44a957d0-a23f-4094-93b9-033bcf199c8c"
                }
            ]
        },
        {
            "id": "cf39a337-7ac7-4436-867e-7992cf43823e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a7e3948-6d88-42a4-9166-f19ed70aebdd",
            "compositeImage": {
                "id": "2c18a85c-fcf2-479a-9665-e7ca1e2922ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf39a337-7ac7-4436-867e-7992cf43823e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7c4e12f-1aea-4848-9579-1313c20ed567",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf39a337-7ac7-4436-867e-7992cf43823e",
                    "LayerId": "44a957d0-a23f-4094-93b9-033bcf199c8c"
                }
            ]
        },
        {
            "id": "f56b94ff-c46e-457d-8fcb-5229f9d60cc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a7e3948-6d88-42a4-9166-f19ed70aebdd",
            "compositeImage": {
                "id": "235cf4cb-1df3-4cd5-b137-301ec2da2156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56b94ff-c46e-457d-8fcb-5229f9d60cc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead3e95f-43a6-4d25-94b6-e0ab4fe5391b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56b94ff-c46e-457d-8fcb-5229f9d60cc9",
                    "LayerId": "44a957d0-a23f-4094-93b9-033bcf199c8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "44a957d0-a23f-4094-93b9-033bcf199c8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a7e3948-6d88-42a4-9166-f19ed70aebdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}