{
    "id": "7b4c5256-52c8-4e93-8151-ed4b069ae7ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "14b68e50-7d8c-45d0-b6fd-4961fc25fe86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b4c5256-52c8-4e93-8151-ed4b069ae7ab",
            "compositeImage": {
                "id": "27f9c6a4-4e61-460a-bc44-dc25e5a27f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b68e50-7d8c-45d0-b6fd-4961fc25fe86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86cbf30d-4ed5-42b2-9a88-38f2826ead71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b68e50-7d8c-45d0-b6fd-4961fc25fe86",
                    "LayerId": "7032ab01-05f0-412b-90ea-b75b5fb04a19"
                }
            ]
        },
        {
            "id": "f06c4d9f-b63c-4fac-8692-dcabc19322d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b4c5256-52c8-4e93-8151-ed4b069ae7ab",
            "compositeImage": {
                "id": "42886521-8670-4379-962a-95bab56254e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06c4d9f-b63c-4fac-8692-dcabc19322d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a12d59f-3ce3-4e5d-bf95-176e36eb3b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06c4d9f-b63c-4fac-8692-dcabc19322d5",
                    "LayerId": "7032ab01-05f0-412b-90ea-b75b5fb04a19"
                }
            ]
        },
        {
            "id": "6c0a8af7-d96e-4083-a9a6-50ecd4e5a210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b4c5256-52c8-4e93-8151-ed4b069ae7ab",
            "compositeImage": {
                "id": "6dda90dc-9d01-4351-9d44-6ab66b1858cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c0a8af7-d96e-4083-a9a6-50ecd4e5a210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04de18b7-f89a-49f3-b1f8-9faf695e14c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c0a8af7-d96e-4083-a9a6-50ecd4e5a210",
                    "LayerId": "7032ab01-05f0-412b-90ea-b75b5fb04a19"
                }
            ]
        },
        {
            "id": "8de12436-dd1f-4bba-83c0-51351860aa15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b4c5256-52c8-4e93-8151-ed4b069ae7ab",
            "compositeImage": {
                "id": "20ceea6f-53b5-4d94-8933-2b3a4c868439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8de12436-dd1f-4bba-83c0-51351860aa15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9677bc5c-b2cc-4e84-be2f-4f696dab32a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8de12436-dd1f-4bba-83c0-51351860aa15",
                    "LayerId": "7032ab01-05f0-412b-90ea-b75b5fb04a19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7032ab01-05f0-412b-90ea-b75b5fb04a19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b4c5256-52c8-4e93-8151-ed4b069ae7ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}