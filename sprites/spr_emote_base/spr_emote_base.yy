{
    "id": "f07f0fcc-c48b-418e-8c13-2cc866b2f9c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_emote_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1897021-ecc6-41b4-a6c3-74eaa90d875e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f07f0fcc-c48b-418e-8c13-2cc866b2f9c0",
            "compositeImage": {
                "id": "bbcc471e-142e-4294-a5f1-b5cf1543ff7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1897021-ecc6-41b4-a6c3-74eaa90d875e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d5e249-cefc-4bf4-9ce3-9d52d2aa27a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1897021-ecc6-41b4-a6c3-74eaa90d875e",
                    "LayerId": "8d2ebc31-6e88-4728-b069-b483a6a40361"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "8d2ebc31-6e88-4728-b069-b483a6a40361",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f07f0fcc-c48b-418e-8c13-2cc866b2f9c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 19
}