{
    "id": "077d338d-df3f-4bbb-a0c4-1f00c14156c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_plains_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b88bf96e-8f15-4080-afeb-54c7e0302df4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "077d338d-df3f-4bbb-a0c4-1f00c14156c3",
            "compositeImage": {
                "id": "ebaaff40-2b92-4fe3-966c-90689b67d672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b88bf96e-8f15-4080-afeb-54c7e0302df4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c74cc4-d2d6-488d-89ba-752b9fa4bce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b88bf96e-8f15-4080-afeb-54c7e0302df4",
                    "LayerId": "d5520398-6cf6-4a78-ac00-ce3754ca1ff7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d5520398-6cf6-4a78-ac00-ce3754ca1ff7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "077d338d-df3f-4bbb-a0c4-1f00c14156c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}