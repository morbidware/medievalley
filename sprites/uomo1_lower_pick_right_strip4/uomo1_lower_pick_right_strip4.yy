{
    "id": "c7722ba8-faac-433a-a810-714498ed9e8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_lower_pick_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 12,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3feffc51-69b5-494b-92f3-38cd1438b6fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7722ba8-faac-433a-a810-714498ed9e8b",
            "compositeImage": {
                "id": "7b7199b0-6998-414b-8b8e-13ede3e7bb94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3feffc51-69b5-494b-92f3-38cd1438b6fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a4755a-445b-4f03-afe5-228c6448dd14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3feffc51-69b5-494b-92f3-38cd1438b6fb",
                    "LayerId": "8b7e73fa-6167-4d89-8002-579c11afe829"
                }
            ]
        },
        {
            "id": "957e40fb-2df0-401e-a214-5011f5cada3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7722ba8-faac-433a-a810-714498ed9e8b",
            "compositeImage": {
                "id": "f20b6920-bb75-435d-bd86-2145e0cad580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "957e40fb-2df0-401e-a214-5011f5cada3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde9b5e6-7416-4658-850e-06d7faa7dab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "957e40fb-2df0-401e-a214-5011f5cada3a",
                    "LayerId": "8b7e73fa-6167-4d89-8002-579c11afe829"
                }
            ]
        },
        {
            "id": "a16efbd2-4843-40f5-a625-7859e08d6fee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7722ba8-faac-433a-a810-714498ed9e8b",
            "compositeImage": {
                "id": "2943bd86-ada7-4ec8-a68e-eb5e6a2cdc6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a16efbd2-4843-40f5-a625-7859e08d6fee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1394c1-91f3-4d94-8324-683645e0f52d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a16efbd2-4843-40f5-a625-7859e08d6fee",
                    "LayerId": "8b7e73fa-6167-4d89-8002-579c11afe829"
                }
            ]
        },
        {
            "id": "f41920d6-78c2-4386-8220-fee5cdda84e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7722ba8-faac-433a-a810-714498ed9e8b",
            "compositeImage": {
                "id": "d3f936a5-cba0-499e-a49d-4555544694f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f41920d6-78c2-4386-8220-fee5cdda84e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7b11ec-3668-4583-bd83-f89d9d5437f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f41920d6-78c2-4386-8220-fee5cdda84e9",
                    "LayerId": "8b7e73fa-6167-4d89-8002-579c11afe829"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b7e73fa-6167-4d89-8002-579c11afe829",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7722ba8-faac-433a-a810-714498ed9e8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}