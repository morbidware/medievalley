{
    "id": "526ea02d-ca02-4f9b-a13a-f875b28ddd0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_idle_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 14,
    "bbox_right": 32,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88909811-ec1a-4ab3-a8c8-212a0c5407e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "526ea02d-ca02-4f9b-a13a-f875b28ddd0d",
            "compositeImage": {
                "id": "5a2f797d-2822-499d-ab7f-ec5c9f425502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88909811-ec1a-4ab3-a8c8-212a0c5407e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f44804f6-941e-444c-931f-4e1fbf5e6b4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88909811-ec1a-4ab3-a8c8-212a0c5407e3",
                    "LayerId": "63cf64ce-22a5-40b5-9a95-c5dc8f543823"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "63cf64ce-22a5-40b5-9a95-c5dc8f543823",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "526ea02d-ca02-4f9b-a13a-f875b28ddd0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}