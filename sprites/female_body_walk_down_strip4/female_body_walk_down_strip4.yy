{
    "id": "f84b021d-86f1-4dd8-af50-3a0d46a1d084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "female_body_walk_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3356d1ab-036d-4934-abcf-064da5409d6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f84b021d-86f1-4dd8-af50-3a0d46a1d084",
            "compositeImage": {
                "id": "667ab246-4f97-4195-be75-0064b872d118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3356d1ab-036d-4934-abcf-064da5409d6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "852a6b05-ff3e-43f5-b22f-68252afe798a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3356d1ab-036d-4934-abcf-064da5409d6a",
                    "LayerId": "f6adae16-1e07-450f-8064-1cdb83a869bb"
                }
            ]
        },
        {
            "id": "73c3e179-295a-40a7-975a-08182b1eab9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f84b021d-86f1-4dd8-af50-3a0d46a1d084",
            "compositeImage": {
                "id": "b886eec4-17cd-4dcb-8a54-205536c141e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c3e179-295a-40a7-975a-08182b1eab9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58b5cab9-9023-4ab3-9c10-17e2926aed12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c3e179-295a-40a7-975a-08182b1eab9d",
                    "LayerId": "f6adae16-1e07-450f-8064-1cdb83a869bb"
                }
            ]
        },
        {
            "id": "f7120be8-ea1f-4f38-83f4-5ccf2b611aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f84b021d-86f1-4dd8-af50-3a0d46a1d084",
            "compositeImage": {
                "id": "ef4cf346-94ac-45bf-a3b1-b17648b27bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7120be8-ea1f-4f38-83f4-5ccf2b611aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5205a8-dd50-4d3f-92ed-d8b04abb5ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7120be8-ea1f-4f38-83f4-5ccf2b611aac",
                    "LayerId": "f6adae16-1e07-450f-8064-1cdb83a869bb"
                }
            ]
        },
        {
            "id": "d9d94e9d-7636-4422-a79b-1529dc1cc10c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f84b021d-86f1-4dd8-af50-3a0d46a1d084",
            "compositeImage": {
                "id": "3ac363f1-f525-4ce7-b9cb-92aacf0f7697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d94e9d-7636-4422-a79b-1529dc1cc10c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48266e66-41be-40bd-8c46-c6d7fc0f1206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d94e9d-7636-4422-a79b-1529dc1cc10c",
                    "LayerId": "f6adae16-1e07-450f-8064-1cdb83a869bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f6adae16-1e07-450f-8064-1cdb83a869bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f84b021d-86f1-4dd8-af50-3a0d46a1d084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}