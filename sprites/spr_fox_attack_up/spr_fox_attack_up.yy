{
    "id": "d65d1b15-af8a-4ead-87d1-1197ec9ebd19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 17,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0b443a7-bae4-4d6e-9612-d10fba57bca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d65d1b15-af8a-4ead-87d1-1197ec9ebd19",
            "compositeImage": {
                "id": "409a820f-a382-4f1c-9be4-9417ab988e98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0b443a7-bae4-4d6e-9612-d10fba57bca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "678b54fb-33c2-4094-a394-cfe5123e4958",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0b443a7-bae4-4d6e-9612-d10fba57bca3",
                    "LayerId": "f7263000-f55e-437e-a9c9-5224223a05ff"
                }
            ]
        },
        {
            "id": "b0088609-148d-4e4a-a331-de5c27d88e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d65d1b15-af8a-4ead-87d1-1197ec9ebd19",
            "compositeImage": {
                "id": "4dcdfde2-f479-4432-9b7d-bc451c925aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0088609-148d-4e4a-a331-de5c27d88e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1680fbac-42ad-4898-bae9-a6f3071020be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0088609-148d-4e4a-a331-de5c27d88e67",
                    "LayerId": "f7263000-f55e-437e-a9c9-5224223a05ff"
                }
            ]
        },
        {
            "id": "d385ae96-b749-4e65-a1dc-e53b86387ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d65d1b15-af8a-4ead-87d1-1197ec9ebd19",
            "compositeImage": {
                "id": "079bf306-eeff-431a-bdb0-802e33636abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d385ae96-b749-4e65-a1dc-e53b86387ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5f6bbf-1186-4b8a-88ac-2efce528c528",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d385ae96-b749-4e65-a1dc-e53b86387ad1",
                    "LayerId": "f7263000-f55e-437e-a9c9-5224223a05ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f7263000-f55e-437e-a9c9-5224223a05ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d65d1b15-af8a-4ead-87d1-1197ec9ebd19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 22
}