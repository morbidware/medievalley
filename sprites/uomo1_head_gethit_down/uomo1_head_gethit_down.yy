{
    "id": "e5935870-d8d9-4e19-8dc4-7d8ca9cc84d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2541801c-4c8a-4f88-bdf3-7d67e6319943",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5935870-d8d9-4e19-8dc4-7d8ca9cc84d2",
            "compositeImage": {
                "id": "e16f16db-18ab-47e9-b3b3-aa75fd116858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2541801c-4c8a-4f88-bdf3-7d67e6319943",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b65138f9-1953-47c6-ae17-ee70605d9cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2541801c-4c8a-4f88-bdf3-7d67e6319943",
                    "LayerId": "91d93e6a-bb99-4c4f-9b72-220acb86d58a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "91d93e6a-bb99-4c4f-9b72-220acb86d58a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5935870-d8d9-4e19-8dc4-7d8ca9cc84d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}