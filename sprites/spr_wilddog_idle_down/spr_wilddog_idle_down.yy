{
    "id": "33a57651-b4ed-46e5-8722-59aebf4a4094",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wilddog_idle_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 14,
    "bbox_right": 25,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "840a9c94-e142-4841-afaf-b563e7cd0062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a57651-b4ed-46e5-8722-59aebf4a4094",
            "compositeImage": {
                "id": "cde45340-66ca-4877-9157-4bc14a554b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "840a9c94-e142-4841-afaf-b563e7cd0062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9258df69-f1e5-4fe4-8c94-3b77e132c8d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "840a9c94-e142-4841-afaf-b563e7cd0062",
                    "LayerId": "4de7f2e2-4194-42dc-9cd4-72565dcc78af"
                }
            ]
        },
        {
            "id": "16e2c9c9-5cad-4567-8e05-4bc98634916d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a57651-b4ed-46e5-8722-59aebf4a4094",
            "compositeImage": {
                "id": "2c341542-a1b5-4f7b-b9f5-72eca855bd90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16e2c9c9-5cad-4567-8e05-4bc98634916d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bb3a0c-890a-44a2-8a83-2d7c41a28af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16e2c9c9-5cad-4567-8e05-4bc98634916d",
                    "LayerId": "4de7f2e2-4194-42dc-9cd4-72565dcc78af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4de7f2e2-4194-42dc-9cd4-72565dcc78af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33a57651-b4ed-46e5-8722-59aebf4a4094",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 22
}