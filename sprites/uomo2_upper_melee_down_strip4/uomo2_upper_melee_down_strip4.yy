{
    "id": "e5c663b4-fec6-4c6b-9213-dc019836c75e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo2_upper_melee_down_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe423195-d20d-45d8-a72a-19f50394e752",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c663b4-fec6-4c6b-9213-dc019836c75e",
            "compositeImage": {
                "id": "b88d2db5-886f-4405-96cd-7d202a4bb90c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe423195-d20d-45d8-a72a-19f50394e752",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f117d230-933b-4f71-91f0-15da1e5fa0c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe423195-d20d-45d8-a72a-19f50394e752",
                    "LayerId": "ce1c0696-90da-4911-87b9-9f57f77ffb50"
                }
            ]
        },
        {
            "id": "c02936aa-1647-4686-b904-951b5060eb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c663b4-fec6-4c6b-9213-dc019836c75e",
            "compositeImage": {
                "id": "f79cf6a0-2bcd-49b4-8cf9-7c3c561bd20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c02936aa-1647-4686-b904-951b5060eb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b48dcad7-1dd0-4c05-b9b2-cd649a8757b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c02936aa-1647-4686-b904-951b5060eb72",
                    "LayerId": "ce1c0696-90da-4911-87b9-9f57f77ffb50"
                }
            ]
        },
        {
            "id": "b9048bc4-39df-40ed-9dc5-28872152c202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c663b4-fec6-4c6b-9213-dc019836c75e",
            "compositeImage": {
                "id": "37f5f98d-08fc-4ca6-b1fa-99c1d46a0a3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9048bc4-39df-40ed-9dc5-28872152c202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ea65fbe-4799-4926-950f-b0f282882508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9048bc4-39df-40ed-9dc5-28872152c202",
                    "LayerId": "ce1c0696-90da-4911-87b9-9f57f77ffb50"
                }
            ]
        },
        {
            "id": "fd359d92-3afd-427f-857a-ba87437f248d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c663b4-fec6-4c6b-9213-dc019836c75e",
            "compositeImage": {
                "id": "53158022-dea7-411a-97f7-638e31e286ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd359d92-3afd-427f-857a-ba87437f248d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac884214-26ee-40ce-89a9-bd72a0dc8249",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd359d92-3afd-427f-857a-ba87437f248d",
                    "LayerId": "ce1c0696-90da-4911-87b9-9f57f77ffb50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce1c0696-90da-4911-87b9-9f57f77ffb50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5c663b4-fec6-4c6b-9213-dc019836c75e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}