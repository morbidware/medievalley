{
    "id": "a62541c1-20e9-4172-b942-9c3388d70fa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_soil_variation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b87f2e2-1fa0-4074-844c-6b8024e09b35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a62541c1-20e9-4172-b942-9c3388d70fa1",
            "compositeImage": {
                "id": "841ac7c9-4b71-4337-815a-fb3ff38a2e27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b87f2e2-1fa0-4074-844c-6b8024e09b35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b6c0048-8943-4d5d-8873-0762da292d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b87f2e2-1fa0-4074-844c-6b8024e09b35",
                    "LayerId": "10060587-7eac-4011-a139-64aac48bf5b9"
                }
            ]
        },
        {
            "id": "3a382422-3d81-4aa2-b99d-59bfe4759227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a62541c1-20e9-4172-b942-9c3388d70fa1",
            "compositeImage": {
                "id": "84663a97-4b5a-4e38-a619-61aac576c140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a382422-3d81-4aa2-b99d-59bfe4759227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb5e80dc-18eb-4e9d-9fe8-da33b027fbe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a382422-3d81-4aa2-b99d-59bfe4759227",
                    "LayerId": "10060587-7eac-4011-a139-64aac48bf5b9"
                }
            ]
        },
        {
            "id": "a7c487e9-9cdf-4ee2-b3ad-3eca2156ccd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a62541c1-20e9-4172-b942-9c3388d70fa1",
            "compositeImage": {
                "id": "00fc17b1-8a67-4e46-b4ea-39ff1b54ef94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7c487e9-9cdf-4ee2-b3ad-3eca2156ccd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c3399ca-e22b-45e4-b6bc-977a93a913dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7c487e9-9cdf-4ee2-b3ad-3eca2156ccd1",
                    "LayerId": "10060587-7eac-4011-a139-64aac48bf5b9"
                }
            ]
        },
        {
            "id": "3ea2beb1-8ac9-4e19-9b75-64fa753e571e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a62541c1-20e9-4172-b942-9c3388d70fa1",
            "compositeImage": {
                "id": "a8ffc976-c6ab-4749-9684-f6e16a0b8ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea2beb1-8ac9-4e19-9b75-64fa753e571e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f0b906d-1a6e-409a-9f3c-f422cf70119d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea2beb1-8ac9-4e19-9b75-64fa753e571e",
                    "LayerId": "10060587-7eac-4011-a139-64aac48bf5b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "10060587-7eac-4011-a139-64aac48bf5b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a62541c1-20e9-4172-b942-9c3388d70fa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}