{
    "id": "7a3ebfb8-6391-4825-af84-b5e492e69a06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_witch_lair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c973770-0e16-499a-972f-080118452ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a3ebfb8-6391-4825-af84-b5e492e69a06",
            "compositeImage": {
                "id": "6b0c6258-55d4-40ae-82e8-6b94e02ec125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c973770-0e16-499a-972f-080118452ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66e7eab-49e8-43cd-aa85-02534ad5bff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c973770-0e16-499a-972f-080118452ef6",
                    "LayerId": "0687144f-92a6-4272-b843-bc7405843275"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0687144f-92a6-4272-b843-bc7405843275",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a3ebfb8-6391-4825-af84-b5e492e69a06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}