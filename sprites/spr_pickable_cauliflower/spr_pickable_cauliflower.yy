{
    "id": "64c85de8-1c9d-48a7-9e3e-e0e36d6dedd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_cauliflower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58d340db-6f47-4e64-a67b-e4d80a47aebe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64c85de8-1c9d-48a7-9e3e-e0e36d6dedd6",
            "compositeImage": {
                "id": "53bb89fe-d60c-4e86-81e2-1e1107033252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58d340db-6f47-4e64-a67b-e4d80a47aebe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be8ffdd5-32c9-4259-8369-0779181be3a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58d340db-6f47-4e64-a67b-e4d80a47aebe",
                    "LayerId": "22d9fdad-f1bd-4c95-8b58-c5c1d4774d43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "22d9fdad-f1bd-4c95-8b58-c5c1d4774d43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64c85de8-1c9d-48a7-9e3e-e0e36d6dedd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}