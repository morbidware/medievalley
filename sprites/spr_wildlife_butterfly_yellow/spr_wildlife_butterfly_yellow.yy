{
    "id": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wildlife_butterfly_yellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80b98897-0d79-4709-9200-1abdd70981f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "038c364a-682b-44c0-ac0b-2b588926910e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80b98897-0d79-4709-9200-1abdd70981f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca10d4f-0996-4ba4-9ff2-2e916f31a6d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80b98897-0d79-4709-9200-1abdd70981f8",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "a4606868-eb8c-4565-8294-41c43c5045c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "34c549e9-d9a0-4d66-8965-fbad55eafdfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4606868-eb8c-4565-8294-41c43c5045c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2145a6-6694-40f7-bc8c-c53989c8055c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4606868-eb8c-4565-8294-41c43c5045c1",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "b8bfa086-5e93-4ef7-83db-f7a97e546524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "a619b077-8622-41b3-8f9e-9d2221feabf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8bfa086-5e93-4ef7-83db-f7a97e546524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f75bb604-a9db-4c09-904c-d49d87238420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8bfa086-5e93-4ef7-83db-f7a97e546524",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "609f7f5d-66c0-4ccb-8dfe-6065192e7b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "493289b4-6f16-46db-a6b2-882a2e6669b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609f7f5d-66c0-4ccb-8dfe-6065192e7b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a16568-57b5-4918-a2b3-35594e5c9fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609f7f5d-66c0-4ccb-8dfe-6065192e7b3b",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "fa14e960-e755-4d10-a001-3ed556a143d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "92969836-fda0-40e7-a2a9-b9eb09bc310a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa14e960-e755-4d10-a001-3ed556a143d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e465baa9-a6a3-4e45-9262-f0991c217e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa14e960-e755-4d10-a001-3ed556a143d6",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "2f39e194-3b4c-4ba8-a294-91e0e344ce17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "f9a80aae-fca9-4c5f-8ce1-45621560438a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f39e194-3b4c-4ba8-a294-91e0e344ce17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0774e7ee-d16f-495b-8873-b0f99e12ced5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f39e194-3b4c-4ba8-a294-91e0e344ce17",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "83299310-1e01-454f-b126-1865a51f3735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "74fbe82d-6dfd-4f45-8974-8822e56ae7f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83299310-1e01-454f-b126-1865a51f3735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b8f8fc-e3ed-4b9f-b912-6107f7d9f7b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83299310-1e01-454f-b126-1865a51f3735",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "38860e31-c2ca-4d21-972b-4837e4cc1828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "91af0855-767e-42f4-8dff-33cd0917aec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38860e31-c2ca-4d21-972b-4837e4cc1828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65fc1d56-c70b-4d2e-879e-7c1fe4d489a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38860e31-c2ca-4d21-972b-4837e4cc1828",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "0208e98a-4f31-44ab-885c-932fd27e47c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "88347583-cc0c-484f-b41d-52d7a420d6cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0208e98a-4f31-44ab-885c-932fd27e47c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e5a639-8edc-47d4-b6d3-488ab5e81241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0208e98a-4f31-44ab-885c-932fd27e47c2",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "f9a46650-4608-4b6f-8689-f0034161b629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "e64278ee-7642-45f6-8d65-91b23ce39932",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9a46650-4608-4b6f-8689-f0034161b629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abbec5f8-000a-46ac-9ab9-ead181499564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9a46650-4608-4b6f-8689-f0034161b629",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "b48a3f3c-16fa-414a-9bb0-7e29aa856ad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "f9705db5-8381-4622-8308-9c8287aec389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48a3f3c-16fa-414a-9bb0-7e29aa856ad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1481b862-a671-4f0e-9854-ff4e48e7b57f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48a3f3c-16fa-414a-9bb0-7e29aa856ad7",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        },
        {
            "id": "bb905efa-ef76-42f1-8d7b-9091bba79ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "compositeImage": {
                "id": "42291262-2157-4c17-b81f-0b151bbbd74d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb905efa-ef76-42f1-8d7b-9091bba79ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86bb0cb9-047e-42d5-aa76-9027852559f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb905efa-ef76-42f1-8d7b-9091bba79ec5",
                    "LayerId": "e7379e7a-42cd-402c-bdb4-ce75a74594ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e7379e7a-42cd-402c-bdb4-ce75a74594ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0980deaa-fcb1-4bb3-afd2-4aae76a8bd37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}