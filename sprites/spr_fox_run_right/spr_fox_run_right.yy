{
    "id": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fox_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96ba5019-9153-48ca-8200-d1d0e646ec7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "compositeImage": {
                "id": "57a1feae-a989-45c3-8a5d-4eba9edf1bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ba5019-9153-48ca-8200-d1d0e646ec7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5195daf4-f4e7-4796-8bd0-09c2e3cbd8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ba5019-9153-48ca-8200-d1d0e646ec7d",
                    "LayerId": "d9d0c12a-e90c-454a-968f-c42eddd38c6f"
                }
            ]
        },
        {
            "id": "0d4cc521-63db-493b-ada0-2bed451a1d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "compositeImage": {
                "id": "b87915f0-9394-4029-bcd2-fecec5753ead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d4cc521-63db-493b-ada0-2bed451a1d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5e7f2f-b37a-4bfe-b1ad-80e304f6c4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d4cc521-63db-493b-ada0-2bed451a1d3b",
                    "LayerId": "d9d0c12a-e90c-454a-968f-c42eddd38c6f"
                }
            ]
        },
        {
            "id": "38651321-c4b4-4e23-8841-80428be8cbab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "compositeImage": {
                "id": "f5ba9a26-7c61-477d-a689-c7376bde027b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38651321-c4b4-4e23-8841-80428be8cbab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68457f2f-8960-4573-979c-2217d56dd85e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38651321-c4b4-4e23-8841-80428be8cbab",
                    "LayerId": "d9d0c12a-e90c-454a-968f-c42eddd38c6f"
                }
            ]
        },
        {
            "id": "00d41463-a96f-4782-bf54-b9c7b541996c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "compositeImage": {
                "id": "c40f0ff0-d0ae-4b37-9f8d-5d2faf240c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00d41463-a96f-4782-bf54-b9c7b541996c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eda816c-41ac-4f31-8979-e00978a09c1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00d41463-a96f-4782-bf54-b9c7b541996c",
                    "LayerId": "d9d0c12a-e90c-454a-968f-c42eddd38c6f"
                }
            ]
        },
        {
            "id": "51fd7f7b-665b-4af0-9052-3929aefe645e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "compositeImage": {
                "id": "6b99bf1d-cedc-42ab-a264-8ebbae62d641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51fd7f7b-665b-4af0-9052-3929aefe645e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71dc455e-8d4c-42fb-b735-dd5f23c7cb70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51fd7f7b-665b-4af0-9052-3929aefe645e",
                    "LayerId": "d9d0c12a-e90c-454a-968f-c42eddd38c6f"
                }
            ]
        },
        {
            "id": "44cc444a-b414-4665-9011-ccda9b10d43b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "compositeImage": {
                "id": "e7f03bf7-2d7f-40d9-bcfa-4a6afd32a063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44cc444a-b414-4665-9011-ccda9b10d43b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac3852e-9d0e-429c-9c1c-f501874e3b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44cc444a-b414-4665-9011-ccda9b10d43b",
                    "LayerId": "d9d0c12a-e90c-454a-968f-c42eddd38c6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "d9d0c12a-e90c-454a-968f-c42eddd38c6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc5c0d4c-7ee7-4682-bb95-d86654c12eda",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}