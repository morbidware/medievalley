{
    "id": "45bbc257-7903-4b3e-9d05-05844836c793",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_suffer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 5,
    "bbox_right": 43,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed2fb51e-117d-412a-afef-6e8a0e8dcf26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45bbc257-7903-4b3e-9d05-05844836c793",
            "compositeImage": {
                "id": "c610eeac-c8ea-4193-b837-863c9b8bfebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed2fb51e-117d-412a-afef-6e8a0e8dcf26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccba42fb-ad84-4eba-92e3-02dc67dfd8db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed2fb51e-117d-412a-afef-6e8a0e8dcf26",
                    "LayerId": "bd810e23-b08f-48bb-b516-69fc4a3522b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "bd810e23-b08f-48bb-b516-69fc4a3522b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45bbc257-7903-4b3e-9d05-05844836c793",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}