{
    "id": "49f2d285-d443-49a1-8204-a4c94d682dcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9889fb13-afd9-4847-a1f7-0e418e8d7aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f2d285-d443-49a1-8204-a4c94d682dcf",
            "compositeImage": {
                "id": "394b0022-8bf2-436e-b407-0b5f2bf98adb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9889fb13-afd9-4847-a1f7-0e418e8d7aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd9f9407-7c84-4e78-9af1-e46ce3af6f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9889fb13-afd9-4847-a1f7-0e418e8d7aac",
                    "LayerId": "eb6db979-fd46-466a-8b95-0d90817f8ba4"
                }
            ]
        },
        {
            "id": "b1c606ac-80f2-47dc-8924-a5a89b543ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f2d285-d443-49a1-8204-a4c94d682dcf",
            "compositeImage": {
                "id": "380b2e36-1b7c-47bc-b89e-f8b9621b40e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c606ac-80f2-47dc-8924-a5a89b543ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c3524cc-9134-4365-b066-f839b87446df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c606ac-80f2-47dc-8924-a5a89b543ef5",
                    "LayerId": "eb6db979-fd46-466a-8b95-0d90817f8ba4"
                }
            ]
        },
        {
            "id": "8240090d-3ad4-4ca2-b591-eecd2e497274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f2d285-d443-49a1-8204-a4c94d682dcf",
            "compositeImage": {
                "id": "6bf07d01-dadf-493f-8fcb-d8890ba2e861",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8240090d-3ad4-4ca2-b591-eecd2e497274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df251d5d-d4f4-4c8d-ae81-ccd6b7112a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8240090d-3ad4-4ca2-b591-eecd2e497274",
                    "LayerId": "eb6db979-fd46-466a-8b95-0d90817f8ba4"
                }
            ]
        },
        {
            "id": "1370b49d-0685-4a30-9f06-96ec2267380c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49f2d285-d443-49a1-8204-a4c94d682dcf",
            "compositeImage": {
                "id": "187df161-d75b-4e13-9e85-a5bba400fdcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1370b49d-0685-4a30-9f06-96ec2267380c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2851ef-e01d-4428-b1c4-2b123c4dcdd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1370b49d-0685-4a30-9f06-96ec2267380c",
                    "LayerId": "eb6db979-fd46-466a-8b95-0d90817f8ba4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eb6db979-fd46-466a-8b95-0d90817f8ba4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49f2d285-d443-49a1-8204-a4c94d682dcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}