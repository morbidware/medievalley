{
    "id": "2e612e13-64cd-4f08-bc4a-6225276e7c70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna1_upper_hammer_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2969c0c-ef49-4c8c-8150-c5486907c3f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e612e13-64cd-4f08-bc4a-6225276e7c70",
            "compositeImage": {
                "id": "95814826-fdcc-4001-a558-261ae29c12d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2969c0c-ef49-4c8c-8150-c5486907c3f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda4cba8-8373-4065-b441-83d85f0fd350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2969c0c-ef49-4c8c-8150-c5486907c3f7",
                    "LayerId": "763a3337-06e2-4c33-b243-4ac7b907d27f"
                }
            ]
        },
        {
            "id": "f79e9e55-d1b6-432c-9edc-3ba73d26d269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e612e13-64cd-4f08-bc4a-6225276e7c70",
            "compositeImage": {
                "id": "8da316b5-1141-400e-b3de-2bd13415f3a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f79e9e55-d1b6-432c-9edc-3ba73d26d269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4728ce3-1ed6-4a5f-8b5c-63dba5209b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f79e9e55-d1b6-432c-9edc-3ba73d26d269",
                    "LayerId": "763a3337-06e2-4c33-b243-4ac7b907d27f"
                }
            ]
        },
        {
            "id": "a1cd47dc-cf01-486a-b3ff-cddbf594ee2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e612e13-64cd-4f08-bc4a-6225276e7c70",
            "compositeImage": {
                "id": "c6388411-4497-4306-b44b-866f34f06599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1cd47dc-cf01-486a-b3ff-cddbf594ee2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08655fa-0234-4cd7-8e43-f32ff50f368b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1cd47dc-cf01-486a-b3ff-cddbf594ee2d",
                    "LayerId": "763a3337-06e2-4c33-b243-4ac7b907d27f"
                }
            ]
        },
        {
            "id": "5f7ce100-76d5-4721-a62e-ae5149b09aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e612e13-64cd-4f08-bc4a-6225276e7c70",
            "compositeImage": {
                "id": "a7831e53-978f-41aa-9dfb-db9075129522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7ce100-76d5-4721-a62e-ae5149b09aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcaaca1d-f468-41db-a4f9-6c3f23ac3930",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7ce100-76d5-4721-a62e-ae5149b09aa9",
                    "LayerId": "763a3337-06e2-4c33-b243-4ac7b907d27f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "763a3337-06e2-4c33-b243-4ac7b907d27f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e612e13-64cd-4f08-bc4a-6225276e7c70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}