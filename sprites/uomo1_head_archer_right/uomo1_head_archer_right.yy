{
    "id": "e995d5d0-cc9d-4e7f-8dcf-9dd5a2d21f74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_archer_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 11,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73e188ed-f6e3-4e83-8dc8-3f5a001e4e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e995d5d0-cc9d-4e7f-8dcf-9dd5a2d21f74",
            "compositeImage": {
                "id": "2c955875-38ee-49a1-b1f9-8cbcad81da2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73e188ed-f6e3-4e83-8dc8-3f5a001e4e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "426c5125-731c-4c66-8be1-e0efeba0cdfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73e188ed-f6e3-4e83-8dc8-3f5a001e4e4e",
                    "LayerId": "189cb1b5-05a4-46ff-b9a1-f5df065cbc74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "189cb1b5-05a4-46ff-b9a1-f5df065cbc74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e995d5d0-cc9d-4e7f-8dcf-9dd5a2d21f74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}