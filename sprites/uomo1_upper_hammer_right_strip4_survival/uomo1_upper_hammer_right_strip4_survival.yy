{
    "id": "b683f7ae-d39c-43e7-809f-5aff0811264d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_upper_hammer_right_strip4_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1a2285a-1abf-4050-828d-05671bf5c045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "compositeImage": {
                "id": "e7986af2-5e7d-42f4-a534-2923d2178058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1a2285a-1abf-4050-828d-05671bf5c045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7e0be1-c099-4342-ab17-c6fadfc7d533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1a2285a-1abf-4050-828d-05671bf5c045",
                    "LayerId": "f127d774-c673-443b-b122-8ece211c5f8e"
                }
            ]
        },
        {
            "id": "f5ed30e3-142e-47c2-a8c0-f404cf8eadb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "compositeImage": {
                "id": "b9b9f12a-6db0-493e-bc10-3d1b0c6e15ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ed30e3-142e-47c2-a8c0-f404cf8eadb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec3e6d1-1a99-44ba-b9e6-5b87bc8a379e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ed30e3-142e-47c2-a8c0-f404cf8eadb3",
                    "LayerId": "f127d774-c673-443b-b122-8ece211c5f8e"
                }
            ]
        },
        {
            "id": "0fcb5450-fce1-4ef2-ae50-a5535a877781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "compositeImage": {
                "id": "0117a90a-eb15-48e2-a5a2-64860f0bdb17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fcb5450-fce1-4ef2-ae50-a5535a877781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c4094c2-64a4-40fd-ad77-21179eb38f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fcb5450-fce1-4ef2-ae50-a5535a877781",
                    "LayerId": "f127d774-c673-443b-b122-8ece211c5f8e"
                }
            ]
        },
        {
            "id": "69967875-0ab1-4074-89a3-7bc5b795d1e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "compositeImage": {
                "id": "c9448e3b-ad13-4834-acde-4789077238f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69967875-0ab1-4074-89a3-7bc5b795d1e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd158ea7-9330-4569-9a7c-717394c84589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69967875-0ab1-4074-89a3-7bc5b795d1e0",
                    "LayerId": "f127d774-c673-443b-b122-8ece211c5f8e"
                }
            ]
        },
        {
            "id": "67ecccbf-6167-49ff-af72-34bcc4dd6518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "compositeImage": {
                "id": "baab551e-0a66-4c16-a636-c22fcb747cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67ecccbf-6167-49ff-af72-34bcc4dd6518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fdd312d-6cfa-4c6e-a7eb-e1501ae14afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67ecccbf-6167-49ff-af72-34bcc4dd6518",
                    "LayerId": "f127d774-c673-443b-b122-8ece211c5f8e"
                }
            ]
        },
        {
            "id": "e7c92703-928f-48cb-9010-130c6370fa88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "compositeImage": {
                "id": "e0623143-f6db-45da-80f9-0db68594b196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c92703-928f-48cb-9010-130c6370fa88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b697e7ab-3050-4c19-bc14-2b5be88285ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c92703-928f-48cb-9010-130c6370fa88",
                    "LayerId": "f127d774-c673-443b-b122-8ece211c5f8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f127d774-c673-443b-b122-8ece211c5f8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b683f7ae-d39c-43e7-809f-5aff0811264d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 120,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}