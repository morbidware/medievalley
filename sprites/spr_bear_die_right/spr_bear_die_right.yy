{
    "id": "cf6759c4-5379-4c6f-98c5-8f8bc49c2826",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bear_die_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99b6a914-c4d0-454b-b604-9224ca53efce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf6759c4-5379-4c6f-98c5-8f8bc49c2826",
            "compositeImage": {
                "id": "82f6fdf7-ccac-4a89-b694-71ca4385bf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b6a914-c4d0-454b-b604-9224ca53efce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc03df7b-a939-4d41-9658-26226b2d4ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b6a914-c4d0-454b-b604-9224ca53efce",
                    "LayerId": "322bb4a3-e7a6-4ffa-ae46-3c06bef0737b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "322bb4a3-e7a6-4ffa-ae46-3c06bef0737b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf6759c4-5379-4c6f-98c5-8f8bc49c2826",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 44
}