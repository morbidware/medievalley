{
    "id": "9c966a0e-730a-4daf-9fd1-27482c2380d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nightwolf_suffer_right_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "693465ba-9f09-4d5e-84e5-116b8762e6b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c966a0e-730a-4daf-9fd1-27482c2380d2",
            "compositeImage": {
                "id": "f65c2898-2a9a-4dc3-866e-ead51691776f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693465ba-9f09-4d5e-84e5-116b8762e6b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da39b520-1411-47e9-97a2-add9b644925f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693465ba-9f09-4d5e-84e5-116b8762e6b1",
                    "LayerId": "629471b5-35b1-4f65-8468-739ee02783cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "629471b5-35b1-4f65-8468-739ee02783cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c966a0e-730a-4daf-9fd1-27482c2380d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 36
}