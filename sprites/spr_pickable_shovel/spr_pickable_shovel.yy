{
    "id": "9ef441d6-f817-47e7-8d4f-f0545532cc13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_shovel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54894dfe-d454-478c-87ef-62b4075f2f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ef441d6-f817-47e7-8d4f-f0545532cc13",
            "compositeImage": {
                "id": "42ac8d0d-6492-4fe4-8241-396509f03cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54894dfe-d454-478c-87ef-62b4075f2f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a501276-d322-4111-a5c7-96603df746fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54894dfe-d454-478c-87ef-62b4075f2f5f",
                    "LayerId": "ca181d08-dd55-40ed-a19c-f23672abc8a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ca181d08-dd55-40ed-a19c-f23672abc8a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ef441d6-f817-47e7-8d4f-f0545532cc13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}