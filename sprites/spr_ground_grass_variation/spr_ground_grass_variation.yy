{
    "id": "e09671de-c823-4398-b9bc-96d5056dbc6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_grass_variation",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a00fd2de-857b-4bef-97b4-b215aca15044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e09671de-c823-4398-b9bc-96d5056dbc6a",
            "compositeImage": {
                "id": "3a9836b5-c547-4e6b-b688-786b3551812a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a00fd2de-857b-4bef-97b4-b215aca15044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d823a8-a6ed-446e-9503-f4d758758789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a00fd2de-857b-4bef-97b4-b215aca15044",
                    "LayerId": "e7da6ff6-f161-428e-8275-f00303ee55c7"
                }
            ]
        },
        {
            "id": "22dcc572-faeb-4aa5-827b-ada3cb1208b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e09671de-c823-4398-b9bc-96d5056dbc6a",
            "compositeImage": {
                "id": "8f94f840-f0d2-4ea0-a9c0-881c1ff9bb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22dcc572-faeb-4aa5-827b-ada3cb1208b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca45f821-de9b-4b18-87aa-a576d9ba07a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22dcc572-faeb-4aa5-827b-ada3cb1208b1",
                    "LayerId": "e7da6ff6-f161-428e-8275-f00303ee55c7"
                }
            ]
        },
        {
            "id": "e3fa0787-5585-4098-85e5-32707e6f0c86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e09671de-c823-4398-b9bc-96d5056dbc6a",
            "compositeImage": {
                "id": "3c2433df-f44a-4688-8776-9f88630cdf85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3fa0787-5585-4098-85e5-32707e6f0c86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8910d1ad-534b-4e3c-9cca-3a5c09aac8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3fa0787-5585-4098-85e5-32707e6f0c86",
                    "LayerId": "e7da6ff6-f161-428e-8275-f00303ee55c7"
                }
            ]
        },
        {
            "id": "38337405-5843-48fb-b00e-7e32b264e73a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e09671de-c823-4398-b9bc-96d5056dbc6a",
            "compositeImage": {
                "id": "43075a79-ab24-4c0a-ac7a-e0d4ad1a50e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38337405-5843-48fb-b00e-7e32b264e73a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4d8045-107d-429a-a7fc-e6fc6f55fab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38337405-5843-48fb-b00e-7e32b264e73a",
                    "LayerId": "e7da6ff6-f161-428e-8275-f00303ee55c7"
                }
            ]
        },
        {
            "id": "0a595d23-d85d-46ae-b885-0e59852de609",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e09671de-c823-4398-b9bc-96d5056dbc6a",
            "compositeImage": {
                "id": "ae646758-64bd-4046-a41e-6cf94d62e021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a595d23-d85d-46ae-b885-0e59852de609",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772b5fc0-4efa-48e5-9502-dcf288b7a024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a595d23-d85d-46ae-b885-0e59852de609",
                    "LayerId": "e7da6ff6-f161-428e-8275-f00303ee55c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e7da6ff6-f161-428e-8275-f00303ee55c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e09671de-c823-4398-b9bc-96d5056dbc6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}