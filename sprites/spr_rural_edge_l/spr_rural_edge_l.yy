{
    "id": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_edge_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01430785-a1df-4d02-8185-8ec725f1f0bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
            "compositeImage": {
                "id": "86b23020-e00c-47d3-bb96-6a6653e13778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01430785-a1df-4d02-8185-8ec725f1f0bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef72745-ad87-47cc-8063-f1cd68e0a13c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01430785-a1df-4d02-8185-8ec725f1f0bd",
                    "LayerId": "0cc34d2c-73f0-4387-9521-2eaf1598fb1a"
                }
            ]
        },
        {
            "id": "943645d4-4f7c-44d4-8029-ee79ea6ced37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
            "compositeImage": {
                "id": "99df2d3c-ebc6-4560-bfc7-e05e15aceee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "943645d4-4f7c-44d4-8029-ee79ea6ced37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "086a9ab2-b3ef-4948-90c4-2d79627bb0a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "943645d4-4f7c-44d4-8029-ee79ea6ced37",
                    "LayerId": "0cc34d2c-73f0-4387-9521-2eaf1598fb1a"
                }
            ]
        },
        {
            "id": "c1718ae2-9c01-4c38-9e90-f56ebb5675ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
            "compositeImage": {
                "id": "b21e1b6c-15a6-4f48-89ed-7839b9c3b6f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1718ae2-9c01-4c38-9e90-f56ebb5675ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71a86f89-7d38-4015-b1f1-61470a65d88f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1718ae2-9c01-4c38-9e90-f56ebb5675ae",
                    "LayerId": "0cc34d2c-73f0-4387-9521-2eaf1598fb1a"
                }
            ]
        },
        {
            "id": "c09ce46b-cb77-49cc-9cf9-d30aa1b33f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
            "compositeImage": {
                "id": "f54d65da-8db1-42c0-93e7-bb1ed0d37a1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c09ce46b-cb77-49cc-9cf9-d30aa1b33f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371c2d3c-a005-4a76-b57e-29ffe592aff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c09ce46b-cb77-49cc-9cf9-d30aa1b33f64",
                    "LayerId": "0cc34d2c-73f0-4387-9521-2eaf1598fb1a"
                }
            ]
        },
        {
            "id": "23df291d-61a3-49fd-bd6a-b0a4980ed3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
            "compositeImage": {
                "id": "010e471b-c9fb-431d-ad95-e2b5ec65d31c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23df291d-61a3-49fd-bd6a-b0a4980ed3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d3b2d29-ab24-4dde-8ef1-4b85d94142b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23df291d-61a3-49fd-bd6a-b0a4980ed3db",
                    "LayerId": "0cc34d2c-73f0-4387-9521-2eaf1598fb1a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0cc34d2c-73f0-4387-9521-2eaf1598fb1a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70319ba0-b428-40b1-9b3c-34e4a9972b97",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4278223103,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}