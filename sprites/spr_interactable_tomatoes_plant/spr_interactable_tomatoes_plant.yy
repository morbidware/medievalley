{
    "id": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_interactable_tomatoes_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 185,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b412aadd-07f4-456b-afd6-48b1ef315aa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "compositeImage": {
                "id": "1c4669eb-31cc-484c-91b6-f7ca5a62286c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b412aadd-07f4-456b-afd6-48b1ef315aa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "724f80eb-322f-48aa-80e7-8a86e9fa46a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b412aadd-07f4-456b-afd6-48b1ef315aa4",
                    "LayerId": "0c0a05ba-bc26-4526-95ac-212c82b8591e"
                }
            ]
        },
        {
            "id": "d54bafee-abdb-488c-9731-50270b501971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "compositeImage": {
                "id": "2e4ccf5e-6214-46f5-9bc5-238d11accc53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d54bafee-abdb-488c-9731-50270b501971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b6b0ef-6092-4a27-96be-51c0bbd48c5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d54bafee-abdb-488c-9731-50270b501971",
                    "LayerId": "0c0a05ba-bc26-4526-95ac-212c82b8591e"
                }
            ]
        },
        {
            "id": "4df93c76-f9d7-439b-bcbc-20c58df84564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "compositeImage": {
                "id": "4e9aeca5-6d65-415f-ae8c-46ba3e60a39c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4df93c76-f9d7-439b-bcbc-20c58df84564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7773108e-15b4-4891-988d-45e17d63c2bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4df93c76-f9d7-439b-bcbc-20c58df84564",
                    "LayerId": "0c0a05ba-bc26-4526-95ac-212c82b8591e"
                }
            ]
        },
        {
            "id": "f2828ce8-fb09-40f9-9e26-a85a970330bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "compositeImage": {
                "id": "1dc33187-38d6-4501-a881-408a4bac2e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2828ce8-fb09-40f9-9e26-a85a970330bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e13a1ab-53b3-4223-a7cb-00d1290c0d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2828ce8-fb09-40f9-9e26-a85a970330bf",
                    "LayerId": "0c0a05ba-bc26-4526-95ac-212c82b8591e"
                }
            ]
        },
        {
            "id": "321740e8-18e4-4fe3-95ea-117b423ab5a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "compositeImage": {
                "id": "513ad624-05bb-4e9b-8baf-ba06b5d3c9ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "321740e8-18e4-4fe3-95ea-117b423ab5a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cb7e262-9fdf-4e97-a025-842a4cbea1d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "321740e8-18e4-4fe3-95ea-117b423ab5a0",
                    "LayerId": "0c0a05ba-bc26-4526-95ac-212c82b8591e"
                }
            ]
        },
        {
            "id": "af2be817-8b36-4149-be53-27f5eb9a1d70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "compositeImage": {
                "id": "5af1f396-d7b0-4f7b-b004-e6ed936e058a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af2be817-8b36-4149-be53-27f5eb9a1d70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d04af6e-b3e9-4a06-9fa8-d7545f2d79cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af2be817-8b36-4149-be53-27f5eb9a1d70",
                    "LayerId": "0c0a05ba-bc26-4526-95ac-212c82b8591e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0c0a05ba-bc26-4526-95ac-212c82b8591e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1955736c-a902-4632-9c8c-f7eb2fb21b61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 29
}