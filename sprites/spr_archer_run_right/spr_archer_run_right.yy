{
    "id": "cfab3dd4-4254-4814-b512-9e29e3bc5449",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_archer_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26f055fa-b91b-4ff1-950c-25e4faff3dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfab3dd4-4254-4814-b512-9e29e3bc5449",
            "compositeImage": {
                "id": "1bd59607-8588-45c3-8d66-449a58bee004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f055fa-b91b-4ff1-950c-25e4faff3dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a74d637-4ddc-405b-bc64-72e8a73fd628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f055fa-b91b-4ff1-950c-25e4faff3dc7",
                    "LayerId": "a4ea4a50-e9a2-4fd4-a821-5d46d55d91b3"
                }
            ]
        },
        {
            "id": "4449c390-3b4b-4f3d-9dd0-e9e4d9eb255d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfab3dd4-4254-4814-b512-9e29e3bc5449",
            "compositeImage": {
                "id": "d2932484-f646-4be2-90ab-a4911eded226",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4449c390-3b4b-4f3d-9dd0-e9e4d9eb255d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01d2e56e-eebf-4561-80c5-d4d5f87d0fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4449c390-3b4b-4f3d-9dd0-e9e4d9eb255d",
                    "LayerId": "a4ea4a50-e9a2-4fd4-a821-5d46d55d91b3"
                }
            ]
        },
        {
            "id": "a241fd83-c592-4380-b121-da7068d01cb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfab3dd4-4254-4814-b512-9e29e3bc5449",
            "compositeImage": {
                "id": "3e7eab32-1a41-49b1-91bb-759e1dec8fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a241fd83-c592-4380-b121-da7068d01cb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "079107ab-02bd-4772-a287-3aaea1de3c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a241fd83-c592-4380-b121-da7068d01cb5",
                    "LayerId": "a4ea4a50-e9a2-4fd4-a821-5d46d55d91b3"
                }
            ]
        },
        {
            "id": "c3f3253b-0673-49b7-a276-92071287928e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfab3dd4-4254-4814-b512-9e29e3bc5449",
            "compositeImage": {
                "id": "8bdcbf21-7224-4c8d-b359-e5864461d6b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f3253b-0673-49b7-a276-92071287928e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9c25bb-1d9b-40b5-94e6-38a870f2d4b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f3253b-0673-49b7-a276-92071287928e",
                    "LayerId": "a4ea4a50-e9a2-4fd4-a821-5d46d55d91b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a4ea4a50-e9a2-4fd4-a821-5d46d55d91b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfab3dd4-4254-4814-b512-9e29e3bc5449",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 31
}