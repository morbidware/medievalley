{
    "id": "c80230dc-bdaa-48e7-89bd-42569fa760af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_cliff_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cc925c7-21e9-4023-9b7b-6ba0224111f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "f5e794c8-74e5-4b2e-bb88-c61bec2dc7c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cc925c7-21e9-4023-9b7b-6ba0224111f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4213ab61-d100-4d69-a16d-5df6a28f3b2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cc925c7-21e9-4023-9b7b-6ba0224111f4",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "5ee22750-27cd-4956-bbdd-0945400cb1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "988feb04-e9f1-4b5c-86ea-42d878ff126a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee22750-27cd-4956-bbdd-0945400cb1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8eb96a3b-be53-49d5-800f-9725f188d8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee22750-27cd-4956-bbdd-0945400cb1f5",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "76285909-3da5-4d07-801c-b4154248a920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "44d28f8e-6390-4ef7-9d1a-4e8bb38edd30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76285909-3da5-4d07-801c-b4154248a920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f97e73b0-832e-4a9e-aa73-b3e7d895ac8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76285909-3da5-4d07-801c-b4154248a920",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "e2193e9e-e974-4ab9-b2a5-a2ae9deda33b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "a683a543-5236-406b-9786-d3dd9dc4b7c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2193e9e-e974-4ab9-b2a5-a2ae9deda33b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eede4abe-e134-45c8-a8f4-2a3da3e6f22e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2193e9e-e974-4ab9-b2a5-a2ae9deda33b",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "8a91532e-b2f9-4671-a219-5cbefcdf9883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "3704a9de-a055-4564-abcf-4d77f7a53a05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a91532e-b2f9-4671-a219-5cbefcdf9883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c370bf6c-16f5-4889-a637-905111ad8dc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a91532e-b2f9-4671-a219-5cbefcdf9883",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "84e2bfda-c07a-41a8-9940-8932388e26b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "eb5b23a0-29b3-4e8c-8108-230c3a81d9e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84e2bfda-c07a-41a8-9940-8932388e26b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4d697a-36e5-49c0-b62a-eac5abb4953d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84e2bfda-c07a-41a8-9940-8932388e26b9",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "87487de3-eb24-4b7f-b8f4-85b2c65ee1a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "3d55e870-07be-45c1-a89a-490eede0a2e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87487de3-eb24-4b7f-b8f4-85b2c65ee1a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4c745c-9856-4a67-9266-ae5cde1a47fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87487de3-eb24-4b7f-b8f4-85b2c65ee1a0",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "445d6aec-833d-4f47-bacb-af625a7585b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "64859abd-c9ff-4deb-bcfd-1416017c8055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "445d6aec-833d-4f47-bacb-af625a7585b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a5a4239-7ae3-4217-bc2b-efe1991446aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "445d6aec-833d-4f47-bacb-af625a7585b1",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "73d7529a-ee90-4e6b-ac5e-30e24ce7834e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "9f8bd87b-ac58-40f5-9c35-7800f85187e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73d7529a-ee90-4e6b-ac5e-30e24ce7834e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8987b88d-f3c7-45ff-9151-4958a7449999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73d7529a-ee90-4e6b-ac5e-30e24ce7834e",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "7918cdaa-8ca1-495f-bd84-3dada10e4c22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e09f1e56-970d-4154-819a-bbb876351225",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7918cdaa-8ca1-495f-bd84-3dada10e4c22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "650b436e-2a3c-4062-a69f-d918d40ff043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7918cdaa-8ca1-495f-bd84-3dada10e4c22",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "133e8554-ed2f-425a-b130-36b3970bf67a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "921b8b25-ffe4-40b7-9725-0d660a468beb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "133e8554-ed2f-425a-b130-36b3970bf67a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf945c6f-9a3f-4467-9b44-7aaf269d7c53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "133e8554-ed2f-425a-b130-36b3970bf67a",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "f73085a2-edb5-43b7-bf32-d1b936f290bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "0f2a63d1-5bcd-4fef-9ba3-e216dc0dea97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f73085a2-edb5-43b7-bf32-d1b936f290bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75668ca6-cb12-47fc-a6b1-53c0e34545ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f73085a2-edb5-43b7-bf32-d1b936f290bd",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "6daa86c4-7788-4bfe-a725-9473f87bea01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "658ad542-8c27-4185-8bab-5e20ed667d0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6daa86c4-7788-4bfe-a725-9473f87bea01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fdfde6a-aabd-49c9-ad7d-6781c51eebad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6daa86c4-7788-4bfe-a725-9473f87bea01",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "687fc963-a03a-419b-82b7-be6e576df12b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "40dac017-ebb9-447f-979c-a451db16f62b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "687fc963-a03a-419b-82b7-be6e576df12b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41d82501-59fe-4835-b0b4-9930977ef183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "687fc963-a03a-419b-82b7-be6e576df12b",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "ead37201-1a96-430d-b122-84c444b2a63f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "27b579cf-58df-46ec-a14d-1b9ff6df26e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ead37201-1a96-430d-b122-84c444b2a63f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a545cfc7-08fe-4337-8247-0a3b779838bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ead37201-1a96-430d-b122-84c444b2a63f",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "c9c3e457-8f87-4470-865b-9b7ac0d22976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "2e48904e-e8d8-4b73-a0d9-9408780ebed3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c3e457-8f87-4470-865b-9b7ac0d22976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa583b9-7193-4706-afb1-794f7b91bdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c3e457-8f87-4470-865b-9b7ac0d22976",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "55766c61-7bf3-4921-a914-0c900197f0ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "8c06a427-0fc7-48a2-9912-207853f6a534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55766c61-7bf3-4921-a914-0c900197f0ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0a4929-7ba2-4be9-92ea-bac3b727fa79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55766c61-7bf3-4921-a914-0c900197f0ec",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "1c7228c2-019c-434b-98ff-9b850d4fa399",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "edbca8e7-8237-491e-9e53-e6804306fa57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c7228c2-019c-434b-98ff-9b850d4fa399",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4aa7266-2703-494e-b6a4-22ec5d3eeaf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c7228c2-019c-434b-98ff-9b850d4fa399",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "9c0b4753-2136-4858-81d7-c801a89f32b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "37031420-ff2f-4cf7-88a1-bcdc3a326dcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c0b4753-2136-4858-81d7-c801a89f32b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ed01f0-d42a-4af9-a857-59b764b0840d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c0b4753-2136-4858-81d7-c801a89f32b6",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "dee3de7d-8c22-4ebc-b6b1-c05fead9de63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e1bb6e8f-67b8-46f2-9a27-4311faa67577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee3de7d-8c22-4ebc-b6b1-c05fead9de63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c83d30a-cb33-41e6-bb02-cabc96bf0c95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee3de7d-8c22-4ebc-b6b1-c05fead9de63",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "843ec953-5f36-405b-8b58-37b3a6cc96f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "d81cebea-2df3-4f8b-b626-b9cbddcb76ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "843ec953-5f36-405b-8b58-37b3a6cc96f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba4c862-5aa0-41eb-8b3f-9e8b1c1cb905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "843ec953-5f36-405b-8b58-37b3a6cc96f1",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "d8d35f3a-9d08-4fd6-9414-2a8d5aec0c59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "ca7ad2c0-7221-4e2d-b081-d4a6a6eb4436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8d35f3a-9d08-4fd6-9414-2a8d5aec0c59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01dd236d-ca49-481c-adcd-76f1d3f3ef25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8d35f3a-9d08-4fd6-9414-2a8d5aec0c59",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "43ad9eb5-f8a6-402b-98fd-65d1bca12591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "d5dea3ef-e0fd-4d89-8d78-737a62fb20da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ad9eb5-f8a6-402b-98fd-65d1bca12591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466b85a6-bd49-43b7-92a0-fd85820ffb57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ad9eb5-f8a6-402b-98fd-65d1bca12591",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "b1cc4381-f48b-43fb-9c1a-7e4921a918fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "ba45b277-86ad-4c30-8884-02b1a85d070e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1cc4381-f48b-43fb-9c1a-7e4921a918fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9bd607a-d06a-4cbd-8da0-17fab8f8af9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1cc4381-f48b-43fb-9c1a-7e4921a918fc",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "5139fd6c-75f7-479d-b800-61aa6cf5ec6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e59673ad-a83f-4712-8ab4-2bf6faf9408c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5139fd6c-75f7-479d-b800-61aa6cf5ec6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c067542-0a46-4ebc-9a94-97de73f52b4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5139fd6c-75f7-479d-b800-61aa6cf5ec6a",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "1af7c23a-b714-4d1e-b0a8-d2d4cf0f5223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "1478085b-3e5a-4e24-9023-bd9a25c1b1d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af7c23a-b714-4d1e-b0a8-d2d4cf0f5223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efe02006-5d8d-448c-bacb-4088aec27e90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af7c23a-b714-4d1e-b0a8-d2d4cf0f5223",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "0f49158f-de90-426f-a1e1-d5219fdfec7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "4e80a5c7-2645-4251-bd25-eb107d6568a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f49158f-de90-426f-a1e1-d5219fdfec7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50ee8f21-d842-4cd1-a8f8-9e8e9ed16f9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f49158f-de90-426f-a1e1-d5219fdfec7d",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "26032544-ebbd-459b-b686-314197634ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "5fce665a-db7e-4c62-ac63-f87639ee2fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26032544-ebbd-459b-b686-314197634ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5f3f93c-7af5-458d-be59-963648a19354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26032544-ebbd-459b-b686-314197634ee5",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "c28e09c2-e1f0-433f-a7e8-3e448e3e3828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "5bbeca4f-5ab9-402e-b99e-ed59faa6c332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28e09c2-e1f0-433f-a7e8-3e448e3e3828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72bd629a-588f-4cc9-943a-03d9c9383cf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28e09c2-e1f0-433f-a7e8-3e448e3e3828",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "7cdef5a4-fb2c-4b8e-93a3-93337f47ba0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "ec986eca-7726-4457-a551-e43060d1cc91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cdef5a4-fb2c-4b8e-93a3-93337f47ba0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "712f3d5a-4ff6-45ca-8aed-f3d5223d1a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cdef5a4-fb2c-4b8e-93a3-93337f47ba0c",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "e1d00be4-a5b7-4013-80c3-145b456fd0be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e26669e1-61ba-491c-8460-8268548b3b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d00be4-a5b7-4013-80c3-145b456fd0be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a524d5d-1615-43e6-8d57-f58f9c8172ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d00be4-a5b7-4013-80c3-145b456fd0be",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "e7cf9d0e-e40c-4c30-83ac-0323ba4216fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "93908191-7058-4e13-9be1-f76a8c3a23c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7cf9d0e-e40c-4c30-83ac-0323ba4216fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc04d07-1c05-4cac-82a3-ff0fb6c4ef7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7cf9d0e-e40c-4c30-83ac-0323ba4216fa",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "94af4214-626f-4992-8713-4fd191033e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e57fe79c-9e65-4c3a-9b78-451d0fc06ff5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94af4214-626f-4992-8713-4fd191033e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aef6cbe-1f25-46ad-bebb-5299c1b7f4be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94af4214-626f-4992-8713-4fd191033e77",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "5b08b6e9-7410-4a5b-8f65-85f31af13235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "d99f72ef-e515-4748-a814-185985a79918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b08b6e9-7410-4a5b-8f65-85f31af13235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "900bfd44-b599-489b-957c-ad7de86554b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b08b6e9-7410-4a5b-8f65-85f31af13235",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "e653b92d-b565-46c9-ba02-827828305f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "a5197714-b008-4ca8-8269-6ada89bd5807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e653b92d-b565-46c9-ba02-827828305f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83c268a5-4031-4283-a150-284b4a4ad179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e653b92d-b565-46c9-ba02-827828305f42",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "4067bdb1-2358-4b2f-b780-4deabf7c6b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "56d08ac7-cf6a-4efe-96cf-0c42e5644691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4067bdb1-2358-4b2f-b780-4deabf7c6b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b09ee9-3aaa-4e05-97b4-563b5207790f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4067bdb1-2358-4b2f-b780-4deabf7c6b5c",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "8b9b9e56-8ed4-45f2-89df-c46f44f258c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e25bacd5-784f-4c1c-a91f-e2b00023ed8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b9b9e56-8ed4-45f2-89df-c46f44f258c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da0b8247-a0ed-4283-ab90-ce19d4b7ca9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b9b9e56-8ed4-45f2-89df-c46f44f258c6",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "7d4a0e5f-91fd-4c19-8535-0824789bb022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "2e2e2cbd-0706-437b-a6ed-38010ddd73ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4a0e5f-91fd-4c19-8535-0824789bb022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61fd08a5-8464-4551-9a0d-78b5293ab53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4a0e5f-91fd-4c19-8535-0824789bb022",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "7774ecf0-a562-4e9e-94d8-2ffd7ab7760d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "961674bf-8d97-48e1-bcce-1fac9be5eab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7774ecf0-a562-4e9e-94d8-2ffd7ab7760d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f190f4a-6368-4e1e-af11-1f827518ad20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7774ecf0-a562-4e9e-94d8-2ffd7ab7760d",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "7fa79afd-96f9-46c8-834c-b6f1aa3d0486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "4b049cb0-46da-47d2-85d0-a804bc6d0dbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa79afd-96f9-46c8-834c-b6f1aa3d0486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e2b556f-a547-4f7a-95a7-7c9d783410ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa79afd-96f9-46c8-834c-b6f1aa3d0486",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "28ca8359-9387-403c-9bb0-8c0829e1fd1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "d52d63e6-3e64-4543-aaa4-76c92d0354bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ca8359-9387-403c-9bb0-8c0829e1fd1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "211b13be-9acd-4824-a766-62c1377c213a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ca8359-9387-403c-9bb0-8c0829e1fd1b",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "0aa06998-e872-411c-983a-dae41787474d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "e342c30b-c152-47f2-9528-5b6646dddbc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aa06998-e872-411c-983a-dae41787474d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a28e6c-9088-47c6-92d9-325126de3785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aa06998-e872-411c-983a-dae41787474d",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "e20a3edd-50bd-4942-a96a-4e68646971af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "69a99173-c41e-4576-8d8f-9e918ac01e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e20a3edd-50bd-4942-a96a-4e68646971af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62d48d90-c3eb-4d4e-be09-677a95c151a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e20a3edd-50bd-4942-a96a-4e68646971af",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "585be31e-33d0-4fac-99d7-edecb1a5aa58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "8b536aa2-a643-4cbd-84a8-1dc81fec8fc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "585be31e-33d0-4fac-99d7-edecb1a5aa58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b712be17-3f82-432b-bbb5-8b5dc5704b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "585be31e-33d0-4fac-99d7-edecb1a5aa58",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "05e39309-785e-4978-a488-424cd01a67a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "55559491-c172-4343-b0b2-a646517afdcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e39309-785e-4978-a488-424cd01a67a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c433546-2a00-4105-a8e5-ee02c7720f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e39309-785e-4978-a488-424cd01a67a2",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "e216f735-c932-4d13-8716-ae0b98ecaec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "f521ed8b-7a31-4505-a4da-43d393622278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e216f735-c932-4d13-8716-ae0b98ecaec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48c616c0-18e8-432d-a249-10a8beb4ed31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e216f735-c932-4d13-8716-ae0b98ecaec2",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "7cb554cf-086f-4df9-8f58-1b2ebddf48b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "68aabfca-804d-4eef-b04d-5d12599a62d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb554cf-086f-4df9-8f58-1b2ebddf48b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee34e28f-ef9d-405e-a74a-6801504d65e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb554cf-086f-4df9-8f58-1b2ebddf48b9",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "5675804f-3fed-471d-992d-6b15790e8527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "b1d69577-ec2c-4126-a2de-25d3cd829b25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5675804f-3fed-471d-992d-6b15790e8527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe48c1ce-efc2-4936-bae5-6832b6773b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5675804f-3fed-471d-992d-6b15790e8527",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "042b1707-a893-4379-b67c-cd28437a9d86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "d39b6f48-fab2-4a09-aa68-036db263bda4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042b1707-a893-4379-b67c-cd28437a9d86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf00a1d6-4b0b-4035-9e5c-25a20e11968c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042b1707-a893-4379-b67c-cd28437a9d86",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "5262bed2-9489-4a97-9e4a-a88699cb068a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "afd973e0-9785-410b-b096-49b00736159e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5262bed2-9489-4a97-9e4a-a88699cb068a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb50e7a-7e6f-4694-89ab-0b7b5669c289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5262bed2-9489-4a97-9e4a-a88699cb068a",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "c68d97fb-abe0-43ac-bba2-d250e3cc7bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "ee69d474-b766-44b3-b454-e2bfb2ddaf62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68d97fb-abe0-43ac-bba2-d250e3cc7bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f5b7c27-67e5-409a-9156-f1b2249943a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68d97fb-abe0-43ac-bba2-d250e3cc7bf1",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "65582396-ef77-40b2-a154-daf22e9c2a0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "2cab192d-7853-414e-91fd-82072443f930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65582396-ef77-40b2-a154-daf22e9c2a0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e37cef3-f3af-434f-a07d-c394d9693100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65582396-ef77-40b2-a154-daf22e9c2a0d",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "92b28f7f-4998-42b6-a5cd-e9c593f58818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "ab209de9-efb2-43a3-95f2-a05e5e61ffda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92b28f7f-4998-42b6-a5cd-e9c593f58818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "800fa07b-ef5a-4747-a4ca-47fbce06ba40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92b28f7f-4998-42b6-a5cd-e9c593f58818",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "6a87eaf2-36d0-4800-8905-0a7f24f899c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "2c988155-13e6-4800-b0f1-ad0a8e362a02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a87eaf2-36d0-4800-8905-0a7f24f899c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f64f180f-8e2e-4b6a-816f-d9e329f760b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a87eaf2-36d0-4800-8905-0a7f24f899c4",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "cd50bba4-c40d-4e5f-aabe-c0f9b70934c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "a4b00212-81c3-4e14-9d82-d166c0a1b0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd50bba4-c40d-4e5f-aabe-c0f9b70934c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72992945-0e7e-4d52-b432-5adc7060f710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd50bba4-c40d-4e5f-aabe-c0f9b70934c3",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "c0f67a4a-7954-48d8-a864-cfb94b9bbed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "81ef6c93-9bc4-4dd2-9538-1bce51791d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0f67a4a-7954-48d8-a864-cfb94b9bbed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "550b1a07-784f-43a5-bbb5-d8650167ebfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0f67a4a-7954-48d8-a864-cfb94b9bbed4",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        },
        {
            "id": "d1803dd2-0e6d-4f61-bdc3-40abe8517787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "compositeImage": {
                "id": "80b8df36-8f6e-42b0-b83b-45316f8a0353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1803dd2-0e6d-4f61-bdc3-40abe8517787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2019641-7f43-4eb5-80af-aa2b1ac95a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1803dd2-0e6d-4f61-bdc3-40abe8517787",
                    "LayerId": "e3f0080e-d510-4774-ad89-049610526d65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e3f0080e-d510-4774-ad89-049610526d65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c80230dc-bdaa-48e7-89bd-42569fa760af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}