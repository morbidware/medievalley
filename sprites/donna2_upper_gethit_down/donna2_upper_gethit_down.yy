{
    "id": "fbcc18a8-7450-43c4-8353-a653525c6c40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_upper_gethit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96cfa64c-cdf4-44e0-bd4a-959bc5b94655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbcc18a8-7450-43c4-8353-a653525c6c40",
            "compositeImage": {
                "id": "a9c4063f-5684-45d3-87f1-c4f852531407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96cfa64c-cdf4-44e0-bd4a-959bc5b94655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39d0f37e-9922-4c9e-8ec3-47f708be9c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96cfa64c-cdf4-44e0-bd4a-959bc5b94655",
                    "LayerId": "cde19bb0-b65e-4331-8c74-ae72c3b44c07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "cde19bb0-b65e-4331-8c74-ae72c3b44c07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbcc18a8-7450-43c4-8353-a653525c6c40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}