{
    "id": "d28a98ab-5a55-41ce-bd9b-c42bda474999",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_graveyard_fence_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 227,
    "bbox_left": 2,
    "bbox_right": 237,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7299616-fcd5-45e8-89fc-992db1dcb7d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d28a98ab-5a55-41ce-bd9b-c42bda474999",
            "compositeImage": {
                "id": "cd434491-0d43-4a41-ad9c-b955865622cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7299616-fcd5-45e8-89fc-992db1dcb7d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c0fb59d-54d9-44ae-be16-1fd1a8276fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7299616-fcd5-45e8-89fc-992db1dcb7d2",
                    "LayerId": "b5ea5f64-fc6a-4bc1-8040-177dddaebb7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 228,
    "layers": [
        {
            "id": "b5ea5f64-fc6a-4bc1-8040-177dddaebb7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d28a98ab-5a55-41ce-bd9b-c42bda474999",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 120,
    "yorig": 34
}