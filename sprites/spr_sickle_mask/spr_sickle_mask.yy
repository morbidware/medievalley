{
    "id": "02fcd46b-39b7-4fc5-b482-3fc893fe455c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sickle_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d67e979-2aca-4622-b269-7ba17f2cc43a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02fcd46b-39b7-4fc5-b482-3fc893fe455c",
            "compositeImage": {
                "id": "a5a73936-04cb-41ed-9fa3-4f3ada462884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d67e979-2aca-4622-b269-7ba17f2cc43a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491d0f2c-4ff1-462b-a87c-6bb20f2f7264",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d67e979-2aca-4622-b269-7ba17f2cc43a",
                    "LayerId": "8c93974b-2d4d-4ba4-a033-c4b16785c47a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "8c93974b-2d4d-4ba4-a033-c4b16785c47a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02fcd46b-39b7-4fc5-b482-3fc893fe455c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 17
}