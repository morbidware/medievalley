{
    "id": "1bc3ff25-4635-46ce-abb2-70e0e6086fe2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "male_body_archer_right_survival",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3044eff2-4e9d-404f-8e48-54c6332fc5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bc3ff25-4635-46ce-abb2-70e0e6086fe2",
            "compositeImage": {
                "id": "bba3bf86-0df5-4abc-be92-9c254930fc05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3044eff2-4e9d-404f-8e48-54c6332fc5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24223378-16e3-485e-b00d-6ee80c81e016",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3044eff2-4e9d-404f-8e48-54c6332fc5ec",
                    "LayerId": "c7a14b06-88bc-4827-afea-8ee1dd889ab8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c7a14b06-88bc-4827-afea-8ee1dd889ab8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bc3ff25-4635-46ce-abb2-70e0e6086fe2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}