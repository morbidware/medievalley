{
    "id": "65d64d9f-5aaf-4dab-a90e-62918878b9c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_crafting_food",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f8868a2-e51d-4e9a-af4f-a9148b8dbe3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d64d9f-5aaf-4dab-a90e-62918878b9c8",
            "compositeImage": {
                "id": "b66e3291-d4e7-4f01-a6a1-4f0d867df623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f8868a2-e51d-4e9a-af4f-a9148b8dbe3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3d804b3-cd61-4e1c-a287-cfaf3f827aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f8868a2-e51d-4e9a-af4f-a9148b8dbe3a",
                    "LayerId": "b8e57ef8-b346-46f2-a4f4-4ca6e49c24c9"
                }
            ]
        },
        {
            "id": "7bae16d1-2e3a-45e5-aef9-9390a90dfec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65d64d9f-5aaf-4dab-a90e-62918878b9c8",
            "compositeImage": {
                "id": "bd6e9bf7-7294-41ae-8a48-861d15f628d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bae16d1-2e3a-45e5-aef9-9390a90dfec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35af011-fa98-47ee-8714-f18a9458b511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bae16d1-2e3a-45e5-aef9-9390a90dfec1",
                    "LayerId": "b8e57ef8-b346-46f2-a4f4-4ca6e49c24c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "b8e57ef8-b346-46f2-a4f4-4ca6e49c24c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65d64d9f-5aaf-4dab-a90e-62918878b9c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 0,
    "yorig": 0
}