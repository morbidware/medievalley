{
    "id": "ef3896ad-5fe2-465a-bf8b-e405e1bdf259",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 0,
    "bbox_right": 215,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe362888-6c63-4107-99fe-cce0825096a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef3896ad-5fe2-465a-bf8b-e405e1bdf259",
            "compositeImage": {
                "id": "79df17c9-2904-4b2f-a4fb-e290ec24c62a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe362888-6c63-4107-99fe-cce0825096a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e3ee826-c902-45dd-9462-92fe1e624f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe362888-6c63-4107-99fe-cce0825096a2",
                    "LayerId": "e77abb51-570e-4005-b417-280e24736c18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 99,
    "layers": [
        {
            "id": "e77abb51-570e-4005-b417-280e24736c18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef3896ad-5fe2-465a-bf8b-e405e1bdf259",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 216,
    "xorig": 108,
    "yorig": 49
}