{
    "id": "046e790b-b58d-4f70-8d03-b0c86da224b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pickable_groundtrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2480da0-717d-4ce5-a74b-1bc714906b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046e790b-b58d-4f70-8d03-b0c86da224b9",
            "compositeImage": {
                "id": "2bb7e400-742a-4261-a228-66bd3fc71229",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2480da0-717d-4ce5-a74b-1bc714906b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fbf1d1c-1cc2-4f7f-92b0-e25cff022c73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2480da0-717d-4ce5-a74b-1bc714906b12",
                    "LayerId": "611c8b36-8b15-4662-8483-9da0b2a16042"
                }
            ]
        },
        {
            "id": "f6603776-182f-4370-a11a-9a4e651df14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "046e790b-b58d-4f70-8d03-b0c86da224b9",
            "compositeImage": {
                "id": "4e34361b-30f8-4c6f-b62b-c049f94167ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6603776-182f-4370-a11a-9a4e651df14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282fbf0c-a43f-4ec9-85ed-577c35fa04f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6603776-182f-4370-a11a-9a4e651df14e",
                    "LayerId": "611c8b36-8b15-4662-8483-9da0b2a16042"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "611c8b36-8b15-4662-8483-9da0b2a16042",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "046e790b-b58d-4f70-8d03-b0c86da224b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}