{
    "id": "a75ce01e-e575-463b-809a-9070a3070688",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dock_sundial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3f321c1-f026-47e0-9ff5-bf247ec286df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a75ce01e-e575-463b-809a-9070a3070688",
            "compositeImage": {
                "id": "60f7843c-a73a-4a9e-a242-88b1435b32fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f321c1-f026-47e0-9ff5-bf247ec286df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6694e4ed-d684-4cc0-b0b2-45cb8a68f345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f321c1-f026-47e0-9ff5-bf247ec286df",
                    "LayerId": "c70fabb4-9185-41ee-8d67-444898e94b00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c70fabb4-9185-41ee-8d67-444898e94b00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a75ce01e-e575-463b-809a-9070a3070688",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}