{
    "id": "bfe4ad49-7cd0-45ae-bc17-531b92b34521",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo1_head_walk_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5384b922-5f19-491b-b717-2dd736f0ef98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfe4ad49-7cd0-45ae-bc17-531b92b34521",
            "compositeImage": {
                "id": "62fe3cb0-c274-41fe-80e2-8ef4a8471b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5384b922-5f19-491b-b717-2dd736f0ef98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1251834-a1d0-44bc-92f7-ccea9ffd79ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5384b922-5f19-491b-b717-2dd736f0ef98",
                    "LayerId": "25042d64-a48f-405b-82ef-56cc85634e6e"
                }
            ]
        },
        {
            "id": "f4f1ca6a-2a8c-436d-a2d1-21581ac17080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfe4ad49-7cd0-45ae-bc17-531b92b34521",
            "compositeImage": {
                "id": "89778c51-21cd-4a6a-a01d-899458cf821b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4f1ca6a-2a8c-436d-a2d1-21581ac17080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52d01fc4-eecc-4307-9184-8d44820b3d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4f1ca6a-2a8c-436d-a2d1-21581ac17080",
                    "LayerId": "25042d64-a48f-405b-82ef-56cc85634e6e"
                }
            ]
        },
        {
            "id": "2ad29132-e8f4-4aa9-a55e-aaae6ae36adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfe4ad49-7cd0-45ae-bc17-531b92b34521",
            "compositeImage": {
                "id": "53e3dca4-ce4f-4da4-b679-efae5e998471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad29132-e8f4-4aa9-a55e-aaae6ae36adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b096375-e3fb-44cc-90b2-e335814ff8a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad29132-e8f4-4aa9-a55e-aaae6ae36adf",
                    "LayerId": "25042d64-a48f-405b-82ef-56cc85634e6e"
                }
            ]
        },
        {
            "id": "9b12821e-72e6-49f9-b122-b19bc0ad36da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfe4ad49-7cd0-45ae-bc17-531b92b34521",
            "compositeImage": {
                "id": "8c59b73a-e901-4771-b96f-d1457b96ce33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b12821e-72e6-49f9-b122-b19bc0ad36da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe2f777-82ec-4f97-b375-fed349ff6524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b12821e-72e6-49f9-b122-b19bc0ad36da",
                    "LayerId": "25042d64-a48f-405b-82ef-56cc85634e6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "25042d64-a48f-405b-82ef-56cc85634e6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfe4ad49-7cd0-45ae-bc17-531b92b34521",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}