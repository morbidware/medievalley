{
    "id": "bba32043-4878-4467-9a21-c2abe63ecea0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "donna2_head_hammer_right_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c0d482e-d99e-4cd1-b800-9e176d823174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba32043-4878-4467-9a21-c2abe63ecea0",
            "compositeImage": {
                "id": "4b342ad0-1844-4f91-a345-3a614410eba0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c0d482e-d99e-4cd1-b800-9e176d823174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "931016a0-4d58-4c66-af36-53d42424e7fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c0d482e-d99e-4cd1-b800-9e176d823174",
                    "LayerId": "b171bf6e-eb50-4ca3-89ff-9cefb8d755a4"
                }
            ]
        },
        {
            "id": "3d5b8fa3-d18e-4a43-9217-be65e667f90d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba32043-4878-4467-9a21-c2abe63ecea0",
            "compositeImage": {
                "id": "279f4c5f-bfe3-48b7-8df1-933d18d55283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5b8fa3-d18e-4a43-9217-be65e667f90d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31aa38e8-a1f5-4e67-aa01-68ea67d077fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5b8fa3-d18e-4a43-9217-be65e667f90d",
                    "LayerId": "b171bf6e-eb50-4ca3-89ff-9cefb8d755a4"
                }
            ]
        },
        {
            "id": "71a41312-0140-4ea3-b1a8-da39ae0af8d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba32043-4878-4467-9a21-c2abe63ecea0",
            "compositeImage": {
                "id": "dbb68095-af05-4507-bfa4-573696c12ec0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a41312-0140-4ea3-b1a8-da39ae0af8d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f04f517a-1f53-48b3-bab9-a29b41adb621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a41312-0140-4ea3-b1a8-da39ae0af8d9",
                    "LayerId": "b171bf6e-eb50-4ca3-89ff-9cefb8d755a4"
                }
            ]
        },
        {
            "id": "057da6fc-7c8b-4a37-9e1e-cc54fcc6e4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba32043-4878-4467-9a21-c2abe63ecea0",
            "compositeImage": {
                "id": "3f8edd11-02f4-400e-9154-cac7dc387721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057da6fc-7c8b-4a37-9e1e-cc54fcc6e4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e040a41-e186-4084-bc30-8eb6f95226f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057da6fc-7c8b-4a37-9e1e-cc54fcc6e4a3",
                    "LayerId": "b171bf6e-eb50-4ca3-89ff-9cefb8d755a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b171bf6e-eb50-4ca3-89ff-9cefb8d755a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bba32043-4878-4467-9a21-c2abe63ecea0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}