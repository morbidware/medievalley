{
    "id": "7b1d3d4c-bcce-490c-9454-f7fa3b4270f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rural_graveyard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8368f1ed-1929-4f24-a369-a35579bf2603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b1d3d4c-bcce-490c-9454-f7fa3b4270f6",
            "compositeImage": {
                "id": "0e00ff9d-b45f-4641-a3f3-e08dc2e66430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8368f1ed-1929-4f24-a369-a35579bf2603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a47c6acc-4c7d-4956-933a-b37137fe33c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8368f1ed-1929-4f24-a369-a35579bf2603",
                    "LayerId": "d52b5583-4c0b-46d1-9f3d-712332794073"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d52b5583-4c0b-46d1-9f3d-712332794073",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b1d3d4c-bcce-490c-9454-f7fa3b4270f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}