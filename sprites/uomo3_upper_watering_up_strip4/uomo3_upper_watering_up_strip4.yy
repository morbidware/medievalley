{
    "id": "c9464955-a2a0-405b-9cf8-390ed5d67c66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "uomo3_upper_watering_up_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bbe5416-eb19-4ab4-8155-b8955fe55873",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9464955-a2a0-405b-9cf8-390ed5d67c66",
            "compositeImage": {
                "id": "8b52c169-73e2-4e8f-aa16-d05e101bcccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bbe5416-eb19-4ab4-8155-b8955fe55873",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b9e6fb0-afbd-4c9c-8baa-9cd424fcb8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bbe5416-eb19-4ab4-8155-b8955fe55873",
                    "LayerId": "fbc450b1-3647-4847-b821-adbe1658f80f"
                }
            ]
        },
        {
            "id": "0c7630ce-5f80-4a7f-8357-734e4d8a6983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9464955-a2a0-405b-9cf8-390ed5d67c66",
            "compositeImage": {
                "id": "1d070f78-1a1a-4192-8de0-6581ab7de5c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c7630ce-5f80-4a7f-8357-734e4d8a6983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "595db3db-048e-43cf-951e-9131836176fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c7630ce-5f80-4a7f-8357-734e4d8a6983",
                    "LayerId": "fbc450b1-3647-4847-b821-adbe1658f80f"
                }
            ]
        },
        {
            "id": "840fb106-5256-4d74-9b74-c17c350a5622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9464955-a2a0-405b-9cf8-390ed5d67c66",
            "compositeImage": {
                "id": "38008215-1001-42a1-85ec-7739104d2fc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "840fb106-5256-4d74-9b74-c17c350a5622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97a91f7a-fc39-498b-82f8-fecb188a44c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "840fb106-5256-4d74-9b74-c17c350a5622",
                    "LayerId": "fbc450b1-3647-4847-b821-adbe1658f80f"
                }
            ]
        },
        {
            "id": "11dfc9ce-197f-4dbb-b96f-f394b5303b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9464955-a2a0-405b-9cf8-390ed5d67c66",
            "compositeImage": {
                "id": "a8d07e51-98a2-4c45-a412-26bc8f9881e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11dfc9ce-197f-4dbb-b96f-f394b5303b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "941d9b5b-c2b8-4561-a264-fcae9925ad30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11dfc9ce-197f-4dbb-b96f-f394b5303b1e",
                    "LayerId": "fbc450b1-3647-4847-b821-adbe1658f80f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fbc450b1-3647-4847-b821-adbe1658f80f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9464955-a2a0-405b-9cf8-390ed5d67c66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}