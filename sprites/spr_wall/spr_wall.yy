{
    "id": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "febe7f7d-85a9-4745-b5d6-1045762ecd75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
            "compositeImage": {
                "id": "1cf70a7c-cd22-4535-b0f5-873fa11acbb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "febe7f7d-85a9-4745-b5d6-1045762ecd75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab79861f-e265-422b-9ae1-0b7a48077ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "febe7f7d-85a9-4745-b5d6-1045762ecd75",
                    "LayerId": "07b6d86a-af78-4eef-928c-2f30aa00d9a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "07b6d86a-af78-4eef-928c-2f30aa00d9a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45e0cfc1-0d9a-4e56-8fee-52ebe169b003",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}